function show_opt4D_results(plan_file, out_file_prefix)
%show_opt4D_results(plan, out_file_prefix)
%  The out_file_prefix can be a cell array of strings


% Call up a dialog to choose a plan file if it is not included already.
if(~exist('plan_file'))
    [file_name, path_name, filter_index] = uigetfile( ...
         {'*.pln', 'Opt4D Plan File (*.pln)'}, ...
         'Pick pln file', plan_file_name);
    if(filter_index == 0)
        disp( 'User pressed cancel.');
	return
    elseif(filter_index == 1)
	plan_file = [path_name, file_name];
    end
end

% Read plan from file if a file name was given
if(isstr(plan_file))
    plan = read_pln(plan_file);
end

max_steps = 1;

% Make sure plan has all required parts
if(isstruct(plan))
    if(~isfield(plan,'title'))
	plan.title = '';
    end
    % if( || ~isfield(plan,'vois'))
	% error('plan is not a valid structure.');
    % end
else
    error('plan is not a structure.');
end

if(~iscell(out_file_prefix))
    out_file_prefix = {out_file_prefix};
end

plot_opt4D_convergence(plan, out_file_prefix);

legends = cell(size(out_file_prefix));
for(iRun = 1:numel(out_file_prefix))
    [pathstr, name, ext] = fileparts(out_file_prefix{iRun});
    legends{iRun} = strrep(name, '_', ' ');
end


for(iRun = 1:numel(out_file_prefix))
    temp_prefix = out_file_prefix{iRun};

    % Plot DVH
    if(exist([temp_prefix 'DVH.dat'], 'file'))
      figure('name', ['DVH: ' temp_prefix]);
      plot_DVH([temp_prefix 'DVH.dat'], plan_file);
    end

    % Show beamlet intensities
    if(exist([temp_prefix 'beam_1_1.bwf'],'file'))
        figure('name', ['Bixel Map: ' temp_prefix]);
        show_beamlet_intensities([temp_prefix 'beam'], plan.nInstances, ...
        plan.nBeams);
    end

end


% Explore the dose if it exists
for(iRun = 1:numel(out_file_prefix))
    temp_prefix = out_file_prefix{iRun};

    if(exist([temp_prefix 'dose.dat'], 'file'))
	figure('name',[plan.title ' Dose: ' temp_prefix]);
        explore_dose([temp_prefix 'dose.dat'], plan_file);
    end
end
