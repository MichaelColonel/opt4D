%example_phantom.m Script to test phantom

nBeams = 3;


plan_name = tempname;
writeRtogPhantom(plan_name, nBeams)
optimize_plan([plan_name '.pln'],[plan_name '_results_']);
Plan = read_pln([plan_name '.pln'])

eval(['!rm ' plan_name '*'])
