function [gantry_position] = table_to_gantry(table_angle, ...
        gantry_angle, table_position)
%table_to_gantry Converts from table coordinates to gantry coordinates
%   [gantry_position] = table_to_gantry(table_angle, gantry_angle, ...
%   table_position) Calculates the x,y,z position of voxel in gantry
%   coordinate system.
%
%   table_position can be a (3x1) column vector or a (3xn) matrix.
%   Each column of table_position is the x,y,z position of one point.
%   gantry_position has the same size as table_position.
%

    % Calculate rotation matrix
    cot = cos(table_angle * pi / 180);
    sot = sin(table_angle * pi / 180);
    cog = cos(gantry_angle * pi / 180);
    sog = sin(gantry_angle * pi / 180);
    rotation_matrix = [cog,0,sog;0,1,0;-sog,0,cog] *...
                      [cot,sot,0;-sot,cot,0;0,0,1];

    % Perform rotation
    gantry_position = rotation_matrix * table_position;

return
