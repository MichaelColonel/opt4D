function [d_min, d_max, q_under, q_over] = find_voxel_constraints(plan)
%find_voxel_constraints finds the constraints for each voxel in a plan.
%
%   [d_min, d_max, q_under, q_over] = find_voxel_constraints(plan) takes a plan
%   structure of the name of an opt4D pln file and returns vextors with the
%   constraints and weights for each voxel.
%

% Read plan file if needed
if(isstr(plan))
    plan = read_pln(plan);
end

% Read dimension information file
dif = read_dif(plan.dif_file_name);

nRows = dif.nZ;
nColumns = dif.nX;
nSlices = dif.nY;
nVoxels = nRows * nColumns * nSlices;
nObjectives = length(plan.objectives);

% Read Voi file
voi_list = reshape(read_voi(plan), nVoxels, 1);

% Find prescriptions for each voxel
d_min = zeros(nVoxels,1);
d_max = repmat(inf, nVoxels,1);
q_under = ones(nVoxels, 1);
q_over = ones(nVoxels, 1);

for(i = 1:nObjectives)
    objective = plan.objectives(i);
    voiNo = objective.voiNo;
    voi_mask = voi_list == voiNo;

    voi_size = sum(voi_mask);

    if(voi_size > 0)
        if(objective.is_min_constraint)
            d_min(voi_mask) = objective.desired_dose;
            q_under(voi_mask) = objective.weight / voi_size;
        elseif(objective.is_max_constraint)
            d_max(voi_mask) = objective.desired_dose;
            q_over(voi_mask) = objective.weight / voi_size;
        elseif(objective.is_equal_constraint)
            d_min(voi_mask) = objective.desired_dose;
            q_under(voi_mask) = objective.weight / voi_size;
            d_max(voi_mask) = objective.desired_dose;
            q_over(voi_mask) = objective.weight / voi_size;
        else
            objective
            error('Objective type not supported yet.');
        end
    end
end

return
