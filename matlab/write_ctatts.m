function write_ctatts(ctatts_file, ctatts)
%write_ctatts Writes Konrad ct attributes file.
%   write_ctatts(ctatts_file, ctatts) Writes the ctatts file from the 
%   structure:
%
%     ctatts.cti_file_name
%     ctatts.slice_dimension
%     ctatts.slice_number
%     ctatts.pixel_size
%     ctatts.slice_distance
%     ctatts.Pos_Isocenter_X
%     ctatts.Pos_Isocenter_Y
%     ctatts.Pos_Isocenter_Z
%     ctatts.Number_Of_Voxels_in_CT_Cube_Z


% First open the file for writing
try
    [fid, message] = fopen(ctatts_file, 'wt');
catch
    error(['Error opening ctatts file: ' ctatts_file ' (' lasterr ')']);
end

fprintf(fid, '%s\n', ctatts.cti_file_name);
fprintf(fid, 'slice_dimension %d\n', ctatts.slice_dimension);
fprintf(fid, 'slice_number %d\n', ctatts.slice_number);
fprintf(fid, 'pixel_size %n\n', ctatts.pixel_size);
fprintf(fid, 'slice_distance %n\n', ctatts.slice_distance);
fprintf(fid, 'Pos-Isocenter-X %n\n', ctatts.Pos_Isocenter_X);
fprintf(fid, 'Pos-Isocenter-Y %n\n', ctatts.Pos_Isocenter_Y);
fprintf(fid, 'Pos-Isocenter-Z %n\n', ctatts.Pos_Isocenter_Z);
fprintf(fid, 'Number-Of-Voxels-in-CT-Cube-Z %n\n', ctatts.Number_Of_Voxels_in_CT_Cube_Z);

fclose(fid);

