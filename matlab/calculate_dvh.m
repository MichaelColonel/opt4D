function [dvh, dvh_bins] = calculate_dvh(dose, dvh_bin_size, voi_list, nVois)
%calculate_dvh  Calculates a dose volume histogram
%
%   [dvh] = calculate_dvh(dose) returns an array with the first column vector
%   for the dose in each bin and the second is the volume in each bin.
%
%   [dvh] = calculate_dvh(dose, dvh_bin_size) uses the specified dose bin size 
%   (the default is 0.1)
%
%   [dvh] = calculate_dvh(dose, dvh_bin_size, voi_list, nVois) returns an
%   array with the first column is the dose in each bin and the n'th column
%   corresponds to the volume of the n-1'th volume of interest.
%
%   [dvh, dvh_bins] = calculate_dvh(...) returns the DVH and the dose to each
%   bin seperately.
%
%   Example:   dose = Dij * w;
%              dvh = calculate_dvh(dose, 0.1, voi_list, nVois);
%              plot(dvh(:,1), dvh(:,2:end));
%              title('Dose Volume Histogram');
%              xlabel('Dose (Gy)'); ylabel('Percentage Volume');
%             

if(~exist('dose'))
    error('Insufficient inputs to calculate_dvh');
end

if(~exist('dvh_bin_size') || isempty(dvh_bin_size))
    dvh_bin_size = 0.1;
end
if(~exist('voi_list') || isempty(voi_list))
    voi_list = ones(size(dose));
end
if(~exist('nVois') || isempty(nVois))
    nVois = max(voi_list(:));
end
    

% max_data = max(dose(:));
data_bin = ceil(dose(:) / dvh_bin_size) + 1;
nBins = max(data_bin) + 1;

dvh = [dvh_bin_size * (1:nBins)', zeros(nBins, nVois)];
for(iVoi = 1:nVois)
    temp_bin = data_bin(voi_list == iVoi);
    if(numel(temp_bin) > 0)
        temp_dvh = dvh(:,iVoi+1);
        for(i = 1:numel(temp_bin))
            temp_dvh(temp_bin(i)) = temp_dvh(temp_bin(i)) + 1;
        end
        dvh(:,iVoi+1) = flipud(cumsum(flipud(temp_dvh)))*100/ numel(temp_bin);
    end
end

if(nargout == 2)
    dvh_bins = dvh(:,1);
    dvh = dvh(:,2:end);
end

return
