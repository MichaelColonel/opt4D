function [DVH, DVH_dose] = read_DVH(dvh_file)
%read_DVH  Read a DVH produced by opt4D
%
%   [DVH, DVH_dose] = read_DVH(dvh_file) returns the DVH as an array
%   with one column vector for each volume of interest.
%  
%   See also plot_DVH.


try
    DVH=load(dvh_file);
catch
    error(['Error loading ' dvh_file ' (' lasterr ')']);
end
DVH_dose = DVH(:,1);
DVH = DVH(:,2:end);

return
