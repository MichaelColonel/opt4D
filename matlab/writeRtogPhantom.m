function flag = writeRtogPhantom(outFile, nBeams, nInstances)
% writeRtogPhantom  Writes Konrad formated files for a RTOG phantom
%     [flag] = writeRtogPhantom(outFile, nBeams, nInstances)
%     writes out Konrad files for a RTOG phantom.
%     Creates .dij, .stf, .bwf, .dif, .voi, and .voi2 files.
%
%     IM is the data structure with information about the problem.
%
%     outFile is the path and name for the Konrad files.
%
%     structNums is an array of structure numbers to include in the Konrad
%     files.  Any voxels outside of the structures will be assumed to be air.
%     The first voxels will take priority.  Use structNums = [] to include all
%     structures that have a goal defined.
%
%     beamWeights is an array containing beam weights to use for the start of
%     optimization.  This should be a vector the same length as the number of
%     beamlets.  Use beamWeights = [] to set all weights to 1.
%
%     beamletThreshold is a value between 0 and 1 to only allow beamlets with at
%     least one Dij entry greater than beamletThreshold times the maximum Dij
%     entry.
%
%     flag returns 'error' or 'no error'.
%
%     For example,
%
%         [flag] = writeKonradFiles(IM, 'default')
%
%     creates files named default.voi, default.dif, default_1.dij,
%     default_2.dij, etc.
%
%     Notes:
%     * There are many non-essential values that were given arbitrary defaults.
%       This shouldn't be a problem for optimization, but better values are
%       obviously prefered.
%
%     * To avoid having to change the counting of the indexes of the voxels,
%       this function currently just assigns all of the voxels removed in
%       downsampling to be air.  Run showVoi to see the effect.  This
%       shouldn't be a huge deal since everthing other than the voi is in a
%       sparse format, but it is better avoided.
%
%     * Note that writing out multiple beams will only work if the
%       IM.beams(beam).beamNumber is set correctly.


% ----------------------------------------------------------------- %
% Make sure the inputs to the function are correct.

% Both the output file prefix and the number of beams are required
if(~exist('outFile') || ~exist('nBeams'))
    error('Both the output file prefix and the number of beams are required');
end

% Multiple instances has not been implemented yet.
if(~exist('nInstances'))
    nInstances = 1;
elseif(nInstances > 1)
    error('multiple instances is not implemented yet');
end

% ------------------------------------------------------------------- %
% Initialize global variables

clear global KonradStruct Plan;
global KonradStruct Plan;

% Initialize waitbar
h = waitbar(0);

% ------------------------------------------------------------------- %
% General plan description for creating plan file

Plan.title       = 'RTOG Phantom';
Plan.description = 'RTOG Phantom';
Plan.nInstances  = nInstances;
Plan.nBeams      = nBeams;
Plan.file_root   = outFile;
Plan.params = {'ctatts_file_name','DVH_bin_size'};
Plan.values = {'/home/bmartin/cerr_ex3/cwg_plan1_7beams.ctatts','0.2'};

Plan.vois(1).name = 'target';
Plan.vois(1).color = '1 0 0';
Plan.vois(1).is_target = 'true';
Plan.vois(1).max_DVH = '200 0%';
Plan.vois(1).min_DVH = '200 100%';
Plan.vois(1).weight_over = '1';
Plan.vois(1).weight_under = '1';
Plan.vois(1).motion = ['[ ' num2str(zeros(1,20)) '; ' num2str(zeros(1,20)) '; ' num2str((cos(pi*[0:19]/20)).^4) ']' ];

Plan.vois(2).name = 'organ at risk';
Plan.vois(2).color = '0 0 1';
Plan.vois(2).max_DVH = '120 0%';
Plan.vois(2).weight_over = '1';

Plan.vois(3).name = 'undifferentiated patient';
Plan.vois(3).color = '.5 .5 .5';

% Write the plan file out
try
    waitbar( 0, h,'Writing pln file');
    writePln([outFile '.pln'], Plan);
catch
    warning(['Error writing pln file: ' outFile '.pln']);
    flag = 'error';
    return
end


% ------------------------------------------------------------------- %
% Define general properties of phantom

% Define dimensions of dose cube
sizeX = 15; % cm
sizeY =  5; % cm
sizeZ = 15; % cm

% Define voxel size
dX = 0.2; % cm/row
dY = 0.3; % cm/slice
dZ = 0.2; % cm/column

% Find size of dose cube in voxels
nR = round(sizeX / dX);
nC = round(sizeZ / dZ);
nS = round(sizeY / dY);

% Define index of isocenter (0,0,0) in x,y,z space
isoR = round(nR / 2);
isoC = round(nC / 2);
isoS = round(nS / 2);

isoX = sizeX / 2;
isoY = floor(nS / 2) * dY;
isoZ = sizeZ / 2;

% ------------------------------------------------------------------- %
% Put properties into KonradStruct

% Voxel dimensions in the dose cube
KonradStruct.voxeldX  = dX;
KonradStruct.voxeldZ  = dZ;
KonradStruct.voxeldY  = dY;

% Dose cube dimensions
KonradStruct.doseCubeNr = nR;
KonradStruct.doseCubeNc = nC;
KonradStruct.doseCubeNs = nS;

% ISO index in dose cube
KonradStruct.doseIsoIndexR = isoR;
KonradStruct.doseIsoIndexC = isoC;
KonradStruct.doseIsoIndexS = isoS;
KonradStruct.doseIsoX = isoX;
KonradStruct.doseIsoY = isoY;
KonradStruct.doseIsoZ = isoZ;

% CT cube dimensions
KonradStruct.ctNr  = nR;
KonradStruct.ctNc  = nC;
KonradStruct.ctNs  = nS;

% ISO index in CT cube
% #### NOT RIGHT ####
KonradStruct.ctIndexR  = 1;
KonradStruct.ctIndexC  = 1;
KonradStruct.ctIndexS  = 1;


% ------------------------------------------------------------------- %
% Create volume of interest and influence matrix information
% for phantom

waitbar(0, h, 'Creating Volume of Intrest');
tic

% The RTOG phantom is based on the following geometry:
tx1 = 1.3;              %  half axis of ellipse x, target inner
tz1 = 1.3;              %                       z, target inner
tx2 = 3.8;              %  half axis of ellipse x, target outer
tz2 = 3.8;              %  half axis of ellipse z, target outer

rx = 1.0;               %  half axis of ellipse x, OAR
rz = 1.0;               %                       z, OAR


% Create empty VOI cube
voiCube = zeros(nR,nC,nS);

% Calculate position for every point in VOI cube
voxel_position = get_xyz_table([1:numel(voiCube)]);
x = voxel_position(1,:);
y = voxel_position(2,:);
z = voxel_position(3,:);

% Add target
voi_mask = (z >= 0) & ((x.^2)/(tx2^2) + (z.^2)/(tz2^2) <= 1.0)...
                    & ((x.^2)/(tx1^2) + (z.^2)/(tz1^2) >= 1.0);
voiCube(voi_mask) = 1;

% Add critical tissue
voi_mask = (x.^2)/(rx^2) + (z.^2)/(rz^2) <= 1;
voiCube(voi_mask) = 2;


% ------------------------------------------------------------------- %
% Put Voi Info into KonradStruct

% Find index of each voxel in each structure
for iVoi = 1:length(Plan.vois)
    KonradStruct.voiData{iVoi} = find(voiCube == iVoi);
end



% ------------------------------------------------------------------- %
% Write general files (dif and voi)

% Write the dif file out
try
    waitbar( 0, h,'Writing dif file'); pause(0);
    writeDif(KonradStruct, [outFile '.dif']);
catch
    warning(['Error writing dif file: ' outFile '.dif']);
    flag = 'error';
    return
end

% Write the voi2 file out
try
    waitbar( 0, h,'Writing voi2 file'); pause(0);
    writeVoi2(KonradStruct, [outFile '.voi2']);
catch
    warning(['Error writing voi2 file: ' outFile '.voi2']);
    flag = 'error';
    return
end

% Write the voi file out
try
    waitbar( 0, h,'Writing voi file'); pause(0);
    writeVoi(KonradStruct, [1:length(Plan.vois)], [outFile '.voi']);
catch
    warning(['Error writing voi file: ' outFile '.voi']);
    flag = 'error';
    return
end

% figure;
% showVoi([outFile '.voi'], nR, nC, 0);


% ------------------------------------------------------------------- %
% Create files for each beam
%

% geometry of Siemens MLC: 40x40 cm^2, leaves are 1 cm thick, the outer two
% leaf pairs are 6.5 cm
beam_SAD = 100.;     % 100 cm source to axis distance
beam_SAD = 100000000000000000.;     % 100 cm source to axis distance
beam_Nx=79;
beam_Ny=29;

beam_dx_min = 0.4;  %cm
beam_dy_min = 1; %cm
beam_dx = beam_dx_min * ones(beam_Nx, 1); 
beam_dy = [6.5; beam_dy_min * ones(beam_Ny - 2, 1); 6.5]; 

% Find center of each column of bixels
beam_xpos = ([-(beam_Nx-1)/2:(beam_Nx-1)/2].' * beam_dx_min);

% Find center of each row of bixels.  Note that the top and bottom rows
% have a different width, so they must be accounted for.
beam_ypos = ([-(beam_Ny-1)/2:(beam_Ny-1)/2].' * beam_dy_min);
beam_ypos(1) = beam_ypos(2) - (beam_dy(1)+beam_dx(2))/2;
beam_ypos(end) = beam_ypos(end-1) + (beam_dy(end)+beam_dx(end-1))/2;


for iBeam = 1:nBeams

    toc
    waitbar((iBeam-1) / nBeams, h, ['Creating Beam ' num2str(iBeam)]);
    tic

    % Store important beam properties
    % - Beam number
    KonradStruct.beamNumber = iBeam;

    % Store trivial beam properties
    % - Machine name
    KonradStruct.machineName   = 'phantom';

    beam_energy = 6.0;

    % Beam geometry
    KonradStruct.tableAngle = 0;
    KonradStruct.gantryAngle = 360 / nBeams * (iBeam - 1);
    KonradStruct.collimatorAngle  = 0.0;
    
    % Bixel dimensions
    KonradStruct.spotdx  = beam_dx_min;
    KonradStruct.spotdy  = beam_dy_min;

      
    % Determine which pencil beams interact with targets

    % Create incidence map with no incidence
    beam_aperture = false(beam_Nx, beam_Ny);

    for(iVoi = 1:length(Plan.vois))
      if(Plan.vois(iVoi).is_target)

        % Find position of voxels in target in table coordinates
        position = get_xyz_table(KonradStruct.voiData{iVoi}');
        position = table_to_gantry(position);
        position = gantry_to_cone(position, beam_SAD);

        % Remove points outside range of beam
        position = position(:,position(1,:) > (beam_xpos(1)-beam_dx(1)/2));
        position = position(:,position(1,:) < (beam_xpos(end)+beam_dx(end)/2));
        position = position(:,position(2,:) > (beam_ypos(1)-beam_dy(1)/2));
        position = position(:,position(2,:) < (beam_ypos(end)+beam_dy(end)/2));

        % Find which bixel affects each beam
        for(pos = position)
            % Find which beamlet the voxel belongs to
            ix = min(find(beam_xpos >= pos(1)));
            if(pos(1) < beam_xpos(ix) - beam_dx(ix)/2)
                ix = ix-1;
            end

            iy = min(find(beam_ypos >= pos(2)));
            if(pos(2) < beam_xpos(iy) - beam_dx(iy)/2)
                iy = iy-1;
            end

            % Add that beamlet to aperture
            beam_aperture(ix, iy) = true;
        end
      end
    end
    
    % Store information about all bixels (makes comparing plans easier)
    [xi, yi] = find(ones(size(beam_aperture)));
    
    % - Number of pencil beams
    KonradStruct.numPencilBeams  = beam_Nx * beam_Ny;

    % - Beamlet position
    KonradStruct.spotX  = beam_xpos(xi);
    KonradStruct.spotY  = beam_ypos(yi);

    % - Beamlet energy
    KonradStruct.energy = beam_energy * ones(KonradStruct.numPencilBeams, 1); 

    % Set beam weight to zero for bixels outside aperture
    KonradStruct.beamWeight = beam_aperture(:);

    % Preallocate information about beamlets
    KonradStruct.index = cell(KonradStruct.numPencilBeams,1);
    KonradStruct.value = cell(KonradStruct.numPencilBeams,1);
    KonradStruct.numVoxels = zeros(KonradStruct.numPencilBeams, 1);


    % Calculate beamlet influence matrix

    % - Precalculate position in beam cone coordinates for all points within
    %   dose cube
    position = get_xyz_table([1:nR*nC*nS]);
    position = table_to_gantry(position);
    position = gantry_to_cone(position, beam_SAD);

    % - Calculate influence of all bixels in aperture
    maxdose = 0;
    for(iBixel=1:beam_Nx*beam_Ny)
        % Skip all this if bixel outside of aperture
        if(~beam_aperture(iBixel))
            continue
        end
        
        [bixel_ix, bixel_iy] = ind2sub([beam_Nx, beam_Ny], iBixel);
        bixel_x = beam_xpos(bixel_ix);
        bixel_y = beam_ypos(bixel_iy);
        bixel_dx = beam_dx(bixel_ix);
        bixel_dy = beam_dy(bixel_iy);
        
        % Find water-equivalent radioactive path length for every voxel
        [z_rpl] = radioactive_path_length(position(3,:), beam_SAD);

        % Find position within pencil beam
        pencil_pos = [position(1,:) - bixel_x;
                      position(2,:) - bixel_y;
                      z_rpl];

        % Find dose within pencil beam
        dose_val = pencil_beam_dose(pencil_pos, bixel_dx, bixel_dy);

	if(exist('old_dose_val') && any(old_dose_val & dose_val))
		figure; plot([old_dose_val,dose_val]);
		keyboard;
	end
	old_dose_val = dose_val;

        % Store bixel influence data in KonradStruct
        [KonradStruct.index{iBixel}, dummy, KonradStruct.value{iBixel}]...
                 = find(dose_val);

        KonradStruct.numVoxels(iBixel) = length(KonradStruct.index{iBixel});

        % Store maxdose
        maxdose = max([maxdose; KonradStruct.value{iBixel}]);

    end

    % Convert stored value to a short integer
    % - Calculate optimal dose coefficient
    KonradStruct.doseCoefficient = maxdose / (2^15 - 1);

    % - Convert dose value to short integer
    for(iBixel = 1:length(KonradStruct.value))
        KonradStruct.value{iBixel} = uint16(KonradStruct.value{iBixel}...
                ./ KonradStruct.doseCoefficient);
    end
    

    % Now write the dij file out for this beam
    try
        waitbar( iBeam / nBeams, h,'Writing dij file'); pause(0);
        writeDij(KonradStruct, ...
                 [outFile '_' num2str(KonradStruct.beamNumber) '.dij']);
    catch
        warning(['Error writing dij file: ' ...
                outFile '_' num2str(KonradStruct.beamNumber) '.dij']);
        flag = 'error';
        return
    end

    % Now write the stf file out for this beam
    try
        waitbar( iBeam / nBeams, h,'Writing stf file'); pause(0);
        writeStf(KonradStruct, ...
                 [outFile '_' num2str(KonradStruct.beamNumber) '.stf']);
    catch
        warning(['Error writing stf file: ', ...
                outFile, '_', num2str(KonradStruct.beamNumber), '.stf']);
        flag = 'error';
        return
    end

    % Now write the bwf file out for this beam
    try
        waitbar( iBeam / nBeams, h,'Writing bwf file'); pause(0);
        write_bwf([outFile '_' num2str(KonradStruct.beamNumber) '.bwf'], ...
            KonradStruct);
    catch
        warning(['Error writing bwf file: ', outFile, '_', ...
                 num2str(KonradStruct.beamNumber) '.bwf']);
        flag = 'error';
        return
    end

end

% Close status bar
close(h);
toc 

flag = 'no error';

return


% --------------------------------------------------------------------------- %
function [table_position] = get_xyz_table(iVoxel)
% Calculate x,y,z position of voxel in table coordinate system
%
% iVoxel can be a scalar or a row-vector (1xn), in which case table_postion
% will be a matrix with each column corresponding to the three-dimensional
% location of a voxel in table-space.
%
% Global variable KonradStruct is required.

    global KonradStruct;

    % Find row, column, and slice number from index
    [rowNo, columnNo, sliceNo] = ind2sub(...
            [KonradStruct.doseCubeNr, KonradStruct.doseCubeNc, KonradStruct.doseCubeNs],iVoxel);

    % Convert to x, y, and z
    x =  columnNo * KonradStruct.voxeldX - KonradStruct.doseIsoX;
    y =  sliceNo * KonradStruct.voxeldY - KonradStruct.doseIsoY;
    z =  rowNo * KonradStruct.voxeldZ - KonradStruct.doseIsoZ;

    % Combine into table_position
    table_position = [x; y; z];

return

% --------------------------------------------------------------------------- %
function [gantry_position] = table_to_gantry(table_position)
% Calculate x,y,z position of voxel in gantry coordinate system
%
% table_position can be a (3x1) column vector or a (3xn) matrix.
% Each column of table_position is the x,y,z position of one point.
% gantry_position has the same size as table_position.
%
% Global variable KonradStruct is required.

    global KonradStruct;

    % Calculate rotation matrix
    cot = cos(KonradStruct.tableAngle * pi / 180);
    sot = sin(KonradStruct.tableAngle * pi / 180);
    cog = cos(KonradStruct.gantryAngle * pi / 180);
    sog = sin(KonradStruct.gantryAngle * pi / 180);
    rotation_matrix = [cog,0,sog;0,1,0;-sog,0,cog] *...
                      [cot,sot,0;-sot,cot,0;0,0,1];

    % Perform rotation
    gantry_position = rotation_matrix * table_position;

return

% --------------------------------------------------------------------------- %
function [conical_position] = gantry_to_cone(gantry_position, beam_SAD)
% Calculate x,y,z position of voxel in beam cone coordinate system
%
% gantry_position can be a (3x1) column vector or a (3xn) matrix.
% Each column of gantry_position is the x,y,z position of one point.
% conical_position has the same size as gantry_position.
%

    % global KonradStruct;

    % Calculate scale factor

    scalefact = beam_SAD ./ (beam_SAD-(gantry_position(3,:)));



    % into cone system (beam divergence)
    % z is the distance from the source

    conical_position(1,:) = gantry_position(1,:) .* scalefact;
    conical_position(2,:) = gantry_position(2,:) .* scalefact;

    % Correct formula for z:
    % z = (SAD-z) * sqrt(1. + ((x)*(x)+(y)*(y))/SAD/SAD);

    % 1st order Taylor series: sqrt(1+eps) approx 1 + eps/2
    conical_position(3,:) = (beam_SAD-gantry_position(3,:))...
        .* (1 + (conical_position(1,:).^2 + conical_position(2,:).^2)...
                 ./ (2 * beam_SAD^2));		

return


% --------------------------------------------------------------------------- %
function [z_rpl] = radioactive_path_length(z, beam_SAD)
% Calculate water-equivalent radioactive path length
%
% z_rpl is the same size matrix as z
%
% Note that this is a dummy implementation

    % Constant
    r_phant=15.;

    % Vectorized version:
    z_rpl = z - beam_SAD + r_phant;
    z_rpl(z > beam_SAD + r_phant) = 2 * r_phant;
    z_rpl(z < beam_SAD - r_phant) = 0;

  % if (z < beam_SAD - r_phant)
  %     z_rpl = 0.;
  % elseif (z > beam_SAD + r_phant)
  %     z_rpl = 2 * r_phant;
  % else 
  %     z_rpl = z-beam_SAD + r_phant;
  % end

return

% --------------------------------------------------------------------------- %
function [dose_val] = pencil_beam_dose(pencil_pos, dx, dy);
% Calculate dose for points within pencil beam
%
% pencil_pos is a 3xN matrix of 3x1 position vectors
%
% dose_val is a Nx1 column vector
%
% Note that this is a dummy implementation

% if ((pencil_pos(1) >= -dx/2) && (pencil_pos(1) < dx/2) &&...
%     (pencil_pos(2) >= -dy/2) && (pencil_pos(2) < dy/2))
%    dose_val = 1;
% else
%    dose_val = 0;
% end

% Vectorized version:
dose_val = zeros(size(pencil_pos,2),1);
dose_val((pencil_pos(1,:) >= -dx/2) & (pencil_pos(1,:) < dx/2) &...
         (pencil_pos(2,:) >= -dy/2) & (pencil_pos(2,:) < dy/2)) = 1;

return
