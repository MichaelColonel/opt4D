function writePln(plan_file_name, plan)
%writePln writes a opt4D plan file.
%   The .pln format is used by opt4D to hold useful information about the
%   plan to be optimized.  It holds general information about the plan
%   as well as the paramaters for optimization.  It is designed as a very
%   flexible format that is human readable.
%
%   writePln(plan_file_name, plan) writes a plan file to the specified file
%   based on the information in the plan structure.
%
%   Example: plan.title = 'Sample Plan';
%            plan.description = 'A sample plan with two vois';
%            plan.file_root = '/home/user/data/konrad_files/sample_plan';
%            plan.nInstances = 1;
%            plan.nBeams = 7;
%            plan.vois(1).name = 'tumor';
%            plan.vois(1).min_DVH = '72 95%';
%            plan.vois(1).weight_over = 1;
%            plan.vois(1).max_DVH = '72 5%';
%            plan.vois(1).weight_under = 50;
%            plan.vois(2).name = 'Spinal Cord';
%            plan.vois(2).max_DVH = '20 0%';
%            plan.vois(2).weight_over = 1;
%            writePln('sample_plan.pln', plan);
%
%    Other possible properties include
%            plan.vois(1).color = '1 0.5 0'; 
%            plan.vois(1).EUD_p = -5;
%            plan.vois(2).EUD_p = 5;
%
% plan is a struct with the following fields:
%   plan.title       - Plan title
%   plan.description - Plan description
%   plan.file_root   - Root of Konrad formatted files
%   plan.nInstances  - Number of instances of geometry (a.k.a. time slices)
%   plan.nBeams      - Number of beams
%   plan.params      - Cell array of strings of any other plan paramaters
%   plan.values      - Cell array of strings of values for other paramaters
%   plan.vois        - 

% First open the file for writing
fid = fopen(plan_file_name,'wt');

% First make all numerical arguments into strings
for(param = fields(plan)')
    if(isa(plan.(param{:}),'double'))
        plan.(param{:}) = num2str(plan.(param{:}));
    end
end

if(isfield(plan, 'title'))
    fprintf(fid, 'plan_title %s\n', plan.title);
end

if(isfield(plan, 'description'))
    fprintf(fid, 'plan_description %s\n', plan.description);
end

if(isfield(plan, 'file_root'))
    fprintf(fid, 'plan_file_root %s\n', plan.file_root);
end


if(isfield(plan, 'nInstances'))
    fprintf(fid, 'nInstances %s\n', plan.nInstances);
end

if(isfield(plan, 'nBeams'))
    fprintf(fid, 'nBeams %s\n', plan.nBeams);
end

% Write out any other paramaters
if(isfield(plan, 'params'))
    for(iParam = 1:length(plan.params))
        fprintf(fid, '%s %s\n', plan.params{iParam}, plan.values{iParam});
    end
end

% Write VOI paramaters
if(isfield(plan, 'vois'))
    for(iVoi = 1:length(plan.vois))

        % Write out all paramaters for VOI
        for param = fieldnames(plan.vois)'

            % Convert param from a cell to a string
            param = param{:};
            
            if(~isempty(plan.vois(iVoi).(param)))

                % Make sure paramaters is a string
                if(isa(plan.vois(iVoi).(param),'double'))
                    plan.vois(iVoi).(param) = num2str(plan.vois(iVoi).(param));
                end
                fprintf(fid, 'VOI %d %s %s\n', iVoi, param,...
                        plan.vois(iVoi).(param));
            end
        end
        
    end
end

fclose(fid);
