function plot_trans_slice(plot_param, dose_file_path, dose_file_root, pln_file_name, slice_dose)

% print dose distributions for one slice in the dose cube with contours
% from the vdx file

% parameters:

% plot_param: plot parameters set in file read_plot_parameters.m

% dose_file_path: string containing the path to the directory where dose
% results are stored. Name of dose file must be "dose.dat"

% dose_file_root: string containing the dose file name without .dat extension

% pln_file_name: string containing the plan file name and path

% slice_dose: integer for the dose cube slice

if(~exist('plot_param') || ~isstruct(plot_param))
    error('need plot parameters as argument');
end

if(~exist('dose_file_path') || ~isstr(dose_file_path))
    error('no directory path to dose files');
end

if(~exist('dose_file_root') || ~isstr(dose_file_root))
    error('no dose file given');
end

if(~exist('pln_file_name') || ~isstr(pln_file_name))
    error('no plan file given');
end

if(~exist('slice_dose') )
    error('no slice number in dose cube given');
end


% create dose file name
dose = [dose_file_path '/' dose_file_root '.dat'];

% read plan file
plan = read_pln(pln_file_name);

% Get dimension information
if(exist(plan.dif_file_name))
    [dif] = read_dif(plan.dif_file_name);
else
    error('no dif file name');
end


% get dose cube information
nRows = dif.nZ;
nSlices = dif.nY;
nColumns = dif.nX;
dX = dif.dX; dY = dif.dY; dZ = dif.dZ;

% read dose
[Dose] = read_dose(dif, dose);

% check
if(nSlices ~= size(Dose, 3) || nRows ~= size(Dose, 1) || nColumns ~= size(Dose, 2))

    error('size of dose cube does not match .dif file');

end

% create absolute coordinate vextors
x_coord = linspace( -dif.isoX*dif.dX, (dif.nX-1-dif.isoX)*dif.dX, dif.nX );
y_coord = linspace( -dif.isoY*dif.dY, (dif.nY-1-dif.isoY)*dif.dY, dif.nY );
z_coord = linspace( -dif.isoZ*dif.dZ, (dif.nZ-1-dif.isoZ)*dif.dZ, dif.nZ );


% normalize dose
if(plot_param.Norm~=0)
    Scaled_Dose = Dose / plot_param.Norm;
else
    max_dose = max(max(Dose(:,:,slice_dose)))
    Scaled_Dose = Dose / max_dose;
end


% calculate which VDX contour slice corresponds to the dose cube slice
slice_dose_z_coord = (slice_dose-dif.isoY)*dif.dY;   % z-coord in the dose cube coordinate system
slice_vdx = int16( (plot_param.CT_isocenter_Z + slice_dose_z_coord) / plot_param.CT_slice_thickness ) 
% note that the slice number in the dose cube corresponds to the Y axis in
% the KonRad coordinate system, but the Z coordinate in the Plan file

% open figure
figure;

% plot dose data
%imagesc(x_coord,z_coord,Scaled_Dose(:,:,slice_dose));set(gca,'YDir','normal');
contourf(x_coord,z_coord,Scaled_Dose(:,:,slice_dose),plot_param.IsoDoseLevels);
%surface(x_coord,z_coord,Scaled_Dose(:,:,slice_dose),'EdgeColor','none');

hold on;

% read the voi contours

% create cell array for the contour arrays
nVois = length(plot_param.VoisToRead);

for iVoi=1:nVois
    
    % read vdx
    [contours,oarname,posout] = read_vdx_multi(plot_param.vdx_file_name,plot_param.VoisToRead(iVoi),slice_vdx);
    nConts = max(size(contours));
    
    for iCont=1:nConts
    
        % voi might not exist in this slice
        if size(contours{iCont}) ~= [0,0]

            % correct for factor 16 and CT voxel size
            contours{iCont}(:,1) = (contours{iCont}(:,1) / 16.0) * plot_param.CT_voxel_size;
            contours{iCont}(:,2) = (plot_param.CT_size-contours{iCont}(:,2) / 16.0) * plot_param.CT_voxel_size;  % note that CT Y-axis is mirrored

            % correct for isocenter shift
            contours{iCont}(:,1) = contours{iCont}(:,1) - plot_param.CT_isocenter_X;  % this is just the first value from the KonRad plan file
            contours{iCont}(:,2) = contours{iCont}(:,2) - (plot_param.CT_size*plot_param.CT_voxel_size - plot_param.CT_isocenter_Y);  % note that CT Y-axis is mirrored

            % plot
            plot(contours{iCont}(:,1),contours{iCont}(:,2),'Color','k','LineWidth',3);
            plot(contours{iCont}(:,1),contours{iCont}(:,2),'Color','w','LineWidth',1);
        end

    end

end

% set properties
%title([dose_file_path '/' dose_file_root '_s' int2str(slice_dose)],'Interpreter','none');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
caxis([plot_param.MinDose plot_param.MaxDose]);
colormap(jet);
axis([plot_param.axisLeft plot_param.axisRight plot_param.axisBottom plot_param.axisTop]);
set(gca,'DataAspectRatio',[1 1 1]);
drawnow;
%colorbar;

% build file name
FileName = [dose_file_path '/' dose_file_root '_s' int2str(slice_dose)];
set(gcf, 'PaperUnits', 'centimeter');
set(gcf, 'PaperPosition', [0 0 plot_param.plot_width plot_param.plot_width]);
print ( '-depsc',FileName );
print ( '-djpeg',FileName );

return
