function [hed] = read_hed(hed_file)
%read_hed Read ct header file (*.HED or *.head).
%   Reads the header file into a structure:
%
%     hed.version
%     hed.modality
%     hed.primary_view
%     hed.data_type
%     hed.num_bytes
%     hed.byte_order
%     hed.patient_name
%     hed.slice_dimension
%     hed.pixel_size
%     hed.slice_distance
%     hed.slice_number
%     hed.xoffset
%     hed.dimx
%     hed.yoffset
%     hed.dimy
%     hed.zoffset
%     hed.dimz
%
%   See also read_cti.

% Read the header file into memory
try
    [command, value] = textread(hed_file, '%s %s', 'commentstyle', 'shell');
catch
    error(['Error reading ct headder file: ' hed_file ' (' lasterr ')']);
end


% Initialize the hed structure
hed = struct( ...
    'version',        '', ...
    'modality',       '', ...
    'primary_view',   '', ...
    'data_type',      '', ...
    'num_bytes',       0, ...
    'byte_order',     '', ...
    'patient_name',   '', ...
    'slice_dimension', 0, ...
    'pixel_size',      0, ...
    'slice_distance',  0, ...
    'slice_number',    0, ...
    'xoffset',         0, ...
    'dimx',            0, ...
    'yoffset',         0, ...
    'dimy',            0, ...
    'zoffset',         0, ...
    'dimz',            0);


% Read the file into the structure
for(iCommand = 1:length(command))

    % Replace dashes with underscores if neccessary
    temp_command = strrep(command{iCommand},'-','_');

    if(isfield(hed, temp_command))
	if(isnumeric(hed.(temp_command)))
	    hed.(temp_command) = str2num(value{iCommand});
	else
	    hed.(temp_command) = value(iCommand);
	end
    end
end

return
