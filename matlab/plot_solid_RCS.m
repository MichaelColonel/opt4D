function plot_solid_RCS( rcs_data, row, column, slice, cmap, dx, dy, dz)
%plot_solid_RCS   Plot data in RCS format as a solid block.
%    Plot data thet represents the value at various points in a 3D solid.
%
%    plot_solid_RCS(rcs_data, rows, columns, slices, cmap, dx, dy, dz) uses
%    the colormap cmap and voxel size (dx, dy, dz) to show data covering the
%    range specified in rows, columns, and slices.
%
%    plot_solid_RCS(rcs_data, rows, columns, slices, cmap) assumes voxels are
%    cubes.
%
%    plot_solid_RCS(rcs_data, [], [], [], cmap) shows the entire solid.
%
%    plot_solid_RCS(rcs_data) uses a grayscale color map.

[nR, nC, nS] = size(rcs_data);

% Use grayscale colormap if needed
if(~exist('cmap') || isempty(cmap))
    cmap = gray(256);
end

if(nargin < 8)
    dx = 1; dy = 1; dz = 1;
end

if(isempty(row))
    row = nR;
end
if(isempty(column))
    column = nC;
end
if(isempty(slice))
    slice = 1;
end

% Range of data to show
rows = 1:row;
columns = 1:column;
slices = slice:nS;

clf;


% Draw Axial face
temp_axial = rcs_data(rows,columns,slices(1));
x=[1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
y=slices(1) * ones(2,3);
z = [rows(1);rows(end)] * [1 1 1];
axial_handle = warp(x,y,z,temp_axial,cmap);
hold on;

% Draw Coronal face
temp_coronal = rcs_data(rows(end),columns,slices);	
temp_coronal = squeeze(permute(temp_coronal,[3,1,2]));
x=[1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
y= [slices(1);slices(end)] * [1 1 1];
z = rows(end) * ones(2,3);
coronal_handle = warp(x,y,z,temp_coronal,cmap);

% Draw Sagital face
temp_sagital = rcs_data(rows(end):-1:rows(1),columns(end),slices);	
temp_sagital = squeeze(permute(temp_sagital,[3,2,1]));
x = columns(end) * ones(2,3);
y = [slices(1);slices(end)] * [1 1 1];
z = [1;1]*[rows(end) (rows(end)+rows(1))/2  rows(1)];
sagital_handle = warp(x,y,z,temp_sagital,cmap);

% Draw outline of dose cube
x = [1,1,    nC,    nC,1,1,    nC,    nC,    nC,    nC,    nC;
     1,1,column,column,1,1,column,column,column,column,column]';
y = [1,nS,nS,1,1,1,1,1,nS,nS,1;
     slice,nS,nS,slice,slice,slice,slice,slice,nS,nS,slice]';
z = [nR,nR,nR,nR,nR,1,1,nR,nR,1,1;
     row,row,row,row,row,1,1,row,row,1,1]';
l = line(x,y,z);
set(l(1),'LineStyle',':');
set(l(2),'Color',[0 1 0]);
set(gca,'color','none');

% Add labels
xlabel('columns');
ylabel('slices');
zlabel('rows');

% Set view
default_view = [  4.4029    0.3061         0   -2.3545
                 -0.7537    0.5068    3.9152   -1.8341
                 -1.1986    0.8059   -2.4620   34.5111
                  0         0         0         1.0000];
view(default_view); set(gca,'ydir','normal');
axis tight
set(gca,'DataAspectRatio',[1/dx 1/dy 1/dz]);

hold off;

return
