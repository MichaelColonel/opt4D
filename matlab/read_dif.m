function [dif] = read_dif(dif_file)
%read_dif Read Konrad dimention information file.
%   Reads the dif file into a structure.  
%
%   The dif format is based on the Konrad geometry, so x, y, and z correspond
%   to column, slice, and row in the CERR geometry, respectively.
%
%     .dif file        dif structure
%
%   Delta-X ----------- dif.dX
%   Delta-Y ----------- dif.dY
%   Delta-Z ----------- dif.dZ
%   Dimension-CT-X ---- dif.ct_nX
%   Dimension-CT-Y ---- dif.ct_nY
%   Dimension-CT-Z ---- dif.ct_nZ
%   Dimension-Dose-X -- dif.nX
%   Dimension-Dose-Y -- dif.nY
%   Dimension-Dose-Z -- dif.nZ
%   ISO-Index-CT-X ---- dif.ct_isoX
%   ISO-Index-CT-Y ---- dif.ct_isoY
%   ISO-Index-CT-Z ---- dif.ct_isoZ
%   ISO-Index-Dose-X -- dif.isoX
%   ISO-Index-Dose-Y -- dif.isoY
%   ISO-Index-Dose-Z -- dif.isoZ


% Read the plan file into memory
try
    [command, value] = textread(dif_file, '%s %n', 'commentstyle', 'shell');
catch
    error(['Error reading dif file: ' dif_file ' (' lasterr ')']);
end

% Initialize the dif structure
dif = struct;

% Read the dose cube voxel size
dif.dX = 0;
for i = find(strcmp(command, 'Delta-X'))'
    [dif.dX] = value(i);
end
dif.dY = 0;
for i = find(strcmp(command, 'Delta-Y'))'
    [dif.dY] = value(i);
end
dif.dZ = 0;
for i = find(strcmp(command, 'Delta-Z'))'
    [dif.dZ] = value(i);
end

% Read the ct cube size
dif.nX = 0;
for i = find(strcmp(command, 'Dimension-CT-X'))'
    [dif.ct_nX] = value(i);
end
dif.nY = 0;
for i = find(strcmp(command, 'Dimension-CT-Y'))'
    [dif.ct_nY] = value(i);
end
dif.nZ = 0;
for i = find(strcmp(command, 'Dimension-CT-Z'))'
    [dif.ct_nZ] = value(i);
end

% Read the dose cube size
dif.nX = 0;
for i = find(strcmp(command, 'Dimension-Dose-X'))'
    [dif.nX] = value(i);
end
dif.nY = 0;
for i = find(strcmp(command, 'Dimension-Dose-Y'))'
    [dif.nY] = value(i);
end
dif.nZ = 0;
for i = find(strcmp(command, 'Dimension-Dose-Z'))'
    [dif.nZ] = value(i);
end

% Read the CT iso-center
dif.isoX = 0;
for i = find(strcmp(command, 'ISO-Index-CT-X'))'
    [dif.ct_isoX] = value(i);
end
dif.isoY = 0;
for i = find(strcmp(command, 'ISO-Index-CT-Y'))'
    [dif.ct_isoY] = value(i);
end
dif.isoZ = 0;
for i = find(strcmp(command, 'ISO-Index-CT-Z'))'
    [dif.ct_isoZ] = value(i);
end

% Read the dose cube iso-center
dif.isoX = 0;
for i = find(strcmp(command, 'ISO-Index-Dose-X'))'
    [dif.isoX] = value(i);
end
dif.isoY = 0;
for i = find(strcmp(command, 'ISO-Index-Dose-Y'))'
    [dif.isoY] = value(i);
end
dif.isoZ = 0;
for i = find(strcmp(command, 'ISO-Index-Dose-Z'))'
    [dif.isoZ] = value(i);
end
