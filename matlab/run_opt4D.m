function run_opt4D(plan_file, results_prefix, options, show_log, append_log)
% run_opt4D(plan_file, results_prefix, options, show_log, append_log)
% 

% Define constants
opt4D = 'opt4D';

if(~exist('options'))
    options = '';
end

if(~exist('show_log'))
    show_log = true;
end

if(~exist('append_log'))
    append_log = false;
end

if(~iscell(options))
    options = {options};
end

if(~iscell(results_prefix))
    results_prefix = {results_prefix};
end

% Read plan file
Plan = read_pln(plan_file);


% Run optimization
for(i = 1:length(results_prefix))

    command = ['!time ' opt4D ' --out-file-prefix ' ...
        results_prefix{i} ' ' options{i} ' ' plan_file];

    if(append_log)
        command = [command ' >> ' results_prefix{i} '.log' ' &'];
    else
        command = [command ' > ' results_prefix{i} '.log' ' &'];
    end
    command
    eval(command);

    % Wait for optimization to finish
    pause(1);
    count = 1;
    while(~isempty(evalc('!ps -a | grep opt4D')))
	count=count+1;
        if(mod(count,30) == 0)
            disp(sprintf('%d seconds', count))
        end
        pause(1);
    end

    if(show_log)
        % Open a window with the log file
        eval(['!xterm -e less ', results_prefix{i}, '.log &']);
    end
end

