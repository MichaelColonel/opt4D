function [dose] = pencil_beam_dose(dx, dy, dose_type, pencil_pos);
%pencil_beam_dose Calculate dose for points within pencil beam
%   [dose] = pencil_beam_dose(beam_dx, beam_dy, dose_type, position) returns
%   the dose to each position in the pencil beam.
%
%   position is a 3xN matrix of 3x1 position vectors.
%
%   dose is a sparse Nx1 column vector.
%
%   dose_type can be 'ideal rectangular' or 'tripple gaussian'

switch(dose_type)
case 'ideal rectangular'

    % Vectorized version:
    dose = zeros(size(pencil_pos,2),1);
    dose((pencil_pos(1,:) >= -dx/2) & (pencil_pos(1,:) < dx/2) &...
             (pencil_pos(2,:) >= -dy/2) & (pencil_pos(2,:) < dy/2)) = 1;
    dose = sparse(dose);

case 'tripple gaussian'
    % Based on 
    c = [.534; 0.264; 0.202];
    sigma = [0.052064; 0.974316; 0.113541];

    x = pencil_pos(1,:)';
    y = pencil_pos(2,:)';
    
    dose = zeros(size(x));
    for(i = 1:length(c))
        dose = dose + c(i) * (erf((x+dx/2)/sigma(i))-erf((x-dx/2)/sigma(i))) ...
                          .* (erf((y+dy/2)/sigma(i))-erf((y-dy/2)/sigma(i)));
    end

    [I] = find(dose > 0.001);
    dose = sparse(I,1,dose(I),length(dose),1);
otherwise
    error('Unknown Dose Type');
end


return
