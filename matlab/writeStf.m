function writeStf(DijStruct, outFile)
% writeStf(DijStruct, outfile)
%
% Write the beam steering file based on information in DijStruct.
%
% outFile is the output file.
%
% Throws an error if the file writing fails

% Here's the stf file format:
% 
% The stf file is a human readible file that gives beam steering information
% about each beamlet within a beam.  It starts with a header that gives the
% machine name, gantry angle, table angle, collimator angle (in degrees) and
% bixel size (in mm).  This is followed by a table header and a table of values.
%
% The table columns are:
% DB		beamlet number
% Energy	beamlet energy
% ???		???
% ???		???
% ???		???
% ray-x	y	Position of beamlet relative to beam isocenter (in mm)
%
% Until we have better information about what all the columns mean, default
% values have been used for some values.

% Open file for writing (unix style carriage returns)
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
	error(['Error writing stf file: ' message]);
end


% Print header information
fprintf(outfid,'%s\n','# Header information');
fprintf(outfid,'%s %s\n','# Machinename:', DijStruct.machineName);
fprintf(outfid,'%s %8.6f\n','# Gantry-Angle:', DijStruct.gantryAngle);
fprintf(outfid,'%s %8.6f\n','# Table-Angle:', DijStruct.tableAngle);
fprintf(outfid,'%s %8.6f\n','# Colli-Angle:', DijStruct.collimatorAngle);
fprintf(outfid,'%s %8.6f %s %8.6f','# Delta X:', DijStruct.spotdx * 10,...
				  'Delta Y:', DijStruct.spotdy * 10);

% Print information about each beam
for count = 1:DijStruct.numPencilBeams
	fprintf(outfid,'\n%d %8.6f %8.6f %s %8.6f %8.6f %8.6f', ...
		count-1, DijStruct.energy(count), 0., 'R', 0.,...
   		DijStruct.spotX(count) * 10, DijStruct.spotY(count) * 10);
end    

% Close file
status = fclose(outfid);
if status ~= 0
	error(['Error writing stf file']);
end
