function [conical_position] = gantry_to_cone(beam_SAD, gantry_position)
%gantry_to_cone Convert from gantry coordiantes to cone coordinates
%   [conical_position] = gantry_to_cone(beam_SAD, gantry_position) Calculates
%   x,y,z position of voxel in beam cone coordinate system based on the beam
%   source to axis distance.  gantry_position can be a (3x1) column vector or
%   a (3xn) matrix.  Each column of gantry_position is the x,y,z position of
%   one point.  conical_position has the same size as gantry_position.
%
%   See also table_to_gantry, radioactive_path_length.

    % Calculate scale factor
    scalefact = beam_SAD ./ (beam_SAD-(gantry_position(3,:)));

    % into cone system (beam divergence)
    % z is the distance from the source
    conical_position(1,:) = gantry_position(1,:) .* scalefact;
    conical_position(2,:) = gantry_position(2,:) .* scalefact;

    % Correct formula for z:
    % z = (SAD-z) * sqrt(1. + ((x)*(x)+(y)*(y))/SAD/SAD);

    % 1st order Taylor series: sqrt(1+eps) approx 1 + eps/2
    conical_position(3,:) = (beam_SAD-gantry_position(3,:))...
        .* (1 + (conical_position(1,:).^2 + conical_position(2,:).^2)...
                 ./ (2 * beam_SAD^2));		

return
