function flag = write_1D_phantom(file_root, nBixels, nInstances)
%write_1D_phantom Creates Konrad files for a 1-dimensional phantom
%
%    write_1D_phantom(file_root, nBeamlets, nInstances) creates
%    Creates .dij, .dif, .voi 
%
%    The is a sigle beam only.
%    The program generates Dij's for multiple instances 
%    The .pdf file is ascii and contains the probability for each instance
%
%     For example,
%
%         write_1D_phantom('default')
%
%     creates files named default.voi, default.dif, default.pdf, default_1_1.dij,
%     default_2_1.dij, etc.
%
%     Notes:
%     * There are many non-essential values that were given arbitrary defaults.
%       This shouldn't be a problem for optimization, but better values are
%       obviously prefered.
%


% ----------------------------------------------------------------- %
% Make sure the inputs to the function are correct.

% Both the output file prefix and the number of beams are required
if(~exist('file_root'))
    error('The output file prefix is required');
end

% ---------------------------

% Define geometry (all in mm)

beam_nX=nBixels;
beam_nY=1;

% voxel size
dif.dX = 1;
dif.dY = 1;
dif.dZ = 1;

% dose cube size
dif.nX = nBixels+nInstances;
dif.nY = 1;
dif.nZ = 1;

% Define index of isocenter
dif.isoX = 1;
dif.isoY = 1;
dif.isoZ = 1;

% Define RCS coordinates for later convenience
nR = dif.nZ; isoR = dif.isoZ;
nC = dif.nX; isoC = dif.isoX;
nS = dif.nY; isoS = dif.isoY;

nVoxels = nR * nC * nS;

% Define other dif values
dif.ct_nX = dif.nX;
dif.ct_nY = dif.nY;
dif.ct_nZ = dif.nZ;
dif.ct_isoX = dif.isoX;
dif.ct_isoY = dif.isoY;
dif.ct_isoZ = dif.isoZ;

% Write dif file
write_dif([file_root '.dif'], dif);


% ------------------------------------------------------------------- %
% Create volume of interest

% Create empty VOI cube
voiField = ones(nR,nC,nS);

% create target
for i=1:nVoxels
    if i<dif.nX*0.6 && i>dif.nX*0.4
        voiField(i) = 2;
    end
end

% Write voi file
write_voi([file_root '.voi'], voiField);


% ------------------------------------------------------------------- %
% Calculate beamlet influence matrix.  
Dij = spalloc(nBixels, nVoxels, nBixels);

for iInstance=1:nInstances

    % set Dij values
    for iBixel=1:nBixels
        Dij(iBixel,iInstance-1+iBixel) = 1;
    end

    % transpose
    Dij = Dij';

    % Now write the dij file out for this beam
    DijStruct.machineName     = 'oneDmodel';
    DijStruct.beamNumber      = 1;
    DijStruct.gantryAngle     = 0;
    DijStruct.tableAngle      = 0;
    DijStruct.collimatorAngle = 0;
    DijStruct.spot_dx         = dif.dX/10.;
    DijStruct.spot_dy         = dif.dY/10.;
    DijStruct.voxel_dX        = dif.dX/10.;
    DijStruct.voxel_dY        = dif.dY/10.;
    DijStruct.voxel_dZ        = dif.dZ/10.;
    DijStruct.doseCubeNc      = dif.nX;
    DijStruct.doseCubeNs      = dif.nZ;
    DijStruct.doseCubeNr      = dif.nY;
    DijStruct.numPencilBeams  = nBixels;
    DijStruct.energy          = ones(nBixels,1);
    DijStruct.spotX           = linspace(0.5*dif.dX,(beam_nX-0.5)*dif.dX,beam_nX);
    DijStruct.spotY           = dif.dX/2.0 * ones(nBixels,1);
    DijStruct.Dij             = Dij;
    DijStruct.beamWeight      = zeros(nBixels,1);

    write_dij([file_root '_' num2str(iInstance) '_1' '.dij'], DijStruct);

    clear Dij;

end

