function [obj_hist, time_hist, false_obj_hist, multi_obj_hist, ...
false_multi_obj_hist, objective_names]...
= read_opt4D_convergence(out_file_prefix)
%plot_opt4D_convergence(out_file_prefix)
%  The out_file_prefix can be a cell array of strings
%
% [obj_hist, time_hist, false_obj_hist, multi_obj_hist, ...
% false_multi_obj_hist, objective_names] = read_opt4D_convergence(...
% out_file_prefix)
%
% See plot_opt4D_convergence.


max_steps = 1;

if(~iscell(out_file_prefix))
    out_file_prefix = {out_file_prefix};
end
    
obj_hist = repmat([inf], max_steps, numel(out_file_prefix));
false_obj_hist = repmat([inf], max_steps, numel(out_file_prefix));
time_hist = repmat([inf], max_steps, numel(out_file_prefix));

multi_obj_hist = {};
false_multi_obj_hist = {};
objective_names = {};

for(iRun = 1:numel(out_file_prefix))
    temp_prefix = out_file_prefix{iRun};

    % Read objective history
    if(exist([temp_prefix 'obj_history.dat'],'file'))
        temp_obj_hist = textread([temp_prefix 'obj_history.dat']) * [0;1];
        if(length(temp_obj_hist) > max_steps)
	    obj_hist = [obj_hist; ...
                repmat(inf, length(temp_obj_hist) - max_steps, ...
                            numel(out_file_prefix))];
	    false_obj_hist = [false_obj_hist; ...
                repmat(inf, length(temp_obj_hist) - max_steps, ...
                            numel(out_file_prefix))];
            time_hist = [time_hist; ...
                repmat(inf, length(temp_obj_hist) - max_steps, ...
                            numel(out_file_prefix))];
            max_steps = size(obj_hist,1);
        end


        % Read true objective history if present
	if(exist([temp_prefix 'true_obj_history.dat'],'file'))
	    false_obj_hist(1:length(temp_obj_hist), iRun) = temp_obj_hist;
	    
	    temp_obj_hist = textread([temp_prefix ...
	    'true_obj_history.dat'])*[0;1];
	    % disp('have true history');
	end

        obj_hist(1:length(temp_obj_hist), iRun) = temp_obj_hist;



        % Read multi objective history if present
        file_name = [temp_prefix 'multi_obj_history.dat'];
        if(exist(file_name,'file'))
            [objective_names{iRun}, multi_obj_hist{iRun}] = ...
            read_multi_obj_history(file_name);
        end

        % Read true multi objective history if present
        file_name = [temp_prefix 'true_multi_obj_history.dat'];
        if(exist(file_name,'file'))
            false_multi_obj_hist{iRun} = multi_obj_hist{iRun};
            [dummy, multi_obj_hist{iRun}] = ...
            read_multi_obj_history(file_name);
        end

        % Read time history
        if(exist([temp_prefix 'time_history.dat'],'file'))
            temp_time_hist = textread([temp_prefix 'time_history.dat'])*[0;1];
	    time_hist(1:length(temp_time_hist), iRun) = temp_time_hist;
        end
    end
end

% ---------------------------------------------------------%
function [objective_descriptions, multi_obj_hist] = ...
read_multi_obj_history(file_name)
%

[fid, message] = fopen(file_name);
if(fid == -1)
    error(['Error reading: "' file_name '": ' message]);
end

% Throw out first line
dummy = fgetl(fid);

% Read objective descriptions
objective_descriptions = {};
line = fgetl(fid);
while(~strcmp(lower(line),'multi-objective history:'))
    objective_descriptions{end+1} = line;
    line = fgetl(fid);
end

multi_obj_hist = reshape(fscanf(fid,'%lf'),numel(objective_descriptions),[])';

