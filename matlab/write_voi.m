function write_voi( voiFile, voiField)
%write_voi Write a Konrad .voi file
%
%    writeVoi( voiFile, voiField) writes a file based on a voiField defined as
%    a 3-D array of arranged in Row, Column, Slice geometry.  The volume of
%    interest for each voxel is indicated with a number from 0 to 127, with 0
%    signifying an air voxel.

  % Convert voi numbers back to original format
  voiField(voiField == 0) = 128;
  voiField = double(voiField) - 1;
  
  % Rearrange to originial order
  voiField = ipermute(voiField, [3 1 2]);
  
  % Open output file
  [fid,message]=fopen(voiFile,'w');
  if(~isempty(message))
      error(['Error writing voi file: ' voiFile ' (' message ')']);
  end
  
  % Write Voi file
  fwrite(fid, voiField, 'uint8');
  
return

