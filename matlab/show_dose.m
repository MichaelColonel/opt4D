function show_dose(file_name, delay, nRows, nColumns)
%show_dose   Shows an animation of a dose ouput by opt4D.
% show_dose(file_name, delay, nRows, nColumns)


% Call up a dialog to choose a file if it is not included already.
if(~exist('file_name'))
    [file_name, path_name, filter_index] = uigetfile({'*.dat', 'Dose file (*.dat)'}, 'Load dose file');
    if(filter_index == 0)
        disp( 'User pressed cancel.');
        return;
    else
        file_name = [path_name, file_name];
    end
end

if(~exist('nRows'))
    nRows = input('Rows of dose cube? ');
end

if(~exist('nColumns'))
    nColumns = input('Columns of dose cube? ');
end

if(~exist('delay'))
    delay = .1;
end

disp(['Reading dose from ' file_name]);

fid = fopen(file_name);
[Dose, count] = fread(fid, 'float');
fclose(fid);


% Convert dose to a RCS matrix
Dose = permute(reshape(Dose, nColumns, [], nRows), [3 1 2]);

nSlices = size(Dose, 3);

[overallMax, I] = max(Dose(:));
[maxRow, maxColumn, maxSlice] = ind2sub(size(Dose), I);

Dose = uint8(Dose * (255 / overallMax));
cmap = gray(256);


for iSlice = nSlices:-1:1
    plot_solid_RCS(Dose, [], [], iSlice, cmap);
    pause(delay);
end

return
