function [DijStruct] = read_dij(infile)
%read_dij Reads a Konrad .dij file into a data structure
%   [DijStruct] = read_dij(infile) Reads influence matrix in Konrad bixel
%   format from infile.
%
%   DijStruct is a structure with the following fields:
%
%       gantryAngle: in degrees
%        tableAngle: in degrees
%   collimatorAngle: in degrees
%           spot_dx: width of beamlet (cm)
%           spot_dy: height of beamlet (cm)
%          voxel_dX: column width (cm)
%          voxel_dY: slice thickness (cm)
%          voxel_dZ: row height (cm)
%        doseCubeNc: number of columns
%        doseCubeNs: number of slices
%        doseCubeNr: number of rows
%    numPencilBeams: number of pencil beams in beam
%   doseCoefficient: multiply every number in the value cell by this
%            energy: array of beam energy used in each beamlet
%             spotX: beamlet location for each beamlet (cm)
%             spotY: beamlet location for each beamlet (cm)
%         numVoxels: number of voxels influenced by each beam
%             index: cell array with index of each influenced voxel
%             value: cell array with scaled value of each influenced voxel
%
%   See also write_dij.

if(~exist('infile'))
    error('Incorrect inputs');
end

[fid, message] = fopen([infile],'r');
if(~isempty(message))
    error(['Error reading dij file: ' infile ' (' message ')']);
end

% Here's the Dij file format:
% it'a a binary file with no "separators" between the entries
% 
% File header:   
% 
% float    a_g, a_t, a_c  ------ Gantry, table and collimator angles
%   
% float    dx_b, dy_b  --- bixel dimensions (in mm)
%   
% float    dx, dy, dz  ---- voxel dimensions (in mm)
% 
% int       Nx, Ny, Nz  ---- dose cube dimensions (number of voxels)
% 
% int       Npb     -------- number of pencil beams (bixels) used for this field
% 
% float	Dose-scalefactor   ---- since the doses are stored as short
%                               integers, this is the conversion factor to the
%		absolute dose ---- for the best resolution,	this will be:
%			max(Dij_entry)/max(short) = max(Dij_entry)/(2^15-1)
% 
%
% This is followed by "Npb" series of entries for individual beam elements
% 
%   
% Beam header:  
% 
% float     Energy
%
% float     spot_x, spot_y ------ position of the bixel with respect to the
%					central axis of the field (in mm)
% int     Nvox   ---- number of voxels with non-zero dose from this beamlet
%   
% Now for each Nvox:
%   
% int     VoxelNumber     ---- voxel ID in the cube (between 1 and Nx*Ny*Nz)
%				this can be ?
% 
% short    Value  ----- multiply this by Dose-scalefactor to get the dose
%			deposited by this beamlet to the voxel VoxelNumber,
%			assuming the beamlet weight of 1
% 
%   
% Now same for the next beamlet etc.
% 
% Of course, the index VoxelNumber is related to the voxel position in the cube.
%   
% KonRad uses a right-handed Cartesian coordinate system for the dose cube:
% 
% for gantry at 0, in beam's eye view, x is pointing to the right, y == up,
% and z == towards the gantry.
% 
% So , the origin of the cube is in the distal lower left corner.
% 
% For a voxel at (x,y,z) w/ respect to the cube origin,  the VoxelNumber =
%					x/dx + ( y/dy + z/dz*Ny )*Nx 
% 

% Read header from file (Distances need to be converted from mm to cm):
tmp = fread(fid,8,'float32');
DijStruct.gantryAngle     = tmp(1);
DijStruct.tableAngle      = tmp(2);
DijStruct.collimatorAngle = tmp(3);
DijStruct.spot_dx         = tmp(4) / 10;
DijStruct.spot_dy         = tmp(5) / 10;
DijStruct.voxel_dX        = tmp(6) / 10;
DijStruct.voxel_dY        = tmp(7) / 10;
DijStruct.voxel_dZ        = tmp(8) / 10;

tmp = fread(fid,4,'int');
DijStruct.doseCubeNc     = tmp(1);
DijStruct.doseCubeNs     = tmp(2);
DijStruct.doseCubeNr     = tmp(3);
DijStruct.numPencilBeams = tmp(4);

tmp = fread(fid,1,'float32');
DijStruct.doseCoefficient = tmp(1);

% Read information for each beamlet
DijStruct.energy = zeros(DijStruct.numPencilBeams,1);
DijStruct.spotX = zeros(DijStruct.numPencilBeams,1);
DijStruct.spotY = zeros(DijStruct.numPencilBeams,1);
DijStruct.numVoxels = zeros(DijStruct.numPencilBeams,1);
DijStruct.index = cell(DijStruct.numPencilBeams,1);
DijStruct.value = cell(DijStruct.numPencilBeams,1);

for count=1:DijStruct.numPencilBeams;
    % Beam header
    tmp = fread(fid,3,'float32');
    DijStruct.energy(count) = tmp(1);
    DijStruct.spotX(count)  = tmp(2) / 10;
    DijStruct.spotY(count)  = tmp(3) / 10;

    tmp = fread(fid,1,'int');
    DijStruct.numVoxels(count) = tmp;
    
    % Read voxel data if there are any voxels
    if(DijStruct.numVoxels(count) > 0)

	% Read beamlet data in raw form
	tmp = fread(fid,[3 DijStruct.numVoxels(count)],'ushort');
	
	% Rearange into indexes and values
	indexV =  tmp(2,:)'.*2^16 + tmp(1,:)' + 1;

	% Convert Konrad indexes to CERR indexing
	% First read coordinates in CERR system from Konrad index values
	[tempC,tempS,tempR] = ind2sub([DijStruct.doseCubeNc, ...
		DijStruct.doseCubeNs, DijStruct.doseCubeNr],...
		indexV);
	% Then calculate indexes for CERR indexing
	DijStruct.index{count}= sub2ind([DijStruct.doseCubeNr,...
		DijStruct.doseCubeNc,  DijStruct.doseCubeNs], ...
		tempR, tempC, tempS);
	
	% Store the value
	DijStruct.value{count} = tmp(3,:)'; 
    end
end

% Close file
fclose(fid);
