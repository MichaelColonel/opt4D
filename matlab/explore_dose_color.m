function explore_dose(dose, pln_file_name, iso_dose_levels, show_dose_profiles)
%explore_dose   Allows user to interactively examine a dose ouput by opt4D.
%
%   explore_dose(dose_file_name, pln_file_name) Explores the dose of the
%   specified files.
%
%   explore_dose With no arguments allows a user to select the dose files
%   from a dialog box.
%
%   explore_dose(dose, plan) The dose can also be passed in as an array and the
%   plan as either a file name or as a plan structure (see read_pln.m)
%
%   explore_dose(dose, plan_file_name, iso_dose_levels) Rounds the dose 
%   shown to the levels passed in.
%
%   explore_dose(dose, plan_file_name, [] show_dose_profiles) Where
%   show_dose_profiles is boolean can extract dose profiles from the dose.
%
%   See also explore_voi


% Call up a dialog to choose a file if it is not included already.
if(~exist('dose'))
    [dose, path_name, filter_index] = ...
        uigetfile({'*.dat', 'Dose file (*.dat)'}, 'Load dose file');
    if(filter_index == 0)
        disp( 'User pressed cancel.');
        return;
    else
        dose = [path_name, dose];
    end
end

% Call up a dialog to choose a plan file if it is not included already.
if(~exist('pln_file_name') && isstr(dose))
    [file_name, path_name, filter_index] = uigetfile( ...
         {'*.pln', 'Opt4D Plan File (*.pln)'; ... 
	  '*.dif', 'Dimention Information file (*.dif)'}, ...
         'Pick dif or pln file', dose);
    if(filter_index == 0)
        disp( 'User pressed cancel.');
    elseif(filter_index == 1)
	pln_file_name = [path_name, file_name];
    elseif(filter_index == 2)
	dif_file_name = [path_name, file_name];
    end
end

% Read plan file if it exists
if(exist('pln_file_name'))
    if(isstr(pln_file_name));
        plan = read_pln(pln_file_name);
    else
        plan = pln_file_name;
    end
    
    dif_file_name = plan.dif_file_name;

    % Read voi outline to add to plot.
    [voi_outline, voi_cmap] = read_voi_outline(plan);
end

% Get dimension information
if(exist('dif_file_name'))
    [dif] = read_dif(dif_file_name);
elseif(isstr(dose))
    dif.nZ = input('Rows of dose cube? ');
    dif.nX = input('Columns of dose cube? ');
    dif.dX = 1; dif.dY = 1; dif.dZ = 1;
else
    dif.nZ = size(dose,1);
    dif.nX = size(dose, 2);
    dif.dX = 1; dif.dY = 1; dif.dZ = 1;
end
    
nRows = dif.nZ;
nColumns = dif.nX;
dX = dif.dX; dY = dif.dY; dZ = dif.dZ;

if(~exist('iso_dose_levels'))
    iso_dose_levels = [];
end

if(~exist('show_dose_profiles'))
    show_dose_profiles = false;
end

if(isstr(dose))
    [Dose] = read_dose(dif, dose);
else
    Dose = dose;
    if(size(Dose,1) ~= nRows || size(Dose,2) ~= nColumns)
        Dose = reshape(Dose, nRows, nColumns, []);
    end
end

nSlices = size(Dose, 3);

[overallMax, I] = max(Dose(:));
[maxRow, maxColumn, maxSlice] = ind2sub(size(Dose), I);

% Level doses to iso-dose levels if supplied
iso_dose_levels = sort(iso_dose_levels);
if(~isempty(iso_dose_levels))
    leveled_dose = Dose;
    for(i = 1:numel(iso_dose_levels))
        leveled_dose(Dose >= iso_dose_levels(i)) = iso_dose_levels(i);
    end
    Dose = leveled_dose;
end


if(exist('voi_outline'))
    nVois = double(max(voi_outline(:)));

    Scaled_Dose = uint8(nVois + 1 + Dose * ((255-nVois) / overallMax));
    Scaled_Dose(voi_outline ~= 0) = voi_outline(voi_outline ~= 0); 
    cmap = [voi_cmap; jet(255-nVois)];
else
    Scaled_Dose = uint8(Dose * (255 / overallMax));
    cmap = jet(256);
end


% Initialize row, column, slice for exploration
row = nRows;
column = nColumns;
slice = 1;

solid_fig = figure;
[new_row,new_column,new_slice]=explore_solid_RCS(Scaled_Dose, cmap,dZ,dX,dY,row,column,slice);

if(show_dose_profiles)
    isodose_fig = figure;
    slicevdose_fig = figure;
    columnvdose_fig = figure;
    rowvdose_fig = figure;

    while((row~=new_row)||(slice~=new_slice)||(column~=new_column))
	row = new_row; column = new_column; slice = new_slice;

        % Plot isodose
        figure(isodose_fig); clf;
        if(exist('voi_outline'))
            h = subimage(voi_outline(:,:,slice), voi_cmap);
            axis xy
            keyboard
        end
        hold on;
        [c,h] = contour(Dose(:,:,slice), 72*[.95 .8 .5]);
        clabel(c,h);
        hold off;
        

        
	% Plot dose along row
	figure(rowvdose_fig);
	plot(Dose(:,column,slice));
	title(['Dose Profile at Column ' num2str(column) ' and Slice ' num2str(slice)]);
	xlabel('Row (Z)');
	ylabel('Dose');
	
	% Plot dose along slice
	figure(slicevdose_fig);
	plot(squeeze(Dose(row,column,:)));
	title(['Dose Profile at Column ' num2str(column) ' and Row ' num2str(row)]);
	xlabel('Slice (Y)');
	ylabel('Dose');

	% Plot dose along column
	figure(columnvdose_fig);
	plot(Dose(row,:,slice));
	title(['Dose Profile at Row ' num2str(row) ' and Slice ' num2str(slice)]);
	xlabel('Column (X)');
	ylabel('Dose');

	figure(solid_fig);
	[new_row,new_column,new_slice] = ...
	    explore_solid_RCS(Scaled_Dose, cmap,1,1,1,row,column,slice);
    end
end
    
return
