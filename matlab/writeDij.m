function writeDij(DijStruct, outFile)
% writeDij(DijStruct, outFile)
%
% Write influence matrix out in bixel format
% IM is the intensity modulated data structure.
% structNum is the structure number to be written out.
% outFile is the output file.
%
% Writes a correctly formatted dij file.
%
% Throws an error if the file writing fails

if(~exist('outFile'))
    outFile = [pwd '/default.dij'];
end


outfid = fopen(outFile, 'w');

% Here's the Dij file format:
% it'a a binary file with no "separators" between the entries
% 
% File header:   
% 
% float    a_g, a_t, a_c  ------ Gantry, table and collimator angles
%   
% float    dx_b, dy_b  --- bixel dimensions (in mm)
%   
% float    dx, dy, dz  ---- voxel dimensions (in mm)
% 
% int       Nx, Ny, Nz  ---- dose cube dimensions (number of voxels)
% 
% int       Npb     -------- number of pencil beams (bixels) used for this field
% 
% float	Dose-scalefactor   ---- since the doses are stored as short
%                               integers, this is the conversion factor to the
%		absolute dose ---- for the best resolution,	this will be:
%			max(Dij_entry)/max(short) = max(Dij_entry)/(2^15-1)
% 
%
% This is followed by "Npb" series of entries for individual beam elements
% 
%   
% Beam header:  
% 
% float     Energy
%
% float     spot_x, spot_y ------ position of the bixel with respect to the
%					central axis of the field (in mm)
% int     Nvox   ---- number of voxels with non-zero dose from this beamlet
%   
% Now for each Nvox:
%   
% int     VoxelNumber     ---- voxel ID in the cube (between 1 and Nx*Ny*Nz)
%				this can be ?
% 
% short    Value  ----- multiply this by Dose-scalefactor to get the dose
%			deposited by this beamlet to the voxel VoxelNumber,
%			assuming the beamlet weight of 1
% 
%   
% Now same for the next beamlet etc.
% 
% Of course, the index VoxelNumber is related to the voxel position in the cube.
%   
% KonRad uses a right-handed Cartesian coordinate system for the dose cube:
% 
% for gantry at 0, in beam's eye view, x is pointing to the right, y == up,
% and z == towards the gantry.
% 
% So , the origin of the cube is in the distal lower left corner.
% 
% For a voxel at (x,y,z) w/ respect to the cube origin,  the VoxelNumber =
%					x/dx + ( y/dy + z/dz*Ny )*Nx 
% 

% Write header to file:
tmp = [ DijStruct.gantryAngle;
        DijStruct.tableAngle;
        DijStruct.collimatorAngle;
        DijStruct.spotdx * 10;
        DijStruct.spotdy * 10;
        DijStruct.voxeldC * 10;
	DijStruct.voxeldS * 10;
	DijStruct.voxeldR * 10];
fwrite(outfid, tmp, 'float32');

tmp = [ DijStruct.doseCubeNc;
        DijStruct.doseCubeNs;
        DijStruct.doseCubeNr;
        DijStruct.numPencilBeams];
fwrite(outfid, tmp, 'int');

tmp = [ DijStruct.doseCoefficient];
fwrite(outfid, tmp, 'float32');

% Write information for each beamlet
for count=1:DijStruct.numPencilBeams;
    % Beam header
    tmp = [ DijStruct.energy(count);
            DijStruct.spotX(count) * 10;
            DijStruct.spotY(count) * 10];
    fwrite(outfid, tmp, 'float32');
    tmp = [ DijStruct.numVoxels(count)];
    fwrite(outfid, tmp, 'int');
    
    % Write voxel data if there are any voxels
    if(DijStruct.numVoxels(count) > 0)

		% Convert CERR indexes to Konrad indexing
		% First read coordinates in Konrad system from CERR index values
		[tempZ,tempX,tempY] = ind2sub([DijStruct.doseCubeNr,...
			DijStruct.doseCubeNc,  DijStruct.doseCubeNs],...
			DijStruct.index{count});
		% Then calculate indexes for Konrad indexing
		indexV = sub2ind([DijStruct.doseCubeNc, DijStruct.doseCubeNs,...
				DijStruct.doseCubeNr], tempX, tempY, tempZ) - 1;

		% Now rearrange data for storage and write it out
        tmp = uint16(zeros(3,DijStruct.numVoxels(count)));
        tmp(1,:) = bitshift(indexV', 0, 16);
        tmp(2,:) = bitshift(indexV', -16, 16);
        tmp(3,:) = DijStruct.value{count}';
        fwrite(outfid,tmp,'ushort');    
    end
end

% Close file
status = fclose(outfid);
if status ~= 0
	error(['Error writing dij file']);
end
