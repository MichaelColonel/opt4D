function [stf] = read_stf(file_name)
%READ_STF Read a .stf file (beam steering file)
%
%   stf = READ_STF(file_name) Reads the beam steering file into
%   the stf structure.  stf has the following fields:
%
%      stf.machineName          Machine name (string)
%      stf.gantryAngle          Gantry angle in degrees
%      stf.tableAngle           Table angle in degrees
%      stf.collimatorAngle      Collimator angle in degrees
%      stf.spot_dx              Bixel width (cm)
%      stf.spot_dy              Bixel height (cm)
%      stf.energy               Energy for each beamlet (n x 1)
%      stf.spotX                Bixel x position in cm (nx1)
%      stf.spotY                Bixel y position in cm (nx1)
%
%   Throws an error if the file writing fails
%
%   See also write_stf.

% First read header
[fid,message] = fopen(file_name,'rt');
if(fid == -1)
    error(['Error opening stf file: ' file_name, ...
           '  System message: ' message]);
end

% Throw away first line
fgetl(fid);
stf.machineName = fscanf(fid, '# Machinename: %s\n', 1);
stf.gantryAngle = fscanf(fid, '# Gantry-Angle: %g\n', 1);
stf.tableAngle  = fscanf(fid, '# Table-Angle: %g\n', 1);
stf.collimatorAngle = fscanf(fid, '# Colli-Angle: %g\n', 1);
spot_dx = fscanf(fid, '# Delta X: %g', 1);
spot_dy = fscanf(fid, ' Delta Y: %g\n', 1);
stf.spot_dx = spot_dx / 10;
stf.spot_dy = spot_dy / 10;
fclose(fid);

% Read information about each beamlet
[bixel_no, energy, spotX, spotY] = textread(file_name, ...
            '%n%n%*n%*s%*n%n%n', 'commentstyle', 'shell');

% Store in stf structure
stf.energy(bixel_no+1,1) = energy;
stf.spotX(bixel_no+1,1) = spotX / 10;
stf.spotY(bixel_no+1,1) = spotY / 10;

