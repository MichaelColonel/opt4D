function [C, c_min, c_max, p_under, p_over, c_to_d] = ...
calculate_reduced_problem(D, d_min, d_max, q_under, q_over, dif, threshold, ...
fraction_reduction)
%calculate_reduced_problem Calculate smaller IMRT problem from the larger one
%
%
% [C, c_min, c_max, p_under, p_over, c_to_d] = calculate_reduced_problem(...
% D, d_min, d_max, q_under, q_over, dif, threshold, fraction_reduction)
%
%   See also calculate_voxel_clustering.m

if(~exist('threshold'))
    threshold = 0;
end

if(~exist('fraction_reduction'))
    fraction_reduction = 0;
end

nVoxels = size(D,1);

% Generate voi_list from voxel prescriptions
combined_stats = [d_min, d_max, q_under, q_over];
voi_stats = unique(combined_stats, 'rows');
nVois = size(voi_stats, 2);

voi_list = zeros(nVoxels, 1);
for(i = 1:nVoxels)
    for(iVoi = 1:nVois)
        if(all(combined_stats(i,:) == voi_stats(iVoi,:)))
            voi_list(i) = iVoi;
            break
        end
    end
end

[c_to_d, d_to_c] = calculate_voxel_clustering(D, voi_list, dif, ...
   threshold, fraction_reduction);

C = d_to_c * D;
c_min = d_to_c * d_min;
c_max = d_to_c * d_max;
p_under = c_to_d' * q_under;
p_over = c_to_d' * q_over;


belongs_to = zeros(nVoxels, 1);
for(i = 1:nVoxels)
    belongs_to(i) = find(d_to_c(:,i));
end
figure;
image(reshape(mod(belongs_to(dif.nX * dif.nZ + (1:dif.nX*dif.nZ)),...
    256), dif.nZ, dif.nX));
colormap(rand(256, 3));

return
