function [contours,oarname,posout] = read_vdx_multi(VDXfile,crit_str_num,slicenum,fpos);
%   function READ_VDX: read critical structure outlines from Konrad VDX-file
%                       Usage: out = read_vdx(VDXfile,crit_str_num,slicenum);
%                       out(n,2) = matrix with x,y coordinates of the outline nodes
%                       VDXfile = name of the file
%                       str_num = critical structure number
%                       slicenum = slice number
%                       fpos = field position to start search from

if nargin<4
    fpos = 0;
end

filein = fopen(VDXfile,'rt');
found = 0;
nn=0;
tmp = [];
out = [];
contours = cell(1,1);
nContours = 0;
linein = ' ';
oarname = [];

if (crit_str_num>1 & fpos>100)
    status = fseek(filein,fpos-100,'bof');
    %nn=1;
    % grab the first volume encountered
    crit_str_num = 1;
end
posout = 0;

while (nn<crit_str_num)
    linein = fgetl(filein);
    if ~isempty(linein) & (linein==-1) return; end
    tmp = findstr(linein,'type');
    if ~isempty(tmp)
        nn=nn+1;
        if nn==2 & fpos==0
            posout = ftell(filein);
        end
    end
end

if posout==0 & fpos>0
    posout=fpos;
end

tmp = findstr(linein,' ');
if ~isempty(tmp) oarname = linein(tmp(1)+1:tmp(2)-1); end

nn=0;
tmp = [];

while (nn<=slicenum)
    if found == 0
        linein = fgetl(filein);
    end
    if ~isempty(linein) & (linein==-1) return; end
    tmp = findstr(linein,'type');
    if ~isempty(tmp)
        break
    end
    tmp = findstr(linein,'SagittalObjects');
    if ~isempty(tmp)
        break
    end
    tmp = findstr(linein,'FrontalObjects');
    if ~isempty(tmp)
        break
    end

    tmp = findstr(linein,'slice#');
    if ~isempty(tmp)
        tmp = findstr(linein,' ');
        nn = str2num(linein(tmp(1)+1:tmp(2)-1));
    end

    if nn==slicenum

        out = [];
        points = [];
        nContours = nContours + 1;
        found = 1;
        
        linein = fgetl(filein);
        tmp = findstr(linein,' ');
        % number of points
        npts = sscanf(linein(tmp(1)+1:end),'%d');
        linein = fgetl(filein);
        tmp = findstr(linein,' ');
        % read the first line
        points = [points; sscanf(linein(tmp(1)+1:end),'%d')];
        tmp=[];
        while isempty(tmp)
            linein = fgetl(filein);
            %if linein==-1 return; end
            tmp = findstr(linein,'#');
            if ~isempty(tmp)
                break
            end
            %linein
            points = [points; sscanf(linein,'%d')];
        end
        tmp = max(size(points));
        if 2*npts ~= tmp
            disp(['Found ' num2str(tmp) ' records, expected 2*' num2str(npts) ]);
        end
        for ii=1:floor(tmp/2)
            out(ii,:)= points(2*ii-1:2*ii)';
        end
        contours{nContours} = out;
        disp(['Volume name: (' num2str(crit_str_num) ') ' oarname ', contour: ' num2str(nContours)]);
    end
end

if found ~= 1
    disp([ oarname ' (' num2str(crit_str_num) '): Slice ' num2str(slicenum) ' not detected']);
end
status = fclose(filein);



