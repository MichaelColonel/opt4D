function [z_rpl] = radioactive_path_length(beam_SAD, density, radius, z)
%radioactive_path_length Calculates a radioactive path length
%
%   [z_rpl] = radioactive_path_length(beam_SAD, density, radius, z) Calculates
%   water-equivalent radioactive path length.  Returns the same size
%   matrix as z.  Z should be in the conic dimension such that z=0 corresponds
%   to the isocenter and z=beam_SAD corresponds to the source position.
%
%   Note that this is a dummy implementation.  It assumes that the patient is a
%   cylinder of uniform density.

    z_rpl = density * (z - beam_SAD + radius);
    z_rpl(z > beam_SAD + radius) = 2 * density * radius;
    z_rpl(z < beam_SAD - radius) = 0;

return


