function draw_voi_outlines(plan)
%draw_voi_outlines Draws the 3D outline of the VOIs
%  draw_voi_outlines(plan) Draws the 3D outline of the VOIs in a new
%    figure.  plan can be either the name of a plan file or the structure for
%    one.

if(isstr(plan))
    plan = read_pln(plan);
end

dif = read_dif(plan.dif_file_name);
voi_field = read_voi(plan);

iso_slice = voi_field(:,:,dif.isoY);

figure
imagesc(iso_slice);

slice = iso_slice;

figure;

for iSlice = 1:size(voi_field, 3)
    [x, y] = find_edges(voi_field(:,:,iSlice));
    h = line(x,y,repmat(iSlice, size(x)));
    set(h,'color',[0 0 0]);
    drawnow
end




% -------------------------------------------
function [x,y] = find_edges(slice)

% Creating x,y coordinate system relative to 0,0 position in 2-d array passed, 
% normalized to each pixel = 1 unit

% Find horizontal edges
[i, j] = find(slice(1:end-1,:) ~= slice(2:end, :));
x = [i'+.5; i'+.5];
y = [j'-.5; j'+.5];

% Find vertical edges
[i, j] = find(slice(:,1:end-1) ~= slice(:,2:end));
x = [x, [i'-.5; i'+.5]];
y = [y, [j'+.5; j'+.5]];

% line(y,x);

