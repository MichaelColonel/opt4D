function [simulated_multi_objectives, simulated_euds] = ...
    simulate_setup_error(plan, static_dose, shift_function, nTreatments)
%simulate_setup_error Simulates the effects of rigid shifts to a plan
%
%    Does not plot anything if you are using the return values.
%  example: simulate_setup_error(plan, static_dose, 'randn(3,30)', 20);


% Read plan file
if(isstr(plan))
    plan = read_pln(plan);
end

% Read dimension information file
dif = read_dif(plan.dif_file_name);

nRows = dif.nZ;
nColumns = dif.nX;
nSlices = dif.nY;
nVoxels = nRows * nColumns * nSlices;
nVois = length(plan.VOI);

% Read Voi file
voi_list = reshape(read_voi(plan), nVoxels, 1);
nNonairVoxels = sum(voi_list > 0);

% Find prescriptions for each voxel
[d_min, d_max, q_under, q_over] = find_voxel_constraints(plan);

% Read Dij matrix from files
% [Dij, bixel_info] = read_simplified_dij(plan);
% nBixels = length(bixel_info.beamNo);

if(isstr(static_dose))
    static_dose = read_dose(dif, static_dose);
end
[static_multi_objective, static_objective] = calculate_multi_objective(...
    plan, voi_list, static_dose);

% Examine results
static_dvh = calculate_dvh(static_dose, 0.1, voi_list, nVois);

% Show DVH if applicable
if(nargout == 0)
    dvh_fig = figure; subplot(2,2,1);
    plot_DVH(static_dvh, plan, [], true);
    title('Static DVH');

    % Show objective
    objective_fig = figure;
    stem(static_objective,1,'filled');
end

% Calculate each simulated treatment
total_dose = zeros(size(static_dose));
total_dvh = zeros(size(static_dvh));
total_dvh(:,1) = static_dvh(:,1);
simulated_objectives = zeros(nTreatments, 1);
simulated_multi_objectives = zeros(nTreatments, 2 * nVois);
simulated_euds = zeros(nTreatments, nVois);
for(iTreatment = 1:nTreatments)
    shifts = eval(shift_function);
    % dose = calculate_registration_matrix(shifts, dif) * Dij ...
    %        * beam_weights / size(shifts,2);
    dose = convolve_dose(static_dose, shifts, dif,...
         repmat(1/size(shifts,2), 1, size(shifts,2)));
         % repmat(1/size(shifts,2), 1, size(shifts,2)));
    total_dose = total_dose + dose;

    dvh = calculate_dvh(dose, 0.1, voi_list, nVois);
    if(size(total_dvh,1) == size(dvh,1))
        total_dvh(:,2:end) = dvh(:,2:end) + total_dvh(:,2:end);
    elseif(size(total_dvh,1) > size(dvh,1))
        total_dvh(1:size(dvh,1),2:end) = dvh(:,2:end) + ...
            total_dvh(1:size(dvh,1),2:end);
    else % (size(total_dvh,1) < size(dvh,1))
        total_dvh = [dvh(:,1), ...
           [total_dvh(:,2:end); zeros(size(dvh,1)-size(total_dvh,1), ...
               size(dvh,2)-1)] + dvh(:,2:end)];
    end

    dvh_of_mean_dose = calculate_dvh(total_dose/iTreatment, 0.1, voi_list, ...
        nVois);

    % Calculate objective
    [multi_objective, objective] = calculate_multi_objective(...
	plan, voi_list, dose);
    simulated_multi_objectives(iTreatment,:) = multi_objective';
    simulated_objectives(iTreatment) = objective;
    [simulated_euds(iTreatment,:)] = calculate_EUDs(plan, voi_list, dose)';
    objective_of_expected_dose = calculate_objective(...
        total_dose(:)/iTreatment, d_min, d_max, q_under, q_over);

    % Make figures if applicable
    if(nargout == 0)
	% Plot average dvh
	figure(dvh_fig); subplot(2,2,2);
	plot_DVH([total_dvh(:,1), total_dvh(:,2:end)/iTreatment], plan);
	title('Average DVH');

	% Plot dvh of average dose
	figure(dvh_fig); subplot(2,2,3);
	plot_DVH(dvh_of_mean_dose, plan);
	title('DVH of Average Dose');

	% Plot dvh of one treatment
	figure(dvh_fig); subplot(2,2,4);
	if(iTreatment > 1)
            hold on;
            plot_DVH(dvh, plan);
            hold off;
        else
            plot_DVH(dvh, plan);
        end
        title(['DVH of ' num2str(iTreatment) ' Simulated Treatments']);

	figure(objective_fig);
	stem(static_objective,1,'filled');
	hold on;
	hist(simulated_objectives(simulated_objectives ~= 0));
	stem(objective_of_expected_dose, 1, 'bx');
	hold off;
    end
end

% Show intensity maps
% figure;
% plot_intensity_maps(w, bixel_info);

% figure;
% explore_dose(static_dose,plan);

% keyboard

