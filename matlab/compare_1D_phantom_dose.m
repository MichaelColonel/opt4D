function compare_1D_phantom_dose(pln_file_name, dose_file_names, result_file_name)
%compare_1D_phantom_dose(pln_file_name, dose_file_names, result_file_name)
%
%plots dose distributions for a 1D phantom
%
%pln_file_name and dose_file_names are required arguments,
%result_file_name defaults to 'test' if not provided.
%dose_file_names has to be a cell array of strings


if(nargin < 2)
    error('Not enough arguments');
end

if(nargin < 3)
    result_file_name = 'test';
end

% read plan file
plan = read_pln(pln_file_name);

% Get dimension information
if(exist(plan.dif_file_name))
    [dif] = read_dif(plan.dif_file_name);
else
    error('no dif file name');
end

% create absolute coordinate vextors
x_coord = linspace( -dif.isoX*dif.dX, (dif.nX-1-dif.isoX)*dif.dX, dif.nX );

% hard code line styles
myLine{1} = '-';
myLine{2} = '--';
myLine{3} = ':';
myLine{4} = '-.';

% hard code line colors
myColor{1} = 'k';
myColor{2} = 'r';
myColor{3} = 'b';
myColor{4} = 'g';
myColor{5} = 'y';

% number of dose files
nFiles = max(size(dose_file_names))
line_handles = [];
for iFile=1:nFiles
    infile = [dose_file_names{iFile}];
    fid = fopen(infile);
    [dose, count] = fread(fid, 'float');
    h = plot(x_coord,dose);
    hold on;
    set(h,'LineWidth',2);
    if( iFile <= max(size(myLine)) )
        set(h,'LineStyle', myLine{1})
    end
    if( iFile <= max(size(myColor)) )
        set(h, 'color', myColor{iFile});
    end
    line_handles(iFile) = h;
    legends{iFile} = [dose_file_names{iFile}];

end

hold off

legend(line_handles, strrep(legends, '_', '\_'), 0);
legend(line_handles,'location', 'NorthOutside');
legend('boxoff');
xlabel('Position','FontSize',15);
ylabel('Dose','FontSize',15);
set(gca,'FontSize',12);

FileName = [result_file_name];
set(gcf, 'PaperUnits', 'centimeter');
set(gcf, 'PaperPosition', [0 0 16 16]);
print ( '-depsc',FileName );
print ( '-djpeg',FileName );

