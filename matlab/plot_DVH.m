function plot_DVH(dvh, plan, plot_vois, show_prescriptions, should_color_errors)
%plot_DVH  Read and plot DVH produced by opt4D
%   Plots a graph of a DVH file as output by opt4D.
%
%   plot_DVH(dvh) plots the DVH with random colors for each volume of
%   interest.  The dvh can either be a file name or an array of column
%   vectors with the first column being the dose for each bin.
%
%   plot_DVH(dvh, plan_file) plots the DVH with information from the
%   plan file.  Uses random colors if colors aren't included in the file.
%
%   plot_DVH(dvh, plan) uses information in a plan structure.  See
%   read_pln.m for information on the structure.
%
%   plot_DVH(dvh, plan, plot_vois) only plots the Volumes of
%   Interst listed in plot_vois.  plot_vois is a vector of integers.  An empty
%   array ([]) is interpreted as including all VOIs.
%
%   plot_DVH(dvh, plan, plot_vois, show_prescriptions, color_errors) draws
%   the prescriptions onto the plot.  show_prescriptions and color_errors
%   are boolean arguments to draw the prescriptions and to color the 
%   errors, respectively.
%
%   Ex:
%      plot_DVH('patient_DVH.dat', 'patient.pln');
%
%   Is the same as:
%      [dvh, dvh_dose_bins] = read_DVH('patient_DVH.dat');
%      plan = read_pln('patient.pln');
%      plot_DVH([dvh_dose_bins dvh], plan);
%
%   The first usage is most common, but the second allows the user to change
%   the plan or dvh if needed.
%
%   Ex:
%      plot_DVH('patient_DVH.dat', 'patient.pln', [1 3 5]);
%   only plots volume of interest 1, 3, and 5.
%
%   Ex:
%      plot_DVH('patient_DVH.dat', 'patient.pln', [], true);
%   shows the prescription on the plot.
%
%   See also read_DVH.


% Read the DVH if needed
if(isstr(dvh))
    [dvh, dvh_dose] = read_DVH(dvh);
else
    dvh_dose = dvh(:,1);
    dvh = dvh(:,2:end);
end

% Find Vois with at least one voxel
nonzero_vois = find(dvh(1,:) > 0);

nVois = size(dvh,2);
nDVH_bins = size(dvh_dose,1);

% Check if the plan is defined
if( exist('plan'))
    % Read plan from file if a file name was given
    if(isstr(plan))
        plan = read_pln(plan);
    end
    
    % Make sure plan has all required parts
    if(isstruct(plan))
        if(~isfield(plan,'title') || ~isfield(plan,'VOI'))
            error('plan is not a valid structure.');
        end
    else
        error('plan is not a valid structure.');
    end

    % Make sure all VOIs are included
    if(nVois > length(plan.VOI))
        for(iVoi = length(plan.VOI)+1:nVois)
            plan.VOI(iVoi).name = ['Struct ', num2str(iVoi)];
        end
    end
else
    % Create default attributes for undefined plan
    plan = struct('title', '');
    plan.VOI = repmat(struct('name',''), 1, nVois);
    for iVoi = 1:length(plan.VOI)
        plan.VOI(iVoi).name = ['Struct ', num2str(iVoi)];
    end
end

% Default to not showing prescriptions
if( ~exist('show_prescriptions'))
    show_prescriptions = false;
end

% Default to not coloring in prescriptions
if( ~exist('should_color_errors'))
    should_color_errors = false;
end

% Make sure the color field exists
if(~isfield(plan.VOI, 'color'))
    plan.VOI(1).color = [];
end

% Assign random colors to structures without colors
temp_color = rand(length(plan.VOI),3);
for iVoi = 1:length(plan.VOI)
    if(isempty(plan.VOI(iVoi).color))
        plan.VOI(iVoi).color = temp_color(iVoi,:);
    end
end

% Discard some vois if appropriate
if(exist('plot_vois') && ~isempty(plot_vois))
    nonzero_vois = intersect(nonzero_vois, plot_vois);
end

% Quit if there are no vois left to plot
if(isempty(nonzero_vois))
    return
end

line_handles = [];
legends = cell(size(nonzero_vois));
for(i = 1:length(nonzero_vois))
    iVoi = nonzero_vois(i);

    % There is some dose delivered, so plot on DVH
    h = plot(dvh_dose, dvh(:,iVoi), '--');
    hold on;
    
    set(h,'LineWidth',2);

    if(~isempty(plan.VOI(iVoi).color))
        set(h, 'color', plan.VOI(iVoi).color)
    end
    
    if(isfield(plan.VOI, 'isTarget') && ~isempty(plan.VOI(iVoi).isTarget) && plan.VOI(iVoi).isTarget)
        set(h,'LineStyle', '-')
    end

    
    line_handles(i) = h;
    legends{i} = [plan.VOI(iVoi).name];
end


% Display prescriptions (only if requested)
if(show_prescriptions)
    nObjectives = numel(plan.objectives);
    for(iObjective = 1:nObjectives)
        objective = plan.objectives(iObjective);
        voiNo = objective.voiNo;

        % Display prescriptions only if color defined
        if(~isempty(plan.VOI(voiNo).color))

            if(objective.is_min_constraint | objective.is_equality_constraint)
                objective.desired_volume = 100;

                g = line(objective.desired_dose + [0 0;-5 0],...
                objective.desired_volume + [0 0;0 -5]);
                set(g, 'color', plan.VOI(voiNo).color);

                min_DVH_bin = max(find(dvh_dose <= objective.desired_dose));
                if(~isempty(min_DVH_bin) ...
                    && dvh(min_DVH_bin,voiNo) < objective.desired_volume)
                    g = line(dvh_dose(min_DVH_bin) + [0;0], ...
                    [objective.desired_volume; dvh(min_DVH_bin,voiNo)]);
                    set(g, 'color', plan.VOI(voiNo).color,'linestyle',':');
                end

                if(should_color_errors & (dvh(1,voiNo) > 0))
                    for(iDVH_bin=voiNo:nVois:nDVH_bins)
                        if(dvh_dose(iDVH_bin) < objective.desired_dose)
                            if(dvh(iDVH_bin,voiNo) < ...
                                objective.desired_volume)
                                h = line(dvh_dose(iDVH_bin)*[1;1], ...
                                [dvh(iDVH_bin,voiNo); 
                                objective.desired_volume]);
                                set(h,'Color',plan.VOI(voiNo).color);
                            end
                        end
                    end
                end

            end

            if(objective.is_max_constraint | objective.is_equality_constraint)
                objective.desired_volume = 0;

                g = line(objective.desired_dose + [0 0; 5 0], ...
                    objective.desired_volume + [0 0; 0 5]);
                set(g, 'color', plan.VOI(voiNo).color);
        
                max_DVH_bin = min(find(dvh_dose >= objective.desired_dose));
                if(~isempty(max_DVH_bin) ...
                    && dvh(max_DVH_bin,voiNo) > objective.desired_volume)

                    g = line(dvh_dose(max_DVH_bin) + [0;0], ...
                    [objective.desired_volume; dvh(max_DVH_bin,voiNo)]);
                    set(g, 'color', plan.VOI(voiNo).color,'linestyle',':');
                end
            
                if(should_color_errors & (dvh(1,voiNo) > 0))
                    for(iDVH_bin=voiNo:nVois:nDVH_bins)
                        if(dvh_dose(iDVH_bin) > objective.desired_dose)
                            if(dvh(iDVH_bin,voiNo) > objective.desired_volume)
                                h = line(dvh_dose(iDVH_bin)*[1;1], ...
                                [dvh(iDVH_bin,voiNo); ...
                                objective.desired_volume]);
                                set(h,'Color',plan.VOI(voiNo).color);
                            end
                        end
                    end
                end
            end
        end
    end
end

hold off

title(strrep(plan.title, '_', '\_'));
legend(line_handles, strrep(legends, '_', '\_'), 0);
xlabel('Dose (Gy)','FontSize',15);
ylabel('Relative volume (%)','FontSize',15);
set(gca,'FontSize',13);

return
