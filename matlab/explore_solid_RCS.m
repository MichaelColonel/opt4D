function [row, column, slice] = explore_solid_RCS( rcs_data, cmap, ...
						   dx, dy, dz, ...
                                                   row, column, slice)
%explore_solid_RCS   Examine data in RCS format.
%    Examine data thet represents the value at various points in a 3D solid.
%    The user can click to move through data by slices.
%
%    explore_solid_RCS(rcs_data, cmap, dx, dy, dz) uses the colormap cmap and
%    voxel size (dx, dy, dz).
%
%    explore_solid_RCS(rcs_data, cmap) assumes voxels are cubes.
%
%    explore_solid_RCS(rcs_data) uses a grayscale color map.
%    
%    [row, column, slice] = explore_solid_RCS(...)  Returns the final row,
%    column and slice.
%
%    Click on a side with left button to remove a slice.
%    Click on a side with right button to add a slice.
%    Click on a side with middle button to add all slices back.
%    Click outside the volume to exit.

[nR, nC, nS] = size(rcs_data);

% Use grayscale colormap if needed
if(nargin < 2 || isempty(cmap))
    cmap = gray(256);
end

if(nargin < 5)
    dx = 1; dy = 1; dz = 1;
end

if(nargin < 8)
    row = nR;
    column = nC;
    slice = 1;
end

levels = double( unique(rcs_data(:)));

clf;

% Range of data to show
rows = 1:row;
columns = 1:column;
slices = slice:nS;

% Draw Axial face
temp_axial = rcs_data(rows,columns,slices(1));
x = [1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
y = slices(1) * ones(2,3);
z = [rows(1);rows(end)] * [1 1 1];
axial_handle = warp(x,y,z,temp_axial,cmap);
hold on;

% Draw Coronal face
temp_coronal = rcs_data(rows(end),columns,slices);	
temp_coronal = squeeze(permute(temp_coronal,[3,1,2]));
x=[1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
y= [slices(1);slices(end)] * [1 1 1];
z = rows(end) * ones(2,3);
coronal_handle = warp(x,y,z,temp_coronal,cmap);

% Draw Sagital face
temp_sagital = rcs_data(rows(end):-1:rows(1),columns(end),slices);	
temp_sagital = squeeze(permute(temp_sagital,[3,2,1]));
x = columns(end) * ones(2,3);
y = [slices(1);slices(end)] * [1 1 1];
z = [1;1]*[rows(end) (rows(end)+rows(1))/2  rows(1)];
sagital_handle = warp(x,y,z,temp_sagital,cmap);

% Draw outline of dose cube
x = [1,1,    nC,    nC,1,1,    nC,    nC,    nC,    nC,    nC]';
y = [1,nS,nS,1,1,1,1,1,nS,nS,1]';
z = [nR,nR,nR,nR,nR,1,1,nR,nR,1,1]';
big_box_handle = line(x,y,z);
set(big_box_handle,'LineStyle',':');

% Draw outline of cut data
x = [1,1,column,column,1,1,column,column,column,column,column]';
y = [slice,nS,nS,slice,slice,slice,slice,slice,nS,nS,slice]';
z = [row,row,row,row,row,1,1,row,row,1,1]';
inner_box_handle = line(x,y,z);
set(inner_box_handle,'Color',[0 1 0]);

set(gca,'color','none');

% Add labels
xlabel('columns');
ylabel('slices');
zlabel('rows');

% Set view
default_view = [  4.4029    0.3061         0   -2.3545
                 -0.7537    0.5068    3.9152   -1.8341
                 -1.1986    0.8059   -2.4620   34.5111
                  0         0         0         1.0000];
view(default_view); set(gca,'ydir','normal');
axis tight
set(gca,'DataAspectRatio',[1/dx 1/dy 1/dz]);

set(axial_handle, 'ButtonDownFcn', @axial_callback);
set(coronal_handle, 'ButtonDownFcn', @coronal_callback);
set(sagital_handle, 'ButtonDownFcn', @sagital_callback);

user_data = struct('data', rcs_data,...
    'row', row, 'column', column, 'slice', slice,...
    'nR', nR, 'nC', nC, 'nS', nS, ...
    'axial_handle',   axial_handle,...
    'coronal_handle', coronal_handle,...
    'sagital_handle', sagital_handle,...
    'inner_box_handle', inner_box_handle);
set(gca,'UserData', user_data);


return

keyboard

done = false;
while(~done)

    % Get button click
    [x, y, button] = ginput(1);

    if(y > .5 & y < 1.5)
        % Clicked on front face.
        if(button==1 && slice < nS)
            slice = slice + 1;
        elseif(button==2)
            slice = 1;
        elseif(button==3 && slice > 1)
            slice = slice - 1;
        end
        slices = slice:nS;
    elseif(y > 1 && y < nS)
        % Clicked on a side of the cube
        if(x == nC)
            % Clicked on right (sagital) face
            if(button==1 && column > 1)
                column = column - 1;
            elseif(button==2)
                column = nC;
            elseif(button==3 && column < nC)
                column = column + 1;
            end
            columns = 1:column;
        else
            % Clicked on top face
            if(button==1 && row > 1)
                row = row - 1;
            elseif(button==2)
                row = nR;
            elseif(button==3 && row < nR)
                row = row + 1;
            end
            rows = 1:row;
        end
    else
        done = true;
    end

    % Update Axial face
    set(axial_handle, 'cdata', double(rcs_data(rows,columns,slices(1)))+1, ...
        'xdata', [1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)],...
        'ydata', slices(1) * ones(2,3), ...
        'zdata', [rows(1);rows(end)] * [1 1 1]);

    % Update Coronal face
    temp_coronal = rcs_data(rows(end),columns,slices);
    temp_coronal = squeeze(permute(temp_coronal,[3,1,2]));
    set(coronal_handle, 'cdata', double(temp_coronal)+1, ...
        'xdata', [1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)],...
        'ydata', [slices(1);slices(end)] * [1 1 1], ...
        'zdata', rows(end) * ones(2,3));

    % Update Coronal face
    temp_sagital = rcs_data(rows(end):-1:rows(1),columns(end),slices);	
    temp_sagital = squeeze(permute(temp_sagital,[3,2,1]));
    set(sagital_handle, 'cdata', double(temp_sagital)+1, ...
        'xdata', columns(end) * ones(2,3),...
        'ydata', [slices(1);slices(end)] * [1 1 1],...
        'zdata', [1;1]*[rows(end) (rows(end)+rows(1))/2  rows(1)]);


    % Draw outline of cut data
    set(inner_box_handle, ...
        'xdata', ...
        [1,1,column,column,1,1,column,column,column,column,column]',...
        'ydata', ...
        [slice,nS,nS,slice,slice,slice,slice,slice,nS,nS,slice]',...
        'zdata', ...
        [row,row,row,row,row,1,1,row,row,1,1]');
end

hold off;

return


%------------------------------------
function axial_callback(src, event_data)
% Called when clicking on axial side

user_data = get(gca, 'userdata');
selector = get(gcf,'SelectionType');
if(strcmp(selector,'open') && isfield(user_data, 'last_selector'))
    selector = user_data.last_selector;
end
if(strcmp(selector, 'normal') && user_data.slice < user_data.nS)
    user_data.slice = user_data.slice + 1;
elseif(strcmp(selector, 'alt') && user_data.slice > 1)
    user_data.slice = user_data.slice - 1;
elseif(strcmp(selector, 'extend'))
    user_data.slice = 1;
end
user_data.last_selector = selector;
set(gca, 'userdata', user_data);

update_figure;

return

%------------------------------------
function sagital_callback(src, event_data)
% Called when clicking on sagital side

user_data = get(gca, 'userdata');
selector = get(gcf,'SelectionType');
if(strcmp(selector,'open') && isfield(user_data, 'last_selector'))
    selector = user_data.last_selector;
end
if(strcmp(selector, 'normal') && user_data.column > 1)
    user_data.column = user_data.column - 1;
elseif(strcmp(selector, 'alt') && user_data.column < user_data.nC)
    user_data.column = user_data.column + 1;
elseif(strcmp(selector, 'extend'))
    user_data.column = user_data.nC;
end
user_data.last_selector = selector;
set(gca, 'userdata', user_data);

update_figure;

return

%------------------------------------
function coronal_callback(src, event_data)
% Called when clicking on coronal side

user_data = get(gca, 'userdata');
selector = get(gcf,'SelectionType');
if(strcmp(selector,'open') && isfield(user_data, 'last_selector'))
    selector = user_data.last_selector;
end

if(strcmp(selector, 'normal') && user_data.row > 1)
    user_data.row = user_data.row - 1;
elseif(strcmp(selector, 'alt') && user_data.row < user_data.nR)
    user_data.row = user_data.row + 1;
elseif(strcmp(selector, 'extend'))
    user_data.row = user_data.nR;
end

user_data.last_selector = selector;

set(gca, 'userdata', user_data);

update_figure;

return

%---------------------------------------
function update_figure
% Called after clicking on any side

% Read data out of figure
user_data = get(gca, 'userdata');

nS = user_data.nS;
row = user_data.row;
column = user_data.column;
slice = user_data.slice;

% Range of data to show
rows = 1:row;
columns = 1:column;
slices = slice:user_data.nS;

% Update Axial face
set(user_data.axial_handle,...
    'cdata', double(user_data.data(rows,columns,slices(1)))+1, ...
    'xdata', [1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)],...
    'ydata', slices(1) * ones(2,3), ...
    'zdata', [rows(1);rows(end)] * [1 1 1]);

% Update Coronal face
temp_coronal = user_data.data(rows(end),columns,slices);
temp_coronal = squeeze(permute(temp_coronal,[3,1,2]));
set(user_data.coronal_handle,...
    'cdata', double(temp_coronal)+1, ...
    'xdata', [1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)],...
    'ydata', [slices(1);slices(end)] * [1 1 1], ...
    'zdata', rows(end) * ones(2,3));

% Update Coronal face
temp_sagital = user_data.data(rows(end):-1:rows(1),columns(end),slices);	
temp_sagital = squeeze(permute(temp_sagital,[3,2,1]));
set(user_data.sagital_handle, ...
    'cdata', double(temp_sagital)+1, ...
    'xdata', columns(end) * ones(2,3),...
    'ydata', [slices(1);slices(end)] * [1 1 1],...
    'zdata', [1;1]*[rows(end) (rows(end)+rows(1))/2  rows(1)]);



% Draw outline of cut data
set(user_data.inner_box_handle, ...
    'xdata', ...
    [1,1,column,column,1,1,column,column,column,column,column]',...
    'ydata', ...
    [slice,nS,nS,slice,slice,slice,slice,slice,nS,nS,slice]',...
    'zdata', ...
    [row,row,row,row,row,1,1,row,row,1,1]');

return
