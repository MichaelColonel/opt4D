function writePln(plan_file_name, plan)

error('writePln.m is out of date and cannot yet write .pln files in version 0.1 format.');
%writePln writes a opt4D plan file.
%   The .pln format is used by opt4D to hold useful information about the
%   plan to be optimized.  It holds general information about the plan
%   as well as the paramaters for optimization.  It is designed as a very
%   flexible format that is human readable.
%
%   writePln(plan_file_name, plan) writes a plan file to the specified file
%   based on the information in the plan structure.
%
%   Example: plan.pln_file_version = 0.1;
%            plan.title = 'Sample Plan';
%            plan.description = 'A sample plan with two VOIs';
%            plan.plan_file_root = '/home/user/data/konrad_files/sample_plan';
%            plan.nInstances = 1;
%            plan.nBeams = 7;
%            plan.VOI(1).name = 'tumor';
%            plan.objective(1).VOI(1).min_DVH = '72 95%';
%            plan.VOI(1).weight_over = 1;
%            plan.VOI(1).max_DVH = '72 5%';
%            plan.VOI(1).weight_under = 50;
%            plan.VOI(2).name = 'Spinal Cord';
%            plan.VOI(2).max_DVH = '20 0%';
%            plan.VOI(2).weight_over = 1;
%            writePln('sample_plan.pln', plan);
%
%    Other possible properties include
%            plan.VOI(1).color = '1 0.5 0'; 
%            plan.VOI(1).EUD_p = -5;
%            plan.VOI(2).EUD_p = 5;
%
% plan is a struct with the following fields:
%   plan.plan_title       - Plan title
%   plan.plan_description - Plan description
%   plan.plan_file_root   - Root of Konrad formatted files
%   plan.nInstances  - Number of instances of geometry (a.k.a. time slices)
%   plan.nBeams      - Number of beams
%   plan.VOI        - 

% First open the file for writing
fid = fopen(plan_file_name,'wt');

% Write out all paramaters for VOI
for param = fieldnames(plan)'

    % Convert param from a cell to a string
    param = param{:};
    
    if(~strcmp(param,'VOI'))
	value = plan.(param);
	if(~isempty(value))
	    % Make sure value is a string
	    if(isa(value,'double'))
		value = num2str(value);
	    end
	    fprintf(fid, '%s %s\n', param, value);
	else
	    fprintf(fid, '%s\n', param);

	end
    end
end

% Write VOI paramaters
if(isfield(plan, 'VOI'))
    for(iVoi = 1:length(plan.VOI))
        for param = fieldnames(plan.VOI)'

            % Convert param from a cell to a string
            param = param{:};
            
            if(~isempty(plan.VOI(iVoi).(param)))

                % Make sure paramaters is a string
                if(isstr(plan.VOI(iVoi).(param)))
                    value = plan.VOI(iVoi).(param);
                    fprintf(fid, 'VOI %d %s %s\n', iVoi, param, value);
                elseif(isa(plan.VOI(iVoi).(param),'double'))
                    value = num2str(plan.VOI(iVoi).(param));
                    fprintf(fid, 'VOI %d %s %s\n', iVoi, param, value);
                elseif(isa(plan.VOI(iVoi).(param), 'logical'))
                    if(plan.VOI(iVoi).(param))
                        value = 'true';
                    else
                        value = 'false';
                    end
                    fprintf(fid, 'VOI %d %s %s\n', iVoi, param, value);
                elseif(isstruct(plan.VOI(iVoi).(param)))
                    if(strcmp(param, 'min_DVH'))
                        for(iConstraint = 1:numel(plan.VOI(iVoi).min_DVH.dose))
                            value = num2str(...
                              [plan.VOI(iVoi).min_DVH.dose(iConstraint), ...
                               plan.VOI(iVoi).min_DVH.volume(iConstraint)]);
                            fprintf(fid, 'VOI %d %s %s\n', iVoi, param, value);
                        end
                    elseif(strcmp(param, 'max_DVH'))
                        for(iConstraint = 1:numel(plan.VOI(iVoi).max_DVH.dose))
                            value = num2str(...
                              [plan.VOI(iVoi).max_DVH.dose(iConstraint), ...
                               plan.VOI(iVoi).max_DVH.volume(iConstraint)]);
                            fprintf(fid, 'VOI %d %s %s\n', iVoi, param, value);
                        end
                    else
                        error(['Problem with voi field ' param]);
                    end
                else
                    error(['Problem with voi field ' param]);
                end
            end
        end
    end
end

fclose(fid);
