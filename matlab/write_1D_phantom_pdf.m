function flag = write_1D_phantom_pdf(file_root,nInstances,nModes,sigmas,nSamples)
%write_1D_phantom Creates Konrad files for a 1-dimensional phantom
%
%    write_1D_phantom(file_root,nInstances,nModes,nSamples) creates
%    Creates .pdf file and sample_Pdfs.dat/.eps file
%
%    the modes are cos functions: phi_i = cos(nMode*pi*i/nInstances)
%    where nMode is an integer <= nModes
%
%    parameters:
%    sigmas = vector of amplitudes for each mode
%    nModes = number of perturbation modes
%    nInstances = number of positions
%    nSamples = number of Pdf samples to be generated

% ------------------------------------------------------------------- %
% generate Pdf
Pdf = zeros(nInstances,1);
Modes = zeros(nModes,nInstances);
Sigmas = sigmas;

shape = 2;
outFile = [file_root 'phantom.pdf'];
pdfnorm = 0;

for iInstance=1:nInstances
        
    norm_pos = (iInstance-0.5)/nInstances;
    
    % lujan Pdf
    %    Pdf(iInstance) = (1.0/(pi*shape*nInstances)) / ( norm_pos^((2*shape-1)/(2*shape)) ...
    %        * sqrt(1-norm_pos^(1/shape)) );

    % cosine Pdf
    Pdf(iInstance) = 4.0 + cos(2*pi*norm_pos);

    % normalization
    pdfnorm = pdfnorm + Pdf(iInstance);

    % cosine modes
    for iMode=1:nModes
        Modes(iMode,iInstance) = cos(iMode*pi*norm_pos);
    end
end

% normalize
for iInstance=1:nInstances
    Pdf(iInstance) = Pdf(iInstance) / pdfnorm;
    for iMode=1:nModes
        % scale modes according to the absolute value of the pdf
        Modes(iMode,iInstance) = Modes(iMode,iInstance) * Pdf(1);
    end
end


% ------------------------------------------------------------------- %
% write Pdf file

% open file
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
    error(['Error writing pdf file: ' message]);
end

% header
fprintf(outfid,'number_of_instances %d\n',nInstances);
fprintf(outfid,'number_of_modes %d\n',nModes);

% sigmas
fprintf(outfid,'sigmas:\n');
for iMode=1:nModes
    
    fprintf(outfid,'%d %f\n',iMode, Sigmas(iMode));
    
end

% pdf and modes
fprintf(outfid,'pdf:\n');
for iInstance=1:nInstances

    fprintf(outfid,'%d %f ',iInstance, Pdf(iInstance));
    
    for iMode=1:nModes

        fprintf(outfid,'%f ',Modes(iMode,iInstance));

    end

    fprintf(outfid, '\n');
    
end

% Close file
status = fclose(outfid);
if status ~= 0
    error(['Error writing pdf file']);
end


% ------------------------------------------------------------------- %
% generate sample Pdfs
sample_pdfs = zeros(nSamples,nInstances);

for iSample=1:nSamples

    is_valid = 0;
    while is_valid==0

        % set Pdf values
        temp_pdf = Pdf';
        for iMode=1:nModes
            temp_pdf = temp_pdf + Sigmas(iMode) * randn() * Modes(iMode,:);
        end

        % check whether Pdf is positive definite
        if any(temp_pdf < 0)
            is_valid = 0
        else
            is_valid = 1;
        end
    end
    
    sample_pdfs(iSample,:) = temp_pdf;
end

% open file
outFile = [file_root 'sample_Pdfs.dat'];
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
    error(['Error writing pdf file: ' message]);
end

% write
for iInstance=1:nInstances

    for iSample=1:nSamples
        fprintf(outfid,'%f ', sample_pdfs(iSample,iInstance));
    end

    fprintf(outfid, '\n');
end

% Close file
status = fclose(outfid);
if status ~= 0
    error(['Error writing pdf file']);
end

% plot sample pdfs
outfile = [file_root 'sample_Pdfs'];
figure
plot(sample_pdfs');
print ( '-depsc',outfile );
print ( '-djpeg',outfile );

