function flag = writeKonradFiles(outFile, structNums, beamWeights,...
                                 beamletThreshold)
% writeKonradFiles  Writes Konrad formated files from CERR structures
%     [flag] = writeKonradFiles(outFile, structNums, beamWeights, beamletThreshold)
%     writes out Konrad files for the current plan using the values stored in
%     memory.  Creates .dij, .stf, .bwf, .dif, .voi, and .voi2 files.
%
%     outFile is the path and name for the Konrad files.
%
%     structNums is an array of structure numbers to include in the Konrad
%     files.  Any voxels outside of the structures will be assumed to be air.
%     The first voxels will take priority.  Use structNums = [] to include all
%     structures that have a goal defined.
%
%     beamWeights is an array containing beam weights to use for the start of
%     optimization.  This should be a vector the same length as the number of
%     beamlets.  Use beamWeights = [] to set all weights to 1.
%
%     beamletThreshold is a value between 0 and 1 to only allow beamlets with at
%     least one Dij entry greater than beamletThreshold times the maximum Dij
%     entry.
%
%     flag returns 'error' or 'no error'.
%
%     For example,
%
%         [flag] = writeKonradFiles(IM, 'default')
%
%     creates files named default.voi, default.dif, default_1.dij, default_2.dij, etc.
%
%     Notes:
%     * There are many non-essential values that were given arbitrary defaults.
%       This shouldn't be a problem for optimization, but better values are
%       obviously prefered.
%
%     * Note that writing out multiple beams will only work if the
%       IM.beams(beam).beamNumber is set correctly.

global planC; 
global stateS;
indexS = planC{end};
IM = planC{indexS.IM}.IMDosimetry;

% ------------------------------------------------------------------- %
% If structNums = [], find structures with a goal specified.
% Assumes that goal 1 is more important than goal 2 and so on.
if(~exist('structNums') || isempty(structNums))
%Tarek Modification
        structNums = [IM.goals(:).structNum];
end

% ------------------------------------------------------------------- %
% If beamWeights = [], use all beam weights equal to 1
if(~exist('beamWeights') || isempty(beamWeights))
        beamWeights = ones(size(IM.beamlets,2),1);
end

% ------------------------------------------------------------------- %
% If beamletThreshold = [], use all beamlets
if(~exist('beamletThreshold') || isempty(beamletThreshold))
        beamletThreshold = 0;
else
    beamletThreshold = beamletThreshold * (2^15-1);
end

% ------------------------------------------------------------------- %
% Make sure all structures use the same sampling rate.
sampleRate = IM.beamlets(structNums(1),1).sampleRate;
for count = 2:length(structNums)
    if (sampleRate ~= IM.beamlets(structNums(count),1).sampleRate)
        error('Sample rates must be same in all strucures for Konrad output');
    end
end

% ------------------------------------------------------------------- %
% Make sure all beams have beamNum assigned correctly.
% This is probably not the best way to handle this.
for beam = 1:length(IM.beams)
    if (IM.beams(beam).beamNum ~= beam)
        error('Beamnumber must be assigned when using multiple beams');
    end
end

% ------------------------------------------------------------------- %
% First combine volume of interest and influence matrix information
% for all structures (downsampled in row and column by sampleRate)

% Combining the information takes a very long time, so display a
% status bar with the expected time to go
h = waitbar(0, 'Finding Points in Voi', 'name', 'Writing Konrad Files');
pause(0);
startTime = clock;



% Get all points in the last structure
mask3M = getUniformStr(structNums(1));

% Get sampling Matrix
maskSample3D = getDown3Mask(true(size(mask3M)), sampleRate, 1);

% Find full to sampled lookup table
sampled_index_to_full_index = find(maskSample3D);
full_index_to_sampled_index = zeros(numel(maskSample3D), 1);
full_index_to_sampled_index(sampled_index_to_full_index) = ...
    [1:numel(sampled_index_to_full_index)];

% Find dimensions of sampled
sampled_nR = max(max(sum(maskSample3D,1)));
sampled_nC = max(max(sum(maskSample3D,2)));
sampled_nS = max(max(sum(maskSample3D,3)));

% Store voxels in array
DijStruct.voiData{structNums(1)} = full_index_to_sampled_index( ...
    find(maskSample3D & mask3M));

% Find information about the rest of the structures and combine
for structCount = 2:length(structNums)
        
    % Update status bar
    waitbar( structCount / length(structNums), h,...
                ['Finding Points in Voi (~'...
                num2str(floor(etime(clock,startTime)*...
                (length(structNums)-structCount)/structCount))...
                's to go)']); pause(0);
    
    % Get all voxels in the structure (and downsample)
    DijStruct.voiData{structNums(structCount)} =...
        full_index_to_sampled_index( ...
            find(getUniformStr(structNums(structCount)) & maskSample3D));
end

  
% Find the influence matrix for the problem
waitbar( 1, h,'Now Finding Influence Matrix'); pause(0);
inflM = getGlobalInfluenceM(IM, structNums);

% ------------------------------------------------------------------- %
% Reduce voi by sampling




% ------------------------------------------------------------------- %
% Find general information about the problem that is the same for all
% beams

    % Read dimensions from voiField (according to Konrad coordinates)
    [nR,nC,nS] = size(mask3M);

    % Voxel dimensions in the dose cube
    DijStruct.voxeldR  = sampleRate * ...
    planC{indexS.scan}.uniformScanInfo.grid1Units;
    DijStruct.voxeldC  = sampleRate * ... 
    planC{indexS.scan}.uniformScanInfo.grid2Units;
    DijStruct.voxeldS  = sampleRate * ... 
    planC{indexS.scan}.uniformScanInfo.sliceThickness;

    % Dose cube dimensions
    DijStruct.doseCubeNr  = sampled_nR;
    DijStruct.doseCubeNc  = sampled_nC;
    DijStruct.doseCubeNs  = sampled_nS;

    % ISO index in dose cube
    DijStruct.doseIsoIndexR  = 1;
    DijStruct.doseIsoIndexC  = 1;
    DijStruct.doseIsoIndexS  = 1;

    % CT cube dimensions
    DijStruct.ctNr = nR;
    DijStruct.ctNc = nC;
    DijStruct.ctNs = nS;

    % ISO index in CT cube
    DijStruct.ctIndexR  = 1;
    DijStruct.ctIndexC  = 1;
    DijStruct.ctIndexS  = 1;

% ------------------------------------------------------------------- %
% Write general files (dif and voi)

% Write the dif file out
try
    waitbar( 1, h,'Writing dif file'); pause(0);
    writeDif(DijStruct, [outFile '.dif']);
catch
    warning(['Error writing dif file: ' outFile '.dif']);
    flag = 'error';
    return
end

% Write the voi2 file out
try
    waitbar( 1, h,'Writing voi2 file'); pause(0);
    writeVoi2(DijStruct, [outFile '.voi2']);
catch
    warning(['Error writing voi2 file: ' outFile '.voi2']);
    flag = 'error';
    return
end

% Write the voi file out
try
    waitbar( 1, h,'Writing voi file'); pause(0);
    writeVoi(DijStruct, structNums, [outFile '.voi']);
catch
    warning(['Error writing voi file: ' outFile '.voi']);
    flag = 'error';
    return
end

% ------------------------------------------------------------------- %
% Create files for each beam

%Vector telling which beam a beamlet is part of.
for i = 1:size(IM.beamlets,2)
    beamsV(i) = max([0, IM.beamlets(:,i).beamNum]);
end

% Create files for each beam
for beam = 1:length(IM.beams)
    % First find information about beam
    % Note: Since many of these values are not critical to optimization,
    %       they have been made up.

    %
    % Important information for optimization
    %

    % Beam number
    DijStruct.beamNumber      = IM.beams(beam).beamNum;

    % Number of pencil beams
    DijStruct.numPencilBeams  = length(IM.beams(beam).xPBPosV);


    %
    % Unimportant information.  These should be verified, but they are
    % good enough for now
    %
    
    DijStruct.machineName   = IM.beams(beam).beamModality;

    DijStruct.gantryAngle   = IM.beams(beam).gantryAngle;

    if(isempty(IM.beams(beam).collimatorAngle))
        DijStruct.collimatorAngle  = 0.0;
    else
        DijStruct.collimatorAngle  = IM.beams(beam).collimatorAngle;
    end

    % Is tableAngle the same as couch angle? arc angle?
    DijStruct.tableAngle          = 0.0;

    % Bixel dimensions
    DijStruct.spotdx      = IM.beams(beam).beamletDelta_x;
    DijStruct.spotdy      = IM.beams(beam).beamletDelta_y;

    %
    % Preallocate information about beamlets
    %

    % Beamlet Header
    DijStruct.energy = IM.beams(beam).beamEnergy ...
        * ones(DijStruct.numPencilBeams, 1);
    DijStruct.spotX  = IM.beams(beam).xPBPosV';
    DijStruct.spotY  = IM.beams(beam).yPBPosV';

    % Beamlet Data
    DijStruct.index = cell(DijStruct.numPencilBeams,1);
    DijStruct.value = cell(DijStruct.numPencilBeams,1);
    DijStruct.numVoxels = zeros(DijStruct.numPencilBeams, 1);

    % Calculate optimal dose coefficient
    DijStruct.doseCoefficient = full(max(max(inflM))) / (2^15 - 1);

    %
    % Prepare influence data for each beamlet in beam
    %

    % PBNum holds the CERR beamlet number while beamletNum holds the beamlet
    % number within the beam
    PBNum = find(beamsV == beam);
    for beamletNum = 1:length(PBNum)

        % Find voxels with non-zero contributions from this pencil beam
        [DijStruct.index{beamletNum}, dummy, DijStruct.value{beamletNum}]...
        = find(inflM(:,PBNum(beamletNum)));

        % Replace voxel index by sampled voxel index
        DijStruct.index{beamletNum} = ...
        full_index_to_sampled_index(DijStruct.index{beamletNum});

        % Convert dose value to short integer
        DijStruct.value{beamletNum} = uint16(DijStruct.value{beamletNum}...
            ./ DijStruct.doseCoefficient);

        % Store number of voxels
        DijStruct.numVoxels(beamletNum) = size(DijStruct.index{beamletNum},1);

        DijStruct.beamWeight(beamletNum) = beamWeights(PBNum(beamletNum));

        % Throw away unimportant bixels
        if(beamWeights(PBNum(beamletNum)) == 0 || beamletThreshold > 0 ...  && 
            max(DijStruct.value{beamletNum}) < beamletThreshold)

            DijStruct.value{beamletNum} = [];
            DijStruct.index{beamletNum} = [];
            DijStruct.numVoxels(beamletNum) = 
            size(DijStruct.index{beamletNum},1);
            DijStruct.beamWeight(beamletNum) = 0;
        end
    end

    % Now write the dij file out for this beam
    try
        waitbar( 1, h,'Writing dij file'); pause(0);
        writeDij(DijStruct, [outFile '_' num2str(DijStruct.beamNumber) ...
            '.dij']);
    catch
        warning(['Error writing dij file: ' ...
        outFile '_' num2str(DijStruct.beamNumber) '.dij']);
        flag = 'error';
        return
    end

    % Now write the stf file out for this beam
    try
        waitbar( 1, h,'Writing stf file'); pause(0);
        writeStf(DijStruct, [outFile '_' num2str(DijStruct.beamNumber) 
        '.stf']);
    catch
        warning(['Error writing stf file: ' ...
            outFile '_' num2str(DijStruct.beamNumber) '.stf']);
        flag = 'error';
        return
    end

    % Now write the bwf file out for this beam
    try
        waitbar( 1, h,'Writing bwf file'); pause(0);
        write_bwf([outFile '_' num2str(DijStruct.beamNumber) '.bwf'], ...
                DijStruct);
    catch
        warning(['Error writing bwf file: ' ...
            outFile '_' num2str(DijStruct.beamNumber) '.bwf']);
        flag = 'error';
        return
    end

end

% Close status bar
close(h); pause(0);

flag = 'no error';
