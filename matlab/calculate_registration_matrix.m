function [R] = calculate_registration_matrix(shifts, dif)
%calculate_registration_matrix  Calculates a rigid deformation matrix
%
%  [R] = calculate_registration_matrix(shifts, dif) calculates a sparse
%  deformation matrix for a given set of shifts and geometry.  The shifts are
%  passed in as a 3xn matrix where each column corresponds to one shift in
%  the Konrad coordinate system.  The created R matrix will be in terms of the
%  CERR row, column, slice coordinate system.
%
%   The registration matrix is the matrix R such that D_shifted = R * D_static
%
%  See help on read_dif.m for the dif format.

start_time = cputime;

nShifts = size(shifts, 2);
nVoxels = dif.nZ * dif.nX * dif.nY;

% static_position = [(column - dif.isoZ) * dif.dZ;
%                    (row    - dif.isoX) * dif.dX;
%                    (slice  - dif.isoY) * dif.dY];


% Convert the shifts to row, column, slice coordinates
rcs_shifts = [shifts(3,:)./dif.dZ; shifts(1,:)./dif.dX; shifts(2,:)./dif.dY];

% Find the contribution from each of the corners of the 8-voxel cube with the
% lower corner located at floor(rcs_shifts)

fraction = ones(8, nShifts);
% Shift in row direction
fraction([1,3,5,7],:) = ones(4,1)*(1-rcs_shifts(1,:)+floor(rcs_shifts(1,:)))...
                        .* fraction([1,3,5,7],:);
fraction([2,4,6,8],:) = ones(4,1)*(rcs_shifts(1,:) - floor(rcs_shifts(1,:)))...
                        .* fraction([2,4,6,8],:);
% Shift in column direction
fraction([1,2,5,6],:) = ones(4,1)*(1-rcs_shifts(2,:)+floor(rcs_shifts(2,:)))...
                        .* fraction([1,2,5,6],:);
fraction([3,4,7,8],:) = ones(4,1)*(rcs_shifts(2,:) - floor(rcs_shifts(2,:)))...
                        .* fraction([3,4,7,8],:);
% Shift in slice direction
fraction([1,2,3,4],:) = ones(4,1)*(1-rcs_shifts(3,:)+floor(rcs_shifts(3,:)))...
                        .* fraction([1,2,3,4],:);
fraction([5,6,7,8],:) = ones(4,1)*(rcs_shifts(3,:) - floor(rcs_shifts(3,:)))...
                        .* fraction([5,6,7,8],:);

fraction_shifts = [0 1 0 1 0 1 0 1;
                   0 0 1 1 0 0 1 1;
                   0 0 0 0 1 1 1 1];


% Combine all shifts into one weighted list
weighted_rcs_shifts = zeros(3,nShifts * 8);
weights = zeros(1,nShifts * 8);

for(iShift = 1:nShifts)
    weights(1,8*(iShift-1) + [1:8]) = fraction(:,iShift)';
    weighted_rcs_shifts(:,8*(iShift-1) + [1:8]) = ...
        floor(rcs_shifts(:,iShift)) * ones(1,8) + fraction_shifts;
end



% Sort shifts to speed process
% [weighted_rcs_shifts,I] = sortrows(weighted_rcs_shifts');
% [weighted_rcs_shifts,I] = sortrows(weighted_rcs_shifts', [3 2 1]);
% weighted_rcs_shifts = weighted_rcs_shifts';
% weights = weights(I);

disp(['Shifts figured out: ' num2str(cputime - start_time)]);

% Allocate sparse R matrix (bigger than neccessary
i = ones(nVoxels * 8 * nShifts,1);
j = ones(nVoxels * 8 * nShifts,1);
s = zeros(nVoxels * 8 * nShifts,1);
% R_T = sparse([],[],[],nVoxels,nVoxels,nVoxels * 8 * nShifts);

disp(['R allocated: ' num2str(cputime - start_time)]);

% Find row, column, and slice for each voxel
% [row, column, slice] = ind2sub([dif.nZ, dif.nX, dif.nY], [1:nVoxels]);

% h = waitbar(0,'Calculating R');

% Add each row to sparse matrix
iVoxel = 0;
sparse_entries = 0;
for iSlice = 1:dif.nY
    s_mask = (weighted_rcs_shifts(3,:) + iSlice  >= 1) ...
           & (weighted_rcs_shifts(3,:) + iSlice  <= dif.nY);
    for iColumn = 1:dif.nX
        c_mask = s_mask & (weighted_rcs_shifts(2,:) + iColumn >= 1) ...
               & (weighted_rcs_shifts(2,:) + iColumn <= dif.nX);
        for iRow = 1:dif.nZ
            mask = c_mask & (weighted_rcs_shifts(1,:) + iRow >= 1) ...
                   & (weighted_rcs_shifts(1,:) + iRow <= dif.nZ);

            iVoxel = iVoxel + 1;

            if(any(mask))
                % Originally:
                % temp_j = sub2ind([dif.nZ, dif.nX, dif.nY], ...
                %             weighted_rcs_shifts(1,mask) + iRow, ...
                %             weighted_rcs_shifts(2,mask) + iColumn, ...
                %             weighted_rcs_shifts(3,mask) + iSlice);
                % Replacement:
                temp_j = (weighted_rcs_shifts(2,mask) + iColumn) ...
                    + dif.nX * (weighted_rcs_shifts(1,mask) + iRow - 1 ...
                    + dif.nZ * (weighted_rcs_shifts(3,mask) + iSlice - 1));


                temp_s = weights(mask)';
                [temp_j, ind] = sort(temp_j);
                temp_s = temp_s(ind);
                temp_sparse_entries = numel(temp_j);

                i(sparse_entries + [1:temp_sparse_entries]) = iVoxel;
                j(sparse_entries + [1:temp_sparse_entries]) = temp_j;
                s(sparse_entries + [1:temp_sparse_entries]) = temp_s;
                sparse_entries = sparse_entries + temp_sparse_entries;
            end
            
            % R_T(sub2ind([dif.nZ, dif.nX, dif.nY], ...
            %             weighted_rcs_shifts(1,mask) + iRow, ...
            %             weighted_rcs_shifts(2,mask) + iColumn, ...
            %             weighted_rcs_shifts(3,mask) + iSlice), iVoxel) ...
            %     = weights(mask)';
        end
    end
    % waitbar(iSlice / dif.nY,h);
end
% close(h);

disp(['R populated: ' num2str(cputime - start_time)]);

% Allocate transpose of R
R_T = sparse(j,i,s,nVoxels,nVoxels);

% Shrink R
[j,i,s] = find(R_T);
[m,n] = size(R_T);
R = sparse(i,j,s,n,m);

disp(['R converted to sparse: ' num2str(cputime - start_time)]);
return
