function [dose] = convolve_dose(static_dose, shifts, dif, weight)
%convolve_dose  Calculates a convolved dose distribution
%
%  [dose] = convolve_dose(static_dose, shifts, weight dif) calculates the
%  convolved dose distribution resulting from the shifts given.  static_dose
%  can be either a vector or a 3D matrix in row-column-slice order.  shifts is
%  a 3xn array with each column corresponding to one shift.

%  [R] = calculate_registration_matrix(shifts, dif) calculates a sparse
%  deformation matrix for a given set of shifts and geometry.  The shifts are
%  passed in as a 3xn matrix where each column corresponds to one shift in
%  the Konrad coordinate system.  The created R matrix will be in terms of the
%  CERR row, column, slice coordinate system.
%
%
%   The registration matrix is the matrix R such that D_shifted = R * D_static
%
%  See help on read_dif.m for the dif format.

start_time = cputime;

nShifts = size(shifts, 2);
nVoxels = dif.nZ * dif.nX * dif.nY;

if(~exist('weight'));
    weight = ones(1,nshifts);
end
if(issparse(static_dose))
    static_dose = full(static_dose);
end

if(size(static_dose,1) == nVoxels)
    % Dose is a vector, convert to 3D matrix
    static_dose = reshape(static_dose, dif.nZ, dif.nX, dif.nY);
elseif(size(static_dose) ~= [dif.nZ, dif.nX, dif.nY])
    error('Dose is not shaped in row, column, slice order');
end

% Find shifts in voxel space
rcs_shifts = diag([1/dif.dZ, 1/dif.dX, 1/dif.dY]) * shifts;

% Find the contribution from each of the corners of the 8-voxel cube with the
% lower corner located at floor(rcs_shifts)

fraction = ones(8, nShifts);
% Shift in row direction
fraction([1,3,5,7],:) = ones(4,1)*(1-rcs_shifts(1,:)+floor(rcs_shifts(1,:)))...
                        .* fraction([1,3,5,7],:);
fraction([2,4,6,8],:) = ones(4,1)*(rcs_shifts(1,:) - floor(rcs_shifts(1,:)))...
                        .* fraction([2,4,6,8],:);
% Shift in column direction
fraction([1,2,5,6],:) = ones(4,1)*(1-rcs_shifts(2,:)+floor(rcs_shifts(2,:)))...
                        .* fraction([1,2,5,6],:);
fraction([3,4,7,8],:) = ones(4,1)*(rcs_shifts(2,:) - floor(rcs_shifts(2,:)))...
                        .* fraction([3,4,7,8],:);
% Shift in slice direction
fraction([1,2,3,4],:) = ones(4,1)*(1-rcs_shifts(3,:)+floor(rcs_shifts(3,:)))...
                        .* fraction([1,2,3,4],:);
fraction([5,6,7,8],:) = ones(4,1)*(rcs_shifts(3,:) - floor(rcs_shifts(3,:)))...
                        .* fraction([5,6,7,8],:);

fraction_shifts = [0 1 0 1 0 1 0 1;
                   0 0 1 1 0 0 1 1;
                   0 0 0 0 1 1 1 1];

% Combine all shifts into one weighted list
weighted_rcs_shifts = zeros(3,nShifts * 8);
weights = zeros(1,nShifts * 8);

for(iShift = 1:nShifts)
    weights(1,8*(iShift-1) + [1:8]) = weight(iShift) * fraction(:,iShift)';
    weighted_rcs_shifts(1:3,8*(iShift-1) + [1:8]) = ...
        floor(rcs_shifts(:,iShift)) * ones(1,8) + fraction_shifts;
end

% Create convolution kernal

% Determine shift necessary in kernal to center it
offset = max(abs(weighted_rcs_shifts), [], 2);

kernal = zeros(offset' .* 2 + 1);

% Fill kernal
for(i = 1:nShifts * 8)
    kernal(weighted_rcs_shifts(1,i) + offset(1)+1,...
        weighted_rcs_shifts(2,i) + offset(2)+1,...
        weighted_rcs_shifts(3,i) + offset(3)+1) = weights(i) ...
        + kernal(weighted_rcs_shifts(1,i) + offset(1)+1,...
            weighted_rcs_shifts(2,i) + offset(2)+1,...
            weighted_rcs_shifts(3,i) + offset(3)+1);
end

dose = convn(static_dose, kernal, 'same');
return
