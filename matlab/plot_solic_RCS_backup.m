function plot_solid_RCS( rcs_data, rows, columns, slices, cmap)
% plot_solid_RCS( rcs_data, rows, columns, slices, cmap)
%
% Shows selected rows, columns, and slices as a solid.
% 

[nR, nC, nS] = size(rcs_data);

if(isempty(rows))
    rows = 1:nR;
end

if(isempty(columns))
    columns = 1:nC;
end

if(isempty(slices))
    slices = 1:nS;
end

if(isempty(cmap))
    cmap = gray(256);
end

clf; 
set(gca,'color','none');
hold on;

temp_axial = rcs_data(rows,columns,slices(1));
x=[1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
y=slices(1) * ones(2,3);
z = [rows(1);rows(end)] * [1 1 1];
warp(x,y,z,temp_axial,cmap);

temp_coronal = rcs_data(rows(end),columns,slices);
temp_coronal = squeeze(permute(temp_coronal,[3,1,2]));
x=[1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
y= [slices(1);slices(end)] * [1 1 1];
z = rows(end) * ones(2,3);
warp(x,y,z,temp_coronal,cmap);

temp_sagital = rcs_data(rows(end):-1:rows(1),columns(end),slices);
temp_sagital = squeeze(permute(temp_sagital,[3,2,1]));
x = columns(end) * ones(2,3);
y = [slices(1);slices(end)] * [1 1 1];
z = [1;1]*[rows(end) (rows(end)+rows(1))/2  rows(1)];
warp(x,y,z,temp_sagital,cmap);

% Draw outline of dose cube
x = [1,1,nC,nC,1,1,nC,nC,nC,nC,nC]';
y = [1,nS,nS,1,1,1,1,1,nS,nS,1]';
z = [nR,nR,nR,nR,nR,1,1,nR,nR,1,1]';
line(x,y,z);

xlabel('columns');
ylabel('slices');
zlabel('rows');
default_view = [  4.4029    0.3061         0   -2.3545
                 -0.7537    0.5068    3.9152   -1.8341
                 -1.1986    0.8059   -2.4620   34.5111
                  0         0         0         1.0000];

view(default_view); set(gca,'ydir','normal');
axis equal;


hold off;
return
