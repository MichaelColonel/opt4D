function [step_size, objective] = find_approximately_optimal_stepsize( ...
    d, d_direction, d_min, d_max, q_under, q_over, first_guess)
%[step_size, objective] = find_approximately_optimal_stepsize( ...
%         d, d_direction, d_min, d_max, q_under, q_over, first_guess)
% find optimal stepsize

alpha_min = 0;
F_alpha_min = calculate_objective(d, d_min, d_max, q_under, q_over);

alpha = alpha_min;
F = F_alpha_min;

alpha_temp = first_guess;
% Calculate F(alpha_temp)
d_temp = d + alpha_temp * d_direction;
F_alpha_temp = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

alpha(end+1) = alpha_temp;
F(end+1) = F_alpha_temp;

if(F_alpha_temp > F_alpha_min)
    % The alpha_max is valid, so we need to find alpha_mid
    alpha_max = alpha_temp;
    F_alpha_max = F_alpha_temp;

    alpha_mid = mean([alpha_min, alpha_max]);
    % Calculate F(alpha_mid)
    d_temp = d + alpha_mid * d_direction;
    F_alpha_mid = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

    alpha(end+1) = alpha_mid;
    F(end+1) = F_alpha_mid;

    while(F_alpha_mid > F_alpha_min)
        alpha_max = alpha_mid;
        F_alpha_max = F_alpha_mid;

        alpha_mid = mean([alpha_min, alpha_max]);
        % Calculate F(alpha_temp)
        d_temp = d + alpha_mid * d_direction;
        F_alpha_mid = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

        alpha(end+1) = alpha_mid;
        F(end+1) = F_alpha_mid;
    end

else
    % We need to find alpha_max
    alpha_mid = alpha_temp;
    F_alpha_mid = F_alpha_temp;

    alpha_max = 2 * alpha_mid;
    % Calculate F(alpha_max)
    d_temp = d + alpha_max * d_direction;
    F_alpha_max = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

    alpha(end+1) = alpha_max;
    F(end+1) = F_alpha_max;

    while(F_alpha_mid > F_alpha_max)
        alpha_mid = alpha_max;
        F_alpha_mid = F_alpha_max;

        alpha_max = 2 * alpha_mid;
        % Calculate F(alpha_max)
        d_temp = d + alpha_max * d_direction;
        F_alpha_max = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

        alpha(end+1) = alpha_max;
        F(end+1) = F_alpha_max;
    end
end

% The following conditions are now true:
% alpha_mid < alpha_mid < alpha_max
% F_alpha_min > F_alpha_mid
% F_alpha_max > F_alpha_mid

if(alpha_mid > alpha_max || alpha_mid < alpha_min)
    alpha_min
    alpha_mid
    alpha_max
    keyboard
end

if(F_alpha_mid > F_alpha_max || F_alpha_mid > F_alpha_min)
    F_alpha_min
    F_alpha_mid
    F_alpha_max
    keyboard
end


for(i = 1:5)
    % Place alpha_temp at the minimum of the quadratic polynomial that goes
    % through alpha_min, alpha_mid, and alpha_max
    % (from _Nonlinear Programming_ by Dimitry P. Bertsekas, pg. 725)
    alpha_temp = 0.5 * (F_alpha_min * (alpha_max^2 - alpha_mid^2) ...
        + F_alpha_mid * (alpha_min^2 - alpha_max^2) ...
        + F_alpha_max * (alpha_mid^2 - alpha_min^2)) ...
        / (F_alpha_min * (alpha_max - alpha_mid) ...
        + F_alpha_mid * (alpha_min - alpha_max) ...
        + F_alpha_max * (alpha_mid - alpha_min));

    if(alpha_temp >= alpha_max || alpha_temp <= alpha_min ...
            || alpha_temp == alpha_mid)
        % The algorithm has broken down, so go with the best alpha found
        % thus far.  This typically is a rounding error in the above
        % function when alpha_min is very close to either alpha_max or
        % alpha_min.
        break;
    end

    % Calculate F(alpha_temp)
    d_temp = d + alpha_temp * d_direction;
    F_alpha_temp = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

    alpha(end+1) = alpha_temp;
    F(end+1) = F_alpha_temp;

    if(alpha_temp > alpha_mid)
        if(F_alpha_temp < F_alpha_mid)
            alpha_min = alpha_mid;
            F_alpha_min = F_alpha_mid;

            alpha_mid = alpha_temp;
            F_alpha_mid = F_alpha_temp;
        elseif(F_alpha_temp > F_alpha_mid)
            alpha_max = alpha_temp;
            F_alpha_max = F_alpha_temp;
        else
            disp('(F_alpha_temp == F_alpha_mid)');
            break;
        end
    elseif(alpha_temp < alpha_mid)
        if(F_alpha_temp < F_alpha_mid)
            alpha_max = alpha_mid;
            F_alpha_max = F_alpha_mid;

            alpha_mid = alpha_temp;
            F_alpha_mid = F_alpha_temp;
        elseif(F_alpha_temp > F_alpha_mid)
            alpha_min = alpha_temp;
            F_alpha_min = F_alpha_temp;
        else
            disp('(F_alpha_temp == F_alpha_mid)');
            break;
        end
    else
        disp('alpha_temp == alpha_mid');
        break;
    end
end

step_size = alpha_mid;
objective = F_alpha_mid;

global DEBUG;
if(DEBUG)
    for(alpha_temp = [1:10] * max(alpha) / 10)
        % Calculate F(alpha_temp)
        d_temp = d + alpha_temp * d_direction;
        F_alpha_temp = calculate_objective(d_temp, d_min, d_max, q_under, q_over);

        alpha(end+1) = alpha_temp;
        F(end+1) = F_alpha_temp;
    end
end


% figure(fig);
subplot(1,3,3);
plot(alpha,F,':',alpha,F,'x',alpha_mid,F_alpha_mid,'o');
title('Finding the optimal alpha');
ylabel('Objective(\alpha)');
xlabel('\alpha');

drawnow
    
return
