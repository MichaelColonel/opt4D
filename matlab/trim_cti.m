function Dataout = trimcti(Data,nslice,lim1,lim2)
%   flip bytes in CT data
if nargin<2
    nslice = 1;
end

pixsize = 1;%Data(end,end,end);
tmp = Data(:,:,nslice);
[xsize,ysize] = size(tmp);

tmp = permute(fliplr(tmp),[2 1]);

% this flips the two bytes of the int*16 Hounsfield number
tmp(tmp<0) = tmp(tmp<0)+2^16;
tmp2 = floor(tmp/2^8);
tmp = tmp - tmp2*2^8;
tmp = tmp*2^8+tmp2;
tmp(tmp>=2^15) = tmp(tmp>=2^15)-2^16;

tmp(tmp>30000) = -1024;     % get rid of the blank aperture space

if nargin<3 | lim1 >= lim2
    lim1 = -1024;
    lim2 = 1023;
end
tmp(tmp>lim2) = lim2;
tmp(tmp<lim1) = lim1;

while 0
[xx,yy] = meshgrid(0:pixsize:(xsize-1)*pixsize,0:pixsize:(ysize-1)*pixsize);
surface(xx,yy,tmp,'EdgeColor','none')
colormap(bone)
colorbar
end

Dataout = tmp;
