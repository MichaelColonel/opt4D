function varargout = call_opt4D(varargin)
% CALL_OPT4D M-file for call_opt4D.fig
%      CALL_OPT4D, by itself, creates a new CALL_OPT4D or raises the existing
%      singleton*.
%
%      H = CALL_OPT4D returns the handle to a new CALL_OPT4D or the handle to
%      the existing singleton*.
%
%      CALL_OPT4D('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CALL_OPT4D.M with the given input arguments.
%
%      CALL_OPT4D('Property','Value',...) creates a new CALL_OPT4D or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before call_opt4D_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to call_opt4D_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help call_opt4D

% Last Modified by GUIDE v2.5 04-May-2006 11:26:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @call_opt4D_OpeningFcn, ...
                   'gui_OutputFcn',  @call_opt4D_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before call_opt4D is made visible.
function call_opt4D_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to call_opt4D (see VARARGIN)

% Choose default command line output for call_opt4D
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes call_opt4D wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = call_opt4D_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function opt4D_executable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to opt4D_executable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function opt4D_executable_Callback(hObject, eventdata, handles)
% hObject    handle to opt4D_executable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of opt4D_executable as text
%        str2double(get(hObject,'String')) returns contents of opt4D_executable as a double


% --- Executes on button press in opt4D_executable_pushbutton.
function opt4D_executable_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to opt4D_executable_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile( ...
       {'*.*'}, ...
        'Pick opt4D executable');
if(pathname ~= 0)
    set(handles.opt4D_executable, 'String', [pathname filename]);
end

% --- Executes during object creation, after setting all properties.
function plan_file_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plan_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function plan_file_Callback(hObject, eventdata, handles)
% hObject    handle to plan_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plan_file as text
%        str2double(get(hObject,'String')) returns contents of plan_file as a double


% --- Executes on button press in plan_file_pushbutton.
function plan_file_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to plan_file_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile( ...
       {'*.pln'}, ...
        'Pick plan file');
if(pathname ~= 0)
    set(handles.plan_file, 'String', [pathname filename]);
end

% --- Executes during object creation, after setting all properties.
function results_folder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to results_folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function results_folder_Callback(hObject, eventdata, handles)
% hObject    handle to results_folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of results_folder as text
%        str2double(get(hObject,'String')) returns contents of results_folder as a double


% --- Executes on button press in results_folder_pushbutton.
function results_folder_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to results_folder_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


[pathname] = uigetdir('', 'Pick results folder');
if(pathname ~= 0)
    set(handles.results_folder, 'String', pathname);
end



% --- Executes during object creation, after setting all properties.
function beam_weight_file_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beam_weight_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function beam_weight_file_Callback(hObject, eventdata, handles)
% hObject    handle to beam_weight_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of beam_weight_file as text
%        str2double(get(hObject,'String')) returns contents of beam_weight_file as a double


% --- Executes on button press in beam_weight_file_pushbutton.
function beam_weight_file_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to beam_weight_file_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile( ...
       {'*.bwf'}, ...
        'Pick beam weight file');
if(pathname ~= 0)
    set(handles.opt4D_executable, 'String', [pathname filename]);
end

% --- Executes during object creation, after setting all properties.
function results_prefix_CreateFcn(hObject, eventdata, handles)
% hObject    handle to results_prefix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function results_prefix_Callback(hObject, eventdata, handles)
% hObject    handle to results_prefix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of results_prefix as text
%        str2double(get(hObject,'String')) returns contents of results_prefix as a double


% --- Executes on button press in save_results.
function save_results_Callback(hObject, eventdata, handles)
% hObject    handle to save_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of save_results

update_disabled_sections(handles);


% --- Executes on button press in write_dose_file.
function write_dose_file_Callback(hObject, eventdata, handles)
% hObject    handle to write_dose_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of write_dose_file


% --- Executes on button press in write_dvh_history_file.
function write_dvh_history_file_Callback(hObject, eventdata, handles)
% hObject    handle to write_dvh_history_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of write_dvh_history_file


% --- Executes on button press in select_initial_beam_weight_file.
function select_initial_beam_weight_file_Callback(hObject, eventdata, handles)
% hObject    handle to select_initial_beam_weight_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of select_initial_beam_weight_file

update_disabled_sections(handles);


% --- Executes on button press in optimize_plan.
function optimize_plan_Callback(hObject, eventdata, handles)
% hObject    handle to optimize_plan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of optimize_plan
update_disabled_sections(handles);

% --- Executes during object creation, after setting all properties.
function max_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function max_steps_Callback(hObject, eventdata, handles)
% hObject    handle to max_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_steps as text
%        str2double(get(hObject,'String')) returns contents of max_steps as a double


% --- Executes during object creation, after setting all properties.
function min_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function min_steps_Callback(hObject, eventdata, handles)
% hObject    handle to min_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min_steps as text
%        str2double(get(hObject,'String')) returns contents of min_steps as a double


% --- Executes during object creation, after setting all properties.
function max_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function max_time_Callback(hObject, eventdata, handles)
% hObject    handle to max_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_time as text
%        str2double(get(hObject,'String')) returns contents of max_time as a double


% --- Executes during object creation, after setting all properties.
function stopping_fraction_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stopping_fraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function stopping_fraction_Callback(hObject, eventdata, handles)
% hObject    handle to stopping_fraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stopping_fraction as text
%        str2double(get(hObject,'String')) returns contents of stopping_fraction as a double


% --- Executes on button press in fixed_step_size.
function fixed_step_size_Callback(hObject, eventdata, handles)
% hObject    handle to fixed_step_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fixed_step_size

set(handles.optimal_step_size, 'value', 0);
update_disabled_sections(handles);

% --- Executes during object creation, after setting all properties.
function step_size_multiplier_CreateFcn(hObject, eventdata, handles)
% hObject    handle to step_size_multiplier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function step_size_multiplier_Callback(hObject, eventdata, handles)
% hObject    handle to step_size_multiplier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of step_size_multiplier as text
%        str2double(get(hObject,'String')) returns contents of step_size_multiplier as a double


% --- Executes on button press in prevent_overstepping.
function prevent_overstepping_Callback(hObject, eventdata, handles)
% hObject    handle to prevent_overstepping (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of prevent_overstepping


% --- Executes on button press in optimal_step_size.
function optimal_step_size_Callback(hObject, eventdata, handles)
% hObject    handle to optimal_step_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of optimal_step_size
set(handles.fixed_step_size, 'value', 0);
update_disabled_sections(handles);


% --- Executes during object creation, after setting all properties.
function line_search_steps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to line_search_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function line_search_steps_Callback(hObject, eventdata, handles)
% hObject    handle to line_search_steps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of line_search_steps as text
%        str2double(get(hObject,'String')) returns contents of line_search_steps as a double


% --- Executes on button press in steepest_descent.
function steepest_descent_Callback(hObject, eventdata, handles)
% hObject    handle to steepest_descent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of steepest_descent
set(handles.diagonalized_newton, 'value', 0);


% --- Executes on button press in diagonalized_newton.
function diagonalized_newton_Callback(hObject, eventdata, handles)
% hObject    handle to diagonalized_newton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of diagonalized_newton
set(handles.steepest_descent, 'value', 0);

% --- Ensure that disabled sections are handled correctly
function update_disabled_sections(handles)
% handles    structure with handles and user data (see GUIDATA)

if(get(handles.select_initial_beam_weight_file, 'value') == 0)
    set(handles.beam_weight_file, 'enable', 'off');
    set(handles.beam_weight_file_pushbutton, 'enable', 'off');
else
    set(handles.beam_weight_file, 'enable', 'on');
    set(handles.beam_weight_file_pushbutton, 'enable', 'on');
end

if(get(handles.save_results, 'value') == 0)
    set(handles.results_folder,            'enable', 'off');
    set(handles.results_folder_pushbutton, 'enable', 'off');
    set(handles.results_prefix,            'enable', 'off');
    set(handles.write_dose_file,           'enable', 'off');
    set(handles.write_dvh_history_file,    'enable', 'off');
    set(handles.show_results,              'enable', 'off');
else
    set(handles.results_folder,            'enable', 'on');
    set(handles.results_folder_pushbutton, 'enable', 'on');
    set(handles.results_prefix,            'enable', 'on');
    set(handles.write_dose_file,           'enable', 'on');
    set(handles.write_dvh_history_file,    'enable', 'on');
    set(handles.show_results,              'enable', 'on');
end

if(get(handles.optimize_plan, 'value') == 0)
    set(handles.steepest_descent,     'enable', 'off');
    set(handles.diagonalized_newton,  'enable', 'off');
    set(handles.max_steps,            'enable', 'off');
    set(handles.min_steps,            'enable', 'off');
    set(handles.max_time,             'enable', 'off');
    set(handles.stopping_fraction,    'enable', 'off');
    set(handles.fixed_step_size,      'enable', 'off');
    set(handles.step_size_multiplier, 'enable', 'off');
    set(handles.prevent_overstepping, 'enable', 'off');
    set(handles.optimal_step_size,    'enable', 'off');
    set(handles.line_search_steps,    'enable', 'off');
else
    set(handles.steepest_descent,     'enable', 'on');
    set(handles.diagonalized_newton,  'enable', 'on');
    set(handles.max_steps,            'enable', 'on');
    set(handles.min_steps,            'enable', 'on');
    set(handles.max_time,             'enable', 'on');
    set(handles.stopping_fraction,    'enable', 'on');
    set(handles.fixed_step_size,      'enable', 'on');
    if(get(handles.fixed_step_size, 'value') == 1)
	set(handles.step_size_multiplier, 'enable', 'on');
	set(handles.prevent_overstepping, 'enable', 'on');
    else
	set(handles.step_size_multiplier, 'enable', 'off');
	set(handles.prevent_overstepping, 'enable', 'off');
    end
    set(handles.optimal_step_size,    'enable', 'on');
    if(get(handles.optimal_step_size, 'value') == 1)
	set(handles.line_search_steps,    'enable', 'on');
    else
	set(handles.line_search_steps,    'enable', 'off');
    end
end


% --- Executes during object creation, after setting all properties.
function command_line_CreateFcn(hObject, eventdata, handles)
% hObject    handle to command_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function command_line_Callback(hObject, eventdata, handles)
% hObject    handle to command_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of command_line as text
%        str2double(get(hObject,'String')) returns contents of command_line as a double


% --- Executes on button press in generate_command.
function generate_command_Callback(hObject, eventdata, handles)
% hObject    handle to generate_command (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

generate_command(handles);

% --- Generate command
function generate_command(handles)
% handles    structure with handles and user data (see GUIDATA)

command_string = get(handles.opt4D_executable, 'string');

if(get(handles.select_initial_beam_weight_file, 'value') == 1)
    bwf_file = get(handles.beam_weight_file, 'string');
    bwf_root = regexprep(bwf_file, '(_[0-9]*)?_[0-9]*[.]bwf', '')
    command_string = [command_string ' --initial-bwf-root ' bwf_root];
end

if(get(handles.save_results, 'value') == 1)
    % command_string = [command_string ' --save-results'];

    results_folder = get(handles.results_folder, 'string');
    % Check if the name of the folder ends in a slash
    if(results_folder(end) ~= filesep)
	results_folder(end+1) = filesep;
    end

    results_prefix = [results_folder, get(handles.results_prefix, 'string')];
    command_string = [command_string ' --out-file-prefix ' results_prefix];

    if(get(handles.write_dose_file, 'value') == 1)
	command_string = [command_string ' --write-dose'];
    else
	% command_string = [command_string ' --dont-write-dose'];
    end
	
    if(get(handles.write_dvh_history_file, 'value') == 1)
	command_string = [command_string ' --write-DVH-history'];
    else
	% command_string = [command_string ' --dont-write-DVH-history'];
    end
else
    command_string = [command_string ' --dont-save-results'];
end

if(get(handles.optimize_plan, 'value') == 1)
    
    if(get(handles.steepest_descent, 'value') == 1)
	command_string = [command_string ' --dont-use-hessian'];
    end
    
    if(get(handles.diagonalized_newton, 'value') == 1)
	command_string = [command_string ' --use-hessian'];
    end

    if(~isempty(get(handles.max_steps, 'string')))
	command_string = [command_string ' --max-steps ' ...
	    get(handles.max_steps, 'string')];
    end

    if(~isempty(get(handles.min_steps, 'string')))
	command_string = [command_string ' --min-steps ' ...
	    get(handles.min_steps, 'string')];
    end

    if(~isempty(get(handles.max_time, 'string')))
	command_string = [command_string ' --max-time ' ...
	    get(handles.max_time, 'string')];
    end

    if(~isempty(get(handles.stopping_fraction, 'string')))
	command_string = [command_string ' --stop-fract ' ...
	    get(handles.stopping_fraction, 'string')];
    end

    if(get(handles.fixed_step_size, 'value') == 1)
	command_string = [command_string ' --dont-use-optimal-steps'];

	if(~isempty(get(handles.step_size_multiplier, 'string')))
	    command_string = [command_string ' --scale-factor-multiplier ' ...
		get(handles.step_size_multiplier, 'string')];
	end

	if(get(handles.prevent_overstepping, 'value') == 1)
	    command_string = [command_string ' --dont-allow-overstepping'];
	else
	    % command_string = [command_string ' --allow-overstepping'];
	end
    end

    if(get(handles.optimal_step_size, 'value') == 1)
	command_string = [command_string ' --use-optimal-steps'];
	
	if(~isempty(get(handles.line_search_steps, 'string')))
	    command_string = [command_string ' --linesearch-steps ' ...
		get(handles.line_search_steps, 'string')];
	end
    else
	% command_string = [command_string ' --dont-use-optimal-steps'];
    end
else
    command_string = [command_string ' -c'];
end

command_string = [command_string ' ' get(handles.plan_file, 'string')];

set(handles.command_line, 'string', command_string);


% --- Executes on button press in run_opt4D.
function run_opt4D_Callback(hObject, eventdata, handles)
% hObject    handle to run_opt4D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

generate_command(handles);


results_folder = get(handles.results_folder, 'string');
if(~exist(results_folder, 'dir'))
    % Folder doesn't exist, so we need to create it
    mkdir(results_folder);
end

eval(['!xterm -hold -e ' get(handles.command_line, 'string') ' &']);


% --- Executes on button press in show_results.
function show_results_Callback(hObject, eventdata, handles)
% hObject    handle to show_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

plan = get(handles.plan_file, 'string');

results_folder = get(handles.results_folder, 'string');
% Check if the name of the folder ends in a slash
if(results_folder(end) ~= filesep)
    results_folder(end+1) = filesep;
end
results_prefix = [results_folder, get(handles.results_prefix, 'string')];

show_opt4D_results(plan, results_prefix);
