function [row, column, slice] = explore_solid_RCS( rcs_data, cmap, ...
						   dx, dy, dz, ...
                                                   row, column, slice)
%explore_solid_RCS   Examine data in RCS format.
%    Examine data thet represents the value at various points in a 3D solid.
%    The user can click to move through data by slices.
%
%    explore_solid_RCS(rcs_data, cmap, dx, dy, dz) uses the colormap cmap and
%    voxel size (dx, dy, dz).
%
%    explore_solid_RCS(rcs_data, cmap) assumes voxels are cubes.
%
%    explore_solid_RCS(rcs_data) uses a grayscale color map.
%    
%    [row, column, slice] = explore_solid_RCS(...)  Returns the final row,
%    column and slice.
%
%    Click on a side with left button to remove a slice.
%    Click on a side with right button to add a slice.
%    Click on a side with middle button to add all slices back.
%    Click outside the volume to exit.

[nR, nC, nS] = size(rcs_data);

% Use grayscale colormap if needed
if(nargin < 2 || isempty(cmap))
    cmap = gray(256);
end

if(nargin < 5)
    dx = 1; dy = 1; dz = 1;
end

if(nargin < 8)
    row = nR;
    column = nC;
    slice = 1;
end

levels = double( unique(rcs_data(:)));

done = false;
while(~done)
    clf;

    % Range of data to show
    rows = 1:row;
    columns = 1:column;
    slices = slice:nS;

    % Draw Axial face
    temp_axial = rcs_data(rows,columns,slices(1));
    x = [1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
    y = slices(1) * ones(2,3);
    z = [rows(1);rows(end)] * [1 1 1];
    try
	axial_handle = warp(x,y,z,temp_axial,cmap);
        hold on;
    catch
        start_fig = gcf;
        if(~exist('backup_fig'))
            backup_fig = figure('name', 'Backup plot (warp failed)');
        end
        figure(backup_fig);
        image(temp_axial);
        colormap(cmap);
        title(['Axial Slice (' num2str(slices(1)) ')']);
        % keyboard;
        figure(start_fig);
    end

    % Draw Coronal face
    temp_coronal = rcs_data(rows(end),columns,slices);	
    temp_coronal = squeeze(permute(temp_coronal,[3,1,2]));
    x=[1;1]*[columns(1) (columns(1)+columns(end))/2  columns(end)];
    y= [slices(1);slices(end)] * [1 1 1];
    z = rows(end) * ones(2,3);
    try
	coronal_handle = warp(x,y,z,temp_coronal,cmap);
    end

    % Draw Sagital face
    temp_sagital = rcs_data(rows(end):-1:rows(1),columns(end),slices);	
    temp_sagital = squeeze(permute(temp_sagital,[3,2,1]));
    x = columns(end) * ones(2,3);
    y = [slices(1);slices(end)] * [1 1 1];
    z = [1;1]*[rows(end) (rows(end)+rows(1))/2  rows(1)];
    try
	sagital_handle = warp(x,y,z,temp_sagital,cmap);
    end

    % Draw outline of dose cube
    x = [1,1,    nC,    nC,1,1,    nC,    nC,    nC,    nC,    nC;
         1,1,column,column,1,1,column,column,column,column,column]';
    y = [1,nS,nS,1,1,1,1,1,nS,nS,1;
         slice,nS,nS,slice,slice,slice,slice,slice,nS,nS,slice]';
    z = [nR,nR,nR,nR,nR,1,1,nR,nR,1,1;
         row,row,row,row,row,1,1,row,row,1,1]';
    l = line(x,y,z);
    set(l(1),'LineStyle',':');
    set(l(2),'Color',[0 1 0]);
    set(gca,'color','none');

    % Add labels
    xlabel('columns');
    ylabel('slices');
    zlabel('rows');

    % Set view
    default_view = [  4.4029    0.3061         0   -2.3545
                     -0.7537    0.5068    3.9152   -1.8341
                     -1.1986    0.8059   -2.4620   34.5111
                      0         0         0         1.0000];
    view(default_view); set(gca,'ydir','normal');
    axis tight
    set(gca,'DataAspectRatio',[1/dx 1/dy 1/dz]);

    % keyboard

    % Get button click
    [x, y, button] = ginput(1);

    if(y > .5 & y < 1.5)
        % Clicked on front face.
        if(button==1 && slice < nS)
            slice = slice + 1;
        elseif(button==2)
            slice = 1;
        elseif(button==3 && slice > 1)
            slice = slice - 1;
        end
    elseif(y > 1 && y < nS)
        % Clicked on a side of the cube
        if(x == nC)
            % Clicked on right (sagital) face
            if(button==1 && column > 1)
                column = column - 1;
            elseif(button==2)
                column = nC;
            elseif(button==3 && column < nC)
                column = column + 1;
            end
        else
            % Clicked on top face
            if(button==1 && row > 1)
                row = row - 1;
            elseif(button==2)
                row = nR;
            elseif(button==3 && row < nR)
                row = row + 1;
            end
        end
    else
        done = true;
    end

    hold off;
end

return
