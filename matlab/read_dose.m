function [dose] = read_dose(dif, dose_file_name)
%[dose] = read_dose(dif, dose_file_name)
%
%  example: [plan] = read_pln('bob.pln');
%           [dif]  = read_dif(plan.dif_file_name);
%           [dose] = read_dose(dif, 'bob_dose.dat');

disp(['Reading dose from ' dose_file_name]);

if(exist(dose_file_name, 'file'))
    fid = fopen(dose_file_name);
    [dose, count] = fread(fid, 'float');
    fclose(fid);
else
    error(['Dose file does not exist: ' dose_file_name]);
end

nColumns = dif.nX;
nRows    = dif.nZ;

% Convert dose to a RCS matrix
dose = permute(reshape(dose, nColumns, [], nRows), [3 1 2]);

return
