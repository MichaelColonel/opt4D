function plot_opt4D_sampling_history(out_file_prefix, fig_titles)
%plot_opt4D_convergence Plots the objective history for an opt4D optimization.
%   This is useful for understanding how the various algorithms are running.
%   Also plots the multi-objective history for each optimization and the
%   sampling rate history if appropriate.
%
% plot_opt4D_sampling_history(out_file_prefix) Plots the convergence for the
%   given out file prefix.  The out_file_prefix can be a cell array
%   of strings.  Repeated runs will be plotted on the same plot.
%
% plot_opt4D_sampling_history(out_file_prefix, titles) Allows the user to
%   override the default titles.
%
%  See also read_opt4D_sampling_history


% Set default multi_run_mapping
[multi_run_mapping, single_file_prefix] = ...
find_multirun_mapping(out_file_prefix);

[objective_names, sampling_fraction_hist]...
= read_opt4D_sampling_history(single_file_prefix);

nGroups = max(multi_run_mapping);
nRuns = numel(multi_run_mapping);

% Plot sampling fraction history for multiruns
for(iMultiRun = 1:nGroups)
    % More than one run might be present so make a combined figure

    first_pass = true;
    for(iRun = 1:nRuns)
        if((multi_run_mapping(iRun) == iMultiRun) && ...
                numel(sampling_fraction_hist) >= iRun && ...
                ~isempty(sampling_fraction_hist{iRun}))
            if(first_pass)
                if(sum(multi_run_mapping(:) == iMultiRun) > 1)
                    % More than one run present
                    figure('name',['Multi-run sampling: ' ...
                        fig_titles{iMultiRun}]);
                else
                    % Only one run present
                    figure('name',['Sampling: ' ...
                        fig_titles{iMultiRun}]);
                end
                plot(sampling_fraction_hist{iRun});
                legend(objective_names{iRun});
                xlabel('Optimization step');
                ylabel('Objective Sampling Fraction');
                title(fig_titles{iMultiRun});
                hold on;
                first_pass = false;
            else
                plot(sampling_fraction_hist{iRun});
            end
        end
    end
    if(~first_pass)
        hold off;
    end
end

