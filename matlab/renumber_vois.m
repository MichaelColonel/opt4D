function renumber_vois(plan_file, output_voi_file, input_voiNos, output_voiNos)
%renumber_vois(plan_file, output_voi_file, input_voiNos, output_voiNos)

% Check inputs
if(size(input_voiNos) ~= size(output_voiNos))
    error('Input and Output voi numbers must be the same size');
end

plan = read_pln(plan_file);
dif = read_dif(plan.dif_file_name);

voifield = read_voi(plan.voi_file_name, dif.nZ, dif.nX);
new_voifield = voifield;
for(iInputVoi = 1:length(input_voiNos))
    new_voifield(voifield == input_voiNos(iInputVoi)) = output_voiNos(iInputVoi);
end

write_voi(output_voi_file, new_voifield);
