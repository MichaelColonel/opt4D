function [w] = stochastically_optimize_plan(plan)
% optimize_plan Optimize a konrad plan using Matlab
%
%    w = optimize_plan(plan)

% Read plan file
if(isstr(plan))
    plan = read_pln(plan);
end

% Read dimension information file
dif = read_dif(plan.dif_file_name);

nRows = dif.nZ;
nColumns = dif.nX;
nSlices = dif.nY;
nVoxels = nRows * nColumns * nSlices;
nVois = length(plan.vois);

% Read Voi file
voi_list = reshape(read_voi(plan), nVoxels, 1);
nNonairVoxels = sum(voi_list > 0);

% Find prescriptions for each voxel
[d_min, d_max, q_under, q_over] = find_voxel_constraints(plan);

% Find sampling fraction for each voxel
sampling_fraction = ones(nVoxels, 1);
for(i = 1:nVois)
    if(~isempty(plan.vois(i).sampling_fraction) && voi_size > 0)
        sampling_fraction(voi_list==i) = plan.vois(i).sampling_fraction;
    end
end

global Dij;
global bixel_info;
if(isempty(Dij))
    [Dij, bixel_info] = read_simplified_dij(plan);
end
nBixels = length(bixel_info.beamNo);

w_initial = zeros(nBixels, 1);

options.max_steps = 100;
options.min_improvement = 0.00001;
options.step_size = 1;
options.step_size = 'optimized';
% options.step_direction = 'diagonallized newton';
 options.step_direction = 'steepest descent';
% options.step_direction = 'conjugate gradient';
% options.voxel_grid_size = [nRows nColumns*nSlices];
options.dif = dif;
options.enforce_coverage = false;
options.weighted_sampling = true;
options.sampling_fraction = .1;
w_reduced = optimize_IMRT(Dij, d_min, d_max, q_under, q_over, ...
                                w_initial, options);
% options.reduce_problem = true;
% options.reduction_fraction = .75;
% w_normal = optimize_IMRT(Dij, d_min, d_max, q_under, q_over, ...
                                % w_initial, options);

% dose_difference = reshape(Dij*(w_normal - w_reduced), ...
%    [dif.nZ dif.nX dif.nY]);
% for(iSlice = 1:dif.nY)
%     figure('name', ['Slice ' num2str(iSlice)]);
%     imagesc(dose_difference(:,:,iSlice));
%     title('Difference between optimized dose');
%     colorbar;
% end


w = w_reduced;

dose = Dij * w;

% Examine results

% Show DVH
figure;
dvh = calculate_dvh(dose, 0.1, voi_list, nVois);
plot_DVH(dvh, plan);

% Show intensity maps
figure;
plot_intensity_maps(w, bixel_info);

% figure;
figure;
explore_dose(dose,plan);

