function [w] = optimize_stochastic_IMRT(Dijp, d_min, d_max, q_under, q_over,...
				w_initial, options);
%OPTIMIZE_STOCHASTIC_IMRT Optimizes stochastic IMRT plan in Matlab
%   [w] = optimize_stochastic_IMRT(Dijp, d_min, d_max, q_under, q_over, ...
%   w_initial) finds the optimium beam weights to use for an IMRT plan.
%   d_min and d_max are a minimum and maximum constraint for each voxel.
%   q_under and q_over are the penalty for overdosing and underdosing each
%   voxel.  w_initial is the starting beam weights.
%
%   [w] = optimize_stochastic_IMRT(..., options) uses the optimization options
%   from the options structure with the following fields possible:
%
%            max_steps  The maximum number of optimization steps to run
%      min_improvement  Stop if the objective is not improved by this fraction
%            step_size  The step size rule to use.  Can be 'optimized',
%                           'approximately optimized', or a number.
%       step_direction  The step direction rule to use.  Can be
%                            'steepest descent', 'newton', or
%                            'diagonallized newton'
%      stochastic_type  The type of stochastic optimization to do.  Can be
%                       'worst case', 'average treatment', or 'average dose'
%
%   Ex:  options.max_steps = 100;
%        options.min_improvement = 0;
%        options.step_size = 'optimized';
%        options.step_direction = 'steepest descent';
%        options.stochastic_type = 'worst case';
%        [w] = optimize_stochastic_IMRT(Dijp, d_min, d_max, ...
%                        q_under, q_over, w_initial, options);


tic

nInstances = numel(Dijp);


%
% Prepare optimization_options
%  

if(~exist('options') || ~isstruct(options))
    options = struct;
end

if(~isfield(options, 'max_steps'))
    options.max_steps = 100;
end

if(~isfield(options, 'min_improvement'))
    options.min_improvement = 0;
end

if(~isfield(options, 'voxel_grid_size'))
    options.voxel_grid_size = [];
end

if(~isfield(options, 'step_size'))
    options.step_size = 'optimized';
end

if(~isfield(options, 'step_direction'))
    options.step_direction = 'diagonallized newton';
end

if(~isfield(options, 'stochastic_type'))
    options.stochastic_type = 'average treatment';
elseif(strcmp(options.stochastic_type, 'average dose'))
    Dij = Dijp{1};
    for(i = 2:numel(Dijp))
        Dij = Dijp{i} + Dij;
    end
    Dij = Dij ./ numel(Dijp);
    w = optimize_IMRT(Dij, d_min, d_max, q_under, q_over, w_initial, options);
    return
end


% Store useful information
objective_history = zeros(nInstances,options.max_steps);
alpha_history = zeros(1,options.max_steps);
nVoxels = numel(d_min);
nNonairVoxels = sum(d_max < inf);

% Precompute Transpose
Dij_transpose = cell(size(Dijp));
Dij_transpose_sum_of_sqrares_inv = cell(size(Dijp));
for(i = 1:nInstances)
    Dij_transpose{i} = Dijp{i}';
    Dij_transpose_sum_of_squares_inv{i} = sum(Dij_transpose{i}.^2,2).^-1;
end

% Run first step of optimization
step = 1;
step_size = 1;
max_step_size = inf;

% Start with initial beam weights
w = w_initial;
d = Dijp{1} * w;

% Find goal dose
d_goal = min(d_max, max(d_min,d));

% Find cost of error
q_active = ones(size(d));
q_active(d < d_min) = q_under(d < d_min);
q_active(d > d_max) =  q_over(d > d_max);

% objective = q_active' * ((d - d_goal).^2);

% Determine direction to move
iInstance = 1;
gradient = Dij_transpose{iInstance} * (q_active .* (d - d_goal));
switch lower(options.step_direction)
  case {'steepest descent'}
    w_direction = gradient;
  case {'diagonallized newton'}
    w_direction = 0.5 * (q_active' * (Dijp{iInstance}.^2)).^-1' .* gradient;
  case {'newton'}
    w_direction = 0.5 * inv(Dijp{iInstance}' * sparse(diag(q_active)) ...
        * Dijp{iInstance}) * gradient;
  otherwise
    disp('Unknown method.')
end

fig = figure;
% % Determine initial damping factor
if(isnumeric(options.step_size))
    d_temp = Dijp{1} * max(0, w - step_size * w_direction);
    prescribed_dose = sum(d_max(d_min > 0));
    delivered_dose = sum(d_temp(d_min > 0));
    step_size =  prescribed_dose / delivered_dose
    step_size = step_size * options.step_size
else
    % Find optimal step size
    [step_size, objective] = find_optimal_stepsize( ...
        Dijp{1}, w, w_direction, d_min, d_max, q_under, q_over, step_size, ...
        max_step_size);
end

% Compute current beam weights
w = max(0, w - step_size * w_direction);


max_step_size = 10 * step_size;
max_step_size = inf;

% Create figures
objective_fig = figure('name','Objective History');
fluence_fig   = figure('name','Input Fluence');
dose_fig      = figure('name','Dose');
step_size_fig = figure('name','Finding optimal step size');


% step = 1;
while(step < options.max_steps)

    % compute stats for current solution
    for(i = 1:nInstances)
        objective_history(i,step) = sqrt(calculate_objective(Dijp{i} * w, ...
            d_min, d_max, q_under, q_over) / nNonairVoxels);
    end
    [max_objective, worst_instance] = max(objective_history(:,step));
    mean_objective = mean(objective_history(:,step));
    random_instance = ceil(rand * nInstances);

    alpha_history(step) = step_size;

    % Show current status
    figure(objective_fig);
    semilogy(objective_history');
    title('Objective History');
    
    figure(fluence_fig);
    plot(w);
    title('Input Fluence');
    
    figure(dose_fig);
    if(~isempty(options.voxel_grid_size))
        mesh(reshape(d,options.voxel_grid_size));
    else
        plot(d);
    end
    title('Expected Dose');

    disp(['Step: ' num2str(step)...
          ' Mean Objective: ' num2str(mean_objective)...
          ' Worst Objective: ' num2str(max_objective)...
          ' WorstTreatmentNo: ' num2str(worst_instance)...
          ' time: ' num2str(toc) ]);

    % Store last objective
    if(step > 1)
        last_objective = objective;
    else
        last_objective = inf;
    end
    
    % Determine which objective we are trying to improve
    switch lower(options.stochastic_type)
    case 'worst case'
      objective = max_objective;
    case 'average treatment'
      objective = mean_objective;
    end

    % Check if the minimum allowable improvement was made
    if(objective <= 0 ...
        || abs(last_objective - objective)/objective < options.min_improvement)
       break
    end

    % Determine the direction to move in
    switch lower(options.stochastic_type)
    case 'worst case'
        iInstance = worst_instance;

        d = Dijp{iInstance} * w;

        % Find goal dose
        d_goal = min(d_max, max(d_min,d));

        % Find cost of error
        q_active = ones(size(d));
        q_active(d < d_min) = q_under(d < d_min);
        q_active(d > d_max) =  q_over(d > d_max);

        % Determine direction to move
        gradient = Dij_transpose{iInstance} * (q_active .* (d - d_goal));
        switch lower(options.step_direction)
        case {'steepest descent'}
            w_direction = gradient;
        case {'diagonallized newton'}
            w_direction = 0.5 * (q_active' * (Dijp{iInstance}.^2)).^-1' ...
                .* gradient;
        case {'newton'}
            w_direction = 0.5*inv(Dijp{iInstance}'*sparse(diag(q_active))...
                * Dijp{iInstance}) * gradient;
        otherwise
            disp('Unknown method.')
        end

        % Find optimal step size
        figure(step_size_fig);
        if(~isnumeric(options.step_size))
            switch lower(options.step_size)
            case 'optimized'
                step_size = find_optimal_stepsize( ...
                    Dijp{worst_instance}, w, w_direction, d_min, d_max, ...
                    q_under, q_over, step_size, max_step_size);
            case 'approximately optimized'
                step_size = find_approximately_optimal_stepsize( ...
                    Dijp{worst_instance} * w, ...
                    Dijp{worst_instance} * w_direction, d_min, d_max, ...
                    q_under, q_over, step_size, max_step_size);
            end
        end

    case 'average treatment'

        % Find direction by averaging direction from each instance
        w_direction = zeros(size(w));
        for(iInstance = 1:nInstances)
            d = Dijp{iInstance} * w;

            % Find goal dose
            d_goal = min(d_max, max(d_min,d));

            % Find cost of error
            q_active = ones(size(d));
            q_active(d < d_min) = q_under(d < d_min);
            q_active(d > d_max) =  q_over(d > d_max);

            % Determine direction to move
            gradient = Dij_transpose{iInstance} * (q_active .* (d - d_goal));
            switch lower(options.step_direction)
            case {'steepest descent'}
                w_direction = w_direction + gradient;
            case {'diagonallized newton'}
                w_direction = w_direction + 0.5 ...
                    * (q_active' * (Dijp{iInstance}.^2)).^-1' .* gradient;
            case {'newton'}
                w_direction = w_direction + 0.5 * inv(Dijp{iInstance}' ...
                    * sparse(diag(q_active)) * Dijp{iInstance}) * gradient;
            otherwise
                disp('Unknown method.')
            end
        end
        w_direction = w_direction ./ nInstances;

        % Find optimal step size
        figure(step_size_fig);
        if(~isnumeric(options.step_size))
            switch lower(options.step_size)
            case 'optimized'
                step_size = find_optimal_stepsize( ...
                    Dijp{random_instance}, w, w_direction, d_min, d_max, ...
                    q_under, q_over, step_size, max_step_size);
            case 'approximately optimized'
                step_size = find_approximately_optimal_stepsize( ...
                    Dijp{random_instance} * w, ...
                    Dijp{random_instance} * w_direction, d_min, d_max, ...
                    q_under, q_over, step_size, max_step_size);
            end
        end
    end

    % Start work on next solution
    step = step + 1;

    % Compute current beam weights
    w = max(0, w - step_size * w_direction);
end

figure('name','Objective History');
subplot(1,2,1);
plot(objective_history');
title('Objective History');
xlabel('Step');
ylabel('Objective Function');

subplot(1,2,2);
plot(alpha_history);
title('Step Size History');
xlabel('Step');
ylabel('Optimal Step Size');

return


% -----------------------------------------
function [objective] = calculate_mean_objective(Dijp, w, d_min, d_max, ...
    q_under,   q_over)
%calculate_mean_objective(Dijp, w, d_min, d_max, q_under, q_over)
%   [objective] = calculate_mean_objective(Dijp, w, d_min, d_max, ...
%   q_under, q_over)
%
%  Calculate the mean objective.

objective = 0;
for(i = 1:numel(Dijp))
    objective = objective + calculate_objective(Dijp{i} * w, d_min, d_max,...
        q_under, q_over);
end
objective = objective / numel(Dijp);

return


% -----------------------------------------
function [objective] = calculate_max_objective(Dijp, w, d_min, d_max, ...
    q_under,   q_over)
%calculate_max_objective(Dijp, w, d_min, d_max, q_under, q_over)
%   [objective] = calculate_max_objective(Dijp, w, d_min, d_max, ...
%   q_under, q_over)
%
%  Calculate the max objective.

objective = 0;
for(i = 1:numel(Dijp))
    objective = max(objective, calculate_objective(Dijp{i} * w, ...
        d_min, d_max, q_under, q_over));
end

return


