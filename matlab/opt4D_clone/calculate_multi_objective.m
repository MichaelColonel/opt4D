function [multi_objective, objective] = calculate_multi_objective(plan, ...
    voi_list, dose)
%calculate_multi_objective  Calculates the violation of each dose constraint
%
%   Example: [multi_objective, objective] = calculate_multi_objective(plan, ...
%                voi_list, dose);

% Read plan file if needed
if(isstr(plan))
    plan = read_pln(plan);
end

% Read voi_list if needed
if(~exist('voi_list') || isempty(voi_list))
    voi_list = reshape(read_voi(plan), nVoxels, 1);
end

% Use zero dose if none given
if(~exist('dose') || isempty(dose))
    dose = zeros(size(voi_list));
end


nObjectives = length(plan.objectives);
nVois = length(plan.VOI);

multi_objective = zeros(nObjectives, 1);
for(iObjective = 1:nObjectives)
    objective = plan.objectives(iObjective);

    voi_dose = dose(voi_list == objective.voiNo);
    if(~isempty(voi_dose))
        if(objective.is_max_constraint)
            multi_objective(iObjective) = objective.weight * ...
            mean(max(0, voi_dose - objective.desired_dose).^2);
        elseif(objective.is_min_constraint)
            multi_objective(iObjective) = objective.weight * ...
            mean(max(0, objective.desired_dose - voi_dose).^2);
        elseif(objective.is_equality_constraint)
            multi_objective(iObjective) = objective.weight * ...
            mean((voi_dose - objective.desired_dose).^2);
        else
            error('Objective type not supported by this function.');
        end
    end
end

% Calculate the objective
objective = sum(multi_objective(:));
