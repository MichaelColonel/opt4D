function [w] = stochastically_optimize_IMRT(...
  Dij, voilist, d_min, d_max, q_under, q_over, sampling_fraction, ...
  x_initial, options);
%STOCHASTICALLY_OPTIMIZE_IMRT Optimizes IMRT plan in Matlab
% OUT OF DATE DESCRIPTION!!!!!
%   [w] = optimize_IMRT(Dij, d_min, d_max, q_under, q_over, w_initial)
%   finds the optimium beam weights to use for an IMRT plan.  d_min and d_max
%   are a minimum and maximum constraint for each voxel.  q_under and q_over
%   are the penalty for overdosing and underdosing each voxel.  w_initial is
%   the starting beam weights.
%
%   [w] = optimize_IMRT(..., options) uses the optimization options from the
%   options structure with the following fields possible:
%
%   options.max_steps
%   options.min_improvement
%   options.voxel_grid_size
%   options.step_size       The step size rule to use.  Can be 'optimized',
%                           'approximately optimized', or a number.
%   options.step_direction  The step direction rule to use.  Can be
%                            'steepest descent', 'newton',                      
%                            'diagonallized newton' or 'conjugate gradient'
%   options.reduce_problem  If true, runs calculate_reduced_problem
%   options.reduction_threshold Threshold of acceptable combination
%   options.dif
%   options.enforce_coverage Treat minimums as hard constraints.

tic


% Ensure that voilist is 
if(numel(voilist) ~= size(voilist, 1))
    voilist = voilist(:);
end

%
% optimization_options
%  

if(~exist('options') || ~isstruct(options))
    options = struct;
end

if(~isfield(options, 'max_steps'))
    options.max_steps = 100;
end

if(~isfield(options, 'min_improvement'))
    options.min_improvement = 0;
end

if(~isfield(options, 'voxel_grid_size'))
    options.voxel_grid_size = [];
end

if(~isfield(options, 'step_size'))
    options.step_size = 'optimized';
end

if(~isfield(options, 'step_direction'))
    options.step_direction = 'diagonallized newton';
end

if(~isfield(options, 'reduce_problem'))
    options.reduce_problem = false;
end

if(~isfield(options, 'reduction_threshold'))
    options.reduction_threshold = 0;
end

if(~isfield(options, 'enforce_coverage'))
    options.enforce_coverage = false;
end

if(~isfield(options, 'sampling_fraction'))
    options.sampling_fraction = 1;
end


% Create cell array of transposed Dij matrices for each structure
nVois = max(voilist);
Dij_chunks = cell(nVois, 1);
Dij_t = Dij';
for(iVoi = 1:nVois)
    Dij_chunks{iVoi} = Dij_t(voilist == iVoi, :);
end
clear Dij_t;

x = x_initial;

% Calculate gradient
grad_x = zeros(size(x));
F = 0;
for(iVoi = 1:nVois)
    d = (x' * Dij_chunks{iVoi})';
    F = F + mean(q_over(iVoi) .* (max(d - d_max, 0) .^2) ...
      + q_under(iVoi) .* (max(d_min - d, 0) .^2));

    grad_f = (2 * q_over(iVoi) .* (max(d - d_max, 0)) ...
      - 2 * q_under(iVoi) .* (max(d_min - d, 0))) / numel(d);

    grad_x = grad_x + Dij_chunks{iVoi} * grad_f;
end

% Estimate gradient
e_grad_x = zeros(size(x));
e_F = 0;
for(iVoi = 1:nVois)
    mask = rand(
    d = (x' * Dij_chunks{iVoi})';
    F = F + mean(q_over(iVoi) .* (max(d - d_max, 0) .^2) ...
      + q_under(iVoi) .* (max(d_min - d, 0) .^2));

    grad_f = (2 * q_over(iVoi) .* (max(d - d_max, 0)) ...
      - 2 * q_under(iVoi) .* (max(d_min - d, 0))) / numel(d);

    grad_x = grad_x + Dij_chunks{iVoi} * grad_f;
end


% Project onto feasible direction
constrained_grad_x = grad_x .* (grad_x > 0 | x > 0);




start_time = cputime;

% Store useful information
objective_history = zeros(1,options.max_steps);
full_objective_history = zeros(1,options.max_steps);
alpha_history = zeros(1,options.max_steps);
nVoxels = numel(d_min);

% Precompute Transpose

% Dij_transpose = Dij';
% Dij_transpose_sum_of_squares_inv = sum(Dij.^2,2).^-1;

% Seperate out tumor underdose constraints
if(options.enforce_coverage)
    Dtarget = Dij((d_min > 0),:);
    d_target_min = d_min(d_min > 0);
end

% Run first step of optimization
step_size = 1;
max_step_size = inf;

% Start with initial beam weights
w = w_initial;
d = Dij * w;

% Find goal dose
% d_goal = d;
d_goal = min(d_max, max(d_min,d));

% Find cost of error
q_active = ones(size(d));
q_active(d < d_min) = q_under(d < d_min);
q_active(d > d_max) =  q_over(d > d_max);

objective = q_active' * ((d - d_goal).^2);

% Determine initial direction to move
gradient = 2 * ((q_active .* (d - d_goal))' * Dij)';
switch lower(options.step_direction)
  case {'steepest descent'}
    w_direction = -gradient;
  case {'diagonallized newton'}
    w_direction = -0.5 * (q_active' * (Dij.^2)).^-1' .* gradient;
  case {'newton'}
    w_direction = -0.5 * inv(Dij' * sparse(diag(q_active)) * Dij) * gradient;
  case {'conjugate gradient'}
    w_direction = -gradient;
  otherwise
    error('Unknown descent method.');
end

fig = figure;
% % Determine initial damping factor
if(isnumeric(options.step_size))
    d_temp = Dij * max(0, w + step_size * w_direction);
    prescribed_dose = sum(d_max(d_min > 0))
    delivered_dose = sum(d_temp(d_min > 0))
    step_size =  prescribed_dose / delivered_dose
    step_size = step_size * options.step_size
else
    iterations = 50;

    % Find optimal step size
    subplot(2,2,3);
    [step_size, objective] = find_optimal_stepsize( ...
        Dij, w, w_direction, d_min, d_max, q_under, q_over, step_size, ...
	max_step_size, iterations);
end

max_step_size = 10 * step_size;
max_step_size = inf;

w = max(0, w + step_size * w_direction);
% Project onto plans that cover the tumor fully
if(options.enforce_coverage && any(Dtarget * w < d_target_min))
    % keyboard
    % w = w + linprog(ones(size(w)), -Dtarget, Dtarget * w - d_target_min, ...
    %     [], [], zeros(size(w)), inf * ones(size(w)));
    d_target = Dtarget * w;
    [y,i] = max(d_target_min - d_target);
    w = w * (d_target_min(i) / d_target(i));
end

objective = calculate_objective(Dij * w, d_min, d_max, q_under, q_over);
if(options.sampling_fraction < 1)
    full_objective = calculate_objective(full_Dij * w, ...
       full_d_min, full_d_max, full_q_under, full_q_over);
else
    full_objective = objective;
end

alpha_history(1) = step_size;
objective_history(1) = objective;
full_objective_history(1) = full_objective;

steps = 1;
while(steps < options.max_steps)
    steps = steps + 1;

    % Resample if needed
    if(options.sampling_fraction < 1 && (mod(steps,1) == 0))
        if(isfield(options, 'weighted_sampling') && options.weighted_sampling == true)
            % Calculate dose
            d = full_Dij * w;
            obj = (max(d - full_d_max, 0) .^2) .* full_q_over + (max(full_d_min - d, 0) .^2) .* full_q_under;
            figure(18); hist(obj);

            sample_probability = obj ./ mean(obj) .* options.sampling_fraction;
            % sample_probability = options.sampling_fraction;
        
            sample_mask = rand(size(full_d_min)) < sample_probability;
            Dij = full_Dij(sample_mask,:);
            d_min = full_d_min(sample_mask);
            d_max = full_d_max(sample_mask);
	    q_under = full_q_under(sample_mask) ./ sample_probability(sample_mask);
	    q_over = full_q_over(sample_mask) ./ sample_probability(sample_mask);
        else
            sample_probability = options.sampling_fraction;
            sample_mask = rand(size(full_d_min)) < sample_probability;
            Dij = full_Dij(sample_mask,:);
            d_min = full_d_min(sample_mask);
            d_max = full_d_max(sample_mask);
	    q_under = full_q_under(sample_mask) ./ sample_probability;
            q_over = full_q_over(sample_mask);
        end
    end


    % Calculate dose
    d = Dij * w;

    % Show current progress
    figure(fig);
    subplot(2,2,1);
    plot(w);
    title('Input Fluence');
    figure(fig);
    subplot(2,2,2);
    if(~isempty(options.voxel_grid_size))
	if(options.reduce_problem)
	    mesh(reshape(c_to_d * d,options.voxel_grid_size));
	else
	    mesh(reshape(d,options.voxel_grid_size));
	end
    else
	if(options.reduce_problem)
	    plot(c_to_d * d);
	else
	    plot(d);
	end
    end
    title('Expected Dose');
    subplot(2,2,4);
    semilogy(full_objective_history);
    title('Objective History');
    ylabel('Objective');
    xlabel('Optimization Step');

    % Find goal dose
    d_goal = min(d_max, max(d_min,d));

    % Find cost of error
    q_active = ones(size(d));
    q_active(d < d_min) = q_under(d < d_min);
    q_active(d > d_max) =  q_over(d > d_max);

    % Determine direction to move
    last_direction = w_direction;
    last_gradient = gradient;
    gradient = 2 * ((q_active .* (d - d_goal))' * Dij)';
    switch lower(options.step_direction)
      case {'steepest descent'}
	w_direction = -gradient;
      case {'diagonallized newton'}
        % q_active = (q_under .* (d < d_min) + q_over .* (d > d_max));
	w_direction = -gradient .* ((2 * q_active' * (Dij.^2)).^-1)';
      case {'newton'}
	q_active = (q_under .* (d < d_min) + q_over .* (d > d_max));
	w_direction = -.5 * inv(Dij'*sparse(diag(q_active))*Dij)*gradient;
      case {'conjugate gradient'}
	beta = gradient' * (gradient - last_gradient) ...
	      / (last_gradient' * last_gradient);
	% Apply reset
	% if(mod(steps,10) == 0)
	%     beta = 0;
	% end
	w_direction = -gradient + beta * last_direction;
      otherwise
	disp('Unknown method.')
    end

    last_objective = objective;

    % Determine step size
    if(isnumeric(options.step_size))

        % Calculate objective
        objective = calculate_objective(Dij * max(0, w + step_size * w_direction), d_min, d_max, q_under, q_over);

    elseif(strcmpi(options.step_size, 'optimized'))
        
        figure(fig); subplot(2,2,3);
        % Find optimal step size
        [step_size, objective] = find_optimal_stepsize( ...
            Dij, w, w_direction, d_min, d_max, q_under, q_over, step_size, max_step_size);

    elseif( strcmpi(options.step_size, 'approximately optimized'))
        
        % Find optimal step size
        [step_size, objective] = find_approximately_optimal_stepsize( ...
            Dij*w, Dij*w_direction, d_min, d_max, q_under, q_over, step_size);
    else
        error(['options.step_size option unrecognized: ' ...
               options.step_size]);
    end 

 
    alpha_history(steps) = step_size;
    objective_history(steps) = objective;

    disp(['Step: ' num2str(steps) ' Objective: ' num2str(objective) ' time: ' num2str(toc)]);

    w = max(0,w + step_size * w_direction);

    % Calculate the objective for the full case
    if(options.sampling_fraction < 1)
        full_objective = calculate_objective(full_Dij * w, ...
           full_d_min, full_d_max, full_q_under, full_q_over);
    else
        full_objective = objective;
    end
    full_objective_history(steps) = full_objective;

    % Test if stopping criteria ia met
    if(objective <= 0 || ...
        abs(last_objective - objective)/objective < options.min_improvement)
        break
    end

    % Project onto plans that cover the tumor fully
    if(options.enforce_coverage && any(Dtarget * w < d_target_min))
        % keyboard
      % w = w + linprog(ones(size(w)), -Dtarget, Dtarget * w - d_target_min, ...
      %     [], [], zeros(size(w)), inf * ones(size(w)));
        d_target = Dtarget * w;
        [y,i] = max(d_target_min - d_target);
	if(d_target(i) > 0)
	    w = w * (d_target_min(i) / d_target(i));
	end
    end

end

disp(['Optimizing the problem for ' num2str(steps) ' steps'...
      ' took ' num2str(cputime - start_time) ' seconds.']);

figure('name','Objective History');
subplot(1,2,1);
plot(objective_history);
title('Objective History');
xlabel('Step');
ylabel('Objective Function');

subplot(1,2,2);
plot(alpha_history);
title('Step Size History');
xlabel('Step');
ylabel('Optimal Step Size');

if(options.sampling_fraction < 1)
    figure('name', 'Full Objective History');
    semilogy([objective_history', ...
        full_objective_history']);
    legend('Sampled Objective', 'True Objective');
    title(['Sampled vs. True Objective, ('...
        num2str(options.sampling_fraction) ')']);
    xlabel('Step');
    ylabel('Objective Value');
end


return


% -----------------------------------------
function [objective] = calculate_objective(d, d_min, d_max, q_under, q_over)
%[objective] = calculate_objective(d, d_min, d_max, q_under, q_over)
%
%  Calculate the quadradic objective function.  

objective = q_over' * (max(0,d-d_max).^2) + q_under' * (max(0,d_min-d).^2);

% % Find goal dose for each voxel
% d_goal = d;
% d_goal(d < d_min) = d_min(d < d_min);
% d_goal(d > d_max) = d_min(d > d_max);

% % Find cost of error in each voxel
% q_active = ones(size(d));
% q_active(d < d_min) = q_under(d < d_min);
% q_active(d > d_max) =  q_over(d > d_max);

% % Calculate objective
% objective = q_active' * ((d - d_goal).^2);

return


% -----------------------------------------
function [position_offsets] = calculate_position_offset(nPositions)
% [position_offsets] = calculate_position_offset(nPositions)
%
%  Generate position offsets


% Generate setup error
% Assumes zero mean normally distributed error of variance one
% setup_error = randn(2,nPositions);
setup_error = 2 * randn(2,nPositions) + 2 * randn(2,1) * ones(1,nPositions);

% Generate starting phase for breathing
breathing_start_phase = rand(1,nPositions);

% Generate corresponding breathing position
breathing_position = [zeros(1,nPositions);...
                      20 * cos(pi*breathing_start_phase).^6];

% Combind breathing and setup errors:
position_offsets = setup_error + breathing_position;
% position_offsets = 4* setup_error;


return

