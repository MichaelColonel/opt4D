%function D=KSparse2MSparse(KonradD)

%5/20/05 i think i will make this function a script since having KonradD
%two places in memory dies on big cases like the LungCase



%5/12/05 craft:  significantly better than my previous versions
%allocating space for myrows and mycols and vals was a big time saver,
%plus creating the D matrix the smart way using the sparse command.

numGantryAngles = length(KonradD);

numnonzeros=0;

for i=1:numGantryAngles
  for j = 1:length(KonradD{i})
    pb = KonradD{i}{j};
    numnonzeros = numnonzeros + length(pb.index);
  end
end

fp=1;
myrows=zeros(1,numnonzeros);
mycols=zeros(1,numnonzeros);
vals=zeros(1,numnonzeros);

currSize=1;

for i=1:numGantryAngles
  disp(['processing gantry angle' num2str(i)]);
  for j = 1:length(KonradD{i})

    %pencilBeam{currSize} = KonradD{i}{j};
    pb = KonradD{i}{j};
    
    myrows(fp:fp+length(pb.value)-1) = pb.index;
    mycols(fp:fp+length(pb.value)-1) = currSize*ones(1,length(pb.index));
    vals(fp:fp+length(pb.value)-1) = pb.value;
    fp = fp+length(pb.value);
        
    currSize = currSize+1;
  end
end

clear KonradD;

disp('creating matlab sparse D ...')
D = sparse(myrows,mycols,vals);

disp('writing D.mat to disk ...')

save D D;

disp('matlab sparse D created and written to file D.mat');

