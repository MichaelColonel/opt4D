function newmap = makeColorMap(oldmap,limits,ticks)
% function MAKECOLORMAP: create a colormap for dose wash 

ticks = round(ticks);
if length(ticks)~=2 | ticks(1)<1 | ticks(1)>size(oldmap,1) | ticks(2)<ticks(1) | ticks(2) > size(oldmap,1)
    error('Wrong limits for the colormap')
end
limits = round(sort(limits));
limits = limits-min(limits);

while 0
if min(limits)<0
   error('Wrong limits for the colormap')
end
if limits(1)~=0
    limits = [0 limits];
end
end

% purple on top
spectrum = [1 1 1; 0 .5 1; 0 .75 0; .5 1 1; 1 1 0; 1 .5 0; 1 0 0; .67 0 1];
%old time (aka clines64)
spectrum = [1 1 1; 0 .5 1; 0 .75 0; .5 1 1; .5 1 0; 1 1 0; 1 .5 0; 1 0 0];
% 9 colors
spectrum = [1 1 1; 0 .5 1; 0 .75 0; .5 1 1; .5 1 0; 1 1 0; 1 .5 0; 1 0 0; .67 0 1];

% hot/cold
spectrum = [1 1 1;  0 .75 0; .5 1 1; 0 .5 1;1 1 0; 1 .5 0; 1 0 0; .67 0 1; 0 .6 .6; .33 0 .67];
%spectrum = [1 1 1; 0 .5 1; 0 .75 0; .5 1 1; .5 1 0; 1 1 0; 1 .5 0; 1 0 0];
tmp = oldmap(ticks(1):ticks(2),:);
nn = ticks(2)-ticks(1)+1;
limits = round(limits/max(limits)*nn);

start=1;
for i1 = 2:length(limits)
    ncol = i1-1;
    while ncol>length(spectrum)
        ncol=ncol-length(spectrum);
    end
    color = spectrum(ncol);
    for i2 = start:limits(i1)
        tmp(i2,:) = spectrum(ncol,:);
    end
    start=limits(i1)+1;
end
newmap = oldmap;
newmap(ticks(1):ticks(2),:) = tmp;


