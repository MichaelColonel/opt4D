function [voiField] = read_voi( voiFile_or_plan, nRows_or_dif, nColumns)
%READ_VOI  Read Konrad voi file.
%   Rearranges into row, column, slice order.  The Vois are numbered
%   1 to 127 and air is numbered 0.
%
%   [voifield] = READ_VOI(PLAN) reads the voi for the given PLAN.  PLAN can
%   be either a structure (see READ_PLN) or a string containing a file name.
%
%   [voifield] = READ_VOI(voi_file, plan) uses the number of rows and columns
%   from a plan file or structure.
%
%   [voifield] = READ_VOI(voi_file, dif) uses the number of rows and columns
%   from a dif file or structure.
%
%   [voifield] = READ_VOI( voi_file, nRows, nColumns) Reads the voi file using
%   the given number of rows and columns.
%
%   See also WRITE_VOI, EXPLORE_VOI.

% Check what input was given
switch(nargin)
case 1
    % PLAN was given
    if(isstr(voiFile_or_plan))
        plan = read_pln(voiFile_or_plan);
    else
        plan = voiFile_or_plan;
    end

    voi_file = plan.voi_file_name;
    
    % Read dif file
    dif = read_dif(plan.dif_file_name);
    nRows = dif.nZ;
    nColumns = dif.nX;
case 2
    % A voi file name was given with either a plan or dif
    voi_file = voiFile_or_plan;
    
    % Parse input to get dif structure
    if(isstr(nRows_or_dif))
        % A file name was passed in, so we need to find out what type it is
        if(strfind(nRows_or_dif,'dif'))
            dif = read_dif(nRows_or_dif);
        elseif(strfind(nRows_or_dif,'pln'))
            plan = read_pln(nRows_or_dif);
            dif = read_dif(plan.dif_file_name);
        else
            error(['read_voi can''t read file: ' nRows_or_dif]);
        end
    elseif(isstruct(nRows_or_dif))
        % A structure was passed in, so we need to determin what it is
        if(isfield(nRows_or_dif, 'nX'))
            dif = nRows_or_dif;
        elseif(isfield(nRows_or_dif, 'dif_file_name'))
            plan = nRows_or_dif;
            dif = read_dif(plan.dif_file_name);
        else
            error('Invalid input');
        end
    else
        error('Invalid input');
    end
    nRows = dif.nZ;
    nColumns = dif.nX;
case 3
    % A voi file name was given with nRows and nColumns
    voi_file = voiFile_or_plan;
    nRows = nRows_or_dif;
    nColumns = nColumns;
end
    
[fid,message]=fopen(voi_file);
if(~isempty(message))
    error(['Error reading voi file: ' voi_file ' (' message ')']);
end

voiField = uint8(fread(fid,inf,'uint8') + 1);
nColumns
nRows
voiField = reshape(voiField, nColumns, [], nRows);
voiField = permute(voiField, [3 1 2]);

voiField(voiField == 128) = 0;

return


