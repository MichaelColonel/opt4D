function [optimal_parameters] = tune_opt4D( plan, ...
parameter_names, parameter_values, initial_guess, general_options, repeats)
% Tune parameters for opt4D


results_prefix = tempname;

nParameters = numel(parameter_names);
subplot_coords = [ceil(sqrt(nParameters)),...
ceil(nParameters /ceil(sqrt(nParameters)))];

current_optimal = initial_guess;
for(iPass = 1:3)
    % Find order
    param_order = randperm(nParameters);
    pass_figure = figure('name', ...
    ['Optimizing Parameters, pass ' num2str(iPass)]);
    for(iParameter = param_order)
        nValues = numel(parameter_values{iParameter});
        if(nValues == 1)
            % Skip optimization, since there is only one choice
            continue
        end

        param_figure = figure('name', ...
        ['Optimizing ' parameter_names{iParameter} ', ' num2str(iPass)]);
        
        % Find the optimal value for the parameter
        data = repmat(inf, repeats, nValues);
        for(iParameter_value = 1:nValues)
            % Generate options string
            options = [general_options ' --repeat ' num2str(repeats)];
            for(jParameter = 1:nParameters)
                if(jParameter == iParameter)
                    options = [options ' ' parameter_names{jParameter} ' '...
                    num2str(parameter_values{jParameter}(iParameter_value))];
                else
                    options = [options ' ' parameter_names{jParameter} ' '...
                    num2str(current_optimal(jParameter))];
                end
            end
            
            % Run optimization
	    show_log = false;
	    append_log = true;
	    run_opt4D(plan, results_prefix, options, show_log, append_log);

            % Read result
            if(exist([results_prefix 'repeat_log.dat'],'file'))
                data(:,iParameter_value) = textread(...
                [results_prefix 'repeat_log.dat']) * [0;1];
            else
                error('Failed reading repeat log');
            end

            % Plot results
            figure(param_figure);
            plot(parameter_values{iParameter}, data', 'kx');
            ylabel('Objective');
            xlabel('Parameter Value');
            title( parameter_names{iParameter});

            % Plot results
            figure(pass_figure);
            subplot(subplot_coords(1), subplot_coords(2), iParameter);
            plot(parameter_values{iParameter}, data', 'kx');
            ylabel('Objective');
            xlabel('Parameter Value');
            title( parameter_names{iParameter});
        end

        % Find optimal value of parameter
        [y,i] = min(max(data));
        current_optimal(iParameter) = parameter_values{iParameter}(i);

        % Plot results
        figure(param_figure);
        plot(parameter_values{iParameter}, data', 'kx',...
        parameter_values{iParameter}(i), max(data(:,i)), 'ro');
        ylabel('Objective');
        xlabel('Parameter Value');
        title( parameter_names{iParameter});

        % Plot results
        figure(pass_figure);
        subplot(subplot_coords(1), subplot_coords(2), iParameter);
        plot(parameter_values{iParameter}, data', 'kx',...
        parameter_values{iParameter}(i), max(data(:,i)), 'ro');
        ylabel('Objective');
        xlabel('Parameter Value');
        title( parameter_names{iParameter});


        pause(1);
        current_optimal
    end
end

optimal_parameters = current_optimal;
