function writeVoi2(DijStruct, outFile)
% writeVoi2(DijStruct, outFile)
%
% Write the Voxel In Structure file in opt4D .voi2 format.
%
% DijStruct  -- contains information about the problem.  The values used by
%                   writeVoi2 are:
%
%   DijStruct.doseCubeNr --  The dimensions of the dose cube.
%   DijStruct.doseCubeNc
%   DijStruct.doseCubeNs
%
%   DijStruct.voiData -- A cell array containing the indexes of each voxel
%                        within each structure.
%
% outFile    -- the name of the output file.
%
% Throws an error if the file writing fails

% Here's the voi file format:
% 
% VIS files are binary files that list the indexes of all voxels within each
% structure.  The specific structure is as follows:
%
% File Header:
%   uint8  -   Number of the highest volume of interest
%
% Structure Header:
%   int8   -   Number of structure.  0 corresponds to structure 1 in CERR
%   int32  -   Number of voxels in structure
%
% Structure Data:
%   uint32  -   Voxel index according to Konrad's indexing scheme.


% Open file for writing (binary data)
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
    error(['Error opening stf file for writing (' outFile '): ' message]);
end

% Write number of structures
fwrite(outfid, length(DijStruct.voiData), 'int8');

for structNum = 1:length(DijStruct.voiData)
    % First check if voi is empty
    if(isempty(DijStruct.voiData{structNum}))
        fwrite(outfid, structNum - 1, 'int8');
        fwrite(outfid, 0, 'uint32');
    else
        % Convert CERR indexes to Konrad indexing
        % First read coordinates in Konrad system from CERR index values
        [tempZ,tempX,tempY] = ind2sub([DijStruct.doseCubeNr,...
            DijStruct.doseCubeNc,  DijStruct.doseCubeNs],...
            DijStruct.voiData{structNum});
        % Then calculate indexes for Konrad indexing
        indexV = sub2ind([DijStruct.doseCubeNc, DijStruct.doseCubeNs,...
            DijStruct.doseCubeNr], tempX, tempY, tempZ) - 1;
        indexV = sort(indexV);

        % Now write out structure number, number of voxels, and the
        % voxel indexes
        fwrite(outfid, structNum - 1, 'int8');
        fwrite(outfid, length(indexV), 'uint32');
        fwrite(outfid, indexV, 'uint32');
    end
end

% Close file
status = fclose(outfid);
if status ~= 0
    error(['Error writing stf file']);
end
