function writeVoi(DijStruct, structures, outFile)
% writeVoi(DijStruct, structures, outFile)
%
% Write the volume of interest file in Konrad .voi format.
%
% DijStruct     --  contains information about the problem.  The values used by
%                   writeVoi are:
%
%   DijStruct.doseCubeNr    --  The dimensions of the dose cube.
%   DijStruct.doseCubeNc
%   DijStruct.doseCubeNs
%
%   DijStruct.voiData       --  A cell array containing the indexes of each voxel
%                               within each structure.
%
% structures    --  a vector containing structure numbers in decreasing order of
%                   importance.
%
% outFile       --  the name of the output file.
%
% Use showVoi to visualize the Voi file.
%
% Throws an error if the file writing fails

% Here's the voi file format:
% 
% Voi files are simply a list of 8 bit integers.  Each integer tells
% the volume of interest that the corresponding voxel in the dose cube
% belongs to.  For example, segment 1 in CERR corresponds to a 0 in the
% voi file.  Volume of intersest 128 (127 in file) corresponds to air
% and is ignored during optimization.
% 
% These voi numbers are written in column, slice, row order.  For
% example, a 3x4x2 dose cube in CERR would be written in this order:
%
%  slice 1: (toward feet)
%       front       r
% l  [ 1  2  3  4]  i
% e  [ 9 10 11 12]  h
% f  [17 18 19 20]  h
% t     back        t
%
%  slice 2: (toward head)
%       front       r
% l  [ 5  6  7  8]  i
% e  [13 14 15 16]  h
% f  [21 22 23 24]  h
% t     back        t
%
% Note that left corresponds to patient right and vice versa.


% Open file for writing (binary data)
[outfid, message] = fopen(outFile, 'w');
if ~isempty(message)
    error(['Error opening stf file for writing (' outFile '): ' message]);
end

% Preallocate voiField and fill with air (127)
voiField = 127 * ones(DijStruct.doseCubeNr * DijStruct.doseCubeNc *...
            DijStruct.doseCubeNs, 1);

for structNum = length(structures):-1:1
    % First check if voi is empty
    if(~isempty(DijStruct.voiData{structures(structNum)}))
        % Convert CERR indexes to Konrad indexing
        % First read coordinates in Konrad system from CERR index values
        [tempZ,tempX,tempY] = ind2sub([DijStruct.doseCubeNr,...
            DijStruct.doseCubeNc,  DijStruct.doseCubeNs],...
            DijStruct.voiData{structures(structNum)});
        % Then calculate indexes for Konrad indexing
        indexV = sub2ind([DijStruct.doseCubeNc, DijStruct.doseCubeNs,...
            DijStruct.doseCubeNr], tempX, tempY, tempZ);

        % Now store the structure number in voiField
        voiField(indexV) = structures(structNum) - 1;
    end
end

% Write out field
fwrite(outfid, voiField, 'int8');

% Close file
status = fclose(outfid);
if status ~= 0
    error(['Error writing stf file']);
end
