function explore_voi( pln_or_voi_file_name, dif_file_or_nRows, nColumns)
%explore_voi  Explore a volume of information cube.
%   To navigate:
%     Left click on a face to cut it away,
%     Right click to add a face back,
%     middle click to restore it completely
%
%   explore_voi( voi_file_name, nRows, nColumns) explores the file with the 
%   specified dimensions (nRows and nColumns correspond to nZ and nX in Konrad 
%   coordinates, repectively)
%
%   explore_voi( pln_file_name) explores the volumes of intrest for the given 
%   plan.  The plan can either be a structure (see read_pln) or a file name.
%
%   explore_voi( voi_file_name, dif_file_name) explores the given voi file 
%   based on the dimensions in the dif file.
%
%   explore_voi without any arguments calls up a diologue box to enter the file 
%   names that you wish to.
%

fig = figure('name', 'VOI Explorer');

% Find file to read if it is not passed in
if(nargin < 1)

    [file_name, path_name, filter_index] = uigetfile( ...
        {'*.pln', 'Plan file (*.pln)'; ...
         '*.voi', 'VOI file (*.voi)'}, ...
	 'Load pln file or voi file');
    if(filter_index == 0)
        disp( 'User pressed cancel.');
        return;
    elseif(filter_index == 1)
	pln_file_name = [path_name, file_name];
    else
	voi_file_name = [path_name, file_name];
    end
else
    % Process first argument
    if(isstr(pln_or_voi_file_name))
	% At least one string argument was passed in, check what it was
	[pathstr,name,ext,versn] = fileparts(pln_or_voi_file_name);
	if(strcmp(ext, '.pln'))
	    pln_file_name = pln_or_voi_file_name;
	elseif(strcmp(ext, '.voi'))
	    voi_file_name =  pln_or_voi_file_name;
	end
    elseif(isstruct(pln_or_voi_file_name))
	Plan = pln_or_voi_file_name;
    else
	error('First passed agument was bad');
    end
end


if(exist('pln_file_name'))
    Plan = read_pln(pln_file_name);
end

if(exist('Plan'))
    dif_file_name = Plan.dif_file_name;
    voi_file_name = Plan.voi_file_name;
end

% Process second argument
if(exist('dif_file_or_nRows'))
    if(isstr(dif_file_or_nRows))
	dif_file_name = dif_file_or_nRows;
    elseif(isstruct(dif_file_or_nRows))
	dif = dif_file_or_nRows;
    elseif(isnumeric(dif_file_or_nRows))
	nRows = dif_file_or_nRows;
    end
end

% Make sure either a dif was loaded or the number of rows was given
if(~exist('nRows') && ~exist('dif_file_name') && ~exist('dif'))
    [file_name, path_name, filter_index] = uigetfile( ...
	{'*.dif', 'Dif file (*.dif)'}, ...
	'Load dif file if you have one');
    if(filter_index == 0)
	disp( 'User pressed cancel.  Manually enter dimensions');
	nRows = input('Rows in dose cube? ');
	nColumns = input('Columns in dose cube? ');
    else
	dif_file_name = [path_name, file_name];
    end
end


if(exist('dif_file_name'))
    % Read dimension information file
    dif = read_dif(dif_file_name);
end

if(exist('dif'))
    % Pull attributes from dif structure
    nRows = dif.nZ;
    nColumns = dif.nX;
    nSlices = dif.nY;

    dx = dif.dX;
    dy = dif.dY;
    dz = dif.dZ;
else
    % No dif structure, so use cubic voxels
    dx = 1; dy = 1; dz = 1;
end

[voiField] = readVoi( voi_file_name, nRows, nColumns);

nSlices = size(voiField, 3);

% Find all VOIs in cube
voiNos = double(unique(voiField(:)));

% Use colormap from plan file if it exists
cmap = get_plan_cmap(Plan);

% [voi_outline] = find_outline(voiField);
% explore_solid_RCS(voi_outline, cmap, dx, dy, dz);

figure(fig);
explore_solid_RCS(voiField, cmap, dx, dy, dz);

return




% ------------------------------------------------------------------- %
function [voiField] = readVoi( voiFile, nRows, nColumns)

  [fid,message]=fopen(voiFile);
  if(~isempty(message))
      error(['Error reading voi file: ' voiFile ' (' message ')']);
  end

  voiField = uint8(fread(fid,inf,'uint8') + 1);
  
  voiField = reshape(voiField, nColumns, [], nRows);
  voiField = permute(voiField, [3 1 2]);

  voiField(voiField == 128) = 0;
  
return


% ------------------------------------------------------------------- %
function [voi_outline] = find_outline(voiField)

voi_outline = voiField;
voi_size = size(voiField);

insides_mask = [(voiField(1:end-1,:,:) == voiField(2:end,:,:)) ; false(1,voi_size(2), voi_size(3))] ...
    & [false(1,voi_size(2), voi_size(3)); (voiField(1:end-1,:,:) == voiField(2:end,:,:))] ...
    & [(voiField(:,1:end-1,:) == voiField(:,2:end,:)), false(voi_size(1), 1, voi_size(3))] ...
    & [false(voi_size(1), 1, voi_size(3)), (voiField(:,1:end-1,:) == voiField(:,2:end,:))] ...
    & cat(3, (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)), false(voi_size(1), voi_size(2), 1)) ...
    & cat(3, false(voi_size(1), voi_size(2), 1), (voiField(:,:,1:end-1) == voiField(:,:,2:end,:)));
voi_outline(insides_mask) = 0;
  
return

