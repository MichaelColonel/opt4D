function Data = readcubes(fname);
% [Data] = readcubes(fname);
% copied from savedata.m 
% from Alex

dir_work = pwd;
[path,file,ext,ver] = fileparts(fname);
if isequal(ext,'.h5')
    fnameold = fname;
    fname = [path '/' file '.hdf'];
    ! h5toh4 [fnameold] [fname]
end
%sd_id = hdfsd('start','/home/trofimov/konrad/data/pat/Inter/1412087_3x3_dc.hdf','rdonly');

sd_id = hdfsd('start',fname,'rdonly');
[ndatasets, nglobal_atts, stat] = hdfsd('fileinfo',sd_id);

for attr_idx=0:(nglobal_atts-1)
    [attr, status ] = hdfsd('readattr',sd_id,attr_idx);
end

for sds_idx = 0:(ndatasets-1)
    sds_id = hdfsd('select',sd_id,sds_idx);
    [dsname, dsndims, dsdims, dstype, dsatts, stat] = ...
        hdfsd('getinfo',sds_id);
    
    nn = sds_idx+1;
%dsdims = [105 105];
    Data = zeros([dsdims]);
%    dsndims = 2;
    ds_start = zeros(1,dsndims);
    ds_stride = [];
    ds_edges = dsdims;
    [Data, status] = hdfsd('readdata',sds_id,ds_start,ds_stride,ds_edges);
   
    %min(min(min(Data)));
    %max(max(max(Data)));
%    disp(sprintf('Found minimum and maximum values: %f %f', ...
%        double(min(min(min(Data{nn})))),double(max(max(max(Data{nn}))))));
    
end
Data = double(Data);

stat = hdfsd('end',sd_id);
