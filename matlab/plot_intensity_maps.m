function plot_intensity_maps(bwf_file_root_or_w, nInstances_or_plan, nBeams, max_intensity)
%plot_intensity_maps  Show beamlet intensity maps on the current figure.
% plot opt4D fluence maps %
%
%   plot_intensity_maps(bwf_file_root, nInstances, nBeams)
%
%   plot_intensity_maps(bwf_file_root, plan)
%
%   plot_intensity_maps(beam_weights, bixel_info)
%
%   
% 

% Get handle to current figure
fig = gcf;

% Figure out how the function was called and prepare variables
if(isstr(bwf_file_root_or_w))
    bwf_file_root = bwf_file_root_or_w;

    % Find number of instances and beams if required.
    if(isnumeric(nInstances_or_plan))
        nInstances = nInstances_or_plan;
        nBeams = nBeams;
    else
        if(isstr(nInstances_or_plan))
            plan = read_pln(nInstances_or_plan);
        elseif(isstruct(nInstances_or_plan))
            plan = nInstances_or_plan;
        end
        nInstances = plan.nInstances;
        nBeams = plan.nBeams;
    end

    % Read bwf files
    [w, bixel_info] = read_bwf(bwf_file_root, nInstances, nBeams);
else
    % The function was called with beam weights and beam info
    w = bwf_file_root_or_w;
    bixel_info = nInstances_or_plan;
    
    nInstances = max(bixel_info.instanceNo);
    nBeams = max(bixel_info.beamNo);
end


% Determine grid for plotting
subplotRows = floor(sqrt(nBeams));
subplotCols = ceil(nBeams / subplotRows);

if(~exist('max_intensity') || isempty(max_intensity))
    max_intensity = max(w);
end


% Display the influence map for each instance
for iInstance=1:nInstances
    if(nInstances > 1)
        fig = figure;
        set(fig, 'Name',['Instance ' num2str(iInstance)]);
    end

    for iBeam=1:nBeams

        % pull info on beam from data structures
        temp_map = ((bixel_info.instanceNo == iInstance) ...
                        & (bixel_info.beamNo == iBeam));

        temp_fluence = w(temp_map);
        temp_spotX = bixel_info.spotX(temp_map);
        temp_spotY = bixel_info.spotY(temp_map);

        xpos = unique(temp_spotX);
        ypos = unique(temp_spotY);
        
        tempImage = zeros(length(xpos),length(ypos));
        for iPB = 1:size(temp_fluence,1)
            ix = find(xpos == temp_spotX(iPB));
            iy = find(ypos == temp_spotY(iPB));
            tempImage(ix, iy) = temp_fluence(iPB);
        end

        figure(fig);
        subplot(subplotRows, subplotCols, iBeam);
        imagesc(tempImage', [0 max_intensity]);
        set(gca,'PlotBoxAspectRatio',[1,1,1]);
        axis off;
        colorbar('horiz');
        colormap(gray);
        ylabel(['Max Intensity: ' num2str(max(tempImage(:)))])

        if(exist('plan'))
            % Find beam angle from stf file
            try
                stf = read_stf([plan.stf_file_root '_' num2str(iBeam) '.stf']);
                title(['Gantry Angle: ' num2str(stf.gantryAngle)]);
                set(gca,'PlotBoxAspectRatio',[stf.spot_dx, stf.spot_dy,1])
            catch
            end
        end
    end
end

return
