function Attributes = readatts(fname);
% copied from savedata.m 
% From Alex
dir_work = pwd;
[path,file,ext,ver] = fileparts(fname);
if isequal(ext,'.h5')
    fnameold = fname;
    fname = [path '/' file '.hdf'];
    ! h5toh4 [fnameold] [fname]
end

file_id = hdfh('open',fname,'rdonly',1);
status = hdfv('start',file_id);
vgroup_ref = hdfv('findclass',file_id,'HDF5');
vgroup_id = hdfv('attach',file_id,vgroup_ref,'r');
%[vgroup_name,status] = hdfv('getname',vgroup_id);
count = hdfv('nattrs',vgroup_id);
for ii = 0:count-1
    [name,data_type,count,nbytes,status] = hdfv('attrinfo',...
          vgroup_id,ii);
    [value,status] = hdfv('getattr',vgroup_id,ii);
%    value = double(value);
% assume count==1 always    
    Attributes(ii+1) = value;
%    disp(sprintf('Found an attribute: %s Value: %f ',name,value));
end

status = hdfv('detach',vgroup_id);
status = hdfv('end',file_id);
status = hdfh('close',file_id);

Attributes = double(Attributes);