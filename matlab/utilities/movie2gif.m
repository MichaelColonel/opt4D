function movie2gif( mov, out_file, gifsicle_options)
%movie2gif Create an animated gif from a matlab movie.
%
%   movie2gif( mov, out_file) writes a movie to an animated gif.  If
%     out_file is not supplied, the user will be querried for one.
%
%   movie2gif( mov, out_file, gifsicle_options) passes the supplied options
%     to gifsicle when making the animated gif.
%
%   movie2gif Relies on the availability of Gifsicle and ImageMagick
%
%   See also MOVIE, GETFRAME.

if(~exist('out_file') || isempty(out_file))
    [filename, pathname] = uiputfile('*.gif',...
    'Choose an output file name for the animated gif');
    if isequal(filename,0) | isequal(pathname,0)
       error('User pressed cancel');
    else
       disp(['User selected ', fullfile(pathname, filename)])
    end
end

if(~exist('gifsicle_options'))
    gifsicle_options = [];
end

nFrames = numel(mov);
temp_png_name = [tempname '.png'];
temp_gif_prefix = [tempname];
temp_gif_list = '';
for(iFrame = 1:nFrames)
    [x,xmap] = frame2im(mov(iFrame));
    if(isempty(xmap))
        imwrite(x,temp_png_name,'png');
    else
        imwrite(x,xmap,temp_png_name,'png');
    end
    temp_gif_name = [temp_gif_prefix num2str(iFrame) '.gif'];
    eval(['!convert ' temp_png_name ' ' temp_gif_name]);
    temp_gif_list = [temp_gif_list ' ' temp_gif_name];
end

eval(['!gifsicle ' gifsicle_options ' ' temp_gif_list ' > ' out_file]);
delete(temp_png_name);
delete([temp_gif_prefix '*.gif']);

return

