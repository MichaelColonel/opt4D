function [euds] = calculate_EUDs(plan, voi_list, dose)
%calculate_EUDs  Calculates the Equivalent Uniform Dose for each VOI
%   
%
%   Example: [euds] = calculate_EUDs(plan, voi_list, dose);
%
%            [eud] = calculate_EUDs(dose, EUD_p);
%            DOESN'T WORK YET.

% Read plan file if needed
if(isstr(plan))
    plan = read_pln(plan);
end

% Read voi_list if needed
if(~exist('voi_list') || isempty(voi_list))
    voi_list = reshape(read_voi(plan), nVoxels, 1);
end

% Use zero dose if none given
if(~exist('dose') || isempty(dose))
    dose = zeros(size(voi_list));
end

nVois = length(plan.VOI);

euds = zeros(nVois, 1);
for(i = 1:nVois)
    if(isfield(plan.VOI(i), 'EUD_p') && ~isempty(plan.VOI(i).EUD_p))
	euds(i) = (mean(dose(voi_list == i).^plan.VOI(i).EUD_p))...  
	.^(1/plan.VOI(i).EUD_p);
    end
end

