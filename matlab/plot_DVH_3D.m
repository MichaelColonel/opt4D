function plot_DVH_3D(dvh, plan, plot_vois, show_prescriptions, should_color_errors)
%plot_DVH_3D  Read and plot DVH produced by opt4D (3D plot)
%   Plots a 3D graph of a DVH file as output by opt4D.
%
%   Does not currently show prescription information on the plot
%
%   plot_DVH_3D(dvh) plots the DVH with random colors for each volume of
%   interest.  The dvh can either be a file name or an array of column
%   vectors with the first column being the dose for each bin.
%
%   plot_DVH_3D(dvh, plan_file) plots the DVH with information from the
%   plan file.  Uses random colors if colors aren't included in the file.
%
%   plot_DVH_3D(dvh, plan) uses information in a plan structure.  See
%   read_pln.m for information on the structure.
%
%   plot_DVH_3D(dvh, plan, plot_vois) only plots the Volumes of
%   Interst listed in plot_vois.  plot_vois is a vector of integers.
%
%   Ex:
%      plot_DVH_3D('patient_DVH.dat', 'patient.pln');
%
%   Is the same as:
%      [dvh, dvh_dose_bins] = read_DVH('patient_DVH.dat');
%      plan = read_pln('patient.pln');
%      plot_DVH_3D([dvh_dose_bins dvh], plan);
%
%   The first usage is most common, but the second allows the user to change
%   the plan or dvh if needed.
%
%   Ex:
%      plot_DVH_3D('patient_DVH.dat', 'patient.pln', [1 3 5]);
%   only plots volume of interest 1, 3, and 5.
%
%   See also read_DVH.


% Read the DVH if needed
if(isstr(dvh))
    [dvh, dvh_dose] = read_DVH(dvh);
else
    dvh_dose = dvh(:,1);
    dvh = dvh(:,2:end);
end

% Find Vois with at least one voxel
nonzero_vois = find(dvh(1,:) > 0);

% Reduce bins in DVH
dvh = dvh(1:4:end,:);
dvh_dose = dvh_dose(1:4:end);

nVois = size(dvh,2);
nDVH_bins = size(dvh_dose,1);

% Check if the plan is defined
if( exist('plan'))
    % Read plan from file if a file name was given
    if(isstr(plan))
        plan = read_pln(plan);
    end
    
    % Make sure plan has all required parts
    if(isstruct(plan))
        if(~isfield(plan,'title') || ~isfield(plan,'VOI'))
            error('plan is not a valid structure.');
        end
    else
        error('plan is not a valid structure.');
    end

    % Make sure all VOIs are included
    if(nVois > length(plan.VOI))
        for(iVoi = length(plan.VOI)+1:nVois)
            plan.VOI(iVoi).name = ['Struct ', num2str(iVoi)];
        end
    end
else
    % Create default attributes for undefined plan
    plan = struct('title', '');
    plan.VOI = repmat(struct('name',''), 1, nVois);
    for iVoi = 1:length(plan.VOI)
        plan.VOI(iVoi).name = ['Struct ', num2str(iVoi)];
    end
end

% Default to not showing prescriptions
if( ~exist('show_prescriptions'))
    show_prescriptions = false;
end

% Default to not coloring in prescriptions
if( ~exist('should_color_errors'))
    should_color_errors = false;
end

% Make sure the color field exists
if(~isfield(plan.VOI, 'color'))
    plan.VOI(1).color = [];
end

% Assign random colors to structures without colors
temp_color = rand(length(plan.VOI),3);
for iVoi = 1:length(plan.VOI)
    if(isempty(plan.VOI(iVoi).color))
        plan.VOI(iVoi).color = temp_color(iVoi,:);
    end
end

% Discard some vois if appropriate
if(exist('plot_vois') && ~isempty(plot_vois))
    nonzero_vois = intersect(nonzero_vois, plot_vois);
end

% Quit if there are no vois left to plot
if(isempty(nonzero_vois))
    return
end

line_handles = [];
legends = cell(size(nonzero_vois));

fill3([0 0 0 0]', [0 100 100 0]', [0 0 .1 .1]', 'k');
hold on;
fill3([0 max(dvh_dose) max(dvh_dose) 0]', ...
      [0 0 0 0]', [0 0 .1 .1]', 'k');
for(i = 1:length(nonzero_vois))
    iVoi = nonzero_vois(i);

    % There is some dose delivered, so plot on DVH
    % h = plot(dvh_dose, dvh(:,iVoi), '--');
    h = fill3( [dvh_dose; flipud(dvh_dose)], ...
	[dvh(:,iVoi); flipud(dvh(:,iVoi))], ...
	[0 * ones(size(dvh_dose)); .1 * ones(size(dvh_dose))], 'r');
    
    % set(h,'LineWidth',2);

    if(~isempty(plan.VOI(iVoi).color))
        set(h, 'FaceColor', plan.VOI(iVoi).color)
    end
    
    if(isfield(plan.VOI, 'isTarget') && ~isempty(plan.VOI(iVoi).isTarget) && plan.VOI(iVoi).isTarget)
     %   set(h,'LineStyle', '-')
    end

    % Display prescriptions (only if color defined)
    if(~isempty(plan.VOI(iVoi).color))
        if(isfield(plan.VOI, 'min_DVH_dose') && ~isempty(plan.VOI(iVoi).min_DVH_dose))
            if(show_prescriptions)
                g = line(plan.VOI(iVoi).min_DVH_dose + [0 0;-5 0],...
                         plan.VOI(iVoi).min_DVH_volume + [0 0;0 -5]);
                set(g, 'color', plan.VOI(iVoi).color);
            end

            min_DVH_bin = max(find(dvh_dose <= plan.VOI(iVoi).min_DVH_dose));
            if(~isempty(min_DVH_bin) && dvh(min_DVH_bin,iVoi) < ...
                plan.VOI(iVoi).min_DVH_volume)
                g = line(dvh_dose(min_DVH_bin) + [0;0], ...
                    [plan.VOI(iVoi).min_DVH_volume; dvh(min_DVH_bin,iVoi)]);
                set(g, 'color', plan.VOI(iVoi).color,'linestyle',':');
            end

            if(should_color_errors)
                for(iDVH_bin=iVoi:nVois:nDVH_bins)
                    if(dvh_dose(iDVH_bin) < plan.VOI(iVoi).min_DVH_dose)
                        if(dvh(iDVH_bin,iVoi) < plan.VOI(iVoi).min_DVH_volume)
                            h = line(dvh_dose(iDVH_bin)*[1;1], ...
                                [dvh(iDVH_bin,iVoi); ...
                                plan.VOI(iVoi).min_DVH_volume]);
                            set(h,'Color',plan.VOI(iVoi).color);
                        end
                    end
                end
            end
        end

        if(isfield(plan.VOI, 'max_DVH_dose') && ...
            ~isempty(plan.VOI(iVoi).max_DVH_dose))
            if(show_prescriptions)
                g = line(plan.VOI(iVoi).max_DVH_dose + [0 0; 5 0], ...
                    plan.VOI(iVoi).max_DVH_volume + [0 0; 0 5]);
                set(g, 'color', plan.VOI(iVoi).color);
            end
        
            max_DVH_bin = min(find(dvh_dose >= plan.VOI(iVoi).max_DVH_dose));
            if(~isempty(max_DVH_bin) && dvh(max_DVH_bin,iVoi) > ...
                plan.VOI(iVoi).max_DVH_volume)
                g = line(dvh_dose(max_DVH_bin) + [0;0], ...
                    [plan.VOI(iVoi).max_DVH_volume; dvh(max_DVH_bin,iVoi)]);
                set(g, 'color', plan.VOI(iVoi).color,'linestyle',':');
            end
            
            if(should_color_errors)
                for(iDVH_bin=iVoi:nVois:nDVH_bins)
                    if(dvh_dose(iDVH_bin) > plan.VOI(iVoi).max_DVH_dose)
                        if(dvh(iDVH_bin,iVoi) > plan.VOI(iVoi).max_DVH_volume)
                            h = line(dvh_dose(iDVH_bin)*[1;1], ...
                                [dvh(iDVH_bin,iVoi); plan.VOI(iVoi).max_DVH_volume]);
                            set(h,'Color',plan.VOI(iVoi).color);
                        end
                    end
                end
            end
        end
    end
    
    line_handles(i) = h;
    legends{i} = [plan.VOI(iVoi).name];
end
hold off
% campos([max(dvh_dose), 110, 1]);
set(gca, 'DataAspectRatio', [40, 100, 1], 'ZTick', []);

title(strrep(plan.title, '_', '\_'));
legend(line_handles, strrep(legends, '_', '\_'), 0);
xlabel('Dose (Gy)','FontSize',15);
ylabel('Relative volume (\%)','FontSize',15);
set(gca,'FontSize',13);

return
