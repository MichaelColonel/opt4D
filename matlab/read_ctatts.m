function [ctatts] = read_ctatts(ctatts_file)
%read_ctatts Read Konrad ct attributes file.
%   Reads the ctatts file into a structure:
%
%     ctatts.slice_dimension
%     ctatts.slice_number
%     ctatts.pixel_size
%     ctatts.slice_distance
%     ctatts.Pos_Isocenter_X
%     ctatts.Pos_Isocenter_Y
%     ctatts.Pos_Isocenter_Z
%     ctatts.Number_Of_Voxels_in_CT_Cube_Z

% Read the plan file into memory
try
    [command, value] = textread(ctatts_file, '%s %n', 'commentstyle', 'shell');
catch
    error(['Error reading ctatts file: ' ctatts_file ' (' lasterr ')']);
end



% Initialize the ctatts structure
ctatts = struct( ...
    'slice_dimension', 0, ...
    'slice_number', 0, ...
    'pixel_size', 0, ...
    'slice_distance', 0, ...
    'Pos_Isocenter_X', 0, ...
    'Pos_Isocenter_Y', 0, ...
    'Pos_Isocenter_Z', 0, ...
    'Number_Of_Voxels_in_CT_Cube_Z', 0);


% Read the cti file name
ctatts.cti_file_name = command{1};

% Read the rest of the file into the structure
for(iCommand = 2:length(command))
    temp_command = strrep(command{iCommand},'-','_');
    if(isfield(ctatts, temp_command))
        ctatts.(temp_command) = value(iCommand);
    end
end

