function [w, bixel_info] = read_bwf(bwf_file_root, nInstances_or_plan, nBeams)
%read_bwf Reads all of the beam weights and bixel info from bwf files
%
%  [w, bixel_info] = read_bwf(bwf_file_root, nInstances, nBeams) reads the 
%  files with the given number of instances and beams
%
%  [w, bixel_info] = read_bwf(bwf_file_root, plan) finds the number of beams
%  in the plan structure or plan file.
%
%  bixel_info is a struct of arrays that give the information about each 
%       beamlet.
%  bixel_info.instanceNo is the beam that each beamlet comes from.
%  bixel_info.beamNo is the beam that each beamlet comes from.
%  bixel_info.energy is the energy of that beam
%  bixel_info.spotX and bixel_info.spotY are the position of the beamlet within 
%       the beam (in cm)
%

% ------------------------------------------------------------
% Figure out how the function was called and prepare variables

% Find number of instances and beams if required.
if(isstr(nInstances_or_plan))
    plan = read_pln(nInstances_or_plan);
    nInstances = plan.nInstances;
    nBeams = plan.nBeams;
elseif(isstruct(nInstances_or_plan))
    plan = nInstances_or_plan;
    nInstances = plan.nInstances;
    nBeams = plan.nBeams;
else
    nInstances = nInstances_or_plan;
    nBeams = nBeams;
end

% -------------------------------------------------
% Read the bwf files

w = [];
bixel_info = struct('instanceNo',[], 'beamNo',[], 'energy',[], ...
            'spotX',[], 'spotY',[]);

% Read the beams for each instance
for iInstance=1:nInstances

    % Read bwf files for each beam
    for iBeam=1:nBeams
        % Read beam intensity from file
        if(iInstance > 1)
            bwf_file_name = [bwf_file_root '_' num2str(iInstance) '_' ...
                num2str(iBeam) '.bwf'];
        else
            bwf_file_name = [bwf_file_root '_' num2str(iBeam) '.bwf'];
        end

        [weight, spotX, spotY] = textread(bwf_file_name, ...
            '%*n %n %*n %*n %*n %*n %*n %n %n', 'commentstyle', 'shell');

        w = [w; weight];
	bixel_info.spotX = [bixel_info.spotX; spotX / 10];
	bixel_info.spotY = [bixel_info.spotY; spotY / 10];
        bixel_info.instanceNo = [bixel_info.instanceNo; ...
                             iInstance * ones(size(weight))];
        bixel_info.beamNo = [bixel_info.beamNo; ...
                             iBeam * ones(size(weight))];
        bixel_info.energy = [bixel_info.energy; zeros(size(weight))];
    end
end

return
