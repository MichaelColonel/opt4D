function plot3views( rcs_data, row, column, slice, cmap)
% plot3views( rcs_data, row, column, slice, cmap)
%
% Shows axial, coronal, and sagital views of data indexed in row, column, slice
% coordinates.

[nR, nC, nS] = size(rcs_data);

subplot(2,2,3)
    temp_axial = rcs_data(end:-1:1,:,slice);
    imshow(temp_axial,cmap);
    title('Axial');
subplot(2,2,1)
    temp_coronal = permute(reshape(rcs_data(row,:,nS:-1:1), nC, nS),[2,1]);
    imshow(temp_coronal,cmap);
    title('Coronal');
subplot(2,2,2)
    temp_sagital = permute(reshape(rcs_data(nR:-1:1,column,nS:-1:1), nR, nS),[2,1]);
    imshow(temp_sagital,cmap);
    title('Sagital');
subplot(2,2,4)
    temp = [temp_coronal, zeros(nS, 10), temp_sagital;
            zeros(10, nC+nR+10);
            temp_axial, zeros(nR, 10+nR)];
    imshow(temp,cmap);
drawnow
