function add_margin(old_plan, new_voi_file_name, voiNo, margin, ...
            new_voiNo);
%ADD_MARGIN Creates new plan files with a margin around a volume of interest.
%   The margin can be added to an existing volume of interest or added as a
%   new volume of interest.  ADD_MARGIN creates new .voi file based
%   on an existing .pln file.
%
%   ADD_MARGIN(old_plan, new_voi_file_name, voiNo, margin) Adds a margin around
%   an existing volume of interest.
%
%   ADD_MARGIN(old_plan, new_voi_file_name, voiNo, margin, new_voiNo)
%   creates a new volume of interest surrounding the old one.
%
%   ADD_MARGIN(old_plan, new_voi_file_name, voiNo, margin, []) uses
%   the next available number for the new VOI.
%
%   Examples:
%      add_margin('patient.pln', 'patient_with_margin.voi', 1, 2.5)
%        Creates new file patient_with_margin.voi
%        such that volume of interest 1 has been expanded by 2.5cm.
%
%  $Id: add_margin.m,v 1.6 2008/03/14 00:47:14 bmartin Exp $

% Read plan file if given as a file name
if(isstr(old_plan))
    old_plan = read_pln(old_plan);
end

% Check if new voiNo was given
if(exist('new_voiNo'))
    % The margin should be added as a new volume of interest
    if(isempty(new_voiNo))
        % No number was given, so use next available number
	new_voiNo = numel(old_plan.VOI) + 1;
    end
else
    % The margin shold be added to the existing VOI
    new_voiNo = voiNo;
end

% Read dif file
dif = read_dif(old_plan.dif_file_name);
nVoxels = dif.nX * dif.nY * dif.nZ;

% Read voi field
voi_field = read_voi(old_plan);

% Find voxels in voi to outline
voi_members = find(voi_field == voiNo);
voi_non_members = find(voi_field ~= voiNo);

% Find location of each member of voi
voi_position = get_xyz_table(dif, voi_members);

% Find location of every non-member voxel
non_voi_position = get_xyz_table(dif, voi_non_members);

% Check if each non-member is in range of a member and add to voi_field
update_period = floor(length(voi_members) / 100);
start_time = cputime;
h = waitbar(0,'Voxels Processed');
for i = 1:length(voi_members)
    distance = ((non_voi_position(1,:) - voi_position(1,i)).^2 ...
                   +(non_voi_position(2,:) - voi_position(2,i)).^2 ...
                   +(non_voi_position(3,:) - voi_position(3,i)).^2);
    non_members_in_margin = find(distance <= margin*margin);
    voi_field(voi_non_members(non_members_in_margin)) = new_voiNo;
    if(mod(i,update_period) == 0)
        speed = (cputime - start_time) / i;
        time_remaining = round((length(voi_members) - i) * speed);
        waitbar(i/length(voi_members),h,[num2str(time_remaining) 's left']);
    end
end
close(h);

write_voi(new_voi_file_name, voi_field);

