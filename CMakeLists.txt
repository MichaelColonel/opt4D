project (opt4D)

cmake_minimum_required (VERSION 2.8.12)

if (NOT CMAKE_BUILD_TYPE)
  set (CMAKE_BUILD_TYPE "Release" CACHE STRING
    "Choose the type of build, options are: Debug Release
      RelWithDebInfo MinSizeRel." FORCE)
endif ()

set (opt4D_LIBRARY_SRC
  src/AbsoluteErrorObjective.cpp
  src/AmplParser.cpp
  src/Aperture.cpp
  src/ApertureDoseInfluenceMatrix.cpp
  src/ApertureSequencer.cpp
  src/BiologicallyEquivalentDoseModel.cpp
  src/BixelPositivityConstraint.cpp
  src/CellSurvivalObjective.cpp
  src/ColumnGenerationDaoOptimizer.cpp
  src/ConjugateGradient.cpp
  src/ConstrainedMetaObjective.cpp
  src/ConstrainedObjective.cpp
  src/Constraint.cpp
  src/CpuTimer.cpp
  src/DaoVector.cpp
  src/DeliveryMachineModel.cpp
  src/DeltaBarDelta.cpp
  src/DijFileParser.cpp
  src/DirectParameterVector.cpp
  src/DoseConstraint.cpp
  src/DoseDeliveryModel.cpp
  src/DoseInfluenceMatrix.cpp
  src/DoseParser.cpp
  src/DoseVector.cpp
  src/DvhCoverageObjective.cpp
  src/Dvh.cpp
  src/DvhObjective.cpp
  src/ExpectedQuadraticObjectiveIntegrate.cpp
  src/ExpectedQuadraticObjectiveTensor.cpp
  src/ExpectedValueMetaObjective.cpp
  src/ExternalOptimizer.cpp
  src/FastMeanDoseComputer.cpp
  src/FastMeanObjective.cpp
  src/GaussianSetupError.cpp
  src/GenericConstraint.cpp
  src/Geometry.cpp
  src/GEudObjective.cpp
  src/GradientOptimizer.cpp
  src/KonradBixelVector.cpp
  src/KonradDoseInfluenceMatrix.cpp
  src/KonradSquareRootBixelVector.cpp
  src/Lbfgs.cpp
  src/LETModel.cpp
  src/LinearMeanObjective.cpp
  src/LinearProjectionOptimizer.cpp
  src/LQUncertaintyModel.cpp
  src/MaxMinOrMinMaxObjective.cpp
  src/MeanConstraint.cpp
  src/MeanDoseObjective.cpp
  src/MeanSquaredErrorObjective.cpp
  src/MetaObjective.cpp
  src/MosekInterface.cpp
  src/NonSeparableObjective.cpp
  src/Objective.cpp
  src/Optimization.cpp
  src/OptppInterface.cpp
  src/ParameterVector.cpp
  src/ParameterVectorDirection.cpp
  src/Plan.cpp
  src/Prescription.cpp
  src/RangeSetupUncertaintyModel.cpp
  src/RangeUncertaintyModel.cpp
  src/RespiratoryMotionModel.cpp
  src/RobustifiedDoseConstraint.cpp
  src/SeparableObjective.cpp
  src/SetupUncertaintyModel.cpp
  src/SfudHomogeneityObjective.cpp
  src/SquaredMeanObjective.cpp
  src/SquareRootBixelVector.cpp
  src/SteepestDescent.cpp
  src/TumorEUDObjective.cpp
  src/TxtFileParser.cpp
  src/VirtualBixelModel.cpp
  src/VirtualBixelTrafoMatrix.cpp
  src/Voi.cpp
  src/VoiParser.cpp
  src/VoxelDoseConstraint.cpp
  src/VoxelMaxDoseObjective.cpp
  src/VvParser.cpp
  )
include_directories (BEFORE ${CMAKE_CURRENT_SOURCE_DIR}/src)
include_directories (BEFORE ${CMAKE_CURRENT_SOURCE_DIR}/include)
add_library (opt4D STATIC ${opt4D_LIBRARY_SRC})

set (opt4D_EXE_SRC
  src/opt4D.cpp
  )
add_executable (opt4Dexe ${opt4D_EXE_SRC})
target_link_libraries (opt4Dexe opt4D)
set_target_properties (opt4Dexe PROPERTIES OUTPUT_NAME opt4D)

set (unused_SRC
  src/blurrdij.cpp
  src/convertdij.cpp
  src/convertvoi.cpp
  src/convertvv.cpp
  src/dif2diagonaldij.cpp
  src/shrinkdij.cpp
  )
