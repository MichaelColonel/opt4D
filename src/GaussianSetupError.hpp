#ifndef GAUSSIANSETUPERROR_HPP
#define GAUSSIANSETUPERROR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <stdexcept>

// Predefine class before including others
class GaussianSetupError;

#include "Geometry.hpp"
#include "TxtFileParser.hpp"


// ---------------------------------------------------------------------
// Gaussian Setup Error
// ---------------------------------------------------------------------


/**
 * This class implements Gaussian setup errors.
 */
class GaussianSetupError
{

  public:
      class options
      {
          public:
              /// Constructor from TxtFileParser
              options(const TxtFileParser* pln_parser);

              /// Consructor from detailed data
              options(
                      unsigned int nFractions=1,
                      float systematic_sigmaX=0,
                      float systematic_sigmaY=0,
                      float systematic_sigmaZ=0,
                      float random_sigmaX=0,
                      float random_sigmaY=0,
                      float random_sigmaZ=0,
                      float cutoff=2)
                  : nFractions_(nFractions),
                  systematic_sigmaX_(systematic_sigmaX),
                  systematic_sigmaY_(systematic_sigmaY),
                  systematic_sigmaZ_(systematic_sigmaZ),
                  random_sigmaX_(random_sigmaX),
                  random_sigmaY_(random_sigmaY),
                  random_sigmaZ_(random_sigmaZ),
                  cutoff_(cutoff) {} ;

              /// the number of treatment fractions to model
              unsigned int nFractions_;

              /// The standard deviation of the systematic error (in mm)
              float systematic_sigmaX_, systematic_sigmaY_, systematic_sigmaZ_;

              /// The standard deviation of the random error (in mm)
              float random_sigmaX_, random_sigmaY_, random_sigmaZ_;

              /// The cutoff used for truncated Gaussian (normalized by sigma).
              float cutoff_;

              /// Write class to stream
              friend std::ostream & operator<< (
                      std::ostream& o, const GaussianSetupError::options& rhs);

      };

      /// Constructor
      GaussianSetupError(
              const Geometry *the_geometry,
              GaussianSetupError::options options);

      /// destructor
      ~GaussianSetupError();

      /// Write class to stream
      friend std::ostream & operator<< (
              std::ostream& o, const GaussianSetupError& rhs);

      /// Generate random shifts
      void generate_random_shifts(
              vector<Shift_in_Patient_Coord>& shifts) const;

      /// Generate nominal shifts
      void generate_nominal_shifts(
              vector<Shift_in_Patient_Coord>& shifts) const;
  private:
      /// Default constructor not allowed
      GaussianSetupError();

      /// gives gaussian random number
      float get_normal_distributed_random_number() const;

      /// gives gaussian random number from a truncated distribution
      float get_truncated_normal_distributed_random_number(
              float cutoff = 2) const;

      /// gives a 3D Gaussian setup error in a 2sigma elipse
      Shift_in_Patient_Coord get_truncated_gaussian_setup_error(
              float sigmaX, float sigmaY, float sigmaZ,
              float cutoff = 2) const;

      /// Hold a pointer to the Geometry
      const Geometry* the_geometry_;

      // Hold options
      GaussianSetupError::options options_;

};

#endif
