/**
 * @file SeparableObjective.hpp
 * SeparableObjective Class header
 *
 * $Id: SeparableObjective.hpp,v 1.6 2008/03/24 17:05:00 bmartin Exp $
 */

#ifndef SEPARABLEOROBJECTIVE_HPP
#define SEPARABLEOROBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class SeparableObjective is a virtual class that is inherited to build 
 * objectives.
 */
class SeparableObjective : public Objective
{

    public:

        // Constructor
        SeparableObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement);

        // Virtual destructor
        virtual ~SeparableObjective();


        //
        // SeparableObjective specific functions
        //
       
        /// Find out how many voxels were in objective
        virtual unsigned int get_nVoxels() const;

        /// Find out voxel sampling rate
        virtual float get_sampling_fraction() const;

        /// Find out if objective uses sampling
        virtual bool supports_voxel_sampling() const;

        virtual void set_sampling_fraction(const float sampling_fraction);

        size_t get_voiNo() const;

        //
        // Functions that must be re-implemented in children
        // 
        virtual double calculate_voxel_objective(
		float dose, unsigned int voxelNo, double nVoxels) const = 0;
        virtual double calculate_dvoxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const = 0;
        virtual double calculate_d2voxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const = 0;

        // Print a description of the objective
        virtual void printOn(std::ostream& o) const = 0;
        

        //
        // Functions reimplemented from Objective
        //

        virtual void initialize(DoseInfluenceMatrix& Dij);


        virtual double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );
        virtual double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );
        virtual double calculate_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                const vector<float> & v,
                vector<float> & Hv,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective based on just the dose vector provided
        // Returns zero if the objective does not suppor dose objectives
        virtual bool supports_calculation_via_dose_vector() const {return true;}; // objective can be calculated by supplying a dose vector

        virtual double calculate_dose_objective(
                const DoseVector & the_dose
                );

        // Calculate the gradient of the objective with respect to the dose vector
        virtual double calculate_dose_objective_and_gradient(
				const DoseVector & the_dose,
				DoseVector & dose_gradient);

        // incorporating uncertainty

        /// Does the objective support RandomScenario?
        virtual bool supports_RandomScenario() const;

        // Calculate the objective for one random scenario
        virtual double calculate_scenario_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective and gradient for one random scenario
        virtual double calculate_scenario_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & dij,
                BixelVectorDirection & gradient, 
                float gradient_multiplier,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling,
                double &estimated_ssvo
                );

        // Calculate the objective and gradient and Hv for one random scenario
        // virtual double calculate_scenario_objective_and_gradient_and_Hv(
                // const BixelVector & beam_weights,
                // DoseInfluenceMatrix & dij,
                // BixelVectorDirection & gradient,
                // float gradient_multiplier,
                // const vector<float> & v,
                // vector<float> & Hv,
                // const DoseDeliveryModel &dose_model,
                // const RandomScenario &random_scenario,
                // bool use_voxel_sampling,
                // double &estimated_ssvo
                // );

    protected:
        Voi *the_voi_;
        float sampling_fraction_;
        bool sample_with_replacement_;

    private:
        // Default constructor not allowed
        SeparableObjective();
};

/*
 * Inline functions
 */


inline
size_t SeparableObjective::get_voiNo() const
{
  return the_voi_->get_voiNo();
}


/**
 *  Find out if objective uses sampling
 */
inline
bool SeparableObjective::supports_voxel_sampling() const
{
  return true;
}

inline
bool SeparableObjective::supports_RandomScenario() const
{
  return true;
}
 

#endif
