/*
 * @file DaoVector.hpp
 * DaoVector Class Definition.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef DAOVECTOR_HPP
#define DAOVECTOR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>
#include <stdexcept>
#include <exception>
#include <limits>
using std::numeric_limits;

// Predefine class before including others
class DaoVector;

#include "ParameterVector.hpp"
#include "BixelVector.hpp"
#include "Aperture.hpp"
#include "Geometry.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "ApertureDoseInfluenceMatrix.hpp"


/**
 * Class DaoVector implements a parameter vector for direct aperture optimization
 */
class DaoVector : public ParameterVector
{
	
public:
	
  /// constructor, creates an empty DAO vector
  DaoVector(Geometry* geometry, DoseInfluenceMatrix* bixel_dij);
  
  /// destructor, is responsible for deleting the bixel vector
  //~DaoVector();
  
  /// return type
  ParameterVectorType getVectorType() const {
	return DAO_VECTOR_TYPE;
  }

  /// get a copy
  std::auto_ptr<ParameterVector> get_copy() const;

  /// translate the DAO vector into an effective bixel vector
  BixelVector * translateToBixelVector() const;

  /// Translate the gradient in terms of bixels to aperture weight gradients
  ParameterVectorDirection translateBixelGradient( BixelVectorDirection & bixel_gradient ) const;

  /// not implemented (would correspond to a sequencer)
  void translateBixelVector (BixelVector &bv);

  /// add most promising aperture 
  bool add_best_aperture(const BixelVectorDirection bixel_gradient);	

  /// get current number of apertures
  unsigned int get_nApertures() const;

  DoseInfluenceMatrix * get_aperture_dij();
  ParameterVector * get_bixel_vector();
  std::auto_ptr<ParameterVector> get_aperture_BixelVector() const;
  std::auto_ptr<ParameterVector> get_aperture_SquareRootBixelVector() const;

  // set the parameter values
  void synchronize(ParameterVector * aperture_weights);  

  // minor functions
  void set_zero_intensities();
  void set_unit_intensities();
  void scale_intensities(float scale_factor);
  unsigned int get_nBixels () const; 
  unsigned int get_beamNo (const int &ip) const;

  // input output
  void load_parameter_files (string parameters_file_root);
  void load_parameter_file (string parameters_file_root, unsigned int beamNo);
  void write_parameter_files (string parameters_file_root) const;
  void write_parameter_file (string parameters_file_root, const unsigned int beamNo) const;




private:
  
  /// synchronize the internal bixelvector with the current parameters
  void synchronize_bixel_vector() const;
  void synchronize_aperture_weight_vector() const;
  void synchronize_parameter_vector();


  /// add an aperture to the aperture-dij
  void add_aperture_to_dij(unsigned int apNo);

	/// initialize the parameters with the internal bixelvector
  //	void initialize();
	


  /// geometry information
  Geometry * geometry_;

  /// number of beams
  unsigned int nBeams_;

  /// test apertures, one for every beam
  vector<Aperture> test_apertures_;
  
  /// total number of apertures
  unsigned int nApertures_;
  
  /// the apertures
  vector<Aperture> apertures_;
  
  /// Dij Matrix containing the voxel dose contributions of the apertures
  ApertureDoseInfluenceMatrix aperture_dij_;
  
  /// the corresponding bixel dij
  DoseInfluenceMatrix * bixel_dij_;
  
  /// the corresponding bixel vector
  mutable BixelVector bixel_vector_;
  //  mutable BixelVector * bixel_vector_;
  
};
	


/// get current number of apertures
inline
unsigned int DaoVector::get_nApertures() const
{
  return nApertures_;
}

inline
DoseInfluenceMatrix * DaoVector::get_aperture_dij() 
{
  return &aperture_dij_;
}


#endif
