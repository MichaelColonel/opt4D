
/**
 * @file VvParser.cpp
 *
 * Function definitions for VvParser class.
 * 
 *  $Id: VvParser.cpp,v 1.1 2008/07/17 17:16:17 bmartin Exp $
 */

#include "VvParser.hpp"

#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <sstream>

const VvParser::header VvParser::reference_header_ = {
    {'v', 'v', '-', 'o', 'p', 't', '4', 'D'}, 25185, '\n',
    {'#', 'V', 'O', 'I', 's', ':'}
};

/**
 * Construct from a vector of vectors of indices (used for writing)
 *
 * @param voi_indice each voi is a vector of indices
 */
VvParser::VvParser(
        const vector<std::pair<string,vector<unsigned int> > > & vois)
: vois_(vois)
{
    // sort entries in each voi
    for(unsigned int i = 0; i < size(); i++) {
        std::sort(vois_[i].second.begin(), vois_[i].second.end());
    }
}


/**
 * Constructor to read from a file
 *
 * @param file_name The file to read
 */
VvParser::VvParser(
        const string & file_name)
: vois_()
{
    //
    // Attempt to open the file for reading
    //
    std::ifstream infile;
    infile.open(file_name.c_str(), std::ios::in | std::ios::binary);
    if (!infile.is_open()) {
        string temp = "Can't open file: " + file_name;
        std::cout << temp << std::endl;
        throw(std::runtime_error(temp));
    }
    std::cout << "Reading VOI data from file: " << file_name << std::endl;

    //
    // Read and validate file header
    //

    header m;
    infile.read( (char*)(&m), reference_header_size_);

    // do a byte-wise comparison of the two structs
    bool good = true;
    char* ptr1 = (char*) &m;
    char* ptr2 = (char*) &reference_header_;
    for(int i = 0; good && i <  reference_header_size_; i++) {
        good = ptr1[i] == ptr2[i];
    }
    if(!good) {
        string temp = "Error reading file: " + file_name
            + " (bad header)";
        ;
        std::cout << temp << std::endl;
        throw(std::runtime_error(temp));
    }

    // Read number of structures
    int nVois = 0;
    while(infile.peek() >= '0' && infile.peek() <= '9') {
        char c;
        infile.get(c);
        nVois *= 10;
        nVois += int(c - '0');
    }

    // Ignore end of line
    infile.ignore();

    // Allocate structures
    vois_.resize(nVois);

    // Read names of each structure
    for(unsigned int iVoi = 0; iVoi < nVois; iVoi++) {

        // Read number of the structure
        int voiNo = 0;
        infile >> voiNo;

        if(voiNo != iVoi) {
            std::cout << voiNo << '\t' << iVoi << '\n';
            string temp = "Error reading file: " + file_name
                + " (invalid VOI number)";

            std::cout << temp << std::endl;
            throw(std::runtime_error(temp));
        }

        // ignore space
        infile.ignore();

        // Read name
        char voi_name[80];
        infile.getline(voi_name, 80);
        vois_[iVoi].first = voi_name;
    }

    // Read voxels in each structure
    for(unsigned int iVoi = 0; iVoi < nVois; iVoi++) {
        // Read the number of voxels in the structure
        unsigned int nVoxels = 0;
        infile.read((char*)(&nVoxels),4);
        std::cout << "voi: " << iVoi << "(" << vois_[iVoi].first << ") has "
            << nVoxels << " voxels\n";

        // Allocate space to hold voxels
        vois_[iVoi].second.resize(nVoxels);

        int voxels_read = 0;
        while(voxels_read < nVoxels) {

            // Read each voxel, length pair
            struct {
                unsigned int voxelNo;
                unsigned char length;
            } group;
            infile.read((char*)(& group), 5);

            // opt4D expects lengths to be zero-offset, not one-offset
            group.length -= 1;

            // set voxels
            unsigned char offset = 0;
            while(offset <= group.length) {
                vois_[iVoi].second[voxels_read] = group.voxelNo + offset;
                ++voxels_read;
                ++offset;
            }
        }
    }
    std::cout << "At end of constructor\n";
}


// Write a voi file
void VvParser::write_file(
        const string & file_name
        ) const
{
    //
    // Attempt to open the file for writing
    //
    std::ofstream outfile;
    outfile.open(file_name.c_str(), std::ios::out | std::ios::binary);
    if (!outfile.is_open()) {
        string temp = "Can't open file: " + file_name;
        std::cout << temp << std::endl;
        throw(std::runtime_error(temp));
    }
    std::cout << "Writing VOI data to file: " << file_name << std::endl;

    outfile.write((char*)(&reference_header_), reference_header_size_);

    std::ostringstream oss;
    oss << size();
    string s = oss.str();
    while(s.size() < 5) {
        s.insert(s.begin(),'0');
    }
    outfile << s << '\n';
    
    // print out the name of each voi
    for(unsigned int i = 0; i < size(); i++) {
        std::ostringstream oss;
        oss << i;
        string s = oss.str();
        while(s.size() < 5) {
            s.insert(s.begin(),'0');
        }
        s.append(" ");
        s.append(vois_[i].first);
        s.append("\n");
        outfile.write(s.c_str(), s.size());
    }

    // write out entries in voi
    for(unsigned int i = 0; i < size(); i++) {
        unsigned int nVoxels = vois_[i].second.size();
        outfile.write((char*)(&nVoxels), sizeof(int));
        std::cout << "voi: " << i << "(" << vois_[i].first << ") has "
            << nVoxels << " voxels\n";
        
        if(nVoxels == 0) {
            continue;
        }

        // We know that the first entry is valid at least
        vector<unsigned int>::const_iterator iter = vois_[i].second.begin();

        while(iter != vois_[i].second.end()) {
            unsigned int range_start = *iter;
            unsigned char range_length = 0;

            // find first element after current range
            --range_length;
            do {
                ++ range_length;
                ++iter;
            } while((iter != vois_[i].second.end()) && 
                    (*iter == (range_start + range_length)) &&
                    (range_length < 0xFF));

            // write range
            outfile.write((char*)(&range_start), sizeof(range_start));
            outfile.write((char*)(&range_length), sizeof(range_length));
        }

    }
}

// Write a voi file
void VvParser::write_voi_file(const string & file_name,
			      const vector<unsigned int> & voilist,
			      const unsigned int nVoxels) const
{
  // 'vois' is a list of voi indices
  // later vois will overwrite earliers, so put highest priority last

  std::cout << "Writing .voi file: " << file_name << std::endl;

  // initialize vois with air
  vector<unsigned int> tempvoi(nVoxels,127);

  // loop over vois
  for(unsigned int i = 0; i < voilist.size(); i++) {

    //    std::cout << "index: " <<  << std::endl;

    vector<unsigned int>::const_iterator iter = vois_[voilist[i]].second.begin();

    while(iter != vois_[voilist[i]].second.end()) {
      
      tempvoi[*iter] = i;
      ++iter;
    }
  
  }

  std::ofstream outfile;

  // open file
  outfile.open(file_name.c_str(), std::ios::out | std::ios::binary);
  if (!outfile.is_open()) {
    std::cout << "Can't open file: " << file_name << std::endl;
    throw(std::runtime_error("cannot open file"));
  }

  //write new file
  for (unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
    outfile.write((char*) &(tempvoi[iVoxel]), sizeof(char));
  }

  // close file
  outfile.close();
}




/**
 * check if two VvParser objects are identical
 */
bool VvParser::operator ==(const VvParser & rhs) const {
    if(this == &rhs) {
        // Both objects are the same, so they must be equal
        return true;
    }
    unsigned int nVois = vois_.size();
    if(nVois != rhs.vois_.size()) {
        return false;
    }
    for(unsigned int i = 0; i < nVois; i++) {
        if(vois_[i].first != rhs.vois_[i].first) {
            return false;
        }
        unsigned int nVoxels = vois_[i].second.size();
        if(nVoxels != rhs.vois_[i].second.size()) {
            return false;
        }
        for(unsigned int j = 0; j < nVoxels; j++) {
            if(vois_[i].second[j] != rhs.vois_[i].second[j]) {
                return false;
            }
        }
    }
    return true;
};
