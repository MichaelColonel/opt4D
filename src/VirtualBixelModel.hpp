#ifndef VIRTUALBIXELMODEL_HPP
#define VIRTUALBIXELMODEL_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

// Predefine class before including others
class VirtualBixelModel;
class VirtualBixelSetupModel;
class VirtualBixelRangeModel;
class VirtualBixelRangeSetupModel;
class VirtualBixelRandomAndSystematicSetupModel;
class GaussianSystematicRangeError;
class GaussianSystematicSetupError;

#include "Geometry.hpp"
#include "DoseDeliveryModel.hpp"


// ---------------------------------------------------------------------
// Virtual bixel model base class
// ---------------------------------------------------------------------


/**
 * This class implements a base class for dose delivery models
 * which use virtual beamlets to approximate dose distributions.
 * In addition to the real beamlets which are delivered (physical beamlets)
 * additional beamlets are benerated which are only used to approximate
 * dose distributions for setup or range errors
 */
class VirtualBixelModel : public DoseDeliveryModel
{

  public:
    /// constructor
    VirtualBixelModel(const Geometry *the_geometry,
		      const string dij_file_root,
		      const string stf_file_root);

    /// destructor
    virtual ~VirtualBixelModel();

    /// calculate voxel dose for a scenario
    float calculate_voxel_dose(unsigned int voxelNo,
			       const BixelVector &bixel_grid,
			       DoseInfluenceMatrix &dij,
			       const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

  protected:

    /// get subscripts of a physical bixels in the virtual bixel grid
    inline Bixel_Subscripts get_physical_bixel_subscripts(unsigned int bixelNo) const {
	return physical_bixel_subscripts_.at(bixelNo); };

    /// generate virtual bixel transformation matrix based on patient shift
    virtual void generate_virtual_bixel_trafo_matrix(RandomScenario &random_scenario) const = 0;

    /// The geometry for the virtual bixel grid
    std::auto_ptr<Geometry> virtual_geometry_;

    /// The virtual bixel vector
    std::auto_ptr<BixelVector> virtual_bixels_;

    /// The dose influence matrix containing the virtual bixels
    std::auto_ptr<DoseInfluenceMatrix> virtual_dij_;

    /// the subscripts of the physical bixels in the virtual bixel grid
    vector<Bixel_Subscripts> physical_bixel_subscripts_;

  private:

    /// Make sure default constructor is never used
    VirtualBixelModel();
};



// ---------------------------------------------------------------------
// Systematic setup uncertainty
// ---------------------------------------------------------------------


/**
 * This class implements systematic setup errors. Dose distributions in the shifted patient
 * are approximated by neighboring virtual beamlets.
 */
class VirtualBixelSetupModel : public VirtualBixelModel
{

  public:
    /// constructor
    VirtualBixelSetupModel(const Geometry *the_geometry,
			   const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~VirtualBixelSetupModel();

    /// generate a random scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const = 0;

    /// generate nominal scenario
    void generate_nominal_scenario(RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    bool set_dose_evaluation_scenario(map<string,string> scenario_map);

  protected:
    /// generate virtual bixel transformation matrix based on patient shift
    void generate_virtual_bixel_trafo_matrix(RandomScenario &random_scenario) const;

  private:
    /// Make sure default constructor is never used
    VirtualBixelSetupModel();
};


/**
 * Gaussian systematic setup error
 */
class GaussianSystematicSetupError
{

  public:
    /// constructor
    GaussianSystematicSetupError(const DoseDeliveryModel::options *options);

    /// gives gaussian random number from a truncated distribution
    float get_truncated_normal_distributed_random_number(float width, float cutoff = 2) const;

    /// gives a 3D Gaussian setup error in a 2sigma elipse
    Shift_in_Patient_Coord get_truncated_gaussian_setup_error(float cutoff = 2) const;

  private:

    /// Compare two floats using an absolute tolerance
    bool comp_float_abs(float a, float b, float epsilon) const;

    // magnitude of uncertainty
    float sigmaX_;
    float sigmaY_;
    float sigmaZ_;
};

/**
 * Implementation of a systematic setup error as a continuous Gaussian distributed error
 */
class ContinuousVirtualBixelSetupModel : public VirtualBixelSetupModel
{

  public:
    /// constructor
    ContinuousVirtualBixelSetupModel(const Geometry *the_geometry,
				     const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~ContinuousVirtualBixelSetupModel();

    /// get type of uncertainty model
    string get_uncertainty_model() const {return("ContinuousVirtualBixelSetupModel");};

    /// generate a random scenario
    void generate_random_scenario(RandomScenario &random_scenario) const;

    /// write customized results
    void write_dose(BixelVector &bixel_grid,
		    DoseInfluenceMatrix &dij,
		    string file_prefix,
		    Dvh dvh) const;

  protected:

  private:
    /// Make sure default constructor is never used
    ContinuousVirtualBixelSetupModel();

    /// use a continuous gaussian setup error
    GaussianSystematicSetupError setup_uncertainty_;

};





// ---------------------------------------------------------------------
// Range uncertainty
// ---------------------------------------------------------------------


/**
 * This class implements range uncertainties. Dose distributions in the patient
 * are approximated using virtual beamlets in neighboring energy layers.
 */
class VirtualBixelRangeModel : public VirtualBixelModel
{

  public:
    /// constructor
    VirtualBixelRangeModel(const Geometry *the_geometry,
			   const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~VirtualBixelRangeModel();

    /// generate nominal scenario
    void generate_nominal_scenario(RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    bool set_dose_evaluation_scenario(map<string,string> scenario_map);

  protected:
    /// generate virtual bixel transformation matrix based on patient shift
    void generate_virtual_bixel_trafo_matrix(RandomScenario &random_scenario) const;

  private:
    /// Make sure default constructor is never used
    VirtualBixelRangeModel();

};

/**
 * Gaussian systematic setup error
 */
class GaussianSystematicRangeError
{

  public:
    /// constructor
    GaussianSystematicRangeError(const Geometry *the_geometry,
				 const Geometry *virtual_geometry,
				 const DoseDeliveryModel::options *options);

    /// gives gaussian random number from a truncated distribution
    float get_truncated_normal_distributed_random_number(float width, float cutoff = 2) const;

    /// generate Gaussian range error for each beamlet
    void generate_gaussian_range_error(vector<float> &range_shifts) const;

    /// generate Gaussian range error for each beamlet depending on setup error
    void generate_gaussian_range_error(vector<float> &range_shifts, Shift_in_Patient_Coord shift) const;

    /// generate Gaussian range error for each beamlet for a sigma of one
    void generate_normalized_gaussian_range_error(vector<float> &range_shifts) const;

    /// read file containing range uncertainty for each bixel.
    void read_bixel_range_uncertainty(const string sigma_file_root);

    /// set range uncertainty of physical beamlets based on the virtual beamlets
    void set_physical_bixel_range_uncertainty();

  private:

    /// returns true if bixels j and k are correlated
    bool bixels_correlated(unsigned int BixelNo_j,unsigned int BixelNo_k) const;

    /// pointer to Geometry
    const Geometry* the_geometry_;

    /// pointer to virtual bixel geometry
    const Geometry* virtual_geometry_;

    /// correlation model for beamlets
    string correlation_model_;

    /// magnitude of range uncertainty for each beamlet
    vector<float> sigmaRange_;

    /// magnitude of range uncertainty for each virtual beamlet
    vector<float> virtual_sigmaRange_;
};

/**
 * Implementation of a systematic range error as a continuous Gaussian distributed error
 */
class ContinuousVirtualBixelRangeModel : public VirtualBixelRangeModel
{

  public:
    /// constructor
    ContinuousVirtualBixelRangeModel(const Geometry *the_geometry,
				     const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~ContinuousVirtualBixelRangeModel();

    /// get type of uncertainty model
    string get_uncertainty_model() const {return("ContinuousVirtualBixelRangeModel");};

    /// generate a random scenario
    void generate_random_scenario(RandomScenario &random_scenario) const;

    /// write customized results
    void write_dose(BixelVector &bixel_grid,
		    DoseInfluenceMatrix &dij,
		    string file_prefix,
		    Dvh dvh) const;

  protected:

  private:
    /// Make sure default constructor is never used
    ContinuousVirtualBixelRangeModel();

    /// use a continuous gaussian range error
    GaussianSystematicRangeError range_uncertainty_;

};



// ---------------------------------------------------------------------
// Range and Setup uncertainty
// ---------------------------------------------------------------------


/**
 * This class implements range uncertainties. Dose distributions in the patient
 * are approximated using virtual beamlets in neighboring energy layers
 * and lateral positions.
 */
class VirtualBixelRangeSetupModel : public VirtualBixelModel
{

  public:
    /// constructor
    VirtualBixelRangeSetupModel(const Geometry *the_geometry,
				const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~VirtualBixelRangeSetupModel();

    /// generate nominal scenario
    void generate_nominal_scenario(RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    bool set_dose_evaluation_scenario(map<string,string> scenario_map);

  protected:
    /// generate virtual bixel transformation matrix based on patient shift
    void generate_virtual_bixel_trafo_matrix(RandomScenario &random_scenario) const;

  private:
    /// Make sure default constructor is never used
    VirtualBixelRangeSetupModel();
};

/**
 * Implementation of a systematic range and setup error as a continuous Gaussian distributed error
 */
class ContinuousVirtualBixelRangeSetupModel : public VirtualBixelRangeSetupModel
{

  public:
    /// constructor
    ContinuousVirtualBixelRangeSetupModel(const Geometry *the_geometry,
					  const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~ContinuousVirtualBixelRangeSetupModel();

    /// get type of uncertainty model
    string get_uncertainty_model() const {return("ContinuousVirtualBixelRangeSetupModel");};

    /// generate a random scenario
    void generate_random_scenario(RandomScenario &random_scenario) const;

    /// write customized results
    void write_dose(BixelVector &bixel_grid,
		    DoseInfluenceMatrix &dij,
		    string file_prefix,
		    Dvh dvh) const;

  protected:

  private:
    /// Make sure default constructor is never used
    ContinuousVirtualBixelRangeSetupModel();

    /// use a continuous gaussian range error
    GaussianSystematicRangeError range_uncertainty_;

    /// use a continuous gaussian setup error
    GaussianSystematicSetupError setup_uncertainty_;
};

/**
 * Implementation of a systematic range and setup error as a continuous Gaussian distributed error
 */
class DiscreteVirtualBixelRangeSetupModel : public VirtualBixelRangeSetupModel
{

  public:
    /// constructor
    DiscreteVirtualBixelRangeSetupModel(const Geometry *the_geometry,
					const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~DiscreteVirtualBixelRangeSetupModel();

    /// get type of uncertainty model
    string get_uncertainty_model() const {return("DiscreteVirtualBixelRangeSetupModel");};

    /// generate a random scenario
    void generate_random_scenario(RandomScenario &random_scenario) const;

    /// returns true
    //bool supports_calculate_exact_objective() const {return true;};
    bool has_discrete_scenarios() const {return true;};

    /// write customized results
    void write_dose(BixelVector &bixel_grid,
		    DoseInfluenceMatrix &dij,
		    string file_prefix,
		    Dvh dvh) const;

  protected:

  private:
    /// Make sure default constructor is never used
    DiscreteVirtualBixelRangeSetupModel();
};


// ---------------------------------------------------------------------
// Random and systematic setup errors
// ---------------------------------------------------------------------


/**
 * This class implements random and systematic setup errors. Dose distributions in the shifted patient
 * are approximated by neighboring virtual beamlets.
 */
class VirtualBixelRandomAndSystematicSetupModel : public VirtualBixelModel
{

  public:
    /// constructor
    VirtualBixelRandomAndSystematicSetupModel(const Geometry *the_geometry,
					      const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~VirtualBixelRandomAndSystematicSetupModel();

    /// returns type of uncertainty model
    std::string get_uncertainty_model() const {
	return("VirtualBixelRandomAndSystematicSetupModel");}

    /// generate a random sample scenario
    void generate_random_scenario(RandomScenario &random_scenario) const;

    /// generate nominal scenario
    void generate_nominal_scenario(RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    bool set_dose_evaluation_scenario(vector<string> scenario_str);

    /// write results
    void write_dose(BixelVector &bixel_grid,
		    DoseInfluenceMatrix &dij,
		    string file_prefix,
		    Dvh dvh) const;

    /// get number of fractions
    inline unsigned int get_nFractions() const {return nFractions_;};

  protected:

  private:

    /// Make sure default constructor is never used
    VirtualBixelRandomAndSystematicSetupModel();

    /// generate a setup error scenario
    void generate_setup_errors(RandomScenario &random_scenario, unsigned int nSamples) const;

    /// generate virtual bixel transformation matrix based on patient shift
    void generate_virtual_bixel_trafo_matrix(RandomScenario &random_scenario) const;

    /// number of fractions
    unsigned int nFractions_;

    // magnitude of systematic error
    float sigmaX_sys_;
    float sigmaY_sys_;
    float sigmaZ_sys_;

    // magnitude of random error
    float sigmaX_rand_;
    float sigmaY_rand_;
    float sigmaZ_rand_;
};



#endif

