
/**
 * @file Lbfgs.hpp
 * Lbfgs class Header
 *
 */

#ifndef LBFGS_HPP
#define LBFGS_HPP

class Lbfgs;

#include <iostream>
#include <stdexcept>
#include <deque>
#include "Optimizer.hpp"
#include "ParameterVectorDirection.hpp"

using std::deque;
using std::cout;
using std::endl;

/**
 * Class Lbfgs is an optimizer object to implement the limited memory
 * BFGS Quasi Newton algorithm
 *
 * It stores the gradient and the parameter vector of the last m steps
 * to calculate the descent direction for the next step.
 *
 * It only needs the gradient and the current parameter vector as input.
 */
class Lbfgs : public Optimizer
{

public:

  class HistoryPoint;
  class options;

  /// Constructor
  Lbfgs(Lbfgs::options options, unsigned int nParams);
  
  /// Destructor (default)
  ~Lbfgs();
  
  /// delete history
  void reset_optimizer();

  /// reset the last stored gradient
  void reset_optimizer(const ParameterVectorDirection & gradient);

  /// Calculate the direction based on current gradient and parameter vector
  void calculate_direction(const ParameterVectorDirection & gradient,
			   const ParameterVector & param);
  
  /// Print out options (no options for CG class)
  void print_options(std::ostream& o) const {};

  class HistoryPoint 
  {
  public:
    /// constructor 
    HistoryPoint();
    HistoryPoint(unsigned int nParams);

    /// parameter vector differences
    ParameterVectorDirection diff_param_;

    /// gradient differences
    ParameterVectorDirection diff_grad_;

    /// one over gradient difference times parameter difference
    float product_;
    void calculate_product();

  };

  class options
  {
  public:
    /// constructor
    options();

    /// number of past gradients to estimate the hessian from
    unsigned int nHistory_;
  };
  
private:

  /// options and parameters
  Lbfgs::options options_;

  /// contains previous parameter vectors / gradients
  deque<HistoryPoint*> history_;

  /// last parameter vector
  ParameterVectorDirection last_param_;

  /// last gradient
  ParameterVectorDirection last_gradient_;
 
  /// previous parameter vector differences
  //  deque<std::auto_ptr<ParameterVectorDirection> > diff_grad_;
  
  /// previous parameter vector differences
  //  deque<std::auto_ptr<ParameterVector> > diff_param_;
  
  /// one over diff_grad_ times diff_param_
  //  deque<float> grad_times_param_;

  bool is_initialized_;
};

#endif

