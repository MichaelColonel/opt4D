/**
 * @file GenericConstraint.hpp
 * Generic Class header
 *
 */

#ifndef GENERICCONSTRAINT_HPP
#define GENERICCONSTRAINT_HPP

#define OPT4D_INFINITY 1e30
#define OPT4D_EPSILON 1e-7

class GenericConstraint;
class GenericOptimizationData;

#include <vector>
using std::vector;

// define constraint types
enum BoundType {FREE, LOWERBOUND, UPPERBOUND, TWOSIDED, EQUALITY};



/**
 * Class to hold information about a generic constraint
 */
class GenericConstraint
{     

public:
  
  /// Constructor for unknown number of nonzeros
  GenericConstraint(); 

  /// destructor
  ~GenericConstraint(); 

  /// Constructor
  GenericConstraint(unsigned int nNonZeros); 

  /// initialize
  void initialize(unsigned int nNonZeros); 

  /// number of nonzero coefficients
  unsigned int nNonZeros_;

  /// index to the variable
  int *index_;

  /// value of the coefficients
  double *value_;

  /// upper bound
  double upper_bound_;

  /// lower bound
  double lower_bound_;

  /// constraint type
  BoundType type_;

};


/**
 * Class to hold information of a set of constraints
 */
class GenericOptimizationData
{

public:

    /// Constructor
    GenericOptimizationData();

    /// Constructor
    GenericOptimizationData(unsigned int nVarInt,
			    unsigned int nVarAux,
			    unsigned int nConstraints);

    /// Destructor
    ~GenericOptimizationData();

    /// initialize
    void initialize(unsigned int nVarInt,
		    unsigned int nVarAux,
		    unsigned int nConstraints);


    /// number of intrinsic variables
    unsigned int nVarInt_;
    
    /// number of auxiliary variables
    unsigned int nVarAux_;
 
    /// total number of variables
    unsigned int nVar_;
 
    /// upper bound of variables
    double* upper_var_bound_;

    /// lower bound for variables
    double* lower_var_bound_;

    /// type of variable bound
    BoundType* var_bound_type_;

     /// number of constraints
    unsigned int nConstraints_;
      
    /// vector containing the individual constraints
    std::vector<GenericConstraint> constraints_;

    /// total number of nonzero coefficients in constraints
    unsigned int nNonZeros_;

    /// vector containing the coefficients in the objective function
    double* obj_coeffs_;

    /// number of constraints added so far
    unsigned int nConstraints_until_now_;

    /// number of auxiliary variables added so far
    unsigned int nAuxiliaries_until_now_;
};



#endif



