
/**
 * @file Optimizer.hpp
 * Optimizer Class Header
 *
 * $Id: Optimizer.hpp,v 1.1 2007/09/20 19:03:08 bmartin Exp $
 */

#ifndef OPTIMIZER_HPP
#define OPTIMIZER_HPP

class Optimizer;

#include <stdexcept>
#include "ParameterVectorDirection.hpp"


/**
 * Class Optimizer is an abstract base class that allows for the implementation
 * of any optimization algorithm that needs just the gradient, the gradient and
 * the diagonal hessian, or the gradient and the hessian times a vector.
 *
 * The optimizer classes are used to calculate the direction for each step.  On
 * the first time calculate_direction is called, the first direction is
 * calculated and the optimization is initialized.
 */
class Optimizer
{

    public:

	// Default destructor is fine
	virtual ~Optimizer() {};

	/// Calculate the direction based on the gradient.  If anything else is
	/// needed, it should have been passed in seperately
	virtual void calculate_direction(
		const ParameterVectorDirection & gradient,
		const ParameterVector & param) = 0;
 
	/// Get the calculated direction
	virtual const ParameterVectorDirection & get_direction() const
	{   return the_direction_;  };

	/// Query which data the optimization type needs for the current step
	virtual bool uses_diagonal_hessian() const {return false;};

	/// Get the vector to hold diagonal hessian
	virtual vector<float> & get_diagonal_hessian_vector()
	{
	    throw std::logic_error("Called unimplemented get_hessian_vector");
	}

	/// Query which data the optimization type needs for the current step
	virtual bool uses_hessian_times_vector() const {return false;};

	/// Get the vector to multiply the hessian by
	virtual const vector<float> & get_v_vector()
	{
	    throw std::logic_error("Called unimplemented get_hessian_vector");
	}

	/// Get the vector to hold HV
	virtual vector<float> & get_Hv_vector()
	{
	    throw std::logic_error("Called unimplemented get_hessian_vector");
	}

	/// Print out options
	virtual void print_options(std::ostream& o) const = 0;

    /// resets the optimizer to some initial state, does nothing if not implemented in derived class
    virtual void reset_optimizer() {};
    virtual void reset_optimizer(const ParameterVectorDirection & gradient) {};

    protected:

	/// Constructor with size of direction vector (only accessable to
	/// derived classes)
	Optimizer(unsigned int nParams):the_direction_(nParams) {};

	/// The direction of the next step
  //	BixelVectorDirection the_direction_;
	ParameterVectorDirection the_direction_;

    private:

	// Default constructor not allowed
	Optimizer();

};


#endif
