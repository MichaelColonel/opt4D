/**
 * @file BixelPositivityConstraint.hpp
 * BixelPositivityConstraint Class header
 *
 */

#ifndef BIXELPOSITIVITYCONSTRAINT_HPP
#define BIXELPOSITIVITYCONSTRAINT_HPP

#include "Constraint.hpp"
#include "Voi.hpp"
#include <cmath>
#include <limits>


/**
 * Class BixelPositivityConstraint implements the positivity constraint for bixel weights
 */
class BixelPositivityConstraint : public Constraint
{

public:
  
  // Constructor
  BixelPositivityConstraint(unsigned int consNo, float penalty);
  
  // Virtual destructor
  ~BixelPositivityConstraint();
  
  /// Print a description of the constraint
  void printOn(std::ostream& o) const;

  /// initialize the Constraint
  void initialize(DoseInfluenceMatrix& Dij);
  
  /// set upper bound of the constraint
  void set_upper_bound(float value) {
    // do nothing
  };

  /// get upper bound of the constraint
  float get_upper_bound() const {return std::numeric_limits<float>::max();};

  /// set lower bound of a constraint
  void set_lower_bound(float value) {
    // do nothing
  };

  /// set lower bound of a constraint
  float get_lower_bound() const {return 0;};


  //
  // specific functions for augmented lagrangian approach not implemented
  // this is dealt with through projection
  //

  bool supports_augmented_lagrangian() const {return(true);};

  /// calculate the augmented lagrangian function
  double calculate_aug_lagrangian(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij);

  /// calculate the augmented lagrangian function and its gradient
  double calculate_aug_lagrangian_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier);

  /// update lagrange parameter
  void update_lagrange_multipliers(const BixelVector & beam_weights,
				   DoseInfluenceMatrix & Dij);

  /// update penalty parameter
  void update_penalty(const BixelVector & beam_weights,
					  DoseInfluenceMatrix & Dij, 
					  float tol, 
					  float multiplier);



  //
  // specific functions for projection method
  //
  
  bool supports_projection_solver() const {return(true);};

  /// sequentially project onto (beyond) the constraints
  unsigned int project_onto(BixelVector & beam_weights,
			    DoseInfluenceMatrix & Dij);

  /// ckeck if active set is empty
  bool is_active_set_empty() const {return !in_active_set_;};

  /// reset active set
  void reset_active_set() {in_active_set_ = true;};


  //
  // specific functions for solver interface not implemented
  // this is dealt with elsewhere
  //

private:

  // Default constructor not allowed
  BixelPositivityConstraint();

  /// penalty factor
  vector<float> penalty_;

  /// initial penalty factor
  float initial_penalty_;

  /// lagrange multiplier
  vector<float> lagrange_;

  /// defines the active set of constraints
  bool in_active_set_;

};


inline
unsigned int BixelPositivityConstraint::project_onto(BixelVector & beam_weights,
						     DoseInfluenceMatrix & Dij)
{
  //  cout << "projecting onto constraint " << get_consNo() << ": ";

  unsigned int nProjections = 0;
  for(unsigned int i=0; i<beam_weights.get_nBixels(); i++) {
    if(beam_weights.get_intensity(i) < 0) {
      beam_weights.set_intensity(i,-1*beam_weights.get_intensity(i));
      nProjections++;
    }
  }

  if(nProjections==0) {
    in_active_set_ = false;
  }
  else {
    in_active_set_ = true;
  }
  return nProjections;
}







#endif
