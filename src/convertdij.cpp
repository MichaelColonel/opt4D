/**
 * @file convertdij.cpp
 * The main file for convertdij.
 *
 * converts voxel numbering from CT coordinate system to opt4D system
 *
 */

/**
 * \par Welcome to convertdij
 *
 * convertdij is a utility to perform convert a dij matrix in CT coordinate system to opt4D system. 
 * This flips the y axis and then interchanges y and z axis.  Type convertdij --help for usage information.
 */

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <iomanip>
#include <exception>
#include <fstream>
#include <limits>
#include <tclap/CmdLine.h>
#include <string>
using std::string;

#include "DijFileParser.hpp"

void convert_dijfile(string in_file_name,
		     string out_file_name,
		     float dose_scale_factor,
		     bool change_bixel_coord);

int convert_voxel_index(int voxelNo, int nx, int ny, int nz); 

/**
 * Main function to interpret command line options and run program.
 */
int main(int argc, char* argv[])
{

    // Wrap everything in a try block to catch TCLAP exceptions.
    try {

        /*
         * Defining the command line syntax
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         *
         * We define the command line object, and add various switches and 
         * value arguments to it.  The CmdLine object parses the argv array 
         * based on the Arg objects that it contains.
         */

        // Define the command line object and insert a message
        // that describes the program. This is printed last in the help text.  
        // The second argument is the delimiter (usually space) and the last 
        // one is the version number.   
        TCLAP::CmdLine cmd("convertdij voxel coordinate system conversion tool for dij",
                ' ', "0.1");

        /*
         * TCLAP syntax
         * ^^^^^^^^^^^^
         *          
         * TCLAP reads two kinds of command line parameters, value arguments 
         * and flags.  Value argments are used to pass data to the program 
         * (such as file names).  Flags are used to pass true or false 
         * information to the program.
         *
         * SwitchArg
         * ---------
         * Switches are used to tell the program something true or false, but 
         * must be false by default, so the default behavior comes from not 
         * using any flags at all.
         *
         * SwitchArg constructor:
         *
         * TCLAP::SwitchArg::SwitchArg  ( const std::string &   flag,
         *                                const std::string &   name,
         *                                const std::string &   desc,
         *                                CmdLineInterface &    parser)
         *
         * Parameters:
         *     flag   - The one character flag that identifies this argument
         *              on the command line.
         *     name   - A one word name for the argument. Can be used as a
         *              long flag on the command line.
         *     desc   - A description of what the argument is for or does.
         *     parser - A CmdLine parser object to add this Arg to
         *
         *
         * ValueArg
         * --------
         * Value arguments can read any type data into a variable that you 
         * create.  Most arguments use this format.
         *
         * ValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::ValueArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * Parameters:
         *     flag     - The one character flag that identifies this argument
         *                on the command line.
         *     name     - A one word name for the argument. Can be used as a
         *                long flag on the command line.
         *     desc     - A description of what the argument is for or does.
         *     req      - Whether the argument is required on the command line.
         *     value    - The default value assigned to this argument if it is
         *                not present on the command line.
         *     typeDesc - A short, human readable description of the type that
         *                this object expects. This is used in the generation 
         *                of the USAGE statement. The goal is to be helpful to 
         *                the end user of the program.
         *      parser  - A CmdLine parser object to add this Arg to
         *
         * UnlabledValueArg
         * ----------------
         *
         * The plan file name is read by an UnlabledValueArg, so any argument 
         * that is not processed by something else will be assumed to be the 
         * plan file name.  Don't add any other UnlabledValueArg arguments!!!
         *
         * UnlabledValueArg constructor:
         * 
         * template<class T>
         * TCLAP::ValueArg< T >::UnlabledValueArg (
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         *
         * MultiArg
         * --------
         *
         * If several values may be passed in for one argument, use a MultiArg, 
         * which returns a vector of values instead of a single one.
         *
         * MultiArg syntax:
         *
         * template<class T>
         * TCLAP::MultiArg< T >::MultiArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * UnlabeldMultiArg
         * ----------------
         *
         *  Syntax:
         *
         *  template<class T>
         *  TCLAP::UnlabledMultiArg< T >::UnlabeledMultiArg (
         *              const std::string & name,
         *              const std::string & desc,
         *              bool                req,
         *              const std::string & typeDesc,
         *              CmdLineInterface &  parser,
         *              bool                ignoreable=false)
 	 *
         */

        /*
         * Add General options to cmd
         */

        // Input file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> in_file_prefix_arg("i",
                "infile_prefix","Input file prefix",
                false, "","dij file prefix",cmd);

        // Output file prefix in case of processing a group of files
        TCLAP::ValueArg<std::string> out_file_prefix_arg("o",
                "outfile_prefix","Output file prefix (defaults to converted_)",
                false, "", "dij file prefix",cmd);

        // Output file prefix in case of processing a group of files
        TCLAP::ValueArg<float> dose_scale_factor_arg("d",
                "dose_scale_factor","dose scale factor",
                false, 1, "any positive float",cmd);

        // interchange also x and y in the bixel grid
        TCLAP::SwitchArg dont_change_bixel_coord_switch("b","dont_change_bixel_coord",
                "rotate x-y coordinate stystem by 90 degrees", cmd);

        TCLAP::UnlabeledMultiArg<std::string> in_file_names_arg(
                "infile","Input file names",false,"dij file names",cmd);

        // Parse the command line
        cmd.parse(argc, argv);

	float dose_scale_factor = 1.0;
	if(dose_scale_factor_arg.isSet()) {
	    dose_scale_factor = dose_scale_factor_arg.getValue();
	}

	bool change_bixel_coord = true;
	if(dont_change_bixel_coord_switch.isSet()) {
	    change_bixel_coord = false;
	}

        string out_file_prefix = out_file_prefix_arg.getValue();

        if(in_file_prefix_arg.isSet()) {
            // In file prefix is supplied, so convert any files that can be 
            // found

            string in_file_prefix = in_file_prefix_arg.getValue();

            // Find out file prefix
            string out_file_prefix;
            if(out_file_prefix_arg.isSet()) {
                out_file_prefix = out_file_prefix_arg.getValue();
            } else {
                out_file_prefix = "converted_" + in_file_prefix;
            }

            unsigned int instanceNo = 0;
            unsigned int beamNo = 0;
            while(true) {

                // Find file names
                string in_file_name;
                string out_file_name;
                char cbeam[10];
                sprintf(cbeam,"%d",beamNo + 1);
                if(instanceNo == 0) {
                    in_file_name = in_file_prefix
                        + "_" + string(cbeam) + ".dij";
                    out_file_name = out_file_prefix
                        + "_" + string(cbeam) + ".dij";
                } else {
                    char cinstance[10];
                    sprintf(cinstance,"%d",instanceNo);
                    in_file_name = in_file_prefix
                        + "_" + string(cinstance) 
                        + "_" + string(cbeam) + ".dij";
                    out_file_name = out_file_prefix
                        + "_" + string(cinstance) 
                        + "_" + string(cbeam) + ".dij";
                }

                try {
                    convert_dijfile( in_file_name, out_file_name, dose_scale_factor, change_bixel_coord);
		    beamNo++;
                }
                catch (DijFileParser::exception & e) {
                    if(beamNo == 0) {
                        // We're done since we're at an instance with no beams
                        break;
                    } else {
                        // We found at least one beam before failing, so go to 
                        // next instance
                        beamNo = 0;
                        instanceNo++;
                    }
                }
            }
        }

        if(in_file_names_arg.getValue().size() > 0) {
            // Input files are supplied, so convert them

            // Find out file prefix
            string out_file_prefix;
            if(out_file_prefix_arg.isSet()) {
                out_file_prefix = out_file_prefix_arg.getValue();
            } else {
                out_file_prefix = "converted_";
            }

            vector<string>::const_iterator iter;
            for(iter = in_file_names_arg.getValue().begin();
                    iter != in_file_names_arg.getValue().end();
                    iter++) {
                try {
                    convert_dijfile( *iter, 
				     out_file_prefix + *iter, 
				     dose_scale_factor,
				     change_bixel_coord);
                }
                catch (DijFileParser::exception & e) {
                    cerr << "Error while convering file: " << *iter << "\n";

                }
            }
        }

    }
    catch (TCLAP::ArgException &e) { // catch any exceptions
        std::cerr << "error: " << e.error() << " for arg " << e.argId()
            << std::endl;
    }
    catch (std::exception & e) { // Catch other exceptions
        std::cerr << "Error caught in convertdij.cpp, main(): " << e.what()
                                                              << std::endl;
    }

    return(0);
}


/**
 * Convert a dij file with the given name from CT to opt4D coordinate system.  Throws an exception if 
 * the input file does not exist or the output cannot be written to.
 *
 * @param in_file_name The file to read
 * @param out_file_name The file to write
 */
void convert_dijfile(
        string in_file_name,
        string out_file_name,
	float dose_scale_factor,
	bool change_bixel_coord)
{
    DijFileParser::header in_head;
    DijFileParser::header out_head;
    DijFileParser::bixel in_bixel;
    DijFileParser::bixel out_bixel;

    // Open infile for reading
    DijFileParser in_file(in_file_name);

    // get header
    in_head = in_file.get_header();

    cout << "working on " << in_file_name << endl; 
    cout << "number of beamlets: " << in_head.npb_ << endl;

    // copy header
    out_head = in_head;

    // interchange dimensions in y and z
    out_head.ny_ = in_head.nz_;
    out_head.nz_ = in_head.ny_;

    // interchange voxel dimensions in y and z
    out_head.dy_ = in_head.dz_;
    out_head.dz_ = in_head.dy_;

    // modify scale factor
    out_head.scalefactor_ = in_head.scalefactor_ * dose_scale_factor;

    // change bixel grid
    if(change_bixel_coord) {
      out_head.dx_b_ = in_head.dy_b_;
      out_head.dy_b_ = in_head.dx_b_;
    }

    // Open outfile for writing
    DijFileParser out_file(out_file_name, out_head);

    // Process each bixel
    for(int iBixel = 0; iBixel < in_head.npb_; iBixel++) {

        // Read bixel
        in_file.read_next_bixel(in_bixel);

	out_bixel = in_bixel;

	// change bixel coordinates
	if(change_bixel_coord) {
	    out_bixel.spot_y_ = - in_bixel.spot_x_;
	    out_bixel.spot_x_ = in_bixel.spot_y_;
	}

        // Do voxel number conversion
	for (unsigned int iEntry = 0; iEntry<in_bixel.voxelNo_.size(); iEntry++) {

	    out_bixel.voxelNo_[iEntry] = 
		convert_voxel_index(in_bixel.voxelNo_[iEntry], out_head.nx_, out_head.ny_, out_head.nz_);
        }

        // Write out bixel
        out_file.write_next_bixel(out_bixel);
    }

    // Completed successfully, so report success
    cout << "done" << endl; 
}
  
int convert_voxel_index(int voxelNo, int nx, int ny, int nz) 
{
    int tempcx,tempcy,tempcz;
    int new_voxelNo;

    tempcx = voxelNo % nx;
    tempcy = (voxelNo/nx) / (nz);
    tempcz = (nz-1) - ( (voxelNo/nx) % (nz) );

    new_voxelNo = tempcx+tempcy*nx+tempcz*nx*ny;

    assert(new_voxelNo<nx*ny*nz);
    assert(new_voxelNo>=0);

    return (new_voxelNo);
}

