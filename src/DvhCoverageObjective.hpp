/**
 * @file DvhCoverageObjective.hpp
 * DvhCoverageObjective Class header
 *
 * $Id: DvhCoverageObjective.hpp,v 1.2 2008/03/14 15:00:26 bmartin Exp $
 */

#ifndef DVHCOVERAGEOBJECTIVE_HPP
#define DVHCOVERAGEOBJECTIVE_HPP

#include "NonSeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class DvhCoverageObjective is an indicator objective.  It returns one if the 
 * fraction of the VOI that recieves the given dose is above or below the 
 * minimum.  It returns zero otherwise.
 *
 * Does not set any gradient, so it cannot be used for optimization.
 */
class DvhCoverageObjective : public NonSeparableObjective
{

    public:

        // Constructor
        DvhCoverageObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement,
                float dose,
                float min_volume,
                float max_volume
                );

        // Virtual destructor
        virtual ~DvhCoverageObjective();

        //
	// Functions reimplemented from NonSeparableObjective
        // 
        virtual double calculate_objective(
                const vector<float> & dose) const;
        virtual double calculate_objective_and_d(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose) const;
        virtual double calculate_objective_and_d2(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose,
	        vector<float> & d2_obj_by_dose) const;

        //
        // Functions reimplemented from Objective
        //

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;

    private:
        // Default constructor not allowed
        DvhCoverageObjective();

        /// dose in Gy
        float dose_;

        /// Minimum fractional volume to recieve at least the dose.
        /// Must be between 0 and 1
        float min_volume_;

        /// Maximum fractional volume to recieve at least the dose.
        /// Must be between 0 and 1
        float max_volume_;
};

/*
 * Inline functions
 */


/**
 * Calculate the objective from some voxels with the given doses.
 *
 * @param dose The vector of voxel doses.
 */
inline
double DvhCoverageObjective::calculate_objective(
	const vector<float> & dose) const
{
    int nVoxels = dose.size();

    // Count voxels above or equal to dose
    int nOver = 0;
    vector<float>::const_iterator iter;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
        if(*iter >= dose_) {
            nOver++;
        }
    }

    // Calculate volume
    float volume_over = static_cast<float>(nOver) / static_cast<float>(nVoxels);

    // Check if volume violates constraints
    if(volume_over > max_volume_) {
        return 1;
    }

    if(volume_over < min_volume_) {
        return 1;
    }

    return 0;
}


/**
 * Calculate the objective and partial derivative of the voxel contribution to 
 * the objective from one voxel with the given dose.  Always 0 for this 
 * objective since it is not differentiable.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 */
inline
double DvhCoverageObjective::calculate_objective_and_d(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose) const
{
    // Resize return vector
    d_obj_by_dose.resize(dose.size());

    // Fill return vector with zeros
    std::fill(d_obj_by_dose.begin(), d_obj_by_dose.end(), 0.0f);

    return calculate_objective(dose);
}


/**
 * Calculate the objective and first and second partial derivatives of the 
 * voxel contribution to the objective with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 * @param d2_obj_by_dose The vector to hold the returned partial derivatives
 */
inline
double DvhCoverageObjective::calculate_objective_and_d2(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose,
	vector<float> & d2_obj_by_dose) const
{
    // Resize return vectors
    d_obj_by_dose.resize(dose.size());
    d2_obj_by_dose.resize(dose.size());

    // Fill return vectors with zeros
    std::fill(d_obj_by_dose.begin(), d_obj_by_dose.end(), 0.0f);
    std::fill(d2_obj_by_dose.begin(), d2_obj_by_dose.end(), 0.0f);

    return calculate_objective(dose);
}


#endif
