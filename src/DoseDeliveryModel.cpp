/**
 * @file: UncertaintyModel.cpp
 * UncertaintyModel Class implementation.
 *
 */

#include "DoseDeliveryModel.hpp"
#include <sstream>
#include <numeric>
#include <cstdio>

// Write out like in matlab
std::ostream& write_matlab_vector (
        std::ostream& o,
        const vector<unsigned int> &rhs)
{
    o << "[";

    if(rhs.size() > 0) {
        unsigned int start = 0;
        unsigned int end;

        // Loop over vector, consolidating runs
        while(start < rhs.size()) {
            // Find extent of current run
            end = start;
            while(end + 1 < rhs.size() && rhs[end+1] == rhs[end]+1) {
                end++;
            }

            // Write comma if needed
            if(start > 0) {
                o << ", ";
            }

            // Write out range
            if(start == end) {
                o << rhs[start];
            } else {
                o << rhs[start] << ":" << rhs[end];
            }

            // Increment counters
            start = end+1;
        }
    }

    o << "]";
    return o;
}

//
// RigidDoseShift functions
// ^^^^^^^^^^^^^^^^^^^^^^^^
//

/// Swap contents with another RigidDoseShift
void RigidDoseShift::swap(RigidDoseShift & rhs)
{
    std::swap(weight_, rhs.weight_);
    std::swap(subscript_shift_, rhs.subscript_shift_);
    std::swap(auxiliaryNo_, rhs.auxiliaryNo_);
    std::swap(use_bixelNos_, rhs.use_bixelNos_);
    std::swap(bixelNos_, rhs.bixelNos_);
    std::swap(use_bixel_auxiliaryNos_, rhs.use_bixel_auxiliaryNos_);
    std::swap(bixel_auxiliaryNos_, rhs.bixel_auxiliaryNos_);
    std::swap(use_random_rounding_, rhs.use_random_rounding_);
    std::swap(shift_, rhs.shift_);
}


/// Merge contents with another RigidDoseShift
void RigidDoseShift::merge(const RigidDoseShift & rhs)
{
    // Shift is the weighted average of the two shifts
    shift_ *= weight_;
    shift_ += rhs.shift_ * rhs.weight_;

    // Update weight
    weight_+=rhs.weight_;

    // Scale shift back to correct size
    shift_ *= 1/weight_;
}


/**
 * Lexicographic comparison operator
 *
 * Tries to do cheap comparisons first.
 *
 * Compares subscript_shift_, then auxialiaryNo_, then use_random_rounding_,
 * then (if use_random_rounding) shift_, then use_bixelNos_,  then
 * use_bixel_auxiliaryNos_, then bixelNos_.size(), then bixelNos_, then
 * bixel_auxiliaryNos_.size(), then bixel_auxiliaryNos_.  Never compares
 * weight_.  If use_random_rounding_ is false then doesn't compare shift_.
 *
 * Only shifts where (a < b) and (b < a) are both false can be merged.
 *
 * @param rhs The thing to compare against
 */
bool RigidDoseShift::operator<(const RigidDoseShift& rhs) const
{
    if(subscript_shift_ < rhs.subscript_shift_) {
        return true;
    } else if(rhs.subscript_shift_ < subscript_shift_) {
        return false;
    }

    if(auxiliaryNo_ < rhs.auxiliaryNo_) {
        return true;
    } else if(rhs.auxiliaryNo_ < auxiliaryNo_) {
        return false;
    }

    if(use_bixelNos_ != rhs.use_bixelNos_) {
        return use_bixelNos_ < rhs.use_bixelNos_;
    }

    if(use_random_rounding_ != rhs.use_random_rounding_) {
        return (use_random_rounding_ < rhs.use_random_rounding_);
    }

    if(use_random_rounding_) {
        if(shift_ < rhs.shift_) {
            return true;
        } else if(rhs.shift_ < shift_) {
            return false;
        }
    }

    if(use_bixelNos_) {
        // use_bixel_auxiliaryNos_ is relevant, so check it
        if(use_bixel_auxiliaryNos_ != rhs.use_bixel_auxiliaryNos_) {
            return (use_bixel_auxiliaryNos_ < rhs.use_bixel_auxiliaryNos_);
        }

        // unfortunately, bixelNos_ is relevant, so we need to try it

        // First check the size
        if(bixelNos_.size() != rhs.bixelNos_.size()) {
            // Different size vector
            return (bixelNos_.size() < rhs.bixelNos_.size());
        }

        // The bixelNos_ vectors are the same size, so compare elements
        std::pair<vector<unsigned int>::const_iterator,
            vector<unsigned int>::const_iterator> mis_iter = std::mismatch(
                    bixelNos_.begin(), bixelNos_.end(),
                    rhs.bixelNos_.begin());
        if(mis_iter.first != bixelNos_.end()) {
            return *mis_iter.first < *mis_iter.second;
        }

        if(use_bixel_auxiliaryNos_) {
            // unfortunately, bixel_auxiliaryNos_ is relevant, so we need to
            // try it

            // First check the size
            if(bixel_auxiliaryNos_.size() != rhs.bixel_auxiliaryNos_.size()) {
                // Different size vector
                return (bixel_auxiliaryNos_.size() <
                        rhs.bixel_auxiliaryNos_.size());
            }

            // The bixel_auxiliaryNos_ vectors are the same size, so compare
            // elements
            std::pair<vector<unsigned int>::const_iterator,
                vector<unsigned int>::const_iterator> mis_iter = std::mismatch(
                    bixel_auxiliaryNos_.begin(), bixel_auxiliaryNos_.end(),
                    rhs.bixel_auxiliaryNos_.begin());
            if(mis_iter.first != bixel_auxiliaryNos_.end()) {
                return *mis_iter.first < *mis_iter.second;
            }
        }
    }

    // Found no difference so far, so false
    return false;
}

/**
 * set bixel_auxiliaryNos_ given a vector of auxiliary numbers for each bixel
 */
void RigidDoseShift::set_bixel_auxiliaryNos(vector<unsigned int> & bixel_auxiliaryNos)
{
    bixel_auxiliaryNos_.resize(0);

    use_bixel_auxiliaryNos_ = true;

    for(vector<unsigned int>::const_iterator iter = bixelNos_.begin();
	iter !=bixelNos_.end();
	iter++) {
        // copy the auxiliary number
        bixel_auxiliaryNos_.push_back(bixel_auxiliaryNos.at(*iter));
    }
}


//
// Random Scenario functions
// ^^^^^^^^^^^^^^^^^^^^^^^^^
//


/**
 * Round shifts for each scenario
 */
void RandomScenario::round_shifts(
        const Geometry& geo ///< The geometry to use
        )
{
    unsigned int nShifts = rigid_shifts_.size();

    // Generate subscript shifts for each shift by rounding
    for(int iShift = 0; iShift < nShifts; iShift++) {
        rigid_shifts_[iShift].subscript_shift_ =
            geo.convert_to_Shift_Subscripts(
                    rigid_shifts_[iShift].shift_);
    }
}


/**
 * Linearly interpolate shifts
 *
 * Creates up to 8 new shifts to replace current ones
 */
void RandomScenario::linearly_interpolate_shifts(
        const Geometry& geo ///< The geometry to use
        )
{
    unsigned int nShifts = rigid_shifts_.size();

    // Temp vector to hold new shifts
    vector<RigidDoseShift> temp_shifts;
    temp_shifts.reserve(nShifts * 8);

    RigidDoseShift temp_shift;

    // Temp vector to hold results of linear interpolation
    vector<std::pair<Voxel_Shift_Subscripts, float> > weighted_shifts;
    weighted_shifts.reserve(8);
    for(unsigned int iShift = 0; iShift < nShifts; iShift++) {
        temp_shift = rigid_shifts_[iShift];
        weighted_shifts.resize(0);
        geo.linearly_interpolate_to_Shift_Subscripts(
                temp_shift.shift_, temp_shift.weight_, weighted_shifts);

        // Add individual shifts to random scenario
        for(unsigned int i = 0; i < weighted_shifts.size(); i++) {
            temp_shift.subscript_shift_ = weighted_shifts[i].first;
            temp_shift.shift_ = geo.convert_to_Shift_in_Patient_Coord(
                    temp_shift.subscript_shift_);
            temp_shift.weight_ = weighted_shifts[i].second;
            temp_shifts.push_back(temp_shift);
        }
    }

    // Swap the new shifts in for the old
    rigid_shifts_.swap(temp_shifts);
}

/**
 * Sort the shifts and merge identical shifts
 * If two elements are identical except for different shifts and randomized
 * rounding is turned off, the shift is chosen arbitrarily.
 */
void RandomScenario::merge_shifts()
{
    // Sort the shifts
    std::sort(rigid_shifts_.begin(), rigid_shifts_.end());

    //
    // Merge equal shifts
    //

    // The place to write the current merge
    vector<RigidDoseShift>::iterator target = rigid_shifts_.begin();

    // The item to be compared with the current target and possibly merged
    vector<RigidDoseShift>::iterator merge_probe = rigid_shifts_.begin();

    // Loop through shifts list
    while(merge_probe != rigid_shifts_.end()) {
        // Swap merge_probe to target
        if(target != merge_probe) {
            target->swap(*merge_probe);
        }

        // Find matched pair
        merge_probe++;
        while(merge_probe != rigid_shifts_.end()
                && !(*target < *merge_probe)) {
            target->merge(*merge_probe);
            merge_probe++;
        }

        // Finished merging shifts into target so Move target
        target++;
    }

    // Eliminate all vector entries at or after target.
    if(target != rigid_shifts_.end()) {
        rigid_shifts_.erase(target,rigid_shifts_.end());
    }
}


std::ostream& operator<< (
        std::ostream& o,
        const RandomScenario& rhs)
{
    // Check if a comma is needed
    bool need_comma = false;

    // Check if there are rigid_shifts_
    if(rhs.rigid_shifts_.size() > 0) {
        if(need_comma) {
            o << ", ";
        }
        o << "rigid_shifts_=[" << rhs.rigid_shifts_[0];
        for(unsigned int i = 1; i < rhs.rigid_shifts_.size(); i++) {
            o << ", " << rhs.rigid_shifts_[i];
        }
        o << "]";
        need_comma = true;
    }

    // Check if there are auxiliaryNos_
    if(rhs.auxiliaryNos_.size() > 0) {
        // auxiliary number for each beamlet
        o << "auxiliaryNos=";
        write_matlab_vector(o,rhs.auxiliaryNos_);
        need_comma = true;
    }

    // Check if there are range_shifts_
    if(rhs.range_shifts_.size() > 0) {
        if(need_comma) {
            o << ", ";
        }
        o << "range_shifts_=[" << rhs.range_shifts_[0];
        for(unsigned int i = 1; i < rhs.range_shifts_.size(); i++) {
            o << ", " << rhs.range_shifts_[i];
        }
        o << "]";
        need_comma = true;
    }

    // Check if there are bixelNos_
    if(rhs.bixelNos_.size() > 0) {
        if(need_comma) {
            o << ", ";
        }
        o << "bixelNos_=[";
        for(unsigned int i = 0; i < rhs.bixelNos_.size(); i++) {
            if(i > 0) {
                o << ", ";
            }
            write_matlab_vector(o,rhs.bixelNos_[i]);
        }
        o << "]";
        need_comma = true;
    }

    // Respiratory motion model: Pdf realization
    // Check if there are Pdf_
    if(rhs.Pdf_.size() > 0) {
        if(need_comma) {
            o << ", ";
        }
        o << "Pdf_=[" << rhs.Pdf_[0];
        for(unsigned int i = 1; i < rhs.Pdf_.size(); i++) {
            o << ", " << rhs.Pdf_[i];
        }
        o << "]";
        need_comma = true;
    }

    // Respiratory motion model: corresponding auxiliary Dij indices
    // vector<unsigned int> rhs.auxiliary_dij_lut_;
    // Check if there are auxiliary_dij_lut_
    if(rhs.auxiliary_dij_lut_.size() > 0) {
        if(need_comma) {
            o << ", ";
        }
        o << "auxiliary_dij_lut_=";
        write_matlab_vector(o,rhs.auxiliary_dij_lut_);
        need_comma = true;
    }

    // transformation from physical to virtual beamlets
    // mutable TrafoMatrix virtual_bixel_trafo_;
    // Check if the virtual_bixel_trafo_ is initialized
    if(rhs.virtual_bixel_trafo_.is_initialized()) {
        if(need_comma) {
            o << ", ";
        }
        o << "virtual_bixel_trafo_=[initialized]";
    }

    // Check if there are virtual_bixel_patient_shifts_
    if(rhs.virtual_bixel_patient_shifts_.size() > 0) {
        if(need_comma) {
            o << ", ";
        }
        o << "virtual_bixel_patient_shifts_=[" << rhs.virtual_bixel_patient_shifts_[0];
        for(unsigned int i = 1;
                i < rhs.virtual_bixel_patient_shifts_.size();
                i++) {
            o << ", " << rhs.virtual_bixel_patient_shifts_[i];
        }
        o << "]";
        need_comma = true;
    }

    return o;
}


std::ostream& operator<< (
        std::ostream& o,
        const RigidDoseShift& rhs)
{
    o << "(";
    o << "weight_=" << rhs.weight_ << ", ";
    o << "subscript_shift_=" << rhs.subscript_shift_ << ", ";
    o << "auxiliaryNo_=" << rhs.auxiliaryNo_ << ", ";
    o << "use_bixelNos_=" << rhs.use_bixelNos_ << ", ";
    o << "bixelNos_=";
    write_matlab_vector(o,rhs.bixelNos_);
    o << ", ";
    o << "use_bixel_auxiliaryNos_=" << rhs.use_bixel_auxiliaryNos_ << ", ";
    o << "bixel_auxiliaryNos_=";
    write_matlab_vector(o, rhs.bixel_auxiliaryNos_);
    o << ", ";
    o << "use_random_rounding_=" << rhs.use_random_rounding_ << ", ";
    o << "shift_=" << rhs.shift_;
    o << ")";
    return o;
}

/**
 * constructor
 *
 */
DoseDeliveryModel::DoseDeliveryModel(const Geometry *the_geometry)
    : the_geometry_(the_geometry)
    , nDose_eval_scenarios_(0)
    , dose_eval_scenarios_()
    , dose_eval_pdf_()
{
}

// destructor
DoseDeliveryModel::~DoseDeliveryModel()
{
}

/**
 * calculate dose for a scenario
 */
void DoseDeliveryModel::calculate_dose(
        DoseVector &dose_vector,
        const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario) const
{
    for(unsigned int i=0; i<dose_vector.get_nVoxels(); i++)
    {
        dose_vector.set_voxel_dose(i,
                calculate_voxel_dose(i,bixel_grid,dij,random_scenario));
    }
}

/**
 * calculate expected dose
 */
void DoseDeliveryModel::calculate_expected_dose(DoseVector &dose_vector,
					       const BixelVector &bixel_grid,
					       DoseInfluenceMatrix &dij) const
{
    for(unsigned int i=0; i<dose_vector.get_nVoxels(); i++)
    {
	dose_vector.set_voxel_dose(i, calculate_expected_voxel_dose(i,bixel_grid,dij) );
    }
}

/**
 * calculate dose for nominal case
 */
void DoseDeliveryModel::calculate_nominal_dose(
        DoseVector &dose_vector,
        const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij) const
{
    RandomScenario scenario = RandomScenario();
    generate_nominal_scenario(scenario);

    for(unsigned int i=0; i<dose_vector.get_nVoxels(); i++)
    {
	dose_vector.set_voxel_dose(i,
                calculate_voxel_dose(i,bixel_grid,dij,scenario) );
    }
}

/**
 * calculate dose variance
 */
void DoseDeliveryModel::calculate_variance(DoseVector &variance,
					  const BixelVector &bixel_grid,
					  DoseInfluenceMatrix &dij) const
{
    float var;

    // clear vector
    variance.reset_dose();

    // loop over all voxels
    for(unsigned int iVoxel=0; iVoxel<dij.get_nVoxels(); iVoxel++) {

	// calc variance for the voxel
	var = calculate_voxel_variance(iVoxel, bixel_grid, dij);

	// take square root
	if(var>0) {
	    var = sqrt(var);
	}
	else {
	    var = 0;
	}

	// set value
	variance.set_voxel_dose(iVoxel, var);
    }
}

float DoseDeliveryModel::get_normal_distributed_random_number(float width) const
{
    int iX,iY;
    float fX,fY;
    float fRandomNumber;

    iX = rand();
    iY = rand();

    fX = ((double)iX)/RAND_MAX;
    fY = ((double)iY)/RAND_MAX;

    fRandomNumber = width*sqrt(-2*log(fX))*cos(2*M_PI*fY);

    return(fRandomNumber);
}

float DoseDeliveryModel::get_truncated_normal_distributed_random_number(float width, float cutoff) const
{
    int iX,iY;
    float fX,fY;
    float fRandomNumber;
    int maxTry = 100;
    int tryNo = 0;

    while(tryNo<maxTry) {

	iX = rand();
	iY = rand();

	fX = ((double)iX)/RAND_MAX;
	fY = ((double)iY)/RAND_MAX;

	fRandomNumber = width*sqrt(-2*log(fX))*cos(2*M_PI*fY);

	if(fabs(fRandomNumber) <= cutoff*width) {
	    return(fRandomNumber);
	}

	tryNo++;
    }

    cout << "tried " << maxTry << " times to generate a Gaussian distributed random number" << endl;
    cout << " with sigma " << width << " and cutoff " << cutoff << endl;
    throw(std::runtime_error("cannot generate Gaussian random number"));
}

/**
 * gives a 3D Gaussian setup error in a 2sigma elipse
*/
Shift_in_Patient_Coord DoseDeliveryModel::get_truncated_gaussian_setup_error(
    float sigmaX, float sigmaY, float sigmaZ, float cutoff) const
{
    float epsilon = 0.001;
    float tmpx,tmpy,tmpz;
    unsigned int tryNo = 0;
    unsigned int maxTry = 100;

    Shift_in_Patient_Coord temp;

    while(tryNo<maxTry) {

	// generate random error
	temp.x_ = get_truncated_normal_distributed_random_number(sigmaX);
	temp.y_ = get_truncated_normal_distributed_random_number(sigmaY);
	temp.z_ = get_truncated_normal_distributed_random_number(sigmaZ);

	if(the_geometry_->comp_float_abs(sigmaX,0.0,epsilon)) {
	    tmpx = 0;
	}
	else {
	    tmpx = temp.x_/(sigmaX*cutoff);
	}
	if(the_geometry_->comp_float_abs(sigmaY,0.0,epsilon)) {
	    tmpy = 0;
	}
	else {
	    tmpy = temp.y_/(sigmaY*cutoff);
	}
	if(the_geometry_->comp_float_abs(sigmaZ,0.0,epsilon)) {
	    tmpz = 0;
	}
	else {
	    tmpz = temp.z_/(sigmaZ*cutoff);
	}

	// check if this shift is within 2 sigma elipse
	if(tmpx*tmpx + tmpy*tmpy + tmpz*tmpz < 1) {
	    return Shift_in_Patient_Coord(temp);
	}

	// increment counter
	tryNo++;
    }

    // check if max number of trys the get a valid shift is exceeded
    cout << "Number of attempts to get a valid setup shift exceeded " << maxTry << endl;
    throw(std::runtime_error("cannot get valid setup shift"));
}

/**
 * write dose distributions of scenarios
 */
void DoseDeliveryModel::write_scenario_dose(BixelVector &bixel_grid,
					    DoseInfluenceMatrix &dij,
					    string file_prefix,
					    Dvh dvh) const
{
    string dose_file_suffix = "dose.dat";
    string DVH_file_suffix = "DVH.dat";
    string dose_file, dvh_file;
    char cScen[10];

    DoseVector dose(the_geometry_->get_nVoxels());

    // loop over the dose evaluation scenarios
    unsigned int nScenarios = dose_eval_scenarios_.size();
    for(unsigned int iScenario=0; iScenario<nScenarios;  iScenario++) {

	// calculate dose
	calculate_dose(dose,
		       bixel_grid,
		       dij,
		       dose_eval_scenarios_[iScenario]);

        // Calculate DVH
	dvh.calculate(dose);

        // Write dose
        if(nScenarios < 9) {
            sprintf(cScen,"%d",iScenario+1);
        } else if(nScenarios < 99) {
            sprintf(cScen,"%02d",iScenario+1);
        } else if(nScenarios < 999) {
            sprintf(cScen,"%03d",iScenario+1);
        } else {
            sprintf(cScen,"%04d",iScenario+1);
        }
        dose_file = file_prefix + "scenario_" + string(cScen)
            + '_' + dose_file_suffix;
        dvh_file = file_prefix + "scenario_" + string(cScen)
            + '_' + DVH_file_suffix;

        dose.write_dose(dose_file);
        dvh.write_DVH(dvh_file);
    }
}

/**
 * normalize the PDF to one
 */
void DoseDeliveryModel::normalize_dose_eval_pdf()
{
    cout << "normalize PDF" << endl;

    if(nDose_eval_scenarios_ > 0) {
	float sum = accumulate(dose_eval_pdf_.begin(),dose_eval_pdf_.end(),0.0);
	if(sum>0) {
	    vector<float>::iterator iter = dose_eval_pdf_.begin();
	    while(iter != dose_eval_pdf_.end()) {
		*iter = *iter / sum;
		iter++;
	    }
	}
	else {
	    cout << "Error while normalizing the Pdf for dose evaluation scenarios" << endl;
	    cout << "Do all scenarios have zero probability?" << endl;
	    throw(std::runtime_error("cannot continue"));
	}
    }
}

/**
 * get a random dose evaluation scenario
 */
RandomScenario DoseDeliveryModel::get_random_dose_eval_scenario() const
{
    // picks one of the discrete scenarios based on their probability

    unsigned int scenarioNo = 0;
    float CumPdf = 0;
    float RandomNo = (float) std::rand() / (float) RAND_MAX;
    for(unsigned int iInstance=0; iInstance<get_nDose_eval_scenarios(); iInstance++) {
	CumPdf = CumPdf + get_dose_eval_pdf(iInstance);
	if (RandomNo <= CumPdf) {
	    scenarioNo = iInstance;
	    break;
	}
    }

    // set random scenario
    return(get_dose_eval_scenario(scenarioNo));
}





// --------------------- No Uncertainty model ----------------------

/**
 * constructor
 *
 */
StandardDoseModel::StandardDoseModel(const Geometry *the_geometry)
    : DoseDeliveryModel(the_geometry)
{
    cout << "Create a standard dose model, nothing fancy" << endl;
}

// destructor
StandardDoseModel::~StandardDoseModel()
{
}

// generate a random sample scenario
void StandardDoseModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    generate_nominal_scenario(random_scenario);
}

// generate the nominal scenario
void StandardDoseModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // apply no shift
    random_scenario.rigid_shifts_.resize(0);
    random_scenario.bixelNos_.resize(0);

    // no auxiliary vector
    random_scenario.auxiliaryNos_.resize(0);

    // generate no Pdf
    random_scenario.Pdf_.resize(0);
    random_scenario.auxiliary_dij_lut_.resize(0);
}


// calculate voxel dose for a scenario
float StandardDoseModel::calculate_voxel_dose(
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario) const
{
    // just call the generic dose calculation
    return( dij.calculate_voxel_dose(voxelNo,bixel_grid) );
}


// calculate gradient contributione of the voxel for a scenario
void StandardDoseModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,
 	const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{
    // just call the generic dose calculation
    dij.calculate_voxel_dose_gradient(
            voxelNo,
            gradient,
            gradient_multiplier);
}


// --------------------- auxiliary based uncertainty models ----------------------


/**
 * constructor
 *
 */
AuxiliaryBasedSystematicErrorModel::AuxiliaryBasedSystematicErrorModel(
    const Geometry *the_geometry,
    unsigned int nInstances)
    : DoseDeliveryModel(the_geometry)
    , nInstances_(nInstances)
    , pdf_(nInstances,1)
    , auxiliary_dij_lut_(nInstances,0)
{
}

// destructor
AuxiliaryBasedSystematicErrorModel::~AuxiliaryBasedSystematicErrorModel()
{
}


// calculate voxel dose for a scenario
float AuxiliaryBasedSystematicErrorModel::calculate_voxel_dose(
    unsigned int voxelNo,
    const BixelVector &bixel_grid,
    DoseInfluenceMatrix &dij,
    const RandomScenario &random_scenario) const
{
    float dose_buf = 0;

    for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
	dose_buf += bixel_grid[iter.get_bixelNo()]
	    * iter.get_influence(random_scenario.auxiliaryNos_[iter.get_bixelNo()]);
    }

    return dose_buf;
}


// calculate gradient contribution of the voxel for a scenario
void AuxiliaryBasedSystematicErrorModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,
 	const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{
    for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
	gradient[iter.get_bixelNo()] += gradient_multiplier
            * iter.get_influence(random_scenario.auxiliaryNos_[
                    iter.get_bixelNo()]);
    }
}


// --------------------- shift based uncertainty models ----------------------

/**
 * constructor
 */
ShiftBasedErrorModel::ShiftBasedErrorModel(
    const Geometry *the_geometry)
    : DoseDeliveryModel(the_geometry)
{
}

/**
 * destructor
 */
ShiftBasedErrorModel::~ShiftBasedErrorModel()
{
}


/**
 * generate nominal scenario.
 * Only changes the shift_ vector in the RandomScenario
 *
 * @param random_scenario The scenario to set to nominal
 */
void ShiftBasedErrorModel::generate_nominal_scenario(
        RandomScenario &random_scenario) const
{
    // Remove any existing shifts
    random_scenario.rigid_shifts_.resize(1);

    // Add one nominal shift
    random_scenario.rigid_shifts_[0] = RigidDoseShift();
}


/**
 * calculate voxel dose for a scenario
 * (bixelNos must be sorted too, but is not checked here)
 */
float ShiftBasedErrorModel::calculate_voxel_dose(
    unsigned int voxelNo,
    const BixelVector &bixel_grid,
    DoseInfluenceMatrix &dij,
    const RandomScenario &random_scenario) const
{
    float dose_buf = 0;
    Voxel_Subscripts voxelSub;

    // loop over the different shifts
    vector<RigidDoseShift>::const_iterator shift_iter
        = random_scenario.rigid_shifts_.begin();
    while(shift_iter != random_scenario.rigid_shifts_.end()) {

        // get subscripts of voxel
        voxelSub = the_geometry_->convert_to_Voxel_Subscripts(
                VoxelIndex(voxelNo));

        // Do probabalistic rounding if needed
        Voxel_Shift_Subscripts shift_sub = shift_iter->subscript_shift_;
        if(shift_iter->use_random_rounding_) {
            shift_sub = the_geometry_->randomly_convert_to_Shift_Subscripts(
                    shift_iter->shift_);
        }

        // add shift
        Voxel_Subscripts shifted_voxel_sub = voxelSub + shift_sub;

        // check whether it's inside dose cube and not air
        unsigned int shifted_voxelNo;
        if(the_geometry_->is_valid_voxel(shifted_voxel_sub)) {
            // shift voxel
            shifted_voxelNo = the_geometry_->convert_to_VoxelIndex(
                    shifted_voxel_sub).index_;
        } else {
            // by default don't shift voxel
            shifted_voxelNo = voxelNo;
        }

        // loop over the Dij entries
        if(!shift_iter->use_bixelNos_) {
            // Calculate dose like normal
            dose_buf += shift_iter->weight_ *
                dij.calculate_voxel_dose(
                        shifted_voxelNo,
                        bixel_grid,
                        shift_iter->auxiliaryNo_);
        } else {
            // We need to check if each bixel is inclueded in shift
            if(shift_iter->use_bixel_auxiliaryNos_){
                // Each bixel has an associated instance number
                dose_buf += shift_iter->weight_ *
                    dij.calculate_voxel_dose(
                            shifted_voxelNo,
                            bixel_grid,
                            shift_iter->bixelNos_,
                            shift_iter->bixel_auxiliaryNos_);
            } else {
                // Each bixel has the same instance number
                dose_buf += shift_iter->weight_ *
                    dij.calculate_voxel_dose(shifted_voxelNo, bixel_grid,
                            shift_iter->auxiliaryNo_, shift_iter->bixelNos_);
            }
        }

        ++shift_iter;
    }

    return dose_buf;
}


/**
 * calculate gradient contribution of the voxel for a scenario
 */
void ShiftBasedErrorModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,
 	const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{

    // loop over the different shifts
    vector<RigidDoseShift>::const_iterator shift_iter
        = random_scenario.rigid_shifts_.begin();
    while(shift_iter != random_scenario.rigid_shifts_.end()) {

        // get subscripts of voxel
        Voxel_Subscripts voxelSub = the_geometry_->convert_to_Voxel_Subscripts(
                VoxelIndex(voxelNo));

        // Do probabalistic rounding if needed
        Voxel_Shift_Subscripts shift_sub = shift_iter->subscript_shift_;
        if(shift_iter->use_random_rounding_) {
            shift_sub = the_geometry_->randomly_convert_to_Shift_Subscripts(
                    shift_iter->shift_);
        }

        Voxel_Subscripts shifted_voxel_sub = voxelSub + shift_sub;

        // check whether it's inside dose cube and not air
        unsigned int shifted_voxelNo;
        if(the_geometry_->is_valid_voxel(shifted_voxel_sub)) {
            // shift voxel
            shifted_voxelNo = the_geometry_->convert_to_VoxelIndex(
                    shifted_voxel_sub).index_;
        } else {
            // by default don't shift voxel
            shifted_voxelNo = voxelNo;
        }

        // loop over the Dij entries
        if(!shift_iter->use_bixelNos_) {
            dij.calculate_voxel_dose_gradient(
                    shifted_voxelNo,
                    gradient,
                    gradient_multiplier * shift_iter->weight_,
                    shift_iter->auxiliaryNo_);
        } else {
            // We need to check if each bixel is inclueded in shift
            if(shift_iter->use_bixel_auxiliaryNos_){
                // Each bixel has an associated instance number
                dij.calculate_voxel_dose_gradient(
                        shifted_voxelNo,
                        gradient,
                        gradient_multiplier * shift_iter->weight_,
                        shift_iter->bixelNos_,
                        shift_iter->bixel_auxiliaryNos_);
            } else {
                // Each bixel has the same instance number
                dij.calculate_voxel_dose_gradient(
                        shifted_voxelNo,
                        gradient,
                        gradient_multiplier * shift_iter->weight_,
                        shift_iter->auxiliaryNo_,
                        shift_iter->bixelNos_);
            }
        }
        shift_iter++;
    }
}


/**
 * write dose distributions
 */
void ShiftBasedErrorModel::write_dose(BixelVector &bixel_grid,
					    DoseInfluenceMatrix &dij,
					    string file_prefix,
					    Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}



// --------------------- Uncertainty ----------------------

/**
 * constructor
 */
DoseDeliveryModel::options::options(const TxtFileParser* pln_parser)
    : dose_delivery_model_("")
    , range_correlation_model_("within_beam")
    , range_sigma_(0)
    , range_sigma_file_root_("")
    , range_nRanges_(0)
    , range_max_dev_(0)
    , range_readDijs_(false)
    , range_writeDijs_(false)
    , range_auxDij_root_("")
    , range_dWEL_(0),
    gaussian_setup_options_(pln_parser),
    setup_use_randomized_rounding_(false),
    setup_use_linear_interpolation_(false),
    setup_merge_shifts_(false),
    setup_use_improved_sdca_(false),
    setup_surface_normals_(),
    pdf_file_name_(""),
    pdf_nModes_(0),
    // nFractions_(0),
    virtual_bixel_dij_root_(""),
    virtual_bixel_stf_root_("")
{
    // the dose delivery model to be applied
    dose_delivery_model_ = pln_parser->get_attribute(
            "Dose_Delivery_Model");

    // -------------------- range uncertainty --------------------------

    // beamlet correlation model
    range_correlation_model_ = pln_parser->get_attribute(
	"Uncertainty_Range_correlation");

    // the range uncertainty (sigma of a Gaussian)
    range_sigma_ = pln_parser->get_num_attribute<float>(
	"Uncertainty_Range_width");

    // the range uncertainty file name (sigma of a Gaussian for each bixel)
    range_sigma_file_root_ =  pln_parser->get_attribute(
	"Uncertainty_Range_sigma_file_root");

    // number of auxiliaries/range shifts
    range_nRanges_ = pln_parser->get_num_attribute<unsigned int>(
	"Uncertainty_Range_nRanges");

    // maximum range deviation in multiples of the distribution width
    range_max_dev_ = pln_parser->get_num_attribute<float>(
	"Uncertainty_Range_max");

    // whether to read precalculated auxiliaries
    range_readDijs_ = pln_parser->is_set_attribute(
	"Uncertainty_Range_read_dijs");

    // whether to write generated auxiliaries
    range_writeDijs_ = pln_parser->is_set_attribute(
	"Uncertainty_Range_write_dijs");

    // file root for written Dijs
    range_auxDij_root_ = pln_parser->get_attribute(
	"Uncertainty_Range_aux_dij_file_root");

    // distance of energy layers in water-equivalent range
    if(pln_parser->is_set_attribute("Uncertainty_Range_dWEL")) {
	range_dWEL_ = pln_parser->get_num_attribute<float>(
	    "Uncertainty_Range_dWEL");
    }

    // -------------------- setup uncertainty --------------------------


    // switch on randomized rounding
    setup_use_randomized_rounding_ = pln_parser->is_set_attribute(
	"Uncertainty_Setup_randomized_rounding");

    // switch on linear interpolation
    setup_use_linear_interpolation_ = pln_parser->is_set_attribute(
	"Uncertainty_Setup_linear_interpolation");

    // switch on merging of matching shifts
    setup_merge_shifts_ = pln_parser->is_set_attribute(
	"Uncertainty_Setup_merge_shifts");

    // switch on improved static dose cloud approximation
    setup_use_improved_sdca_ = pln_parser->is_set_attribute(
	"Uncertainty_Setup_correction");


    // -------------------- pdf variation model --------------------------

    // file name containing the pdf
    if(pln_parser->is_set_attribute("Uncertainty_Respiratory_file_name")) {
	pdf_file_name_ = pln_parser->get_attribute(
	    "Uncertainty_Respiratory_file_name");
    }
    else {
	pdf_file_name_ = pln_parser->get_attribute(
	    "plan_file_root");
    }

    // number of Pdf variation modes
    pdf_nModes_ = pln_parser->get_num_attribute<unsigned int>(
	"Uncertainty_Respiratory_nModes");


	// -------------------- BED --------------------------

    // number fractions
    nFractions_ = pln_parser->get_num_attribute<unsigned int>("BED_nFractions");

    // standard fraction dose
    standard_fraction_dose_ = pln_parser->get_num_attribute<float>("BED_standard_fraction_dose");

    // LET scale factor in simple RBE model
    LET_scalefactor_ = pln_parser->get_num_attribute<float>("LET_scalefactor");
    physD_scalefactor_ = pln_parser->get_num_attribute<float>("physD_scalefactor");


    // --------------------- miscellaneous ------------------------------

    // number of fractions
    // nFractions_ = pln_parser->get_num_attribute<unsigned int>(
        // "Uncertainty_RandomSetup_nFractions");

    // file roots for virtual bixel Dij
    virtual_bixel_dij_root_ = pln_parser->get_attribute(
	"Uncertainty_virtual_dij");

    // file roots for virtual bixel steering file
    virtual_bixel_stf_root_ = pln_parser->get_attribute(
	"Uncertainty_virtual_stf");
};


/**
 * set surface normal vectors for each beam
 */
void DoseDeliveryModel::options::set_surface_normals(const TxtFileParser* pln_parser,
						     const Geometry* the_geometry)
{
    // make sure Geometry class has been initialized
    if(!the_geometry->is_initialized()) {
	throw(std::logic_error(
		  "calling DoseDeliveryModel::options::set_surface_normals before initializing geometry class"));
    }

    // clear surface normal vector
    setup_surface_normals_.resize(0);

    // set default for surface normal vector (= in beam direction)
    for(unsigned int beamNo=0;beamNo<the_geometry->get_nBeams();beamNo++) {
	// transform unit vector in Gantry coordinate system to patient
        // coordinate system
	setup_surface_normals_.push_back(
	    the_geometry->transform_to_Shift_in_Patient_Coord(
		Point_in_Gantry_Coord(0,0,1) ,beamNo));
    }
    // get plan file information
    string temp_str = pln_parser->get_attribute(
	"Uncertainty_Setup_surface_normal");
    // interpret it
    if(!temp_str.empty()) {
	// There was a surface normal passed in, so read it
	std::istringstream iss(temp_str);
	unsigned int beamNo;

	while(!(iss >> beamNo).fail()) {
	    // Read the beamNo successfully, now read the normal
	    float x,y,z;
	    if(!(iss >> x >> y >> z).fail()) {
		// Read x,y, and z successfully
		if(beamNo>0 && beamNo<=the_geometry->get_nBeams()) {
		    float norm = sqrt(
			Shift_in_Patient_Coord(x,y,z)
			* Shift_in_Patient_Coord(x,y,z));
		    setup_surface_normals_[beamNo-1] =
			Shift_in_Patient_Coord(x,y,z) * (1.0/norm);
		}
	    }
	    else {
		pln_parser->report_error(string("Error reading ")
					  + "Uncertainty_Setup_surface_normal");
	    }
	}
    }
}


