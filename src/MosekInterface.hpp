/**
 * @file MosekInterface.hpp
 * MosekInterface Class Header
 *
 */
#include "Plan.hpp"

//#define HAVE_MOSEK

#ifndef HAVE_MOSEK

// Mosek is not available
// create dummy class that prints error message

/**
 * MosekInterface is used to prepare the plan data for calling Mosek as an external solver. 
 * The macro HAVE_MOSEK was not defined at compilation. This is a dummy class which prints 
 * an error message if the Mosek interface is used.
 */

class MosekInterface 
{
public:

    /// constructor
    MosekInterface(Plan* the_plan);
    
    /// destructor
    ~MosekInterface();
    
    /// just prints out that Mosek is not available
    void optimize();

private:

    MosekInterface();

    /// The plan to be optimized
    Plan* the_plan_;

};

#else

// Mosek is available

#include "mosek.h"

/**
 * MosekInterface is used to prepare the plan data for calling Mosek as an external solver. 
 * The class GenericOptimizationData is created which temporarily holds all information about 
 * objectives, constraints, and variable bounds in a generic form. Subsequently, the data is passed to Mosek.
 *
 * To utilize the Mosek interface, the function add_to_optimization_data_set() has to be implemented 
 * in every objective and constraint used. This function appends constraints in GenericOptimizationData
 * after the ones that already exist. GenericOptimizationData.nConstraints_until_now_ keeps track 
 * of the current number of constraints
 *
 * GenericOptimizationData.nAuxiliaries_until_now_ keeps track of the current number auxiliary variables.
 * If an objective adds auxiliary variables, their indices start at nIntVar_ + nAuxiliaries_until_now_.
 */

class MosekInterface
{
public:

    /// constructor
    MosekInterface(Plan* the_plan);
    
    /// destructor
    ~MosekInterface();

    /// optimize plan by calling mosek
    void optimize();

private:

    MosekInterface();

    /// get coefficients and bounds for all constraints, objectives ...
    void extract_mosek_data();

  /// create Mosek structures
  void pass_data_to_mosek();

  /// get the type of bound in Mosek notation
  MSKboundkeye convert_bound_type(BoundType type);

    /// The plan to be optimized
    Plan* the_plan_;

    // status of Mosek environment
  MSKrescodee status_; 

    // Mosek task
    MSKtask_t task_;

    // Mosek environment
    MSKenv_t env_;

  // number of constraints
  unsigned int nConstraints_;

  // total number of Variables (including auxiliaries)
  unsigned int nVar_;

    // structure to temporarily hold all data of the optimization problem
    GenericOptimizationData data_;
};




#endif
