#ifndef DELIVERYMACHINEMODEL_HPP
#define DELIVERYMACHINEMODEL_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <map>
using std::map;
#include <iterator>
using std::iterator_traits;
#include <cstddef>
#include <memory>
//#include <assert.h>
//#include <stdexcept>
//#include <exception>
//#include <iostream>

#include "TxtFileParser.hpp"

// Predefine class before including others
class DeliveryMachineModel;
class BeamEnergy;
class BeamEnergyValue;
class DoseRate;
class GantryAngle;
class CollimatorAngle;
class Attenuator;
class ConfinedParameter;
class MovingParameter;
class ModelTranslator;


/**
 * Class DeliveryMachineModel_exception is thrown when there is an error building a DeliveryMachineModel object.
 */
class DeliveryMachineModel_exception : public std::exception {
    virtual const char* what() const throw() {
        return "Failed while building machine model.";
    }
};


/**
 * ConfinedParameter class controls the possible values a parameter can take. It records the maximum value, minimum
 * value if the parameter is discrete or continuous a defined step size if it exists etc. This is a useful parent class
 * for many other parameter classes.
 */
class ConfinedParameter {
  public:

    /// Constructor
	ConfinedParameter( const string & parameterClass,
					   const std::auto_ptr<const TxtFileParser> & txtFileParser,
					   unsigned int pnNum=0 ) throw ( DeliveryMachineModel_exception );

	/// Destructor
    virtual ~ConfinedParameter() {};

    /// Get the minimum value
    virtual float getMinValue() const {		return minValue_;		};

    /// Get the maximum value
    virtual float getMaxValue() const {		return maxValue_;		};

    /// Get the step size for the parameter
    virtual float getStepSize() const {
        if ( isContinuouslyVariable_ ) {
            return stepSize_;
        } else {
            return 0.0f;
        }
    };

    /// Find out if the parameter is continuously variable
    virtual bool getIsContinuouslyVariable() const {	return isContinuouslyVariable_;		};

    /// Get the number of valid paranmeter values.
	inline virtual size_t getNumValues() const {
        if ( isContinuouslyVariable_ ) {
            return (size_t) ( 1 + ( maxValue_ - minValue_ ) / stepSize_ );
        } else {
            return numValues_;
        }
    };

    /// Get an element from the list of valid parameter values.
	inline virtual float getValue(const unsigned int & index) const {
		if ( isContinuouslyVariable_ ) {
			float val = minValue_ + stepSize_ * index;
			assert( val < maxValue_ );
			return val;
		} else {
			assert( index < numValues_ );
			return validValues_[index];
		}
	};

    /// Get the list of valid parameter values.
	virtual vector<float> getAllValues() const;

    /// Get a valid parameter value as close as possible to the requested value.
    virtual float roundToValidValue( const float & inValue ) const;

  protected:

    /// The minimum doserate
    float minValue_;

    /// The maximum doserate
    float maxValue_;

    /// Flag indicating if the dose rate can be varied in regular steps (true) or
    /// by irregular steps between discrete values (false).
    bool isContinuouslyVariable_;

    /// The steps between valid values
    float stepSize_;

    /// Number of valid values
    size_t numValues_;

    /// List of valid values
    vector<float> validValues_;

    friend class DoseRate;
    friend class GantryAngle;
    friend class CollimatorAngle;
    friend class CouchAngle;
    friend class Attenuator;

private :

    /// Hide the default constructor from all but implemting classes
    ConfinedParameter() {};
};


/**
 * MovingParameter class controls the possible values a parameter can take for those parameters which move during the
 * delivery. It records the maximum velocity, minimum velocity of the parameter changes. If the speeds are discrete or
 * continuous and holds a defined step size if it exists etc.
 *
 * Similar in nature to ConfinedParameter class
 */
class MovingParameter {
  public:

    /// Constructor
	MovingParameter( const string & parameterClass,
					 const std::auto_ptr<const TxtFileParser> & txtFileParser,
					 unsigned int pnNum=0 ) throw ( DeliveryMachineModel_exception );

    /// Destructor
    virtual ~MovingParameter() { };

    /// Find out if the parameter can change during the delivery
    virtual bool getCanChangeDuringDelivery() const {
        return canChangeDuringDelivery_;
    };

    /// Find out if the parameter rates of change can change sign during the delivery
    virtual bool getCanChangeDirection() const {
        return canChangeDirection_;
    };

    /// Get the minimum value
    virtual float getMinSpeed() const {
        return minSpeed_;
    };

    /// Get the maximum value
    virtual float getMaxSpeed() const {
        return maxSpeed_;
    };

    /// Get the maximum value
    virtual float getMaxAcceleration() const {
        return maxAcceleration_;
    };

    /// Find out if the parameter rates of change is continuously variable
    virtual bool getIsSpeedContinuouslyVariable() const {
        return isSpeedContinuouslyVariable_;
    };

    /// Get the step size for the parameter rates of change
    virtual float getSpeedStepSize() const {
        if ( isSpeedContinuouslyVariable_ ) {
            return speedStepSize_;
        } else {
            return 0.0f;
        }
    };

    /// Get the number of valid paranmeter value speeds or rates of change.
    virtual size_t getNumSpeeds() const {
        if ( isSpeedContinuouslyVariable_ ) {
            return (size_t) ( 1 + ( maxSpeed_ - minSpeed_ ) / speedStepSize_ );
        } else {
            return numSpeeds_;
        }
    };

    /// Get an element from the list of valid parameter values.
	virtual float validSpeed(const unsigned int & index) const;

    /// Get a valid parameter value as close as possible to the requested value.
    virtual float roundToValidSpeed( const float & inValue ) const;

    /// Find out if a parameter value change will require a beam interrupt.
    virtual bool getChangeRequiresInterrupt() const {
        return changeRequiresInterrupt_;
    };

    /// Find out if a parameter value change will require a beam interrupt.
    virtual float getInterruptLatency() const {
        return interruptLatency_;
    };

  protected:

    /// Flag indicating if the parameter can be changed during the delivery
    bool canChangeDuringDelivery_;

    /// Flag indicating if the direction of parameter changes can be altered during the delivery.
    bool canChangeDirection_;

    /// Flag indicating if the parameter can only be changed while the radiation beam is off
    bool changeRequiresInterrupt_;

    /// The latency after the completion of a motion during a beam interruption before the beam can restart.
    float interruptLatency_;

    /// The minimum rate of change
    float minSpeed_;

    /// The maximum rate of change
    float maxSpeed_;

    /// The maximum rate of change
    float maxAcceleration_;

    /// Flag indicating if the parameter rate of change can be varied in regular steps (true) or
    /// by irregular steps between discrete values (false).
    bool isSpeedContinuouslyVariable_;

    /// The steps between valid speeds
    float speedStepSize_;

    /// Number of valid speeds
    size_t numSpeeds_;

    /// List of valid speeds
    vector<float> validSpeeds_;

    friend class DoseRate;
    friend class GantryAngle;
    friend class CollimatorAngle;
    friend class CouchAngle;
    friend class Attenuator;

  private:

    /// Hide the default constructor
    MovingParameter() {};

};


/**
 * Class performs translation of model parameter values into real world parameter values and vise-versa.
 * This allows all treatment machines to be considered the same internally within the planning system even if axes
 * are flipped or the axis origin is displaced on a particular treatment machine with respect to the modelled treatment
 * machine.
 *
 * The modelled machine is considered to
 * Opt4D has an internal assumed linac geometry and coordinate system which is independant of the actual linac used for
 * treatment. Transforms are therefore sometimes required to convert the internal parameter values to the machine
 * parameters sent to the linac.
 *
 *	The internal assumed linac geometry matches a linac where:
 *		a gentry rotation of zero would deliver a vertical beam passing down into the floor of the treatment room
 *		a couch rotation of zero would align the long axis of the couch with the gun target axis for a standard
 *          (non-inline) linac.
 * 		a collimator rotation of zero would align the collimator y axis with the longest axis of the patient couch when
 *          both gantry and couch are at zero degrees as defined here.
 *		a positive gantry angle is assumed to rotate the gantry in a clockwise direction when viewed from the far end
 *          of the couch at angle zero.
 *		a positive collimator angle is assumed to rotate the collimator in a clockwise direction when viewed from the
 *          source in the direction of the isocenter.
 *		a positive motion along the x axis is assumed to be from the left to right when viewed from the far end of the
 *          couch (with all rotations 0).
 *		a positive motion along the y axis is assumed to be away from an abserver standing at the far end of the couch.
 */
class ModelTranslator {
  public:

    /// Constructor
	ModelTranslator( const string & parameterClass,
					 const std::auto_ptr<const TxtFileParser> & txtFileParser,
					 unsigned int pnNum=0 ) throw ( DeliveryMachineModel_exception );

    /// Constructor
    ModelTranslator(const float & valueOffset, const bool & axAligned )
            : valueOffset_( valueOffset ),  axAligned_( axAligned )  {};

    /// Destructor
    virtual ~ModelTranslator() {};

    /// Translate a real world value into an internal machine model value.
    virtual float realToModel(const float & realValue) const {
        return axAligned_ ? realValue + valueOffset_ : - ( realValue + valueOffset_ );
    }

    /// Translate a real world value into an internal machine model value.
    virtual float modelToReal(const float & modelValue) const {
        return ( axAligned_ ? modelValue : - modelValue ) - valueOffset_;
    }

    /// Set the valueOffset property
    virtual void setValueOffset( const float & valueOffset ) {
        valueOffset_ = valueOffset;
    }

    /// Get the value of the valueOffset property
    virtual float getValueOffset() const {
        return valueOffset_;
    }

    /// Set the increasingDirectionAligned property value
    virtual void setIncreasingDirectionAligned( const bool & increasingDirectionAligned ) {
        axAligned_ = increasingDirectionAligned;
    }

    /// Get the value of the increasingDirectionAligned property
    virtual float getIncreasingDirectionAligned() const {
        return axAligned_;
    }

  protected:

    /// The offset between the model angle and the real angle entered such that in the case where the axes are aligned
    /// the real angle is given by : model angle + offset.
    float valueOffset_;

    /// Flag indicating if the real axis and model axis are aligned rather than counter-aligned that is if they both
    /// increase with a parameter change in the same direction.
    bool axAligned_;

    friend class DoseRate;
    friend class GantryAngle;
    friend class CollimatorAngle;
    friend class CouchAngle;
    friend class Attenuator;

  private:

    /// Hide the default constructor
    ModelTranslator();
};


/**
 * A beam energy value class holds an entry linking the machine to a particular set of dij files.
 *
 * A given machine model may be capable of producing differnet radiation qualities ( requiring different sets of dij
 * files ) but these different radiation qualities will have the same set of constraints on the optimized degrees of
 * freedom and so one mpf file may link to many different dij files. Alternatively the beam energy may itself be a
 * degree of freedom to be optimized. In these case there will be multiple posible energies associated with one machine.
 */
 class BeamEnergyValue {
  public:
    /// Constructor
	BeamEnergyValue( const string & radiationLabel,  const string & dijLabel )
            : radiationLabel_(radiationLabel),  dijLabel_(dijLabel) {};

    /// Destructor
    virtual ~BeamEnergyValue() {};

    /// Get the radiation label
    string getRadiationlabel() const { return radiationLabel_; };

    /// Get the dij label
    string getDijLabel() const { return dijLabel_; };

    /// Compare string to radiation label
    bool compare_label(const string & lbl) const;

    /// Compare string to Dij label
    bool compare_dijlabel(const string & lbl) const;

  private:

    /// Label for this radiation energy or type.
    string radiationLabel_;

    /// Label identifying which dij files are appropriate
    string dijLabel_;
};


/**
 * Class for storing the beam energy information.
 *
 * This class is intended to tie together the machine model with the dij files and prevent optimization of a plan with
 * dij files from one machine and a machine model of a different machine.
 *
 * When the beam energy is considered a degree of freedom the parameter is always considered as discrete this is because
 * each value of the parameter needs an associated dij matrix file.
 */
class BeamEnergy {

  public:

    /// Constructor
    BeamEnergy( const std::auto_ptr<const TxtFileParser> & txtFileParser ) throw (DeliveryMachineModel_exception);

    /// Destructor
    virtual ~BeamEnergy();

    /// Set the default value for the beam energy
    void setDefaultValue( size_t index ) { defaultIndex_ = index; };

    /// Set the default value for the beam energy
    void setDefaultValue( const string & defaultValue );

    /// Get the default value for the beam energy
	inline BeamEnergyValue getDefaultValue() const {
        return getValue( defaultIndex_ );
    };

    /// Get the default value for the beam energy as an index
	float getDefaultIndex() const { return (float) defaultIndex_; };

	/// Get the beam energy index at a given index
	BeamEnergyValue getValue( unsigned int index ) const {
		assert( index < numValues_ );
		return beamEnergyValue_[index];
	}
	/// Get the beam energy with a givel radiation label
	BeamEnergyValue getValue( const string & lbl ) const;

	/// Find out if the parameter can change during the delivery
    virtual bool getCanChangeDuringDelivery() const {	return canChangeDuringDelivery_;	};

    /// Find out if a parameter value change will require a beam interrupt.
    virtual bool getChangeRequiresInterrupt() const {	return changeRequiresInterrupt_;	};

    /// Find out if a parameter value change will require a beam interrupt.
    virtual float getInterruptLatency() const {			return interruptLatency_;			};

    /// Get the number of valid paranmeter values.
    virtual size_t getNumValues() const {				return numValues_;					};

    /// Compare string to radiation label
    inline bool compare_label(const string & lbl) const {
        std::vector< BeamEnergyValue >::const_iterator iter;
        for( iter = beamEnergyValue_.begin(); iter < beamEnergyValue_.end(); ++iter ) {
            if ( iter->compare_label(lbl) )  return true;
        }
        return false;
    };

    /// Compare string to Dij label
    inline bool compare_dijlabel(const string & lbl) const {
        std::vector<  BeamEnergyValue >::const_iterator iter;
        for( iter = beamEnergyValue_.begin(); iter < beamEnergyValue_.end(); ++iter ) {
            if ( iter->compare_dijlabel(lbl) )  return true;
        }
        return false;
    };

    /// Functions included for similarity with objects derived from confined parameter class
    /// As value is best described as string return index of the value in each case.

    /// Get a list of all values.
    inline vector<float> getAllValues() {
        vector<float> valueList( numValues_, 0.0f );
		for ( unsigned int vv=0; vv<numValues_; ++vv ) valueList[vv] = (float) vv;
        return valueList;
    };

   /// Get the minimum for the beam energy index
    inline float getMinValue() {	return 0.0f;	};

    /// Get the maximum for the beam energy index
    inline float getMaxValue() {	return (float) numValues_;	};

    /// Get the maximum for the beam energy index
    inline float getStepSize() {	return 1.0f;	};

    /// Get the maximum for the beam energy index
    inline float getIsContinuouslyVariable() {	return false;	};

    friend class DeliveryMachineModel;

  private:

    /// Hide the default constructor
    BeamEnergy();

    /// Index for the default value for the beam energy property.
    size_t defaultIndex_;

    /// Array of beam energy possibilities.
    vector< BeamEnergyValue > beamEnergyValue_;

    /// Number of valid values
    size_t numValues_;

    /// Flag indicating if the parameter can be changed during the delivery
    bool canChangeDuringDelivery_;

    /// Flag indicating if the direction of parameter changes can be altered during the delivery.
    bool canChangeDirection_;

    /// Flag indicating if the parameter can only be changed while the radiation beam is off
    bool changeRequiresInterrupt_;

    /// The latency after the completion of a motion during a beam interruption before the beam can restart.
    float interruptLatency_;
};


/**
 * DoseRate class holds values indicating how the delivery machine is capable of changing the dose-rate.
 * This is important if doserate is used as a degree of freedom in the optimizer.
 */
class DoseRate : public ConfinedParameter, public MovingParameter {
  public:

    /// Constructor
    DoseRate( const std::auto_ptr<const TxtFileParser> & txtFileParser ) throw ( DeliveryMachineModel_exception );

    /// Destructor
    virtual ~DoseRate() {};

    /// Get the default dose rate
    float getDefaultValue() const {
        return defaultValue_;
    };

    friend class DeliveryMachineModel;

  private:

    /// Hide the dose rate default constructor
    DoseRate() {};

    /// The default dose rate to be used in MU / minute
    float defaultValue_;
};


/**
 * GantryAngle class holds the constraints and limitations relevant to setting the beam angle.
 */
class GantryAngle : public ConfinedParameter, public MovingParameter, public ModelTranslator {
  public:
    /// Constructor
    GantryAngle( const std::auto_ptr<const TxtFileParser> & txtFileParser ) throw ( DeliveryMachineModel_exception );

    /// Destructor
    virtual ~GantryAngle() {};

    friend class DeliveryMachineModel;
  private:
    /// Hide default constructor
    GantryAngle() { };
};


/**
 * CollimatorAngle class holds the constraints and limitations relevant to setting the collimator angle.
 */
class CollimatorAngle : public ConfinedParameter, public MovingParameter, public ModelTranslator {
  public:
    /// Constructor
    CollimatorAngle( const std::auto_ptr<const TxtFileParser> & txtFileParser ) throw ( DeliveryMachineModel_exception );

    /// Destructor
    virtual ~CollimatorAngle() {};

    friend class DeliveryMachineModel;
  private :
    /// Hide default constructor
    CollimatorAngle() {};
};


/**
 * CouchAngle class holds the constraints and limitations relevant to setting the patient support couch.
 */
class CouchAngle : public ConfinedParameter, public MovingParameter, public ModelTranslator {
  public:
    /// Constructor
	CouchAngle( const std::auto_ptr<const TxtFileParser> & txtFileParser )
			throw ( DeliveryMachineModel_exception );

    /// Destructor
    virtual ~CouchAngle() {};

    friend class DeliveryMachineModel;
  private:
    /// Hide default constructor
    CouchAngle() { };
};


/**
 * Attenuators class holds the constraints and limitations relevant to setting the leaf or jaw position.
 */
class Attenuator : public ConfinedParameter, public MovingParameter, public ModelTranslator {
  public:

    /// Constructor
	Attenuator( int atnNum, const std::auto_ptr<const TxtFileParser> & txtFileParser )
            throw ( DeliveryMachineModel_exception );

    /// Destructor
    virtual ~Attenuator() {};

    /// Get the name of the attenuator
    virtual string getName() const {
        return name_;
    };

    /// Is the attenuator set to move in the x-axis direction ?
    virtual bool getIsX() const {
        return isX_;
    };

    /// Is the attenuator set to move in the y-axis direction ?
    virtual bool getIsY() const {
        return isY_;
    };

    /// Get the angle of the attenuator motion axis to the x-axis.
    virtual float getAngle() const {
        return angle_;
    };

    /// Is the attenuator set to withdraw from the field in the positive axis direction ?
    virtual bool getIsWithdrawDirectionPositive() const {
        return isWithdrawDirectionPositive_;
    };

    /// Is the attenuator the full width of the field ?
    virtual bool getIsFullWidth() const {
        return isFullWidth_;
    };

    /// Is the attenuator the full length ?
    virtual bool getIsFullLength() const {
        return isFullLength_;
    };

    /// The attenuator width in mm - leaf width
    virtual float getWidth() const {
        return width_;
    };

    /// The position of the attenuator centre on the non-motion axis in mm
    virtual float getPosition() const {
        return position_;
    };

    /// The length of the attenuator in mm
    /// ( - distance of maximum extent from furthest extended to minimally extended leaf).
    virtual float getLength() const {
        return length_;
    };

    // -------- Extra Attenuator Constraint Properties -------- //

    /// Get the minimum gap between this attenuator and opposing attenuator
    virtual float getMinGapOpposing() const {
        return minGapOpposing_;
    };

    /// Get the minimum gap between this attenuator and opposing attenuator in track above current.
    virtual float getOppositeAboveMinGap() const {
        return oppositeAboveMinGap_;
    };

    /// Get the minimum gap between this attenuator and opposing attenuator in track below current.
    virtual float getOppositeBelowMinGap() const {
        return oppositeBelowMinGap_;
    };

    /// Can the attenuator in the track above the current one interdigitate with the current one.
    virtual bool getOppositeAboveCanInterDigitate() const {
        return oppositeAboveCanInterDigitate_;
    };

    /// Can the attenuator in the track below the current one interdigitate with the current one.
    virtual bool getOppositeBelowCanInterDigitate() const {
        return oppositeBelowCanInterDigitate_;
    };

    // -------- Radiological Properties -------- //

    /// Get the radiation transmission through the attenuator.
    virtual float getTransmission() const {
        return transmission_;
    };

    /// Get the width of the tongue and groove region of the attenuator.
    virtual float getTongueGrooveWidth() const {
        return tongueGrooveWidth_;
    };

    /// Get the radiation transmission through the tongue and groove portion of the attenuator.
    virtual float getTongueGrooveTransmission() const {
        return tongueGrooveTransmission_;
    };

    /// Get the kernel describing penumbra function for the attenuator in the motion direction.
    virtual vector<float> getPenumbraKernel() const {
        return penumbraKernel_;
    };

    /// Test if two attenuators form an opposing pair.
    /// Perfect symetry in constraint properties is assumed.
    virtual bool isOpposing(const Attenuator & atn) const {
        // Test position first as this is most likely to not match
        if ( position_ != atn.position_ ) return false;
        // Test other parameters
        if ( isX_ != atn.isX_ ) return false;
        if ( isY_ != atn.isY_ ) return false;
        if ( angle_ != atn.angle_ ) return false;
        if ( isFullWidth_ != atn.isFullWidth_ ) return false;
        if ( isFullLength_ != atn.isFullLength_ ) return false;
        if ( width_ != atn.width_ ) return false;
        if ( length_ != atn.length_ ) return false;
        if ( minGapOpposing_ != atn.minGapOpposing_ ) return false;
        if ( oppositeAboveMinGap_ != atn.oppositeAboveMinGap_ ) return false;
        if ( oppositeBelowMinGap_ != atn.oppositeBelowMinGap_ ) return false;
        if ( oppositeAboveCanInterDigitate_ != atn.oppositeAboveCanInterDigitate_ ) return false;
        if ( oppositeBelowCanInterDigitate_ != atn.oppositeBelowCanInterDigitate_ ) return false;
        if ( transmission_ != atn.transmission_ ) return false;
        if ( tongueGrooveWidth_ != atn.tongueGrooveWidth_ ) return false;
        if ( tongueGrooveTransmission_ != atn.tongueGrooveTransmission_ ) return false;

        // Finally attenuators should be moving in opposite directions
        if ( isWithdrawDirectionPositive_ == atn.isWithdrawDirectionPositive_ ) return false;

        return true;
    }

  private:

    /// Hide the default constructor
    Attenuator() {};

    // -------- Attenuator Definition Properties -------- //

    /// Name of the attenuator
    string name_;

    /// Flag indicating if the attenuator is moving in the x-direction
    /// Motion axis is the x-axis
    bool isX_;

    /// Flag indicating if the attenuator is moving in the y-direction
    /// Motion axis is the y-axis
    bool isY_;

	/// Flag indicating if the attenuator is a jaw
	bool isJaw_;

	/// Flag indicating if the attenuator is an MLC leaf
	/// Motion axis is the y-axis
	bool isLeaf_;

    /// Angle (in degrees) of the direction of attenuator motion with respect to the x axis.
    /// Motion axis is inclined to the x and y axes.
    float angle_;

    /// Flag indicating if the attenuator withdraws from the field in the positive xaxis direction.
    bool isWithdrawDirectionPositive_;

    /// Flag indicating if the attenuator spans the whole field width (i.e. is a jaw or a leaf)
    bool isFullWidth_;

    /// Flag indicating if the attenuator is long enough to span the whole length of field in motion direction.
    /// ( - is it long enough to span from maxium distance into the field until the largest field edge).
    bool isFullLength_;

    /// The attenuator width in mm - leaf width
    float width_;

    /// The position of the attenuator centre on the non-motion axis in mm
    float position_;

    /// The length of the attenuator in mm
    /// ( - distance of maximum extent from furthest extended to minimally extended leaf).
    float length_;

    // -------- Extra Attenuator Constraint Properties -------- //

    /// Minimum gap between this attenuator and opposing attenuator
    float minGapOpposing_;

    /// Minimum gap between this attenuator and opposing attenuator in track above current.
    float oppositeAboveMinGap_;

    /// Minimum gap between this attenuator and opposing attenuator in track below current.
    float oppositeBelowMinGap_;

    /// Flag indicating if the attenuator spans the whole field width (i.e. is a jaw or a leaf)
    bool oppositeAboveCanInterDigitate_;

    /// Flag indicating if the attenuator spans the whole field width (i.e. is a jaw or a leaf)
    bool oppositeBelowCanInterDigitate_;

    // -------- Radiological Properties -------- //

    /// The radiation transmission through the attenuator relative to the open field radiation
    float transmission_;

    /// The width of the tongue and groove region of the leaf in mm
    float tongueGrooveWidth_;

    /// The radiation transmission through the tongue and groove portion of the attenuator
    /// relative to the open field radiation
    float tongueGrooveTransmission_;

    /// Kernel describing penumbra function for the attenuator in the motion direction
    vector<float> penumbraKernel_;
};


/**
 * Class DeliveryParameterVector handles individual parameters that together precisely define the delivery. Each
 * controllable item in the treatment delivery should have a parameter in each control point.
 */
class DeliveryMachineModel {

  public:

    /// Constructor
    DeliveryMachineModel( const string& mpf_file_name );

    /// Destructor
    virtual ~DeliveryMachineModel();

    /// Copy Constructor
    DeliveryMachineModel( DeliveryMachineModel & dmm );

    /// Name of the machine model
    inline string getMachinename() const {				return machinename_;	};

    /// Beam Energy Degree Of Freedom
    inline BeamEnergy * getBeamEnergy() const {			return beamEnergy_;		};

    /// Dose Rate Degree Of Freedom
    inline DoseRate * getDoseRate() const {				return doseRate_;		};

    /// Gantry Angle Degree Of Freedom
    inline GantryAngle * getGantry() const {			return gantry_;			};

    /// Collimator Angle Degree Of Freedom
    inline CollimatorAngle * getCollimator() const {	return collimator_;		};

    /// Couch Angle Degree Of Freedom
    inline CouchAngle * getCouch() const {				return couch_;			};

    /// Get Jaw Degrees Of Freedom Info
    inline vector< Attenuator* > getJaws() const {		return jaws_;			};
    inline Attenuator* getJaws(const unsigned int num) const throw (DeliveryMachineModel_exception) {
        if ( num >= (nXJaws_+nYJaws_) ) {
            std::cerr << "DirectParameterVector::getJaws(): Invalid Jaw index." << std::endl;
            throw DeliveryMachineModel_exception();
        }
        return jaws_[num];
    };

    /// Get Jaw Degrees Of Freedom Info
    inline Attenuator* getXJaw(const unsigned int num) const throw (DeliveryMachineModel_exception) {
        if ( num >= nXJaws_ ) {
            std::cerr << "DirectParameterVector::getXJaw(): Invalid Jaw index." << std::endl;
            throw DeliveryMachineModel_exception();
        }
        return jaws_[num];
    };

    /// Get Jaw Degrees Of Freedom Info
    inline Attenuator* getYJaw(const unsigned int num) const throw (DeliveryMachineModel_exception) {
        if ( num >= nYJaws_ ) {
            std::cerr << "DirectParameterVector::getYJaw(): Invalid Jaw index." << std::endl;
            throw DeliveryMachineModel_exception();
        }
        return jaws_[ num + nXJaws_ ];
    };

    /// MLC Degrees Of Freedom
    inline vector< Attenuator* > getMlc() const {	return mlc_;	};
    inline Attenuator* getMlc( const unsigned int num ) const {
        if ( num >= ( nXLeaves_ + nYLeaves_ ) ) {
            std::cerr << "DirectParameterVector::getMlc(): Invalid Leaf index or no MLC in X direction." << std::endl;
            throw DeliveryMachineModel_exception();
        }
        return mlc_[num];
    };

    /// MLC Degrees Of Freedom
    inline Attenuator* getXLeaf( const unsigned int num ) const {
        if ( num >= nXLeaves_ ) {
            std::cerr << "DirectParameterVector::getXMlc(): Invalid Leaf index or no MLC in X direction." << std::endl;
            throw DeliveryMachineModel_exception();
        }
        return mlc_[num];
    };

    /// MLC Degrees Of Freedom
    inline Attenuator* getYLeaf( const unsigned int num ) const {
        if ( num >= nYLeaves_ ) {
            std::cerr << "DirectParameterVector::getYMlc(): Invalid Leaf index or no MLC in Y direction." << std::endl;
            throw DeliveryMachineModel_exception();
        }
        return mlc_[num];
    };

    /// Machine has x jaws present
    inline bool getIsXJaws() const {
        return isXJaws_;
    };

    /// Machine has y jaws present
    inline bool getIsYJaws() const {
        return isYJaws_;
    };

    /// Number of X Jaws present
    inline unsigned int getNXJaws() const {
        return nXJaws_;
    };

    /// Number of y Jaws present
    inline unsigned int getNYJaws() const {
        return nYJaws_;
    };

    /// Machine has x MLC present
    inline bool getIsXMlc() const {
        return isXMlc_;
    };

    /// Machine has y MLC present
    inline bool getIsYMlc() const {
        return isYMlc_;
    };

    /// Number of MLC banks
    inline unsigned int getNMlcBanks() const {
        return nMlcBanks_;
    };

    /// Number of X MLC leaves present
    inline unsigned int getNXLeaves() const {
        return nXLeaves_;
    };

    /// Number of y MLC leaves present
    inline unsigned int getNYLeaves() const {
        return nYLeaves_;
    };

    /// Check if the x jaw can move during delivery. Assumume all x jaws have the same constraints.
    inline bool xJawCanMove( const int jawInd=0 ) const {
        return jaws_[jawInd]->getCanChangeDuringDelivery();
    };

    /// Check if the y jaw can move during delivery. Assumume all y jaws have the same constraints.
    inline bool yJawCanMove( const int jawInd=0 ) const {
        return jaws_[nXJaws_+jawInd]->getCanChangeDuringDelivery();
    };

    /// Check if the x mlcs can move during delivery. Assumume all x mlc have the same constraints.
    inline bool xmlcCanMove( const int leafInd = 0) const {
        return mlc_[leafInd]->getCanChangeDuringDelivery();
    };

    /// Check if the y mlcs can move during delivery. Assumume all y mlc have the same constraints.
    inline bool ymlcCanMove( const int leafInd = 0) const {
        return mlc_[ nXLeaves_ + leafInd ]->getCanChangeDuringDelivery();
    };

    /// Check if the leaves can interdigitate. Return false if any leaf cannot interdigitate.
    inline bool xLeafCanInterdigitate( const int leafInd = 0) const {
        if ( ! mlc_[leafInd]->getOppositeAboveCanInterDigitate() ) {
            return false;
        }
        if ( ! mlc_[leafInd]->getOppositeBelowCanInterDigitate() ) {
            return false;
        }
        return true;
    };

    /// Check if the leaves can interdigitate. Return false if any leaf cannot interdigitate.
    inline bool yLeafCanInterdigitate( const int leafInd = 0) const {
        if ( ! mlc_[leafInd+nXLeaves_]->getOppositeAboveCanInterDigitate() ) {
            return false;
        }
        if ( ! mlc_[leafInd+nXLeaves_]->getOppositeBelowCanInterDigitate() ) {
            return false;
        }
        return true;
    };

    /// Check if the leaves are length limited or not
    inline bool xLeafIsFullLength( const int leafInd = 0) const {
        return mlc_[leafInd]->getIsFullLength();
    };

    /// Check if the leaves are length limited or not
    inline bool yLeafIsFullLength( const int leafInd = 0) const {
        return mlc_[leafInd+nXLeaves_]->getIsFullLength();
    };


    /// Get the minimum gap between opposing leaves
    inline float getXLeafMinGapOpposing( const int leafInd=0 ) const {
        return mlc_[leafInd]->getMinGapOpposing();
    };

    /// Get the minimum gap between opposing leaves
    inline float getYLeafMinGapOpposing( const int leafInd=0 ) const {
        return mlc_[leafInd+nXLeaves_]->getMinGapOpposing();
    };

    /// Get the minimum gap between opposing leaves in adjacent tracks
    inline float getXLeafMinGapAdjacent( const int leafInd=0 ) const {
        return mlc_[leafInd]->getOppositeBelowMinGap();
    };

    /// Get the minimum gap between opposing leaves in adjacent tracks
    inline float getYLeafMinGapAdjacent( const int leafInd=0 ) const {
        return mlc_[leafInd+nXLeaves_]->getOppositeBelowMinGap();
    };

    /// Get the maximum to minimum extension range of a leaf in the bank
    inline float getXLeafLength(int lf=0) const {
        return mlc_[lf]->getLength();
    };

    /// Get the maximum to minimum extension range of a leaf in the bank
    inline float getYLeafLength(int lf=0) const {
        return mlc_[nXLeaves_+lf]->getLength();
    };

    inline bool get_cpMaxNumIsLimited() const { return cpMaxNumIsLimited_; };
    inline size_t get_cpMaxNum() const { return cpMaxNum_; };
    inline bool get_cpMinSpacingIslimited() const { return cpMinSpacingIsLimited_; };
    inline float get_cpMinSpacing() const { return cpMinSpacing_; };
    inline bool get_cpTimeResIsLimited() const { return cpTimeResIsLimited_; };
    inline float get_cpTimeRes() const { return cpTimeRes_; };
    inline bool get_cpMaxTimeIsLimited() const { return cpMaxTimeIsLimited_; };
    inline float get_cpMaxTime() const { return cpMaxTime_; };

    inline void set_cpMaxNumIsLimited( const bool isLim ) { cpMaxNumIsLimited_ = isLim; };
    inline void set_cpMinSpacingIslimited( const bool isLim ) { cpMinSpacingIsLimited_ = isLim; };
    inline void set_cpTimeResIsLimited( const bool isLim ) { cpTimeResIsLimited_ = isLim; };
    inline void set_cpMaxTimeIsLimited( const bool isLim ) { cpMaxTimeIsLimited_ = isLim; };
    inline void set_cpMaxNum( const size_t val ) {
        cpMaxNumIsLimited_ = true;
        cpMaxNum_ = val;
    };
    inline void set_cpMinSpacing( const float val ) {
        cpMinSpacingIsLimited_ = true;
        cpMinSpacing_ = val;
    };
    inline void set_cpTimeRes( const float val ) {
        cpTimeResIsLimited_= true;
        cpTimeRes_ = val;
    };
    inline void set_cpMaxTime( const float val ) {
        cpMaxTimeIsLimited_= true;
        cpMaxTime_ = val;
    };

    /// Minimum position on the x axis
    inline float getMinXAx() const {
        return minXAx_;
    };

    /// Maximum position on the x axis
    inline float getMaxXAx() const {
        return maxXAx_;
    };

    /// Minimum position on the y axis
    inline float getMinYAx() const {
        return minYAx_;
    };

    /// Maximum position on the y axis
    inline float getMaxYAx() const {
        return maxYAx_;
    };

 private:

    /// Hide the default constructor
    DeliveryMachineModel() {};

    /// Populate the lists of attributes and default values
    void populate_Attributes( vector<string> & required_attributes,  vector<string> & optional_attributes,
            map<string,string> & default_values,  vector<string> & multi_attributes,
            map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
            map<string, map<string,string> > & default_sub_values ) const;

    /// Populate the lists of sub attributes which relate to absolute constraints on the parameter value
    void populateConstraintAttributes( const string & multiAttributeName,
            map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
            map<string, map<string,string> > & default_sub_values ) const;

    /// Populate the lists of sub attributes which relate to parameter motion or relative changes.
    void populateMotionAttributes( const string & multiAttributeName,
            map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
            map<string, map<string,string> > & default_sub_values ) const;

    /// Populate the lists of sub attributes relating to transformations between actual and model coordinate systems
    void populateModelTransformAttributes( const string & multiAttributeName,
            map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
            map<string, map<string,string> > & default_sub_values ) const;


    /// Sorting operator for sorting by attenuator jaw or leaf
	inline static bool sortByIsJaw( const Attenuator* a, const Attenuator* b ) {
		return a->getIsFullWidth() > b->getIsFullWidth();
    };

    /// Sorting operator for sorting by attenuator angle
	inline static bool sortByAngle( const Attenuator* a, const Attenuator* b ) {
	    return a->getAngle() < b->getAngle();
    };

    /// Sorting operator for sorting by attenuator withdrawal direction
	inline static bool sortByDirection( const Attenuator* a, const Attenuator* b ) {
		return a->getIsWithdrawDirectionPositive() < b->getIsWithdrawDirectionPositive();
    };

    /// Sorting operator for sorting by attenuator position
	inline static bool sortByPosition( const Attenuator* a, const Attenuator* b ) {
		return a->getPosition() < b->getPosition();
    };

    /// Extract the delivery parameter data from the attributes read in from the mpf file.
    void extractDataFromParser();

    /// Read in the control point restrictions from the text file parser.
    void extractControlPointData();

    /// Check the Jaw data to ensure consistancy.
    bool checkJaws();

    /// Check the MLC leaf data to ensure consistancy.
    bool checkMLCs();

    /// Sort the attenuator list
    void sortAttenuators( vector< Attenuator * > & atns );

    /// Get list of isJaw status for list of attenuators
    vector< bool > getListIsJaw( const vector< Attenuator* >::const_iterator & fromAtn,
            const vector< Attenuator* >::const_iterator & toAtn ) const;

    /// Get list of angle status for list of attenuators
    vector< float > getListAngle( const vector< Attenuator *>::const_iterator & fromAtn,
            const vector< Attenuator *>::const_iterator & toAtn ) const;

    /// Get list of getIsWithdrawDirectionPositive status for list of attenuators
    vector< bool > getListDirection( const vector< Attenuator *>::const_iterator & fromAtn,
            const vector< Attenuator *>::const_iterator & toAtn ) const;

    /// Get list of Position values for list of attenuators
    vector< float > getListPosition( const vector< Attenuator *>::const_iterator & fromAtn,
            const vector< Attenuator *>::const_iterator & toAtn ) const;

    /// The mpf file parser
    std::auto_ptr<const TxtFileParser> txtFileParser_;

    /// Name of the machine model
    string machinename_;

    /// Beam Energy Degree Of Freedom
    BeamEnergy * beamEnergy_;

    /// Dose Rate Degree Of Freedom
    DoseRate * doseRate_;

    /// Gantry Angle Degree Of Freedom
    GantryAngle * gantry_;

    /// Collimator Angle Degree Of Freedom
    CollimatorAngle * collimator_;

    /// Couch Angle Degree Of Freedom
    CouchAngle * couch_;

    /// Machine has x jaws present
    bool isXJaws_;

    /// Machine has y jaws present
    bool isYJaws_;

    /// Number of X Jaws present
    unsigned int nXJaws_;

    /// Number of y Jaws present
    unsigned int nYJaws_;

    /// Jaws Degrees Of Freedom
    vector< Attenuator* > jaws_;

    /// Machine has x MLC present
    bool isXMlc_;

    /// Machine has y MLC present
    bool isYMlc_;

    /// Number of MLC opposing bank pairs
    unsigned int nMlcBanks_;

    /// Number of X MLC leaves present
    unsigned int nXLeaves_;

    /// Number of y MLC leaves present
    unsigned int nYLeaves_;

    /// MLC Degrees Of Freedom
    vector< Attenuator* > mlc_;

    /// Flag indicating that the maximum number of control points is limited by the system.
    bool cpMaxNumIsLimited_;

    /// The maximum number of control points.
    size_t cpMaxNum_;

    /// Flag indicating that the minimum control point spacing is limited.
    bool cpMinSpacingIsLimited_;

    /// The minimum control point spacing.
    float cpMinSpacing_;

    /// Flag indicating that the minimum control point spacing is limited.
    bool cpTimeResIsLimited_;

    /// The step by which control points can change
    float cpTimeRes_;

    /// Flag indicating that the maximum control point time in a given field is limited.
    bool cpMaxTimeIsLimited_;

    /// The maximum control point time in a given field.
    float cpMaxTime_;

    /// Minimum position on the x axis
    float minXAx_;

    /// Maximum position on the x axis
    float maxXAx_;

    /// Minimum position on the y axis
    float minYAx_;

    /// Maximum position on the y axis
    float maxYAx_;
};

#endif // DELIVERYMACHINEMODEL_HPP
