/**
 * @file LinearProjectionOptimizer.cpp
 * LinearProjectionOptimizer Class implementation.
 */

#include "LinearProjectionOptimizer.hpp"

/**
 * Constructor
 */
LinearProjectionOptimizer::LinearProjectionOptimizer(Plan* thisplan, string log_file_name, Optimization::options options)
  : Optimization(thisplan,log_file_name,options)
{
}

/**
 * initialize
 */
void LinearProjectionOptimizer::initialize()
{
}


/**
 * Optimize plan 
 */
void LinearProjectionOptimizer::optimize()
{
  linear_projection_solver();
}




/**
 * Optimize plan through projection onto linear constraints
 */
void LinearProjectionOptimizer::linear_projection_solver()
{
    if ( the_plan_->get_parameter_vector()->getVectorType() != BIXEL_VECTOR_TYPE ) {
        throw std::logic_error("Cannot use linear projection solver with parameter vector that is not bixel weights");
    }
    // Restart the timer
    timer_.restart();

    std::cout << "Starting Linear Projection Solver..." << endl;

    // check if we optimize by iteratively tightening constraints
    if(!options_.lps_maxmin_ && !options_.lps_minmax_) {

        cout << "No tightening of constraints, just looking for feasible plan.." << endl;

        // not optimizing, just looking for feasible solution
        bool is_feasible = linear_feasibility_solver();
        if(is_feasible) {
            cout << "Found feasible plan" << endl;
        } else {
            cout << "Could not find feasible plan, returning Plan optained" << endl;
        }
    } else {
        // optimizing plan by tightening constraint
        cout << "Optimize plan by tightening of constraints: " << endl;

        // get index of constraint to tighten
        unsigned int consNo = options_.lps_objNo_ - 1;

		// check that the constraints supports this
		if(!the_plan_->supports_projection_optimizer(consNo)) {
		  cout << "Constraint " << consNo+1 << " does not support that" << endl;
		  throw(std::runtime_error("Cannot continue"));
		}

		if(options_.lps_maxmin_) {
		  cout << "Maximizing the min dose for CONS " << consNo+1 << endl;
		} else {
		  cout << "Minimizing the max dose for CONS " << consNo+1 << endl;
		}
		
		bool stopped = false;
		bool is_feasible;
		float currentbound;
		unsigned int iStep = 0;
		
		// get the initial upper and lower bounds
		float upperbound = the_plan_->get_upper_constraint_bound(consNo);
		float lowerbound = the_plan_->get_lower_constraint_bound(consNo);
		
		// check that feasible constraint set is really feasible
		is_feasible = linear_feasibility_solver();
		if(!is_feasible) {
		  cout << "Initial set of constraints is infeasible." << endl;
		  cout << "Returning Plan obtained so far." << endl;
		  stopped = true;
		} else {
		  cout << "Initial set of constraints is feasible, start tightening..." << endl;
		  // backup intensity vector
		  the_plan_->backup_intensities();
		}
		
		// iteratively tighten constraint until problem is infeasible
		while(!stopped) {
		  
		  iStep++;
		  
		  // get new constraint value
		  currentbound = upperbound - 0.5*(upperbound-lowerbound);
		  cout << "Bisection search step " << iStep << ": current bound " << currentbound << endl;
		  
		  if(options_.lps_maxmin_) {
			// we are maximizing the min
			the_plan_->set_lower_constraint_bound(consNo,currentbound);
		  } else {
			// we are minimizing the max
			the_plan_->set_upper_constraint_bound(consNo,currentbound);
		  }
		  
		  // check feasibility
		  is_feasible = linear_feasibility_solver();
		  
		  // if new constraint is feasible, backup the plan, otherwise restore last feasible plan
		  if(is_feasible) {
			the_plan_->backup_intensities();
		  } else {
			the_plan_->restore_intensities();
		  }
		  
		  // adjust bounds according to bisection search
		  if(is_feasible) { // plan feasible
			if(options_.lps_maxmin_) { // maximizing
			  lowerbound = currentbound;
			} else { // minimizing
			  upperbound = currentbound;
			}
		  } else { // plan infeasible
			if(options_.lps_maxmin_) { // maximizing
			  upperbound = currentbound;
			} else { // minimizing
			  lowerbound = currentbound;
			}
		  }
		  
		  // check stopping criteria
		  if(timer_.get_time() >= options_.max_time_) {
			cout << "Ran out of time\n";
			stopped = true;
		  }
		  if(options_.lps_epsilon_ > upperbound-lowerbound) {
			cout << "Plan is optimal within epsilon = " << options_.lps_epsilon_ << endl;
			stopped = true;
		  }
		  cout << "Elapsed time: " << timer_.get_time() << endl;
		}
	}
}



/**
 * Find feasible solution through projection onto linear constraints
 * Returns true if feasible solution is found, false otherwise
 */
bool LinearProjectionOptimizer::linear_feasibility_solver()
{
  unsigned int nTotalProjections = 0;
  unsigned int nProjections = 0;
  unsigned int iteration = 0;

  //cout << "starting linear feasibilty solver..." << endl;
  //cout << "maximum number of projections: " << options_.max_steps_ << endl;

  // put all constraints back into the active set
  the_plan_->reset_active_set();

  while(true)
    {
      iteration++;

      nProjections = the_plan_->project_onto_constraints();
      nTotalProjections += nProjections;

      // check if active set is empty
      if(the_plan_->is_active_set_empty()) {
	// put all constraints back into the active set
	the_plan_->reset_active_set();
	//cout << "reseting active set" << endl;

	// do another projection step to see if all constraints are fulfilled
	nProjections = the_plan_->project_onto_constraints();

	if(nProjections==0) {
	  // performed zero projections, so all constraints are fulfilled
	  cout << "needed " << nTotalProjections << " projections" << endl;
	  return true;
	}
	else {
	  nTotalProjections += nProjections;
	}
      }

      // Check stopping criteria
      if(nTotalProjections >= options_.max_steps_) {
	cout << "Reached maximum number of " << options_.max_steps_ << " projections" << endl;
	cout << "Current constraint set is thought infeasible" << endl;
	return false;
      }
      //cout << "Iteration " << iteration << ": ";
      //cout << nProjections << " projections" << endl;
    }
  return false;
}

