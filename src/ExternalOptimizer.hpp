/**
 * @file ExternalOptimizer.hpp
 * ExternalOptimizer Class Header
 */

#ifndef EXTERNALOPTIMIZER_HPP
#define EXTERNALOPTIMIZER_HPP

class ExternalOptimizer;


#include "Optimization.hpp"
#include "Plan.hpp"



/**
 * 
 * ExternalOptimizer class is used to call an external solver
 */
class ExternalOptimizer : public Optimization
{
public:

  /// constructor
  ExternalOptimizer(Plan* thisplan, string log_file_name, Optimization::options options);

  /// optimize plan
  void optimize();

  void initialize();

private:

  void call_optpp(); 
  void call_mosek();

  unsigned int optTypeInd_;

};


#endif
