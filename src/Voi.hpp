#ifndef VOI_H
#define VOI_H

/**
 * @file Voi.hpp
 * Voi Class header
 * $Id: Voi.hpp,v 1.19 2008/03/13 22:06:17 bmartin Exp $
 */

class Voi;

#include <vector>
#include <string>
#include <iostream>
#include <assert.h>
#include <memory>
using std::cout;
using std::endl;


#include "Prescription.hpp"
#include "DoseVector.hpp"

/**
 * Volume of Interest Class.
 * Holds one Volumes Of Interest (target volume(s) and critical structures).
 * Also stores the associated prescription.
 */
class Voi
{

    public:
	// Constructors
	Voi(const unsigned int voiNo);

	// Destructor
	~Voi();


	/*
	 * General Voi Info:
	 */

	void set_name(string name) { structure_name_ = name; };
	string get_name() const {return structure_name_;};

	void set_description(string description);
	string get_description() const {return structure_description_;};

	void set_voiNo(unsigned int voiNo) {voiNo_ = voiNo;};
	unsigned int get_voiNo() const {return voiNo_;};

	/// Find out if a structure is air or not
	bool is_air() const {return is_air_;};
	void set_air(const bool is_air) { is_air_ = is_air;};

	/// Find out if a structure is an organ at risk or not
	bool is_organ_at_risk() const {return is_organ_at_risk_;};
	void set_organ_at_risk(const bool is_oar) {is_organ_at_risk_ = is_oar;};

	/// Find out if a structure is a target or not
	bool is_target() const {return is_target_;};
	void set_target(const bool is_target) { is_target_ = is_target;};

	inline void  set_EUD_p(const float EUD_p) { EUD_p_ = EUD_p;};
	inline float get_EUD_p() const { return EUD_p_;};

	inline void  set_alphabeta(const float value) { alphabeta_ = value;};
	inline float get_alphabeta() const { return alphabeta_;};

	inline void  set_alpha(const float value) { alpha_ = value;};
	inline float get_alpha() const { return alpha_;};

	inline void  set_beta(const float value) { beta_ = value;};
	inline float get_beta() const { return beta_;};

	inline void  set_betaalpha(const float value) { betaalpha_ = value;};
	inline float get_betaalpha() const { return betaalpha_;};

        //
	// Voxels in Voi
        //

	inline unsigned int get_nVoxels() const { return nVoxels_; };
	inline unsigned int get_voxel(const unsigned int voxelNo) const
	{return voxel_indices_[voxelNo];};

        /// Get a const reference to the list of voxels
        const vector<unsigned int> & get_voxelNos() const;

	// Reserve space for voxels
	void reserve_voxels( unsigned int nVoxels );

	/// Add a voxel to VOI
	void add_voxel(const unsigned int voxelNo);

	/// Add voxels to VOI
	void add_voxels(const vector<unsigned int> & voxelNos);

	/// Union with another VOI
	void union_Voi(const Voi & rhs);

	/// remove another VOI
	void remove_Voi(const Voi & rhs);

	// Prescription
	Prescription*  get_prescription_ptr() const;


	// Equivalent Uniform Dose
	// Calculate EUD based on dose in Voxel grid
	float calculate_EUD(const DoseVector &dose_vector);

	// Print a summary of the volumes of interest
	void print_voi_info_header(std::ostream& o) const;
	void print_voi_info(std::ostream& o) const;

	// Print statistics for the volumes of interest
	void calculate_stats(const DoseVector &);
	void print_voi_stats_header(std::ostream& o) const;
	void print_voi_stats(std::ostream& o) const;

	void set_color(const float red, const float green, const float blue);

	// Sampling
        // void set_sampling_fraction(const float sampling_fraction);
        // float get_sampling_fraction() const;

        /// Sample voxels into the supplied vector
	void sample_voxels(
                vector<unsigned int> & voxelNos,
                float sampling_fraction,
                bool sample_with_replacement = false) const;

        // void resample();
        // void resample_with_replacement();
        // unsigned int get_sampled_voxel(const int sampleNo) const;
        // unsigned int get_nSamples() const;

	void set_desired_dose(const float desired_dose);
	float get_desired_dose() const;

    private:

	// Properties

	/// The name of the structure
	string structure_name_;

	/// The name of the structure
	string structure_description_;

	/// The structure number
	unsigned int voiNo_;

	/// True if VOI is air
	bool is_air_;

	bool is_target_;
	bool is_organ_at_risk_;

	/// the p-value for the VOI (for calculating EUD)
	float EUD_p_;

    /// the alpha over beta ratio from the LQ model
    float alphabeta_;

    /// the alpha parameter from the LQ model
    float alpha_;

    /// the beta parameter from the LQ model
    float beta_;

    /// the beta parameter from the LQ model
    float betaalpha_;

	/// Color, between 0 and 1
	float red_, green_, blue_;

	/// The voxel indices for each VOI.
	vector<unsigned int> voxel_indices_;
	unsigned int nVoxels_;

	/// Prescriptions for the voi
	std::auto_ptr<Prescription> prescription_;

	/// Statistics
	float max_dose_, min_dose_, total_dose_, mean_dose_;

	/// Statistics
        float EUD_, standard_deviation_;

	/// Fraction of voxels to be included in sampling
        // float sampling_fraction_;

	/// Sampled voxels drawn from VOI
	vector<unsigned int> samples_;

	float desired_dose_;

};

/*
 * Inline functions
 * ^^^^^^^^^^^^^^^^
 */


/**
 * Get a const reference to the list of voxels
 */
inline
const vector<unsigned int> & Voi::get_voxelNos() const
{
    return voxel_indices_;
}


// Add a voxel to VOI
inline
void Voi::add_voxel(const unsigned int voxelNo)
{
  // Assert that the voxel added are sorted
  if(nVoxels_ > 0) {
    assert(voxelNo > voxel_indices_.back());
  }
  nVoxels_++;
  voxel_indices_.push_back(voxelNo);
}


/// Get pointer to the prescription for voxel
inline
Prescription* Voi::get_prescription_ptr() const
{
  return prescription_.get();
}



inline
void Voi::set_color(const float red, const float green, const float blue)
{
  red_ = red;
  green_ = green;
  blue_ = blue;
}


// inline
// void Voi::set_sampling_fraction(const float sampling_fraction)
// {
//     sampling_fraction_ = sampling_fraction;
// }
//
// inline
// float Voi::get_sampling_fraction()const
// {
//     return sampling_fraction_;
// }


inline
float Voi::get_desired_dose() const
{
  return desired_dose_;
}


// inline
// unsigned int Voi::get_sampled_voxel(const int sampleNo) const
// {
  // return samples_[sampleNo];
// }

// inline
// unsigned int Voi::get_nSamples() const
// {
  // return samples_.size();
// }

inline
void Voi::set_description(string description)
{
    structure_description_ = description;
}

inline
void Voi::set_desired_dose(float dose)
{
    desired_dose_=dose;
}

#endif
