/**
 * @file BixelVector.hpp
 * BixelVector Class Definition.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef BIXELVECTOR_HPP
#define BIXELVECTOR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>
#include <stdexcept>
#include <exception>
#include <limits>
using std::numeric_limits;

// Predefine class before including others
class BixelVector;

#include "ParameterVector.hpp"
#include "BixelVectorDirection.hpp"
#include "Geometry.hpp"


/**
 * Class BixelVector handles individual beams and their bixel intensities.
 * The constructor creates set of bixel positions for those bixels that connect with target voxels.
 */
class BixelVector : public ParameterVector {

public:

    /**
     * Constructor.
     * Initializes bixelgrid with a given number of beamlets.
     * By default, each beamlet has zero intensity and is in the aperture
     *
     * @param nJ            The number of bixels in vector.
     * @param set_aperture  Boolean flag for all bixels indicating if the bixel is within the open aperture or not
     *                      (default=true).
     */
    BixelVector( const unsigned int &nJ=0, const bool &set_aperture=true )
            :  ParameterVector( nJ, 0.0f, 0.0f, std::numeric_limits<float>::infinity(),
            set_aperture, 0.0f ) {};

    /**
     * Copy constructor
     */
    BixelVector( const BixelVector &bv) : ParameterVector(bv) {};

    /**
     * If setting bixel as inactive then also set value to zero
     */
    virtual void set_isActive( const int ip, const bool isActive ) {
        if ( !isActive ) set_value( ip, 0.0f );
        isActive_[ip] = isActive;
    }

    /**
     * Read bwf files.
     * Read bixel vector from the beam weight files.
     * N.B. This is not implemented here but is set as a template for derived classes.
     *
     * @param bwf_file_root     The file path and file name prefix from which the full bwf file name can be derived.
     */
  inline virtual void load_bwf_files( const string & bwf_file_root ) {
        std::cout << "load_bwf_files is only implemented for derived classes" << std::endl;
        throw( std::runtime_error( "cannot load bwf files" ) );
    }

    /**
     * Read bwf files.
     * Read bixel vector from the beam weight files.
     * N.B. This is not implemented here but is set as a template for derived classes.
     *
     * @param bwf_file_root     The file path and file name prefix from which the full bwf file name can be derived.
     */
  inline virtual void load_bwf_file( const string & bwf_file_name, unsigned int beamNo ){
        std::cout << "load_bwf_file is only implemented for derived classes" << std::endl;
        throw( std::runtime_error( "cannot load bwf files" ) );
    }

    /**
     * Read bwf files within the aperture.
     * Read bixel vector from the beam weight files for those bixels within the open aperture.
     * N.B. This is not implemented here but is set as a template for derived classes.
     *
     * @param bwf_file_root     The file path and file name prefix from which the full bwf file name can be derived.
     */
    inline virtual void load_bixels_in_aperture( const string & stf_file_root ) {
        std::cout << "load_bixels_in_aperture is only implemented for derived classes" << std::endl;
        throw( std::runtime_error( "cannot load stf files" ) );
    }

    /**
     * Write bwf files.
     * Write the beam weight files from the bixel vector.
     * N.B. This is not implemented here but is set as a template for derived classes.
     *
     * @param bwf_file_root     The file path and file name prefix from which the full bwf file name can be derived.
     */
    inline virtual void write_bwf_files( const string & bwf_file_root ) const {
        std::cout << "Warning: write_bwf_files is only implemented for derived classes" << std::endl;
        throw( std::runtime_error( "cannot write bwf files" ) );
    }

    /**
     * Write bwf files.
     * Write the beam weight files from the bixel vector for the defined beam number.
     * N.B. This is not implemented here but is set as a template for derived classes.
     *
     * @param beamNo            The index of the beam for which the bwf file should be written
     * @param bwf_file_root     The file path and file name prefix from which the full bwf file name can be derived.
     */
    inline virtual void write_bwf_file( const string & bwf_file_name, const unsigned int beamNo ) const {
        std::cout << "Warning: write_bwf_files is only implemented for derived classes" << std::endl;
        throw( std::runtime_error( "cannot write bwf files" ) );
    }

    // Replace inherritted virtual functions with 'bwf' equivalents.
    inline void load_parameter_files( string parameters_file_root ) {
        this->load_bwf_files( parameters_file_root );
    };

    inline void load_parameter_file( string parameters_file_name, unsigned int beamNo ) {
        this->load_bwf_file( parameters_file_name, beamNo );
    };

    inline void write_parameter_files( string parameters_file_root ) const {
        write_bwf_files( parameters_file_root );
    };

    inline void write_parameter_file( string parameters_file_name, const unsigned int beamNo ) const {
        write_bwf_file( parameters_file_name, beamNo );
    };

    // Intensity functions are related to the value of those parameters which control the beam weighting.
    /**
     * Get the value of the bixel with maximum intensity
     */
    float get_max_intensity() const;

    /**
     * Set the value of all bixels to one.
     */
    inline void set_unit_intensities() {
    // TODO (dualta#1#): Should this be only for active bixels ?
        for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
            set_value(pi, 1.0f);
        }
    };

    /**
     * Set the value of all bixels to zero.
     */
    inline void set_zero_intensities() {
        for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
            set_value(pi, 0.0f);
        }
    };

    /**
     * Scale the value of all bixels.
     */
    inline void scale_intensities( float scale_factor ) {
        assert(scale_factor >= 0);
        for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
            value_[pi] *= scale_factor;
        }
    };

    /**
     * set all intensities to zero for bixels not in the current beam
     */
    inline void extract_beam(const Geometry* geometry, unsigned int beamNo) {
	    for (unsigned int i=0; i < this->get_nParameters(); i++) {
		    unsigned int iBeam = geometry->get_beamNo(i);
		    if (iBeam != beamNo) {
			    this->set_intensity(i,0.0);
		    }
	    }
    };


    /**
     * Get the beam number
     */
    inline unsigned int get_beamNo( const int &ip ) const {
      // TODO Write this function taking code from Geometry class
      cout << "Called BixelVector::get_beamNo()" << endl;
      throw(std::runtime_error("Function not implemented"));
    };

    /**
     * Get a pointer to a copy of the object.
     */
    virtual std::auto_ptr<ParameterVector> get_copy() const {
        std::auto_ptr<ParameterVector> temp( new BixelVector( *this ) );
        return temp;
    };

    /**
     * Get a pointer to a copy of the object.
     */
    virtual std::auto_ptr<BixelVector> get_bixel_copy() const {
        std::auto_ptr<BixelVector> temp( new BixelVector( *this ) );
        return temp;
    };

    inline void push_back_parameter(const float &value, const bool &isActive) {
        // Call parent class method with default bounds and step size of first element.
        // In BixelVector all elements should have the same step size.
        ParameterVector::push_back_parameter( value, 0.0f, std::numeric_limits<float>::infinity(),
                                              true, 1.0f );
    };

    // Create functions with names and arguments more relevant to BixelVectors

    /**
	 * Get number of bixels in the bixel maps produced by the parameter translation.
     */
	inline unsigned int get_nBixels() const {
        return this->get_nParameters();
    };


    /**
     * Get the intensity of the bixel element.
     */
    inline float get_intensity(const int i) const {
        return this->get_value(i);
    };


    /**
     * Set the intensity of the bixel element.
     */
    inline void set_intensity(const int i, const float value) {
        this->set_value(i, value);
    };


    /**
     * Get the aperture status of the bixel element.
     */
    inline bool within_aperture( const int ip ) const {
        return this->get_isActive(ip);
    };


    /**
     * Set the activity flag for the parameter element at index.
     */
    inline void set_aperture( const int ip, const bool isActive ) {
        this->set_isActive(ip, isActive);
    };


    /**
     * Adds a new element at the end of the vector, after its current last element.
     *
     * @param value     The value for the new element.
     * @param isActive  The activity flag for the new element.
     */
    inline void push_back_beamlet( float value, bool isActive ) {
        push_back_parameter( value, isActive );
    };


    /**
     * Translate the ParameterVector into a BixelVector
     * Warning calling function should free returned object.
     *
     * Object is already a BixelVector so just return a copy.
     */
    inline BixelVector * translateToBixelVector() const {
        return new BixelVector(*this);
    };


    /**
     * Translate the gradient in terms of bixel parameters to the gradient in terms of
     * the parameters used to describe plan.
     *
     * Using BixelVector to describe plan so return the bixel gradient unchanged.
     */
    inline ParameterVectorDirection translateBixelGradient( BixelVectorDirection & bvd ) const {
        bvd.setVectorType(GENERAL_PVD_TYPE);
        return bvd;
    };

    /**
     * Translate the Bixel Vector to a Parameter Vector
     *
     * Using BixelVector to describe plan so return the bixel vector unchanged.
     */
    inline void translateBixelVector( BixelVector & bv ) {
      for(unsigned int i=0; i<get_nBixels(); i++) {
	(*this).set_intensity(i,bv.get_intensity(i));
      }
    };


    /**
     * Translate the hessian in terms of bixel parameters to the hessian in terms of
     * the parameters used to describe plan.
     *
     * Using BixelVector to describe plan so return the hessian unchanged.
     */
    inline vector<float> translateBixelHessian( vector<float> & hv ) const {
        return hv;
    };


    /**
     * Get the current type of parameter vector
     *
     * @return the parameter vector type enum indicating that this is a bixel vector
     */
    virtual inline ParameterVectorType getVectorType() const {
        return BIXEL_VECTOR_TYPE;
    }

};

/**
 * Find the maximum value within the ParameterVector.
 *
 * @return the maximum value as a float.
 */
inline float BixelVector::get_max_intensity() const {
    // Initialise max_value to an extremly negative number.
    float max_value = -(std::numeric_limits<float>::max());

    for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
        float temp = this->get_value(pi);
        if ( temp > max_value ) {
            max_value = temp;
        }
    }

    return max_value;
}

#endif // BIXELVECTOR_HPP
