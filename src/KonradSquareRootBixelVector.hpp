
/**
 * @file KonradSquareRootBixelVector.hpp
 * KonradSquareRootBixelVector Class Definition.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef KONRADSQUAREROOTBIXELVECTOR_HPP
#define KONRADSQUAREROOTBIXELVECTOR_HPP

// Predefine class before including others
class KonradSquareRootBixelVector;

#include "BixelVector.hpp"
#include "SquareRootBixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Geometry.hpp"

/**
 * Class KonradSquareRootBixelVector constructs a ParameterVector of type SquareRootBixelVector 
 * from Konrad type bwf and stf files
 */
class KonradSquareRootBixelVector : public SquareRootBixelVector {
public:
  
  // Constructors
  
  /// Read bixel vector from .bwf and (optionally) .stf files.
  KonradSquareRootBixelVector( Geometry *geometry,  
			       const unsigned int nInstances,  
			       const unsigned int nBeams_per_instance,
				   const float min_weight,
			       const string bwf_file_root,  
			       const string stf_file_root = "" );
  
  
  /// construct bixel vector based on Dij matrix
  KonradSquareRootBixelVector( const DoseInfluenceMatrix & Dij,  
			       Geometry *geometry,  
			       const unsigned int nInstances,
				   const unsigned int nBeams_per_instance,
				   const float min_weight );

  /// Copy Constructor
  KonradSquareRootBixelVector( const KonradSquareRootBixelVector &rhs );


  /// Get a pointer to a copy of the object.
  std::auto_ptr<ParameterVector> get_copy() const;
  
  // Read and write files
  void load_bwf_files(const string & bwf_file_root);
  void load_bwf_file(const string & bwf_file_name, unsigned int beamNo);
  void write_bwf_files(const string & bwf_file_root) const;
  void write_bwf_file(const string & bwf_file_name, const unsigned int beamNo) const;
  void load_bixels_in_aperture(const string & stf_file_root);



private:

  // default constructor not allowed
  KonradSquareRootBixelVector();

};



#endif 
