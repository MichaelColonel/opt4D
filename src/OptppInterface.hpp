/**
 * @file OptppInterface.hpp
 * OptppInterface Class Header
 *
 */

#ifndef OPTPPINTERFACE_HPP
#define OPTPPINTERFACE_HPP

#include "Plan.hpp"
#include "Optimization.hpp"

class OptppInterface;

//extern OptppInterface* global_optpp;
//extern const int myglobali = 13;



#ifndef HAVE_OPTPP

// Opt++ is not available
// create dummy class that prints error message

/**
 * OptppInterface is used to optimize a plan using the package Opt++. 
 * The macro HAVE_OPTPP was not defined at compilation. This is a dummy class which prints 
 * an error message if the Opt++ interface is used.
 */

class OptppInterface 
{
public:

    /// constructor
    OptppInterface(Plan* the_plan, Optimization::options* options);
    
    /// destructor
    ~OptppInterface();
    
    /// just prints out that Optpp is not available
    void optimize();

    /// returns false
    bool optpp_is_available() {return false;};

private:

    OptppInterface();

    /// The plan to be optimized
    Plan* the_plan_;

    /// Optimization options
    Optimization::options* options_;

};

#else

// Optpp is available

#include "NLF.h"
#include "NLP1.h"
#include "CompoundConstraint.h"
#include "Opt.h"

using namespace::OPTPP;
using NEWMAT::ColumnVector;


/**
 * OptppInterface is used to optimize a plan using the package Opt++. 
 */

class OptppInterface
{
public:

  /// constructor
  OptppInterface(Plan* the_plan, Optimization::options* options);
  
  /// destructor
  ~OptppInterface();
  
  /// optimize plan using Opt++
  void optimize();
  
  /// evaluate objective and gradient
  void get_objective_and_gradient(int mode, 
				  int nVar_,                     // number of optimization variables
				  const ColumnVector& x,         // the optimization variables
				  real& optpp_objective,         // objective value
				  ColumnVector& optpp_gradient,  // gradient
				  int& result_type);

  /// initialize optimization variables
  void initialize(int nVar, ColumnVector& x);

  /// returns true
  bool optpp_is_available() {return true;};

private:

  OptppInterface();

  /// generate constraints
  void make_constraints();

  /// construct the optimizer object 
  void init_optimizer();

  /// set the parameter_vector_ to the Plan's parameter vector
  void synchronize_parameter_vector();

  /// copy the values of the Opt++ optimization variables to parameter_vector_
  void set_parameter_vector(const ColumnVector& x);

  /// copy the gradient obtained form opt4D to the opt++ internal gradient vector
  void set_optpp_gradient(ColumnVector& optpp_gradient) const;

  /// returns lower variable bounds
  ColumnVector get_lower_bounds();

  /// returns upper variable bounds
  ColumnVector get_upper_bounds();

  /// the plan to be optimized
  Plan* the_plan_;

  /// Optimization options
  Optimization::options* options_;

  /// the opt++ nonlinear programming problem with first derivatives available
  NLF1* problem_;

  /// the opt++ optimizer object
  OptimizeClass* optimizer_;

  /// all constraints, including variable bounds
  CompoundConstraint* constraints_;
  
  /// total number of optimization variables
  int nVar_;

  /// gradient
  ParameterVectorDirection gradient_;

  /// Scratch ParameterVector for use by any function
  std::auto_ptr<ParameterVector> parameter_vector_;

  /// values of the individual objectives
  vector<double> multi_objective_;

};




#endif


#endif
