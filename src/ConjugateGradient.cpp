
/**
 * @file ConjugateGradient.cpp
 * ConjugateGradient Class implementation.
 * 
 * $Id: ConjugateGradient.cpp,v 1.1 2007/10/10 00:35:16 bmartin Exp $
 */


#include "ConjugateGradient.hpp"


/**
 * Constructor: initialize options
 *
 */
ConjugateGradient::ConjugateGradient(
	unsigned int nParams ///< The number of bixels
	)
:   Optimizer(nParams),
    last_gradient_(nParams),
    last_direction_(nParams),
    is_initialized_(false)
{
}


/*
 * ConjugateGradient functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/// Calculate the direction based on just the gradient
void ConjugateGradient::calculate_direction(
	const ParameterVectorDirection & gradient,
	const ParameterVector & param)
{
    // Preform any first step actions if this is the first time this function 
    // was called
    if(!is_initialized_) {

        // The first direction is the negative gradient
        the_direction_ = gradient;
        the_direction_ *= -1;

        is_initialized_ = true;
    }
    else {
        // We're already initialized, so find the direction

        // Calculate the beta value
        double beta =  gradient.dot_product(gradient - last_gradient_)
            / (last_gradient_.dot_product(last_gradient_));
        cout << "beta: " << beta << "\n";

        // Set the direction
        the_direction_ = gradient;
        the_direction_ *= -1;
        last_direction_ *= beta;
        the_direction_ += last_direction_;
    }

    // Store the gradient and direction for use next time
    last_direction_ = the_direction_;
    last_gradient_ = gradient;
}

