/**
 * @file VoiParser.hpp
 * Header for VoiParser class.
 *  $Id: VoiParser.hpp,v 1.3 2008/04/14 15:29:29 bmartin Exp $
 */

#ifndef VOIPARSER_HPP
#define VOIPARSER_HPP

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <iostream>
class VoiParser;

/**
 * The VoiParser class reads voi files
 *
 * Reads volumes of interest stored in voi format.  Because of the file format,
 * this is accomplished in two passes, one to find the number of voxel in each
 * structure and one to store the index of each voxel in the appropriate volume.
 * <p>
 * Here's the voi file format:<br>
 * Voi files are simply a list of 8 bit integers.  Each integer tells
 * the volume of interest that the corresponding voxel in the dose cube
 * belongs to.  For example, structure 1 is represented by a 0 in the
 * voi file.  Volume of interest 128 (127 in file) corresponds to air
 * and is ignored during optimization.
 * <p>
 * These voi numbers are written in column, slice, row order.  For
 * example, a 3x4x2 dose cube in CERR would be written in this order:
 * <pre>
 *  slice 1: (toward feet)
 *       front       r
 * l  [ 1  2  3  4]  i
 * e  [ 9 10 11 12]  h
 * f  [17 18 19 20]  h
 * t     back        t
 * </pre><pre>
 *  slice 2: (toward head)
 *       front       r
 * l  [ 5  6  7  8]  i
 * e  [13 14 15 16]  h
 * f  [21 22 23 24]  h
 * t     back        t
 * </pre>
 * Note that left corresponds to patient right and vice versa.  Returns the 
 * number of voxels in the file.
 */
class VoiParser
{
    public:

        // Default constructor
        VoiParser(bool include_air = false)
            : voi_indices_(),
            include_air_(include_air),
            nVoxels_(0),
            nNonair_voxels_(0)
        {};

        // Constructor (Read the file)
        VoiParser(
                const string & file_name,
                bool include_air
                );

        // Construct from a vector of vectors of indices
        VoiParser(
                const vector<vector<unsigned int> > & voi_indices,
                bool include_air,
                unsigned int nVoxels,
                unsigned int nNonair_voxels);

        // Read a file
        void read_file(const string & file_name);

        // Write a voi file
        void write_file(
                const string & file_name
                ) const;

        // Get a the indices in a VOI
        const vector< unsigned int> & operator[](
                const size_t voiNo
                ) const
        { return voi_indices_[voiNo]; };

        // Get a the indices in a VOI
        const vector< unsigned int > & get_voi(
                const size_t voiNo
                ) const
        { return voi_indices_[voiNo]; };

        // Get the number of vois present
        unsigned int get_nVois() const
        { return voi_indices_.size(); };

        // Get the number of voxels in the voi file
        unsigned int get_nVoxels() const
        { return nVoxels_; };

        // Get the number of nonair voxels
        unsigned int get_nNonair_voxels() const
        { return nNonair_voxels_; };


    private:
        vector<vector< unsigned int> > voi_indices_;
        const bool include_air_;
        unsigned int nVoxels_;
        unsigned int nNonair_voxels_;
};

#endif
