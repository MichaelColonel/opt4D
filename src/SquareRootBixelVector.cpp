/**
 * @file SquareRootBixelVector.cpp
 * SquareRootBixelVector class implementation
 *
 */

#include "SquareRootBixelVector.hpp"
#include <cmath>
#include <typeinfo>

/**
 * Constructor.
 *
 * @param nJ            The number of bixels in vector.
 * @param set_aperture  Boolean flag for all bixels indicating if the bixel is within the open aperture or not
 *                      (default=true).
 */
SquareRootBixelVector::SquareRootBixelVector( const float min_weight, const unsigned int &nJ, const bool &set_aperture )
  : ParameterVector( nJ, 0.0f, std::numeric_limits<float>::min(),
		     std::numeric_limits<float>::max(), set_aperture, 0.0f )
  , bixel_vector_()
  , min_weight_(min_weight)
{
  //  cout << "Constructing SquareRootBixelVector" << endl;
  bixel_vector_.reset(new BixelVector(nJ));
}

/**
 * Copy Constructor.
 */
SquareRootBixelVector::SquareRootBixelVector( const SquareRootBixelVector &rhs)
  : ParameterVector(rhs)
  , bixel_vector_()
  , min_weight_(rhs.min_weight_)
{
  //  cout << "Calling SquareRootBixelVector Copy constructor" << endl;
  bixel_vector_ = (*rhs.bixel_vector_).get_bixel_copy();
}


/**
 * Get a pointer to a copy of the object.
 */
std::auto_ptr<ParameterVector> SquareRootBixelVector::get_copy() const {
  std::auto_ptr<ParameterVector> temp( new SquareRootBixelVector( *this ) );
  return temp;
}

/**
 * synchronize the internal bixelvector with the current parameters
 */
void SquareRootBixelVector::synchronize_bixel_vector() const
{

  assert(nParams_== bixel_vector_->get_nParameters());

  for(unsigned int iBixel=0;iBixel<nParams_;iBixel++) {
    bixel_vector_->set_value(iBixel, min_weight_+(value_[iBixel]*value_[iBixel]));
  }
}


/**
 * initialize the parameters with the internal bixelvector
 */
void SquareRootBixelVector::initialize()
{
  // number of parameters
  nParams_ = bixel_vector_->get_nParameters();

  // parameter value
  value_.resize(nParams_,0);
  for(unsigned int iBixel=0;iBixel<nParams_;iBixel++) {
    if(bixel_vector_->get_value(iBixel) >= min_weight_) {
      value_[iBixel] = sqrt(bixel_vector_->get_value(iBixel) - min_weight_);
    }
    else {
      value_[iBixel] = 0;
	  bixel_vector_->set_value(iBixel, min_weight_);
      //cout << "WARNING: negative beam weight found when initializing SquareRootBixelVector" << endl;
      cout << "WARNING: found beam weight below minimum weight when initializing SquareRootBixelVector" << endl;
      cout << "min weight: " << min_weight_ << endl;
      cout << "beam weight " << iBixel << ": " << bixel_vector_->get_value(iBixel) << endl;
    }
  }

  // parameter minimum allowed value
  lowBound_.resize(nParams_,std::numeric_limits<float>::min());
  // parameter minimum allowed value
  highBound_.resize(nParams_,std::numeric_limits<float>::max());
  // parameter is active
  isActive_.resize(nParams_,true);
}


/**
 * add parameter
 */
void SquareRootBixelVector::push_back_parameter( const float &value, const float &lowBound, const float &highBound,
												 const bool &isActive, const float &step_size ) {
  cout << "calling SquareRootBixelVector::push_back_parameter" << endl;

	nParams_++;
	value_.push_back( value );
	lowBound_.push_back( lowBound );
	highBound_.push_back( highBound );
	isActive_.push_back( isActive );
	step_size_.push_back( step_size );

	bixel_vector_->push_back_parameter(0,true);

	synchronize_bixel_vector();
}


/**
 * Translate the ParameterVector into a BixelVector
 * Warning calling function should free returned object.
 */
BixelVector* SquareRootBixelVector::translateToBixelVector() const
{
  synchronize_bixel_vector();
  /*
  cout << "SquareRootBixelVector::translateToBixelVector()" << endl;
  cout << "type: " << typeid(*bixel_vector_).name() << endl;
  BixelVector* temp = new BixelVector(*bixel_vector_);
  cout << "type: " << typeid(*bixel_vector_).name() << endl;
  return temp;
  */
  return new BixelVector(*bixel_vector_);
}


/**
 * Translate the gradient in terms of bixel parameters to the gradient in terms of
 * the parameters used to describe plan.
 */
ParameterVectorDirection SquareRootBixelVector::translateBixelGradient( BixelVectorDirection & bvd ) const
{
  bvd.setVectorType(GENERAL_PVD_TYPE);

  for(unsigned int iBixel=0; iBixel<nParams_; iBixel++) {
    bvd[iBixel] *= 2*value_[iBixel];
  }

  return bvd;
}


/**
 * Translate the Bixel Vector to a Parameter Vector
 */
void SquareRootBixelVector::translateBixelVector( BixelVector & bv )
{
  float x;
  for(unsigned int iBixel=0;iBixel<nParams_;iBixel++) {
    x = bv.get_value(iBixel);
    bixel_vector_->set_value(iBixel,x);
    if(x >= min_weight_) {
      value_[iBixel] = sqrt(x-min_weight_);
    }
    else {
      value_[iBixel] = 0;
      cout << "WARNING: found beam weight below minimum weight when translating BixelVector to SquareRootBixelVector" << endl;
    }
  }
}


/**
 * Get the beam number
 */
unsigned int SquareRootBixelVector::get_beamNo( const int &ip ) const
{
  // TODO Write this function taking code from Geometry class
  cout << "Called SquareRootBixelVector::get_beamNo()" << endl;
  throw(std::runtime_error("Function not implemented"));
}

/*
 * write / load functions not implemented
 */

void SquareRootBixelVector::load_bwf_files( const string & bwf_file_root ) {
  std::cout << "load_bwf_files is only implemented for derived classes" << std::endl;
  throw( std::runtime_error( "cannot load bwf files" ) );
}

void SquareRootBixelVector::load_bwf_file( const string & bwf_file_name, unsigned int beamNo ){
  std::cout << "load_bwf_file is only implemented for derived classes" << std::endl;
  throw( std::runtime_error( "cannot load bwf files" ) );
}

void SquareRootBixelVector::load_bixels_in_aperture( const string & stf_file_root ) {
  std::cout << "load_bixels_in_aperture is only implemented for derived classes" << std::endl;
  throw( std::runtime_error( "cannot load stf files" ) );
}

void SquareRootBixelVector::write_bwf_files( const string & bwf_file_root ) const {
  std::cout << "Warning: write_bwf_files is only implemented for derived classes" << std::endl;
  throw( std::runtime_error( "cannot write bwf files" ) );
}

void SquareRootBixelVector::write_bwf_file( const string & bwf_file_name, const unsigned int beamNo ) const {
  std::cout << "Warning: write_bwf_files is only implemented for derived classes" << std::endl;
  throw( std::runtime_error( "cannot write bwf files" ) );
}
