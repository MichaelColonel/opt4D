/**
 * @file dif2diagonaldij.cpp
 * The main file for dif2diagonaldij.
 *
 * Creates a diagonal dij file from a dif file.
 *
 * $Id: dif2diagonaldij.cpp,v 1.2 2008/03/05 01:11:57 bmartin Exp $
 */

/**
 * \par Welcome to dif2diagonaldij.
 *
 * dif2diagonaldij is a utility to create a diagonal dij file from a dif file.
 *
 *
 * Type dif2diagonaldij 
 * --help for usage information.
 */

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

// #include <iomanip>
#include <exception>
// #include <fstream>
// #include <limits>
#include <tclap/CmdLine.h>
#include <string>
using std::string;

#include "Geometry.hpp"
#include "DijFileParser.hpp"

void write_diagonal_dij(
        string dif_file_name,
        string dij_file_name,
        string voi_file_name,
        vector<int> voiNos
        );

/**
 * Main function to interpret command line options and run program.
 */
int main(int argc, char* argv[])
{
    string dif_file_name, dij_file_name, voi_file_name;
    vector<int> voiNos;


    // Wrap everything in a try block to catch TCLAP exceptions.
    try {

        /*
         * Defining the command line syntax
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         *
         * We define the command line object, and add various switches and 
         * value arguments to it.  The CmdLine object parses the argv array 
         * based on the Arg objects that it contains.
         */

        // Define the command line object and insert a message
        // that describes the program. This is printed last in the help text.  
        // The second argument is the delimiter (usually space) and the last 
        // one is the version number.   
        TCLAP::CmdLine cmd("dif2diagonaldij diagonal dij creation tool",
                ' ', "0.1");

        /*
         * TCLAP syntax
         * ^^^^^^^^^^^^
         *          
         * TCLAP reads two kinds of command line parameters, value arguments 
         * and flags.  Value argments are used to pass data to the program 
         * (such as file names).  Flags are used to pass true or false 
         * information to the program.
         *
         * SwitchArg
         * ---------
         * Switches are used to tell the program something true or false, but 
         * must be false by default, so the default behavior comes from not 
         * using any flags at all.
         *
         * SwitchArg constructor:
         *
         * TCLAP::SwitchArg::SwitchArg  ( const std::string &   flag,
         *                                const std::string &   name,
         *                                const std::string &   desc,
         *                                CmdLineInterface &    parser)
         *
         * Parameters:
         *     flag   - The one character flag that identifies this argument
         *              on the command line.
         *     name   - A one word name for the argument. Can be used as a
         *              long flag on the command line.
         *     desc   - A description of what the argument is for or does.
         *     parser - A CmdLine parser object to add this Arg to
         *
         *
         * ValueArg
         * --------
         * Value arguments can read any type data into a variable that you 
         * create.  Most arguments use this format.
         *
         * ValueArg constructor:
         *
         * template<class T>
         * TCLAP::ValueArg< T >::ValueArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * Parameters:
         *     flag     - The one character flag that identifies this argument
         *                on the command line.
         *     name     - A one word name for the argument. Can be used as a
         *                long flag on the command line.
         *     desc     - A description of what the argument is for or does.
         *     req      - Whether the argument is required on the command line.
         *     value    - The default value assigned to this argument if it is
         *                not present on the command line.
         *     typeDesc - A short, human readable description of the type that
         *                this object expects. This is used in the generation 
         *                of the USAGE statement. The goal is to be helpful to 
         *                the end user of the program.
         *      parser  - A CmdLine parser object to add this Arg to
         *
         * UnlabeledValueArg
         * ----------------
         *
         * The plan file name is read by an UnlabeledValueArg, so any argument 
         * that is not processed by something else will be assumed to be the 
         * plan file name.  Don't add any other UnlabeledValueArg arguments!!!
         *
         * UnlabeledValueArg constructor:
         * 
         * template<class T>
         * TCLAP::ValueArg< T >::UnlabeledValueArg (
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 T                    value
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         *
         * MultiArg
         * --------
         *
         * If several values may be passed in for one argument, use a MultiArg, 
         * which returns a vector of values instead of a single one.
         *
         * MultiArg syntax:
         *
         * template<class T>
         * TCLAP::MultiArg< T >::MultiArg (const std::string &  flag,
         *                                 const std::string &  name,
         *                                 const std::string &  desc,
         *                                 bool                 req,
         *                                 const std::string &  typeDesc,
         *                                 CmdLineInterface &   parser)
         *
         * UnlabeldMultiArg
         * ----------------
         *
         *  Syntax:
         *
         *  template<class T>
         *  TCLAP::UnlabledMultiArg< T >::UnlabeledMultiArg (
         *              const std::string & name,
         *              const std::string & desc,
         *              bool                req,
         *              const std::string & typeDesc,
         *              CmdLineInterface &  parser,
         *              bool                ignoreable=false)
 	 *
         */

        /*
         * Add General options to cmd
         */

        // Input dif file name
        TCLAP::UnlabeledValueArg<std::string> dif_file_arg(
                "dif_file",
                "Input dif file name or prefix",
                true,
                "",
                "dif file name",
                cmd);

        // Output dij file name
        TCLAP::UnlabeledValueArg<std::string> dij_file_arg(
                "dij_file",
                "Output dij file name or prefix",
                true,
                "",
                "dij file name",
                cmd);

        // Voi file name
        TCLAP::ValueArg< std::string > voi_file_arg (
                "v",
                "voi_file",
                "Voi file to select voxels from",
                false,
                "",
                "voi file name",
                cmd);

        // Voi numbers
        TCLAP::MultiArg< int > voiNos_arg (
                "#",
                "voiNo",
                "VOI number of voxels to include",
                false,
                "int > 0",
                cmd);

        // Parse the command line
        cmd.parse(argc, argv);

        // Process dif file name
        dif_file_name = dif_file_arg.getValue();
        int loc = dif_file_name.rfind(".dif");
        if(loc + 4 != dif_file_name.size()) {
            // file suffix not present
            dif_file_name.append(".dif");
        }

        // Process dif file name
        dij_file_name = dij_file_arg.getValue();
        loc = dij_file_name.rfind(".dij");
        if(loc + 4 != dij_file_name.size()) {
            // file suffix not present
            dij_file_name.append("_1.dij");
        }

        // Read voi name and voi numbers
        if(voi_file_arg.isSet() && voiNos_arg.isSet()) {
            voi_file_name = voi_file_arg.getValue();
            voiNos = voiNos_arg.getValue();
        }
    }
    catch (TCLAP::ArgException &e) { // catch any exceptions
        std::cerr << "error: " << e.error() << " for arg " << e.argId()
            << std::endl;
    }
    catch (std::exception & e) { // Catch other exceptions
        std::cerr << "Error caught in dif2diagonaldij.cpp, main(): " << e.what()
                                                              << std::endl;
    }

    try {
        write_diagonal_dij(dif_file_name, dij_file_name,
                voi_file_name, voiNos);
    }
    catch (std::exception & e) { // Catch other exceptions
        std::cerr << "Error caught in dif2diagonaldij.cpp, main(): " << e.what()
                                                              << std::endl;
    }

    return(0);
}


/**
 * Write diagonal dij file.
 * Throws an exception if the input file does not exist or the output cannot be 
 * written to.
 *
 * @param dif_file_name The file to read
 * @param dij_file_name The file to write
 */
void write_diagonal_dij(
        string dif_file_name,
        string dij_file_name,
        string voi_file_name,
        vector<int> voiNos
        )
{

    // Read the dif file into a geometry object
    Geometry the_geometry(dif_file_name);

    // List of voxels to write out
    vector<bool> voxels_to_include(the_geometry.get_nVoxels(),true);
    int nVoxels_included = the_geometry.get_nVoxels();

    // Open voi file if needed and find out which bixels to write out
    if(voi_file_name.size() > 0) {
        std::ifstream voi_file;

        // Attempt to open the file
        voi_file.open(voi_file_name.c_str(), std::ios::in | std::ios::binary);
        if (!voi_file.is_open()) {
            cout << "Can't open file: " << voi_file_name << endl;
            exit(-1);
        }
        cout << "Reading VOI data from file: " << voi_file_name << endl;

        // Loop through file
        nVoxels_included = 0;
        for(int iVoxel = 0; iVoxel < the_geometry.get_nVoxels(); iVoxel++) {
            // verify file is not at end
            if(voi_file.eof()) {
                std::cerr << "Error, voi file is smaller than dose cube.\n";
                exit(-1);
            }

            // Read voxel VOI number
            char aChar;
            voi_file.get(aChar);
            int voiNo = ((int)aChar)+1;
            if (voiNo==128) {
                voiNo = 0;           // air
            }

            // Check if it matches one of the voiNos supplied
            voxels_to_include[iVoxel] = false;
            for(unsigned int iVoiNo = 0; iVoiNo < voiNos.size(); iVoiNo++) {
                if(voiNos[iVoiNo] == voiNo) {
                    voxels_to_include[iVoxel] = true;
                    nVoxels_included++; 
                    break;
                }
            }
        }

        // Done with voifile, so make sure the whole thing was read
        if(voi_file.eof()) {
            voi_file.close();
        } else {
            char achar;
            if(voi_file.get(achar)) {
                std::cerr << "Error, voi file is larger than dose cube.\n";
                exit(-1);
            }
        }
    }


    // Make the header
    DijFileParser::header head;
    head.a_g_ = 0;
    head.a_t_ = 0;
    head.a_c_ = 0;
    head.dx_b_ = 1;
    head.dy_b_ = 1;
    head.dx_ = the_geometry.get_voxel_dx();
    head.dy_ = the_geometry.get_voxel_dy();
    head.dz_ = the_geometry.get_voxel_dz();
    head.nx_ = the_geometry.get_voxel_nx();
    head.ny_ = the_geometry.get_voxel_ny();
    head.nz_ = the_geometry.get_voxel_nz();
    head.npb_ = nVoxels_included;
    head.scalefactor_ = 1.0;

    // Open outfile for writing (and write header)
    DijFileParser out_file(dij_file_name, head);

    // Scratch bixel
    DijFileParser::bixel bixel;

    // Write out each bixel
    for(int iBixel = 0; iBixel < voxels_to_include.size(); iBixel++) {
        // Skip bixel if appropriate
        if(!voxels_to_include[iBixel]) {
           continue;
        }

        // Prepare bixel
        bixel.energy_ = 1;
        bixel.spot_x_ = iBixel;
        bixel.spot_y_ = 1;
        bixel.voxelNo_.resize(1);
        bixel.voxelNo_[0] = iBixel;
        bixel.value_.resize(1);
        bixel.value_[0] = 1;
        bixel.nVox_ = 1;

        // Write bixel
        out_file.write_next_bixel(bixel);
    }

    // Completed successfully, so report success
    cout << "Read " << dif_file_name  << "\n";

    cout << "Wrote " << dij_file_name << " with "
        << head.npb_ << " beamlets and "
        << head.npb_ << " entries" << "\n";
}
  
