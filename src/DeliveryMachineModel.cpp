/**
 * @file DeliveryMachineModel.cpp
 * Code for DeliveryMachineModel class
 *
 * $Id: DeliveryMachineModel.cpp$
 */


#include <cmath>
#include <sstream>
#include <limits>
#include <cassert>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <functional>

#include "DeliveryMachineModel.hpp"
#include "StringFunctions.hpp"

using std::sort;
using std::unique;

#define MPF_FILE_VERSION "0.1"

/**
 * Constructor
 */
DeliveryMachineModel::DeliveryMachineModel( const string& mpf_file_name ) {
    // Create variables for attributes and values to be read in.
    vector< string > required_attributes;
    vector< string > optional_attributes;
    map< string, string > default_values;
    vector< string > multi_attributes;
    map< string, vector< string > > required_sub_attributes;
    map< string, vector< string > > optional_sub_attributes;
    map< string, map< string, string > > default_sub_values;

    // Get a list of attributes to extract from the file.
    populate_Attributes( required_attributes, optional_attributes, default_values, multi_attributes,
            required_sub_attributes, optional_sub_attributes, default_sub_values );

    // Read the mpf file
	txtFileParser_.reset( new TxtFileParser( mpf_file_name, required_attributes, optional_attributes, default_values,
            multi_attributes, required_sub_attributes, optional_sub_attributes, default_sub_values, MPF_FILE_VERSION ) );

    // Extract object data from read in attributes.
    extractDataFromParser();
    extractControlPointData();
}


/**
 * Destructor
 */
DeliveryMachineModel::~DeliveryMachineModel() {
    for ( vector< Attenuator* >::iterator atnIter = mlc_.begin(); atnIter<mlc_.end(); ++atnIter )
        delete( *atnIter );
    mlc_.clear();

    for ( vector< Attenuator* >::iterator atnIter = jaws_.begin(); atnIter<jaws_.end(); ++atnIter )
        delete( *atnIter );
    jaws_.clear();

    delete beamEnergy_;
    delete doseRate_;
    delete gantry_;
    delete collimator_;
    delete couch_;
}


/**
 * Copy Constructor
 */
DeliveryMachineModel::DeliveryMachineModel( DeliveryMachineModel & dmm ) {

    isXJaws_ = dmm.isXJaws_;
    isYJaws_ = dmm.isYJaws_;
    nXJaws_ = dmm.nXJaws_;
    nYJaws_ = dmm.nYJaws_;
    isXMlc_ = dmm.isXMlc_;
    isYMlc_ = dmm.isYMlc_;
    nMlcBanks_ = dmm.nMlcBanks_;
    nXLeaves_ = dmm.nXLeaves_;
    nYLeaves_ = dmm.nYLeaves_;
    cpMaxNumIsLimited_ = dmm.cpMaxNumIsLimited_;
    cpMaxNum_ = dmm.cpMaxNum_;
    cpMinSpacingIsLimited_ = dmm.cpMinSpacingIsLimited_;
    cpMinSpacing_ = dmm.cpMinSpacing_;
    cpTimeResIsLimited_ = dmm.cpTimeResIsLimited_;
    cpTimeRes_ = dmm.cpTimeRes_;
    cpMaxTimeIsLimited_ = dmm.cpMaxTimeIsLimited_;
    cpMaxTime_ = dmm.cpMaxTime_;
    minXAx_ = dmm.minXAx_;
    maxXAx_ = dmm.maxXAx_;
    minYAx_ = dmm.minYAx_;
    maxYAx_ = dmm.maxYAx_;
    machinename_ = dmm.machinename_;

    beamEnergy_ = new BeamEnergy(*dmm.beamEnergy_);
    doseRate_ = new DoseRate(*dmm.doseRate_);
    gantry_ = new GantryAngle(*dmm.gantry_);
    collimator_ = new CollimatorAngle(*dmm.collimator_);
    couch_ = new CouchAngle(*dmm.couch_);

    jaws_.reserve(dmm.jaws_.size());
    vector< Attenuator* >::iterator dmmIter;
    for ( dmmIter = dmm.jaws_.begin(); dmmIter < dmm.jaws_.end(); ++dmmIter )
        jaws_.push_back( new Attenuator(**dmmIter) );

    mlc_.reserve(dmm.mlc_.size());
    for ( dmmIter = dmm.mlc_.begin(); dmmIter < dmm.mlc_.end(); ++dmmIter )
        mlc_.push_back( new Attenuator(**dmmIter) );
}


/**
 * Populate the lists of attributes and default values
 */
void DeliveryMachineModel::populate_Attributes( vector<string> & required_attributes,
        vector<string> & optional_attributes,   map<string,string> & default_values, vector<string> & multi_attributes,
        map<string,vector<string> > & required_sub_attributes,   map<string,vector<string> > & optional_sub_attributes,
        map<string, map<string,string> > & default_sub_values ) const {

    // ---- Required attributes ---- //
    required_attributes.push_back( "file_version" );
    required_attributes.push_back( "MachineName" );

    // ---- Currently no optional base attributes ---- //

    // ---- Multi-Attributes ---- //
    multi_attributes.push_back( "ControlPoint" );
    multi_attributes.push_back( "BeamEnergy" );
    multi_attributes.push_back( "BeamEnergyValue" );
    multi_attributes.push_back( "DoseRate" );
    multi_attributes.push_back( "Gantry" );
    multi_attributes.push_back( "Collimator" );
    multi_attributes.push_back( "CouchAngle" );
    multi_attributes.push_back( "Attenuator" );

    // ---- Control point ---- //
    optional_sub_attributes[ "ControlPoint" ].push_back( "MaxNumber" );
    default_sub_values[ "ControlPoint" ][ "MaxNumber" ] = "NA";
    optional_sub_attributes[ "ControlPoint" ].push_back( "MinSpacing" );
    default_sub_values[ "ControlPoint" ][ "MinSpacing" ] = "NA";
    optional_sub_attributes[ "ControlPoint" ].push_back( "TimeStepSize" );
    default_sub_values[ "ControlPoint" ][ "TimeStepSize" ] = "NA";
    optional_sub_attributes[ "ControlPoint" ].push_back( "MaxTime" );
    default_sub_values[ "ControlPoint" ][ "MaxTime" ] = "NA";
    optional_sub_attributes[ "ControlPoint" ].push_back( "TimeUnits" );
    default_sub_values[ "ControlPoint" ][ "TimeUnits" ] = "MU";

    // ---- Beam Model ---- //
    populateMotionAttributes( "BeamEnergy", required_sub_attributes, optional_sub_attributes, default_sub_values );
    optional_sub_attributes[ "BeamEnergy" ].push_back( "DefaultValue" );
    default_sub_values[ "BeamEnergy" ][ "DefaultValue" ] = "";
    optional_sub_attributes[ "BeamEnergy" ].push_back( "ValueUnits" );
    default_sub_values[ "BeamEnergy" ][ "ValueUnits" ] = "";
    optional_sub_attributes[ "BeamEnergy" ].push_back( "NumberOfValidValues" );
    default_sub_values[ "BeamEnergy" ][ "NumberOfValidValues" ] = "0";

    required_sub_attributes[ "BeamEnergyValue" ].push_back( "RadiationLabel" );
    required_sub_attributes[ "BeamEnergyValue" ].push_back( "DijLabel" );

    // ---- Dose Rate ---- //
    populateConstraintAttributes( "DoseRate", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateMotionAttributes( "DoseRate", required_sub_attributes, optional_sub_attributes, default_sub_values );
    optional_sub_attributes[ "DoseRate" ].push_back( "DefaultValue" );
    default_sub_values[ "DoseRate" ][ "DefaultValue" ] = "0";
    optional_sub_attributes[ "DoseRate" ].push_back( "ValueUnits" );
    default_sub_values[ "DoseRate" ][ "ValueUnits" ] = "MU/Minute";

    // ---- Gantry ---- //
    populateConstraintAttributes( "Gantry", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateMotionAttributes( "Gantry", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateModelTransformAttributes( "Gantry", required_sub_attributes, optional_sub_attributes, default_sub_values );
    default_sub_values[ "Gantry" ][ "ValueUnits" ] = "Degrees";

    // ---- Collimator ---- //
    populateConstraintAttributes( "Collimator", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateMotionAttributes( "Collimator", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateModelTransformAttributes("Collimator", required_sub_attributes,optional_sub_attributes,default_sub_values);
    default_sub_values[ "Collimator" ][ "ValueUnits" ] = "Degrees";

    // ---- Couch ---- //
    populateConstraintAttributes( "CouchAngle", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateMotionAttributes( "CouchAngle", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateModelTransformAttributes( "CouchAngle", required_sub_attributes, optional_sub_attributes, default_sub_values );
    default_sub_values[ "CouchAngle" ][ "ValueUnits" ] = "Degrees";

    // ---- Attenuator ---- //
    populateConstraintAttributes( "Attenuator", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateMotionAttributes( "Attenuator", required_sub_attributes, optional_sub_attributes, default_sub_values );
    populateModelTransformAttributes("Attenuator", required_sub_attributes,optional_sub_attributes,default_sub_values);
    default_sub_values[ "Attenuator" ][ "ValueUnits" ] = "mm";

    required_sub_attributes[ "Attenuator" ].push_back( "Name" );
    required_sub_attributes[ "Attenuator" ].push_back( "IsX" );
    optional_sub_attributes[ "Attenuator" ].push_back( "IsY" ); // If not included is assumed to be !(IsX)
    optional_sub_attributes[ "Attenuator" ].push_back( "Angle" ); // If not included is assumed to be 0 or 90.
    default_sub_values[ "Attenuator" ][ "Angle" ] = "NA";

    optional_sub_attributes[ "Attenuator" ].push_back( "WithdrawDirectionPositive" );
    default_sub_values[ "Attenuator" ][ "WithdrawDirectionPositive" ] = "0";
    optional_sub_attributes[ "Attenuator" ].push_back( "IsFullWidth" );
    default_sub_values[ "Attenuator" ][ "IsFullWidth" ] = "1";
    optional_sub_attributes[ "Attenuator" ].push_back( "Width" );
    default_sub_values[ "Attenuator" ][ "Width" ] = "NA";
    optional_sub_attributes[ "Attenuator" ].push_back( "Position" );
    default_sub_values[ "Attenuator" ][ "Position" ] = "0";
    optional_sub_attributes[ "Attenuator" ].push_back( "IsFullLength" );
    default_sub_values[ "Attenuator" ][ "IsFulllength" ] = "1";
    optional_sub_attributes[ "Attenuator" ].push_back( "Length" );
    default_sub_values[ "Attenuator" ][ "Length" ] = "NA";

    optional_sub_attributes[ "Attenuator" ].push_back( "Transmission" );
    default_sub_values[ "Attenuator" ][ "Transmission" ] = "0.0";
    optional_sub_attributes[ "Attenuator" ].push_back( "TongueGrooveWidth" );
    default_sub_values[ "Attenuator" ][ "TongueGrooveWidth" ] = "0.0";
    optional_sub_attributes[ "Attenuator" ].push_back( "TongueGrooveTransmission" );
    default_sub_values[ "Attenuator" ][ "TongueGrooveTransmission" ] = "0.0";
    optional_sub_attributes[ "Attenuator" ].push_back( "PenumbraKernel" );

    optional_sub_attributes[ "Attenuator" ].push_back( "MinimumGapOpposing" );
    default_sub_values[ "Attenuator" ][ "MinimumGapOpposing" ] = "0";
    optional_sub_attributes[ "Attenuator" ].push_back( "OppositeAboveCanInterDigitate" );
    default_sub_values[ "Attenuator" ][ "OppositeAboveCanInterDigitate" ] = "1";
    optional_sub_attributes[ "Attenuator" ].push_back( "OppositeAboveMinGap" );
    default_sub_values[ "Attenuator" ][ "OppositeAboveMinGap" ] = "NA";
    optional_sub_attributes[ "Attenuator" ].push_back( "OppositeBelowCanInterDigitate" );
    default_sub_values[ "Attenuator" ][ "OppositeBelowCanInterDigitate" ] = "1";
    optional_sub_attributes[ "Attenuator" ].push_back( "OppositeBelowMinGap" );
    default_sub_values[ "Attenuator" ][ "OppositeBelowMinGap" ] = "NA";
}


/**
 * Populate the lists of sub attributes which relate to absolute constraints on the parameter value
 */
void DeliveryMachineModel::populateConstraintAttributes( const string & multiAttributeName,
        map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
        map<string, map<string,string> > & default_sub_values ) const {

    required_sub_attributes[ multiAttributeName ].push_back( "MinValue" );
    required_sub_attributes[ multiAttributeName ].push_back( "MaxValue" );
    required_sub_attributes[ multiAttributeName ].push_back( "ValueIsContinuouslyVariable" );
    optional_sub_attributes[ multiAttributeName ].push_back( "ValueStepSize" );
    default_sub_values[ multiAttributeName ][ "ValueStepSize" ] = "0.0";
    optional_sub_attributes[ multiAttributeName ].push_back( "NumberOfValidValues" );
    default_sub_values[ multiAttributeName ][ "NumberOfValidValues" ] = "0";
    optional_sub_attributes[ multiAttributeName ].push_back( "ValidValues" );
    default_sub_values[ multiAttributeName ][ "ValidValues" ] = "";
    optional_sub_attributes[ multiAttributeName ].push_back( "ValueUnits" );
}


/**
 * Populate the lists of sub attributes which relate to parameter motion or relative changes.
 *
 * All motion parameters are set as optional and if missing it is assumed the parameter cannot change
 */
void DeliveryMachineModel::populateMotionAttributes( const string & multiAttributeName,
        map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
        map<string, map<string,string> > & default_sub_values ) const {

    optional_sub_attributes[ multiAttributeName ].push_back( "ValueCanChangeDuringDelivery" );
    default_sub_values[ multiAttributeName ][ "ValueCanChangeDuringDelivery" ] = "0";
    optional_sub_attributes[ multiAttributeName ].push_back( "DirectionCanChangeDuringDelivery" );
    default_sub_values[ multiAttributeName ][ "DirectionCanChangeDuringDelivery" ] = "1";
    optional_sub_attributes[ multiAttributeName ].push_back( "ChangeRequiresInterrupt" );
    default_sub_values[ multiAttributeName ][ "ChangeRequiresInterrupt" ] = "0";
    optional_sub_attributes[ multiAttributeName ].push_back( "InterruptLatency" );
    default_sub_values[ multiAttributeName ][ "InterruptLatency" ] = "0.0";
    optional_sub_attributes[ multiAttributeName ].push_back( "InterruptLatencyUnits" );
    default_sub_values[ multiAttributeName ][ "InterruptLatencyUnits" ] = "s";
    optional_sub_attributes[ multiAttributeName ].push_back( "SpeedUnits" );
    default_sub_values[ multiAttributeName ][ "SpeedUnits" ] = "/s";
    optional_sub_attributes[ multiAttributeName ].push_back( "AccelerationUnits" );
    default_sub_values[ multiAttributeName ][ "AccelerationUnits" ] = "/s^2";
    optional_sub_attributes[ multiAttributeName ].push_back( "MinSpeed" );
    default_sub_values[ multiAttributeName ][ "MinSpeed" ] = "0.0f";
    optional_sub_attributes[ multiAttributeName ].push_back( "MaxSpeed" );
    default_sub_values[ multiAttributeName ][ "MaxSpeed" ] = "Inf";
    optional_sub_attributes[ multiAttributeName ].push_back( "MaxAcceleration" );
    default_sub_values[ multiAttributeName ][ "MaxSpeed" ] = "Inf";
    optional_sub_attributes[ multiAttributeName ].push_back( "SpeedIsContinuouslyVariable" );
    default_sub_values[ multiAttributeName ][ "SpeedIsContinuouslyVariable" ] = "1";
    optional_sub_attributes[ multiAttributeName ].push_back( "SpeedStepSize" );
    default_sub_values[ multiAttributeName ][ "SpeedStepSize" ] = "1.0";
    optional_sub_attributes[ multiAttributeName ].push_back( "NumberOfValidSpeeds" );
    default_sub_values[ multiAttributeName ][ "NumberOfValidSpeeds" ] = "0";
    optional_sub_attributes[ multiAttributeName ].push_back( "ValidSpeeds" );
    default_sub_values[ multiAttributeName ][ "ValidSpeeds" ] = "";
}


/**
 * Populate the lists of sub attributes relating to transformations between actual and model coordinate systems
 */
void DeliveryMachineModel::populateModelTransformAttributes( const string & multiAttributeName,
        map<string,vector<string> > & required_sub_attributes,  map<string,vector<string> > & optional_sub_attributes,
        map<string, map<string,string> > & default_sub_values ) const {
    required_sub_attributes[ multiAttributeName ].push_back( "ValueOffset" );
    required_sub_attributes[ multiAttributeName ].push_back( "IncreasingDirectionAligned" );
}


/**
 * Extract object data from read in attributes.
 */
void DeliveryMachineModel::extractDataFromParser() {
    // Read in the data
    machinename_ = txtFileParser_->get_attribute( "MachineName" );

    cout << "Reading : energy, " << flush;
    beamEnergy_ = new BeamEnergy(txtFileParser_);
    cout << "doserate, " << flush;
    doseRate_ = new DoseRate(txtFileParser_);
    cout << "gantry, " << flush;
    gantry_ = new GantryAngle(txtFileParser_);
    cout << "collimator, " << flush;
    collimator_ = new CollimatorAngle(txtFileParser_);
    cout << "couch, " << flush;
    couch_ = new CouchAngle(txtFileParser_);
    cout << "attenuators, " << flush;

    // Populate list of jaws and MLCs
    unsigned int nAtn = txtFileParser_->get_multi_attribute_nThings( "Attenuator" );
    vector< Attenuator* > tmpAtnList;
    tmpAtnList.reserve(nAtn);
    for ( unsigned int atnNo=0; atnNo<nAtn; atnNo++ ) {
        tmpAtnList.push_back( new Attenuator(atnNo, txtFileParser_) );
    }
    sortAttenuators( tmpAtnList );

    isXJaws_ = false;
    isYJaws_ = false;
    isXMlc_ = false;
    isYMlc_ = false;
    nXJaws_ = 0;
    nYJaws_ = 0;
    nXLeaves_ = 0;
    nYLeaves_ = 0;
    for ( vector< Attenuator* >::iterator atnNo = tmpAtnList.begin(); atnNo<tmpAtnList.end(); ++atnNo ) {
        if ( (*atnNo)->getIsFullWidth() ) {
            jaws_.push_back( new Attenuator( **atnNo ) );
            if ( (*atnNo)->getIsX() ) {
                isXJaws_=true;
                ++nXJaws_;
            } else {
                isYJaws_=true;
                ++nYJaws_;
            }
        } else {
            mlc_.push_back( new Attenuator( **atnNo ) );
            if ( (*atnNo)->getIsX() ) {
                isXMlc_=true;
                ++nXLeaves_;
            } else {
                isYMlc_=true;
                ++nYLeaves_;
            }
        }
    }

    nMlcBanks_ = 0;
    if ( isXMlc_ ) ++nMlcBanks_;
    if ( isYMlc_ ) ++nMlcBanks_;

    for ( vector< Attenuator* >::iterator atnNo = tmpAtnList.begin(); atnNo<tmpAtnList.end(); ++atnNo )
        delete( *atnNo );
    tmpAtnList.clear();

    cout << "jaws, " << flush;
    if ( ! checkJaws() )
        txtFileParser_->report_error( "Failed while reading DeliveryMachineModel" );
    cout << "mlcs, " << flush;
    if ( ! checkMLCs() )
        txtFileParser_->report_error( "Failed while reading DeliveryMachineModel" );
    cout << "done" << endl;
}


/**
 * Read in the control point restrictions from the text file parser.
 */
void DeliveryMachineModel::extractControlPointData() {

    // Read in units string
    double unitsConversion;
    if ( txtFileParser_->is_set_multi_attribute( "ControlPoint", "TimeUnits", 0 ) ) {
        if ( startsWith( txtFileParser_->get_multi_attribute( "ControlPoint", "TimeUnits", 0 ), "mu" ) ) { //Match MU
            unitsConversion = 1.0;
        } else {
			cerr << "DeliveryMachineModel(): Unrecognized units string in ControlPoint TimeUnits" << endl;
            txtFileParser_->report_error("Failed while reading DoseRate");
        }
    }

    // Read in maximum number of control points
    if ( txtFileParser_->is_set_multi_attribute( "ControlPoint", "MaxNumber", 0 ) ) {
        if ( startsWith( txtFileParser_->get_multi_attribute( "ControlPoint", "MaxNumber", 0 ), "na" ) ) { //Match NA
            cpMaxNumIsLimited_ = false;
        } else {
            cpMaxNumIsLimited_ = true;
            cpMaxNum_ = txtFileParser_->get_num_multi_attribute<size_t>( "ControlPoint", "MaxNumber", 0 );
        }
    } else {
        cpMaxNumIsLimited_ = false;
    }

    // Read in minimum control point spacing
    if ( txtFileParser_->is_set_multi_attribute( "ControlPoint", "MinSpacing", 0 ) ) {
        if ( startsWith( txtFileParser_->get_multi_attribute( "ControlPoint", "MinSpacing", 0 ), "na" ) ) { //Match NA
            cpMinSpacingIsLimited_ = false;
        } else {
            cpMinSpacingIsLimited_ = true;
            cpMinSpacing_ = txtFileParser_->get_num_multi_attribute<float>( "ControlPoint", "MinSpacing", 0 ) * unitsConversion;
        }
    } else {
        cpMinSpacingIsLimited_ = false;
    }

    // Read maximum control point time
    if ( txtFileParser_->is_set_multi_attribute( "ControlPoint", "MaxTime", 0 ) ) {
        if ( startsWith( txtFileParser_->get_multi_attribute( "ControlPoint", "MaxTime", 0 ), "na" ) ) { //Match NA
            cpMaxTimeIsLimited_ = false;
        } else {
            cpMaxTimeIsLimited_ = true;
            cpMaxTime_ = txtFileParser_->get_num_multi_attribute<float>( "ControlPoint", "MaxTime", 0 ) * unitsConversion;
        }
    } else {
        cpMaxTimeIsLimited_ = false;
    }
    cpMaxTime_ = txtFileParser_->get_num_multi_attribute<float>( "ControlPoint", "MaxTime", 0 );

    // Read time step size
    if ( txtFileParser_->is_set_multi_attribute( "ControlPoint", "TimeStepSize", 0 ) ) {
        if ( startsWith( txtFileParser_->get_multi_attribute( "ControlPoint", "TimeStepSize", 0 ), "na" ) ){//Match NA
            cpTimeResIsLimited_ = false;
        } else {
            cpTimeResIsLimited_ = true;
            cpTimeRes_ = txtFileParser_->get_num_multi_attribute<float>( "ControlPoint", "TimeStepSize", 0 ) * unitsConversion;
        }
    } else {
        cpTimeResIsLimited_ = false;
    }
}


/**
 * Check the jaw data to ensure consistancy.
 */
bool DeliveryMachineModel::checkJaws() {

    // Check that there is an even number of jaws
    if ( ( nXJaws_ % 2 != 0 ) && ( nYJaws_ % 2 != 0 ) ) {
		cerr << "DeliveryMachineModel(): Expected even number of jaws." << endl;
        return false;
    }

    minXAx_ = std::numeric_limits<float>::infinity();
    maxXAx_ = -std::numeric_limits<float>::infinity();
    minYAx_ = std::numeric_limits<float>::infinity();
    maxYAx_ = -std::numeric_limits<float>::infinity();

    nXJaws_ = 0;
    nYJaws_ = 0;
    vector<int> jawList;
    for ( vector<Attenuator*>::iterator jw = jaws_.begin(); jw<jaws_.end(); jw++ ) {
        if ( (*jw)->getIsX() ) {
            ++nXJaws_;
            minXAx_ = (*jw)->getMinValue() < minXAx_ ? (*jw)->getMinValue() : minXAx_;
            maxXAx_ = (*jw)->getMaxValue() > maxXAx_ ? (*jw)->getMaxValue() : maxXAx_;
        } else {
            ++nYJaws_;
            minYAx_ = (*jw)->getMinValue() < minYAx_ ? (*jw)->getMinValue() : minYAx_;
            maxYAx_ = (*jw)->getMaxValue() > maxYAx_ ? (*jw)->getMaxValue() : maxYAx_;
        }
        jawList.push_back( jw-jaws_.begin() );
    }
    isXJaws_ = nXJaws_ > 0;
    isYJaws_ = nYJaws_ > 0;

    while ( jawList.size() > 0 ) {
        // Check each jaw has a match
        bool matchFound = false;
        for ( unsigned int jw2=1; jw2<jawList.size(); jw2++ ) {
            if ( (*(jaws_.begin()+jawList[0]))->isOpposing( **(jaws_.begin()+jawList[jw2]) ) ) {
                jawList.erase ( jawList.begin() + jw2 );
                matchFound = true;
                break;
            }
        }

        if ( !matchFound ) {
			cerr << "DeliveryMachineModel(): Failed to find a match for jaw "
                << (*(jaws_.begin()+jawList[0]))->getName() << "." << endl;
            return false;
        }
        jawList.erase ( jawList.begin() );
    }

    return true;
}


/**
 * Check the MLC data to ensure consistancy.
 */
bool DeliveryMachineModel::checkMLCs( ) {

    // Check there are an even number of leaves
    if ( mlc_.size() % 2 != 0 ) {
		cerr << "DeliveryMachineModel(): Expected even number of MLCs." << endl;
        return false;
    }

    // Count the leaves and get global min and max positions
    nXLeaves_ = 0;
    nYLeaves_ = 0;
    for ( vector<Attenuator*>::iterator lf = mlc_.begin(); lf < mlc_.end(); lf++ ) {
        if ( (*lf)->getIsX() ) {
            ++nXLeaves_;
        } else {
            ++nYLeaves_;
        }
    }
    isXMlc_ = nXLeaves_ > 0;
    isYMlc_ = nYLeaves_ > 0;
    nMlcBanks_ = ( ( isXMlc_ ? 1 : 0 ) + ( isYMlc_ ? 1 : 0 ) ) * 2;

    vector< Attenuator* >::iterator lf_XBnk1_Start = mlc_.begin();
    vector< Attenuator* >::iterator lf_XBnk2_Start = lf_XBnk1_Start + nXLeaves_ / 2;
    vector< Attenuator* >::iterator lf_YBnk1_Start = lf_XBnk2_Start + nXLeaves_ / 2;
    vector< Attenuator* >::iterator lf_YBnk2_Start = lf_YBnk1_Start + nYLeaves_ / 2;

    if ( isXMlc_ ) {
        // Check each leaf has a matching opposing leaf
        for ( vector<Attenuator*>::iterator lf=lf_XBnk1_Start; lf<lf_XBnk2_Start; ++lf ) {
            if ( ! (*lf)->isOpposing( **( lf + nXLeaves_ / 2 ) ) ) {
				cerr << "DeliveryMachineModel(): Failed to find a match for leaf." <<
						lf - lf_XBnk1_Start << endl;
                return false;
            }
        }

        // Check we can make complete banks without gaps or overlaps
        // Assumes leaves are properly sorted ( i.e. sortAttenuators has been called )
        float tolerance = 0.001;
        for ( vector<Attenuator*>::iterator lf = (lf_XBnk1_Start+1); lf<lf_XBnk2_Start; lf++ ) {
            if ( std::abs( (*lf)->getPosition() - ( (*lf)->getWidth() / 2 ) - (*(lf-1))->getPosition()
                    - (*(lf-1))->getWidth() / 2) > tolerance ) {
				cerr << "DeliveryMachineModel(): Leaves do not abut properly either gap or overlap present at : "
						<< lf - lf_XBnk1_Start - 1 << " - " << lf - lf_XBnk1_Start << "." << endl;
                return false;
            }
        }
    }

    if ( isYMlc_ ) {
        // Check each leaf has a matching opposing leaf
        for ( vector<Attenuator*>::iterator lf=lf_YBnk1_Start; lf<lf_YBnk2_Start; lf++ ) {
            if ( ! (*lf)->isOpposing( **( lf + nYLeaves_ / 2 ) ) ) {
				cerr << "DeliveryMachineModel(): Failed to find a match for leaf " <<
						lf - lf_YBnk1_Start << endl;
                return false;
            }
        }

        // Check we can make complete banks without gaps or overlaps
        // Assumes leaves are properly sorted ( i.e. sortAttenuators has been called )
        float tolerance = 0.001;
        for ( vector<Attenuator*>::iterator lf=(lf_YBnk1_Start+1); lf<lf_YBnk2_Start; lf++ ) {
            if ( std::abs( (*lf)->getPosition() - ( (*lf)->getWidth() / 2 ) - (*(lf-1))->getPosition()
                    - (*(lf-1))->getWidth() / 2 ) > tolerance ) {
				cerr << "DeliveryMachineModel(): Leaves do not abut properly either gap or overlap present at : "
						<< lf - lf_YBnk1_Start - 1 << " - " << lf - lf_YBnk1_Start << "." << endl;
                return false;
            }
        }
    }

    return true;
}


/**
 * Sort the attenuator list.
 */
void DeliveryMachineModel::sortAttenuators( vector< Attenuator *> & atns ) {

    vector< Attenuator *>::iterator atn_it;
    vector <int> divideAtn;

    divideAtn.push_back(0);
    divideAtn.push_back(atns.size());

    // Sort jaws from MLCs
    sort( atns.begin(), atns.end(), sortByIsJaw );

    // Find divide between different values
    for ( atn_it = atns.begin(); atn_it < atns.end(); atn_it++ )
        if ( ! (*atn_it)->getIsFullWidth() ) break;
    divideAtn.insert( divideAtn.begin()+1, atn_it-atns.begin() );
    sort( divideAtn.begin(), divideAtn.end() );

    // Sort attenuator list portions by angle
    for ( size_t atn_ListInd=0; atn_ListInd < (divideAtn.size()-1); atn_ListInd++ )
        std::sort( atns.begin()+divideAtn[atn_ListInd], atns.begin()+divideAtn[atn_ListInd+1], sortByAngle );

    // Find divide between different angular values
    for ( size_t atn_ListInd=0; atn_ListInd < (divideAtn.size()-1); atn_ListInd++ ) {
        vector< Attenuator *>::iterator fromAtn = atns.begin() + divideAtn[atn_ListInd];
        vector< Attenuator *>::iterator toAtn = atns.begin() + divideAtn[atn_ListInd+1];
        vector< float > angles = getListAngle( fromAtn, toAtn );
        unique( angles.begin(), angles.end() );
        size_t angleIndex = 0;
        for ( atn_it = fromAtn; atn_it < toAtn; atn_it++ ) {
            if ( (*atn_it)->getAngle() != angles[ angleIndex ] ) {
                divideAtn.insert( divideAtn.begin()+atn_ListInd+1, atn_it-atns.begin() );
                ++angleIndex;
                ++atn_ListInd;
                continue;
            }
            if ( angleIndex == angles.size() ) break;
        }
    }
    sort( divideAtn.begin(), divideAtn.end() );

    // Sort attenuator list portions by withdrawal direction
    for ( size_t atn_ListInd=0; atn_ListInd < (divideAtn.size()-1); atn_ListInd++ ) {
        std::sort( atns.begin()+divideAtn[ atn_ListInd ], atns.begin()+divideAtn[ atn_ListInd + 1 ], sortByDirection );
    }

    // Find divide between different withdrawal directions
    for ( size_t atn_ListInd=0; atn_ListInd < (divideAtn.size()-1); atn_ListInd++ ) {
        vector< Attenuator *>::iterator fromAtn = atns.begin() + divideAtn[atn_ListInd];
        vector< Attenuator *>::iterator toAtn = atns.begin() + divideAtn[atn_ListInd+1];
        for ( atn_it = fromAtn; atn_it < toAtn; atn_it++ ) {
            if ( (*atn_it)->getIsWithdrawDirectionPositive() ) {
                divideAtn.insert( divideAtn.begin()+atn_ListInd+1, atn_it-atns.begin() );
                ++atn_ListInd;
                break;
            }
        }
    }
    sort( divideAtn.begin(), divideAtn.end() );

    // Sort attenuator list portions by position
    for ( size_t atn_ListInd=0; atn_ListInd < (divideAtn.size()-1); atn_ListInd++ ) {
        std::sort( atns.begin()+divideAtn[ atn_ListInd ], atns.begin()+divideAtn[ atn_ListInd + 1 ], sortByPosition );
    }

    /*
    cout << endl << "Print out final sorted set of attenuators with relevant properties :" << endl;
    for ( size_t atn_ListInd=0; atn_ListInd < (divideAtn.size()-1); atn_ListInd++ ) {
        for ( atn_it = atns.begin()+divideAtn[atn_ListInd]; atn_it < atns.begin()+divideAtn[atn_ListInd+1]; atn_it++ ) {
            cout << " " << atn_ListInd << " : " << (*atn_it)->getName() << " : " << (*atn_it)->getAngle() << " : "
            << (*atn_it)->getIsWithdrawDirectionPositive() << " : " << (*atn_it)->getPosition() << endl;
        }
    }
    cout << endl;*/
}


/// Get list of isJaw status for list of attenuators
vector< bool > DeliveryMachineModel::getListIsJaw( const vector< Attenuator* >::const_iterator & fromAtn,
            const vector< Attenuator* >::const_iterator & toAtn ) const {
    vector< bool > tmpIsJaw;
    tmpIsJaw.reserve( distance(fromAtn, toAtn) );
    for ( vector< Attenuator* >::const_iterator at = fromAtn; at < toAtn; ++at ) {
        tmpIsJaw.push_back( (*at)->getIsFullWidth() );
    }
    return tmpIsJaw;
}

/// Get list of angle status for list of attenuators
vector< float > DeliveryMachineModel::getListAngle( const vector< Attenuator *>::const_iterator & fromAtn,
            const vector< Attenuator *>::const_iterator & toAtn ) const {
    vector< float > tmpAngles;
    tmpAngles.reserve( distance(fromAtn, toAtn) );
    for ( vector< Attenuator *>::const_iterator at = fromAtn; at < toAtn; ++at ) {
        tmpAngles.push_back( (*at)->getAngle() );
    }
    return tmpAngles;
}

/// Get list of getIsWithdrawDirectionPositive status for list of attenuators
vector< bool > DeliveryMachineModel::getListDirection( const vector< Attenuator *>::const_iterator & fromAtn,
            const vector< Attenuator *>::const_iterator & toAtn ) const {
    vector< bool > tmpDirection;
    tmpDirection.reserve( distance(fromAtn, toAtn) );
    for ( vector< Attenuator *>::const_iterator at = fromAtn; at < toAtn; ++at ) {
        tmpDirection.push_back( (*at)->getIsWithdrawDirectionPositive() );
    }
    return tmpDirection;
}

/// Get list of Position values for list of attenuators
vector< float > DeliveryMachineModel::getListPosition( const vector< Attenuator *>::const_iterator & fromAtn,
            const vector< Attenuator *>::const_iterator & toAtn ) const {
    vector< float > tmpPosition;
    tmpPosition.reserve( distance(fromAtn, toAtn) );
    for ( vector< Attenuator *>::const_iterator at = fromAtn; at < toAtn; ++at ) {
        tmpPosition.push_back( (*at)->getPosition() );
    }
    return tmpPosition;
}


// ----------------------------------------------------------------------- //
// -----------------------  BeamEnergy Class ----------------------------- //
// ----------------------------------------------------------------------- //

/**
 * BeamEnergy Constructor.
 *
 * Read the data from the processed text file and extract the relevant data to populate the fields of the BeamEnergy
 * object, also do some basic checking of validity of input data.
 */
BeamEnergy::BeamEnergy( const std::auto_ptr<const TxtFileParser> & txtFileParser )
        throw (DeliveryMachineModel_exception) {

	if ( txtFileParser->is_set_multi_attribute( "BeamEnergy", "NumberOfValidValues", 0 ) ) {
		numValues_ = txtFileParser->get_num_multi_attribute<size_t>(
				"BeamEnergy", "NumberOfValidValues", 0 );
		if ( numValues_ != txtFileParser->get_multi_attribute_nThings( "BeamEnergyValue" ) ) {
			cerr << "BeamEnergy(): "
					<< "Quoted Number of values and elements in value list do not agree" << endl;
			txtFileParser->report_error("Failed while reading BeamEnergy");
		}
	} else {
		numValues_ = txtFileParser->get_multi_attribute_nThings( "BeamEnergyValue" );
	}

	beamEnergyValue_.reserve(numValues_);
	for ( unsigned int valIndex = 0; valIndex<numValues_; valIndex++ ) {
		string radLbl, dijLbl;
		if ( ! txtFileParser->is_set_multi_attribute( "BeamEnergyValue", "RadiationLabel", valIndex ) ) {
			cerr << "BeamEnergy(): "
					<< "Beam energy value entry has no RadiationLabel field" << endl;
			txtFileParser->report_error("Failed while reading BeamEnergy");
		} else {
			radLbl = txtFileParser->get_multi_attribute("BeamEnergyValue","RadiationLabel",valIndex);
			dijLbl = txtFileParser->get_multi_attribute("BeamEnergyValue","DijLabel",valIndex);

			beamEnergyValue_.push_back( BeamEnergyValue( radLbl, dijLbl ) );
		}
    }

	if ( txtFileParser->is_set_multi_attribute( "BeamEnergy", "DefaultValue", 0 ) ) {
	    try {
            setDefaultValue( txtFileParser->get_multi_attribute( "BeamEnergy", "DefaultValue", 0 ) );
	    } catch (DeliveryMachineModel_exception & dmm_ex) {
            cerr << "Unable to set default beam energy" << endl;
            throw;
	    }
    } else {
        defaultIndex_ = 0;
    }

	if ( txtFileParser->is_set_multi_attribute( "BeamEnergy", "ValueCanChangeDuringDelivery", 0 ) ) {
		canChangeDuringDelivery_ = txtFileParser->get_num_multi_attribute<bool>(
				"BeamEnergy", "ValueCanChangeDuringDelivery", 0 );
    } else {
        canChangeDuringDelivery_ = false;
    }

    if ( !canChangeDuringDelivery_ ) {
        // No changes are allowed so fill parameter attributes with default values
        changeRequiresInterrupt_ = false;
        interruptLatency_ = 0.0f;
    } else {
		if ( txtFileParser->is_set_multi_attribute( "BeamEnergy", "ChangeRequiresInterrupt", 0 ) ) {
			changeRequiresInterrupt_ = txtFileParser->get_num_multi_attribute<bool>(
					"BeamEnergy", "ChangeRequiresInterrupt", 0 );
        } else {
            changeRequiresInterrupt_ = false;
        }
        if ( changeRequiresInterrupt_ ) {
            double latencyUnitConversion = 1.0;
			if ( txtFileParser->is_set_multi_attribute( "BeamEnergy", "InterruptLatencyUnits", 0 ) ) {
				string timeUnitStr = txtFileParser->get_multi_attribute( "BeamEnergy", "SpeedUnits", 0 );
				if ( startsWith( timeUnitStr, "m" ) ) { // Match minutes
                    latencyUnitConversion = 60.0;
				} else if ( startsWith( timeUnitStr, "s" ) ) { // Match seconds
                    latencyUnitConversion = 1.0;
                } else {
					cerr << "MovingParameter(): Unrecognized units string ""SpeedUnits""" << endl;
					txtFileParser->report_error("Failed while reading BeamEnergy" );
                }
            }
		} else {
            interruptLatency_ = 0.0f;
        }
    }
}

/// Destructor
BeamEnergy::~BeamEnergy() {
    /*std::vector< BeamEnergyValue* >::iterator iter;
    for( iter = beamEnergyValue_.begin(); iter != beamEnergyValue_.end(); ++iter ) {
        delete *iter;
        *iter = NULL;
    }
    beamEnergyValue_.clear();*/
};

/**
 * Set the default value for the beam energy.
 * Throws an error if the requested default value is not found in the value list.
 */
void BeamEnergy::setDefaultValue( const string & defaultValue ) {

    defaultIndex_ = numValues_ + 1;
    for ( size_t valIndex = 0; valIndex < numValues_; valIndex++ ) {
		if ( beamEnergyValue_[valIndex].compare_label(defaultValue) ) {
            defaultIndex_ = valIndex;
        }
    }

    if ( defaultIndex_ >= numValues_ ) {
		cerr << "Could not find default value " << defaultValue << " in list." << endl;
		for ( size_t valIndex = 0; valIndex < numValues_; valIndex++ )
		    cerr << beamEnergyValue_[valIndex].getRadiationlabel() << " : " << defaultValue << " : " << beamEnergyValue_[valIndex].compare_label(defaultValue) << endl;
        DeliveryMachineModel_exception dmm_ex;
		throw dmm_ex;
	}
}


/**
 * Get the beam energy value object with a certain requested radiation label.
 * Throws an error if the requested label is not found in the list.
 */
BeamEnergyValue BeamEnergy::getValue( const string & lbl ) const {
	vector< BeamEnergyValue >::const_iterator it;
	for ( it = beamEnergyValue_.begin(); it != beamEnergyValue_.end(); ++it ) {
		if ( it->compare_label(lbl) ) return *it;
	}
	cerr << "BeamEnergy.getValue(): Invalid radiation label" << endl;
	assert(0);
}


// ----------------------------------------------------------------------- //
// -----------------------  BeamEnergyValue Class ------------------------ //
// ----------------------------------------------------------------------- //

/**
 * Compare string to radiation label.
 * Use case insensitive test and return true if two labels are equal
 */
bool BeamEnergyValue::compare_label(const string & lbl) const {
    if (radiationLabel_.length() == lbl.length()) {
        if (to_lower(radiationLabel_) == to_lower(lbl)) {
            return true;
        }
    }
    return false;
}


/**
 * Compare string to Dij label.
 * Use case insensitive test and return true if two labels are equal. Also test if label is to match with any input
 * label, if this is the case then return true whatever the input label.
 */
bool BeamEnergyValue::compare_dijlabel(const string & lbl) const {
    // Check if dijLabel should match any dij file
	string anyDij = "NA";
    if ( dijLabel_.length() == anyDij.length() ) {
        if ( to_lower( dijLabel_ ) == to_lower( anyDij ) ) {
            return true;
        }
    }

    // Check if dij label matches this dij file
    if (dijLabel_.length() == lbl.length()) {
        if (to_lower(dijLabel_) == to_lower(lbl)) {
            return true;
        }
    }

    return false; // No match
};


// ----------------------------------------------------------------------- //
// -----------------------  DoseRate Class ------------------------------- //
// ----------------------------------------------------------------------- //

/**
 * DoseRate Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the DoseRate
 * object, also do some basic checking of validity of input data.
 */
DoseRate::DoseRate( const std::auto_ptr<const TxtFileParser> & txtFileParser ) throw ( DeliveryMachineModel_exception )
    : ConfinedParameter( "DoseRate",  txtFileParser ), MovingParameter( "DoseRate",  txtFileParser ) {

    // Store dose rates in MU / second
    double valueUnitConversion;
	if ( txtFileParser->is_set_multi_attribute( "DoseRate", "ValueUnits", 0 ) ) { // MU / timeUnit
		string unitStr = txtFileParser->get_multi_attribute( "DoseRate", "ValueUnits", 0 );
		string timeUnitStr = remove_whitespace( unitStr.substr( unitStr.find_last_of("/") + 1) );

		if ( startsWith( timeUnitStr, "m" ) ) { // Match minutes
            valueUnitConversion = 1.0 / 60.0;
		} else if ( startsWith( timeUnitStr, "s" ) ) { // Match seconds
            valueUnitConversion = 1.0;
        } else {
			cerr << "DoseRate(): Unrecognized units string """ << unitStr << """" << endl;
			txtFileParser->report_error("Failed while reading DoseRate");
        }
    }

    // min, max and step are all read in by ConfinedParameter() so just scale values appropriately.
    minValue_ *= valueUnitConversion;
    maxValue_ *= valueUnitConversion;
    stepSize_ *= valueUnitConversion;

    minSpeed_ *= valueUnitConversion;
    maxSpeed_ *= valueUnitConversion;
    speedStepSize_ *= valueUnitConversion;

    if ( !isContinuouslyVariable_ )
        for ( vector<float>::iterator itVV = validValues_.begin(); itVV < validValues_.end(); ++itVV )
            *itVV *= valueUnitConversion;

    defaultValue_ = roundToValidValue( txtFileParser->get_num_multi_attribute<float>("DoseRate","DefaultValue",0)
                                      * valueUnitConversion );
}


// ----------------------------------------------------------------------- //
// -----------------------  GantryAngle Class ---------------------------- //
// ----------------------------------------------------------------------- //

/**
 * GantryAngle Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the GantryAngle
 * object, also do some basic checking of validity of input data.
 */
GantryAngle::GantryAngle( const std::auto_ptr<const TxtFileParser> & txtFileParser )
		throw ( DeliveryMachineModel_exception )
		: ConfinedParameter( "Gantry",  txtFileParser ),
		MovingParameter( "Gantry",  txtFileParser ),
		ModelTranslator( "Gantry",  txtFileParser ) {

    // Store angles in degrees
    double valueUnitConversion;
	if ( txtFileParser->is_set_multi_attribute( "Gantry", "ValueUnits", 0 ) ) {
		string unitStr = txtFileParser->get_multi_attribute( "Gantry", "ValueUnits", 0 ); // MU / timeUnit

		if ( startsWith( unitStr, "r" ) ) { // Match radians
            valueUnitConversion = 180.0 / 3.141592;
		} else if ( startsWith( unitStr, "d" ) ) { // Match seconds
            valueUnitConversion = 1.0;
        } else {
			cerr << "GantryAngle(): Unrecognized units string """ << unitStr << """" << endl;
			txtFileParser->report_error("Failed while reading Gantry");
        }
    }

    // min, max and step read in by ConfinedParameter() scale values appropriately.
    minValue_ *= valueUnitConversion;
    maxValue_ *= valueUnitConversion;
    stepSize_ *= valueUnitConversion;

    minSpeed_ *= valueUnitConversion;
    maxSpeed_ *= valueUnitConversion;
    speedStepSize_ *= valueUnitConversion;

    valueOffset_ *= valueUnitConversion;
}


// ----------------------------------------------------------------------- //
// -----------------------  Collimator Class ---------------------------- //
// ----------------------------------------------------------------------- //

/**
 * CollimatorAngle Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the
 * CollimatorAngle object, also do some basic checking of validity of input data.
 */
CollimatorAngle::CollimatorAngle( const std::auto_ptr<const TxtFileParser> & txtFileParser )
		throw ( DeliveryMachineModel_exception )
		: ConfinedParameter( "Collimator",  txtFileParser ),
		MovingParameter( "Collimator",  txtFileParser ),
		ModelTranslator( "Collimator",  txtFileParser ) {

    // Store angles in degrees
    double valueUnitConversion;
	if ( txtFileParser->is_set_multi_attribute( "Collimator", "ValueUnits", 0 ) ) {
		string unitStr = txtFileParser->get_multi_attribute( "Collimator", "ValueUnits", 0 ); // MU / timeUnit

		if ( startsWith( unitStr, "r" ) ) { // Match radians
            valueUnitConversion = 180.0 / 3.141592;
		} else if ( startsWith( unitStr, "d" ) ) { // Match seconds
            valueUnitConversion = 1.0;
        } else {
			cerr << "CollimatorAngle(): Unrecognized units string """ << unitStr << """" << endl;
			txtFileParser->report_error("Failed while reading Collimator");
        }
    }

    // min, max and step read in by ConfinedParameter() scale values appropriately.
    minValue_ *= valueUnitConversion;
    maxValue_ *= valueUnitConversion;
    stepSize_ *= valueUnitConversion;

    minSpeed_ *= valueUnitConversion;
    maxSpeed_ *= valueUnitConversion;
    speedStepSize_ *= valueUnitConversion;

    valueOffset_ *= valueUnitConversion;
}

// ----------------------------------------------------------------------- //
// -----------------------  CouchAngle Class ---------------------------- //
// ----------------------------------------------------------------------- //

/**
 * CouchAngle Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the CouchAngle
 * object, also do some basic checking of validity of input data.
 */
CouchAngle::CouchAngle( const std::auto_ptr<const TxtFileParser> & txtFileParser )
		throw ( DeliveryMachineModel_exception )
		: ConfinedParameter( "CouchAngle",  txtFileParser ),
		MovingParameter( "CouchAngle",  txtFileParser ),
		ModelTranslator( "CouchAngle",  txtFileParser ) {

	// Store angles in degrees
	double valueUnitConversion;
	if ( txtFileParser->is_set_multi_attribute( "CouchAngle", "ValueUnits", 0 ) ) {
		string unitStr = txtFileParser->get_multi_attribute( "CouchAngle", "ValueUnits", 0 ); // MU / timeUnit

		if ( startsWith( unitStr, "r" ) ) { // Match radians
			valueUnitConversion = 180.0 / 3.141592;
		} else if ( startsWith( unitStr, "d" ) ) { // Match seconds
			valueUnitConversion = 1.0;
		} else {
			cerr << "CouchAngle(): Unrecognized units string """ << unitStr << """" << endl;
			txtFileParser->report_error("Failed while reading Couch");
		}
	}

	// min, max and step read in by ConfinedParameter() scale values appropriately.
	minValue_ *= valueUnitConversion;
	maxValue_ *= valueUnitConversion;
	stepSize_ *= valueUnitConversion;

	minSpeed_ *= valueUnitConversion;
	maxSpeed_ *= valueUnitConversion;
	speedStepSize_ *= valueUnitConversion;

	valueOffset_ *= valueUnitConversion;
}


// ----------------------------------------------------------------------- //
// -----------------------  Attenuator Class ---------------------------- //
// ----------------------------------------------------------------------- //

/**
 * Attenuator Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the
 * Attenuator object, also do some basic checking of validity of input data.
 */
Attenuator::Attenuator( int atnNo,  const std::auto_ptr<const TxtFileParser> & txtFileParser )
		throw ( DeliveryMachineModel_exception )
		: ConfinedParameter( "Attenuator",  txtFileParser, atnNo ),
		MovingParameter( "Attenuator",  txtFileParser, atnNo ),
		ModelTranslator( "Attenuator",  txtFileParser, atnNo ) {

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Name", atnNo ) ) {
        name_ = txtFileParser->get_multi_attribute( "Attenuator", "Name", atnNo );
    } else {
        name_ = "";
    }

	isX_ = txtFileParser->get_num_multi_attribute<bool>( "Attenuator", "IsX", atnNo );
	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "IsY", atnNo ) ) {
		isY_ = txtFileParser->get_num_multi_attribute<bool>( "Attenuator", "IsY", atnNo );
    } else {
        isY_ = !isX_;
    }
    if ( !isX_ && !isY_ ) {
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Angle", atnNo ) ) {
			angle_ = txtFileParser->get_num_multi_attribute<float>( "Attenuator", "Angle", atnNo );
        } else {
			cerr << "Attenuator(): If attenuator is not set to X or Y axis then ""Angle"" must be set." << endl;
			stringstream strErr;
			strErr << "Failed while reading Attenuator " << atnNo;
			txtFileParser->report_error( strErr.str() );
        }
    }

    if ( isX_ ) {
		angle_ = 0.0f;
    } else if ( isY_ ) {
		angle_ = 90.0f;
    } else {
		angle_ = fmod( angle_ + 360.0f, 360.0f );
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "WithdrawDirectionPositive", atnNo )){
		isWithdrawDirectionPositive_ = txtFileParser->get_num_multi_attribute<bool>(
				"Attenuator", "WithdrawDirectionPositive",  atnNo );
    } else {
		cerr << "Attenuator(): WithdrawDirectionPositive must be set." << endl;
		stringstream strErr;
		strErr << "Failed while reading Attenuator " << atnNo;
		txtFileParser->report_error( strErr.str() );
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "IsFullWidth", atnNo ) ) {
		isFullWidth_ = txtFileParser->get_num_multi_attribute<bool>(
				"Attenuator", "IsFullWidth", atnNo );
    } else {
        // If we have a Width value then set IsFullWidth to false.
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Width", atnNo ) ) {
			if ( startsWith( txtFileParser->get_multi_attribute("Attenuator","Width",atnNo),"na")){
                isFullWidth_ = true;
            } else {
                isFullWidth_ = false;
            }
        } else {
            isFullWidth_ = true;
        }
    }

    if ( isFullWidth_ ) {
        width_ = -1.0f;
        position_ = 0.0f;
    } else {
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Width", atnNo ) ) {
			width_ = txtFileParser->get_num_multi_attribute<float>( "Attenuator", "Width", atnNo );
        } else {
			cerr << "Attenuator(): Width must be set, when the attenuator is not full width." << endl;
			stringstream strErr;
			strErr << "Failed while reading Attenuator " << atnNo;
			txtFileParser->report_error( strErr.str() );
        }

		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Position", atnNo ) ) {
			position_ = txtFileParser->get_num_multi_attribute<float>( "Attenuator", "Position", atnNo );
        } else {
			cerr << "Attenuator(): Position must be set, when the attenuator is not full width." << endl;
			stringstream strErr;
			strErr << "Failed while reading Attenuator " << atnNo;
			txtFileParser->report_error( strErr.str() );
        }
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "IsFullLength", atnNo ) ) {
		isFullLength_ = txtFileParser->get_num_multi_attribute<bool>( "Attenuator", "IsFullLength", atnNo );
    } else {
        // If we have a Length value then set IsFullLength to false.
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Length", atnNo ) ) {
			if ( startsWith( txtFileParser->get_multi_attribute("Attenuator","Length",atnNo),"na")){
                isFullLength_ = true;
            } else {
                isFullLength_ = false;
            }
        } else {
            isFullLength_ = true;
        }
    }

    if ( isFullLength_ ) {
        length_ = -1.0f;
    } else {
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Length", atnNo ) ) {
			length_ = txtFileParser->get_num_multi_attribute<float>( "Attenuator", "Length", atnNo );
        } else {
			cerr << "Attenuator(): Length must be set, when the attenuator is not full length." << endl;
			stringstream strErr;
			strErr << "Failed while reading Attenuator " << atnNo;
			txtFileParser->report_error( strErr.str() );
        }
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "MinimumGapOpposing", atnNo ) ) {
		minGapOpposing_ = txtFileParser->get_num_multi_attribute<float>(
				"Attenuator", "MinimumGapOpposing", atnNo );
    } else {
		minGapOpposing_ = 0.0f;
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "OppositeAboveCanInterDigitate", atnNo ) ) {
		oppositeAboveCanInterDigitate_ = txtFileParser->get_num_multi_attribute<bool>(
						"Attenuator", "OppositeAboveCanInterDigitate", atnNo );
    } else {
        // If we have a value for the minimum gap above then set OppositeAboveCanInterDigitate to false.
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "OppositeAboveMinGap", atnNo ) ) {
			if ( startsWith( txtFileParser->get_multi_attribute(
					"Attenuator", "OppositeAboveMinGap", atnNo ), "na" ) ){
                oppositeAboveCanInterDigitate_ = true;
            } else {
                oppositeAboveCanInterDigitate_ = false;
            }
        } else {
            oppositeAboveCanInterDigitate_ = true;
        }
    }

    if ( oppositeAboveCanInterDigitate_ ) {
        oppositeAboveMinGap_ = -1.0f;
    } else {
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "OppositeAboveMinGap", atnNo ) ) {
			oppositeAboveMinGap_ = txtFileParser->get_num_multi_attribute<float>(
					"Attenuator", "OppositeAboveMinGap", atnNo );
        } else {
            oppositeAboveMinGap_ = 0.0f;
        }
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "OppositeBelowCanInterDigitate", atnNo ) ) {
		oppositeBelowCanInterDigitate_ = txtFileParser->get_num_multi_attribute<bool>(
				"Attenuator", "OppositeBelowCanInterDigitate", atnNo );
    } else {
        // If we have a value for the minimum gap above then set OppositeAboveCanInterDigitate to false.
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "OppositeBelowMinGap", atnNo ) ) {
			if ( startsWith( txtFileParser->get_multi_attribute(
					"Attenuator", "OppositeBelowMinGap", atnNo ), "na" ) ){
                oppositeBelowCanInterDigitate_ = true;
            } else {
                oppositeBelowCanInterDigitate_ = false;
            }
        } else {
            oppositeBelowCanInterDigitate_ = true;
        }
    }

    if ( oppositeBelowCanInterDigitate_ ) {
        oppositeBelowMinGap_ = -1.0f;
    } else {
		if ( txtFileParser->is_set_multi_attribute( "Attenuator", "OppositeBelowMinGap", atnNo ) ) {
			oppositeBelowMinGap_ = txtFileParser->get_num_multi_attribute<float>(
					"Attenuator", "OppositeBelowMinGap", atnNo );
        } else {
            oppositeBelowMinGap_ = 0.0f;
        }
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "Transmission", atnNo ) ) {
		transmission_ = txtFileParser->get_num_multi_attribute<float>(
				"Attenuator", "Transmission", atnNo );
    } else {
        transmission_ = 0.0f;
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "TongueGrooveWidth", atnNo ) ) {
		tongueGrooveWidth_ = txtFileParser->get_num_multi_attribute<float>(
				"Attenuator", "TongueGrooveWidth", atnNo );
    } else {
        tongueGrooveWidth_ = 0.0f;
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "TongueGrooveTransmission", atnNo ) ) {
        tongueGrooveTransmission_ =
				txtFileParser->get_num_multi_attribute<float>(
						"Attenuator", "TongueGrooveTransmission", atnNo );
    } else {
        tongueGrooveTransmission_ = 0.0f;
    }

	if ( txtFileParser->is_set_multi_attribute( "Attenuator", "PenumbraKernel", atnNo ) ) {
        // TODO (dualta#1#): Implement Penumbra Kernels, Read it in here and then do something with it.
		//cerr << "Attenuator(): PenumbraKernel not implemented yet. Ignoring PenumbraKernel." << endl;
    } else {
        // Do nothing.
    }

    // Set if the attenuator is a leaf or a jaw.
    isJaw_ = isFullWidth_;
    isLeaf_ = !isFullWidth_;
}


// ----------------------------------------------------------------------- //
// -----------------------  ConfinedParameter Class ---------------------- //
// ----------------------------------------------------------------------- //

/**
 * ConfinedParameter Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the
 * ConfinedParameter object, also do some basic checking of validity of input data.
 */
ConfinedParameter::ConfinedParameter( const string & parameterClass,
		const std::auto_ptr<const TxtFileParser> & txtFileParser, unsigned int pnNum )
	throw ( DeliveryMachineModel_exception ) {

	minValue_ = txtFileParser->get_num_multi_attribute<float>( parameterClass, "MinValue", pnNum );
	maxValue_ = txtFileParser->get_num_multi_attribute<float>( parameterClass, "MaxValue", pnNum );

	isContinuouslyVariable_ = txtFileParser->get_num_multi_attribute<bool>(
			parameterClass, "ValueIsContinuouslyVariable", pnNum );
    if (isContinuouslyVariable_) {

        // Get the step size
		if ( txtFileParser->is_set_multi_attribute( parameterClass, "ValueStepSize", pnNum ) ) {
			stepSize_ = txtFileParser->get_num_multi_attribute<float>(
					parameterClass, "ValueStepSize", pnNum );
        } else {
			cerr << "ConfinedParameter(): "
					<< "If value is continuously variable then ValueStepSize must be set." << endl;
			stringstream strErr;
			strErr << "Failed while reading " << parameterClass << pnNum;
			txtFileParser->report_error( strErr.str() );
        }
        numValues_ = 0;

    } else {

        // Get the number of valid values
		if ( txtFileParser->is_set_multi_attribute( parameterClass, "NumberOfValidValues", pnNum)){
			numValues_ = txtFileParser->get_num_multi_attribute<size_t>(
					parameterClass, "NumberOfValidValues", pnNum );
        } else {
			cerr << "ConfinedParameter(): "
			<< "If value is not continuously variable then NumberOfValidValues must be set." << endl;
			txtFileParser->report_error( "Failed while reading ", parameterClass );
        }

        // Get the list of valid values
		validValues_.assign( numValues_, 0.0f );

        // Read the values into a stream
		string valuesList;
		if ( txtFileParser->is_set_multi_attribute( parameterClass, "ValidValues", pnNum ) ) {
				valuesList = txtFileParser->get_multi_attribute(
						parameterClass, "ValidValues", pnNum );
        } else {
			cerr << "ConfinedParameter(): "
					<< "If value is not continuously variable then ValidValues must be set." << endl;
			txtFileParser->report_error( "Failed while reading ", parameterClass );
        }

		// Process each value from string
		stringstream ssValuesList(valuesList);
		for (unsigned int valIndex=0; valIndex<numValues_; valIndex++) {
			if ( ( ssValuesList >> validValues_[valIndex] ).fail() ) {
				cerr << "ConfinedParameter(): "
				<< "Reading ValidValues failed are there " << numValues_ << " values listed." << endl;
				txtFileParser->report_error( "Failed while reading ", parameterClass );
            }
        }

        // Check for extra values
        float tempFlt;
		if ( ! ( ssValuesList >> tempFlt ).fail() ) {
			cerr << "ConfinedParameter(): Reading ValidValues failed too many values listed." << endl;
			txtFileParser->report_error( "Failed while reading ", parameterClass );
        }

		sort( validValues_.begin(), validValues_.end() );
        stepSize_ = 0.0f;
    }
}


/**
 * Get an element from the list of valid parameter values.
 */
vector<float> ConfinedParameter::getAllValues() const {
	assert ( ! isContinuouslyVariable_ );
    return validValues_;
};

/**
 * Get a valid parameter value as close as possible to the requested value.
 *
 * @param inValue The requested value.
 *
 * @return the closest valid value to the requested value.
 */
float ConfinedParameter::roundToValidValue( const float & inValue ) const {
    // Set to maximum value, minimum value or requested value as apporpriate
    float outValue = ( inValue < minValue_ ) ? minValue_ : ( ( inValue > maxValue_ ) ? maxValue_ : inValue );

    if ( isContinuouslyVariable_ ) {
        // round value to nearest step
        // ceil (n - 0.5) = round. ceil is in std lib round is not.
        outValue = minValue_ + std::ceil( ( outValue - minValue_ ) / stepSize_ - 0.5f ) * stepSize_;
    } else {
        // find index of first value greater than requested value.
        // ASSUMES THAT VALID VALUES ARE PROPERLY SORTED IN ACCENDING ORDER
		// bind greater than outValue to function with one argument.
		vector<float>::const_iterator itGt = find_if( validValues_.begin(), validValues_.end(),
				bind2nd( greater<float>(), outValue ) );
        outValue = (itGt==validValues_.end() || (outValue-*(itGt-1))<(*itGt-outValue)) ? *(itGt-1) : *itGt;
    }

    return outValue;
};


// ----------------------------------------------------------------------- //
// -----------------------  MovingParameter Class ------------------------ //
// ----------------------------------------------------------------------- //

/**
 * MovingParameter Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the
 * MovingParameter object, also do some basic checking of validity of input data.
 */
MovingParameter::MovingParameter( const string & parameterClass,
		const std::auto_ptr<const TxtFileParser> & txtFileParser, unsigned int pnNum  )
	throw ( DeliveryMachineModel_exception ) {

	if (txtFileParser->is_set_multi_attribute(parameterClass,"ValueCanChangeDuringDelivery",pnNum)){
		canChangeDuringDelivery_ = txtFileParser->get_num_multi_attribute<bool>(
				parameterClass, "ValueCanChangeDuringDelivery", pnNum );
    } else {
        canChangeDuringDelivery_ = false;
    }

    if ( !canChangeDuringDelivery_ ) {

        // No changes are allowed so fill parameter attributes with default values
        changeRequiresInterrupt_ = false;
        interruptLatency_ = 0.0f;
        minSpeed_ = 0.0f;
        maxSpeed_ = 0.0f;
        maxAcceleration_ = 0;
		isSpeedContinuouslyVariable_ = false;
		speedStepSize_ = 0.0f;
        numSpeeds_ = 0;

    } else {

        // Check Speed units
        // N.B. Currently acceleration units are assumed from speed units and the acceleration units attribute in the
        // file is not checked.
        double valueUnitConversion;
		string unitStr, timeUnitStr;
		if ( txtFileParser->is_set_multi_attribute( parameterClass, "SpeedUnits", pnNum ) ) {
			unitStr = txtFileParser->get_multi_attribute( parameterClass, "SpeedUnits", pnNum );
			timeUnitStr = unitStr.substr( unitStr.find_last_of("/") + 1);

			if ( startsWith( timeUnitStr, "m" ) ) { // Match minutes
                valueUnitConversion = 1.0 / 60.0;
			} else if ( startsWith( timeUnitStr, "s" ) ) { // Match seconds
                valueUnitConversion = 1.0;
            } else {
				cerr << "MovingParameter(): Unrecognized units string """ << unitStr << """" << endl;
				txtFileParser->report_error("Failed while reading ", parameterClass );
            }
		} else {
			unitStr = "mm/s";
			timeUnitStr = "s";
			valueUnitConversion = 1.0;
		}

		if ((!txtFileParser->is_set_multi_attribute(parameterClass,"MinSpeed",pnNum) ) ||
			(!txtFileParser->is_set_multi_attribute(parameterClass,"MaxSpeed",pnNum) ) ||
			(!txtFileParser->is_set_multi_attribute(parameterClass,"MaxAcceleration",pnNum) ) ||
			(!txtFileParser->is_set_multi_attribute(parameterClass,"SpeedIsContinuouslyVariable",pnNum)) ||
			(!txtFileParser->is_set_multi_attribute(parameterClass,"ChangeRequiresInterrupt",pnNum)) ||
			(!txtFileParser->is_set_multi_attribute(parameterClass,"InterruptLatency",pnNum) ) ) {

			cerr << "MovingParameter(): If parameter changes are allowed then : "
					<< """MinSpeed"", ""MaxSpeed"", ""MaxAcceleration"", "
					<< """SpeedIsContinuouslyVariable"", ""ChangeRequiresInterrupt"" and "
					"""InterruptLatency"", must all be set." << endl;

			txtFileParser->report_error( "Failed while reading ", parameterClass );
        }

		minSpeed_ = txtFileParser->get_num_multi_attribute<float>(parameterClass,"MinSpeed",pnNum);
		if (startsWith(txtFileParser->get_multi_attribute(parameterClass,"MaxSpeed",pnNum),"inf")) {
			maxSpeed_ = numeric_limits<float>::infinity();
        } else {
			maxSpeed_ = txtFileParser->get_num_multi_attribute<float>(parameterClass,"MaxSpeed",pnNum);
        }
		minSpeed_ *= valueUnitConversion;
		maxSpeed_ *= valueUnitConversion;

		if ( startsWith( txtFileParser->get_multi_attribute(
				parameterClass, "MaxAcceleration", pnNum ), "inf" ) ) {
			maxAcceleration_ = numeric_limits<float>::infinity();
        } else {
			maxAcceleration_ = txtFileParser->get_num_multi_attribute<float>(
					parameterClass, "MaxAcceleration", pnNum );
			maxAcceleration_ *= valueUnitConversion * valueUnitConversion;
        }

		if (txtFileParser->is_set_multi_attribute(parameterClass,"ChangeRequiresInterrupt",pnNum)){
			changeRequiresInterrupt_ = txtFileParser->get_num_multi_attribute<bool>(
					parameterClass, "ChangeRequiresInterrupt", pnNum );
        } else {
            changeRequiresInterrupt_ = false;
        }
        if ( changeRequiresInterrupt_ ) {
            double latencyUnitConversion = 1.0;
			if (txtFileParser->is_set_multi_attribute(parameterClass,"InterruptLatencyUnits",pnNum)){
				string timeUnitStr = txtFileParser->get_multi_attribute(
						parameterClass, "SpeedUnits", pnNum );
				if ( startsWith( timeUnitStr, "m" ) ) { // Match minutes
                    latencyUnitConversion = 60.0;
				} else if ( startsWith( timeUnitStr, "s" ) ) { // Match seconds
                    latencyUnitConversion = 1.0;
                } else {
					cerr << "MovingParameter(): Unrecognized units string """ << unitStr << """" << endl;
					txtFileParser->report_error("Failed while reading ", parameterClass );
                }
            }
			minSpeed_ = txtFileParser->get_num_multi_attribute<float>(
					parameterClass, "InterruptLatency", pnNum ) * latencyUnitConversion;
        } else {
            interruptLatency_ = 0.0f;
        }

		if ( txtFileParser->is_set_multi_attribute(
				parameterClass, "DirectionCanChangeDuringDelivery", pnNum ) ) {
			canChangeDirection_ = txtFileParser->get_num_multi_attribute<bool>(
					parameterClass, "DirectionCanChangeDuringDelivery", pnNum );
        } else {
            canChangeDirection_ = false;
        }

		if ( txtFileParser->is_set_multi_attribute(
				parameterClass, "SpeedIsContinuouslyVariable", pnNum ) ) {
			isSpeedContinuouslyVariable_ = txtFileParser->get_num_multi_attribute<bool>(
					parameterClass, "SpeedIsContinuouslyVariable", pnNum );
        } else {
            isSpeedContinuouslyVariable_ = false;
        }

        if (isSpeedContinuouslyVariable_) {

            // Get the step size
			if ( txtFileParser->is_set_multi_attribute( parameterClass, "SpeedStepSize", pnNum ) ) {
				if ( ! startsWith( txtFileParser->get_multi_attribute(
						parameterClass, "SpeedStepSize", pnNum ), "na" )){
					speedStepSize_ = txtFileParser->get_num_multi_attribute<float>(
							parameterClass, "SpeedStepSize", pnNum ) * valueUnitConversion;
                } else {
                    speedStepSize_ = 0.0f; // if speedStepSize = 0 then speed is truely continuous
                }
            } else {
                speedStepSize_ = 0.0f;
            }
            numSpeeds_ = 0;

        } else {

            // Get the number of valid values
			if ( txtFileParser->is_set_multi_attribute( parameterClass, "NumberOfValidSpeeds", pnNum ) ) {
				numSpeeds_ = txtFileParser->get_num_multi_attribute<size_t>(
						parameterClass, "NumberOfValidSpeeds", pnNum );
            } else {
				cerr << "MovingParameter(): If speed is not continuously variable "
						<< "then NumberOfValidSpeeds must be set." << endl;
				txtFileParser->report_error( "Failed while reading ", parameterClass );
            }

            // Get the list of valid values
            validSpeeds_.reserve(numSpeeds_);

            // Read the values into a stream
			string speedsList;
			if ( txtFileParser->is_set_multi_attribute( parameterClass, "ValidSpeeds", pnNum ) ) {
					speedsList = txtFileParser->get_multi_attribute(
							parameterClass, "ValidSpeeds", pnNum );
            } else {
				cerr << "MovingParameter(): "
				<< "If speed is not continuously variable then ValidSpeeds must be set." << endl;
				txtFileParser->report_error( "Failed while reading " + parameterClass );
            }

            // Process each value from stream
			stringstream ssSpeedsList;
			for (unsigned int valIndex=0; valIndex<numSpeeds_; valIndex++) {
				if ( ( ssSpeedsList >> validSpeeds_[valIndex] ).fail() ) {
					cerr << "MovingParameter(): Reading ValidSpeeds failed "
							<< "are there " << numSpeeds_ << " values listed?" << endl;
					txtFileParser->report_error( "Failed while reading " + parameterClass );
                }
            }

            // Check for extra values
            float tempFlt;
			if ( ! ( ssSpeedsList >> tempFlt ).fail() ) {
				cerr<<"MovingParameter(): Reading ValidSpeeds failed too many values listed."<<endl;
				txtFileParser->report_error( "Failed while reading ", parameterClass );
            }

			sort( validSpeeds_.begin(), validSpeeds_.end() );
            speedStepSize_ = 0.0f;
        }
    }
}


/**
 * Get an element from the list of valid parameter values.
 */
float MovingParameter::validSpeed(const unsigned int & index) const {
    if ( isSpeedContinuouslyVariable_ ) {
		assert( speedStepSize_ > 0 );
		float val = minSpeed_ + speedStepSize_ * index;
		assert( val < maxSpeed_ );
		return val;
	} else {
		assert( index < numSpeeds_ );
        return validSpeeds_[index];
    }
};


/**
 * Get a valid parameter value as close as possible to the requested value.
 *
 * @param inValue The requested value.
 *
 * @return the closest valid value to the requested value.
 */
float MovingParameter::roundToValidSpeed( const float & inValue ) const {
    // Set to maximum value, minimum value or requested value as apporpriate
	float outValue = ( inValue < minSpeed_ ) ? minSpeed_
		: ( ( inValue > maxSpeed_ ) ? maxSpeed_ : minSpeed_ );

    if ( isSpeedContinuouslyVariable_ ) {
        if ( speedStepSize_ > 0 ) {
            // round value to nearest step
            // ceil (n - 0.5) = round. ceil is in std lib round is not.
			outValue = minSpeed_ + ceil((outValue-minSpeed_)/speedStepSize_-0.5f) * speedStepSize_;
        }
    } else {
        // find index of first value greater than requested value.
        // ASSUMES THAT VALID VALUES ARE PROPERLY SORTED IN ACCENDING ORDER
		// bind greater than outValue to function with one argument.
		vector<float>::const_iterator itGt = find_if( validSpeeds_.begin(), validSpeeds_.end(),
				bind2nd( greater<float>(), outValue ) );

        outValue = ( ( outValue - *(itGt - 1) ) < ( *itGt - outValue ) ) ? *(itGt - 1) : *itGt;
    }

    return outValue;
};


// ----------------------------------------------------------------------- //
// -----------------------  ModelTranslator Class ------------------------------- //
// ----------------------------------------------------------------------- //

/**
 * ModelTranslator Constructor.
 * Read the data from the processed text file and extract the relevant data to populate the fields of the
 * ModelTranslator object.
 */
ModelTranslator::ModelTranslator( const string & parameterClass,
		const std::auto_ptr<const TxtFileParser> & txtFileParser, unsigned int pnNum  )
	throw ( DeliveryMachineModel_exception ) {

	if ( txtFileParser->is_set_multi_attribute( parameterClass, "ValueOffset", pnNum ) ) {
		valueOffset_ = txtFileParser->get_num_multi_attribute<float>(
				parameterClass, "ValueOffset", pnNum );
    } else {
        valueOffset_ = 0.0f;
    }

	if ( txtFileParser->is_set_multi_attribute(parameterClass,"IncreasingDirectionAligned",pnNum)){
		valueOffset_ = txtFileParser->get_num_multi_attribute<bool>(
				parameterClass, "IncreasingDirectionAligned", pnNum );
    } else {
        valueOffset_ = true;
    }
};


