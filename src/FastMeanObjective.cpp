/**
 * @file: FastMeanObjective.cpp
 * FastMeanObjective Class implementation.
 *
 * $Id: FastMeanObjective.cpp,v 1.3 2008/03/24 17:05:00 bmartin Exp $
 */

#include "FastMeanObjective.hpp"

/**
 * Constructor: Initialize variables.  By default, there are no constraints.
 */
FastMeanObjective::FastMeanObjective(
        Voi*  the_voi,
        unsigned int objNo,
        float desired_dose,
        bool  is_max_constraint,
        float weight)
: Objective(objNo, weight),
    the_voi_(the_voi),
    desired_dose_(desired_dose),
    is_max_constraint_(is_max_constraint),
    mean_dose_contribution_()
{
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
FastMeanObjective::~FastMeanObjective()
{
}


/**
 * Initialize the objective.  Finds the contribution of each bixel to the mean.
 */
void FastMeanObjective::initialize(
    DoseInfluenceMatrix& Dij)
{
  // Initialize the mean_dose_contribution_ vector
  size_t nBixels = Dij.get_nBixels();
  mean_dose_contribution_.resize(nBixels);
  fill(mean_dose_contribution_.begin(),mean_dose_contribution_.end(), 0);

  // Get the total dose_contribution from the Dij
  size_t nVoxels = the_voi_->get_nVoxels();
  for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
      mean_dose_contribution_[iter.get_bixelNo()] += iter.get_influence();
    }
  }

  // Scale the dose contributions to the mean
  for(size_t iBixel=0; iBixel<nBixels; iBixel++) {
    mean_dose_contribution_[iBixel] /= nVoxels;
  }

  is_initialized_ = true;
}


/**
 * Calculate the mean dose from a vector of beam weights
 */
float FastMeanObjective::calculate_mean_dose(
        const BixelVector & beam_weights) const
{
  assert(is_initialized_);
  assert(beam_weights.get_nBixels() == mean_dose_contribution_.size());

  unsigned int nBixels = mean_dose_contribution_.size();
  float dose_buf = 0;
  for(unsigned int iBixel = 0; iBixel < nBixels; iBixel++) {
    dose_buf += beam_weights[iBixel] * mean_dose_contribution_[iBixel];
  }
  return dose_buf;
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void FastMeanObjective::printOn(std::ostream& o) const
{
  o << "OBJ " << get_objNo() << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  if(is_max_constraint_) {
    o << "\tmean dose < "; 
  }
  else {
    o << "\tmean dose > ";
  }
  o << desired_dose_;
  o << "\tweight " << weight_;
}



double FastMeanObjective::calculate_dose_objective(
    const DoseVector & the_dose)
{
  size_t nVoxels = the_voi_->get_nVoxels();

  // loop over all voxels in this VOI and calculate mean dose
  float dose_buf = 0;
  for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
    dose_buf += the_dose.get_voxel_dose(the_voi_->get_voxel(iVoxel));
  }
  dose_buf /= nVoxels;

  if((is_max_constraint_ && (dose_buf > desired_dose_))
      || (!is_max_constraint_ && (dose_buf < desired_dose_))) {
    return (dose_buf - desired_dose_)
      * (dose_buf - desired_dose_) * weight_;
  } else {
    return 0;
  }
}


/**
 * Calculate objective from BixelVector and DoseInfluenceMatrix
 * Doesn't require the computation of the entire dose influence matrix.
 */
double FastMeanObjective::calculate_objective(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    estimated_ssvo = 0;

  // loop over all voxels in this VOI and calculate mean dose
  float dose_buf = calculate_mean_dose(beam_weights);

  if((is_max_constraint_ && (dose_buf > desired_dose_))
      || (!is_max_constraint_ && (dose_buf < desired_dose_))) {
    return (dose_buf - desired_dose_)
      * (dose_buf - desired_dose_) * weight_;
  } else {
    return 0;
  }
}



double FastMeanObjective::calculate_objective_and_gradient(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    estimated_ssvo = 0;

  assert(is_initialized_);
  assert(beam_weights.get_nBixels() == gradient.get_nBixels());
  assert(beam_weights.get_nBixels() == mean_dose_contribution_.size());
  unsigned int nBixels = mean_dose_contribution_.size();

  double objective = 0;
    

  // Calculate mean dose
  float dose_buf = calculate_mean_dose(beam_weights);

    // Calculate contribution to objective and gradient
  if((is_max_constraint_ && (dose_buf > desired_dose_))
      || (!is_max_constraint_ && (dose_buf < desired_dose_))) {
    objective = (dose_buf - desired_dose_)
      * (dose_buf - desired_dose_) * weight_;
    double dObjective_by_dose = 2 * (dose_buf - desired_dose_)
      * weight_ * gradient_multiplier;

    for(unsigned int iBixel = 0; iBixel < nBixels; iBixel++) {
      gradient[iBixel] +=
	dObjective_by_dose * mean_dose_contribution_[iBixel];
    }
  } else {
    objective = 0;
  }

  return objective;
}



double FastMeanObjective::calculate_objective_and_gradient_and_Hv(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier,
    const vector<float> & v,
    vector<float> & Hv,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    estimated_ssvo = 0;
  assert(is_initialized_);
  size_t nBixels = beam_weights.get_nBixels();
  assert(gradient.get_nBixels() == nBixels);
  assert(mean_dose_contribution_.size() == nBixels);
  assert(v.size() == nBixels);
  assert(Hv.size() == nBixels);

  double objective = 0;
    

  // Calculate mean dose
  float dose_buf = calculate_mean_dose(beam_weights);

    // Calculate contribution to objective and gradient
  if((is_max_constraint_ && (dose_buf > desired_dose_))
      || (!is_max_constraint_ && (dose_buf < desired_dose_))) {
    objective = (dose_buf - desired_dose_)
      * (dose_buf - desired_dose_) * weight_;
    double dObjective_by_dose = 2 * (dose_buf - desired_dose_)
      * weight_ * gradient_multiplier;
    double d2Objective_by_dose2 = 2 * weight_ * gradient_multiplier;

    // Calculate Contribution to gradient
    for(unsigned int iBixel = 0; iBixel < nBixels; iBixel++) {
      gradient[iBixel] +=
	dObjective_by_dose * mean_dose_contribution_[iBixel];
    }
    
    // Calculate Contribution to Hv
    double z_i = 0;
    for(unsigned int iBixel = 0; iBixel < nBixels; iBixel++) {
      z_i += v[iBixel] * mean_dose_contribution_[iBixel];
    }
    z_i *= d2Objective_by_dose2;
    for(unsigned int iBixel = 0; iBixel < nBixels; iBixel++) {
        Hv[iBixel] += z_i * mean_dose_contribution_[iBixel];
    }
  } else {
    objective = 0;
  }

  return objective;
}
        

/// Find out how many voxels were in objective
unsigned int FastMeanObjective::get_nVoxels() const
{
  return 1;
}


