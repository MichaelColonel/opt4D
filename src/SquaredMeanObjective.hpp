/**
 * @file SquaredMeanObjective.hpp
 * SquaredMeanObjective Class header
 *
 * $Id: SquaredMeanObjective.hpp,v 1.5 2008/04/14 20:42:13 bmartin Exp $
 */

#ifndef SQUAREDMEANOBJECTIVE_HPP
#define SQUAREDMEANOBJECTIVE_HPP

#include "NonSeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class SquaredMeanObjective is used to apply mean squared dose error 
 * objectives.
 */
class SquaredMeanObjective : public NonSeparableObjective
{

    public:

        // Constructor
        SquaredMeanObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement,
                bool  is_max_constraint,
                float max_dose,
                float weight_over,
                bool  is_min_constraint = false,
                float min_dose = 0,
                float weight_under = 1);

        // Virtual destructor
        virtual ~SquaredMeanObjective();

        //
	// SquaredMeanObjective specific functions
        //
        void set_max_dose(float max_dose);
        float get_max_dose()const;
        void set_min_dose(float min_dose);
        float get_min_dose()const;

        //
	// Functions reimplemented from NonSeparableObjective
        // 
        virtual double calculate_objective(
                const vector<float> & dose) const;
        virtual double calculate_objective_and_d(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose) const;
        virtual double calculate_objective_and_d2(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose,
	        vector<float> & d2_obj_by_dose) const;

        //
        // Functions reimplemented from Objective
        //

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;


    private:
        // Default constructor not allowed
        SquaredMeanObjective();

        /// True if this is a maximum dose constraint
        bool is_max_constraint_;

        /// Maximum dose in Gy
        float max_dose_;

        /// Penalty for being over the max dose (multiplied by the weight)
        float weight_over_;

        /// True if this is a minimum dose constraint
        bool is_min_constraint_;

        /// Minimum dose in Gy
        float min_dose_;

        /// Penalty for being under the min dose (multiplied by the weight)
        float weight_under_;

};

/*
 * Inline functions
 */

inline
void SquaredMeanObjective::set_max_dose(float max_dose)
{
    max_dose_ = max_dose;
}

inline
void SquaredMeanObjective::set_min_dose(float min_dose)
{
    min_dose_ = min_dose;
}

inline
float SquaredMeanObjective::get_max_dose()const
{
    return max_dose_;
}


inline
float SquaredMeanObjective::get_min_dose()const
{
    return min_dose_;
}




/**
 * Calculate the objective from some voxels with the given doses.
 *
 * @param dose The vector of voxel doses.
 */
inline
double SquaredMeanObjective::calculate_objective(
	const vector<float> & dose) const
{
    unsigned int nVoxels = dose.size();

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }

    // Calculate Mean dose
    float mean_dose = 0;
    vector<float>::const_iterator iter;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
	mean_dose += *iter;
    }
    mean_dose /= static_cast<float>(nVoxels);

    // Calculate objective
    float objective = 0.0f;
    if(is_max_constraint_ && (mean_dose > max_dose_)) {
	objective += (mean_dose - max_dose_) * (mean_dose - max_dose_) 
            * weight_over_ * weight_ ;
    }

    if(is_min_constraint_ && (mean_dose < min_dose_)) {
	objective+= (mean_dose - min_dose_) * (mean_dose - min_dose_) 
            * weight_under_ * weight_;
    }

    return objective;
}


/**
 * Calculate the objective and partial derivative of the voxel contribution to 
 * the objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 */
inline
double SquaredMeanObjective::calculate_objective_and_d(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose) const
{
    int nVoxels = dose.size();
    float nVoxels_f = static_cast<float>(nVoxels);

    // Resize return vector
    d_obj_by_dose.resize(nVoxels);

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }

    // Calculate Mean dose
    float mean_dose = 0;
    vector<float>::const_iterator iter;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
	mean_dose += *iter;
    }
    mean_dose /= nVoxels_f;

    // Check if over or under dose constraint is active
    double objective = 0;
    double d_obj_by_voxel = 0;
    if(is_max_constraint_ && (mean_dose > max_dose_)) {
        // the mean dose is above the max
        float error = mean_dose - max_dose_;
        objective = error * error * weight_over_ * weight_;
        d_obj_by_voxel = 2 * error * weight_over_ * weight_ / nVoxels_f;
        assert(d_obj_by_voxel > 0);

        // Fill return vector
        iter = dose.begin();
        vector<float>::iterator return_iter = d_obj_by_dose.begin();
        while(iter != dose.end()) {
            // Set the partial derivative to d_obj_by_voxel if the dose is 
            // above zero.
            *return_iter = (*iter > 0) ? d_obj_by_voxel : 0;
            iter++;
            return_iter++;
        }
    }
    else if(is_min_constraint_ && (mean_dose < min_dose_)) {
        // The mean dose is below the min
        float error = mean_dose - min_dose_;
        objective = error * error * weight_under_ * weight_;
        d_obj_by_voxel = 2 * error * weight_under_ * weight_ / nVoxels_f;
        assert(d_obj_by_voxel < 0);

        // Set the partial derivative to d_obj_by_voxel regardless of the dose
        std::fill(d_obj_by_dose.begin(), d_obj_by_dose.end(),  d_obj_by_voxel);
    }
    else {
        // Set the partial derivative to 0 regardless of the dose
        std::fill(d_obj_by_dose.begin(), d_obj_by_dose.end(), 0.0f);
    }

    return objective;
}


/**
 * Calculate the objective and first and second partial derivatives of the 
 * voxel contribution to the objective with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 * @param d2_obj_by_dose The vector to hold the returned partial derivatives
 */
inline
double SquaredMeanObjective::calculate_objective_and_d2(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose,
	vector<float> & d2_obj_by_dose) const
{
    int nVoxels = dose.size();
    float nVoxels_f = static_cast<float>(nVoxels);

    // Resize return vector
    d_obj_by_dose.resize(nVoxels);
    d2_obj_by_dose.resize(nVoxels);

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }


    //
    // Calculate Mean dose
    //
    float mean_dose = 0;
    vector<float>::const_iterator iter;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
	mean_dose += *iter;
    }

    // Check if over or under dose constraint is active
    double objective = 0;
    double d_obj_by_voxel = 0;
    double d2_obj_by_voxel = 0;
    if(is_max_constraint_ && (mean_dose > max_dose_)) {
	// the mean dose is above the max
        float error = mean_dose - max_dose_;
	objective = error * error * weight_over_ * weight_;
	d_obj_by_voxel = 2 * error * weight_over_ * weight_ / nVoxels_f;
	d2_obj_by_voxel = 2 * weight_over_ * weight_ / nVoxels_f;

	// Fill return vectors

        // Set the partial derivative to d_obj_by_voxel if the dose is 
        // above zero.
        iter = dose.begin();
        vector<float>::iterator return_iter = d_obj_by_dose.begin();
        while(iter != dose.end()) {
            *return_iter = (*iter > 0) ? d_obj_by_voxel : 0;
            iter++;
            return_iter++;
        }
    }
    else if(is_min_constraint_ && (mean_dose < min_dose_)) {
        // The mean dose is below the min
        float error = mean_dose - min_dose_;
	objective = error * error * weight_under_ * weight_;
	d_obj_by_voxel = 2 * error * weight_under_ * weight_ / nVoxels_f;
	d2_obj_by_voxel = 2 * weight_under_ * weight_ / nVoxels_f;

        // Set the partial derivative to d_obj_by_voxel regardless of the 
        // dose
        std::fill(d_obj_by_dose.begin(), d_obj_by_dose.end(),  d_obj_by_voxel);
    }
    else {
        // Set the partial derivative to 0 regardless of the dose
        std::fill(d_obj_by_dose.begin(), d_obj_by_dose.end(), 0.0f);
    }

    // Set the second partial derivative to d2_obj_by_voxel regardless of 
    // the dose
    std::fill(d2_obj_by_dose.begin(), d2_obj_by_dose.end(),
            d2_obj_by_voxel);

    return objective;
}




#endif
