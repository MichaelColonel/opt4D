#include "LETModel.hpp"
#include "KonradDoseInfluenceMatrix.hpp"
#include <fstream>




// ---------------------------------------------------------------------
// LET times dose model
// ---------------------------------------------------------------------

/**
 * constructor 
 */
LETModel::LETModel(
    const Geometry *the_geometry, 
    const DoseDeliveryModel::options *options,
    const string dij_file_root,
    const string stf_file_root)
  : DoseDeliveryModel(the_geometry),
    LETxDij_(),
    LET_bixels_(),
    LET_geometry_(),
    LETxD_scalefactor_(options->LET_scalefactor_),
    physD_scalefactor_(options->physD_scalefactor_)
{
    cout << "Constructing LET times dose model" << endl;
    cout << "physD_scalefactor:" << physD_scalefactor_ << endl;
    cout << "LET_scalefactor:" << LETxD_scalefactor_ << endl;

    bool read_beamlets_first = false;

    // construct the virtual geometry
    LET_geometry_.reset(new Geometry(*the_geometry,0,0));

    if(read_beamlets_first)
      {
	// Construct virtual bixel vector
	LET_bixels_.reset(new KonradBixelVector(LET_geometry_.get(), 
						1, 
						the_geometry_->get_nBeams(), 
						stf_file_root, 
						stf_file_root));
	
	// read LET time dose matrix
	LETxDij_.reset(new KonradDoseInfluenceMatrix(dij_file_root, 
						     LET_geometry_.get(), 
						     the_geometry_->get_nBeams(), 
						     1, 
						     *LET_bixels_, 
						     true));
      }
    else
      {
	LETxDij_.reset(new KonradDoseInfluenceMatrix(dij_file_root, LET_geometry_.get(),
						     the_geometry_->get_nBeams(), 1,
						     true));
      }
 

}


/**
 * destructor 
 */
LETModel::~LETModel()
{
}

/**
 * does nothing so far 
 */
void LETModel::generate_random_scenario(RandomScenario &random_scenario) const
{
}

/**
 * does nothing so far 
 */
void LETModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
}

// calculate voxel dose for a scenario
float LETModel::calculate_voxel_dose(
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario) const
{
    // just call the generic dose calculation for the LETij matrix
    return( physD_scalefactor_ * dij.calculate_voxel_dose(voxelNo,bixel_grid) + 
	    LETxD_scalefactor_ * LETxDij_->calculate_voxel_dose(voxelNo,bixel_grid) );
}


// calculate gradient contributione of the voxel for a scenario
void LETModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,
 	const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{
    // physical dose component
    dij.calculate_voxel_dose_gradient(voxelNo,
				      gradient,
				      physD_scalefactor_*gradient_multiplier);

    // just call the generic dose calculation for the LETij matrix
    LETxDij_->calculate_voxel_dose_gradient(voxelNo,
					    gradient,
					    LETxD_scalefactor_*gradient_multiplier);
}


/**
 * write results
 */
void LETModel::write_dose(BixelVector &bixel_grid,
			  DoseInfluenceMatrix &dij,
			  string file_prefix, 
			  Dvh dvh) const
{
    string dose_file, dvh_file;
    DoseVector dose(the_geometry_->get_nVoxels());
    DoseVector phys(the_geometry_->get_nVoxels());
    BixelVector beamweights; 
    char cbeam[10];

    // calculate LET times dose
    dose.reset_dose();
    LETxDij_->dose_forward(dose, bixel_grid); 
    dose.scale_dose(LETxD_scalefactor_);

    // calculate DVH from dose
    dvh.calculate(dose);

    // write files
    dose_file = file_prefix + "LETxD_dose.dat";
    dvh_file = file_prefix + "LETxD_DVH.dat";
    dose.write_dose(dose_file);
    dvh.write_DVH(dvh_file);

    // calculate physical dose
    phys.reset_dose();
    dij.dose_forward(phys, bixel_grid); 

    // calculate DVH from dose
    dvh.calculate(phys);

    // write files
    dose_file = file_prefix + "physical_dose.dat";
    dvh_file = file_prefix + "physical_DVH.dat";
    phys.write_dose(dose_file);
    dvh.write_DVH(dvh_file);

    // biological dose
    phys.scale_dose(physD_scalefactor_);
 
    //dose += phys;
    //dvh.calculate(dose);
    //dose_file = file_prefix + "RBExD_dose.dat";
    //dvh_file = file_prefix + "RBExD_DVH.dat";
    //dose.write_dose(dose_file);
    //dvh.write_DVH(dvh_file);

    phys += dose;
    dvh.calculate(phys);
    dose_file = file_prefix + "RBExD_dose.dat";
    dvh_file = file_prefix + "RBExD_DVH.dat";
    phys.write_dose(dose_file);
    dvh.write_DVH(dvh_file);

    // write individual beam doses
    unsigned int nBeams = the_geometry_->get_nBeams();
    for(unsigned int iBeam=0; iBeam<nBeams; iBeam++)
    {
	// extract beam, set beam weights to zero if not part of the current beam
	beamweights = bixel_grid;
	beamweights.extract_beam(the_geometry_,iBeam);

        if (nBeams < 9) {
            sprintf(cbeam,"%d",iBeam+1);
        } else if (nBeams < 99) {
            sprintf(cbeam,"%02d",iBeam+1);
        } else if (nBeams < 999) {
            sprintf(cbeam,"%03d",iBeam+1);
        } else {
            sprintf(cbeam,"%04d",iBeam+1);
        }

	// physical dose
	dose.reset_dose();
	dij.dose_forward(dose, beamweights); 
        dose_file = file_prefix + "beam_" + string(cbeam) + "_phys.dat";
        dose.write_dose(dose_file);

	// LEX x dose
	dose.reset_dose();
	LETxDij_->dose_forward(dose, beamweights); 
	dose.scale_dose(LETxD_scalefactor_);
        dose_file = file_prefix + "beam_" + string(cbeam) + "_LETxD.dat";
        dose.write_dose(dose_file);
    }
}


