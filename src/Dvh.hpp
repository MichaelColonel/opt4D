/**
 * @file Dvh.hpp
 * Header for Dvh class.
 *  $Id: Dvh.hpp,v 1.3 2007/10/24 16:29:35 unkelbac Exp $
 */

#ifndef DVH_HPP
#define DVH_HPP

#include <vector>
using std::vector;

#include <string>
#include <iostream>
#include <assert.h>

class Dvh;

#include "Voi.hpp"
#include "DoseVector.hpp"

/**
 * The Dvh class calculates, stores, and writes Dose Volume Histograms.
 */
class Dvh
{
    public:

        // Constructor
        Dvh(const vector<const Voi* > & the_vois, float DVH_bin_size);

        // Calculate the DVH from a DoseVector
        void calculate( const DoseVector& dose);

        // add one DVH to another
        Dvh& operator+= (const Dvh &rhs);

        // scale percentage values by a factor
        Dvh& operator*= (const float &rhs);

        // Write DVH to file
        void write_DVH(string DVHfile) const;

        // Append DVH to a file
        void append_DVH(string DVHfile) const;

        // Print DVH on a stream
        void print_on(std::ostream & o) const;

        // Access DVH
        unsigned int get_nBins() const;
        unsigned int get_nBins(unsigned int voiNo) const;
        float get_bin_size() const;
        float get_percentage(unsigned int voiNo, const unsigned int iDose) const;
        float get_dose(const unsigned int iDose) const;

    private:
        /// Default constructor not allowed
        Dvh();

        /// Pointers to the volumes of interest
        const vector<const Voi* > the_vois_;

        /// The number of Vois
        const unsigned int nVois_;

        /// distance between DVH dose samples in Gy
        const float bin_size_;

        /// The normalized integral dose volume histogram for Voi
        vector< vector<float> > DVH_;

        /// The maximum number of non-zero bins in any VOI's DVH
        unsigned int max_nBins_;
};

/*
 * Inline functions
 * ----------------
 */

/**
 * Get the maximum number of non-zero bins for any VOI.
 */
inline
unsigned int Dvh::get_nBins() const
{
    return max_nBins_;
}

/**
 * Get the number of non-zero bins for a particular VOI.
 */
inline
unsigned int Dvh::get_nBins(unsigned int voiNo) const
{
    assert(voiNo < DVH_.size());
    return DVH_[voiNo].size();
}

/**
 * Get the percenage volume at or above the given dose bin in the given VOI.
 */
inline
float Dvh::get_percentage(
	const unsigned int voiNo,
       	const unsigned int iDose)const
{
    assert(voiNo < DVH_.size());
    if(iDose < DVH_[voiNo].size())
	return DVH_[voiNo][iDose];
    else {
	// Past the end of the vector, so dose must be zero
	return 0;
    }
}

/**
 * Get the dose for a dose bin.
 */
inline
float Dvh::get_dose(const unsigned int iDose) const
{
  return float(iDose)*bin_size_;
}


inline
float Dvh::get_bin_size() const
{
  return bin_size_;
}

#endif
