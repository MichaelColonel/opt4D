
/**
 * @file SteepestDescent.hpp
 * SteepestDescent Class Header
 *
 * $Id: SteepestDescent.hpp,v 1.1 2007/09/20 19:03:08 bmartin Exp $
 */

#ifndef STEEPESTDESCENT_HPP
#define STEEPESTDESCENT_HPP

class SteepestDescent;

#include <iostream>
#include "Optimizer.hpp"
#include "BixelVectorDirection.hpp"



/**
 * Class SteepestDescent is an optimizer object to implement a simple steepest
 * descent algorithm.
 * <p>
 * The overall algorithm works like this:
 * <ol>
 *   <li> Initialization:
 *   <ol>
 *     <li> Take step equal to the negative of the gradient times the step size
 *   </ol>
 *   <li> Later steps:
 *   <ol>
 *     <li> Take step equal to the negative of the gradient times the step size
 *   </ol>
 * </ol>
 *
 * Only needs the objective and gradient at each step.
 */
class SteepestDescent : public Optimizer
{

    public:
	class options;

	SteepestDescent(
		SteepestDescent::options options,
		unsigned int nParams
		);


	virtual ~SteepestDescent() {};

	/**
	 * SteepestDescent options class
	 *
	 * Make sure you update the print_on function when adding or changing
	 * options.
	 */
	class options
	{
	    public:
		options()
		    : step_size_(1)
		{};

		// Print options
		friend std::ostream& operator<< (std::ostream& o,
			const SteepestDescent::options& opt);

		/// Number to multiply the default step size by
		double step_size_;
	};

	SteepestDescent::options options_;

	/// Calculate the direction based on just the gradient
        void calculate_direction(const ParameterVectorDirection & gradient,
				 const ParameterVector & param);

	/// Print out options
	virtual void print_options(std::ostream& o) const;

};

#endif
