/**
 * @file CpuTimer.hpp
 * CpuTimer Class Header
 */

#ifndef CPUTIMER_HPP
#define CPUTIMER_HPP

#include <cmath>
#include <assert.h>
#include <time.h>

class CpuTimer;


/**
 * Convienient timer module to use to time program excecution.
 *
 * It relies on one cludge that may make timing inaccurate on 64-bit systems.
 *
 * @todo Test CpuTimer on 64-bit systems.
 */
class CpuTimer
{
  public:
    CpuTimer();

    double get_time() const;
    double get_ignored_time() const;
    void restart();
    void pause();
    void resume();

  private:

    double start_time_;

    bool is_paused_;
    double pause_time_;

    /// Holds the ignored time (in seconds)
    double ignored_time_;

    /// Counts how many times the clock overflows
    mutable unsigned int clock_overflow_counter_;

    /// Last clock measurement, used to detect clock overflows
    mutable clock_t last_clock_;
};

inline
double CpuTimer::get_time() const
{
  clock_t temp_clock = clock();

  // Handle Clock overflows
  if(double(temp_clock) < double(last_clock_)) {
    // Clock overflowed
    clock_overflow_counter_ ++;
  }
  last_clock_ = temp_clock;

  if(is_paused_) {
    return pause_time_;
  }
  else {
    return (((double)temp_clock + clock_overflow_counter_ * std::pow(2.0,32))
        /CLOCKS_PER_SEC)-ignored_time_-start_time_;
  }
}


inline
void CpuTimer::pause()
{
  assert(is_paused_ == false);
  
  pause_time_ = get_time();
  is_paused_ = true;
}

inline
void CpuTimer::resume()
{
  assert(is_paused_ == true);
  
  is_paused_ = false;
  ignored_time_ += get_time() - pause_time_;
}


inline
double CpuTimer::get_ignored_time() const
{
  return ignored_time_;
}

#endif
