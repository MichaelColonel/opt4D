/**
 * @file TxtFileParser.cpp
 * Code for TxtFileParser class
 *
 * $Id: TxtFileParser.cpp$
 */

// #include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <exception>
#include <sstream>
#include <assert.h>

#include "TxtFileParser.hpp"

using namespace std;

txtFile_exception txtFile_ex;

/**
 * TxtFileParser Constructor.
 */
TxtFileParser::TxtFileParser( const string & file_name,  const vector<string> & replacement_strings,
        const vector<string> & required_attributes,  const vector<string> & optional_attributes,
        const map<string, string> & default_values,  const vector<string> & multi_attributes,
        const map<string, vector<string> > & required_sub_attributes,
        const map<string, vector<string> > & optional_sub_attributes,
        const map<string, map<string,string> > & default_sub_values,
        const string &fileVer )
        : file_name_( file_name ),  attributes_(),  multi_attributes_(),  replacement_strings_( replacement_strings ) {

    readFile( required_attributes, optional_attributes, default_values, multi_attributes, required_sub_attributes,
                optional_sub_attributes, default_sub_values, fileVer, true );
}

/**
 * TxtFileParser Constructor.
 */
TxtFileParser::TxtFileParser( const string & file_name,  const vector<string> & required_attributes,
        const vector<string> & optional_attributes,  const map<string, string> & default_values,
        const vector<string> & multi_attributes,  const map<string, vector<string> > & required_sub_attributes,
        const map<string, vector<string> > & optional_sub_attributes,
        const map<string, map<string,string> > & default_sub_values,
        const string &fileVer )
        : file_name_( file_name ),  attributes_(),  multi_attributes_(),  replacement_strings_() {

    readFile( required_attributes, optional_attributes, default_values, multi_attributes, required_sub_attributes,
                optional_sub_attributes, default_sub_values, fileVer, false );
}

/**
 * Read in lines form the file and process information for object.
 * Called from different constructors to build objects
 */
void TxtFileParser::readFile( const vector<string> & required_attributes,
        const vector<string> & optional_attributes,  const map<string, string> & default_values,
        const vector<string> & multi_attributes, const map<string, vector<string> > & required_sub_attributes,
        const map<string, vector<string> > & optional_sub_attributes,
        const map<string, map<string,string> > & default_sub_values,
        const string &fileVer,  bool doStringReplacement=true ) {
    // Open the file
    std::cout << "Reading file " <<  file_name_ << " ..." << std::endl;

    // Read into an array of lines
    vector< TxtFileParser::line > lines = read_lines_from_file( file_name_ );

    // PreProcess the lines to remove white space, comments and blank lines and to replace the strings where necessary
    vector<TxtFileParser::line>::iterator iter = lines.begin();
    while( iter != lines.end() ) {
        // Import include file if one an include directive ( #include ) is found.
        if ( import_include_file( iter, lines ) ) continue;
        // If there is a comment then erase all of the line after the '#'
        if ( remove_comments( iter ) ) continue;
        // Handle combined lines of file
        if ( merge_combined_lines( iter, lines ) ) continue;
        // Handle Replacement strings
        if ( doStringReplacement && replace_strings( iter ) ) continue;
        // Erase empty lines
        if(iter->empty()) {
            iter = lines.erase(iter);
            continue;
        }
        // Completed processing current line, so move to next line
        iter++;
    }

    // Verify the the plan file version number
    // TODO (dualta#1#): The version number checking should be changed to allow support for backwards compatibility (If this is desired).
    if( lines.size() == 0 || lines[0].str_.find( "file_version " + fileVer ) == string::npos ) {
        report_error( string( "Error: Not a valid version " + fileVer +
                "\nFirst line in file must be file version." ) );
    }

    // Read required attributes
    for( vector<string>::const_iterator iter = required_attributes.begin();  iter != required_attributes.end(); iter++ ) {
        attributes_[*iter] = read_attribute( lines, *iter, true );
    }

    // Read optional attributes
    for( vector<string>::const_iterator iter = optional_attributes.begin(); iter != optional_attributes.end(); iter++ ) {
        map<string,string>::const_iterator map_iter = default_values.find( *iter );
        if( map_iter == default_values.end() ) {
            // No default value was present, so use an empty string as default
            attributes_[ *iter ] = read_attribute( lines, *iter, false, "" );
        } else {
            // A default value was present
            attributes_[ *iter ] = read_attribute( lines, *iter, false, map_iter->second );
        }
    }

    /// Read a list of required and optional multi-attributes from the file
    read_multi_attributes_from_file( lines, multi_attributes, required_sub_attributes, optional_sub_attributes,
            default_sub_values );

    // There shouldn't be any lines left
    if(lines.size() > 0) {
        report_error( "Unknown attribute found.", lines.at(0).lineNo_ );
    }
}


/**
 * Get a string attribute.  Throws an exception if the attribute was not present when file was read.
 */
string TxtFileParser::get_attribute( const string & attribute ) const {
    map<string, TxtFileParser::line>::const_iterator iter = attributes_.find( attribute );
    if( iter == attributes_.end() ) {
        report_error( "Attribute: \"" + attribute + "\" not present during parsing." );
        return ""; // Never reached
    } else {
        return iter->second.str_;
    }
}


/**
 * Get a string attribute.  Throws an exception if the attribute was not present when file was read.
 */
bool TxtFileParser::is_set_attribute( const string & attribute ) const {
    map<string, TxtFileParser::line>::const_iterator iter = attributes_.find(attribute);
    if( iter == attributes_.end() ) {
        report_error( "Attribute: \"" + attribute + "\" not present during parsing." );
        return ""; // Never reached
    } else {
        return iter->second.is_set();
    }
}


/**
 * Get a numeric attribute.
 * Throws an exception if the attribute was not present when file was read or if the number is not formatted correctly.
 */
template<class T> T TxtFileParser::get_num_attribute( const string & attribute ) const {
    T temp;

    // Find the attribute if present
    map<string, TxtFileParser::line>::const_iterator iter = attributes_.find( attribute );
    if( iter == attributes_.end() ) {
        report_error( "Attribute: \"" + attribute + "\" not present during parsing." );
    } else {
        string temp_str = iter->second.get_first_word();
        if( !from_string<T>( temp, temp_str, std::dec ) ) {
            report_error( "Problem reading number " + temp_str, iter->second );
        }
    }
    return temp;
}
template bool TxtFileParser::get_num_attribute<bool>( const string & attribute ) const;
template int TxtFileParser::get_num_attribute<int>( const string & attribute ) const;
template unsigned int TxtFileParser::get_num_attribute<unsigned int >( const string & attribute ) const;
template size_t TxtFileParser::get_num_attribute< size_t >( const string & attribute ) const;
template float TxtFileParser::get_num_attribute< float >( const string & attribute ) const;


/**
 * Get number of things in a multi-attribute.
 * Throws an exception if there were no sub attributes present when file was read.
 */
size_t TxtFileParser::get_multi_attribute_nThings( const string & multi_attribute ) const {
    // Find the multi-attribute
    size_t temp = 0;
    map<string, map< string, vector< TxtFileParser::line > > >::const_iterator m_iter
            = multi_attributes_.find( multi_attribute );
    if( m_iter == multi_attributes_.end() ) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Find the first sub-attribute
        map< string, vector< TxtFileParser::line > >::const_iterator s_iter = m_iter->second.begin();
        if( s_iter == m_iter->second.end() ) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", has no Sub-Attributes" );
        } else {
            // Get the nThings from the first sub-attribute
            temp = s_iter->second.size();
        }
    }
    return temp;
}


/**
 * Get a string multi-attribute.
 * Throws an exception if the attribute was not present when file was read.
 */
vector<string> TxtFileParser::get_multi_attribute( const string & multi_attribute, const string & sub_attribute ) const {
    vector<string> temp;

    // Find the multi-attribute
    map< string, map< string, vector< TxtFileParser::line > > >::const_iterator
            m_iter = multi_attributes_.find( multi_attribute );
    if( m_iter == multi_attributes_.end() ) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Find the sub-attribute
        map< string, vector< TxtFileParser::line > >::const_iterator s_iter =
            m_iter->second.find( sub_attribute );
        if( s_iter == m_iter->second.end() ) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", Sub-Attribute: \"" + sub_attribute
                    + "\" not present during parsing." );
        } else {
            // Get the strings from each line
            temp.reserve(s_iter->second.size());
            vector<TxtFileParser::line>::const_iterator line_iter = s_iter->second.begin();
            while(line_iter != s_iter->second.end()) {
                temp.push_back(line_iter->str_);
                line_iter++;
            }
        }
    }
    return temp;
}


/**
 * Get a single value from a string multi-attribute.
 * Throws an exception if the attribute was not present when file was read.
 */
string TxtFileParser::get_multi_attribute( const string & multi_attribute,  const string & sub_attribute,
        size_t thingNo ) const {
    string temp;

    // Find the multi-attribute
    map<string, map<string, vector<TxtFileParser::line> > >::const_iterator m_iter =
            multi_attributes_.find( multi_attribute );
    if( m_iter == multi_attributes_.end() ) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Find the sub-attribute
        map<string, vector<TxtFileParser::line> >::const_iterator s_iter = m_iter->second.find( sub_attribute );
        if(s_iter == m_iter->second.end()) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", Sub-Attribute: \"" + sub_attribute
                    + "\" not present during parsing." );
        } else {
            // Avoid trying to read too large a thingNo
            assert( s_iter->second.size() > thingNo );
            temp = s_iter->second[thingNo].str_;
        }
    }
    return temp;
}


/**
 * Find out if multi-attribute is set for each object.
 * Throws an exception if the attribute was not present when file was read.
 */
vector<bool> TxtFileParser::is_set_multi_attribute(const string & multi_attribute, const string & sub_attribute) const {
    vector<bool> temp;

    // Find the multi-attribute
    map<string, map<string, vector<TxtFileParser::line> > >::const_iterator m_iter =
        multi_attributes_.find( multi_attribute );
    if( m_iter == multi_attributes_.end() ) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Find the sub-attribute
        map<string, vector<TxtFileParser::line> >::const_iterator s_iter = m_iter->second.find(sub_attribute);
        if(s_iter == m_iter->second.end()) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", Sub-Attribute: \"" + sub_attribute
                    + "\" not present during parsing." );
        } else {
            // Get the strings from each line
            temp.reserve(s_iter->second.size());
            vector<TxtFileParser::line>::const_iterator line_iter = s_iter->second.begin();
            while(line_iter != s_iter->second.end()) {
                temp.push_back(line_iter->is_set());
                line_iter++;
            }
        }
    }
    return temp;
}


/**
 * Find out if multi-attribute is set for one thing.
 * Throws an exception if the attribute was not present when file was read.
 */
bool TxtFileParser::is_set_multi_attribute( const string & multi_attribute, const string & sub_attribute,
        size_t thingNo ) const {
    bool temp = false;

    // Find the multi-attribute
    map<string, map<string, vector<TxtFileParser::line> > >::const_iterator m_iter =
        multi_attributes_.find(multi_attribute);
    if( m_iter == multi_attributes_.end() ) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Find the sub-attribute
        map<string, vector<TxtFileParser::line> >::const_iterator s_iter = m_iter->second.find( sub_attribute );
        if(s_iter == m_iter->second.end()) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", Sub-Attribute: \"" + sub_attribute
                    + "\" not present during parsing." );
        } else {
            // Make sure the thingNo is not too large
            assert( s_iter->second.size() > thingNo );
            temp = s_iter->second[thingNo].is_set();
            assert( temp == ( s_iter->second[thingNo].lineNo_ > 0 ) );
        }
    }
    return temp;
}


/**
 * Get a numeric multi-attribute.
 * Throws an exception if the attribute was not present when file was read.
 */
template<class T> vector<T> TxtFileParser::get_num_multi_attribute( const string & multi_attribute,
        const string & sub_attribute ) const {
    vector<T> temp;

    // Find the multi-attribute
    map<string, map<string, vector<TxtFileParser::line> > >::const_iterator m_iter =
            multi_attributes_.find( multi_attribute );
    if( m_iter == multi_attributes_.end() ) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Find the sub-attribute
        map<string, vector<TxtFileParser::line> >::const_iterator s_iter = m_iter->second.find( sub_attribute );
        if(s_iter == m_iter->second.end()) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", Sub-Attribute: \"" + sub_attribute
                    + "\" not present during parsing." );
        } else {
            // Get the strings from each line
            temp.reserve(s_iter->second.size());
            vector<TxtFileParser::line>::const_iterator line_iter = s_iter->second.begin();
            while(line_iter != s_iter->second.end()) {
                T line_temp;
                if( !from_string<T>( line_temp, line_iter->get_first_word(), std::dec ) ) {
                    report_error( "Problem reading number " + line_iter->get_first_word(), *line_iter );
                } else {
                    temp.push_back(line_temp);
                }
                line_iter++;
            }
        }
    }
    return temp;
}
template vector<bool> TxtFileParser::get_num_multi_attribute<bool>(
		const string & multi_attribute, const string & sub_attribute ) const;
template vector<int> TxtFileParser::get_num_multi_attribute<int>(
		const string & multi_attribute, const string & sub_attribute ) const;
template vector<unsigned int> TxtFileParser::get_num_multi_attribute<unsigned int>(
		const string & multi_attribute, const string & sub_attribute ) const;
//template vector<size_t> TxtFileParser::get_num_multi_attribute<size_t>(
//		const string & multi_attribute, const string & sub_attribute ) const;
template vector<float> TxtFileParser::get_num_multi_attribute<float>(
		const string & multi_attribute, const string & sub_attribute ) const;

/**
 * Get a numeric multi-attribute for a single thing.
 * Throws an exception if the attribute was not present when file was read.
 */
template<class T> T TxtFileParser::get_num_multi_attribute( const string & multi_attribute,  const string & sub_attribute,
        size_t thingNo ) const {
    T temp;

    // Find the multi-attribute
    map<string, map<string, vector<TxtFileParser::line> > >::const_iterator m_iter =
            multi_attributes_.find( multi_attribute );

    if(m_iter == multi_attributes_.end()) {
        report_error( "Multi-Attribute: \"" + multi_attribute + "\" not present during parsing." );
    } else {
        // Found the attribute now find the sub-attribute
        map<string, vector<TxtFileParser::line> >::const_iterator s_iter = m_iter->second.find( sub_attribute );
        if(s_iter == m_iter->second.end()) {
            report_error( "Multi-Attribute: \"" + multi_attribute + ", Sub-Attribute: \"" + sub_attribute
                    + "\" not present during parsing." );
        } else {
            // Found the sub-attribute now Get the numbered string from each line.
            if(s_iter->second.size() <= thingNo) {
                report_error("Trying to read too large a thingNo");
            } else {
                // Read number from string, if there is an error then report problem.
                if( !from_string<T>(temp, s_iter->second[ thingNo ].get_first_word(), std::dec) ) {
                    report_error( "Problem reading number " + s_iter->second[ thingNo ].get_first_word(),
                            s_iter->second[thingNo] );
                }
            }
        }
    }
    return temp;
}
template bool TxtFileParser::get_num_multi_attribute<bool>(
		const string & multi_attribute, const string & sub_attribute, size_t thingNo ) const;
template int TxtFileParser::get_num_multi_attribute<int>(
		const string & multi_attribute, const string & sub_attribute, size_t thingNo ) const;
template unsigned int TxtFileParser::get_num_multi_attribute<unsigned int>(
		const string & multi_attribute, const string & sub_attribute, size_t thingNo ) const;
template size_t TxtFileParser::get_num_multi_attribute<size_t>(
		const string & multi_attribute, const string & sub_attribute, size_t thingNo ) const;
template float TxtFileParser::get_num_multi_attribute<float>(
		const string & multi_attribute, const string & sub_attribute, size_t thingNo ) const;

/**
 * Read attribute.
 * Returns the last value of the parameter found and returns an empty string if the attribute is not present.
 * Deletes the lines that have the attribute.
 */
TxtFileParser::line TxtFileParser::read_attribute( vector<TxtFileParser::line> & lines,
        const string & attribute,  bool req, string default_value ) const {
    TxtFileParser::line temp(default_value,0);
    vector<TxtFileParser::line>::iterator iter = lines.begin();
    while(iter != lines.end()) {
        if(iter->compare_first_word(attribute)) {
            // The current line starts with the attribute

            // Trim whitespace and attribute
            temp = *iter;
            temp.trim_first_word();

            // Erase the line and it's line number
            iter = lines.erase(iter);
        } else {
            iter++;
        }
    }

    // Check that the string is non-empty if required
    if(req && temp.empty()) {
        if(temp.lineNo_ == 0) {
            // The attribute was missing
            report_error( "Required attribute " + attribute + " missing" );
        } else {
            // The attribute was present, but blank
            report_error( "Required attribute " + attribute + " cannot be blank",temp.lineNo_ );
        }
    }
    return temp;
}

/**
 * Read a numerical attribute.
 * Returns the last value of the parameter found and returns an empty string if the attribute is not present.
 * Deletes the lines that have the attribute.
 */
template <class T> T TxtFileParser::read_numeric_attribute( vector<TxtFileParser::line> & lines,
        const string & attribute,  bool req, T default_value ) const {
    TxtFileParser::line temp_line = read_attribute( lines, attribute, req );
    T temp_T = default_value;

    // Since we made it here, the attribute was either present or not required
    if(!temp_line.empty()) {
        // The argument has some value, so try to read it
        if( from_string<T>(temp_T, temp_line.str_, std::dec) ) {
            // Read succeeded
        } else {
            // Read failed
            report_error( "Invalid argument for attribute :" + attribute );
        }
    }
    return temp_T;
}

/**
 * Read multi-attribute.  Erases the lines that contain the attribute
 */
vector< vector<TxtFileParser::line> > TxtFileParser::read_multi_attribute(
        vector<TxtFileParser::line> & lines, const string & attribute ) const {
    vector< vector<TxtFileParser::line> > temp_multi;
    TxtFileParser::line temp_line( "", 0 );

    vector<TxtFileParser::line>::iterator iter = lines.begin();
    while( iter != lines.end() ) {
        if( iter->compare_first_word( attribute ) ) {
            // The current line starts with the attribute

            // Trim attribute and whitespace
            temp_line = *iter;
            temp_line.trim_first_word();

            // Read number
            size_t multiNo = 0;
            if( from_string<size_t>( multiNo, temp_line.get_first_word(), std::dec ) ) {
                // Read succeeded
                temp_line.trim_first_word();
            } else {
                // Read failed
                report_error( "Missing number after attribute :" + attribute, temp_line.lineNo_ );
            }

            // Make sure that temp_multi is big enough
            if( temp_multi.size() < multiNo ) {
                temp_multi.resize( multiNo );
            }

            // Store value
            temp_multi[ multiNo - 1 ].push_back( temp_line );

            // Erase the line from the main list
            iter = lines.erase( iter );
        } else {
            // The line doesn't start with the attribute
            ++iter;
        }
    }
    return temp_multi;
}

/**
 * Read a series of line objects from the file.
 */
vector< TxtFileParser::line > TxtFileParser::read_lines_from_file( const string file_name ) {
    std::ifstream inFile;

	inFile.open( file_name.c_str(), std::ios::in );

    // Check if there was an error opening the file
    if (!inFile) {
        report_error( "Error: Unable to open file" );
    }

    // Read into an array of lines
    vector< TxtFileParser::line > lines;

    int line_count = 0;
    char c_string_temp[ 256 ];
    while( inFile.good() ) {
        // Increment line counter
        line_count++;
        inFile.getline( c_string_temp, 256 );
        lines.push_back( TxtFileParser::line( string(c_string_temp), line_count ) );
    }

    inFile.close();

    return lines;
}

/**
 * Check if there is an include directive or comment in the line and import it if found.
 */
bool TxtFileParser::import_include_file( vector<TxtFileParser::line>::iterator &iter,
        vector< TxtFileParser::line > &lines ) {

    string::size_type loc = iter->str_.find( "#include " );
    if(loc != string::npos) {
        // There was a command, so include the file listed

        // Make sure command was at start of line
        if(loc != 0) {
            report_error( string( "Syntax Error: #include must be at start of line" ), iter->lineNo_ );
        }

        // TODO (dualta#1#): This will not work with relative filenames. Need to change this
        // Harvest file name
        string included_file = remove_whitespace(iter->str_.substr(8));

        // Open the file
        std::cout << "Reading included file " <<  included_file << " ..." << std::endl;
        std::ifstream inFile;

        inFile.open( included_file.c_str(), std::ios::in );

        // Check if there was an error opening the file
        if (!inFile) {
            report_error( "Error: Unable to open included file (" + included_file + ")" );
        }

        // Read into an array of lines
        vector< TxtFileParser::line > included_lines;

        int line_count = 0;
        char c_string_temp[256];
        while( inFile.good() ) {
            // Increment line counter
            line_count++;
            inFile.getline( c_string_temp, 256 );
            included_lines.push_back( TxtFileParser::line( string( c_string_temp ), line_count ) );
        }
        inFile.close();

        // Erace current line
        iter->str_.clear();

        // Add included lines in place of current line
        lines.insert( iter, included_lines.begin(), included_lines.end() );

        // Restart at the beginning of lines since iter is now unreliable
        iter = lines.begin();

        return true;
    }
    return false;
}

/**
 * Handle combined lines of file
 */
bool TxtFileParser::merge_combined_lines( vector<TxtFileParser::line>::iterator &iter,
        vector< TxtFileParser::line > &lines ) {
    iter->trim(); // Remove initial or trailing white space

    // Check if the line needs to be combined with the next one by looking at the last character in the string
    if( iter->str_.length() > 0 && *( iter->str_.rbegin() ) == '\\' ) {

        // First delete the backslash
        iter->str_.erase( iter->str_.end() - 1 );

        // Check if the next line is valid
        vector<TxtFileParser::line>::iterator next = iter + 1;
        if( next != lines.end() ) {

            // Next is a valid line, so we need to combine the strings
            iter->str_.append( (*next).str_ );

            // Now clear the next string
            lines.erase(next);
        }

        // There might be another \ on the end of the line now, so don't increment the iterator yet.
        return true;
    }
    return false;
}

/**
 * If there is a comment on the current line then erase all of the line after the '#'
 */
bool TxtFileParser::remove_comments( vector<TxtFileParser::line>::iterator &iter ) {
    // Check if there is a comment in the line
    string::size_type loc = iter->str_.find( '#' );
    if( loc != string::npos ) {
        // There is a comment, so erase all of the line after the '#'
        iter->str_.erase( loc );
        return true;
    }
    return false;
}

/**
 * Handle Replacement strings
 */
bool TxtFileParser::replace_strings( vector<TxtFileParser::line>::iterator &iter ) {
    // Check if there are replacements to be made
    string::size_type loc = iter->str_.find( '$' );
    if( loc != string::npos ) {
        // We found at least one '$'

        // Check if the next character is a number
        string::size_type relative_loc = 1;
        size_t replacementNo = 0;
        while( loc + relative_loc < iter->str_.length() && iter->str_.at( loc + relative_loc ) >= '0'
                    && iter->str_.at( loc + relative_loc ) <= '9' ) {
            // The character is a digit, so read
            replacementNo *= 10;
            replacementNo += (int)( iter->str_.at( loc + relative_loc ) - '0');
            relative_loc++;
        }

        // Check if a replacementNo was found
        if( replacementNo <= 0 ) {
            report_error( string( "Syntax Error: $ present without replacement string number", iter->lineNo_ ));
        }

        // Check if there is a default value
        if( loc + relative_loc < iter->str_.length() && iter->str_.at( loc + relative_loc ) == '[' ) {
            // There is a default value, so read it
            string default_value("");
            relative_loc++;
            while( loc + relative_loc < iter->str_.length() && iter->str_.at( loc + relative_loc ) != ']') {
                // The character is a not the end bracket, so read it
                default_value += iter->str_.at( loc + relative_loc );
                relative_loc++;
            }

            // Make sure there was a closed bracket
            if( loc + relative_loc >= iter->str_.length() || iter->str_.at( loc + relative_loc ) != ']' ) {
                report_error("Syntax Error: missing ']'", iter->lineNo_);
            }

            // Check if there was a replacement string given
            if( replacement_strings_.size() >= replacementNo ) {
                iter->str_.replace( loc, relative_loc+1, replacement_strings_[ replacementNo - 1 ],
                        0, replacement_strings_[ replacementNo - 1 ].length() );
            } else {
                // No replacement string given, so use default
                iter->str_.replace( loc, relative_loc + 1, default_value, 0, default_value.length() );
            }
        } else {
            // There is no default value, so a passed in value is required

            // Check if there was a replacement string given
            if(replacement_strings_.size() >= replacementNo) {
                iter->str_.replace( loc, relative_loc + 1, replacement_strings_[ replacementNo - 1 ],
                        0, replacement_strings_[ replacementNo - 1 ].length() );
            } else {
                // No replacement string given, so throw an exception
                report_error( "Error: Required replacement string not given", iter->lineNo_ );
            }
        }
        return true;
    }
    return false;
}

/**
 * Read a list of required and optional multi-attributes from the file
 */
void TxtFileParser::read_multi_attributes_from_file( vector< TxtFileParser::line > & lines,
        const vector<string> & multi_attributes, const map<string, vector<string> > & required_sub_attributes,
        const map<string, vector<string> > & optional_sub_attributes,
        const map<string, map<string,string> > & default_sub_values ) {

    vector< string >::const_iterator iter, multi_iter;
    for( multi_iter = multi_attributes.begin(); multi_iter != multi_attributes.end(); multi_iter++ ) {

        // Add multi_attribute to multi_attributes
        multi_attributes_[ *multi_iter ];

        // Get the pln file lines corresponding to the multi-attribute
        vector< vector< TxtFileParser::line > > temp_lines = read_multi_attribute( lines, *multi_iter );

        // Read required sub attributes (if they exist)
        map< string, vector< string > >::const_iterator rsa_map_iter = required_sub_attributes.find( *multi_iter );
        if( rsa_map_iter != required_sub_attributes.end() ) {
            // There are required sub attributes for this multi-attribute
            for( iter = rsa_map_iter->second.begin(); iter != rsa_map_iter->second.end(); iter++ ) {

                // Make vector
                size_t nThings = temp_lines.size();
                multi_attributes_[ *multi_iter ][ *iter ].reserve( nThings );
                for( size_t iThing = 0; iThing < nThings; iThing++ ) {
                    if( temp_lines[ iThing ].size() > 0 ) {
                        try {
                            multi_attributes_[ *multi_iter ][ *iter ]
                                .push_back( read_attribute( temp_lines[ iThing ], *iter, true ) );
                        } catch ( txtFile_exception &txtEx ) {
                            cerr << "Problem reading attribute " << *multi_iter << " : " << *iter << endl;
                            throw;
                        }
                    } else {
                        // No lines have the current thingNo, so don't check
                        multi_attributes_[ *multi_iter ][ *iter ].push_back( TxtFileParser::line( "", 0 ) );
                    }
                }
            }
        }

        // Read optional attributes (if they exist)
        map<string,vector<string> >::const_iterator osa_map_iter = optional_sub_attributes.find(*multi_iter);
        map<string,map<string,string> >::const_iterator dsv_map_iter = default_sub_values.find(*multi_iter);
        if(osa_map_iter != optional_sub_attributes.end()) {
            for( iter = osa_map_iter->second.begin(); iter != osa_map_iter->second.end(); iter++ ) {

                string default_value = "";

                // Check if there are any default values for this multi argument
                if(dsv_map_iter != default_sub_values.end()) {
                    // Check if there is a default value for the present subvalue
                    map<string,string>::const_iterator dsv_map_iter_iter = dsv_map_iter->second.find(*iter);
                    if(dsv_map_iter_iter != dsv_map_iter->second.end()) {
                        // There is a default value
                        default_value = dsv_map_iter_iter->second;
                    }
                }

                size_t nThings = temp_lines.size();
                multi_attributes_[*multi_iter][*iter].reserve(nThings);
                for(size_t iThing = 0; iThing < nThings; iThing++) {
                    multi_attributes_[ *multi_iter ][ *iter ]
                            .push_back( read_attribute( temp_lines[ iThing ], *iter, false, default_value ) );
                }
            }
        }

        // Check if there were any unparsed lines
        for(size_t iThing = 0; iThing < temp_lines.size(); iThing++) {
            if(temp_lines[ iThing ].size() > 0) {

                report_error( "Unknown attribute found reading " + *multi_iter + ".", temp_lines[iThing].at(0).lineNo_ );
            }
        }
    }
}

/**
 * Print error message and throw exception
 */
void TxtFileParser::report_error( const string & error_text, size_t lineNo ) const {
    if(lineNo > 0) {
        std::cerr << file_name_ << ":" << lineNo << ": " << error_text << std::endl;
    } else {
        std::cerr << file_name_ << ": " << error_text << std::endl;
    }
    throw(txtFile_ex);
}

/**
 * Read the number from the line. The param should have been parsed off first.
 */
template<class T> bool TxtFileParser::line::read_number(T & num) const {
   return from_string<T>(num, get_first_word(), std::dec);
}

/**
 * Read the number from the line and report error if needed. The param should have been parsed off first.
 */
template<class T> T TxtFileParser::line::read_number(const TxtFileParser & txtfile_parser) const {
    T temp;
    if(!from_string<T>(temp, get_first_word(), std::dec)) {
        txtfile_parser.report_error( "Problem reading number "+ get_first_word(), *this);
    }
    return temp;
}
