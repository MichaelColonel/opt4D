/**
 * @file ApertureDoseInfluenceMatrix.cpp
 * ApertureDoseInfluenceMatrix class implementation
 *
 */

#include "ApertureDoseInfluenceMatrix.hpp"


/**
 * constructor: creates empty matrix 
 */
ApertureDoseInfluenceMatrix::ApertureDoseInfluenceMatrix()
  : DoseInfluenceMatrix()
{
}

/**
 * add an aperture to the DoseInfluenceMatrix 
 *
 * @param apNo        The new aperture index
 * @param voxelNos    voxel indices 
 * @param influence   dose contributions to these voxels
 */
void ApertureDoseInfluenceMatrix::add_aperture(unsigned int apNo, vector<unsigned int> voxelNos, vector<float> influence)
{
  assert( apNo == get_nBixels() );

  // number of elements to add
  unsigned int nElements = voxelNos.size();

  // add empty bixel (does also reserve space)
  add_empty_bixel(nElements);

  // now add the data
  add_Dij_elements_to_bixel(apNo,voxelNos,influence);
}

/**
 * initialize
 *
 * @param nEntries    number of elements to reserve memory for
 * @param nVoxels     number of voxels
 */
void ApertureDoseInfluenceMatrix::initialize(unsigned int nVoxels, unsigned int nEntries)
{
  // set number of voxels
  set_nVoxels(nVoxels);

  // set number of voxels
  set_nBixels(0);

  // reserve memory for bixel start index
  // bixel_start_index_.reserve(nApertures+1);
  // bixel_nEntries_.reserve(nApertures);

  // reserve memory for the entries
  reserve_Dij(nEntries);
}




