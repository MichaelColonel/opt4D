#ifndef TXTFILEPARSER_HPP
#define TXTFILEPARSER_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <map>
using std::map;

#include "StringFunctions.hpp"

#define DEFAULT_FILE_VERSION_NUMBER "0.2"

/**
 * Class txtFile_exception is thrown when there is an error reading a configuration text file.
 */
class txtFile_exception : public std::exception {
    virtual const char* what() const throw() {
        return "Failed while reading file.";
    }
};


/**
 * The TxtfileParser class allows specially formatted ascii text files to be read in.
 * The files consist of property value pairs each on a separate line of the file.
 *
 * This class is used for reading .pln files and .mpf files.
 */
class TxtFileParser {
  public:

    class line; // Line of the file

    /**
     * Constructor
     * Including string replacement
     */
    TxtFileParser( const string &file_name,  const vector< string > & replacement_strings,
            const vector< string > & required_attributes,  const vector< string > & optional_attributes,
            const map< string, string > & default_values,  const vector< string > & multi_attributes,
            const map< string, vector< string > > &  required_sub_attributes,
            const map< string, vector< string > > &  optional_sub_attributes,
            const map< string, map< string, string > > &  default_sub_values,
            const string &fileVer=DEFAULT_FILE_VERSION_NUMBER );

    /**
     * Constructor
     * With no string replacement
     */
    TxtFileParser( const string &file_name,  const vector<string> & required_attributes,
            const vector<string> & optional_attributes,  const map<string, string> & default_values,
            const vector<string> & multi_attributes, const map<string, vector<string> > &  required_sub_attributes,
            const map<string, vector<string> > &  optional_sub_attributes,
            const map<string, map<string,string> > &  default_sub_values,
            const string &fileVer=DEFAULT_FILE_VERSION_NUMBER );

    /**
     * TxtFileParser::line Class honds a line from the file with the original lineNo
     */
    class line {
      public:
        line(const string & str="", size_t lineNo=0)
                : str_(str), lineNo_(lineNo) {};

        // Data accessors
        bool is_set() const;
        bool empty() const;
        bool compare_first_word(const string & word) const;
        string get_first_word() const;

        template<class T> bool read_number(T & num) const;

        template<class T> T read_number(const TxtFileParser & txt_parser) const;

        // Manipulators
        void trim();
        void trim_first_word();

        // Data
        string str_;
        size_t lineNo_;
    };


    /// Find out if an attribute is set
    bool is_set_attribute( const string & attribute ) const;

    /// Get a string attribute
    string get_attribute( const string & attribute ) const;

    /// Get a numeric attribute
    template<class T>  T get_num_attribute( const string & attribute ) const;

    /// Find out if multi-attribute is set for a thing
    bool is_set_multi_attribute( const string & multi_attribute,  const string & sub_attribute,
            size_t thingNo ) const;

    /// Find out if multi-attribute is set for each object
    vector<bool> is_set_multi_attribute( const string & multi_attribute,  const string & sub_attribute ) const;

    /// Get multi-attribute nThings
    size_t get_multi_attribute_nThings( const string & multi_attribute ) const;

    /// Get string multi-attribute
    vector<string> get_multi_attribute( const string & multi_attribute,  const string & sub_attribute ) const;

    /// Get single value of a string multi-attribute
    string get_multi_attribute( const string & multi_attribute,  const string & sub_attribute,
            size_t thingNo ) const;

    /// Get a numeric multi-attribute
    template<class T>  vector<T> get_num_multi_attribute( const string & multi_attribute,
            const string & sub_attribute ) const;

    /// Get a single value for a numeric multi-attribute
    template<class T>  T get_num_multi_attribute( const string & multi_attribute,  const string & sub_attribute,
            size_t thingNo ) const;

    /*
     * Other functions
     */

    // Error reporting
    void report_error( const string & error_text,  size_t lineNo=0 ) const;
    void report_error( const string & error_text,  const TxtFileParser::line & line ) const;

  protected:

  private:

    // Hide the default constructor, so that constructor parameters are required.
    TxtFileParser() {};

    /// Read in lines form the file and process information for object.
    /// Called from different constructors to build objects
    void readFile( const vector<string> & required_attributes,
        const vector<string> & optional_attributes,  const map<string, string> & default_values,
        const vector<string> & multi_attributes, const map<string, vector<string> > & required_sub_attributes,
        const map<string, vector<string> > & optional_sub_attributes,
        const map<string, map<string,string> > & default_sub_values,
        const string &fileVer,  bool doStringReplacement );

    /// Check if there is an include directive or comment in the line and import it if found
    bool import_include_file( vector<TxtFileParser::line>::iterator &iter,
            vector< TxtFileParser::line > &lines );

    /// Handle combined lines of file
    bool merge_combined_lines( vector<TxtFileParser::line>::iterator &iter,
            vector< TxtFileParser::line > &lines );

    /// If there is a comment on the current line then erase all of the line after the '#'
    bool remove_comments( vector<TxtFileParser::line>::iterator &iter );

    /// Read a list of required and optional multi-attributes from the file
    void read_multi_attributes_from_file( vector< TxtFileParser::line > & lines,
        const vector<string> & multi_attributes, const map<string, vector<string> > & required_sub_attributes,
        const map<string, vector<string> > & optional_sub_attributes,
        const map<string, map<string,string> > & default_sub_values );

    /// Handle Replacement strings
    bool replace_strings( vector<TxtFileParser::line>::iterator &iter );

    /// Read normal attributes
    TxtFileParser::line read_attribute( vector<TxtFileParser::line> & lines,  const string & attribute,
            bool req = false, string default_value = "" ) const;

    /// Read attributes such as VOI and OBJECTIVE
    vector< vector<TxtFileParser::line> > read_multi_attribute( vector<TxtFileParser::line> & lines,
            const string & attribute ) const;

    /// Read numeric attribute
    template <class T>  T read_numeric_attribute( vector<TxtFileParser::line> & lines,  const string & attribute,
            bool req, T default_value ) const;

    vector< TxtFileParser::line > read_lines_from_file( const string file_name );

    // Data Members

    /// The file that was read
    const string file_name_;

    /// Holds ordinary attributes
    map<string, TxtFileParser::line> attributes_;

    /// Holds multi-attributes
    map<string, map<string, vector< TxtFileParser::line > > > multi_attributes_;

    /// Values of optional paramaters passed on command line
    vector<string> replacement_strings_;

};


/*
 * TxtFileParser inline functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 */


/**
 * Print error message and throw exception
 */
inline void TxtFileParser::report_error( const string & error_text,  const TxtFileParser::line & line ) const {
    report_error( error_text, line.lineNo_ );
}


/*
 * TxtFileParser::line inline functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/**
 * Trim whitespace at the beginning or end of the line
 */
inline void TxtFileParser::line::trim() {
    str_ = remove_whitespace(str_);
}

/**
 * Check if the line is empty
 */
inline bool TxtFileParser::line::empty() const {
    return str_.empty();
}

/**
 * Check if the line is set
 */
inline bool TxtFileParser::line::is_set() const {
    return lineNo_ > 0;
}

/**
 * Compare a string to the first word of the line (case insensitive)
 */
inline bool TxtFileParser::line::compare_first_word( const string & word ) const {
    if ( str_.length() == word.length() ) {
        if ( to_lower(str_) == to_lower(word) ) {
            // The line is exactly the word
            return true;
        }
    } else if ( str_.length() > word.length() ) {
        if ( to_lower( str_.substr(0,word.length()) ) == to_lower(word) ) {
            // The line starts with the word, make sure the next character is whitespace
            char temp = str_[word.length()];
            if ( temp == ' ' || temp == '\t' ) {
                return true;
            }
        }
    }
    return false;
}

/**
 * Get the first word from the line.
 */
inline string TxtFileParser::line::get_first_word() const {
    string temp;
    string::const_iterator iter = str_.begin();
    while ( iter != str_.end() && *iter != ' ' && *iter != '\t' ) {
        temp.push_back(*iter);
        iter++;
    }
    return temp;
}

/**
 * Remove the first word and any whitespace up to the next word
 */
inline void TxtFileParser::line::trim_first_word() {
    string::iterator iter = str_.begin();
    // Trim non-whitespace at the beginning of the string
    while ( iter != str_.end() && *iter != ' ' && *iter != '\t' ) {
        iter = str_.erase(iter);
    }
    // Trim white space at the beginning of the string
    while ( iter != str_.end() && (*iter == ' ' || *iter == '\t') ) {
        iter = str_.erase(iter);
    }
}

#endif // TXTFILEPARSER_HPP
