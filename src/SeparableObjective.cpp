/**
 * @file: SeparableObjective.cpp
 * SeparableObjective Class implementation.
 *
 * $Id: SeparableObjective.cpp,v 1.7 2008/03/24 17:05:00 bmartin Exp $
 */

#include "SeparableObjective.hpp"

/**
 * Constructor: Initialize variables.  By default, there are no constraints.
 */
SeparableObjective::SeparableObjective(
        Voi*  the_voi,
        unsigned int objNo,
        float weight,
        float sampling_fraction,
        bool sample_with_replacement)
: Objective(objNo, weight),
    the_voi_(the_voi),
    sampling_fraction_(sampling_fraction),
    sample_with_replacement_(sample_with_replacement)
{
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
SeparableObjective::~SeparableObjective()
{
}


/**
 * Initialize the objective.
 * Does nothing for SeparableObjective except set is_initialized_ to true.
 */
void SeparableObjective::initialize(
    DoseInfluenceMatrix& Dij)
{
  // sampling_fraction_ = the_voi_->get_sampling_fraction();
  is_initialized_ = true;
}


/**
 * Calculate the objective for a given dose distribution
 */
double SeparableObjective::calculate_dose_objective(
    const DoseVector & the_dose)
{

    double objective = 0;
    size_t nVoxels = the_voi_->get_nVoxels();

    // loop over all voxels in this VOI and look up dose deviation
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
        float dose_buf = the_dose.get_voxel_dose(the_voi_->get_voxel(iVoxel));
        objective += calculate_voxel_objective(dose_buf, iVoxel, nVoxels);
    }

    return objective;
}


/**
 * Calculate the objective for a given dose distribution
 */
double SeparableObjective::calculate_dose_objective_and_gradient(const DoseVector & the_dose,
																 DoseVector & dose_gradient)
{

    double objective = 0;
    size_t nVoxels = the_voi_->get_nVoxels();

    // loop over all voxels in this VOI and look up dose deviation
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
        float dose_buf = the_dose.get_voxel_dose(voxelNo);
        objective += calculate_voxel_objective(dose_buf, iVoxel, nVoxels);
		dose_gradient[voxelNo] += calculate_dvoxel_objective_by_dose(dose_buf, iVoxel, nVoxels);
    }

    return objective;
}

/**
 * Calculate or Estimate objective from BixelVector and DoseInfluenceMatrix
 */
double SeparableObjective::calculate_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{

    double objective = 0;
    double total_squared_voxel_objective = 0;
    int temp_sampling_threshold; 
    if(sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = use_voxel_sampling ? 
        ((float) nVoxels) * ((float) temp_sampling_threshold)
        / ((float) RAND_MAX): nVoxels;

    // loop over all voxels in this VOI and look up dose deviation
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
        if(use_voxel_sampling && std::rand() > temp_sampling_threshold) {
            continue;
        }
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
        float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
        double voxel_objective = calculate_voxel_objective(dose_buf, voxelNo, expected_nVoxels);

        objective += voxel_objective;
        total_squared_voxel_objective += voxel_objective * voxel_objective;
    }

    // Update SSVO if needed
    estimated_ssvo = use_voxel_sampling ? 
        (total_squared_voxel_objective / sampling_fraction_) 
        : total_squared_voxel_objective;

    return objective;
}



/**
 * Calculate or Estimate the objective and the gradient
 */
double SeparableObjective::calculate_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{

    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;
    double total_squared_voxel_objective = 0;

    // Find sampling threshold to make sampling faster
    int temp_sampling_threshold; 
    if(sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = use_voxel_sampling
        ?  ((float) nVoxels) * ((float) temp_sampling_threshold)
        / ((float) RAND_MAX)
        : nVoxels;
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {

        // Determine whether to skip this voxel
        if(use_voxel_sampling && std::rand() > temp_sampling_threshold) {
            continue;
        }

        // Find the voxelNo of the voxel we are looking at
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

        // Calculate dose to voxel
        float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);

        // Calculate contribution to objective and gradient
        double voxel_objective = calculate_voxel_objective(
	           dose_buf, voxelNo, expected_nVoxels);
	// if(voxel_objective > 0) { JU, Mar 25, 2014
            objective += voxel_objective;
            total_squared_voxel_objective += voxel_objective * voxel_objective;

            // Calculate contribution to gradient
            double dObjective_by_dose = calculate_dvoxel_objective_by_dose(
		    dose_buf, voxelNo, expected_nVoxels);
            Dij.calculate_voxel_dose_gradient(
                    voxelNo,
                    gradient,
                    dObjective_by_dose * gradient_multiplier);
	// }
    }

    // Normalize estimated SSVO if needed
    estimated_ssvo = use_voxel_sampling ?
        total_squared_voxel_objective / sampling_fraction_
        : total_squared_voxel_objective;

    return objective;
}

        
/**
 * Calculate objective and gradient and Hessian times a vector for a separable 
 * objective.
 *
 * @param beam_weights The beam weights to use when calculating the dose
 * @param Dij The dose influence matrix to use
 * @param gradient The gradient is added to this vector.  Does not clear the 
 * vector.
 * @param v The vector to multiply with the hessian matrix.
 * @param Hv The place to put the result of the Hessian.  Does not clear the 
 * vector.
 * @param use_voxel_sampling Should the voxels be sampled?
 * @param estimated_ssvo Place to return the estimated sum of squared voxel 
 * objectives.
 */
double SeparableObjective::calculate_objective_and_gradient_and_Hv(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        const vector<float> & v,
        vector<float> & Hv,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{

    // Make sure the inputs match
    size_t nBixels = beam_weights.get_nBixels();
    assert(nBixels == gradient.get_nBixels());
    assert(v.size() == nBixels);
    assert(Hv.size() == nBixels);

    double objective = 0;
    double total_squared_voxel_objective = 0;

    // Find sampling threshold to make sampling more efficient
    int temp_sampling_threshold; 
    if(sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = use_voxel_sampling ?
        ((float) nVoxels) * ((float) temp_sampling_threshold)
        / ((float) RAND_MAX)
        : nVoxels;
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
        // Determine whether to skip voxel
        if(use_voxel_sampling && std::rand() <= temp_sampling_threshold) {
            continue;
        }

        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

        // Calculate dose to voxel
        float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);

        // Calculate contribution to objective and gradient
        double voxel_objective = calculate_voxel_objective(
		    dose_buf, voxelNo, expected_nVoxels);
        // if(voxel_objective > 0) { JU, Mar 25, 2014
            objective += voxel_objective;

            total_squared_voxel_objective += voxel_objective
                * voxel_objective;

            // Calculate Contribution to gradient
            double dObjective_by_dose = calculate_dvoxel_objective_by_dose(
		    dose_buf, voxelNo, expected_nVoxels);
            Dij.calculate_voxel_dose_gradient(
                    voxelNo,
                    gradient,
                    dObjective_by_dose * gradient_multiplier);

            // Calculate Contribution to Hv
            double d2Objective_by_dose2 = calculate_d2voxel_objective_by_dose(
		    dose_buf, voxelNo, expected_nVoxels);
            double z_i = 0;
            for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
                    iter.get_voxelNo() == voxelNo; iter++) {
                z_i += v[iter.get_bixelNo()] * iter.get_influence();
            }
            z_i *= d2Objective_by_dose2;
            for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
                    iter.get_voxelNo() == voxelNo; iter++) {
                Hv[iter.get_bixelNo()] +=
                    gradient_multiplier * z_i * iter.get_influence();
            }
	// }
    }

    // Scale estimated SSVO if needed
    estimated_ssvo = use_voxel_sampling
        ?  total_squared_voxel_objective / sampling_fraction_
        : total_squared_voxel_objective;

    return objective;
}


/**
 * Estimate the objective and the gradient 
 * for one instance of an uncertain treatment by sampling voxels.
 */
double SeparableObjective::calculate_scenario_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        BixelVectorDirection & gradient, 
        float gradient_multiplier,
        const DoseDeliveryModel &dose_model,
        const RandomScenario &random_scenario,
	bool use_voxel_sampling,
        double &estimated_ssvo)
{

    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;
    double total_squared_voxel_objective = 0;

    int temp_sampling_threshold; 
    if(use_voxel_sampling && sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = ((float) nVoxels)
        * ((float) temp_sampling_threshold) / ((float) RAND_MAX);

    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {

	// ask if voxel sampling should be applied
	if(use_voxel_sampling) {
	    // Determine whether to skip this voxel
	    if(std::rand() > temp_sampling_threshold) {
		continue;
	    }
	}

        // Find the voxelNo of the voxel we are looking at
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

        // Calculate dose to voxel
        float dose_buf = dose_model.calculate_voxel_dose(voxelNo, 
                beam_weights, dij, random_scenario); 

        // Calculate contribution to objective and gradient
        double voxel_objective = calculate_voxel_objective(
		     dose_buf, voxelNo, expected_nVoxels);
        // if(voxel_objective > 0) { JU, Mar 25, 2014
            objective += voxel_objective;

            total_squared_voxel_objective += voxel_objective
                * voxel_objective;

            // Calculate contribution to gradient
            double dObjective_by_dose = calculate_dvoxel_objective_by_dose(
		    dose_buf, voxelNo, expected_nVoxels);

            // calculate gradient of voxel dose with respect to beam weights
            // and add to gradient
            dose_model.calculate_voxel_dose_gradient(
                    voxelNo, 
		    beam_weights,
                    dij,
                    random_scenario,
                    gradient,
                    dObjective_by_dose * gradient_multiplier
                    );
	// }
    }   

    // Store estimated SSVO if needed
    estimated_ssvo = use_voxel_sampling
        ? total_squared_voxel_objective / sampling_fraction_
        : total_squared_voxel_objective;

    return objective;
}


/**
 * Calculate or Estimate the objective for one instance of an uncertain 
 * treatment by sampling voxels.
 */
double SeparableObjective::calculate_scenario_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        const DoseDeliveryModel & dose_model,
        const RandomScenario & random_scenario,
	bool use_voxel_sampling,
        double &estimated_ssvo)
{

    // Voxel_Subscripts voxelSub;

    double objective = 0;
    double total_squared_voxel_objective = 0;

    int temp_sampling_threshold; 
    if(use_voxel_sampling && sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = ((float) nVoxels)
        * ((float) temp_sampling_threshold) / ((float) RAND_MAX);
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {

	// ask if voxel sampling should be applied
	if(use_voxel_sampling) {
	    // Determine whether to skip this voxel
	    if(std::rand() > temp_sampling_threshold) {
		continue;
	    }
	}

        // Find the voxelNo of the voxel we are looking at
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

        // Calculate dose to voxel
        float dose_buf = dose_model.calculate_voxel_dose(voxelNo, 
                beam_weights, dij, random_scenario); 

        // Calculate contribution to objective
        double voxel_objective = calculate_voxel_objective(
		     dose_buf, voxelNo, expected_nVoxels);
        // if(voxel_objective > 0) { JU, Mar 25, 2014
            objective += voxel_objective;
            total_squared_voxel_objective += voxel_objective * voxel_objective;
	// }
    }   

    // Scale estimated SSVO if needed
    estimated_ssvo = total_squared_voxel_objective;
    if(use_voxel_sampling) {
        estimated_ssvo /= sampling_fraction_;
    }

    return objective;
}


/// Find out how many voxels were in objective
unsigned int SeparableObjective::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}

/// Find out voxel sampling rate
float SeparableObjective::get_sampling_fraction() const
{
  return sampling_fraction_;
}

/// Set voxel sampling rate
void SeparableObjective::set_sampling_fraction(float sampling_fraction)
{
  sampling_fraction_= sampling_fraction;
}
