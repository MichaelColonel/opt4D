/**
 * @file: GenericConstraint.cpp
 * GenericConstraint Class implementation.
 *
 */

#include "GenericConstraint.hpp"
#include <iostream>
using std::cout;
using std::endl;

GenericConstraint::GenericConstraint()
    : nNonZeros_(0)
    , index_(NULL)
    , value_(NULL)
    , upper_bound_(0)
    , lower_bound_(0)
    , type_(FREE)
{
  cout << "GenericConstraint default constructor" << endl;
}

GenericConstraint::GenericConstraint(unsigned int nNonZeros)
    : nNonZeros_(nNonZeros)
    , index_(NULL)
    , value_(NULL)
    , upper_bound_(0)
    , lower_bound_(0)
    , type_(FREE)
{
    index_ = new int[nNonZeros_];
    value_ = new double[nNonZeros_];
}


GenericConstraint::~GenericConstraint()
{
  if(index_!=NULL) {
    delete [] index_;
    index_ = NULL;
  }
  if(value_!=NULL) {
    delete [] value_;
    value_ = NULL;
  }
}


void GenericConstraint::initialize(unsigned int nNonZeros)
{
    nNonZeros_ = nNonZeros;    
    
    index_ = new int[nNonZeros_];
    value_ = new double[nNonZeros_];
}


//
// GenericConstraintSet
//

GenericOptimizationData::GenericOptimizationData()
    : nVarInt_(0)
    , nVarAux_(0)
    , nVar_(0)
    , upper_var_bound_(NULL)
    , lower_var_bound_(NULL)
    , var_bound_type_(NULL)
    , nConstraints_(0)
    , constraints_()
    , nNonZeros_(0)
    , obj_coeffs_(NULL)
    , nConstraints_until_now_(0)
    , nAuxiliaries_until_now_(0)
{
    cout << "calling GenericOptimizationData Default Constructor" << endl;
}

GenericOptimizationData::GenericOptimizationData(unsigned int nVarInt,
						 unsigned int nVarAux,
						 unsigned int nConstraints)
    : nVarInt_(nVarInt)
    , nVarAux_(nVarAux)
    , nVar_(nVarAux+nVarInt)
    , upper_var_bound_(NULL)
    , lower_var_bound_(NULL)
    , var_bound_type_(NULL)
    , nConstraints_(nConstraints)
    , constraints_()
    , nNonZeros_(0)
    , obj_coeffs_(NULL)
    , nConstraints_until_now_(0)
    , nAuxiliaries_until_now_(0)
{
    upper_var_bound_ = new double[nVar_];
    lower_var_bound_ = new double[nVar_];
    var_bound_type_ = new BoundType[nVar_];
    obj_coeffs_ = new double[nVar_];

    constraints_.resize(nConstraints_);
}

void GenericOptimizationData::initialize(unsigned int nVarInt,
					 unsigned int nVarAux,
					 unsigned int nConstraints)
{
    cout << "initializing GenericOptimizationData..." << endl;

    nVarInt_ = nVarInt;
    nVarAux_ = nVarAux;
    nVar_ = nVarInt_ +nVarAux_; 
    nConstraints_ = nConstraints;
    nNonZeros_ = 0;
    nConstraints_until_now_ = 0;
    nAuxiliaries_until_now_ = 0;

    upper_var_bound_ = new double[nVar_];
    lower_var_bound_ = new double[nVar_];
    var_bound_type_ = new BoundType[nVar_];
    obj_coeffs_ = new double[nVar_];

    // initialize variable bounds as if they were beamlet intensities
    for(unsigned int i=0; i< nVar_; i++) {
      upper_var_bound_[i] = OPT4D_INFINITY;
      lower_var_bound_[i] = 0;
      var_bound_type_[i] = LOWERBOUND;
      obj_coeffs_[i] = 0;
    }
    
    constraints_.resize(nConstraints_);
}


GenericOptimizationData::~GenericOptimizationData()
{
    cout << "calling GenericOptimizationData Destructor" << endl;

    if(upper_var_bound_!=NULL) {
      delete [] upper_var_bound_;
      upper_var_bound_ = NULL;
    }
    if(lower_var_bound_!=NULL) {
      delete [] lower_var_bound_;
      lower_var_bound_ = NULL;
    }
    if(var_bound_type_!=NULL) {
      delete [] var_bound_type_;
      var_bound_type_ = NULL;
    }
    if(obj_coeffs_!=NULL) {
      delete [] obj_coeffs_;
      obj_coeffs_ = NULL;
    }
}




