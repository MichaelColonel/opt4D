#include "TumorEUDObjective.hpp"

/**
 * Constructor: Initialize variables.  
 */
TumorEUDObjective::TumorEUDObjective(
		Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement)
  : NonSeparableObjective(the_voi, objNo, weight,sampling_fraction, sample_with_replacement)
  , epsilon_(1e-100)
{
  // cout << "calling TumorEUDObjective constructor" << endl;

  // get EUD parameter from Voi
  alpha_ = the_voi->get_alpha();
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
TumorEUDObjective::~TumorEUDObjective()
{
}


/**
 * Initialize the objective.  
 */
void TumorEUDObjective::initialize(
    DoseInfluenceMatrix& Dij)
{
  is_initialized_ = true;
}



/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void TumorEUDObjective::printOn(std::ostream& o) const
{
  o << "OBJ " << get_objNo() << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  o << "\tmaximize Niemierko EUD, alpha = " << alpha_;
}

/**
 * Calculate the objective from some voxels with the given doses.
 *
 * @param dose The vector of voxel doses.
 */
double TumorEUDObjective::calculate_objective(
	const vector<float> & dose) const
{
    unsigned int nVoxels = dose.size();
    double nVoxels_f = static_cast<double>(nVoxels);
    vector<float>::const_iterator iter;

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }

    double tmp = 0;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
      tmp += exp( -1*alpha_*(*iter) )/nVoxels_f; 
    }

    if(tmp>epsilon_) {
      return (weight_*-1*log(tmp)/alpha_);
    }
    else {
      cout << "WARNING: calculate_objective returning zero EUD" << endl;
      return 0;
    }
}


/**
 * Calculate the objective and partial derivative of the voxel contribution to
 * the objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 */
double TumorEUDObjective::calculate_objective_and_d(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose) const
{
    int nVoxels = dose.size();
    double nVoxels_f = static_cast<double>(nVoxels);
    vector<float>::const_iterator iter;

    // Resize return vector
    d_obj_by_dose.resize(nVoxels,0);

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }

    // mean cell survival
    double tmp = 0;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
      tmp += exp( -1*alpha_*(*iter) )/nVoxels_f; 
    }

    if(tmp<epsilon_) {
      cout << "WARNING: calculate_objective_and_d returning zero EUD" << endl;
      return 0;
    }

    // screen output
    cout << "EUD for Voi " << the_voi_->get_voiNo() << ": " << -1*log(tmp)/alpha_ <<endl;

    // Fill return vector
    iter = dose.begin();
    vector<float>::iterator return_iter = d_obj_by_dose.begin();
    while(iter != dose.end()) {
      *return_iter = (float) ( weight_*exp( -1*alpha_*(*iter) ) / (nVoxels_f * tmp) ); 
      iter++;
      return_iter++;
    }
    
    return weight_*(-1*log(tmp)/alpha_);
}



double TumorEUDObjective::calculate_objective_and_d2(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose,
	vector<float> & d2_obj_by_dose) const
{
  std::cerr << "TumorEUDObjective::calculate_objective_and_d2"
	    << "\nis not implemented yet." << std::endl;
  exit(-1);
}








