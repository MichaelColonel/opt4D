/**
 * The model assumes that several treatment plans are simultaneously optimized for one patient -
 * The idea is to maximize the biological effect in the target by non-uniform fraction doses
 * in the target voxels. Hence an inhomogeneous dose distributions is delivered to the target
 * in a single fraction which is compensated for in other fractions.
 *
 * The model allows for uncertainty in the alpha/beta ratio
 */

class BiologicallyEquivalentDoseModel;


#include "DoseDeliveryModel.hpp"


class BiologicallyEquivalentDoseModel : public DoseDeliveryModel
{

  public:
    /// constructor
    BiologicallyEquivalentDoseModel(const Geometry *the_geometry,
									const DoseDeliveryModel::options *options,
									vector< Voi* > the_vois,
									unsigned int nInstances,
									unsigned int nBeams_per_instance,
									unsigned int nFractions,
									float standard_fraction_dose);

    // Virtual destructor
    virtual ~BiologicallyEquivalentDoseModel();

    // get number of plans
    virtual unsigned int get_nInstances() const;

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("BiologicallyEquivalentDoseModel"); }

    /// generate a random sample scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const;

    /// generate the nominal scenario
    virtual void generate_nominal_scenario(
            RandomScenario &random_scenario) const;

    /// calculate biologically equivalent voxel dose for a scenario
    virtual float calculate_voxel_dose(unsigned int voxelNo,
				       const BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

    /// calculate instance dose
    virtual void calculate_instance_dose(DoseVector &dose_vector,
					 const BixelVector &bixel_grid,
					 DoseInfluenceMatrix &dij,
					 unsigned int iInstance) const;

    /// calculate instance doses for list of voxels provided
  virtual void calculate_instance_doses(vector<DoseVector> &instance_doses,
					const BixelVector &bixel_grid,
					DoseInfluenceMatrix &dij,
					vector<unsigned int> voxelNos) const;


    /// write results
    virtual void write_dose(BixelVector &bixel_grid,
			    DoseInfluenceMatrix &dij,
			    string file_prefix,
			    Dvh dvh) const;

  private:

    /// the VOIs
    vector< Voi* > the_vois_;

    /// number of instances (plans to be optimized)
    unsigned int nInstances_;

    /// instance number for each bixel
    vector<unsigned int> bixels_instanceNo_;

    /// voi index for every voxel
    vector<unsigned int> voxels_voiNo_;

    /// number of fractions
    unsigned int nFractions_;

    /// standard fraction dose
    float standard_fraction_dose_;

    /// set voi indices for all voxels
    void set_voi_indices_for_every_voxel();

    /// calculate the physical dose to a voxel for one instance
    float calculate_physical_voxel_dose(unsigned int voxelNo,
					unsigned int instanceNo,
					const BixelVector &bixel_grid,
					DoseInfluenceMatrix &dij) const;

  /// calculate the physical dose to a voxel for all instances
  vector<float> calculate_physical_voxel_doses(unsigned int voxelNo,	
					       const BixelVector &bixel_grid,                  
					       DoseInfluenceMatrix &dij) const;


};


inline
unsigned int BiologicallyEquivalentDoseModel::get_nInstances() const
{
    return nInstances_;
}
