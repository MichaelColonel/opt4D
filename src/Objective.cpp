/**
 * @file: Objective.cpp
 * Objective Class implementation.
 */

#include "Objective.hpp"

/**
 * Constructor: Initialize variables.  By default, there are no constraints.
 */
Objective::Objective(unsigned int objNo, float weight)
  : weight_(weight)
  , is_initialized_(false)
  , objNo_(objNo)
{
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
Objective::~Objective()
{
}


/// Find out how many voxels were in objective if appropriate, otherwise 0
unsigned int Objective::get_nVoxels() const
{
  return 0;
}

/// Find out voxel sampling rate if appropriate, otherwise 1
float Objective::get_sampling_fraction() const
{
  return 1.f;
}

/// returns total number of constraints
unsigned int Objective::get_nGeneric_constraints() const
{
    cout << "WARNING: called Objective::get_nGeneric_constraints()" << endl;
    return(0);
}

/// returns total number of auxiliary variables
unsigned int Objective::get_nAuxiliary_variables() const
{
    cout << "WARNING: called Objective::get_nAuxiliary_variables()" << endl;
    return(0);
}

/// Set voxel sampling rate if appropriate, otherwise do nothing
void Objective::set_sampling_fraction(float sampling_fraction)
{
}

// 
// these functions have to be implemented in the derived class or 
// may not be called
//

void Objective::add_to_optimization_data_set(GenericOptimizationData & data, 
					     DoseInfluenceMatrix & Dij) const
{
    cout << "Called Objective base class's "
       << "add_to_optimization_data_set\n"
       << "The derived objective may not support the external solver interface"
       << endl;
    exit(-1);
}


double Objective::calculate_objective_and_gradient(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    cout << "Called Objective base class's "
       << "calculate_objective_and_gradient\n"
       << "The derived objective may not support gradient descent"
       << endl;
    exit(-1);
}

double Objective::calculate_objective_and_gradient_and_Hv(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        const vector<float> & v,
        vector<float> & Hv,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    cout << "Called Objective base class's "
       << "calculate_objective_and_gradient_and_Hv\n"
       << "It is not supported by the derived class."
       << endl;
    exit(-1);
}

double Objective::calculate_scenario_objective(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    const DoseDeliveryModel &uncertainty_model,
    const RandomScenario &random_scenario,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    cout << "called Objective base class's calculate_scenario_objective\n"
        << "The derived objective may not support uncertainty sampling stuff"
        << endl;
    exit(-1);
}

double Objective::calculate_scenario_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        BixelVectorDirection & gradient, 
        float gradient_multiplier,
        const DoseDeliveryModel &uncertainty_model,
        const RandomScenario &random_scenario,
        bool use_voxel_sampling,
        double &estimated_ssvo
        )
{
    cout << "Called Objective base class's "
       << "calculate_scenario_objective_and_gradient\n"
       << "The derived objective may not support uncertainty sampling stuff"
       << endl;
    exit(-1);
}


double Objective::calculate_scenario_objective_and_gradient_and_Hv(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        BixelVectorDirection & gradient, 
        float gradient_multiplier,
        const vector<float> & v,
        vector<float> & Hv,
        const DoseDeliveryModel &uncertainty_model,
        const RandomScenario &random_scenario,
        bool use_voxel_sampling,
        double &estimated_ssvo
        )
{
    cout << "Called Objective base class's "
       << "calculate_scenario_objective_and_gradient_and_Hv\n"
       << "The derived objective may not support uncertainty sampling stuff"
       << endl;
    exit(-1);
}

double Objective::calculate_dose_objective_and_gradient(const DoseVector & the_dose,
														DoseVector & dose_gradient)
{
    cout << "Objective::calculate_dose_objective_and_gradient" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}




