/**
 * @file: CellSurvivalObjective.cpp
 * CellSurvivalObjective Class implementation.
 *
 */

#include "CellSurvivalObjective.hpp"
#include "DoseParser.hpp"


/**
 * Constructor: Initialize variables, read clonogen density from file
 *
 * @param the_voi The Volume of intrestest that the objective acts on
 * @param objNo The number of this objective
 * @param file_name The file containing the cell density
 * @param maximum_cell_density The absolute number of cells per voxel at the maximum
 * @param nFractions The number of fractions
 * @param uncertainty_type The type of parameter uncertainty in the LQ model
 * @param weight The overall weight for this objective
 * @param nVoxels The total number of voxels in the dose vector
 */
CellSurvivalObjective::CellSurvivalObjective(Voi*  the_voi,
												   unsigned int objNo,
												   string file_name,
												   float maximum_cell_density,
												   unsigned int nFractions,
												   string uncertainty_type,
												   float weight,
												   unsigned int nVoxels)
  : Objective(objNo, weight),
    the_voi_(the_voi),
	nFractions_(nFractions),
	lq_uncertainty_(NULL)
{

  // construct LQ uncertainty model
  if (uncertainty_type == "none") 
	{
	  lq_uncertainty_.reset(new LQModelNominal(the_voi_->get_alpha(),
											   the_voi_->get_betaalpha(),
											   maximum_cell_density,
											   file_name,
											   nVoxels));
	}
  else if (uncertainty_type == "alpha") 
	{
	  lq_uncertainty_.reset(new LQModelAlphaUncertainty(the_voi_->get_alpha(),
														10*the_voi_->get_alpha(),
														10,
														the_voi_->get_betaalpha(),
														maximum_cell_density,
														file_name,
														nVoxels));
	}
  else {
	string temp = "unknown LQ uncertainty type";
	std::cout << temp << std::endl;
	throw(std::runtime_error(temp));
  }

}

/**
 * Destructor: default.
 */
CellSurvivalObjective::~CellSurvivalObjective()
{
}


/// Find out how many voxels were in objective
unsigned int CellSurvivalObjective::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}


/**
 * calculate objective
 */
double CellSurvivalObjective::calculate_objective(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
  double sf = 0;
  vector<double> tsf(lq_uncertainty_->get_nScenarios(),0);

  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

	unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	double dose = Dij.calculate_voxel_dose(voxelNo, beam_weights);
	double fractiondose = dose / (float)nFractions_;

	for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	  double alpha = lq_uncertainty_->get_voxel_alpha(iScenario,voxelNo);
	  double betaalpha = lq_uncertainty_->get_voxel_betaalpha(iScenario,voxelNo);
	  double clonogens = lq_uncertainty_->get_voxel_cell_density(iScenario,voxelNo);

	  tsf[iScenario] += clonogens / exp(alpha*dose*(1 + betaalpha*fractiondose));
	}
  }

  double obj = calculate_obj_from_survival(tsf);

  return weight_*obj;
}




/**
 * calculate objective from dose vector
 */

double CellSurvivalObjective::calculate_dose_objective(const DoseVector & the_dose)
{
  vector<double> tsf(lq_uncertainty_->get_nScenarios(),0);
  
  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

	unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	double dose = the_dose.get_voxel_dose(voxelNo);
	double fractiondose = dose / (float)nFractions_;

	for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	  double alpha = lq_uncertainty_->get_voxel_alpha(iScenario,voxelNo);
	  double betaalpha = lq_uncertainty_->get_voxel_betaalpha(iScenario,voxelNo);
	  double clonogens = lq_uncertainty_->get_voxel_cell_density(iScenario,voxelNo);
	  double prob = lq_uncertainty_->get_scenario_probability(iScenario);

	  tsf[iScenario] += clonogens / exp(alpha*dose*(1 + betaalpha*fractiondose));
	}
  }

  double obj = calculate_obj_from_survival(tsf);
  return weight_*obj;
}

   

/**
 * Calculate the objective and the gradient
 */
double CellSurvivalObjective::calculate_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
  assert(beam_weights.get_nBixels() == gradient.get_nBixels());
  estimated_ssvo = 0.0;

  vector<double> tsf(lq_uncertainty_->get_nScenarios(),0);
  vector<double> dose(get_nVoxels(),0);
  vector<double> fractiondose(get_nVoxels(),0);

  double total_tcp = 0;

  // precalculate voxel doses
  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

	unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	dose[iVoxel] = Dij.calculate_voxel_dose(voxelNo, beam_weights);
	fractiondose[iVoxel] = dose[iVoxel] / (float)nFractions_;
  }

  // precalculate survival
  for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	double prob = lq_uncertainty_->get_scenario_probability(iScenario);
	  
	for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  double alpha = lq_uncertainty_->get_voxel_alpha(iScenario,voxelNo);
	  double betaalpha = lq_uncertainty_->get_voxel_betaalpha(iScenario,voxelNo);
	  double clonogens = lq_uncertainty_->get_voxel_cell_density(iScenario,voxelNo);

	  tsf[iScenario] += clonogens / exp(alpha*dose[iVoxel]*(1 + betaalpha*fractiondose[iVoxel]));
	}

	cout << iScenario << ": " << tsf[iScenario] << " " << exp(-1*tsf[iScenario]) << endl;

  }

  // calculate gradient
  for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	double prob = lq_uncertainty_->get_scenario_probability(iScenario);
  
	for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  double alpha = lq_uncertainty_->get_voxel_alpha(iScenario,voxelNo);
	  double betaalpha = lq_uncertainty_->get_voxel_betaalpha(iScenario,voxelNo);
	  double clonogens = lq_uncertainty_->get_voxel_cell_density(iScenario,voxelNo);

	  double sf = clonogens / exp(alpha*dose[iVoxel]*(1 + betaalpha*fractiondose[iVoxel]));
	  double d_obj_by_survival = calculate_d_obj_by_survival(tsf,iScenario);
	  double d_obj_by_dose = prob * weight_ * d_obj_by_survival * sf * (-1)*alpha*(1+2*betaalpha*fractiondose[iVoxel]);
	  
	  Dij.calculate_voxel_dose_gradient(voxelNo,gradient,d_obj_by_dose*gradient_multiplier);
	}
  }

  // return objective
  double obj = calculate_obj_from_survival(tsf);
  return weight_*obj;
}




/**
 * Poisson TCP model
 */

PoissonTcpObjective::PoissonTcpObjective(Voi*  the_voi,
										 unsigned int objNo,
										 string file_name,
										 float maximum_cell_density,
										 unsigned int nFractions,
										 string uncertainty_type,
										 float weight,
										 unsigned int nVoxels)
  : CellSurvivalObjective(the_voi,objNo,file_name,maximum_cell_density,
						  nFractions,uncertainty_type,weight,nVoxels)
{
}

void PoissonTcpObjective::printOn(std::ostream& o) const
{
  o << "OBJ " << objNo_ << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  o << "\tmaximizing Poisson TCP model";
  o << "\tnFractions " << nFractions_;
}

double PoissonTcpObjective::calculate_obj_from_survival(vector<double> & survival)
{
  double tcp = 0;

  for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	double prob = lq_uncertainty_->get_scenario_probability(iScenario);
	tcp += prob * exp( (-1)*survival[iScenario] );
  }

  return tcp;
}

double PoissonTcpObjective::calculate_d_obj_by_survival(vector<double> & survival, unsigned int scenarioNo)
{
  return ( (-1)*exp( (-1)*survival[scenarioNo] ) );
}



/**
 * Log expected integral survival model
 */

LogSurvivalObjective::LogSurvivalObjective(Voi*  the_voi,
										   unsigned int objNo,
										   string file_name,
										   float maximum_cell_density,
										   unsigned int nFractions,
										   string uncertainty_type,
										   float weight,
										   unsigned int nVoxels)
  : CellSurvivalObjective(the_voi,objNo,file_name,maximum_cell_density,
						  nFractions,uncertainty_type,weight,nVoxels)
{
}

void LogSurvivalObjective::printOn(std::ostream& o) const
{
  o << "OBJ " << objNo_ << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  o << "\tminimizing log survival";
  o << "\tnFractions " << nFractions_;
}

double LogSurvivalObjective::calculate_d_obj_by_survival(vector<double> & survival, unsigned int scenarioNo)
{
  double sf = 0;

  for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	double prob = lq_uncertainty_->get_scenario_probability(iScenario);
	sf += prob * survival[iScenario];
  }

  return (1.0/sf);
}

double LogSurvivalObjective::calculate_obj_from_survival(vector<double> & survival)
{
  double sf = 0;

  for (unsigned int iScenario=0; iScenario<lq_uncertainty_->get_nScenarios(); iScenario++) {

	double prob = lq_uncertainty_->get_scenario_probability(iScenario);
	sf += prob * survival[iScenario];
  }

  return log(sf);
}

