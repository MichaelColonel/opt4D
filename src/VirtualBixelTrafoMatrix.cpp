/**
 * @file: VirtualBixelTrafoMatrix.cpp
 * VirtualBixelTrafoMatrix Class implementation.
 *
 */

#include "VirtualBixelTrafoMatrix.hpp"
#include <sstream>
#include <assert.h>
#include <iostream>
#include <algorithm>


/* ----------------- Trafo matrix functions --------------- */


/**
 * constructor to create an empty Trafo matrix
 */
TrafoMatrix::TrafoMatrix()
    : is_transposed_(false)
    , nPhysical_bixels_(0)
    , nVirtual_bixels_(0)
    , nEntries_(0)
    , start_index_()
    , nVirtualOrPhysical_()
    , is_initialized_(false)
    , Trafo_()
{
}

/*TrafoMatrix::TrafoMatrix(unsigned int nPhysical_bixels, unsigned int nVirtual_bixels, bool transpose)
    : is_transposed_(transpose)
    , nPhysical_bixels_(nPhysical_bixels)
    , nVirtual_bixels_(nVirtual_bixels)
    , nEntries_(0)
    , start_index_()
    , nVirtualOrPhysical_()
    , is_initialized_(false)
    , Trafo_()
{
    unsigned int nBixels_max;

    if(nVirtual_bixels_>= nPhysical_bixels_) {
	nBixels_max = nVirtual_bixels_;
    }
    else {
	nBixels_max = nPhysical_bixels_;
    }

    // reserve
    nVirtualOrPhysical_.reserve(nBixels_max);
    start_index_.reserve(nBixels_max);
    Trafo_.reserve(nBixels_max);

    // resize
    if(!is_transposed_) {
	start_index_.resize(nPhysical_bixels_,0);
	nVirtualOrPhysical_.resize(nPhysical_bixels_,0);
    }
    else {
	start_index_.resize(nVirtual_bixels_,0);
	nVirtualOrPhysical_.resize(nVirtual_bixels_,0);
    }

    Trafo_.resize(0);
}*/

/**
 * destructor
 */
TrafoMatrix::~TrafoMatrix()
{
}

/**
 * sort entries and set start index
 */
void TrafoMatrix::initialize()
{
    sort_entries();
    set_start_index();
    is_initialized_ = true;
}

/**
 * sort entries
 */
void TrafoMatrix::sort_entries()
{
    sort(Trafo_.begin(), Trafo_.end());
}

/**
 * Transpose matrix.
 */
void TrafoMatrix::transpose()
{
  unsigned int old_virtualNo, old_physicalNo;

  std::cout << "Transposing virtual bixel Trafo Matrix....";

  // clear number of entries per bixel
  if(!is_transposed_) {
      nVirtualOrPhysical_.resize(nVirtual_bixels_);
      start_index_.resize(nVirtual_bixels_);
  }
  else {
      nVirtualOrPhysical_.resize(nPhysical_bixels_);
      start_index_.resize(nPhysical_bixels_);
  }

  fill(nVirtualOrPhysical_.begin(),nVirtualOrPhysical_.end(),0);

  // Update index numbers for each Dij entry
  vector<TrafoMatrix::node>::iterator iter(Trafo_.begin());
  while(iter != Trafo_.end()) {
    old_virtualNo = index_to_virtualNo((*iter).index_);
    old_physicalNo = index_to_physicalNo((*iter).index_);

    if(!is_transposed_) {
      (*iter).index_ = (nPhysical_bixels_ * old_virtualNo) + old_physicalNo;
      ++(nVirtualOrPhysical_[old_virtualNo]);
    }
    else {
      (*iter).index_ = (nVirtual_bixels_ * old_physicalNo) + old_virtualNo;
      ++(nVirtualOrPhysical_[old_physicalNo]);
    }
    iter++;
  }

  // Update is_transposed_
  is_transposed_ = !is_transposed_;

  // Sort the entries by their new bixel numbers
  sort_entries();

  // update start index
  set_start_index();
}

void TrafoMatrix::set_start_index()
{
    start_index_[0] = 0;
    for(unsigned int i=0; i<start_index_.size()-1; i++) {
	start_index_[i+1] = start_index_[i] + nVirtualOrPhysical_[i];
    }
    assert(*(start_index_.end()-1) + *(nVirtualOrPhysical_.end()-1) == nEntries_);
}

/**
 * delete zero entries
 */
void TrafoMatrix::delete_zero_entries()
{
    unsigned int nNewElements = 0;
    vector<TrafoMatrix::node>::iterator iter_old(Trafo_.begin());
    vector<TrafoMatrix::node>::iterator iter_new(Trafo_.begin());

    while(iter_old != Trafo_.end()) {

	// copy element in old node vector to its position in the new vector
	*(iter_new) = *(iter_old);

	// delete entry
	if(iter_old->value_ == 0) {
	    nEntries_--;
	    if(!is_transposed_) {
		(nVirtualOrPhysical_[index_to_physicalNo(iter_old->index_)])--;
	    }
	    else {
		(nVirtualOrPhysical_[index_to_virtualNo(iter_old->index_)])--;
	    }
	}
	// increment new vector iterator only if the element is not zero
	else {
	    iter_new++;
	    nNewElements++;
	}

	// incrment iterator over old vector
	iter_old++;
    }
    // resize the node vector
    Trafo_.resize(nNewElements);
}

/**
 * unite entries for the same virtual/physical bixel combination
 */
void TrafoMatrix::unite_entries()
{
    // Sort the entries by their index
    sort_entries();

    // iterate over entries
    vector<TrafoMatrix::node>::iterator iter_pre(Trafo_.begin());
    vector<TrafoMatrix::node>::iterator iter(Trafo_.begin());
    while(iter != Trafo_.end()) {

	if(iter->index_ == iter_pre->index_ && iter != Trafo_.begin()) {
	    // add the value of the previous entry to the current and set it zero
	    iter->value_ += iter_pre->value_;
	    iter_pre->value_ = 0;
	}

	iter_pre = iter;
	iter++;
    }
    // delete zeros
    delete_zero_entries();
}


