/* $Id: Prescription.cpp,v 1.2 2006/07/13 18:33:03 bmartin Exp $ */

/**
 * @version 0.9.4
 * Prescription Class implementation.
 * <pre>
 * ver 0.9.4     BM      Jul 15, 2004    moved is_target, is_oar to Voi class
 * ver 0.6       TB      Dec 20, 2003    creation
 * TB: Thomas Bortfeld
 * BM: Ben Martin
 * Massachusetts General Hospital, Department of Radiation Oncology
 * </pre>
 */

#include "Prescription.hpp"

/**
 * Constructor: Initialize variables.  By default, there are no constraints.
 */
Prescription::Prescription()
  : weight_over_(1.0f)
  , weight_under_(1.0f)
  , is_DVH_(false)
  , is_min_(false)
  , is_max_(false)
  , is_EUD_(false)
  , max_DVH_dose_()
  , max_DVH_volume_()
  , min_DVH_dose_()
  , min_DVH_volume_()
  , max_dose_(0)
  , min_dose_(0)
  , max_EUD_dose_(0)
  , min_EUD_dose_(0)
{
}

/**
 * Destructor: default.
 */
Prescription::~Prescription()
{
}

void Prescription::set_weight_over(float weight)
{
  weight_over_ = weight;
}


void Prescription::set_weight_under(float weight)
{
  weight_under_ = weight;
}


void Prescription::add_max_DVH_point(float dose, float volume)
{
  // Use max type if possible
  if(is_DVH_) {
    // Already a DVH type, so add as a dvh point
    max_DVH_dose_.push_back(dose);
    max_DVH_volume_.push_back(volume);
  }
  else if(volume == 0) {
    // Can be interpreted as a max point
    set_max_dose(dose);
  }
  else {
    // Can't be interpreted as a max point
    is_DVH_ = true;

    // Convert existing min or max to DVH style constraint
    if(is_min_) {
      is_min_ = false;
      if(min_dose_ > 0) {
        add_min_DVH_point(min_dose_, 100.f);
      }
    }
    if(is_max_) {
      // Convert to DVH style constraint
      is_max_ = false;
      add_max_DVH_point(max_dose_, 0.f);
    }

    max_DVH_dose_.push_back(dose);
    max_DVH_volume_.push_back(volume);
  }
}


void Prescription::add_min_DVH_point(float dose, float volume)
{
  // Use min type if possible
  if(is_DVH_) {
    // Already a DVH type, so add as a dvh point
    min_DVH_dose_.push_back(dose);
    min_DVH_volume_.push_back(volume);
  }
  else if(volume == 100) {
    // Can be interpreted as a min point
    set_min_dose(dose);
  }
  else {
    // Can't be interpreted as a min point
    is_DVH_ = true;

    // Convert existing min or max to DVH style constraint
    if(is_min_) {
      is_min_ = false;
      if(min_dose_ > 0) {
        add_min_DVH_point(min_dose_, 100.f);
      }
    }
    if(is_max_) {
      // Convert to DVH style constraint
      is_max_ = false;
      add_max_DVH_point(max_dose_, 0.f);
    }

    min_DVH_dose_.push_back(dose);
    min_DVH_volume_.push_back(volume);
  }
}


void Prescription::set_max_dose(float max_dose)
{
  if(is_DVH_) {
    add_max_DVH_point(max_dose, 0.f);
  } else {
    max_dose_ = max_dose;
    is_max_ = true;
  }
}


void Prescription::set_min_dose(float min_dose)
{
  if(is_DVH_) {
    add_min_DVH_point(min_dose, 100.f);
  } else {
    min_dose_ = min_dose;
    is_min_ = true;
  }
}


void Prescription::set_max_EUD(float EUD)
{
  max_EUD_dose_ = EUD;
  // max_EUD_p_ = p;
  is_EUD_ = true;
}


void Prescription::set_min_EUD(float EUD)
{
  min_EUD_dose_ = EUD;
  // min_EUD_p_ = p;
  is_EUD_ = true;
}
