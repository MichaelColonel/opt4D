/**
 * @file DvhObjective.hpp
 * DvhObjective Class header
 *
 */

#ifndef DVHOBJECTIVE_HPP
#define DVHOBJECTIVE_HPP

#include "NonSeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class for generalized uniform dose objective function
 */
class DvhObjective : public NonSeparableObjective
{
	
public:
	
	class DvhPoint;
	class DvhInputParser;
	
	// Constructor
  DvhObjective(Voi*  the_voi,
			   unsigned int objNo,
			   bool is_max_constraint_,
			   bool is_min_constraint_,
			   vector<DvhPoint> max_dvh_points,
			   vector<DvhPoint> min_dvh_points,
			   float weight,
			   float sampling_fraction,
			   bool sample_with_replacement);
 	
	// Virtual destructor
	virtual ~DvhObjective();
	
    /// get dose limits and penalty
    void get_maxdose_and_weight(float rank, float & doselimit, float & weight) const;

    /// get dose limits and penalty
    void get_mindose_and_weight(float rank, float & doselimit, float & weight) const;
	  
	//
	// Functions reimplemented from NonSeparableObjective
	//
	virtual double calculate_objective(const vector<float> & dose) const;
	
	virtual double calculate_objective_and_d(const vector<float> & dose,
											 vector<float> & d_obj_by_dose) const;
	
	virtual double calculate_objective_and_d2(const vector<float> & dose,
											  vector<float> & d_obj_by_dose,
											  vector<float> & d2_obj_by_dose) const;
	
	//
	// Functions reimplemented from Objective
	//
	
    bool supports_meta_objective() const {return true;};

	/// Prints a description of the objective
	virtual void printOn(std::ostream& o) const;
	
	/// class to hold information about a single Dvh point
	class DvhPoint
	{
	public:
		DvhPoint();
		DvhPoint(float volume,float dose,float weight);
		
		// make Dvh points sortable with respect to volume
		friend bool operator< (const DvhPoint& lhs,const DvhObjective::DvhPoint& rhs) {
			return lhs.volume_ < rhs.volume_;
		};
		
        /// Write out scenario to stream
	    friend std::ostream& operator<< (std::ostream& o, const DvhObjective::DvhPoint& rhs);

		// data
		float volume_;
		float dose_;
		float weight_;
	};
	
	/// class to process plan file input for DVH objectives
	class DvhInputParser
	{
	public:
		DvhInputParser(string input);
	  vector<DvhPoint> get_dvh_points() {return dvh_points_;};
		
	private:
		DvhInputParser();
		vector<DvhPoint> dvh_points_;
	};
	
private:
	// Default constructor not allowed
	DvhObjective();
	
	/// if dvh points are max constraints
	bool is_max_constraint_;
	
	/// if dvh points are min constraints
	bool is_min_constraint_;
	
  /// number of max DVH points
  unsigned int nMax_dvh_points_;

  /// number of min DVH points
  unsigned int nMin_dvh_points_;

	/// the Dvh points for max limits
	vector<DvhPoint> max_dvh_points_;
	
	/// the Dvh points for min limits
	vector<DvhPoint> min_dvh_points_;
	
	
	
};


#endif
