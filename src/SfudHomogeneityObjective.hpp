/**
 * @file SfudHomogeneityObjective.hpp
 * SfudHomogeneityObjective Class header
 *
 * $ Id: $
 */

#ifndef SFUDHOMOGENEITYOBJECTIVE_HPP
#define SFUDHOMOGENEITYOBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include "FastMeanDoseComputer.hpp"
#include <cmath>

/**
 * Class SfudHomogeneityObjective is an objective.
 */
class SfudHomogeneityObjective : public Objective
{

    public:

        // Constructor
        SfudHomogeneityObjective(
                Voi*  the_voi,
                unsigned int objNo,
				const Geometry* geometry,
                float tolerance,
                float weight = 1);

        // Initialize with the Dij
        void initialize(DoseInfluenceMatrix& Dij);

        //
        // Calculate or estimate the objective
        //
        double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                bool use_voxel_sampling,
                double &estimated_ssvo);

        bool supports_gradient() const {return true;};
  
        double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                bool use_voxel_sampling,
                double &estimated_ssvo);

        // Calculate the objective based on just the dose vector provided
        // Returns zero if the objective does not suppor dose objectives
        double calculate_dose_objective(
                const DoseVector & the_dose
                );


        //
        // incorporating uncertainty
        //

        /// Does the objective support RandomScenario?
        bool supports_RandomScenario() const {return false;};

        /// Prints a description of the objective
        void printOn(std::ostream& o) const;

    protected:

    private:
        // Default constructor not allowed
        SfudHomogeneityObjective();

        /// geometry information
        const Geometry* the_geometry_;

        /// corresponding Voi
        Voi *the_voi_;

        /// Class to hold precalculated mean dose contributions
        FastMeanDoseComputer mean_dose_computer_;

  /// homogeneity tolerance
  float tolerance_;

  /// indeces of the bixels that belong to a given beam
  vector<vector<unsigned int> > beam_bixels_;

};




#endif
