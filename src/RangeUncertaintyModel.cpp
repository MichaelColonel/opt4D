/**
 * @file: RangeUncertaintyModel.cpp
 * RangeUncertaintyModel Class implementation.
 *
 */

#include "RangeUncertaintyModel.hpp"
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits.h>

/**
 * constructor: get uncertainty model from the .pln file
 *
 */
AuxiliaryRangeModel::AuxiliaryRangeModel(
    DoseInfluenceMatrix &Dij,
    const Geometry *Geom,
    const DoseDeliveryModel::options *options)
    : AuxiliaryBasedSystematicErrorModel(Geom,options->range_nRanges_)
      ,the_DoseInfluenceMatrix_(&Dij)
      ,max_range_dev_(options->range_max_dev_)
      ,pdf_width_(Dij.get_nBixels(),options->range_sigma_)
//      ,range_shifts_(options->range_nRanges_,vector<float>(Dij.get_nBixels(),0))
      ,range_shifts_(vector< map<unsigned int,float> >(Dij.get_nBixels()))
{
    cout << "Constructing AuxiliaryRangeModel:" << endl;
    cout << "number of range shifts: " << get_nInstances() << endl;
    cout << "Pdf width: " << get_pdf_width(0) << endl;
    cout << "max dev: " << get_pdf_width(0)*get_max_range_dev() << endl;

    // set of range shifts for each beamlet
    init_RangeShifts();

    // create auxiliary Dijs
    init_Dijs();

    if(options->range_readDijs_) {
	// read precalculated auxiliary Dij matrices
	read_range_uncertainty_model(options->range_auxDij_root_);
    }
    else {
	// calculate the auxiliary Dij matrices
	calculate_auxiliary_Dijs();
	if(options->range_writeDijs_) {
	    the_DoseInfluenceMatrix_->ensure_transposed();
	    the_DoseInfluenceMatrix_->ensure_sorted();
	    write_range_uncertainty_model(options->range_auxDij_root_);
	    the_DoseInfluenceMatrix_->write_dij_files(options->range_auxDij_root_);
	}
    }
}


/**
 * generate nominal scenario
 */
void AuxiliaryRangeModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // range shift scenario
    random_scenario.auxiliaryNos_.resize(the_geometry_->get_nBixels(),0);
    fill(random_scenario.auxiliaryNos_.begin(),random_scenario.auxiliaryNos_.end(),0);
}

/**
 * for the best matching auxiliary for a given range shift
 */
/*unsigned int AuxiliaryRangeModel::get_auxiliary_for_range_shift(unsigned int bixelNo, float range_shift) const
{
    // get resolution
    float Resolution = 2 * get_max_range_dev() * get_pdf_width(bixelNo) / (float) (get_nInstances()-1);

    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
    {
	if(fabs(get_RangeShift(iInstance, bixelNo) - range_shift) <= Resolution) {
	    return iInstance;
	}
    }

    // check for smaller than smallest range
    if(range_shift < get_max_range_dev() * get_pdf_width(bixelNo)) {
    }
}*/
unsigned int AuxiliaryRangeModel::get_auxiliary_for_range_shift(unsigned int bixelNo, float range_shift)
{
    // get resolution
    float resolution_by_2 = get_max_range_dev() * get_pdf_width(bixelNo) / (float) (get_nInstances()-1);

    float min_diff = (float) INT_MAX;
    unsigned int instanceNo = 0;

    map<unsigned int,float>::iterator iter = range_shifts_[bixelNo].begin();
    while(iter != range_shifts_[bixelNo].end()) {

	float diff = fabs(iter->second - range_shift);
	if(diff < min_diff) {
	    min_diff = diff;
	    instanceNo = iter->first;
	}
	iter++;
    }
    return(instanceNo);
}

/**
 * set the set of beamlet ranges
 */
/*void AuxiliaryRangeModel::init_RangeShifts()
{
    float Resolution;

    // loop over the beamlets
    for (unsigned int iBixel=0; iBixel<the_DoseInfluenceMatrix_->get_nBixels(); iBixel++)
    {
	if (get_nInstances() > 1)
	{
	    // get resolution
	    Resolution = 2 * get_max_range_dev() * get_pdf_width(iBixel) / (float) (get_nInstances()-1);

	    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	    {
		// set range deviation values
		set_RangeShift(iInstance,
			       iBixel,
			       - get_max_range_dev() * get_pdf_width(iBixel) + iInstance * Resolution );
	    }
	}
	else {
	    set_RangeShift(0,iBixel,0);
	}
    }
}*/
void AuxiliaryRangeModel::init_RangeShifts()
{
    float Resolution;

    // loop over the beamlets
    for (unsigned int iBixel=0; iBixel<the_DoseInfluenceMatrix_->get_nBixels(); iBixel++)
    {
	if (get_nInstances() > 1)
	{
	    // get resolution
	    Resolution = 2 * get_max_range_dev() * get_pdf_width(iBixel) / (float) (get_nInstances()-1);

	    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	    {
		// set range deviation values
		range_shifts_[iBixel][iInstance] = (- get_max_range_dev() * get_pdf_width(iBixel) + iInstance * Resolution);
	    }
	}
	else {
	    range_shifts_[iBixel][0] = 0;
	}
    }
}


/**
 * This function allocates memory for an additional Dij matrix for each instance and stores the number of that Dij in DoseDeliveryModel. (It does not check whether one of the existing Dijs can be used for this instance.)
 *
 */
void AuxiliaryRangeModel::init_Dijs()
{
    // instance loop
    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
    {
	// set index to this Dij
	set_AuxiliaryDijNo(iInstance, the_DoseInfluenceMatrix_->get_nAuxiliaries()+1);

	// create new auxiliary Dij
	the_DoseInfluenceMatrix_->create_auxiliary_Dij();
    }
}



/**
 * Calculate all auxiliary Dij matrices.
 * Problem: first we have to extend the original Dij
 * because for enlarged ranges, we need dose contributions
 * to voxels beyond the bragg peak
 */
void AuxiliaryRangeModel::calculate_auxiliary_Dijs()
{
    /* make sure everything is initialized and prepared for Dij calculation:
       at this stage the instances have to be defined and auxiliary Dijs
       were created */

    // make sure Dij is not transposed
    the_DoseInfluenceMatrix_->ensure_not_transposed();

    // first, shift the voxels in the nominal Dij in direction of the range variation
    // in order to determine to which voxels the dose is shifted.
    // Count the number of new voxels and add blank space
    // in the Dij at the end of each beamlet
    add_blank_space_to_Dij();

    // For each beamlet, add the indices of the new voxels
    // to the nominal Dij with zero influence
    add_elements_to_Dij();

    // Finally, calculate the auxiliaries for each Dij element
    // which is in the nominal Dij.
    calculate_auxiliary_Dij_elements();

/*
    DoseVector dose_vector(the_DoseInfluenceMatrix_->get_nVoxels());
    the_DoseInfluenceMatrix_->copy_pencil_beam_to_dose_vector (1, dose_vector);
    dose_vector.write_dose("dose.dat");
    the_DoseInfluenceMatrix_->copy_pencil_beam_to_dose_vector (1, dose_vector,1);
    dose_vector.write_dose("dose_1.dat");
    the_DoseInfluenceMatrix_->copy_pencil_beam_to_dose_vector (1, dose_vector,3);
    dose_vector.write_dose("dose_3.dat");
    exit(0);
*/
}


/**
 * determines how much blank space in the Dij matrix is needed
 * to add the required new elements
 */
void AuxiliaryRangeModel::add_blank_space_to_Dij()
{
    vector<bool> inDij(the_DoseInfluenceMatrix_->get_nVoxels());
    vector<unsigned int> nNewEntries(the_DoseInfluenceMatrix_->get_nBixels(),0);

    Point_in_Patient_Coord point, point_shifted;
    Voxel_Subscripts subscripts, subscripts_shifted;
    int voxelNo, voxelNo_shifted;
    unsigned int BeamNo;

    cout << "determining the number of required new Dij elements" << endl;

    // loop over the beamlets
    for (unsigned int iBixel=0; iBixel<the_DoseInfluenceMatrix_->get_nBixels(); iBixel++)
    {
	BeamNo = the_geometry_->get_beamNo(iBixel);

	// identify voxels currently in the Dij
	inDij.assign(inDij.size(),false);
	for( DoseInfluenceMatrix::iterator iter = the_DoseInfluenceMatrix_->begin_bixel(iBixel);
	     iter.get_bixelNo()==iBixel; ++iter)
	{
	    inDij.at(iter.get_voxelNo()) = true;
	}

	// loop over the voxels
	for( DoseInfluenceMatrix::iterator iter = the_DoseInfluenceMatrix_->begin_bixel(iBixel);
	     iter.get_bixelNo()==iBixel; ++iter)
	{
	    // get voxel
	    voxelNo = iter.get_voxelNo();
	    subscripts = the_geometry_->convert_to_Voxel_Subscripts( VoxelIndex(voxelNo) );
	    point = the_geometry_->convert_to_Point_in_Patient_Coord( subscripts );

	    // loop over instances
	    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	    {
		// add range shift
		point_shifted = the_geometry_->transform_to_Patient_Coord(
		    the_geometry_->transform_to_Gantry_Coord(point,BeamNo) +
		    Point_in_Gantry_Coord(0,0,-get_RangeShift(iInstance,iBixel)) , BeamNo );

		subscripts_shifted = the_geometry_->convert_to_Voxel_Subscripts(point_shifted);

		// check if this voxel is inside the dose cube and not air
		if ( the_geometry_->is_inside(subscripts_shifted) ) {
		    // get index
		    voxelNo_shifted = the_geometry_->convert_to_VoxelIndex(subscripts_shifted).index_;

		    if ( !the_geometry_->is_air(voxelNo_shifted) &&
			 !inDij.at(voxelNo_shifted) )
		    {
			inDij.at(voxelNo_shifted) = true;
			nNewEntries.at(iBixel) ++;
		    }
		}
	    }
	}
    }

    // add blank space
    the_DoseInfluenceMatrix_->add_blank_space(nNewEntries);
/*
    for (unsigned int iBixel=0; iBixel<the_DoseInfluenceMatrix_->get_nBixels(); iBixel++)
    {
	cout << "bixel " << iBixel << " new elements " << nNewEntries.at(iBixel) << endl;
    }
*/
}

/**
 * add new Dij elements
 */
void AuxiliaryRangeModel::add_elements_to_Dij()
{
    vector<bool> inDij(the_DoseInfluenceMatrix_->get_nVoxels());
    vector<float> influence;
    vector<unsigned int> voxelNos;
    influence.reserve(the_DoseInfluenceMatrix_->get_nVoxels());
    voxelNos.reserve(the_DoseInfluenceMatrix_->get_nVoxels());

    Point_in_Patient_Coord point, point_shifted;
    Voxel_Subscripts subscripts, subscripts_shifted;
    int voxelNo, voxelNo_shifted;
    unsigned int BeamNo;
    unsigned int nNewElements;

    cout << "adding the required new Dij elements to the nominal Dij" << endl;

    // loop over the beamlets
    for (unsigned int iBixel=0; iBixel<the_DoseInfluenceMatrix_->get_nBixels(); iBixel++)
    {
	BeamNo = the_geometry_->get_beamNo(iBixel);
	nNewElements = 0;
	voxelNos.resize(0);

	// identify voxels currently in the Dij
	inDij.assign(inDij.size(),false);
	for( DoseInfluenceMatrix::iterator iter = the_DoseInfluenceMatrix_->begin_bixel(iBixel);
	     iter.get_bixelNo()==iBixel; ++iter)
	{
	    inDij.at(iter.get_voxelNo()) = true;
	}

	// loop over the voxels
	for( DoseInfluenceMatrix::iterator iter = the_DoseInfluenceMatrix_->begin_bixel(iBixel);
	     iter.get_bixelNo()==iBixel; ++iter)
	{
	    // get voxel
	    voxelNo = iter.get_voxelNo();
	    subscripts = the_geometry_->convert_to_Voxel_Subscripts( VoxelIndex(voxelNo) );
	    point = the_geometry_->convert_to_Point_in_Patient_Coord( subscripts );

	    // loop over instances
	    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	    {
		// add range shift
		point_shifted = the_geometry_->transform_to_Patient_Coord(
		    the_geometry_->transform_to_Gantry_Coord(point,BeamNo) +
		    Point_in_Gantry_Coord(0,0,-get_RangeShift(iInstance,iBixel)) , BeamNo );

		subscripts_shifted = the_geometry_->convert_to_Voxel_Subscripts(point_shifted);

		// check if this voxel is inside the dose cube and not air
		if ( the_geometry_->is_inside(subscripts_shifted) ) {
		    // get index
		    voxelNo_shifted = (the_geometry_->convert_to_VoxelIndex(subscripts_shifted)).index_;

		    if ( !the_geometry_->is_air(voxelNo_shifted) &&
			 !inDij.at(voxelNo_shifted) )
		    {
			voxelNos.push_back(voxelNo_shifted);
			inDij.at(voxelNo_shifted) = true;
			nNewElements++;
		    }
		}
	    }
	}

	// resize vector
	voxelNos.resize(nNewElements);
	influence.resize(nNewElements,0);

	// add elements to nominal Dij (influence is obviously zero)
	the_DoseInfluenceMatrix_->add_Dij_elements_to_bixel(iBixel,voxelNos,influence);

    }
}


/**
 * calculate the auxiliary Dijs for all range shifts
 */
void AuxiliaryRangeModel::calculate_auxiliary_Dij_elements()
{
    Point_in_Patient_Coord point, point_shifted;
    Voxel_Subscripts subscripts, subscripts_shifted;
    int voxelNo, voxelNo_shifted;
    unsigned int BeamNo;
    float dose;

    cout << "approximate auxiliary Dij matrices for range shifts" << endl;

    // create temporary dose vector
    DoseVector temp_DoseVector(the_DoseInfluenceMatrix_->get_nVoxels());

    // loop over the beamlets
    for (unsigned int iBixel=0; iBixel<the_DoseInfluenceMatrix_->get_nBixels(); iBixel++)
    {
	BeamNo = the_geometry_->get_beamNo(iBixel);

	// copy pencil beam to dose vector
	the_DoseInfluenceMatrix_->copy_pencil_beam_to_dose_vector (iBixel, temp_DoseVector);

	// loop over the voxels
	for( DoseInfluenceMatrix::non_const_iterator iter = the_DoseInfluenceMatrix_->non_const_begin_bixel(iBixel);
	     iter.get_bixelNo()==iBixel; ++iter)
	{
	    // get voxel
	    voxelNo = iter.get_voxelNo();
	    subscripts = the_geometry_->convert_to_Voxel_Subscripts( VoxelIndex(voxelNo) );
	    point = the_geometry_->convert_to_Point_in_Patient_Coord( subscripts );

	    // loop over instances
	    for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	    {
		// if this instance uses the original Dij, nothing has to be calculated
		if (get_AuxiliaryDijNo(iInstance) == 0) {
		    // do nothing
		}
		else {
		    // add range shift
		    point_shifted = the_geometry_->transform_to_Patient_Coord(
			the_geometry_->transform_to_Gantry_Coord(point,BeamNo) +
			Point_in_Gantry_Coord(0,0,get_RangeShift(iInstance,iBixel)) , BeamNo );

		    subscripts_shifted = the_geometry_->convert_to_Voxel_Subscripts(point_shifted);

		    // check if this voxel is inside the dose cube and not air
		    if ( the_geometry_->is_inside(subscripts_shifted) ){
//		    if(0==1) { // copy values of nominal Dij for testing purpose
			// get index
			voxelNo_shifted = the_geometry_->convert_to_VoxelIndex(subscripts_shifted).index_;

			if (!the_geometry_->is_air(voxelNo_shifted)) {
			    // get dose at this coordinate by tri-linear interpolation
			    dose = the_geometry_->get_dose_by_trilinear_interpolation(temp_DoseVector,point_shifted);

			    // store the dose value in the auxiliary Dij
			    iter.set_influence(dose,get_AuxiliaryDijNo(iInstance));
			}
			else {
			    // just return the dose value of the voxel without range shift
			    // this is not ideal for voxels near the patient surface but will do for the time being
			    dose = temp_DoseVector.get_voxel_dose(voxelNo);

			    // store the dose value in the auxiliary Dij
			    iter.set_influence(dose,get_AuxiliaryDijNo(iInstance));
			}
		    }
		    else {
			// just return the dose value of the voxel without range shift
			// this is not ideal for voxels near the patient surface but will do for the time being
			dose = temp_DoseVector.get_voxel_dose(voxelNo);

			// store the dose value in the auxiliary Dij
			iter.set_influence(dose,get_AuxiliaryDijNo(iInstance));
		    }

		}
	    }
	}
    }
}

/**
 * read precalculated auxiliary Dij matrices
 */
void AuxiliaryRangeModel::read_range_uncertainty_model(string dij_file_root)
{
    int file_id_number;
    string attribute;
    unsigned int uiDummy,iInstance,iBixel;
    float fDummy;

    string file_name = dij_file_root + ".aif";
    std::ifstream infile;

    infile.open(file_name.c_str(), std::ios::in);
    // check if the file exists
    if ( !infile.is_open() ) {
        std::cerr << "Can't open file: " << file_name << std::endl;
        throw("Unable to open auxiliary information file");
    }

    // write uncertainty model information file
    std::cout << "Reading auxiliary information file: " << file_name << endl;

    // Header
    infile >> attribute;
    if(attribute != "range_uncertainty_model") {
	cout << "Error: expected range_uncertainty_model" << endl;
        throw("Error reading auxiliary information file");
    }

    infile >> attribute;
    if(attribute != "auxiliary_identification_number") {
	cout << "Error reading auxiliary information file" << endl;
        throw("Error reading auxiliary information file");
    }
    infile >> file_id_number;


    infile >> attribute;
    if(attribute != "number_of_ranges") {
	cout << "Error: expected number_of_ranges" << endl;
        throw("Error reading auxiliary information file");
    }
    infile >> uiDummy;
    if(uiDummy != get_nInstances()) {
	cout << "Error: wrong number of instances" << endl;
        throw("Error reading auxiliary information file");
    }

    // auxiliary Dij indices
    infile >> attribute;
    if(attribute != "auxiliary_indices:") {
	cout << "Error: expected auxiliary_indices:" << endl;
        throw("Error reading auxiliary information file");
    }
    for(unsigned int i=0; i<get_nInstances(); i++) {
	infile >> iInstance;
	infile >> uiDummy;
	if(iInstance != i || uiDummy != get_AuxiliaryDijNo(i)) {
	    cout << "Error: wrong auxiliary Dij number" << endl;
	    throw("Error reading auxiliary information file");
	}
    }

    // range shifts
    infile >> attribute;
    if(attribute != "range_shifts:") {
	cout << "Error: expected range_shifts:" << endl;
        throw("Error reading auxiliary information file");
    }
    for(unsigned int i=0; i<get_nInstances(); i++) {
	for(unsigned int j=0; j<the_DoseInfluenceMatrix_->get_nBixels(); j++) {
	    infile >> iInstance;
	    infile >> iBixel;
	    infile >> fDummy;
	    if(iInstance != i || iBixel != j || fDummy != get_RangeShift(i,j)) {
		cout << "Error: wrong range shift" << endl;
		throw("Error reading auxiliary information file");
	    }
	}
    }

    infile.close();

    // read auxiliary Dijs
    string adij_file_name = dij_file_root + ".adij";
    the_DoseInfluenceMatrix_->read_auxiliary_Dijs(adij_file_name,
            file_id_number);
}

/**
 * write auxiliary Dij matrices to file
 */
void AuxiliaryRangeModel::write_range_uncertainty_model(string dij_file_root)
{
    // get a random number to correlate range model and auxiliary Dijs
    int file_id_number = rand();

    string file_name = dij_file_root + ".aif";
    std::ofstream ofile;

    ofile.open(file_name.c_str(), std::ios::out);
    // check if the file exists
    if ( !ofile.is_open() ) {
        std::cerr << "Can't open file: " << file_name << std::endl;
        throw("Unable to open auxiliary information file");
    }

    // write uncertainty model information file
    std::cout << "Writing auxiliary information file: " << file_name << endl;

    // Header
    ofile << "range_uncertainty_model" << endl;
    ofile << "auxiliary_identification_number " << file_id_number << endl;
    ofile << "number_of_ranges " << get_nInstances() << endl;

    // auxiliary Dij indices
    ofile << "auxiliary_indices:" << endl;
    for(unsigned int i=0; i<get_nInstances(); i++) {
	ofile << i << " " << get_AuxiliaryDijNo(i) << endl;
    }

    // range shifts
    ofile << "range_shifts:" << endl;
    for(unsigned int i=0; i<get_nInstances(); i++) {
	for(unsigned int j=0; j<the_DoseInfluenceMatrix_->get_nBixels(); j++) {
	    ofile << i << " " << j << " " << get_RangeShift(i,j) << endl;
	}
    }

    ofile.close();

    // write auxiliary Dijs
    string adij_file_name = dij_file_root + ".adij";
    the_DoseInfluenceMatrix_->write_auxiliary_Dijs(adij_file_name,
            file_id_number);
}


/**
 * store a dose evaluation scenario
 */
bool AuxiliaryRangeModel::set_dose_evaluation_scenario(
    map<string,string> scenario_map)
{

    float dummy,prob;
    unsigned int beamNo,instNo;
    vector<unsigned int> instanceNos(the_geometry_->get_nBeams(),0);
    vector<float> rangeShifts(the_geometry_->get_nBeams(),0);
    RandomScenario scenario;

    // ----------------------- probability -----------------------------------

    std::istringstream iss_p(scenario_map["prob"]);

    if(!(iss_p >> dummy).fail()) {
	prob = dummy;
    }
    else {
	return false;
    }

    // ----------------------- range -----------------------------------

    std::istringstream iss_r(scenario_map["range"]);

    // read the beam number
    while(!(iss_r >> beamNo).fail()
	  && beamNo <= the_geometry_->get_nBeams()
	  && beamNo >= 1) {

	// read the range shift for this beam
	if(!(iss_r >> dummy).fail()) {
	    rangeShifts[beamNo-1] = dummy;
	}
	else {
	    return false;
	}
    }

    // check if all beams are supplied
    if(rangeShifts.size() != the_geometry_->get_nBeams()) {
	return false;
    }

    // convert range shifts to instanceNo
    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	unsigned int iBeam = the_geometry_->get_beamNo(iBixel);
	instanceNos[iBeam] = get_auxiliary_for_range_shift(iBixel,rangeShifts[iBeam]);
    }

    // resize auxiliary vector
    scenario.auxiliaryNos_.resize(the_geometry_->get_nBixels(),0);

    // there is a range shift for every beam, now create auxiliary vector
    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	scenario.auxiliaryNos_[iBixel] =
	    get_AuxiliaryDijNo(instanceNos[the_geometry_->get_beamNo(iBixel)]);
    }

    // store scenario
    dose_eval_scenarios_.push_back(scenario);
    dose_eval_pdf_.push_back(prob);
    nDose_eval_scenarios_++;

    // print info to screen
    cout << "set dose evaluation scenario " << dose_eval_scenarios_.size() << endl;
    cout << "Probability: " << prob << endl;
    cout << "Range shifts: " << endl;

    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	for(unsigned int iBixel = 0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	    if(the_geometry_->get_beamNo(iBixel) == iBeam) {
		cout << "beam " << iBeam << ": wanted " << rangeShifts[iBeam]
		     << ", get " << get_RangeShift(instanceNos[iBeam],iBixel)
		     << "(instance " << instanceNos[iBeam] << ")" << endl;
		break;
	    }
	}
    }
    return(true);
}



// ------------------------- GeneralAuxiliaryRangeModel ---------------------------


/**
 * constructor
 */
GeneralAuxiliaryRangeModel::GeneralAuxiliaryRangeModel(
    DoseInfluenceMatrix &Dij,
    const Geometry *Geom,
    const DoseDeliveryModel::options *options)
    : AuxiliaryRangeModel(Dij,Geom,options)
    ,correlation_model_(options->range_correlation_model_)
{
    cout << "constructing GeneralAuxiliaryRangeModel ..." << endl;
    cout << "beamlet correlation model: " << get_correlation_model() << endl;

    // initialize the Pdf
    init_Pdf();
}


/**
 * set Pdf
 *
 */
void GeneralAuxiliaryRangeModel::init_Pdf()
{
    init_GaussianPdf();
}



/**
 * set Gaussian Pdf
 *
 */
void GeneralAuxiliaryRangeModel::init_GaussianPdf()
{
    float dist;
    float norm=0.0;

    if( get_nInstances() > 1 )
    {
	// calculate normalization constant first
	for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	{
	    // get position
	    dist = - get_max_range_dev() + iInstance * (2*get_max_range_dev()/(float)(get_nInstances()-1));

	    // normalization
	    norm += exp(-0.5*dist*dist);
	}

	// now set the Pdf
	for (unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++)
	{
	    dist = - get_max_range_dev() + iInstance * (2*get_max_range_dev()/(float)(get_nInstances()-1));
	    set_Pdf(iInstance, exp(-0.5*dist*dist) / norm);
	}
    }
    else {
	set_Pdf(0,1);
    }
}

/**
 * generate a random sample scenario
 */
void GeneralAuxiliaryRangeModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    generate_random_auxiliaries(random_scenario);
}

/**
 * generate random auxiliaries
 */
void GeneralAuxiliaryRangeModel::generate_random_auxiliaries(RandomScenario &random_scenario) const
{
    // resize auxiliary vector
    random_scenario.auxiliaryNos_.resize(the_geometry_->get_nBixels(),0);

    if(get_correlation_model() == "within_beam")
    {
	unsigned int auxNo;
	float CumPdf, RandomNo;

	// pick random auxiliary Dij for each beam
	for(unsigned int iBeam=0;iBeam<the_geometry_->get_nBeams();iBeam++) {
	    CumPdf = 0;
	    RandomNo = (float) std::rand() / (float) RAND_MAX;
	    auxNo = 0;
	    for(unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++) {
		CumPdf = CumPdf + get_Pdf(iInstance);
		if (RandomNo <= CumPdf) {
		    auxNo = get_AuxiliaryDijNo(iInstance);
		    break;
		}
	    }

	    // set this auxiliary number for all beamlets in the beam
	    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
		if( the_geometry_->get_beamNo(iBixel) == iBeam ) {
		    random_scenario.auxiliaryNos_.at(iBixel) = auxNo;
		}
	    }
	}
    }
    else if(get_correlation_model() == "correlated")
    {
	unsigned int auxNo;
	float CumPdf, RandomNo;

	// pick random auxiliary Dij
	CumPdf = 0;
	RandomNo = (float) std::rand() / (float) RAND_MAX;
	auxNo = 0;
	for(unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++) {
	    CumPdf = CumPdf + get_Pdf(iInstance);
	    if (RandomNo <= CumPdf) {
		auxNo = get_AuxiliaryDijNo(iInstance);
		break;
	    }
	}

	// set this auxiliary number for all beamlets in the beam
	for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	    random_scenario.auxiliaryNos_.at(iBixel) = auxNo;
	}
    }
    else
    {
	cout << "Unknown beamlet correlation model in range uncertainty model" << endl;
	exit(-1);
    }
}

/**
 * returns true if two beamlets are correlated according to the specified correlation_model.
 *
 */
bool GeneralAuxiliaryRangeModel::bixels_correlated(unsigned int BixelNo_j,unsigned int BixelNo_k) const
{

    if(get_correlation_model() == "within_beam")
    {
	if( the_geometry_->get_beamNo(BixelNo_j) == the_geometry_->get_beamNo(BixelNo_k) )
	{
	    return(true);
	}
	return(false);
    }
    else
    {
	cout << "Unknown beamlet correlation model in range uncertainty model" << endl;
	exit(-1);
    }
}

/**
 * calculate voxel variance for within_beam correlation
 */
float GeneralAuxiliaryRangeModel::calculate_expected_voxel_dose(unsigned int voxelNo,
								const BixelVector &bixel_grid,
								DoseInfluenceMatrix &dij) const
{
    float exp_dose, prob;
    unsigned int AuxiliaryNo;

    exp_dose = 0;

    // loop over the instances
    for(unsigned int iInstance=0; iInstance<get_nInstances();iInstance++) {

	AuxiliaryNo = get_AuxiliaryDijNo(iInstance);
	prob = get_Pdf(iInstance);

	    // calc dose to voxel
	    exp_dose += prob * dij.calculate_voxel_dose(voxelNo,
							bixel_grid,
							AuxiliaryNo);
    }

    return exp_dose;
}

/**
 * calculate voxel variance.
 * Only works with "within_beam" correlation model.
 *
 * @param voxelNo The voxel number to calculate for
 * @param bixel_grid The beam weights
 * @param dij The dose influence matrix to use
 */
float GeneralAuxiliaryRangeModel::calculate_voxel_variance(
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij) const
{
    if (get_correlation_model() == "within_beam") {
	return(calculate_voxel_variance_within_beam(voxelNo, bixel_grid, dij));
    }
    else if (get_correlation_model() == "correlated"){
	cout << "calculation of variance not implemented for correlation model "
	     << get_correlation_model() << endl;
	exit(-1);
    }
    else {
	cout << "Unknown beamlet correlation model in range uncertainty model"
            << endl;
	exit(-1);
    }
}


/**
 * calculate voxel variance for within_beam correlation
 */
float GeneralAuxiliaryRangeModel::calculate_voxel_variance_within_beam(
        unsigned int voxelNo,
        const BixelVector &bixel_grid,
        DoseInfluenceMatrix &dij) const
{
    unsigned int nBeams = the_geometry_->get_nBeams();
    unsigned int nInstances = get_nInstances();

    vector< vector<float> > beam_dose(nBeams);
    vector<float> exp_beam_dose(nBeams,0);
    vector<float> exp_beam_dose_2(nBeams,0);

    for (size_t i=0;i<nBeams;i++) {
	beam_dose[i].resize( nInstances,0 );
    }

    float exp_dose, prob, var;
    unsigned int AuxiliaryNo;

    exp_dose = 0;
    var = 0;

    // loop over the beams
    for(unsigned int iBeam=0; iBeam<nBeams; iBeam++) {

	exp_beam_dose[iBeam] = 0;
	exp_beam_dose_2[iBeam] = 0;

	// loop over the instances
	for(unsigned int iInstance=0; iInstance<nInstances;iInstance++) {

	    AuxiliaryNo = get_AuxiliaryDijNo(iInstance);
	    prob = get_Pdf(iInstance);

	    // calc dose to voxel by this beam for one instance
	    beam_dose[iBeam][iInstance] = dij.calculate_voxel_dose_per_beam(
                    voxelNo, bixel_grid, iBeam, *the_geometry_, AuxiliaryNo);
            // calc expected values
            exp_beam_dose[iBeam] += beam_dose[iBeam][iInstance] * prob;
            exp_beam_dose_2[iBeam] += beam_dose[iBeam][iInstance]
                * beam_dose[iBeam][iInstance] * prob;
        }

        // total expected dose
        exp_dose += exp_beam_dose[iBeam];

        // total variance
        var += exp_beam_dose_2[iBeam]
            - exp_beam_dose[iBeam] * exp_beam_dose[iBeam];
    }

    return var;
}

/**
 * calculate voxel contribution to quadratic objective
 */
float GeneralAuxiliaryRangeModel::calculate_quadratic_objective(
    unsigned int voxelNo,
    float prescribed_dose,
    const BixelVector &bixel_grid,
    DoseInfluenceMatrix &dij,
    BixelVectorDirection &gradient,
    float gradient_multiplier,
    double adjusted_weight) const
{
    if (get_correlation_model() == "within_beam") {
	return(calculate_quadratic_objective_within_beam(
                    voxelNo, prescribed_dose,
                    bixel_grid, dij, gradient, gradient_multiplier,
                    adjusted_weight));
    }
    else if (get_correlation_model() == "correlated"){
        cout << "calculate_quadratic_objective_within_beam not implemented for correlation model "
            << get_correlation_model() << endl;
        exit(-1);
    }
    else {
        cout << "Unknown beamlet correlation model in range uncertainty model" << endl;
        exit(-1);
    }

}



/**
 * calculate voxel contribution to quadratic objective for within_beam correlation
 */
float GeneralAuxiliaryRangeModel::calculate_quadratic_objective_within_beam(
    unsigned int voxelNo,
    float prescribed_dose,
    const BixelVector &bixel_grid,
    DoseInfluenceMatrix &dij,
    BixelVectorDirection &gradient,
    float gradient_multiplier,
    double adjusted_weight) const
{
    assert(bixel_grid.get_nBixels() == gradient.get_nBixels());

    unsigned int nBeams = the_geometry_->get_nBeams();
    unsigned int nInstances = get_nInstances();

    float prob;
    unsigned int AuxiliaryNo;
    float exp_dose = 0;
    float objective = 0;
    float dij_2_buf, dij_buf;
    unsigned int bixelNo, beamNo;

    vector< vector<float> > beam_dose(nBeams);
    vector<float> exp_beam_dose(nBeams,0);
    vector<float> exp_beam_dose_2(nBeams,0);

    for (size_t i=0;i<nBeams;i++) {
	beam_dose[i].resize( nInstances,0 );
    }


    // loop over the beams
    for(unsigned int iBeam=0; iBeam<nBeams; iBeam++) {

	exp_beam_dose[iBeam] = 0;
	exp_beam_dose_2[iBeam] = 0;

	// loop over the instances
	for(unsigned int iInstance=0; iInstance<nInstances;iInstance++) {

	    AuxiliaryNo = get_AuxiliaryDijNo(iInstance);
	    prob = get_Pdf(iInstance);

	    // calc dose to voxel by this beam for one instance
	    beam_dose[iBeam][iInstance] = dij.calculate_voxel_dose_per_beam(voxelNo,
									    bixel_grid,
									    iBeam,
									    *the_geometry_,
									    AuxiliaryNo);
	    // calc expected values
	    exp_beam_dose[iBeam] += beam_dose[iBeam][iInstance] * prob;
	    exp_beam_dose_2[iBeam] += beam_dose[iBeam][iInstance]
		* beam_dose[iBeam][iInstance] * prob;
	}

	// total expected dose
	exp_dose += exp_beam_dose[iBeam];

	// add variance contribution to objective
	objective += exp_beam_dose_2[iBeam] - exp_beam_dose[iBeam] * exp_beam_dose[iBeam];
    }

    // add expectation value contribution to objective
    objective += (exp_dose - prescribed_dose) * (exp_dose - prescribed_dose);

    // loop over the bixels that contribute dose to the voxel
    for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {

	bixelNo = iter.get_bixelNo();
	beamNo = the_geometry_->get_beamNo(bixelNo);

	// calc expected influence
	dij_buf = 0;
	dij_2_buf = 0;
	for(unsigned int iInstance=0;iInstance<nInstances;iInstance++) {

	    AuxiliaryNo = get_AuxiliaryDijNo(iInstance);
	    prob = get_Pdf(iInstance);

	    dij_buf += iter.get_influence(AuxiliaryNo) * prob;
	    dij_2_buf += 2 * iter.get_influence(AuxiliaryNo)
		* beam_dose[beamNo][iInstance] * prob;
	}
	// add expectation value contribution
        gradient[bixelNo] += gradient_multiplier * adjusted_weight
            * 2*(exp_dose - prescribed_dose) * dij_buf;

	// add variance contribution
        gradient[bixelNo] += gradient_multiplier * adjusted_weight
            * (dij_2_buf - 2*exp_beam_dose[beamNo]*dij_buf);
    }

    return (adjusted_weight*objective);
}

/**
 * calculate bixel pair contributions to quadratic objective
 */
void GeneralAuxiliaryRangeModel::calculate_bixel_pair_contributions_to_quadratic_objective(
    unsigned int voxelNo,
    vector< vector<double> > &QuadraticTerms,
    vector<double> &LinearTerms,
    vector<float> &Temp_DijRow,
    vector<float> &Temp_LinearTerms) const
{
    //zero temp linear terms
    Temp_LinearTerms.assign(Temp_LinearTerms.size(),0);

    // loop over the auxiliary Dijs
    for (unsigned int iInstance=0; iInstance<get_nInstances();iInstance++)
    {
	// zero tomporary Dij row
	Temp_DijRow.assign(Temp_DijRow.size(),0);

	// get an iterator to the Dij entries of the voxel and copy the bixel contributions to a vector
	for( DoseInfluenceMatrix::iterator iter = the_DoseInfluenceMatrix_->begin_voxel(voxelNo);
	     iter.get_voxelNo()==voxelNo; ++iter)
	{
	    Temp_DijRow[iter.get_bixelNo()] = iter.get_influence(get_AuxiliaryDijNo(iInstance));
	}

	// loop over the bixels
	for (unsigned int iBixelj=0; iBixelj<the_DoseInfluenceMatrix_->get_nBixels(); iBixelj++)
	{
	    if ( Temp_DijRow[iBixelj] > 0 )
	    {
		// calculate linear terms
		Temp_LinearTerms[iBixelj] += Temp_DijRow[iBixelj] * get_Pdf(iInstance);

		// calculate quadratic terms
		for (unsigned int iBixelk=0; iBixelk<=iBixelj; iBixelk++)
		{
		    if ( Temp_DijRow[iBixelk] > 0 )
		    {
			// check correlation of beamlets
			if ( bixels_correlated(iBixelj,iBixelk) )
			{
			    // bixels are correlated, have to perform integration
			    QuadraticTerms[iBixelj][iBixelk] +=
				(double) (Temp_DijRow[iBixelj] * Temp_DijRow[iBixelk] * get_Pdf(iInstance));
			}
		    }
		}
	    }
	}

    }   // end instance integration

    // deal with the terms coming from uncorrelated beamlets
    // loop over the bixels
    for (unsigned int iBixelj=0; iBixelj<the_DoseInfluenceMatrix_->get_nBixels(); iBixelj++)
    {
	if ( Temp_LinearTerms[iBixelj] > 0 )
	{
	    // sum linear terms over the voxels
	    LinearTerms[iBixelj] += (double)Temp_LinearTerms[iBixelj];

	    // calculate quadratic terms
	    for (unsigned int iBixelk=0; iBixelk<=iBixelj; iBixelk++)
	    {
		if ( Temp_LinearTerms[iBixelk] > 0 )
		{
		    // check correlation of beamlets
		    if ( !bixels_correlated(iBixelj,iBixelk) )
		    {
			// bixels are uncorrelated, have take the pre
			QuadraticTerms[iBixelj][iBixelk] += Temp_LinearTerms[iBixelj] * Temp_LinearTerms[iBixelk];
		    }
		}
	    }
	}
    }
}

/**
 * write results
 */
void GeneralAuxiliaryRangeModel::write_dose(BixelVector &bixel_grid,
					    DoseInfluenceMatrix &dij,
					    string file_prefix,
					    Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}



// ------------------------- DiscreteAuxiliaryRangeModel ---------------------------


/**
 * constructor
 */
DiscreteAuxiliaryRangeModel::DiscreteAuxiliaryRangeModel(
    DoseInfluenceMatrix &Dij,
    const Geometry *Geom,
    const DoseDeliveryModel::options *options)
    : AuxiliaryRangeModel(Dij,Geom,options)
{
    cout << "constructing DiscreteAuxiliaryRangeModel ..." << endl;
}

/**
 * generate a random sample scenario
 */
void DiscreteAuxiliaryRangeModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    random_scenario = get_random_dose_eval_scenario();
}

/**
 * write results
 */
void DiscreteAuxiliaryRangeModel::write_dose(BixelVector &bixel_grid,
					    DoseInfluenceMatrix &dij,
					    string file_prefix,
					    Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}



