
/**
 * @file DeltaBarDelta.hpp
 * DeltaBarDelta Class Header
 *
 * $Id: DeltaBarDelta.hpp,v 1.1 2007/10/09 22:30:22 bmartin Exp $
 */

#ifndef DELTABARDELTA_HPP
#define DELTABARDELTA_HPP

class DeltaBarDelta;

#include <iostream>
#include <stdexcept>
#include "Optimizer.hpp"
#include "ParameterVectorDirection.hpp"



/**
 * Class DeltaBarDelta is an optimizer object to implement a simple steepest
 * descent algorithm.
 * <p>
 * The overall algorithm works like this:
 * <ol>
 *   <li> Initialization:
 *   <ol>
 *     <li> Take step equal to the negative of the gradient times the step size
 *   </ol>
 *   <li> Later steps:
 *   <ol>
 *     <li> Take step equal to the negative of the gradient times the step size
 *   </ol>
 * </ol>
 *
 * Only needs the objective and gradient at each step.
 */
class DeltaBarDelta : public Optimizer
{

    public:
	class options;

	DeltaBarDelta(
		DeltaBarDelta::options options,
		unsigned int nParams
		);


	virtual ~DeltaBarDelta() {};

	/**
     * DeltaBarDelta options class
	 *
	 * Make sure you update the print_on function when adding or changing
	 * options.
	 */
	class options
	{
        public:
            options() : step_size_(1), r_(0.1), phi_(0.9), theta_(0.7) {};

            // Print options
            friend std::ostream& operator<< (std::ostream& o,
			const DeltaBarDelta::options& opt);

            /// Check the validity of the options (throw exception if not valid)
            void check_options() const;

            /// Number to multiply the default step size by
            double step_size_;

            /// Delta-bar-delta learning rate increment. r_ > 0
            float r_;
            /// Delta-bar-delta learning_rate_decrement. 0 <= phi_ < 1
            float phi_;
            /// Delta-bar-delta gradient smoothing exponent, 0 <= theta_ < 1
            double theta_;
	};

	DeltaBarDelta::options options_;

	/// Calculate the direction based on just the gradient
    void calculate_direction(const ParameterVectorDirection & gradient,
				 const ParameterVector & param);

	/// Print out options
	virtual void print_options(std::ostream& o) const;

    //
    // Delta-bar-delta specific data
    //

    /// Smoothed gradient
    ParameterVectorDirection smoothed_gradient_;

    /// Learning rate for each parameter
    vector<float> learning_rate_;

    bool is_initialized_;
};

#endif
