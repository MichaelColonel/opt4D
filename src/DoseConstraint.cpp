/**
 * @file: DoseConstraint.cpp
 * DoseConstraint Class implementation.
 *
 */

#include "DoseConstraint.hpp"

/**
 * Constructor: Initialize variables.  
 */
DoseConstraint::DoseConstraint(Voi*  the_voi,
			       unsigned int consNo,
			       bool is_max_constraint,
			       bool is_min_constraint,
			       float max_dose,
			       float min_dose,
			       float initial_penalty)
: Constraint(consNo),
  dose_contribution_norm_(),
  the_voi_(the_voi),
  is_max_constraint_(is_max_constraint),
  is_min_constraint_(is_min_constraint),
  max_dose_(max_dose),
  min_dose_(min_dose),
  penalty_(),
  initial_penalty_(initial_penalty/(double)get_nVoxels()),
  lagrange_(),
  in_active_set_(get_nVoxels(),true)
{
  dose_contribution_norm_ = vector<float>(get_nVoxels(),0);
  lagrange_ = vector<float>(get_nVoxels(),0);
  penalty_ = vector<float>(get_nVoxels(),initial_penalty/(double)get_nVoxels());
  reset_active_set();
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
DoseConstraint::~DoseConstraint()
{
}


/**
 * Initialize the objective: calculate norm of dose contribution vectors
 */
void DoseConstraint::initialize(DoseInfluenceMatrix& Dij)
{
  for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
    {
      unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
      for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	                                       iter.get_voxelNo() == voxelNo; iter++) {
	dose_contribution_norm_[iVoxel] += (iter.get_influence()) * (iter.get_influence()); 
      }
    }
  is_initialized_ = true;
  verbose_=false;
}



/**
 * Prints a description of the constraint on the given stream
 *
 * @param o The output stream to write to
 */
void DoseConstraint::printOn(std::ostream& o) const
{
  if(is_max_constraint_) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmaximum dose hard constraint: max dose < " << max_dose_ << endl;
  }
  if(is_min_constraint_ && min_dose_ > 0) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tminimum dose hard constraint: min dose > " << min_dose_ << endl;
  }
}


/**
 * get lagrange multipliers as pairs <voxelNo,value>
 */
vector<pair<unsigned int,float> > DoseConstraint::get_lagrange_multipliers()
{
  cout << "get lagrange multipliers for constraint " << get_consNo() << endl;

  vector<pair<unsigned int,float> > data;

  for(unsigned int i=0; i<lagrange_.size(); i++) {
    unsigned int voxelNo = the_voi_->get_voxel(i);
    data.push_back(pair<unsigned int,float>(voxelNo,lagrange_[i]));
  }
  return data;
}



/**
 * update the lagrange multipliers
 */
void DoseConstraint::update_lagrange_multipliers(const BixelVector & beam_weights,
						 DoseInfluenceMatrix & Dij)
{
  if(verbose_) {
    cout << "updating lagrange multipliers for constraint " << get_consNo() << endl;
  }  

  for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
    {
      unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
      float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
      float lag_buf = lagrange_[iVoxel];
      float penalty = penalty_[iVoxel];

      // if constraint satisfied
      float temp_lagrange = 0;

      // upper bound active
      if(is_max_constraint_) {
		double cons = dose_buf-max_dose_;
		if(-1*lag_buf/penalty < cons) {
		  temp_lagrange = lag_buf + penalty*cons;
		}
      }

      // lower bound active
      if(is_min_constraint_) {
		double cons = dose_buf-min_dose_;
		if(-1*lag_buf/penalty > cons) {
		  temp_lagrange = lag_buf + penalty*cons;
		}
      }

      lagrange_[iVoxel] = temp_lagrange;
    }
}


/**
 * update the penalty factor
 */
void DoseConstraint::update_penalty(const BixelVector & beam_weights,
									DoseInfluenceMatrix & Dij, 
									float tol, float multiplier)
{
    size_t nVoxels = the_voi_->get_nVoxels();
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	  
	  // Find the voxelNo of the voxel we are looking at
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // Calculate dose to voxel
	  float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);

	  // upper bound active
	  if(is_max_constraint_) {
		double cons = dose_buf-max_dose_;
		if(cons > tol*max_dose_) { // constraint violated by more than the tolerance
		  penalty_[iVoxel] *= multiplier;
		}
	  }
	  
	  // lower bound active
	  if(is_min_constraint_) {
		double cons = dose_buf-min_dose_;
		if(cons < -1*tol*min_dose_) {
		  penalty_[iVoxel] *= multiplier;
		}
	  }
	}
}

/**
 * Calculate contribution of constraint to augmented Lagrangian
 */
double DoseConstraint::calculate_aug_lagrangian(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
		double & constraint,
		double & merit)
{
  if(verbose_) {
    cout << "calc augmented lagragian for constraint " << get_consNo() << endl;
  }

    double objective = 0;
	constraint = 0;
	merit = 0;
    double voxel_lagrange = 0;
    double voxel_cons = 0;
	
    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	  
	  // Find the voxelNo of the voxel we are looking at
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // Calculate dose to voxel
	  float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
	  float lag_buf = lagrange_[iVoxel];
	  float penalty = penalty_[iVoxel];

	  // unconstrained minimum of penalty function
	  voxel_lagrange = -0.5*lag_buf*lag_buf/penalty;
	  voxel_cons=0;	  

	  // upper bound active
	  if(is_max_constraint_) {
		double cons = dose_buf-max_dose_;
		if(-1*lag_buf/penalty < cons) {
		  voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;
		}
		if(cons > 0) {
		  voxel_cons = cons;
		}
	  }
	  
	  // lower bound active
	  if(is_min_constraint_) {
		double cons = dose_buf-min_dose_;
		if(-1*lag_buf/penalty > cons) {
		  voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;
		}
		if(cons < 0) {
		  voxel_cons = -1*cons;
		}
	  }
	  
	  // lagrange function
	  objective += voxel_lagrange;

	  // merit function returns the quadratic penalty term with the initial penalty factor
	  merit += voxel_cons*voxel_cons;

	  // returns the maximum constraint violation for any voxel in the VOI
	  if(voxel_cons > constraint) {
		constraint = voxel_cons;
	  }
    }
	
	merit *= 0.5*initial_penalty_;
    return objective; 
}



/**
 * Calculate contribution of constraint to augmented Lagrangian and its gradient
 */
double DoseConstraint::calculate_aug_lagrangian_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier)
{
  if(verbose_) {
    cout << "calc gradient of augmented lagragian for constraint " << get_consNo() << endl;
  }

    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;
    double voxel_lagrange = 0;

    // loop over all voxels in this VOI and look up dose deviation
    size_t nVoxels = the_voi_->get_nVoxels();
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {

        // Find the voxelNo of the voxel we are looking at
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

        // Calculate dose to voxel
        float dose_buf = Dij.calculate_voxel_dose(voxelNo, beam_weights);
		float lag_buf = lagrange_[iVoxel];
		float penalty = penalty_[iVoxel];

	// unconstrained minimum of penalty function
	voxel_lagrange = -0.5*lag_buf*lag_buf/penalty;

	// upper bound active
	if(is_max_constraint_) {
	  double cons = dose_buf-max_dose_;
	  if(-1*lag_buf/penalty < cons) {
	    voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;

	    double factor = lag_buf + penalty * cons;
	    Dij.calculate_voxel_dose_gradient(voxelNo,
					      gradient,
					      factor * gradient_multiplier);
	  }
	}

	// lower bound active
	if(is_min_constraint_) {
	  double cons = dose_buf-min_dose_;
	  if(-1*lag_buf/penalty > cons) {
	    voxel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;

	    double factor = lag_buf + penalty * cons;
	    Dij.calculate_voxel_dose_gradient(voxelNo,
					      gradient,
					      factor * gradient_multiplier);
	  }
	}

	objective += voxel_lagrange;
    }
    return objective;
}

        

/**
 * sequentially project onto voxel constraints
 */
unsigned int DoseConstraint::project_onto(BixelVector & beam_weights,
					  DoseInfluenceMatrix & Dij)
{
  unsigned int nProjections = 0;

  // cout << "projecting onto constraint " << get_consNo() << ": ";

  for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
    {
      if(is_active(iVoxel)) 
	{
	  // Find the voxelNo of the voxel we are looking at
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // Calculate dose to voxel
	  float dose = Dij.calculate_voxel_dose(voxelNo, beam_weights);

	  // need to distinguish four cases
	  float factor;
	  bool project = false;
	  if(dose>max_dose_){
	    project = true;
	    if(dose-max_dose_<0.5*(max_dose_-min_dose_)) {
	      factor = -2*(dose-max_dose_); 
	    }
	    else {
	      factor = -1*(dose-max_dose_) - 0.5*(max_dose_-min_dose_); 
	    }
	  } 
	  if (dose<min_dose_){
	    project = true;
	    if(min_dose_-dose<0.5*(max_dose_-min_dose_)) {
	      factor = 2*(min_dose_-dose);
	    }
	    else {
	      factor = (min_dose_-dose) + 0.5*(max_dose_-min_dose_);
	    }
	  }

	  if(project) {
	    // divide by the norm of the dij column
	    factor = factor / dose_contribution_norm_[iVoxel];

	    // do the actual weight change
	    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
		                                     iter.get_voxelNo() == voxelNo; iter++) {
	      beam_weights[iter.get_bixelNo()] += factor * iter.get_influence();
	    }
	    // increment projection counter
	    nProjections++;
	  }
	  else {
	    // constraint satisfied, no projection needed
	    // remove voxel from active set
	    in_active_set_[iVoxel] = false;
	  }
	}
    }
  //  cout << nProjections << " projections" << endl;
  return nProjections;
}

/**
 * returns total number of constraints for commercial solver interface
 */
unsigned int DoseConstraint::get_nGeneric_constraints() const
{
    return(get_nVoxels());
}

/**
 * prepare constraints for commercial solver interface
 */
void DoseConstraint::add_to_optimization_data_set(GenericOptimizationData & data, DoseInfluenceMatrix & Dij) const
{
  cout << "Creating generic constraint set for CONS " << get_consNo() << endl;

  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

    // Find the voxelNo of the voxel we are looking at
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
 
    // get number of nonzero dij elements
    unsigned int nEntries = Dij.get_nEntries_per_voxel(voxelNo);

    // add to number of nonzero coefficients
    data.nNonZeros_ += nEntries;

    // initialize next constraint
    data.constraints_[data.nConstraints_until_now_].initialize(nEntries);

    // the coefficients are just the Dij elements
    unsigned int iEntry = 0;
    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
 	data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iter.get_bixelNo();
	data.constraints_[data.nConstraints_until_now_].value_[iEntry] = iter.get_influence();
	iEntry++;
    }

    // bounds
    if(is_max_constraint_) {
      //	data.constraints_.back().upper_bound_ = max_dose_;
      data.constraints_[data.nConstraints_until_now_].upper_bound_ = max_dose_;
    }
    else {
      //	data.constraints_.back().upper_bound_ = OPT4D_INFINITY;
      data.constraints_[data.nConstraints_until_now_].upper_bound_ = OPT4D_INFINITY;
    }

    if(is_min_constraint_) {
      //	data.constraints_.back().lower_bound_ = min_dose_;
      data.constraints_[data.nConstraints_until_now_].lower_bound_ = min_dose_;
    }
    else {
      //	data.constraints_.back().lower_bound_ = -OPT4D_INFINITY;
      data.constraints_[data.nConstraints_until_now_].lower_bound_ = -OPT4D_INFINITY;
    }

    // constraint type
    if(is_max_constraint_ && is_min_constraint_) {
	if(abs(max_dose_-min_dose_) < OPT4D_EPSILON) {
	  //	    data.constraints_.back().type_ = EQUALITY;
	  data.constraints_[data.nConstraints_until_now_].type_ = EQUALITY;
	}
	else {
	  //data.constraints_.back().type_ = TWOSIDED;
	  data.constraints_[data.nConstraints_until_now_].type_ = TWOSIDED;
	}
    }
    else if(is_max_constraint_ && !is_min_constraint_) {
      //	data.constraints_.back().type_ = UPPERBOUND;
      data.constraints_[data.nConstraints_until_now_].type_ = UPPERBOUND;
    }
    else if(!is_max_constraint_ && is_min_constraint_) {
      //	data.constraints_.back().type_ = LOWERBOUND;
      data.constraints_[data.nConstraints_until_now_].type_ = LOWERBOUND;
    }
    else {
	cout << "WARNING: CONS " << get_consNo() << " has neither max nor min dose." << endl; 
	//	data.constraints_.back().type_ = TWOSIDED; // bounds are +/- infinity
	data.constraints_[data.nConstraints_until_now_].type_ = FREE; 
    }

    // increment constrait counter
    data.nConstraints_until_now_ += 1;

  }
}
  

  




