/**
 * @file: VirtualBixelModel.cpp
 * VirtualBixelModel Class implementation.
 *
 */

#include "VirtualBixelModel.hpp"
#include "KonradDoseInfluenceMatrix.hpp"
#include "KonradBixelVector.hpp"
#include "DijFileParser.hpp"
#include <sstream>
#include <fstream>

using std::ifstream;

// ---------------------------------------------------------------------
// Base class
// ---------------------------------------------------------------------

/**
 * Constructor that reads the virtual bixels from file
 */
VirtualBixelModel::VirtualBixelModel(const Geometry *the_geometry,
				     const string dij_file_root,
				     const string stf_file_root)
    : DoseDeliveryModel(the_geometry),
      virtual_geometry_(),
      virtual_bixels_(),
      virtual_dij_(),
      physical_bixel_subscripts_()
{
    cout << endl << "constructing the virtual bixel model base class ... " << endl; 

    unsigned int nBeams = the_geometry->get_nBeams();

    // construct the virtual geometry
    virtual_geometry_.reset(new Geometry(*the_geometry,0,0));

    // Construct virtual bixel vector
    virtual_bixels_.reset(new KonradBixelVector(virtual_geometry_.get(), 1, nBeams, stf_file_root, stf_file_root));
	    
    // read virtual bixel Dij
    virtual_dij_.reset(new KonradDoseInfluenceMatrix(dij_file_root, 
						 virtual_geometry_.get(), 
						 nBeams, 1, *virtual_bixels_, true));

    // calculate the subscripts of the physical bixels in the virtual bixel grid
    unsigned int nBixels = the_geometry->get_nBixels();
    unsigned int beamNo;
    physical_bixel_subscripts_.resize(nBixels);
    for(unsigned int iBixel=0; iBixel<nBixels; iBixel++) {
	beamNo = the_geometry->get_beamNo(iBixel);
	physical_bixel_subscripts_[iBixel] = 
	    virtual_geometry_->convert_to_Bixel_Subscripts(the_geometry_->get_bixel_position(iBixel), beamNo);
	// make sure the subscript is valid
	virtual_geometry_->assert_valid_bixel(physical_bixel_subscripts_[iBixel],beamNo);
    }
}


/**
 * Destructor
 */
VirtualBixelModel::~VirtualBixelModel()
{
}

/**
 * calculate voxel dose for a scenario
 */
float VirtualBixelModel::calculate_voxel_dose(
    unsigned int voxelNo,	
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &junk_dij, // isn't actually used
    const RandomScenario &random_scenario) const
{
    unsigned int virt_bixelNo;
    float dose_buf = 0;
    float infl_buf = 0;

    for(DoseInfluenceMatrix::iterator iter_dij = virtual_dij_->begin_voxel(voxelNo);
	iter_dij.get_voxelNo() == voxelNo; iter_dij++) {
	// set virtual bixel No
	virt_bixelNo = iter_dij.get_bixelNo();

	// check if this bixel has an entry in the trafo matrix
	if(random_scenario.virtual_bixel_trafo_.bixel_used(virt_bixelNo)) {

	    // iterate over the physical beamlets which contribute
	    infl_buf = 0;
	    for(TrafoMatrix::iterator iter_trafo = random_scenario.virtual_bixel_trafo_.begin_virtual(virt_bixelNo);
		iter_trafo.get_virtualNo() == virt_bixelNo; iter_trafo++) {
		infl_buf += bixel_grid[iter_trafo.get_physicalNo()] * iter_trafo.get_value();
	    }
	    // add up dose
	    dose_buf += infl_buf * iter_dij.get_influence();
	}
    }
    return dose_buf;
}


/**
 * calculate gradient contribution of the voxel for a scenario
 */
void VirtualBixelModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,	
	const BixelVector &bixel_grid,                  
        DoseInfluenceMatrix &junk_dij, // isn't actually used
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{
    unsigned int virt_bixelNo;
    for(DoseInfluenceMatrix::iterator iter_dij = virtual_dij_->begin_voxel(voxelNo);
	iter_dij.get_voxelNo() == voxelNo; iter_dij++) {
	// set virtual bixel No
	virt_bixelNo = iter_dij.get_bixelNo();
	// check if this bixel has an entry in the trafo matrix
	if(random_scenario.virtual_bixel_trafo_.bixel_used(virt_bixelNo)) {
	    // iterate over the physical beamlets which contribute
	    for(TrafoMatrix::iterator iter_trafo = random_scenario.virtual_bixel_trafo_.begin_virtual(virt_bixelNo);
		iter_trafo.get_virtualNo() == virt_bixelNo; iter_trafo++) {
		// add to gradient
		gradient[iter_trafo.get_physicalNo()] += gradient_multiplier *
                    iter_dij.get_influence() * iter_trafo.get_value();
	    }
	}
    }
}


// ---------------------------------------------------------------------
// Setup uncertainty
// ---------------------------------------------------------------------

/**
 * Constructor
 */
VirtualBixelSetupModel::VirtualBixelSetupModel(const Geometry *the_geometry,
					       const DoseDeliveryModel::options *options)
    : VirtualBixelModel(the_geometry,
			options->virtual_bixel_dij_root_, 
			options->virtual_bixel_stf_root_)
{
}

/**
 * Destructor
 */
VirtualBixelSetupModel::~VirtualBixelSetupModel()
{
}


/**
 * generate virtual bixel transformation matrix based on patient shift
 */
void VirtualBixelSetupModel::generate_virtual_bixel_trafo_matrix(
    RandomScenario &random_scenario) const
{
    TrafoMatrix &trafo = random_scenario.virtual_bixel_trafo_; 
    Shift_in_Patient_Coord setup_shift = random_scenario.virtual_bixel_patient_shifts_[0]; 

    Bixel_Subscripts temp_sub;
    unsigned int temp_No;
    unsigned int nErrors = 0;

    // clear trafo matrix
    trafo.reset(the_geometry_->get_nBixels(), virtual_geometry_->get_nBixels(), true);

    // create temporary vector to store shifts 
    // in Collimator coordinates for each beam
    vector<Bixel_Shift_Subscripts> shifts(0);

    // get discretized shift projections
    for(unsigned int iBeam = 0; iBeam<virtual_geometry_->get_nBeams(); iBeam++) {
	shifts.push_back( virtual_geometry_->convert_to_Lateral_Bixel_Shift_Subscripts(
			      virtual_geometry_->transform_to_Shift_in_Collimator_Coord(setup_shift,iBeam)*(-1),iBeam));
    }

    // loop over the physical beamlets
    for(unsigned int iBixel = 0; iBixel<the_geometry_->get_nBixels(); iBixel++) {

	unsigned int beamNo = the_geometry_->get_beamNo(iBixel);

	// get subscript of bixel
	temp_sub = get_physical_bixel_subscripts(iBixel);

	// add shift 
	temp_sub = temp_sub + shifts[beamNo];

	// check whether this is a valid bixel
	if(virtual_geometry_->is_valid_bixel(temp_sub,beamNo)) {

	    // convert it back to a bixelNo
	    temp_No = virtual_geometry_->get_bixelNo(temp_sub,beamNo);
	    
	    // push back new element in the transformation matrix
	    trafo.push_back_element(iBixel, temp_No, 1);
	}
	else { // this should not happen, the bixel grid is too small

	    nErrors++;

	    // don't shift bixel, just get correct virtual bixel number
	    temp_No = virtual_geometry_->get_bixelNo(get_physical_bixel_subscripts(iBixel),beamNo);
	    
	    // push back new element in the transformation matrix
	    trafo.push_back_element(iBixel, temp_No, 1);
	}
    }
    // initialize the transformation matrix
    trafo.initialize();

    // print warning
    if(nErrors>0) {
	cout << "WARNING: could not shift " << nErrors 
	     << " bixels for shift " << setup_shift << endl;
    }    
}

/**
 * generate nominal scenario
 */
void VirtualBixelSetupModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // setup error
    random_scenario.virtual_bixel_patient_shifts_.resize(1);
    random_scenario.virtual_bixel_patient_shifts_[0] = Shift_in_Patient_Coord(0,0,0);
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * store a dose evaluation scenario
 */
bool VirtualBixelSetupModel::set_dose_evaluation_scenario(
    map<string,string> scenario_map)
{

    float x,y,z,dummy,prob;
    string sx,sy,sz;
    unsigned int beamNo;
    RandomScenario scenario;

    // ----------------------- probability -----------------------------------

    std::istringstream iss_p(scenario_map["prob"]);

    if(!(iss_p >> dummy).fail()) {
	prob = dummy;
    }
    else {
	return false;
    }

    // ----------------------- setup -----------------------------------

    std::istringstream iss_s(scenario_map["setup"]);

    // read the string
    if((iss_s >> sx >> x >> sy >> y >> sz >> z).fail()) {
	return false;
    }
    if(!(sx=="x" && sy=="y" && sz=="z")) {
	return false;
    }

    // read shifts successfully 
    Shift_in_Patient_Coord shift = Shift_in_Patient_Coord(x,y,z);
    
    // set values in scenario
    scenario.virtual_bixel_patient_shifts_.resize(1);
    scenario.virtual_bixel_patient_shifts_[0] = shift;


    // ----------------------- store scenario ---------------------


    // generate transformation matrix for range shifts
    generate_virtual_bixel_trafo_matrix(scenario);
	
    // store scenario
    dose_eval_scenarios_.push_back(scenario);
    dose_eval_pdf_.push_back(prob);
    nDose_eval_scenarios_++;

    // print info to screen	
    cout << "set dose evaluation scenario " << dose_eval_scenarios_.size() << endl;
    cout << "Probability: " << prob << endl;
    cout << "Patient shift: " << shift << endl;
    cout << "virtual bixel shifts:" << endl;

    Bixel_Shift_Subscripts temp;
    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	// setup
        temp = virtual_geometry_->convert_to_Lateral_Bixel_Shift_Subscripts(
	    virtual_geometry_->transform_to_Shift_in_Collimator_Coord(shift,iBeam)*(-1),iBeam);

	cout << "beam " << iBeam << ": " << temp << endl;
    }

    return(true);
}


/**
 * Constructor
 */
ContinuousVirtualBixelSetupModel::ContinuousVirtualBixelSetupModel(
    const Geometry *the_geometry,
    const DoseDeliveryModel::options *options)
    : VirtualBixelSetupModel(the_geometry,options)
    , setup_uncertainty_(GaussianSystematicSetupError(options))
{
    cout << "Constructing ContinuousVirtualBixelSetupModel:" << endl;
}

/**
 * Destructor
 */
ContinuousVirtualBixelSetupModel::~ContinuousVirtualBixelSetupModel()
{
}

/**
 * generate sample scenario
 */
void ContinuousVirtualBixelSetupModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    // generate setup error  
    random_scenario.virtual_bixel_patient_shifts_.resize(1);
    random_scenario.virtual_bixel_patient_shifts_[0] = setup_uncertainty_.get_truncated_gaussian_setup_error();

    // generate virtual bixel transformation matrix
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * write results
 */
void ContinuousVirtualBixelSetupModel::write_dose(BixelVector &bixel_grid,
						  DoseInfluenceMatrix &dij,
						  string file_prefix, 
						  Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}


/**
 * constructor
*/
GaussianSystematicSetupError::GaussianSystematicSetupError(
    const DoseDeliveryModel::options *options)
    : sigmaX_(options->gaussian_setup_options_.systematic_sigmaX_)
    , sigmaY_(options->gaussian_setup_options_.systematic_sigmaY_)
    , sigmaZ_(options->gaussian_setup_options_.systematic_sigmaZ_)
{
    cout << "GaussianSystematicSetupError: " << endl;
    cout << "sigmaX (LR): " << sigmaX_ << endl;
    cout << "sigmaY (SI): " << sigmaY_ << endl;
    cout << "sigmaZ (AP): " << sigmaZ_ << endl;
}

/**
 * gives a 3D Gaussian setup error in a 2sigma elipse
*/
Shift_in_Patient_Coord GaussianSystematicSetupError::get_truncated_gaussian_setup_error(float cutoff) const
{
    float epsilon = 0.001;
    float tmpx,tmpy,tmpz;
    unsigned int tryNo = 0;
    unsigned int maxTry = 100;

    Shift_in_Patient_Coord temp;

    while(tryNo<maxTry) {

	// generate random error
	temp.x_ = get_truncated_normal_distributed_random_number(sigmaX_);
	temp.y_ = get_truncated_normal_distributed_random_number(sigmaY_);
	temp.z_ = get_truncated_normal_distributed_random_number(sigmaZ_);

	if(comp_float_abs(sigmaX_,0.0,epsilon)) {
	    tmpx = 0;
	}
	else { 
	    tmpx = temp.x_/(sigmaX_*cutoff);
	}
	if(comp_float_abs(sigmaY_,0.0,epsilon)) {
	    tmpy = 0;
	}
	else { 
	    tmpy = temp.y_/(sigmaY_*cutoff);
	}
	if(comp_float_abs(sigmaZ_,0.0,epsilon)) {
	    tmpz = 0;
	}
	else { 
	    tmpz = temp.z_/(sigmaZ_*cutoff);
	}
	
	// check if this shift is within cutoff*sigma elipse
	if(tmpx*tmpx + tmpy*tmpy + tmpz*tmpz < 1) {
	    return Shift_in_Patient_Coord(temp);
	}
	
	// increment counter
	tryNo++;
    }

    // check if max number of trys the get a valid shift is exceeded
    cout << "Number of attempts to get a valid setup shift exceeded " << maxTry << endl;
    throw(std::runtime_error("cannot get valid setup shift"));
}

/**
 * gives gaussian random number from a truncated distribution
*/
float GaussianSystematicSetupError::get_truncated_normal_distributed_random_number(float width, float cutoff) const
{
    int iX,iY;
    float fX,fY;
    float fRandomNumber;
    int maxTry = 100;
    int tryNo = 0;

    while(tryNo<maxTry) {

	iX = rand();
	iY = rand();
	
	fX = ((double)iX)/RAND_MAX;
	fY = ((double)iY)/RAND_MAX;
	
	fRandomNumber = width*sqrt(-2*log(fX))*cos(2*M_PI*fY);

	if(fabs(fRandomNumber) <= cutoff*width) {
	    return(fRandomNumber);
	}

	tryNo++;
    }

    cout << "tried " << maxTry << " times to generate a Gaussian distributed random number" << endl;
    cout << " with sigma " << width << " and cutoff " << cutoff << endl; 
    throw(std::runtime_error("cannot generate Gaussian random number"));
}

/** 
 * Compare two floats using an absolute tolerance
 */
bool GaussianSystematicSetupError::comp_float_abs(float a, float b, float epsilon) const
{
    if(fabs(a-b) < epsilon) {
	return true;
    }
    return false;
}


// ---------------------------------------------------------------------
// Range uncertainty
// ---------------------------------------------------------------------

/**
 * Constructor
 */
VirtualBixelRangeModel::VirtualBixelRangeModel(const Geometry *the_geometry,
					       const DoseDeliveryModel::options *options)
    : VirtualBixelModel(the_geometry,
			options->virtual_bixel_dij_root_, 
			options->virtual_bixel_stf_root_)
{
    // make sure distance of energy layers is known
    if(options->range_dWEL_ == 0) {
	throw(std::runtime_error("cannot create VirtualBixelRangeModel if range_dWEL_ is unknown"));
    }
    else {
	// set axial resolution of beam spots in geometry class
	virtual_geometry_->set_beam_dWEL(vector<float>(virtual_geometry_->get_nBeams(),options->range_dWEL_));
    }
}

/**
 * Destructor
 */
VirtualBixelRangeModel::~VirtualBixelRangeModel()
{
}

/**
 * generate virtual bixel transformation matrix based on range shifts
 */
void VirtualBixelRangeModel::generate_virtual_bixel_trafo_matrix(
    RandomScenario &random_scenario) const
{
    TrafoMatrix &trafo = random_scenario.virtual_bixel_trafo_; 

    Bixel_Subscripts temp_sub;
    Bixel_Shift_Subscripts temp_shift;
    unsigned int temp_No;
    unsigned int nErrors = 0;

    // clear trafo matrix
    trafo.reset(the_geometry_->get_nBixels(), virtual_geometry_->get_nBixels(), true);

    // loop over the physical beamlets
    for(unsigned int iBixel = 0; iBixel<the_geometry_->get_nBixels(); iBixel++) {

	unsigned int beamNo = the_geometry_->get_beamNo(iBixel);

	// get subscript of bixel
	temp_sub = get_physical_bixel_subscripts(iBixel);

	// convert range shift to subscripts shift
	temp_shift = virtual_geometry_->convert_to_Axial_Bixel_Shift_Subscripts(
	    random_scenario.range_shifts_[iBixel],beamNo);

	// shift bixel
	temp_sub = temp_sub + temp_shift;

	// check whether this is a valid bixel
	if(virtual_geometry_->is_valid_bixel(temp_sub,beamNo)) {

	    // convert it back to a bixelNo
	    temp_No = virtual_geometry_->get_bixelNo(temp_sub,beamNo);
	    
	    // push back new element in the transformation matrix
	    trafo.push_back_element(iBixel, temp_No, 1);
	}
	else { // this should not happen, the bixel grid is too small

	    nErrors++;

	    // don't shift bixel, just get correct virtual bixel number
	    temp_No = virtual_geometry_->get_bixelNo(get_physical_bixel_subscripts(iBixel),beamNo);
	    
	    // push back new element in the transformation matrix
	    trafo.push_back_element(iBixel, temp_No, 1);
	}
    }
    // initialize the transformation matrix
    trafo.initialize();

    // print warning
    if(nErrors>0) {
	cout << "WARNING: could not shift " << nErrors << " bixels" << endl;
    }    
}

/**
 * generate nominal scenario
 */
void VirtualBixelRangeModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // range error
    random_scenario.range_shifts_.resize(the_geometry_->get_nBixels());
    fill(random_scenario.range_shifts_.begin(),random_scenario.range_shifts_.end(),0);
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * store a dose evaluation scenario
 */
bool VirtualBixelRangeModel::set_dose_evaluation_scenario(
    map<string,string> scenario_map)
{

    float dummy,prob;
    unsigned int beamNo;
    vector<float> rangeShifts(0);
    RandomScenario scenario;

    // ----------------------- probability -----------------------------------

    std::istringstream iss_p(scenario_map["prob"]);

    if(!(iss_p >> dummy).fail()) {
	prob = dummy;
    }
    else {
	return false;
    }

    // ----------------------- range -----------------------------------

    scenario.range_shifts_.resize(the_geometry_->get_nBixels());

    std::istringstream iss_r(scenario_map["range"]);

    // read the beam number
    while(!(iss_r >> beamNo).fail() 
	  && beamNo <= the_geometry_->get_nBeams()) {
	
	// read the range shift for this beam
	if(!(iss_r >> dummy).fail()) {
	    rangeShifts.push_back(dummy);
	}
	else {
	    return false;
	}
    }

    // check if all beams are supplied
    if(rangeShifts.size() != the_geometry_->get_nBeams()) {
	return false;
    }

    // set range shift for each bixel
    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	beamNo = the_geometry_->get_beamNo(iBixel);
	scenario.range_shifts_[iBixel] = rangeShifts[beamNo];
    }


    // ----------------------- store scenario ---------------------


    // generate transformation matrix for range shifts
    generate_virtual_bixel_trafo_matrix(scenario);
	
    // store scenario
    dose_eval_scenarios_.push_back(scenario);
    dose_eval_pdf_.push_back(prob);
    nDose_eval_scenarios_++;

    // print info to screen	
    cout << "set dose evaluation scenario " << dose_eval_scenarios_.size() << endl;
    cout << "Probability: " << prob << endl;
    cout << "Range shifts: " << endl;

    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	cout << "beam " << iBeam << ": " << rangeShifts[iBeam] << endl;
    }

    cout << "virtual bixel shifts:" << endl;

    Bixel_Shift_Subscripts temp;
    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {

	temp = virtual_geometry_->convert_to_Axial_Bixel_Shift_Subscripts(
	    rangeShifts[iBeam],iBeam);

	cout << "beam " << iBeam << ": " << temp << endl;
    }

    return(true);
}


/**
 * Constructor
 */
ContinuousVirtualBixelRangeModel::ContinuousVirtualBixelRangeModel(
    const Geometry *the_geometry,
    const DoseDeliveryModel::options *options)
    : VirtualBixelRangeModel(the_geometry,options)
    , range_uncertainty_(GaussianSystematicRangeError(the_geometry_,virtual_geometry_.get(),options))
{
    cout << "Constructing ContinuousVirtualBixelRangeModel:" << endl;
}

/**
 * Destructor
 */
ContinuousVirtualBixelRangeModel::~ContinuousVirtualBixelRangeModel()
{
}

/**
 * generate sample scenario
 */
void ContinuousVirtualBixelRangeModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    // range error
    range_uncertainty_.generate_gaussian_range_error(random_scenario.range_shifts_);
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * write results
 */
void ContinuousVirtualBixelRangeModel::write_dose(BixelVector &bixel_grid,
						  DoseInfluenceMatrix &dij,
						  string file_prefix, 
						  Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}

/**
 * Constructor
 */
GaussianSystematicRangeError::GaussianSystematicRangeError(
    const Geometry *the_geometry,
    const Geometry *virtual_geometry,
    const DoseDeliveryModel::options *options)
    : the_geometry_(the_geometry)
    , virtual_geometry_(virtual_geometry)
    , correlation_model_(options->range_correlation_model_)
    , sigmaRange_(the_geometry_->get_nBixels(),0)
    , virtual_sigmaRange_(virtual_geometry_->get_nBixels(),0)
{
  // read bixel dependent range uncertainties if provided
  if(options->range_sigma_file_root_ != "") {
    read_bixel_range_uncertainty(options->range_sigma_file_root_);
  }
  // same range uncertainty for all bixels
  else {
    sigmaRange_ = vector<float>(the_geometry_->get_nBixels(),options->range_sigma_);
    virtual_sigmaRange_ = vector<float>(virtual_geometry_->get_nBixels(),options->range_sigma_);
  }

  // print info
  cout << "GaussianSystematicRangeError:" << endl;
  cout << "bixel correlation model: " << correlation_model_ << endl;

  if(options->range_sigma_file_root_ != "") {
    cout << "sigmaRange : bixel dependent" << endl;
  }
  else {
    cout << "sigmaRange : " << options->range_sigma_ << endl;
  }

}

/**
 * returns true if two beamlets are correlated according to the specified correlation_model.
 *
 */
bool GaussianSystematicRangeError::bixels_correlated(unsigned int BixelNo_j,unsigned int BixelNo_k) const
{

    if(correlation_model_ == "within_beam")
    {
	if( the_geometry_->get_beamNo(BixelNo_j) == the_geometry_->get_beamNo(BixelNo_k) )
	{
	    return(true);
	}
	return(false);	
    }
    else if(correlation_model_ == "correlated")
    {
        return(true);
    }
    else if(correlation_model_ == "lateral")
    {
	if( the_geometry_->get_beamNo(BixelNo_j) == the_geometry_->get_beamNo(BixelNo_k) )
	{
	  if((the_geometry_->get_bixel_subscript(BixelNo_j)).iX_ == (the_geometry_->get_bixel_subscript(BixelNo_k)).iX_)
	    {
	      if((the_geometry_->get_bixel_subscript(BixelNo_j)).iY_ == (the_geometry_->get_bixel_subscript(BixelNo_k)).iY_)
		{
		  return(true);
		}
	      return(false);
	    }
	  return(false);
	}
	return(false);	
    }
    else 
    {
	throw(std::runtime_error("Unknown beamlet correlation model"));
    }
}

/**
 * generate normalized range shift for each bixel
 */
void GaussianSystematicRangeError::generate_normalized_gaussian_range_error(vector<float> &range_shifts) const
{
    // resize range shifts vector
    range_shifts.resize(the_geometry_->get_nBixels(),0);

    if(correlation_model_ == "within_beam")
    {
	vector<float> temp;
	temp.resize(the_geometry_->get_nBeams());

	// get random number for each beam
	for(unsigned int iBeam=0;iBeam<the_geometry_->get_nBeams();iBeam++) {
	    temp[iBeam] = get_truncated_normal_distributed_random_number(1.0);
	}

	// set range shift for each bixel
	unsigned int iBeam;
	for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	    iBeam = the_geometry_->get_beamNo(iBixel);
	    range_shifts[iBixel] = temp[iBeam];
	}
    }
    else if(correlation_model_ == "correlated")
    {
        float temp = get_truncated_normal_distributed_random_number(1.0);

	// set range shift for each bixel
	for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	    range_shifts[iBixel] = temp;
	}
    }
    else if(correlation_model_ == "lateral")
    {
	vector<float> temp;

	for(unsigned int iBeam=0;iBeam<the_geometry_->get_nBeams();iBeam++) {

	  // get size of bixel grid
	  unsigned int nX = the_geometry_->get_bixel_grid_nX(iBeam);
	  unsigned int nY = the_geometry_->get_bixel_grid_nY(iBeam);

	  // create random variable for every lateral position
	  temp.resize((2*nX+1)*(2*nY+1));
	  for(vector<float>::iterator iter = temp.begin(); iter != temp.end(); iter++) {
	    *iter = get_truncated_normal_distributed_random_number(1.0);
	  }

	  // set range shift for each bixel
	  for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {

	    if(iBeam == the_geometry_->get_beamNo(iBixel)) {

	      unsigned int iX = the_geometry_->get_bixel_subscript(iBixel).iX_;
	      unsigned int iY = the_geometry_->get_bixel_subscript(iBixel).iY_;
	      range_shifts[iBixel] = temp[iY*nX+iX];
	    }
	  }
	}
    }
    else 
    {
	throw(std::runtime_error("Unknown beamlet correlation model"));
    }
}

/**
 * generate range shift for each bixel
 */
void GaussianSystematicRangeError::generate_gaussian_range_error(vector<float> &range_shifts) const
{
  // generate range shifts with a sigma of one
  generate_normalized_gaussian_range_error(range_shifts);
    
  // multiply normalized range shift with the sigma of this bixel
  for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
    range_shifts[iBixel] *= sigmaRange_[iBixel];
  }
}


/**
 * generate range shift for each bixel given a setup error
 */
void GaussianSystematicRangeError::generate_gaussian_range_error(vector<float> &range_shifts, 
								 Shift_in_Patient_Coord shift) const
{
  // generate range shifts with a sigma of one
  generate_normalized_gaussian_range_error(range_shifts);
    
  // create temporary vector to store setup shifts in Collimator coordinates for each beam
  vector<Bixel_Shift_Subscripts> shifts(0);
  
  // get discretized shift projections
  for(unsigned int iBeam = 0; iBeam<virtual_geometry_->get_nBeams(); iBeam++) {
    shifts.push_back( virtual_geometry_->convert_to_Lateral_Bixel_Shift_Subscripts(   
		      virtual_geometry_->transform_to_Shift_in_Collimator_Coord(shift,iBeam)*(-1),iBeam));
  }
  
  unsigned int beamNo;
  unsigned int virt_bixelNo;
  Bixel_Subscripts virt_sub;
  Bixel_Subscripts virt_temp;
  unsigned int nErrors = 0;

  // apply setup shift for every bixel and get range uncertainty of the virtual bixel
  for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
    
    // get beam number
    beamNo = the_geometry_->get_beamNo(iBixel);
    
    // get virtual bixel subscripts of physical bixel
    virt_sub = virtual_geometry_->convert_to_Bixel_Subscripts(the_geometry_->get_bixel_position(iBixel), beamNo);
    virtual_geometry_->assert_valid_bixel(virt_sub,beamNo);

    // add shift
    virt_temp = virt_sub + shifts[beamNo];

    if(virtual_geometry_->is_valid_bixel(virt_temp,beamNo)) {
      // get number of shifted bixel
      virt_bixelNo = virtual_geometry_->get_bixelNo(virt_temp,beamNo);
    }
    else { // this should not happen, the bixel grid is too small
      nErrors++;
      // don't shift bixel, just get correct virtual bixel number
      virt_bixelNo = virtual_geometry_->get_bixelNo(virt_sub,beamNo);
    }

    // multiply the range shift with the sigma
    range_shifts[iBixel] *= virtual_sigmaRange_[virt_bixelNo];
  }
   
  // print warning
  if(nErrors>0) {
    cout << "WARNING: could not shift " << nErrors 
	 << " bixels for shift " << shift << endl;
  }    
}

/**
 * gives gaussian random number from a truncated distribution
*/
float GaussianSystematicRangeError::get_truncated_normal_distributed_random_number(float width, float cutoff) const
{
    int iX,iY;
    float fX,fY;
    float fRandomNumber;
    int maxTry = 100;
    int tryNo = 0;

    while(tryNo<maxTry) {

	iX = rand();
	iY = rand();
	
	fX = ((double)iX)/RAND_MAX;
	fY = ((double)iY)/RAND_MAX;
	
	fRandomNumber = width*sqrt(-2*log(fX))*cos(2*M_PI*fY);

	if(fabs(fRandomNumber) <= cutoff*width) {
	    return(fRandomNumber);
	}

	tryNo++;
    }

    cout << "tried " << maxTry << " times to generate a Gaussian distributed random number" << endl;
    cout << " with sigma " << width << " and cutoff " << cutoff << endl; 
    throw(std::runtime_error("cannot generate Gaussian random number"));
}

/**
 * read file containing range uncertainty for each bixel. 
 * The file has the same syntax as the stf file, except for 
 * an additional column for the range uncertainty sigma
 */
void GaussianSystematicRangeError::read_bixel_range_uncertainty(const string sigma_file_root)
{
  char cbeam[10];
  string sigma_file_name;
  ifstream infile;

  // make sure Geometry class has been initialized
  if(!virtual_geometry_->is_initialized()) {
    throw(std::logic_error("calling DoseDeliveryModel::options::read_virtual_bixel_range_uncertainty before initializing geometry class"));
  }
  
  if(!(sigma_file_root.size() > 0)) {
    cout << "no file name for bixel range uncertainty provided!" << endl;
    throw(std::logic_error("cannot open file"));
  }
  else {
    
    for(unsigned int iBeam=0; iBeam<virtual_geometry_->get_nBeams(); iBeam++) { 
      
      // Open file
      sprintf(cbeam,"%d",(int)(iBeam+1));
      sigma_file_name = sigma_file_root + "_" + string(cbeam) + ".rus"; 
      infile.clear();
      infile.open(sigma_file_name.c_str());
      if (!infile.is_open()) {
	cout << "Can't open file: " << sigma_file_name << endl;
	throw(std::runtime_error("cannot open file"));
      }
      
      cout << "Reading range uncertainty file: " << sigma_file_name << endl;
      
      // prepare header check against geometry class
      DijFileParser::header header_1(*virtual_geometry_,iBeam);
      DijFileParser::header header_2 = header_1;
      
      // Read file header
      while(infile.peek() == '#') {
	infile.ignore(1);
	infile >> std::ws;
	
	string attribute;
	
	infile >> attribute;
	
	if(attribute == "Header" || attribute == "Machinename:") {
	  infile.ignore(256, '\n');
	}
	else if(attribute == "Gantry-Angle:") {
	  infile >> header_2.a_g_;
	}
	else if(attribute == "Table-Angle:") {
	  infile >> header_2.a_t_;
	}
	else if(attribute == "Colli-Angle:") {
	  infile >> header_2.a_c_;
	}
	else if(attribute == "Delta") {
	  char line_buffer[128];
	  infile.getline(line_buffer, 128, '\n');
	  sscanf(line_buffer, "%*s %f %*s %*s %f", &(header_2.dx_b_), &(header_2.dy_b_));
	}
	infile >> std::ws;
      }
      
      // check header against geometry class
      if(!(header_1==header_2)) {
	cout << "header in " << sigma_file_name << " does not match the geometry class!" << endl;
	cout << "In " << sigma_file_name << ": " << header_2 << "\n"
	     << "In Geometry: " << header_1 << "\n";
	throw(std::runtime_error("Inconsistent files"));
      }
      
      // Read file body
      char line_buffer[128];
      unsigned int nCount=0;
      while(infile.good()) {
	
	infile.getline(line_buffer, 128, '\n');
	
	float temp_E(0), temp_x(0), temp_y(0), temp_r(0);
	int n;
	
	sscanf(line_buffer, "%d %f %*s %*s %*s %f %f %f", &n,
	       &temp_E, &temp_x, &temp_y, &temp_r);

	// get bixel position	
	Bixel_Position temp_bpos(temp_x,temp_y,temp_E);
	
	// convert to subscripts in the bixel grid
	Bixel_Subscripts temp_bsub = virtual_geometry_->convert_to_Bixel_Subscripts(temp_bpos,iBeam);
	
	// check if this bixel is contained in the bixel vector
	if(virtual_geometry_->is_valid_bixel(temp_bsub,iBeam)) {

	  nCount++;

	  // get grid index
	  BixelIndex temp_bind = virtual_geometry_->convert_to_BixelIndex(temp_bsub,iBeam);

	  // get bixelNo
	  unsigned int temp_bno = virtual_geometry_->get_bixelNo(temp_bind,iBeam);

	  // set range uncertainty
	  virtual_sigmaRange_.at(temp_bno) = temp_r;
	}
      }
      infile.close();

      // check if all bixels are found
      if(nCount != virtual_geometry_->get_nBixels_in_beam(iBeam)) {
	cout << "read range uncertainty for " <<  nCount << " bixels in beam " << iBeam;
	cout << ", expected " << virtual_geometry_->get_nBixels_in_beam(iBeam) << endl;
	throw(std::runtime_error("Inconsistent files"));
      }
    }
  }
  // set range uncertainty of physical beamlets based on the virtual beamlets
  set_physical_bixel_range_uncertainty();
}

/**
 * set range uncertainty of physical beamlets based on the virtual beamlets
 */
void GaussianSystematicRangeError::set_physical_bixel_range_uncertainty()
{
  unsigned int beamNo;
  unsigned int virt_bixelNo;
  Bixel_Subscripts virt_sub;

  for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
    
    // get beam number
    beamNo = the_geometry_->get_beamNo(iBixel);
    
    // get virtual bixel subscripts of physical bixel
    virt_sub = virtual_geometry_->convert_to_Bixel_Subscripts(the_geometry_->get_bixel_position(iBixel), beamNo);
    virtual_geometry_->assert_valid_bixel(virt_sub,beamNo);

    // get number of shifted bixel
    virt_bixelNo = virtual_geometry_->get_bixelNo(virt_sub,beamNo);

    // copy range uncertainty value
    sigmaRange_[iBixel] = virtual_sigmaRange_[virt_bixelNo];
  }
}



// ---------------------------------------------------------------------
// Range and Setup uncertainty
// ---------------------------------------------------------------------

/**
 * Constructor
 */
VirtualBixelRangeSetupModel::VirtualBixelRangeSetupModel(
    const Geometry *the_geometry,
    const DoseDeliveryModel::options *options)
    : VirtualBixelModel(the_geometry,
			options->virtual_bixel_dij_root_, 
			options->virtual_bixel_stf_root_)
{
    // make sure distance of energy layers is known
    if(options->range_dWEL_ == 0) {
	throw(std::runtime_error("cannot create VirtualBixelRangeModel if range_dWEL_ is unknown"));
    }
    else {
	// set axial resolution of beam spots in geometry class
	virtual_geometry_->set_beam_dWEL(vector<float>(virtual_geometry_->get_nBeams(),options->range_dWEL_));
    }
}

/**
 * Destructor
 */
VirtualBixelRangeSetupModel::~VirtualBixelRangeSetupModel()
{
}

/**
 * generate nominal scenario
 */
void VirtualBixelRangeSetupModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // range error
    random_scenario.range_shifts_.resize(the_geometry_->get_nBixels());
    fill(random_scenario.range_shifts_.begin(),random_scenario.range_shifts_.end(),0);

    // setup error
    random_scenario.virtual_bixel_patient_shifts_.resize(1);
    random_scenario.virtual_bixel_patient_shifts_[0] = Shift_in_Patient_Coord(0,0,0);

    // generate virtual bixel trafo for combined error
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * generate virtual bixel transformation matrix based on patient shift
 */
void VirtualBixelRangeSetupModel::generate_virtual_bixel_trafo_matrix(
    RandomScenario &random_scenario) const
{
    TrafoMatrix &trafo = random_scenario.virtual_bixel_trafo_; 
    Shift_in_Patient_Coord setup_shift = random_scenario.virtual_bixel_patient_shifts_[0]; 

    Bixel_Subscripts temp_sub;
    Bixel_Shift_Subscripts temp_shift;
    unsigned int temp_No;
    unsigned int nErrors = 0;

    // clear trafo matrix
    trafo.reset(the_geometry_->get_nBixels(), virtual_geometry_->get_nBixels(), true);

    // create temporary vector to store setup shifts 
    // in Collimator coordinates for each beam
    vector<Bixel_Shift_Subscripts> shifts(0);

    // get discretized shift projections
    for(unsigned int iBeam = 0; iBeam<virtual_geometry_->get_nBeams(); iBeam++) {
	shifts.push_back( virtual_geometry_->convert_to_Lateral_Bixel_Shift_Subscripts(
			      virtual_geometry_->transform_to_Shift_in_Collimator_Coord(setup_shift,iBeam)*(-1),iBeam));
    }

    // loop over the physical beamlets
    for(unsigned int iBixel = 0; iBixel<the_geometry_->get_nBixels(); iBixel++) {

	unsigned int beamNo = the_geometry_->get_beamNo(iBixel);

	// get subscript of bixel
	temp_sub = get_physical_bixel_subscripts(iBixel);

	// convert range shift to subscripts shift
	temp_shift = virtual_geometry_->convert_to_Axial_Bixel_Shift_Subscripts(
	    random_scenario.range_shifts_[iBixel],beamNo);

	// shift bixel
	temp_sub = temp_sub + temp_shift + shifts[beamNo];

	// check whether this is a valid bixel
	if(virtual_geometry_->is_valid_bixel(temp_sub,beamNo)) {

	    // convert it back to a bixelNo
	    temp_No = virtual_geometry_->get_bixelNo(temp_sub,beamNo);
	    
	    // push back new element in the transformation matrix
	    trafo.push_back_element(iBixel, temp_No, 1);
	}
	else { // this should not happen, the bixel grid is too small

	    nErrors++;

	    // don't shift bixel, just get correct virtual bixel number
	    temp_No = virtual_geometry_->get_bixelNo(get_physical_bixel_subscripts(iBixel),beamNo);
	    
	    // push back new element in the transformation matrix
	    trafo.push_back_element(iBixel, temp_No, 1);
	}
    }
    // initialize the transformation matrix
    trafo.initialize();

    // print warning
    if(nErrors>0) {
	cout << "WARNING: could not shift " << nErrors 
	     << " bixels for shift " << setup_shift << endl;
    }    
}


/**
 * store a dose evaluation scenario
 */
bool VirtualBixelRangeSetupModel::set_dose_evaluation_scenario(
    map<string,string> scenario_map)
{

    float x,y,z,dummy,prob;
    string sx,sy,sz;
    unsigned int beamNo;
    vector<float> rangeShifts(0);
    RandomScenario scenario;

    // ----------------------- probability -----------------------------------

    std::istringstream iss_p(scenario_map["prob"]);

    if(!(iss_p >> dummy).fail()) {
	prob = dummy;
    }
    else {
	return false;
    }

    // ----------------------- range -----------------------------------

    scenario.range_shifts_.resize(the_geometry_->get_nBixels());

    std::istringstream iss_r(scenario_map["range"]);

    // read the beam number
    while(!(iss_r >> beamNo).fail() 
	  && beamNo <= the_geometry_->get_nBeams()) {
	
	// read the range shift for this beam
	if(!(iss_r >> dummy).fail()) {
	    rangeShifts.push_back(dummy);
	}
	else {
	    return false;
	}
    }

    // check if all beams are supplied
    if(rangeShifts.size() != the_geometry_->get_nBeams()) {
	return false;
    }

    // set range shift for each bixel
    for(unsigned int iBixel=0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	beamNo = the_geometry_->get_beamNo(iBixel);
	scenario.range_shifts_[iBixel] = rangeShifts[beamNo];
    }

    // ----------------------- setup -----------------------------------

    std::istringstream iss_s(scenario_map["setup"]);

    // read the string
    if((iss_s >> sx >> x >> sy >> y >> sz >> z).fail()) {
	return false;
    }
    if(!(sx=="x" && sy=="y" && sz=="z")) {
	return false;
    }

    // read shifts successfully 
    Shift_in_Patient_Coord shift = Shift_in_Patient_Coord(x,y,z);
    
    // set values in scenario
    scenario.virtual_bixel_patient_shifts_.resize(1);
    scenario.virtual_bixel_patient_shifts_[0] = shift;


    // ----------------------- store scenario ---------------------


    // generate transformation matrix for range shifts
    generate_virtual_bixel_trafo_matrix(scenario);
	
    // store scenario
    dose_eval_scenarios_.push_back(scenario);
    dose_eval_pdf_.push_back(prob);
    nDose_eval_scenarios_++;

    // print info to screen	
    cout << "set dose evaluation scenario " << dose_eval_scenarios_.size() << endl;
    cout << "Probability: " << prob << endl;
    cout << "Patient shift: " << shift << endl;
    cout << "Range shifts: " << endl;

    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	cout << "beam " << iBeam << ": " << rangeShifts[iBeam] << endl;
    }

    cout << "virtual bixel shifts:" << endl;

    Bixel_Shift_Subscripts temp;
    for(unsigned int iBeam = 0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	// setup
        temp = virtual_geometry_->convert_to_Lateral_Bixel_Shift_Subscripts(
	    virtual_geometry_->transform_to_Shift_in_Collimator_Coord(shift,iBeam)*(-1),iBeam);
	// range
	temp = temp + virtual_geometry_->convert_to_Axial_Bixel_Shift_Subscripts(
	    rangeShifts[iBeam],iBeam);

	cout << "beam " << iBeam << ": " << temp << endl;
    }

    return(true);
}



/**
 * Constructor
 */
ContinuousVirtualBixelRangeSetupModel::ContinuousVirtualBixelRangeSetupModel(
    const Geometry *the_geometry,
    const DoseDeliveryModel::options *options)
    : VirtualBixelRangeSetupModel(the_geometry,options)
    , range_uncertainty_(GaussianSystematicRangeError(the_geometry_,virtual_geometry_.get(),options))
    , setup_uncertainty_(GaussianSystematicSetupError(options))
{
    cout << "Constructing ContinuousVirtualBixelSetupModel:" << endl;
}

/**
 * Destructor
 */
ContinuousVirtualBixelRangeSetupModel::~ContinuousVirtualBixelRangeSetupModel()
{
}

/**
 * generate sample scenario
 */
void ContinuousVirtualBixelRangeSetupModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    // generate setup error  
    random_scenario.virtual_bixel_patient_shifts_.resize(1);
    Shift_in_Patient_Coord shift = setup_uncertainty_.get_truncated_gaussian_setup_error();
    random_scenario.virtual_bixel_patient_shifts_[0] = shift;

    // generate range error (depends on the setup error bixel dependent range uncertainty)
    range_uncertainty_.generate_gaussian_range_error(random_scenario.range_shifts_,shift);

    // generate virtual bixel transformation matrix
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * write results
 */
void ContinuousVirtualBixelRangeSetupModel::write_dose(BixelVector &bixel_grid,
						       DoseInfluenceMatrix &dij,
						       string file_prefix, 
						       Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}


/**
 * Constructor
 */
DiscreteVirtualBixelRangeSetupModel::DiscreteVirtualBixelRangeSetupModel(
    const Geometry *the_geometry,
    const DoseDeliveryModel::options *options)
    : VirtualBixelRangeSetupModel(the_geometry,options)
{
    cout << "Constructing DiscreteVirtualBixelRangeSetupModel:" << endl;
}

/**
 * Destructor
 */
DiscreteVirtualBixelRangeSetupModel::~DiscreteVirtualBixelRangeSetupModel()
{
}

/**
 * generate sample scenario
 */
void DiscreteVirtualBixelRangeSetupModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    random_scenario = get_random_dose_eval_scenario();
}

/**
 * write results
 */
void DiscreteVirtualBixelRangeSetupModel::write_dose(BixelVector &bixel_grid,
						     DoseInfluenceMatrix &dij,
						     string file_prefix, 
						     Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}



// ---------------------------------------------------------------------
// Random and Systematic Setup uncertainty
// ---------------------------------------------------------------------

/**
 * Constructor
 */
VirtualBixelRandomAndSystematicSetupModel::VirtualBixelRandomAndSystematicSetupModel(
    const Geometry *the_geometry,
    const DoseDeliveryModel::options *options)
    : VirtualBixelModel(the_geometry,
			options->virtual_bixel_dij_root_,
			options->virtual_bixel_stf_root_)
    , nFractions_(options->gaussian_setup_options_.nFractions_)
    , sigmaX_sys_(options->gaussian_setup_options_.systematic_sigmaX_)
    , sigmaY_sys_(options->gaussian_setup_options_.systematic_sigmaY_)
    , sigmaZ_sys_(options->gaussian_setup_options_.systematic_sigmaZ_)
    , sigmaX_rand_(options->gaussian_setup_options_.random_sigmaX_)
    , sigmaY_rand_(options->gaussian_setup_options_.random_sigmaY_)
    , sigmaZ_rand_(options->gaussian_setup_options_.random_sigmaZ_)
{
    cout << "Constructing VirtualBixelRandomAndSystematicSetupModel:" << endl;
    cout << "number of fractions: " << nFractions_ << endl;
    cout << "systematic sigmaX (LR): " << sigmaX_sys_ << endl;
    cout << "systematic sigmaY (SI): " << sigmaY_sys_ << endl;
    cout << "systematic sigmaZ (AP): " << sigmaZ_sys_ << endl;
    cout << "random sigmaX (LR): " << sigmaX_rand_ << endl;
    cout << "random sigmaY (LR): " << sigmaY_rand_ << endl;
    cout << "random sigmaZ (LR): " << sigmaZ_rand_ << endl;
}

/**
 * Destructor
 */
VirtualBixelRandomAndSystematicSetupModel::~VirtualBixelRandomAndSystematicSetupModel()
{
}

/**
 * generate sample scenario
 */
void VirtualBixelRandomAndSystematicSetupModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    // setup error
    generate_setup_errors(random_scenario,get_nFractions());

    // generate virtual bixel trafo for combined error
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * generate setup error for every fraction
 */
void VirtualBixelRandomAndSystematicSetupModel::generate_setup_errors(
    RandomScenario &random_scenario, unsigned int nSamples) const
{
    float epsilon = 0.001;
    float tmpx,tmpy,tmpz;
    unsigned int tryNo = 0;
    unsigned int maxTry = 100;

    Shift_in_Patient_Coord temp;
    Shift_in_Patient_Coord sys;

    // resize vector
    random_scenario.virtual_bixel_patient_shifts_.resize(nSamples);

    // generate systematic error
    sys = get_truncated_gaussian_setup_error(sigmaX_sys_,sigmaY_sys_,sigmaZ_sys_);

    // for every fraction
    for(unsigned int iFrac=0; iFrac<nSamples; iFrac++) {

	// generate random error
	temp = get_truncated_gaussian_setup_error(sigmaX_rand_,sigmaY_rand_,sigmaZ_rand_);

	// add together
	temp = temp + sys;

	// store it
	random_scenario.virtual_bixel_patient_shifts_[iFrac] = temp;
    }
}

/**
 * generate nominal scenario
 */
void VirtualBixelRandomAndSystematicSetupModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // setup error
    random_scenario.virtual_bixel_patient_shifts_.resize(1);
    random_scenario.virtual_bixel_patient_shifts_[0] = Shift_in_Patient_Coord(0,0,0);
    generate_virtual_bixel_trafo_matrix(random_scenario);
}

/**
 * generate virtual bixel transformation matrix based on patient shift
 */
void VirtualBixelRandomAndSystematicSetupModel::generate_virtual_bixel_trafo_matrix(
    RandomScenario &random_scenario) const
{
    TrafoMatrix &trafo = random_scenario.virtual_bixel_trafo_; 

    // number of beams
    unsigned int nBeams = virtual_geometry_->get_nBeams();

    // number of patient shifts
    unsigned int nShifts = random_scenario.virtual_bixel_patient_shifts_.size();

    Bixel_Subscripts temp_sub;
    Shift_in_Patient_Coord temp_setup;
    unsigned int temp_No;
    unsigned int beamNo;
    unsigned int nErrors = 0;

    // clear trafo matrix
    trafo.reset(the_geometry_->get_nBixels(), virtual_geometry_->get_nBixels(), true);

    // create temporary vector to store setup shifts 
    // in Collimator coordinates for each beam
    vector<Bixel_Shift_Subscripts> shifts;
    shifts.reserve(virtual_geometry_->get_nBeams());

    // loop over the patient shifts
    for(unsigned int iShift=0; iShift<nShifts; iShift++) { 

	// get patient shift
	temp_setup = random_scenario.virtual_bixel_patient_shifts_[iShift];

	// get discretized shift projections
	shifts.resize(0);
	for(unsigned int iBeam = 0; iBeam<virtual_geometry_->get_nBeams(); iBeam++) {
	    shifts.push_back( virtual_geometry_->convert_to_Lateral_Bixel_Shift_Subscripts(
				  virtual_geometry_->transform_to_Shift_in_Collimator_Coord(temp_setup,iBeam)*(-1),iBeam));
	}

	// loop over the physical beamlets
	for(unsigned int iBixel = 0; iBixel<the_geometry_->get_nBixels(); iBixel++) {
	    
	    beamNo = the_geometry_->get_beamNo(iBixel);
	    
	    // get subscript of bixel
	    temp_sub = get_physical_bixel_subscripts(iBixel);
	    
	    // shift bixel
	    temp_sub = temp_sub + shifts[beamNo];
	    
	    // check whether this is a valid bixel
	    if(virtual_geometry_->is_valid_bixel(temp_sub,beamNo)) {
		
		// convert it back to a bixelNo
		temp_No = virtual_geometry_->get_bixelNo(temp_sub,beamNo);
		
		// push back new element in the transformation matrix
		trafo.push_back_element(iBixel, temp_No, 1.0/(float)nShifts);
	    }
	    else { // this should not happen, the bixel grid is too small
		
		nErrors++;
		
		// don't shift bixel, just get correct virtual bixel number
		temp_No = virtual_geometry_->get_bixelNo(get_physical_bixel_subscripts(iBixel),beamNo);
		
		// push back new element in the transformation matrix
		trafo.push_back_element(iBixel, temp_No, 1.0/(float)nShifts);
	    }
	}
    }

    // unite elements for the same virtual/physical bixel combination
    trafo.unite_entries();

    // initialize the transformation matrix
    trafo.initialize();
    
    // print warning
    if(nErrors>0) {
	cout << "WARNING: could not shift " << nErrors << " bixels." << endl;
    }    
}

/**
 * store a dose evaluation scenario (not implemented)
 */
bool VirtualBixelRandomAndSystematicSetupModel::set_dose_evaluation_scenario(vector<string> scenario_str)
{
    return false;
}

/**
 * write results 
 */
void VirtualBixelRandomAndSystematicSetupModel::write_dose(BixelVector &bixel_grid,
							   DoseInfluenceMatrix &dij,
							   string file_prefix, 
							   Dvh dvh) const
{
    string dose_file_suffix = "dose.dat";
    string DVH_file_suffix = "DVH.dat";
    string dose_file, dvh_file;
    char cScen[10];
    unsigned int nSamples = 1000;

    DoseVector dose(the_geometry_->get_nVoxels());
    RandomScenario scenario;

    // generate random errors
    scenario.virtual_bixel_patient_shifts_.resize(nSamples);
    for(unsigned int iSample=0; iSample<nSamples; iSample++) {
	scenario.virtual_bixel_patient_shifts_[iSample] = get_truncated_gaussian_setup_error(sigmaX_rand_,sigmaY_rand_,sigmaZ_rand_);
    }

    // generate virtual bixel trafo for combined error
    generate_virtual_bixel_trafo_matrix(scenario);

    // calculate dose
    calculate_dose(dose,	
		   bixel_grid,                  
		   dij,
		   scenario);

    // Calculate DVH 
    dvh.calculate(dose);

    dose_file = file_prefix + "scenario_1"
	+ '_' + dose_file_suffix;
    dvh_file = file_prefix + "scenario_1" 
	+ '_' + DVH_file_suffix;
    
    dose.write_dose(dose_file);
    dvh.write_DVH(dvh_file);
}



