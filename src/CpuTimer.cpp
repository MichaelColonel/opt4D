/**
 * @file CpuTimer.cpp
 * CpuTimer Class Definitions
 */

#include "CpuTimer.hpp"

CpuTimer::CpuTimer()
  : start_time_(((double)clock()) / CLOCKS_PER_SEC)
  , is_paused_(false)
  , pause_time_(0)
  , ignored_time_(0)
  , clock_overflow_counter_(0)
  , last_clock_(clock())
{
}

void CpuTimer::restart()
{
  start_time_ = (((double)clock()) / CLOCKS_PER_SEC);
  is_paused_ = false;
  ignored_time_ = 0;
  clock_overflow_counter_ = 0;
  last_clock_ = clock();
}
