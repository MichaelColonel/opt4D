/**
 * @file MeanConstraint.hpp
 * MeanConstraint Class header
 *
 */

#ifndef MEANCONSTRAINT_HPP
#define MEANCONSTRAINT_HPP

#include "Constraint.hpp"
#include "Voi.hpp"
#include <cmath>
#include "FastMeanDoseComputer.hpp"

/**
 * Class MeanConstraint implements a hard maximum or minimum mean dose constraint in an ROI.
 */
class MeanConstraint : public Constraint
{

public:

  // Constructor
  MeanConstraint(Voi*  the_voi,
		 unsigned int consNo,
		 bool is_max_constraint,
		 bool is_min_constraint,
		 float max_dose,
		 float min_dose,
		 float initial_penalty);
  
  // Virtual destructor
  ~MeanConstraint();


  //
  // Mean Constraint specific functions
  //
       
  /// Find out how many voxels were in objective
  unsigned int get_nVoxels() const;
  
  /// get corresponding VOI
  unsigned int get_voiNo() const;
  
  /// Print a description of the constraint
  void printOn(std::ostream& o) const;
        
  /// calculates the mean dose contributions
  void initialize(DoseInfluenceMatrix& Dij);

  /// set upper bound of a constraint
  void set_upper_bound(float value) {max_dose_ = value;};

  /// set upper bound of a constraint
  float get_upper_bound() const {return max_dose_;};

  /// set lower bound of a constraint
  void set_lower_bound(float value) {min_dose_ = value;};

  /// set lower bound of a constraint
  float get_lower_bound() const {return min_dose_;};



  //
  // specific functions for augmented lagrangian approach
  //
  
  bool supports_augmented_lagrangian() const {return(true);};

  /// update lagrange parameter
  void update_lagrange_multipliers(const BixelVector & beam_weights,
				   DoseInfluenceMatrix & Dij);

  /// get lagrange multipliers as pairs <voxelNo,value>
  vector<pair<unsigned int,float> > get_lagrange_multipliers();

  /// update lagrange parameter
  void update_penalty(const BixelVector & beam_weights,
					  DoseInfluenceMatrix & Dij, 
					  float tol, 
					  float multiplier);

  /// calculate the augmented lagrangian function
  double calculate_aug_lagrangian(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
				double & constraint,
				double & merit);

  /// calculate the augmented lagrangian function and its gradient
  double calculate_aug_lagrangian_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                BixelVectorDirection & gradient,
                float gradient_multiplier);


  //
  // specific functions for projection method
  //
  
  bool supports_projection_solver() const {return(true);};
  bool supports_projection_optimizer() const {return(true);};

  /// sequentially project onto (beyond) the constraints
  unsigned int project_onto(BixelVector & beam_weights,
			    DoseInfluenceMatrix & Dij);

  /// ckeck if active set is empty
  bool is_active_set_empty() const {return !in_active_set_;};

  /// reset active set
  void reset_active_set() {in_active_set_ = true;};


  //
  // specific functions for commercial solver interface
  //
  
  bool supports_external_solver() const {return(true);};

  /// return number of geeric constraints
  unsigned int get_nGeneric_constraints() const {return(1);};

  /// generate generic constraints for external solver
  void add_to_optimization_data_set(GenericOptimizationData & data, 
				    DoseInfluenceMatrix & Dij) const;


private:

  // Default constructor not allowed
  MeanConstraint();

  /// Class to hold precalculated mean dose contributions
  FastMeanDoseComputer mean_dose_computer_;

  /// the corresponding VOI
  Voi *the_voi_;
  
  /// is max dose constraint
  bool is_max_constraint_;

  /// is min dose constraint
  bool is_min_constraint_;

  /// maximum tolerance dose
  float max_dose_;

  /// minimum tolerance dose
  float min_dose_;
  
  /// penalty factor
  float penalty_;
  
  /// initial penalty factor
  float initial_penalty_;

  /// lagrange multiplier
  float lagrange_;

  /// if constraint is in active set
  bool in_active_set_;
  
};

/*
 * Inline functions
 */


inline
unsigned int MeanConstraint::get_voiNo() const
{
  return the_voi_->get_voiNo();
}

inline
unsigned int MeanConstraint::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}


 
#endif

