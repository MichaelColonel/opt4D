/**
 * @file ExternalOptimizer.cpp
 * ExternalOptimizer Class implementation.
 */

#include "ExternalOptimizer.hpp"
#include "OptppInterface.hpp"
#include "MosekInterface.hpp"



/*
 * declare global variable pointing to the instance of OptppInterface
 * created in the function call_optpp(). This is needed by the wrapper function objective_wrapper 
 * defined in OptppInterface.cpp.
 */
OptppInterface* global_optpp;
	

/**
 * Constructor
 */
ExternalOptimizer::ExternalOptimizer(Plan* thisplan, string log_file_name, Optimization::options options)
  : Optimization(thisplan, log_file_name, options)
  , optTypeInd_(0)
{
}

/**
 * initialize
 */
void ExternalOptimizer::initialize()
{
  // set optimization type
  if(options_.optimization_type_.size() > 1) {
	optTypeInd_ = 1;
  }
}


/**
 * Optimize plan 
 */
void ExternalOptimizer::optimize()
{

  switch ( options_.optimization_type_[optTypeInd_] ) {

  case MOSEK:

	// call mosek
	call_mosek();

	break;

  case OPT_PP:

	// call optpp solver
	call_optpp();

	break;

  default:
	cout << "No external solver of type " << options_.optimization_type_[optTypeInd_] << endl;
	throw Optimization::exception("no external solver found");
  }
}



/**
 * Optimize plan using the optimization package opt++
 */
void ExternalOptimizer::call_optpp() 
{
  // note: if Opt++ is not available, a dummy class is created which prints an error message
    
  // create Opt++ interface object
  // global variable global_optpp is defined here
  global_optpp = new OptppInterface(the_plan_,&options_);
     
  // Restart the timer
  timer_.restart();

  // optimize plan
  global_optpp->optimize();

  // get time
  cout << "optimization time: " << timer_.get_time() << endl;

  // delete Opt++ interface
  delete global_optpp;
}



/**
 * Optimize plan by calling MOSEK as external solver
 */
void ExternalOptimizer::call_mosek()
{
    if ( the_plan_->get_parameter_vector()->getVectorType() != BIXEL_VECTOR_TYPE ) {
        throw std::logic_error("Can only use Mosek with bixel vector");
    }

    // check if all constraints and objectives suppot an external solver
    if(the_plan_->supports_external_solver()) {

        // note: if Mosek is not available, a dummy class is created which prints an error message

        // create Mosek interface object
        MosekInterface mosek_interface = MosekInterface(the_plan_);

        // optimize plan
        mosek_interface.optimize();
    }
    else {
        cout << "Not all objectives and constraints support Mosek." << endl;
        cout << "Not optimizing the plan" << endl;
    }
}
