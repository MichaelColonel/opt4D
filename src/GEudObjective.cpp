#include "GEudObjective.hpp"

/**
 * Constructor
 */
GEudObjective::GEudObjective(
      Voi*  the_voi,
      unsigned int objNo,
      float weight,
      float sampling_fraction,
      bool sample_with_replacement,
      bool  is_maximized)
  : NonSeparableObjective(the_voi, objNo, weight,sampling_fraction, sample_with_replacement)
  , weight_(weight)
  , is_maximized_(is_maximized)
  , epsilon_(1e-12)
{
  // get gEUD parameter from Voi
  EUD_p_ = the_voi->get_EUD_p();
}

/**
 * Destructor: default.
 */
GEudObjective::~GEudObjective()
{
}

/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void GEudObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": ";
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    if(is_maximized_) {
	o << "\tmaximize gEUD, exponent = " << EUD_p_;
    }
    else{
	o << "\tminimize gEUD, exponent = " << EUD_p_;
    }
}

/**
 * Calculate the objective from some voxels with the given doses.
 *
 * @param dose The vector of voxel doses.
 */
double GEudObjective::calculate_objective(
	const vector<float> & dose) const
{
    unsigned int nVoxels = dose.size();
    double nVoxels_f = static_cast<double>(nVoxels);
    vector<float>::const_iterator iter;

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }

    // if exponent is negative (targets) check for zero dose values
    if (EUD_p_ < 0) {
      for(iter = dose.begin(); iter != dose.end(); iter++) {
	if(*iter<epsilon_){
	  cout << "WARNING: could not calculate gEUD for Voi "
	       << the_voi_->get_voiNo()
	       << " because of zero dose in a voxel" << endl;
	  //cout << "WARNING: could not calculate gEUD because of zero dose" << endl;
	  return(0);
	}
      }
    }

    // calculate gEUD
    double mean_dose = 0;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
      mean_dose += pow(*iter,EUD_p_);
    }
    mean_dose /= nVoxels_f;

    if(mean_dose > epsilon_){
      mean_dose = pow(mean_dose,1.0/EUD_p_);
    }
    else {
      cout << "WARNING: zero gEUD" << endl;
      return(0);
    }

    return weight_*mean_dose;
}

/**
 * Calculate the objective and partial derivative of the voxel contribution to
 * the objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 */
double GEudObjective::calculate_objective_and_d(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose) const
{
    int nVoxels = dose.size();
    double nVoxels_f = static_cast<double>(nVoxels);
    vector<float>::const_iterator iter;

    // Resize return vector
    d_obj_by_dose.resize(nVoxels,0);

    // Skip if there are no voxels
    if(nVoxels == 0) {
        return 0;
    }

    // if exponent is negative (targets) check for zero dose values
    if (EUD_p_ < 0) {
      for(iter = dose.begin(); iter != dose.end(); iter++) {
	if(*iter<epsilon_){
	  cout << "WARNING: could not calculate gEUD for Voi "
	       << the_voi_->get_voiNo()
	       << " because of zero dose in a voxel" << endl;
	  return(0);
	}
      }
    }

    // Calculate generalized mean dose
    double mean_dose = 0;
    for(iter = dose.begin(); iter != dose.end(); iter++) {
      mean_dose += pow(*iter,EUD_p_);
    }
    mean_dose /= nVoxels_f;

    double objective =0;
    double temp =0;
    if(mean_dose > epsilon_){
      objective = pow(mean_dose,1.0/EUD_p_);
      temp = weight_ * pow(mean_dose,(1.0/EUD_p_)-1.0) / nVoxels_f;
    }
    else {
      cout << "WARNING: zero gEUD" << endl;
      return(0);
    }

    // screen output
    cout << "eud for Voi " << the_voi_->get_voiNo() << ": " << objective <<endl;

    // Fill return vector
    iter = dose.begin();
    vector<float>::iterator return_iter = d_obj_by_dose.begin();
    while(iter != dose.end()) {
      *return_iter = (float) temp * pow(*iter,EUD_p_-1.0f);
      iter++;
      return_iter++;
    }

    return weight_*objective;
}


/**
 * Calculate the objective and first and second partial derivatives of the
 * voxel contribution to the objective with the given dose.
 *
 * @param dose The dose of the voxels in the objective.
 * @param d_obj_by_dose The vector to hold the returned partial derivatives
 * @param d2_obj_by_dose The vector to hold the returned partial derivatives
 */
inline
double GEudObjective::calculate_objective_and_d2(
	const vector<float> & dose,
	vector<float> & d_obj_by_dose,
	vector<float> & d2_obj_by_dose) const
{
  std::cerr << "GEudObjective::calculate_objective_and_d2"
	    << "\nis not implemented yet." << std::endl;
  exit(-1);
}



