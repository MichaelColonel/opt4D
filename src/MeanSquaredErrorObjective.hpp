/**
 * @file MeanSquaredErrorObjective.hpp
 * MeanSquaredErrorObjective Class header
 *
 * $Id: MeanSquaredErrorObjective.hpp,v 1.26 2008/03/14 15:00:26 bmartin Exp $
 */

#ifndef MEANSQUAREDERROROBJECTIVE_HPP
#define MEANSQUAREDERROROBJECTIVE_HPP

#include "SeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class MeanSquaredErrorObjective is used to apply mean squared dose error 
 * objectives.
 */
class MeanSquaredErrorObjective : public SeparableObjective
{

    public:

        // Constructor
        MeanSquaredErrorObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement,
                bool  is_max_constraint,
                float max_dose,
                float weight_over,
                bool  is_min_constraint = false,
                float min_dose = 0,
                float weight_under = 1);

        // Virtual destructor
        virtual ~MeanSquaredErrorObjective();

        //
        // MeanSquaredErrorObjective specific functions
        //
        void set_max_dose(float max_dose);
        float get_max_dose()const;
        void set_min_dose(float min_dose);
        float get_min_dose()const;

        //
        // Functions reimplemented from SeparableObjective
        //
        virtual double calculate_voxel_objective(
                float dose, unsigned int voxelNo, double nVoxels) const;
        virtual double calculate_dvoxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const;
        virtual double calculate_d2voxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const;

        //
        // Functions reimplemented from Objective
        //

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;

        /// Metaobjective support
        bool supports_meta_objective() const {return true;};


    private:
        // Default constructor not allowed
        MeanSquaredErrorObjective();

        /// True if this is a maximum dose constraint
        bool is_max_constraint_;

        /// Maximum dose in Gy
        float max_dose_;

        /// Penalty for being over the max dose (multiplied by the weight)
        float weight_over_;

        /// True if this is a minimum dose constraint
        bool is_min_constraint_;

        /// Minimum dose in Gy
        float min_dose_;

        /// Penalty for being under the min dose (multiplied by the weight)
        float weight_under_;
};

/*
 * Inline functions
 */

inline
void MeanSquaredErrorObjective::set_max_dose(float max_dose)
{
    max_dose_ = max_dose;
}

inline
void MeanSquaredErrorObjective::set_min_dose(float min_dose)
{
    min_dose_ = min_dose;
}

inline
float MeanSquaredErrorObjective::get_max_dose()const
{
    return max_dose_;
}


/**
 * Calculate the contribution to the objective from one voxel with the given 
 * dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double MeanSquaredErrorObjective::calculate_voxel_objective(
        float dose, unsigned int voxelNo, double nVoxels) const
{
    if(is_max_constraint_ && (dose > max_dose_)) {
        return (dose - max_dose_) * (dose - max_dose_) 
            * weight_over_ * weight_ / nVoxels;
    }
    else if(is_min_constraint_ && (dose < min_dose_)) {
        return (dose - min_dose_) * (dose - min_dose_) 
            * weight_under_ * weight_ / nVoxels;
    }
    return 0;
}


/**
 * Calculate the partial derivative of the voxel contribution to the objective 
 * from one voxel with the given dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double MeanSquaredErrorObjective::calculate_dvoxel_objective_by_dose(
        float dose, unsigned int voxelNo, double nVoxels) const
{
    if(is_max_constraint_ && (dose > max_dose_)) {
        return  2 * (dose - max_dose_)
            * weight_over_ * weight_ / nVoxels;
    }
    else if(is_min_constraint_ && (dose < min_dose_)) {
        return 2 * (dose - min_dose_)
            * weight_under_ * weight_ / nVoxels;
    }
    return 0;
}


/**
 * Calculate the second partial derivative of the voxel contribution to the 
 * objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double MeanSquaredErrorObjective::calculate_d2voxel_objective_by_dose(
        float dose, unsigned int voxelNo, double nVoxels) const
{
    if(is_max_constraint_ && (dose > max_dose_)) {
        return  2 * weight_over_ * weight_ / nVoxels;
    }
    else if(is_min_constraint_ && (dose < min_dose_)) {
        return 2 * weight_under_ * weight_ / nVoxels;
    }
    return 0;
}

#endif
