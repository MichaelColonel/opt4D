/**
 * @file BixelVectorDirection.hpp
 * BixelVectorDirection Class Definition.
 */

#ifndef BIXELGRIDDIRECTION_HPP
#define BIXELGRIDDIRECTION_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

// Predefine class before including others
class BixelVectorDirection;

#include "ParameterVectorDirection.hpp"
#include "DoseVector.hpp"
#include "Voi.hpp"

/**
 * Class BixelVectorDirection handles individual beams and their bixel intensities.
 * The constructor creates set of bixel positions for those bixels that connect with
 * target voxels.
 */
class BixelVectorDirection : public ParameterVectorDirection {

public:
	// Constructors
	BixelVectorDirection( void ) : ParameterVectorDirection() {
		setVectorType(BIXEL_PVD_TYPE);
	};
	BixelVectorDirection( const int nParams ) : ParameterVectorDirection( nParams ) {
		setVectorType(BIXEL_PVD_TYPE);
	};
	BixelVectorDirection( ParameterVectorDirection & pvd ) : ParameterVectorDirection( pvd ) {
		setVectorType(BIXEL_PVD_TYPE);
	};

	// Create functions with names and arguments more relevant to BixelVectors

	inline float get_intensity( const int j ) const {
		return get_value( j );
	};
	inline void  add_intensity( const int j, const float intensity_inc ) {
		add_value( j, intensity_inc );
	};
	inline void  set_intensity( const int j, const float value ) {
		set_value( j, value );
	};
	inline void clear_intensities() {
		clear_values();
	};
	inline void scale_intensities(float scale_factor) {
		scale_values( scale_factor );
	};
	/// get number of bixels in beam
	inline size_t get_nBixels() const {
		return get_nParameters();
	};
};

#endif // BIXELGRIDDIRECTION_HPP
