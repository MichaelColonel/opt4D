/**
 * @file DirectParameterVector.hpp
 * DirectParameterVector class.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef DIRECTPARAMETERVECTOR_HPP
#define DIRECTPARAMETERVECTOR_HPP

// Predefine class before including others
class DirectParameterVector;
#include "ParameterVector.hpp"
#include "DeliveryMachineModel.hpp"
#include "BixelVector.hpp"
#include "Geometry.hpp"

#include <cassert>
#include <cmath>
#include <algorithm>
#include <map>
#include <utility>
#include <memory>

#include <execinfo.h>
#include <cstdio>
#include <cstdlib>


/**
 * Class DeliveryMachineModel_exception is thrown when there is an error building a DeliveryMachineModel object.
 */
class DirectParameterVector_exception : public std::exception {
    virtual const char* what() const throw() {
        return "Failed in Direct Parameter Vector.";
    }
};

enum ParameterType {
    Weight, BeamEnergy, DoseRate, Gantry, Collimator, Couch, XJaw, YJaw, XLeaf, YLeaf
};

/// Print ParameterType
inline std::ostream& operator<< ( std::ostream& o,  const enum ParameterType pType ) {
    switch ( pType ) {
        case Weight:
            return o << "Weight";
        case BeamEnergy:
            return o << "BeamEnergy";
        case DoseRate:
            return o << "DoseRate";
        case Gantry:
            return o << "Gantry";
        case Collimator:
            return o << "Collimator";
        case Couch:
            return o << "Couch";
        case XJaw:
            return o << "XJaw";
        case YJaw:
            return o << "YJaw";
        case XLeaf:
            return o << "XLeaf";
        case YLeaf:
            return o << "YLeaf";
        default:
            throw std::logic_error("Invalid enum value.");
    }
};


/**
 * Class DirectParameterVector handles individual parameters that have a direct relationship to the parameters that
 * control or define the treatment delivery. It is a parent class for parameters used in direct aperture optimisation.
 * N.B. The parameters defined have a direct relationship with the delivery parameters but not necessarily a 1:1
 * relationship.
 */
class DirectParameterVector : public ParameterVector {

  public:

    /**
     * Constructor.
     *
     * @param bixMap        The bixel vector used to initialize the plan
     * @param dao_file_name	The DAO setup filename
     * @param mpf_file_name	The filename of the delivery machine model.
     * @param geometry      The plan geometry object.
     * @param cpPerField    Array of numbers of control points in each field.
     */
    DirectParameterVector( const BixelVector & bixMap, const string &dao_file_name,
                          const string & mpf_file_name, Geometry const * geometry, const vector<unsigned int> &cpPerField );

    /**
     * Constructor.
     *
     * @param bixMap        The bixel vector used to initialize the plan
     * @param dao_file_name	The DAO setup filename
     * @param mpf_file_name	The filename of the delivery machine model.
     * @param geometry      The plan geometry object.
     * @param nFields       The number of fields in the plan.
     * @param cpPerField    The number of control points in every field.
     */
    DirectParameterVector( const BixelVector & bixMap, const string &dao_file_name,
                          const string & mpf_file_name, Geometry const * geometry, unsigned int nFields=0, unsigned int cpPerField=1 );

    /**
     * Constructor.
     *
     * @param bixMap        The bixel vector used to initialize the plan
     * @param dao_file_name	The DAO setup filename
     * @param geometry      The plan geometry object.
     */
    DirectParameterVector( const BixelVector & bixMap, const string &dao_file_name, Geometry const * geometry );

    /**
     * Constructor.
     * Initializes set of parameters sufficent to define the treatment plan.
     *
     * @param bixMap        The bixel vector used to initialize the plan
     * @param machine       The number of fields in the plan.
     * @param geometry      The plan geometry object.
     * @param cpPerField    The number of control points in each field.
     * @param includeLeafPosition		Include leaf position parameters for each field.
     * @param includeLeafPositionChanges Include leaf position parameters for each control point.
     * @param includeJawPosition		Include jaw position parameters for each field.
     * @param includeJawPositionChanges Include jaw position parameters for each control point.
     * @param includeEnergy				Include beam energy parameters for each field.
     * @param includeEnergyChanges		Include beam energy parameters for each control point.
     * @param includeGantryAngle		Include gantry angle parameters for each field.
     * @param includeGantryAngleChanges Include gantry angle parameters for each control point.
     * @param includeCollAngle			Include collimator angle parameters for each field.
     * @param includeCollAngleChanges	Include collimator angle parameters for each control point.
     * @param includeCouchAngle			Include couch angle parameters for each field.
     * @param includeCouchAngleChanges	Include couch angle parameters for each control point.
     * @param includeDoseRate			Include dose rate parameters for each field.
     * @param includeDoseRateChanges	Include dose rate parameters for each control point.
     */
    DirectParameterVector( const BixelVector & bixMap, DeliveryMachineModel & machine, Geometry const * geometry,
                          const vector<unsigned int> &cpPerField,
            bool includeLeafPosition=false,	bool includeLeafPositionChanges=false,
            bool includeJawPosition=false,	bool includeJawPositionChanges=false,
            bool includeEnergy=false,			bool includeEnergyChanges=false,
            bool includeGantryAngle=false,	bool includeGantryAngleChanges=false,
            bool includeCollAngle=false,		bool includeCollAngleChanges=false,
            bool includeCouchAngle=false,		bool includeCouchAngleChanges=false,
            bool includeDoseRate=false,		bool includeDoseRateChanges=false );

    /**
     * Copy Constructor.
     */
    DirectParameterVector( const DirectParameterVector & dpv );

    /**
     * Destructor
     */
    virtual ~DirectParameterVector() {
        //cout << "dpv 1" << endl;
        delete bixMap_;
        //cout << "dpv 2" << endl;
    };

    void initializeCP_metaData(bool includeLeafPosition, bool includeJawPosition, bool includeEnergy,
                               bool includeGantryAngle, bool includeCollAngle, bool includeCouchAngle,
                               bool includeDoseRate );

    /**
     * Get the current type of parameter vector
     *
     * @return the parameter vector type enum indicating that this is a bixel vector
     */
    virtual inline ParameterVectorType getVectorType() const {
        return DIRECT_VECTOR_TYPE;
    }

    /*		Get and Set Functions for parameter properties :	*/

    /// Get the parameter type.
    inline ParameterType get_type( int ip ) const { return type_[ip]; };
    // ///  Set the parameter type.
    //		Warning the parameter type should not be set directly as the ordering of
    //		different parameter order is strictly controlled within a control point.
    //inline void set_type( int ip, ParameterType ty ) { type_[ip] = ty; };

    /// Get the absolute high bound (as independant from other parameter values) for a parameter.
    inline float get_absHighBound( int ip ) const { return absHighBound_[ip]; };
    /// Set the absolute high bound (as independant from other parameter values) for a parameter.
    inline void set_absHighBound( int ip, float bound ) { absHighBound_[ip] = bound; };

    /// Get the absolute low bound (as independant from other parameter values) for a parameter.
    inline float get_absLowBound( int ip ) const { return absLowBound_[ip]; };
    /// Set the absolute low bound (as independant from other parameter values) for a parameter.
    inline void set_absLowBound( int ip, float bound ) { absLowBound_[ip] = bound; };

    /// Find out if the parameter is continuous or discrete.
    inline bool get_isDiscreteVariable( int ip ) const { return isDiscreteVariable_[ip]; };
    /// Set the isDiscreteVariable flag.
    void set_isDiscreteVariable( int ip, bool isDiscrete );

    /// Get the number of valid parameter values.
    inline int get_numberOfValidValues( int ip ) const { return numberOfValidValues_[ip]; };
    /// Get the number of valid parameter values.
    inline vector<float> get_validValues( int ip ) {
        vector<float> tmpVec = validValues_[ip];
        return tmpVec;
    };
    /// get a valid parameter value from a discrete list. Also works for regular parameter values.
	float get_validValue( unsigned int ip, unsigned int iv ) const;
    /// Round the requested speed to a valid value from the list of valid values.
    float get_RoundToValidValue( int ip, float requestVal ) const;

    /*		Get and Set Functions for global properties :	*/
    /// Get the minimum x axis position on the bixel grid for a given parameter.
    inline float get_minXAx( unsigned int ip ) const;
    /// Get the maximum x axis position on the bixel grid for a given parameter.
    inline float get_maxXAx( unsigned int ip ) const;
    /// Get the minimum y axis position on the bixel grid for a given parameter.
    inline float get_minYAx( unsigned int ip ) const;
    /// Get the maximum y axis position on the bixel grid for a given parameter.
    inline float get_maxYAx( unsigned int ip ) const;

    /// Find out if the parameter can change between fields.
    inline bool get_canChangeBetweenFields( int ip ) const { return get_canChangeBetweenCP(ip); };
    /// Find out if the parameter can change between control points.
    inline bool get_canChangeBetweenCP( int ip ) const;
    /// Find out if the parameter can change direction during the delivery.
    inline bool get_canChangeDirection( int ip ) const;
    /// Get the minimum non-zero speed of a parameter value change
    inline float get_minSpeed( int ip ) const;
    /// Get the maximum speed of a parameter value change
    inline float get_maxSpeed( int ip ) const;
    /// Get the maximum acceleration of a parameter value change
    inline float get_maxAcceleration( int ip ) const;
    /// Find out if a parameter change requires a beam interrupt.
    inline bool get_changeRequiresInterrupt( int ip ) const;
    /// Get the value of any latency in restarting the beam after an interruption.
    inline float get_interruptLatency( int ip ) const;


    /// Find out if there is a limit placed on the number of control points in a field.
    inline bool get_cpMaxNumIsLimited() const { return machine_->get_cpMaxNumIsLimited(); };
    /// Get the maximum number of control points in a field.
    inline unsigned int get_cpMaxNum_() const { return machine_->get_cpMaxNum(); };
    /// Find out if there is a limit placed on the minimum spacing between control points in a field.
    inline bool get_cpMinSpacingIsLimited_() const { return machine_->get_cpMinSpacingIslimited(); };
    /// Get the minimum spacing between control points in a field.
    inline float get_cpMinSpacing_() const { return machine_->get_cpMinSpacing(); };
    /// Get the time step defining the precision of control points time placement.
    inline float get_cpTimeRes_() const { return machine_->get_cpTimeRes(); };
    /// Find out if there is a limit placed on the maximum control points time (dose).
    inline bool get_cpMaxTimeIsLimited_() const { return machine_->get_cpMaxTimeIsLimited(); };
    /// Get the maximum control points time value.
    inline float get_cpMaxTime_() const { return machine_->get_cpMaxTime(); };


    /*		Report, add or subtract fields and control points from the plan :	*/
    /// Get the number of fields in the plan.
    inline unsigned int get_nf() const { return nf_; };
    /// Get the number of control points in a particular field.
    inline unsigned int get_ncp( int fd ) const { return ncp_[fd]; };

    /// Get the parameter set which describes one field of the plan.
    DirectParameterVector * copy_field(int fd) const;
    /// Get the parameter set which describes one control point of the plan.
    DirectParameterVector * copy_cp(int cpi) const;
    /// Get the parameter set which describes one control point of one field.
    DirectParameterVector * copy_cp(int fd, int cpi) const;

    /// Add a new field to the parameter set after a given field index.
    void add_field(unsigned int fd, const DirectParameterVector & field);
    /// Add a new control point into the parameter set after a given control point index.
    void add_cp(unsigned int cpi, const DirectParameterVector & field);
    /// Add a new control point into the parameter set for a given field after a given control point index.
    void add_cp(unsigned int fd, unsigned int cpi, const DirectParameterVector & field);

    /// Remove a field from the parameter set.
    void remove_field(unsigned int fd);
    /// Remove a control point from the parameter set.
    void remove_cp(unsigned int cpi);
    /// Remove a control point from the parameter set for a given field.
    void remove_cp(unsigned int fd, unsigned int cpi);

    /// Combine control points from two fields into a single field.
    void combine_fields(unsigned int fd1, unsigned int fd2);
    /// Split a field into two fields at a given control point.
    void split_field(unsigned int fd, unsigned int cpi);

    /// Get the number of parameters in a particular control point.
    inline unsigned int get_paramPerCP( unsigned int & fd, unsigned int & cp ) const {
        return paramPerCP_[ get_absCPindex( fd, cp ) ];
    };

    /// Get parameter index of first parameter relating to a certain control point within a field.
	unsigned int get_PInd( unsigned int fieldInd, unsigned int cpInd ) const;

    /// Set the control point weightings to absolute or relative dose.
    void set_WeightingCPRelative();
    void set_WeightingCPAbsolute();

    /// Override ParameterVector functions to also call resetLimits
    virtual void set_value( const int ip, const float value ) {
        if ( value != value_[ip] ) {
            ParameterVector::set_value(ip, get_RoundToValidValue(ip, value));
            resetLimits();
        }
    };
    virtual void set_isActive( const int ip, const bool isActive ) {
        if ( isActive != isActive_[ip] ) {
            ParameterVector::set_isActive(ip, isActive);
            resetLimits();
        }
    };
    virtual void set_lowBound( const int ip, const float lowBound ) {
        cerr << "Warning : Attempt to explicitly set lowBound of DirectParameterVector." << endl;
        cerr << "Setting absLowBound instead." << endl;
        if ( lowBound != absLowBound_[ip] ) {
            set_absLowBound(ip, lowBound);
            resetLimits();
        }
    };
    virtual void set_highBound( const int ip, const float highBound ) {
        cerr << "Warning : Attempt to explicitly set highBound of DirectParameterVector." << endl;
        cerr << "Setting absHighBound instead." << endl;
        if ( highBound != absHighBound_[ip] ) {
            set_absHighBound(ip, highBound);
            resetLimits();
        }
    };

    virtual void add_value( const int ip, const float value_inc );
    virtual void add_value( const ParameterVector &pv );
    virtual void add_value( const ParameterVectorDirection &pvd );
    virtual void set_value( const ParameterVector &pv );
    virtual void set_value( const ParameterVectorDirection &pvd );
    virtual void add_scaled_parameter_vector( const ParameterVector &pv, const float scale_factor );
    virtual void add_scaled_parameter_vector( const ParameterVector &pv, const double scale_factor );
    virtual void add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const float scale_factor );
    virtual void add_scaled_parameter_vector_direction( const ParameterVectorDirection &pvd, const double scale_factor );


    /// Get/Set the field weighting factor
    inline float get_fieldWeight( unsigned int fieldInd ) const;
    inline void set_fieldWeight( unsigned int fieldInd, float wt );

    /// Reset the current parameter limits based on the absolute limits and the current parameter values
    void resetLimits();

    /// Adapt the current parameter values to make a deliverable set.
    void makePossible();
    void makePossible( bool largeApertures );


    /**
     * Overloaded functions from ParameterVector
     */
    /// Scale the control point weightings by a dose weight scaling factor.
    virtual void scale_intensities( float scale_factor );
    /// Set the control point weightings to zero
    virtual void set_zero_intensities();
    /// Set the control point weightings to give equal dose per beam
    virtual void set_unit_intensities();

    /// Get the dij beam number for a given parameter
    unsigned int get_beamNo(const int &ip) const;

    /// Get a pointer to the current parameter set
    inline virtual auto_ptr<ParameterVector> get_copy() const {
        auto_ptr<ParameterVector> dpv;
        dpv.reset( new DirectParameterVector(*this) );
        return dpv;
    }

    virtual void load_parameter_files( string parameters_file_root ) {
        cerr << "DirectParamterVector.load_parameter_files() Error method has not been implemented yet." << endl;
        assert(0);
    };
    virtual void load_parameter_file( string parameters_file_name, unsigned int beamNo ) {
        cerr << "DirectParamterVector.load_parameter_file() Error method has not been implemented yet." << endl;
        assert(0);
    };
    virtual void write_parameter_files( string parameters_file_root ) const;
    virtual void write_parameter_file( string parameters_file_name, const unsigned int beamNo ) const;

    /// Test if parameter vectors are equal.
    bool operator==( const DirectParameterVector& rhs ) const;

    /// Reserve space for a ParameterVector with nParams elements.
    void reserve_nParams( const unsigned int nParams );

    /// Adds a new element at the end of the vector, after its current last element.
    void push_back_parameter( const float &value, const float &lowBound, const float &highBound,
            const bool &isActive, const float &step_size, const float &absLowBound, const float &absHighBound );

    /// Swap the contents of two ParameterVectors.
    void swap( DirectParameterVector & rhs );

    /// Find out if the parameter set is physically imposible to deliver.
    bool is_impossible() const;

	/**
	 * Get number of bixels in the bixel maps produced by the parameter translation.
	 */
	unsigned int get_nBixels() const {   return bixMap_->get_nBixels();   };


    /**
     * Translate the ParameterVector into a BixelVector
     * Warning calling function should free returned object.
     */
    BixelVector * translateToBixelVector() const;


    /**
     * Translate the gradient in terms of bixel parameters to the gradient in terms of
     * the parameters used to describe plan.
     */
    ParameterVectorDirection translateBixelGradient( BixelVectorDirection &bvd ) const;


    /**
     * Translate the Bixel Vector to a parameter vector
     */
    inline void translateBixelVector( BixelVector & bv ) {
        cerr << "DirectParameterVector.translateBixelVector(): "
                << "Error this function has not been written yet" << endl;
        assert(0);
    };


    /**
     * Translate the hessian in terms of bixel parameters to the hessian in terms of
     * the parameters used to describe plan.
     */
    inline vector<float> translateBixelHessian( vector<float> & hv ) const;


    /// From a control point index within a field get the control point index from the start of the plan.
	inline int get_absCPindex( unsigned int fieldInd, unsigned int cpInd ) const {
        int absCpInd = 0;
		for ( unsigned int fd=0; fd<fieldInd; fd++ )  absCpInd += ncp_[fd];
        return absCpInd + cpInd;
    };

    /// From a control point index from the start of the plan get the field index.
	inline unsigned int get_Fieldindex( unsigned int cpInd ) const {
		int absCpInd = cpInd;
		unsigned int fd;
        for ( fd=0; fd<nf_; fd++ ) {
            absCpInd -= ncp_[fd];
            if ( absCpInd < 0 ) break;
        }
        return fd;
    };

    /// For a given parmeter index find the absolute control point index
    inline unsigned int paramIndToCPInd( unsigned int paramInd ) const throw (DirectParameterVector_exception) {
		int pI = paramInd;
		for ( unsigned int cp = 0; cp<ncpTot_; cp++ ) {
			pI -= paramPerCP_[cp];
			if ( pI < 0 ) return cp;
        }
        std::cerr << "DirectParameterVector::paramIndToCPInd() Parameter Index is invalid" << std::endl;
        throw DirectParameterVector_exception();
    };

    /// Get parameter index of first parameter relating to a certain control point.
	unsigned int get_PIndCP( unsigned int absCpInd ) const;
	/// Get parameter index of first parameter relating to a certain control point within a certain field.
    unsigned int get_PIndCP( unsigned int fieldInd, unsigned int cpInd ) const;
    /// Get parameter index of first parameter relating to a certain field.
	unsigned int get_PIndField( unsigned int fieldInd ) const;


    /// Get parameter index of a the pnum-th weighting parameter within control point absCpInd.
	unsigned int get_PInd_Weight( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, Weight, pnum );
    };
	/// Get parameter index of a the pnum-th BeamEnergy parameter within control point absCpInd.
	unsigned int get_PInd_BeamEnergy( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, BeamEnergy, pnum );
    };
    /// Get parameter index of a the pnum-th DoseRate parameter within control point absCpInd.
	unsigned int get_PInd_DoseRate( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, DoseRate, pnum );
    };
    /// Get parameter index of a the pnum-th Gantry parameter within control point absCpInd.
	unsigned int get_PInd_Gantry( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, Gantry, pnum );
    };
    /// Get parameter index of a the pnum-th Collimator parameter within control point absCpInd.
	unsigned int get_PInd_Collimator( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, Collimator, pnum );
    };
    /// Get parameter index of a the pnum-th Couch parameter within control point absCpInd.
	unsigned int get_PInd_Couch( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, Couch, pnum );
    };
    /// Get parameter index of a the pnum-th XJaw parameter within control point absCpInd.
	unsigned int get_PInd_XJaw( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, XJaw, pnum );
    };
	/// Get parameter index of a the pnum-th YJaw parameter within control point absCpInd.
	unsigned int get_PInd_YJaw( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, YJaw, pnum );
    };
	/// Get parameter index of a the pnum-th XLeaf parameter within control point absCpInd.
	unsigned int get_PInd_XLeaf( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, XLeaf, pnum );
    };
	/// Get parameter index of a the pnum-th YLeaf parameter within control point absCpInd.
	unsigned int get_PInd_YLeaf( unsigned int absCpInd, unsigned int pnum=0 ) const {
        return get_PInd( absCpInd, YLeaf, pnum );
    };

	/// Get the number of Weight parameters within a given control point absCpInd.
    unsigned int get_nParam_Weight( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, Weight );
    };
	/// Get the number of BeamEnergy parameters within control point absCpInd.
	unsigned int get_nParam_BeamEnergy( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, BeamEnergy );
    };
    /// Get the number of DoseRate parameters within control point absCpInd.
	unsigned int get_nParam_DoseRate( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, DoseRate );
    };
    /// Get the number of Gantry parameters within control point absCpInd.
	unsigned int get_nParam_Gantry( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, Gantry );
    };
    /// Get the number of Collimator parameters within control point absCpInd.
	unsigned int get_nParam_Collimator( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, Collimator );
    };
    /// Get the number of Couch parameters within control point absCpInd.
	unsigned int get_nParam_Couch( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, Couch );
    };
    /// Get the number of XJaw parameters within control point absCpInd.
	unsigned int get_nParam_XJaw( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, XJaw );
    };
	/// Get the number of YJaw parameters within control point absCpInd.
	unsigned int get_nParam_YJaw( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, YJaw );
    };
	/// Get the number of XLeaf parameters within control point absCpInd.
	unsigned int get_nParam_XLeaf( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, XLeaf );
    };
	/// Get the number of YLeaf parameters within control point absCpInd.
	unsigned int get_nParam_YLeaf( unsigned int absCpInd ) const {
        return get_nParamOfType( absCpInd, YLeaf );
    };

    /// Get the pNum-th parameter value of a certain type for a certain control point.
    float get_Weight( unsigned int absCpInd, int pNum ) const;
    float get_BeamEnergy( unsigned int absCpInd, unsigned int pNum ) const;
    float get_DoseRate( unsigned int absCpInd, unsigned int pNum ) const;
    float get_Gantry( unsigned int absCpInd, unsigned int pNum ) const;
    float get_Collimator( unsigned int absCpInd, unsigned int pNum ) const;
    float get_Couch( unsigned int absCpInd, unsigned int pNum ) const;
    float get_XJaw( unsigned int absCpInd, unsigned int pNum  ) const;
    float get_YJaw( unsigned int absCpInd, unsigned int pNum  ) const;
    float get_XLeaf( unsigned int absCpInd, unsigned int pNum ) const;
    float get_YLeaf( unsigned int absCpInd, unsigned int pNum ) const;
    bool get_XLeaves( unsigned int absCpInd, vector<float> & leafPos, unsigned int bankNum ) const;
    bool get_YLeaves( unsigned int absCpInd, vector<float> & leafPos, unsigned int bankNum ) const;

    /// Print ParameterType
    friend std::ostream& operator<< ( std::ostream& o,  const enum ParameterType pType );

  protected:

    /// Translate the control point ParameterVector into a BixelVector and add bixel contributions to existing bixel vector.
    void translateControlPointToBixelVector( unsigned int absCpInd, BixelVector * bixVect ) const;

    /// Translate the bixel gradient terms onto jaw/leaf position gradient terms for a given control point.
    void populateGradientForXJaws( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd, unsigned int dijBeamInd,
                                    float cpContribToFlu, float cpCollAngle, float cpXBixSz, float cpYBixSz ) const;
    void populateGradientForYJaws( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd, unsigned int dijBeamInd,
                                    float cpContribToFlu, float cpCollAngle, float cpXBixSz, float cpYBixSz ) const;
    void populateGradientForXLeaves( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd, unsigned int dijBeamInd,
                                    float cpContribToFlu, float cpCollAngle, float cpXBixSz, float cpYBixSz ) const;
    void populateGradientForYLeaves( unsigned int cpInd, BixelVectorDirection &bvd, ParameterVectorDirection &pvd, unsigned int dijBeamInd,
                                    float cpContribToFlu, float cpCollAngle, float cpXBixSz, float cpYBixSz ) const;

    /// Find the mean gradient encountered along a line between two points in a bixel map.
    float findGradientAlongLine( BixelVectorDirection &bvd, float startPosX, float startPosY, float endPosX,
                                float endPosY, unsigned int dijBeamInd, float cpXBixSz, float cpYBixSz ) const;

    /// Remove a parameter from a control point.
    void remove_param( unsigned int pInd );
    /// Insert a parameter
    void insert_param( unsigned int pInd, ParameterType pType );
    void insert_param( unsigned int pInd );
    /// Copy a range of parameters
    void copy_param( unsigned int pIndFrom, unsigned int pIndTo );

    /// Populate a parameter with default properties for a given type
    void putDefaults( unsigned int pInd );

    /// Determine if the field of interest is an arc or not.
    inline virtual bool isArc( int fd ) const;

    /// Get parameter index of a the pnum-th parameter of a type pType within control point absCpInd.
	unsigned int get_PInd( unsigned int absCpInd, ParameterType pType, unsigned int pnum ) const;
	/// Get parameter index of a the pnum-th parameter of a type pType within control point absCpInd without checking.
	unsigned int get_PInd_Initial( unsigned int absCpInd, ParameterType pType, unsigned int pnum ) const;
	/// Get the number of parameters of a particular type within a given control point
    unsigned int get_nParamOfType( unsigned int absCpInd, ParameterType pType ) const;

    /// Get the pNum-th parameter value of a certain type for a certain control point.
    float get_valueAtCP( unsigned int absCpInd, ParameterType pType, unsigned int pNum ) const;

    /// Test if a parameter type should be constant between control points in a given field
    bool isConstantWithinField( unsigned int absCpInd, ParameterType pType ) const;

    /// Set the value, stepsize and absolute limits of the parameter
	void setParam( unsigned int pInd, int nValidValues, float minVal, float maxVal, float stepSize, ParameterType ty);
	void setParam( unsigned int pInd, int nValidValues, float minVal, float maxVal, float stepSize,
                   const std::vector<float> & validValForPn, ParameterType ty);

    /// Initializes a set of parameters describing a control point.
    void initializeCPParams(unsigned int cpi);

    /// Component reset limit functions
    void resetLimits_ArcWeights(unsigned int fd);
    void resetLimits_XJaw( unsigned int fd, vector<float> cpTime );
    void resetLimits_YJaw( unsigned int fd, vector<float> cpTime );
    void resetLimits_XMlc( unsigned int fd, vector<float> cpTime );
    void resetLimits_YMlc( unsigned int fd, vector<float> cpTime );

    /**-- Delivery is possible functions --**/
    /// Find out if the arc control point weights are physically imposible to deliver.
    bool is_impossible_ArcWeights( unsigned int fd ) const;
    /// Find out if the x jaw parameters are physically imposible to deliver.
    bool is_impossible_XJaw(unsigned int fd, std::vector<float> cpTime) const;
    /// Find out if the y jaw parameters are physically imposible to deliver.
    bool is_impossible_YJaw(unsigned int fd, std::vector<float> cpTime) const;
    /// Find out if the x mlc parameters are physically imposible to deliver.
    bool is_impossible_XMlc(unsigned int fd, std::vector<float> cpTime) const;
    /// Find out if the y mlc parameters are physically imposible to deliver.
    bool is_impossible_YMlc(unsigned int fd, std::vector<float> cpTime) const;

    /// Make the parameters obey a motion constraint
    void makePossible_motion( vector<unsigned int> pInds, vector<float> dx, bool prioritiseMin, bool overrideMin, bool overrideMax );
    /// Make the parameters obey a minimum gap constraint
	void makePossible_minGap( unsigned int cp );
    /// Make the parameters obey an interdigitation constraint
	void makePossible_interdigitate( unsigned int cp );
    /// Make the parameters obey a leaf carriage constraint
	void makePossible_carriage( unsigned int fd, bool conservelargeAperture );
    /// Map control point weightings into the allowed region given a certain gantry collimator and or couch motion.
    void makePossible_ArcWeights( unsigned int fd );

    /// Calculate minimum and maximum control point weights for arcs
    vector<vector<float> > findArcWeightLimits( unsigned int fd ) const;

    /// update priority of setting a particular parameter value.
	bool escalatePriority( unsigned int pt, vector<unsigned int> & priority, vector<bool> & isSet,
						  vector<unsigned int>::iterator & priorityIt );
	bool escalatePriority( unsigned int pt, vector<unsigned int> & priority, vector<bool> & isSet );

    /// Populate the vectors required to read in the configuration file.
    void populateParserVectors( vector< string > & required_attributes,
                                vector< string > & optional_attributes,
                                map< string, string > & default_values,
                                vector< string > & multi_attributes,
                                map< string, vector< string > > & required_sub_attributes,
                                map< string, vector< string > > & optional_sub_attributes,
                                map< string, map< string, string > > &default_sub_values );

    /// Get the gantry angle of the dij map associated with a certain parameter
    float get_dijGantryAngle(const int &pInd) const;
    /// Get the collimator angle of the dij map associated with a certain parameter
    float get_dijCollimatorAngle(const int &pInd) const;
    /// Get the couch angle of the dij map associated with a certain parameter
    float get_dijCouchAngle(const int &pInd) const;


    /// Bixel Vector
    mutable BixelVector * bixMap_;

    /// Machine model setting the delivery constraints.
    std::auto_ptr< DeliveryMachineModel > machine_;

    /// Pointer to the geometry class used to query the bixel geometry.
    Geometry const * geometry_;

    /// Field normalisation factor
    double doseNormFactor_;

    /// Field and control point weightings are absolute dose in MUs (false = relative values)
    /// If false then dose for a control point is the product of the doseNormFactor the field_weight and the cp_weight
    bool isWeightingsAbsolute_;

    /// Number of fields
    unsigned int nf_;

    /// Total number of control points in the whole plan.
    unsigned int ncpTot_;

    /// Number of control points in each field, one vector element for each field.
    vector< unsigned int > ncp_;

    /// Number of parameters per control point, one vector element for each control point.
    vector< unsigned int > paramPerCP_;

    /// All control points have the beam energy. One entry per field
    vector< bool > isConstantEnergyInFd_;

    /// All control points have the same dose rate ( i.e. cp weighting is directly proportional to time )
    vector< bool > isConstantDoseRateInFd_;

    /// All control points have the same dose rate
    vector< bool > isConstantGantryInFd_;

    /// All control points have the same collimator angle
    vector< bool > isConstantCollInFd_;

    /// All control points have the same dose rate
    vector< bool > isConstantCouchInFd_;

    /// All control points have the same x jaw position
    vector< bool > isConstantXJawInFd_;

    /// All control points have the same y jaw position
    vector< bool > isConstantYJawInFd_;

    /// All control points have the same x MLC position
    vector< bool > isConstantXLeafInFd_;

    /// All control points have the same y MLC position
    vector< bool > isConstantYLeafInFd_;

    /// Number of weighting parameters in control point, always one in current implementation.
    vector< unsigned int > nWeightingParams_;

    /// Number of beam energy parameters in control point, zero or one element per control point.
    vector< unsigned int > nBeamEnergyParams_;

    /// Number of dose rate parameters in control point, zero or one element per control point.
    vector< unsigned int > nDoseRateParams_;

    /// Number of gantry angle parameters in control point, zero or one element per control point.
    vector< unsigned int > nGantryParams_;

    /// Number of collimator angle parameters in control point, zero or one element per control point.
    vector< unsigned int > nCollimatorParams_;

    /// Number of couch angle parameters in control point, zero or one element per control point.
    vector< unsigned int > nCouchParams_;

    /// Number of X Jaws parameters in control point, zero or more elements per control point.
    vector< unsigned int > nXJawParams_;

    /// Number of y Jaws parameters in control point, zero or more elements per control point.
    vector< unsigned int > nYJawParams_;

    /// Number of X MLC leaf parameters in control point, zero or more elements per control point.
    vector< unsigned int > nXLeafParams_;

    /// Number of y MLC leaf parameters in control point, zero or more elements per control point.
    vector< unsigned int > nYLeafParams_;


    /**
     * Parameter maximum allowed value as independant of values of other parameters.
     * The normal inheritted highBound property is now depedant on parameter values.
     */
    vector<float> absHighBound_;

    /**
     * Parameter minimum allowed value as independant of values of other parameters.
     * The normal inheritted lowBound property is now depedant on parameter values.
     */
    vector<float> absLowBound_;

    /// Value is allowed to change continuously or in regular steps.
    vector<bool> isDiscreteVariable_;

    /// Number of valid values for each parameter.
    vector<unsigned int> numberOfValidValues_;

    /**
     * Parameter type enum.
     * One of : Weight, BeamEnergy, DoseRate, Gantry, Collimator, Couch, XJaw, YJaw, XLeaf or YLeaf
     */
    vector<ParameterType> type_;
    /**
     * Valid values for parameter.
     * Stored as sparse vector with parameter index and value list stored.
     */
    std::map<unsigned int, vector<float> > validValues_;
};

/// Get the number of parameters of a particular type within a given control point
inline unsigned int DirectParameterVector::get_nParamOfType( unsigned int absCpInd, ParameterType pType ) const {
    unsigned int numParamType;
    switch ( pType ) {
    case YLeaf:
        numParamType = nYLeafParams_[ absCpInd ];
        break;
    case XLeaf:
        numParamType = nXLeafParams_[ absCpInd ];
        break;
    case YJaw:
        numParamType = nYJawParams_[ absCpInd ];
        break;
    case XJaw:
        numParamType = nXJawParams_[ absCpInd ];
        break;
    case Couch:
        numParamType = nCouchParams_[ absCpInd ];
        break;
    case Collimator:
        numParamType = nCollimatorParams_[ absCpInd ];
        break;
    case Gantry:
        numParamType = nGantryParams_[ absCpInd ];
        break;
    case DoseRate:
        numParamType = nDoseRateParams_[ absCpInd ];
        break;
    case BeamEnergy:
        numParamType = nBeamEnergyParams_[ absCpInd ];
        break;
    case Weight:
        numParamType = nWeightingParams_[ absCpInd ];
    };
    return numParamType;
};


/**
 * Get parameter index of a the pnum-th parameter of a type pType within control point absCpInd.
 */
inline unsigned int DirectParameterVector::get_PInd( unsigned int absCpInd, ParameterType pType, unsigned int pnum=0 ) const {
    // If there aren't any of this parameter type then throw exception
    if ( pnum >= get_nParamOfType(absCpInd, pType) ) throw DirectParameterVector_exception();

    // find the parameter index
    unsigned int pInd = 0;
    switch ( pType ) {
    case YLeaf:
        pInd += nXLeafParams_[ absCpInd ];
    case XLeaf:
        pInd += nYJawParams_[ absCpInd ];
    case YJaw:
        pInd += nXJawParams_[ absCpInd ];
    case XJaw:
        pInd += nCouchParams_[ absCpInd ];
    case Couch:
        pInd += nCollimatorParams_[ absCpInd ];
    case Collimator:
        pInd += nGantryParams_[ absCpInd ];
    case Gantry:
        pInd += nDoseRateParams_[ absCpInd ];
    case DoseRate:
        pInd += nBeamEnergyParams_[ absCpInd ];
    case BeamEnergy:
        pInd += nWeightingParams_[ absCpInd ];
    case Weight:
        break;
    }
    pInd += pnum;

    // Check we haven't run past the end of the control point
    assert ( pInd < paramPerCP_[ absCpInd ] );

    pInd += get_PIndCP( absCpInd );

    // Check we haven't run past the total number of parameters
    assert( pInd < nParams_ );

    // Run final consistancy check to see that the resulting parameter is the correct type
    assert ( type_[pInd] == pType );

    return pInd;
};


/**
 * Get parameter index of a the pnum-th parameter of a type pType within control point absCpInd.
 * Do without checking type. Used during initialization when type vector has not been populated.
 */
inline unsigned int DirectParameterVector::get_PInd_Initial( unsigned int absCpInd, ParameterType pType, unsigned int pnum=0 ) const {
    // If there aren't any of this parameter type then what am I supposed to return
    assert ( pnum < get_nParamOfType(absCpInd, pType) );

    // find the parameter index
    unsigned int pInd = 0;
    switch ( pType ) {
    case YLeaf:
        pInd += nXLeafParams_[ absCpInd ];
    case XLeaf:
        pInd += nYJawParams_[ absCpInd ];
    case YJaw:
        pInd += nXJawParams_[ absCpInd ];
    case XJaw:
        pInd += nCouchParams_[ absCpInd ];
    case Couch:
        pInd += nCollimatorParams_[ absCpInd ];
    case Collimator:
        pInd += nGantryParams_[ absCpInd ];
    case Gantry:
        pInd += nDoseRateParams_[ absCpInd ];
    case DoseRate:
        pInd += nBeamEnergyParams_[ absCpInd ];
    case BeamEnergy:
        pInd += nWeightingParams_[ absCpInd ];
    case Weight:
        break;
    }
    pInd += pnum;

    // Check we haven't run past the end of the control point
    assert ( pInd < paramPerCP_[ absCpInd ] );

    pInd += get_PIndCP( absCpInd );

    // Check we haven't run past the total number of parameters
    assert( pInd < nParams_ );

    return pInd;
};


/**
 * Get parameter index of first parameter relating to a certain control point
 */
inline unsigned int DirectParameterVector::get_PIndCP( unsigned int absCpInd ) const {
	unsigned int pInd = 0;
	for ( unsigned int cp=0; cp<absCpInd; cp++ )  pInd += paramPerCP_[ cp ];
    return pInd;
};
/// Get parameter index of first parameter relating to a certain control point within a certain field.
inline unsigned int DirectParameterVector::get_PIndCP( unsigned int fieldInd, unsigned int cpInd ) const {
	return get_PIndCP( get_absCPindex( fieldInd, cpInd ) );
};
/// Get parameter index of first parameter relating to a certain field.
inline unsigned int DirectParameterVector::get_PIndField( unsigned int fieldInd ) const {
	return get_PIndCP( get_absCPindex( fieldInd, 0 ) );
};


/// Get the control point parameter value by meaning

/**
 * Test if the parameter type is fixed in a given field
 */
 inline bool DirectParameterVector::isConstantWithinField( unsigned int absCpInd, ParameterType pType ) const {
    switch ( pType ) {
    case YLeaf:
        return isConstantYLeafInFd_[ get_Fieldindex(absCpInd) ];
    case XLeaf:
        return isConstantXLeafInFd_[ get_Fieldindex(absCpInd) ];
    case YJaw:
        return isConstantYJawInFd_[ get_Fieldindex(absCpInd) ];
    case XJaw:
        return isConstantXJawInFd_[ get_Fieldindex(absCpInd) ];
    case Couch:
        return isConstantCouchInFd_[ get_Fieldindex(absCpInd) ];
    case Collimator:
        return isConstantCollInFd_[ get_Fieldindex(absCpInd) ];
    case Gantry:
        return isConstantGantryInFd_[ get_Fieldindex(absCpInd) ];
    case DoseRate:
        return isConstantDoseRateInFd_[ get_Fieldindex(absCpInd) ];
    case BeamEnergy:
        return isConstantEnergyInFd_[ get_Fieldindex(absCpInd) ];
    case Weight:
        return false;
    default:
        cerr << "DirectParameterVector::isConstantWithinField(): Invalid parameter type : " << pType << endl;
        throw DirectParameterVector_exception();
    }
 };

/**
 * Get a value for the pNum-th parameter of a given type within a certain control point.
 * If a partameter is set to be constant within a field then return the parameter value appropriate for that field,
 * even if the actual control point queried does not contain a value.
 */
inline float DirectParameterVector::get_valueAtCP( unsigned int absCpInd, ParameterType pType, unsigned int pNum ) const {
    if ( get_nParamOfType( absCpInd, pType ) > 0 )
        return get_value( get_PInd( absCpInd, pType, pNum ) );

    unsigned int cpFirst = get_absCPindex( get_Fieldindex( absCpInd ), 0 );
    if ( isConstantWithinField( absCpInd, pType ) && get_nParamOfType( cpFirst, pType ) > 0 )
        return get_value( get_PInd( cpFirst, pType, pNum ) );

    // Otherwise no direct value can be extracted from parameters.
    throw DirectParameterVector_exception();
};

/**
 * Get the pNum-th parameter value for the weight in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_Weight( unsigned int absCpInd, int pNum=0 ) const {
    try {
        return get_valueAtCP( absCpInd, Weight, pNum );
    } catch ( DirectParameterVector_exception & e ) {
        return 0.0f; // No weighting entry so set dose to zero.
    }
};

/**
 * Get the pNum-th parameter value for the beam energy in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_BeamEnergy( unsigned int absCpInd, unsigned int pNum=0 ) const {
    try {
        return get_valueAtCP(absCpInd, BeamEnergy, pNum);
    } catch ( DirectParameterVector_exception & e) {
        return machine_->getBeamEnergy()->getDefaultIndex();
    }
};

/**
 * Get the pNum-th parameter value for the doserate in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_DoseRate( unsigned int absCpInd, unsigned int pNum=0 ) const {
    try {
        return get_valueAtCP(absCpInd, DoseRate, pNum);
    } catch ( DirectParameterVector_exception & e) {
        return machine_->getDoseRate()->getDefaultValue();
    }
};

/**
 * Get the pNum-th parameter value for the Gantry in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_Gantry( unsigned int absCpInd, unsigned int pNum=0 ) const {
    try {
        return get_valueAtCP(absCpInd, Gantry, pNum);
    } catch ( DirectParameterVector_exception & e) {
        return 0.0f; // No gantry angle entry so set gantry angle to zero.
    }
};

/**
 * Get the pNum-th parameter value for the Collimator in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_Collimator( unsigned int absCpInd, unsigned int pNum=0 ) const {
    try {
        return get_valueAtCP(absCpInd, Collimator, pNum);
    } catch ( DirectParameterVector_exception & e) {
        return 0.0f; // No collimator angle entry so set collimator angle to zero.
    }
};

/**
 * Get the pNum-th parameter value for the Couch in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_Couch( unsigned int absCpInd, unsigned int pNum=0 ) const {
    try {
        return get_valueAtCP(absCpInd, Couch, pNum);
    } catch ( DirectParameterVector_exception & e) {
        return 0.0f; // No couch position entry so set couch angle to zero.
    }
};

/**
 * Get the pNum-th parameter value for the XJaw in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_XJaw( unsigned int absCpInd, unsigned int pNum  ) const {
    try {
        return get_valueAtCP(absCpInd, XJaw, pNum);
    } catch ( DirectParameterVector_exception & e) {
        unsigned int absCPFd = get_absCPindex( get_Fieldindex(absCpInd), 0 );
        vector<float> leafPos;
        // Try to infer jaw position from mlc

        // Find a set of mlc parameters from which to infer jaw position
        if ( nXLeafParams_[absCpInd] > 0 ) {
            get_XLeaves( absCpInd, leafPos, pNum );
        } else if ( nXLeafParams_[absCPFd] > 0 ) {
            get_XLeaves( absCPFd, leafPos, pNum );
        } else {
            // Otherwise set jaw to fully retracted
            assert( pNum < machine_->getNXJaws() );
            if ( machine_->getXJaw(pNum)->getIsWithdrawDirectionPositive() ) {
                return machine_->getXJaw(pNum)->getMaxValue();
            } else {
                return machine_->getXJaw(pNum)->getMinValue();
            }
        }

        // Infer jaw position from maximally retracted leaf position
        float limVal;
        if ( machine_->getXJaw(pNum)->getIsWithdrawDirectionPositive() ) {
            limVal = machine_->getXJaw(pNum)->getMinValue();
            for ( vector<float>::iterator lfIt = leafPos.begin(); lfIt != leafPos.end(); ++lfIt )
                limVal = ( *lfIt > limVal ) ? *lfIt : limVal;
        } else {
            limVal = machine_->getXJaw(pNum)->getMaxValue();
            for ( vector<float>::iterator lfIt = leafPos.begin(); lfIt != leafPos.end(); ++lfIt )
                limVal = ( *lfIt < limVal ) ? *lfIt : limVal;
        }

        return limVal;
    }
};

/**
 * Get the pNum-th parameter value for the YJaw in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_YJaw( unsigned int absCpInd, unsigned int pNum ) const {
    try {
        return get_valueAtCP(absCpInd, YJaw, pNum);
    } catch ( DirectParameterVector_exception & e) {
        unsigned int absCPFd = get_absCPindex( get_Fieldindex(absCpInd), 0 );
        vector<float> leafPos;
        // Try to infer jaw position from mlc

        // Find a set of mlc parameters from which to infer jaw position
        if ( nYLeafParams_[absCpInd] > 0 ) {
            get_YLeaves( absCpInd, leafPos, pNum );
        } else if ( nYLeafParams_[absCPFd] > 0 ) {
            get_YLeaves( absCPFd, leafPos, pNum );
        } else {
            // Otherwise set jaw to fully retracted
            assert( pNum < machine_->getNYJaws() );
            if ( machine_->getYJaw(pNum)->getIsWithdrawDirectionPositive() ) {
                return machine_->getYJaw(pNum)->getMaxValue();
            } else {
                return machine_->getYJaw(pNum)->getMinValue();
            }
        }

        // Infer jaw position from maximally retracted leaf position
        float limVal;
        if ( machine_->getYJaw(pNum)->getIsWithdrawDirectionPositive() ) {
            limVal = machine_->getYJaw(pNum)->getMinValue();
            for ( vector<float>::iterator lfIt = leafPos.begin(); lfIt != leafPos.end(); ++lfIt )
                limVal = ( *lfIt > limVal ) ? *lfIt : limVal;
        } else {
            limVal = machine_->getYJaw(pNum)->getMaxValue();
            for ( vector<float>::iterator lfIt = leafPos.begin(); lfIt != leafPos.end(); ++lfIt )
                limVal = ( *lfIt < limVal ) ? *lfIt : limVal;
        }

        return limVal;
    }
};

/**
 * Get the pNum-th parameter value for the XLeaf in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_XLeaf( unsigned int absCpInd, unsigned int pNum ) const {
    try {
        return get_valueAtCP(absCpInd, XLeaf, pNum);
    } catch ( DirectParameterVector_exception & e) {
        unsigned int absCPFd = get_absCPindex( get_Fieldindex(absCpInd), 0 );
        // No leaf parameters so align leaf position with the jaw

        // Find a set of mlc parameters from which to infer jaw position

        // Without any parameters assume that pnum is index of physical parameter
        unsigned int jawNum = (pNum < (machine_->getNXLeaves() / 2)) ? 0 : 1;

        if ( nXJawParams_[absCpInd] > 0 ) {
            return get_valueAtCP( absCpInd, XJaw, jawNum );
        } else if ( nXJawParams_[absCPFd] > 0 ) {
            return get_valueAtCP( absCPFd, XJaw, jawNum );
        } else {
            // Otherwise set leaf to fully retracted
            assert( pNum < machine_->getNXLeaves() );
            if ( machine_->getXLeaf(pNum)->getIsWithdrawDirectionPositive() ) {
                return machine_->getXLeaf(pNum)->getMaxValue();
            } else {
                return machine_->getXLeaf(pNum)->getMinValue();
            }
        }
    }
};

/**
 * Get the pNum-th parameter value for the YLeaf in this control point.
 * If there is no such parameter in this control point attempt to find the appropriate value.
 */
inline float DirectParameterVector::get_YLeaf( unsigned int absCpInd, unsigned int pNum ) const {
    try {
        return get_valueAtCP(absCpInd, YLeaf, pNum);
    } catch ( DirectParameterVector_exception & e) {
        unsigned int absCPFd = get_absCPindex( get_Fieldindex(absCpInd), 0 );
        // No leaf parameters so align leaf position with the jaw

        // Find a set of mlc parameters from which to infer jaw position

        // Without any parameters assume that pnum is index of physical parameter
        unsigned int jawNum = (pNum < (machine_->getNYLeaves() / 2)) ? 0 : 1;

        if ( nYJawParams_[absCpInd] > 0 ) {
            return get_valueAtCP( absCpInd, YJaw, jawNum );
        } else if ( nYJawParams_[absCPFd] > 0 ) {
            return get_valueAtCP( absCPFd, YJaw, jawNum );
        } else {
            // Otherwise set leaf to fully retracted
            assert( pNum < machine_->getNYLeaves() );
            if ( machine_->getYLeaf(pNum)->getIsWithdrawDirectionPositive() ) {
                return machine_->getYLeaf(pNum)->getMaxValue();
            } else {
                return machine_->getYLeaf(pNum)->getMinValue();
            }
        }
    }
};

/// Find the leaf positions for one whole bank of leaves
inline bool DirectParameterVector::get_XLeaves( unsigned int absCpInd, vector<float> & leafPos, unsigned int bankNum ) const {
    assert( bankNum == 0 || bankNum == 1 );

    unsigned int nLeafPairs = machine_->getNXLeaves() / 2;
    if ( nLeafPairs == 0 ) return false;

    leafPos.clear();
    leafPos.reserve(nLeafPairs);
    for ( unsigned int lf=0; lf<nLeafPairs; ++lf )
        leafPos.push_back( get_XLeaf(absCpInd, lf + machine_->getNXLeaves() * bankNum / 2 ) );

    return true;
};

/// Find the leaf positions for one whole bank of leaves
inline bool DirectParameterVector::get_YLeaves( unsigned int absCpInd, vector<float> & leafPos, unsigned int bankNum ) const {
    assert( bankNum == 0 || bankNum == 1 );

    unsigned int nLeafPairs = machine_->getNYLeaves() / 2;
    if ( nLeafPairs == 0 ) return false;

    leafPos.clear();
    leafPos.reserve(nLeafPairs);
    for ( unsigned int lf=0; lf<nLeafPairs; ++lf )
        leafPos.push_back( get_YLeaf(absCpInd, lf + machine_->getNYLeaves() * bankNum / 2) );

    return true;
};


/**
 * Get a value from the valid value list
 */
inline float DirectParameterVector::get_validValue( unsigned int ip, unsigned int iv ) const {

	assert( ip < this->get_nParameters() );

    float val;
    if ( ! isDiscreteVariable_[ip] ) {
        cerr << " Cannot return indexed value of a continuously variable parameter ";
        throw new DirectParameterVector_exception();
    }

    if ( step_size_[ip] > 0 ) {
        val = get_lowBound(ip) + ( step_size_[ip] * iv );
        assert( val < get_highBound(ip) );
    } else {
        val = ((validValues_.find(ip))->second).at(iv);
    }
    return val;
}


/**
 * Round the requested speed to a valid value from the list of valid values.
 */
inline float DirectParameterVector::get_RoundToValidValue( int ip, float requestVal ) const {
    // Set to maximum value, minimum value or requested value as apporpriate
    float outValue = ( requestVal < get_lowBound(ip) ) ? get_lowBound(ip) :
            ( ( requestVal > get_highBound(ip) ) ? get_highBound(ip) : requestVal );

    if ( ! isDiscreteVariable_[ip] ) return outValue;

    if ( step_size_[ip] > 0 ) {
        // round value to nearest step
        // ceil (n - 0.5) = round. ceil is in std lib round is not.
        outValue = get_absLowBound(ip) +
                ceil( ( outValue - get_absLowBound(ip) ) / get_step_size(ip) - 0.5f ) * get_step_size(ip);
        outValue += outValue < get_lowBound(ip) ? get_step_size(ip) : 0.0f;
        outValue -= outValue > get_highBound(ip) ? get_step_size(ip) : 0.0f;
    } else {
        // find index of first value greater than requested value.
        // ASSUMES THAT VALID VALUES ARE PROPERLY SORTED IN ACCENDING ORDER
        vector<float> valuelist = validValues_.find(ip)->second;
        vector<float>::iterator itGt = find_if( valuelist.begin(), valuelist.end(),
                bind2nd( greater<float>(), outValue ) ); // bind greater than outValue to function with one argument.
        outValue = (itGt==valuelist.end() || (outValue-*(itGt-1))<(*itGt-outValue)) ? *(itGt-1) : *itGt;
    }

    return outValue;
}


/**
 * Get the field weighting factor
 */
inline float DirectParameterVector::get_fieldWeight( unsigned int fieldInd ) const {
    float fieldWeight = 0.0f;
    for ( unsigned int fd=0; fd < nf_; fd++ ) {
        fieldWeight += get_Weight( get_absCPindex(fieldInd, 0), 0 );
    }
    return fieldWeight;
};


/**
 * Set the isDiscreteVariable flag indicating that the parameter is confined to regular steps rather
 * than continuously variable between the upper and lower bounds.
 * N.B. Exception thrown if parameter is only defined at discrete irregular positions.
 */
inline void DirectParameterVector::set_isDiscreteVariable( int ip, bool isDiscrete ) {
    if ( !isDiscrete && step_size_[ip] <= 0 ) {
        cerr << "Parameter has a finite set of possible values cannot change to continuously variable.";
        throw DirectParameterVector_exception();
    }
    isDiscreteVariable_[ip] = isDiscrete;
};


/**
 * Set the control point weightings to relative dose.
 *
 * When set to relative dose the control point and any field weights are relative [0-1]
 * and the doseNormFactor scales this dose to absolute values.
 *
 * To be safe function does not assume that vector is either completely in relative or absolute
 * state but may be a mixture of the two. After running this function the dose can be scaled by
 * changing the doseNormFactor.
 */
inline void DirectParameterVector::set_WeightingCPRelative() {
    float wtTot = 0.0f;
    vector<float> wtVals(ncpTot_);
    for ( unsigned int cp=0; cp<ncpTot_; ++cp ) {
        wtVals.push_back( get_value( get_PIndCP( cp ) ) );
        wtTot += wtVals[cp];
    }

    for ( unsigned int cp=0; cp<ncpTot_; ++cp )
        value_[ get_PIndCP( cp ) ] = wtVals[cp] / wtTot;

    doseNormFactor_ *= wtTot;
    isWeightingsAbsolute_ = false;
    resetLimits();
}


/**
 * Set the control point weightings to absolute dose.
 *
 * When set to absolute dose the doseFactor and any field weights are set to one
 * and the control point weights are in absolute values.
 *
 * To be safe function does not assume that vector is either completely in relative or absolute
 * state but may be a mixture of the two. The function assumes that prior to running the function
 * the correct prescription dose has been decided by appropriately scaling the doseNormFactor.
 */
inline void DirectParameterVector::set_WeightingCPAbsolute() {
    set_WeightingCPRelative();

    for ( unsigned int cp=0; cp<ncpTot_; ++cp )
        value_[ get_PIndCP( cp ) ] *= doseNormFactor_;

    doseNormFactor_ = 1.0f;
    isWeightingsAbsolute_ = true;
    resetLimits();
}


/// Scale the control point weightings by a dose weight scaling factor.
inline void DirectParameterVector::scale_intensities( float scale_factor ) {
    if ( isWeightingsAbsolute_ ) {
        for ( unsigned int fd=0; fd<nf_; fd++ ) {
            for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
                unsigned int pm = get_PIndCP( fd, cp );
                float newVal = value_[pm] * scale_factor;
                value_[pm] = newVal;
            }
        }
    } else {
        doseNormFactor_ *= scale_factor;
    }
    resetLimits();
};


/// Set the control point weightings to zero
inline void DirectParameterVector::set_zero_intensities() {
    for ( unsigned int fd=0; fd<nf_; fd++ ) {
        for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
            unsigned int pm = get_PIndCP( fd, cp );
            value_[pm] = 0.0f;
        }
    }
    resetLimits();
};

/// Set the control point weightings to give equal dose per beam
inline void DirectParameterVector::set_unit_intensities() {
    for ( unsigned int fd=0; fd<nf_; fd++ ) {
        // Set field weight to one
        // i.e. set control point weights to 1/ncp
        float cpWt = 1.0f / ncp_[fd];
        for ( unsigned int cp=0; cp<ncp_[fd]; cp++ ) {
            unsigned int pm = get_PIndCP( fd, cp );
            assert(cpWt >= lowBound_[pm] && cpWt <= highBound_[pm]);
            value_[pm] = cpWt;
        }
    }
    resetLimits();
};

/**
 * Find out if a field is an arc or a fixed beam angle.
 * For this purpose an arc is defined as a field with a moving gantry, couch or collimator.
 */
inline bool DirectParameterVector::isArc( int fd ) const {
    // An arc must have more than one control point
    if ( ncp_[fd] < 2 ) return false;
    return (!( isConstantGantryInFd_[fd] && isConstantCollInFd_[fd] && isConstantCouchInFd_[fd] ));
};

/// Find out if the parameter can change between control points.
inline bool DirectParameterVector::get_canChangeBetweenCP( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return true;
    case BeamEnergy:
        return machine_->getBeamEnergy()->getCanChangeDuringDelivery();
    case DoseRate:
        return machine_->getDoseRate()->getCanChangeDuringDelivery();
    case Gantry:
        return machine_->getGantry()->getCanChangeDuringDelivery();
    case Collimator:
        return machine_->getCollimator()->getCanChangeDuringDelivery();
    case Couch:
        return machine_->getCouch()->getCanChangeDuringDelivery();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getCanChangeDuringDelivery();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getCanChangeDuringDelivery();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getCanChangeDuringDelivery();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getCanChangeDuringDelivery();
    }
    throw DirectParameterVector_exception();
    return true;
};

/// Find out if the parameter can change direction during the delivery.
inline bool DirectParameterVector::get_canChangeDirection( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return true;
    case BeamEnergy:
        return true;
    case DoseRate:
        return machine_->getDoseRate()->getCanChangeDirection();
    case Gantry:
        return machine_->getGantry()->getCanChangeDirection();
    case Collimator:
        return machine_->getCollimator()->getCanChangeDirection();
    case Couch:
        return machine_->getCouch()->getCanChangeDirection();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getCanChangeDirection();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getCanChangeDirection();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getCanChangeDirection();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getCanChangeDirection();
    }
    throw DirectParameterVector_exception();
    return true;
};

/// Get the minimum non-zero speed of a parameter value change
inline float DirectParameterVector::get_minSpeed( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return 0.0f;
    case BeamEnergy:
        return 0.0f;
    case DoseRate:
        return machine_->getDoseRate()->getMinSpeed();
    case Gantry:
        return machine_->getGantry()->getMinSpeed();
    case Collimator:
        return machine_->getCollimator()->getMinSpeed();
    case Couch:
        return machine_->getCouch()->getMinSpeed();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getMinSpeed();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getMinSpeed();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getMinSpeed();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getMinSpeed();
    }
    throw DirectParameterVector_exception();
    return 0.0f;
};

/// Get the maximum speed of a parameter value change
inline float DirectParameterVector::get_maxSpeed( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return numeric_limits<float>::infinity();
    case BeamEnergy:
        return numeric_limits<float>::infinity();
    case DoseRate:
        return machine_->getDoseRate()->getMaxSpeed();
    case Gantry:
        return machine_->getGantry()->getMaxSpeed();
    case Collimator:
        return machine_->getCollimator()->getMaxSpeed();
    case Couch:
        return machine_->getCouch()->getMaxSpeed();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getMaxSpeed();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getMaxSpeed();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getMaxSpeed();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getMaxSpeed();
    }
    throw DirectParameterVector_exception();
    return 0.0f;
};

/// Get the maximum acceleration of a parameter value change
inline float DirectParameterVector::get_maxAcceleration( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return numeric_limits<float>::infinity();
    case BeamEnergy:
        return numeric_limits<float>::infinity();
    case DoseRate:
        return machine_->getDoseRate()->getMaxAcceleration();
    case Gantry:
        return machine_->getGantry()->getMaxAcceleration();
    case Collimator:
        return machine_->getCollimator()->getMaxAcceleration();
    case Couch:
        return machine_->getCouch()->getMaxAcceleration();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getMaxAcceleration();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getMaxAcceleration();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getMaxAcceleration();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getMaxAcceleration();
    }
    throw DirectParameterVector_exception();
    return 0.0f;
};

/// Find out if a parameter change requires a beam interrupt.
inline bool DirectParameterVector::get_changeRequiresInterrupt( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return false;
    case BeamEnergy:
        return machine_->getBeamEnergy()->getChangeRequiresInterrupt();
    case DoseRate:
        return machine_->getDoseRate()->getChangeRequiresInterrupt();
    case Gantry:
        return machine_->getGantry()->getChangeRequiresInterrupt();
    case Collimator:
        return machine_->getCollimator()->getChangeRequiresInterrupt();
    case Couch:
        return machine_->getCouch()->getChangeRequiresInterrupt();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getChangeRequiresInterrupt();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getChangeRequiresInterrupt();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getChangeRequiresInterrupt();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getChangeRequiresInterrupt();
    }
    throw DirectParameterVector_exception();
    return true;
};

/// Get the value of any latency in restarting the beam after an interruption.
inline float DirectParameterVector::get_interruptLatency( int ip ) const {
    switch ( type_[ip] ) {
    case Weight:
        return 0.0f;
    case BeamEnergy:
        return machine_->getBeamEnergy()->getInterruptLatency();
    case DoseRate:
        return machine_->getDoseRate()->getInterruptLatency();
    case Gantry:
        return machine_->getGantry()->getInterruptLatency();
    case Collimator:
        return machine_->getCollimator()->getInterruptLatency();
    case Couch:
        return machine_->getCouch()->getInterruptLatency();
    case XJaw:
        return machine_->getXJaw(ip-get_PInd(paramIndToCPInd(ip),XJaw,0))->getInterruptLatency();
    case YJaw:
        return machine_->getYJaw(ip-get_PInd(paramIndToCPInd(ip),YJaw,0))->getInterruptLatency();
    case XLeaf:
        return machine_->getXLeaf(ip-get_PInd(paramIndToCPInd(ip),XLeaf,0))->getInterruptLatency();
    case YLeaf:
        return machine_->getYLeaf(ip-get_PInd(paramIndToCPInd(ip),YLeaf,0))->getInterruptLatency();
    }
    throw DirectParameterVector_exception();
    return 0.0f;
};


/// Get the minimum x axis position on the bixel grid for a given parameter.
inline float DirectParameterVector::get_minXAx( unsigned int ip ) const {
    unsigned int bm = get_beamNo(ip);
    float minX = geometry_->get_bixel_minX( bm );
    return minX;
};

/// Get the maximum x axis position on the bixel grid for a given parameter.
inline float DirectParameterVector::get_maxXAx( unsigned int ip ) const {
    unsigned int bm = get_beamNo(ip);
    float maxX = geometry_->get_bixel_maxX( bm );
    return maxX;
};

/// Get the minimum y axis position on the bixel grid for a given parameter.
inline float DirectParameterVector::get_minYAx( unsigned int ip ) const {
    unsigned int bm = get_beamNo(ip);
    float minY = geometry_->get_bixel_minY( bm );
    return minY;
};

/// Get the maximum y axis position on the bixel grid for a given parameter.
inline float DirectParameterVector::get_maxYAx( unsigned int ip ) const {
    unsigned int bm = get_beamNo(ip);
    float maxY = geometry_->get_bixel_maxY( bm );
    return maxY;
};


#endif // DIRECTPARAMETERVECTOR_HPP
