/**
 * @file DijFileParser.cpp
 * DijFileParser class implementation
 *
 */

#include <stdexcept>
#include <cstring>
#include <limits.h>

#include "DijFileParser.hpp"


/**
 * Constructor that reads Konrad formatted Dij from a stream.
 */
DijFileParser::DijFileParser(
	std::string file_name ///< The file to read from
	)
: file_name_(file_name),
    reading_(true),
    input_file_(),
    nBixels_read_(0),
    writing_(false),
    output_file_(),
    nBixels_written_(0),
    header_()
{
    // Open file
    input_file_.reset(new std::ifstream(file_name_.c_str(), std::ios::binary));

    if(!input_file_->good()) {
	throw(DijFileParser::exception("Error opening file for reading"));
    }

    // Read header
    header_.a_g_ = read_binary_float(*input_file_);
    header_.a_t_ = read_binary_float(*input_file_);
    header_.a_c_ = read_binary_float(*input_file_);
    header_.dx_b_ = read_binary_float(*input_file_);
    header_.dy_b_ = read_binary_float(*input_file_);
    header_.dx_ = read_binary_float(*input_file_);
    header_.dy_ = read_binary_float(*input_file_);
    header_.dz_ = read_binary_float(*input_file_);
    header_.nx_ = read_binary_int(*input_file_);
    header_.ny_ = read_binary_int(*input_file_);
    header_.nz_ = read_binary_int(*input_file_);
    header_.npb_ = read_binary_int(*input_file_);
    header_.scalefactor_ = read_binary_float(*input_file_);
}

/**
 * Constructor that writes Konrad formatted Dij to a stream.
 */
DijFileParser::DijFileParser(
	std::string file_name, ///< The stream to write to
	DijFileParser::header head ///< The header for the file to write
	)
: file_name_(file_name),
    reading_(false),
    input_file_(),
    nBixels_read_(0),
    writing_(true),
    output_file_(),
    nBixels_written_(0),
    header_(head)
{
    // Open file
    output_file_.reset(new std::ofstream(file_name_.c_str(), std::ios::binary));
    if(!output_file_->good()) {
	throw DijFileParser::exception("Error opening file for writing");
    }

    // Write header
    write_binary_float(*output_file_, header_.a_g_);
    write_binary_float(*output_file_, header_.a_t_);
    write_binary_float(*output_file_, header_.a_c_);
    write_binary_float(*output_file_, header_.dx_b_);
    write_binary_float(*output_file_, header_.dy_b_);
    write_binary_float(*output_file_, header_.dx_);
    write_binary_float(*output_file_, header_.dy_);
    write_binary_float(*output_file_, header_.dz_);
    write_binary_int(*output_file_, header_.nx_);
    write_binary_int(*output_file_, header_.ny_);
    write_binary_int(*output_file_, header_.nz_);
    write_binary_int(*output_file_, header_.npb_);
    write_binary_float(*output_file_, header_.scalefactor_);
}


/**
 * Destructor.  Throws an exception if the file hasn't been read or written all
 * the way.
 */
DijFileParser::~DijFileParser()
{
    if(reading_) {
	if(nBixels_read_ != header_.npb_) {
	    throw DijFileParser::exception("Didn't read all of the file");
	}
    }
    if(writing_) {
	if(nBixels_written_ != header_.npb_) {
	    throw DijFileParser::exception("Didn't write all of the file");
	}
    }
}


/**
 * Read the next bixel
 */
void DijFileParser::read_next_bixel(
	DijFileParser::bixel & bixel ///< The object to hold the results
	)
{
    assert(reading_);
    assert(input_file_->good());

    // Read bixel header
    bixel.energy_ = read_binary_float(*input_file_);
    bixel.spot_x_ = read_binary_float(*input_file_);
    bixel.spot_y_ = read_binary_float(*input_file_);
    bixel.nVox_ = read_binary_int(*input_file_);

    // Read bixel data
    bixel.voxelNo_.resize(bixel.nVox_);
    bixel.value_.resize(bixel.nVox_);
    for(int i = 0; i < bixel.nVox_; i++) {
	bixel.voxelNo_[i] = read_binary_int(*input_file_);
	bixel.value_[i] = read_binary_short(*input_file_);
    }

    nBixels_read_++;
}


/**
 * Write the next bixel
 */
void DijFileParser::write_next_bixel(
	const DijFileParser::bixel & bixel ///< The data to write
	)
{
    assert(writing_);
    assert(output_file_->good());
    assert(bixel.nVox_ == (int) bixel.voxelNo_.size());
    assert(bixel.nVox_ == (int) bixel.value_.size());

    // Write bixel header
    write_binary_float(*output_file_, bixel.energy_);
    write_binary_float(*output_file_, bixel.spot_x_);
    write_binary_float(*output_file_, bixel.spot_y_);
    write_binary_int(*output_file_, bixel.nVox_);

    // Write bixel data
    for(int i = 0; i < bixel.nVox_; i++) {
	write_binary_int(*output_file_, bixel.voxelNo_[i]);
	write_binary_short(*output_file_, bixel.value_[i]);
    }

    nBixels_written_++;
}


/**
 *  Test if the compiled endianness is correct
 */
void DijFileParser::test_endianness() const
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    unsigned int num = 0;

    // Make sure the int is the right size
    if (sizeof num != 4) {
	throw(std::logic_error("wrong sized ints."));
    }

    //
    unsigned int placeholder = '1';
    unsigned char repr[sizeof num];
    for(size_t i = 0; i < sizeof num; i++) {
	// Left-shift the current value of num by one byte
	num <<= CHAR_BIT;

	// Add the value of placeholder, and increase it by one
	num |= placeholder++;
    }

    // Copy the representation of num into repr
    memcpy(repr, &num, sizeof num);

    // check wheter it is big endian
    if(big_endian) {
	placeholder = '1';
	for(size_t i = 0; i < sizeof num; i++)
	    if (repr[i] != placeholder++)
		throw std::logic_error("Not big endian");
    }

    // check if it's little endian
    if(!big_endian) {
	placeholder = '1';
	for(size_t i = 0; i < sizeof num; i++)
	    if (repr[sizeof num - 1 - i] != placeholder++)
		throw std::logic_error(
			"Not little endian.  Must declare __big_endian__");
    }
}


/*
 * DijFileParser::header functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */


/**
 * Default Constructor.
 */
DijFileParser::header::header()
: a_g_(0),
    a_t_(0),
    a_c_(0),
    dx_b_(0),
    dy_b_(0),
    dx_(0),
    dy_(0),
    dz_(0),
    nx_(0),
    ny_(0),
    nz_(0),
    npb_(0),
    scalefactor_(1)
{
}


/**
 * Constructor to build header from geometry class.  All of the variables are
 * set to meaningful values except for the scale factor, which is not in the
 * Geometry class.
 */
DijFileParser::header::header(
	const Geometry & geom, ///< The Geometry object to copy from
       	unsigned int beamNo     ///< The beam number to get info from
	)
: a_g_(geom.get_gantry_angle().at(beamNo)),
    a_t_(geom.get_table_angle().at(beamNo)),
    a_c_(geom.get_collimator_angle().at(beamNo)),
    dx_b_(geom.get_bixel_grid_dx().at(beamNo)),
    dy_b_(geom.get_bixel_grid_dy().at(beamNo)),
    dx_(geom.get_voxel_dx()),
    dy_(geom.get_voxel_dy()),
    dz_(geom.get_voxel_dz()),
    nx_(geom.get_voxel_nx()),
    ny_(geom.get_voxel_ny()),
    nz_(geom.get_voxel_nz()),
    npb_(geom.get_nBixels_in_beam(beamNo)),
    scalefactor_(1)
{
}

bool DijFileParser::header::operator==(const DijFileParser::header& rhs) const
{
    return
	(comp_float_abs(a_g_,rhs.a_g_,EPSILON)
	 && comp_float_abs(a_t_,rhs.a_t_,EPSILON)
	 && comp_float_abs(a_c_,rhs.a_c_,EPSILON)
	 && comp_float_abs(dx_b_,rhs.dx_b_,EPSILON)
	 && comp_float_abs(dy_b_,rhs.dy_b_,EPSILON)
	 && comp_float_abs(dx_,rhs.dx_,EPSILON)
	 && comp_float_abs(dy_,rhs.dy_,EPSILON)
	 && comp_float_abs(dz_,rhs.dz_,EPSILON)
	 && (nx_ == rhs.nx_)
	 && (ny_ == rhs.ny_)
	 && (nz_ == rhs.nz_)
	 && (npb_ == rhs.npb_)
	 && comp_float_abs(scalefactor_,rhs.scalefactor_,EPSILON));
}


std::ostream& operator<< (std::ostream& o,
	const DijFileParser::header& rhs)
{
    o << "a_g_ = " << rhs.a_g_
    << "\ta_t_ = " << rhs.a_t_
    << "\ta_c_ = " << rhs.a_c_
    << "\tdx_b_ = " << rhs.dx_b_
    << "\tdy_b_ = " << rhs.dy_b_
    << "\tdx_ = " << rhs.dx_
    << "\tdy_ = " << rhs.dy_
    << "\tdz_ = " << rhs.dz_
    << "\tnx_ = " << rhs.nx_
    << "\tny_ = " << rhs.ny_
    << "\tnz_ = " << rhs.nz_
    << "\tnpb_ = " << rhs.npb_
    << "\tscalefactor_ = " << rhs.scalefactor_
    << "\n";
}

