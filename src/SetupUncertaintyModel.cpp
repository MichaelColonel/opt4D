/**
 * @file: SetupUncertaintyModel.cpp
 * SetupUncertaintyModel Class implementation.
 *
 */

#include "SetupUncertaintyModel.hpp"
#include <sstream>

const float EPSILON = 0.000001;

// ---------------------------------------------------------------------
// Setup uncertainty
// ---------------------------------------------------------------------

/**
 * Constructor
 */
SetupUncertaintyModel::SetupUncertaintyModel(const Geometry *the_geometry,
					     const DoseDeliveryModel::options *options)
    : ShiftBasedErrorModel(the_geometry),
    use_shift_correction_(options->setup_use_improved_sdca_),
    use_randomized_rounding_(options->setup_use_randomized_rounding_),
    use_linear_interpolation_(options->setup_use_linear_interpolation_),
    merge_shifts_(options->setup_merge_shifts_),
    setup_model_(
           the_geometry,
            options->gaussian_setup_options_),
    surface_normal_(options->setup_surface_normals_)
{
    cout << "Constructing SetupUncertaintyModel:" << endl;
    cout << setup_model_ ;
    if(use_shift_correction_) {
	cout << "static dose cloud shift correction is applied:" << endl;
	for (unsigned int i=0; i< surface_normal_.size(); i++) {
	    cout << "surface normal vector beam " << i+1 << ": (";
	    cout << surface_normal_[i].x_ << ", " << surface_normal_[i].y_ << ", " << surface_normal_[i].z_ << ")" << endl;
	}
    }
}

/**
 * Destructor
 */
SetupUncertaintyModel::~SetupUncertaintyModel()
{
}

/**
 * This function corrects the primary patient setup shift for the shift 
 * of the static dose cloud for a given beam. 
 * The resulting voxel shift to be applied will be parallel to the patient surface
 * and represents the shift of the beam entrance point on the skin.
 */
Shift_in_Patient_Coord SetupUncertaintyModel::transform_setup_error_to_voxel_shift(
    Shift_in_Patient_Coord setup_shift, 
    unsigned int beamNo) const
{
    // get unit vector in beam direction
    Shift_in_Patient_Coord beam_uvec = 
	the_geometry_->transform_to_Shift_in_Patient_Coord(
	    Point_in_Gantry_Coord(0,0,1),beamNo);

    // get unit vector on patient surface
    Shift_in_Patient_Coord surface_uvec = surface_normal_[beamNo];

    // calculate effective voxel shift
    if( fabs(beam_uvec*surface_uvec) < EPSILON ) {
	// cannot perform shift 
	cout << "WARNING: cannot perform shift correction ";
	cout << "since beam is parallel to patient surface!" << endl;
	return(setup_shift);
    }
    else {
	return ( (beam_uvec*((setup_shift*surface_uvec)/(beam_uvec*surface_uvec)) - setup_shift)*(-1) );
    }
}


/**
 * generate sample setup scenario
 */
void SetupUncertaintyModel::generate_random_scenario(
        RandomScenario &random_scenario) const
{
    // Generate daily shifts (systematic+random)
    vector<Shift_in_Patient_Coord> daily_shifts;
    setup_model_.generate_random_shifts(daily_shifts);
    
    set_shifts_in_scenario(random_scenario, daily_shifts);
}


/**
 * Put shifts in random_scenario
 */
void SetupUncertaintyModel::set_shifts_in_scenario(
        RandomScenario &random_scenario,
        vector<Shift_in_Patient_Coord> daily_shifts) const
{
    unsigned int nFractions = daily_shifts.size();

    float default_weight = 1/static_cast<float>(nFractions);

    // Load default values into a temp_shift
    RigidDoseShift temp_shift;
    temp_shift.weight_ = default_weight;
    temp_shift.auxiliaryNo_ = 0;
    temp_shift.use_bixelNos_ = use_shift_correction_;
    temp_shift.use_bixel_auxiliaryNos_ = false;
    temp_shift.use_random_rounding_ = use_randomized_rounding_;

    // Empty shifts from random_scenario
    random_scenario.rigid_shifts_.resize(0);
    if(!use_shift_correction_) {
        // uncorrected static dose cloud approximation is used

        random_scenario.rigid_shifts_.reserve(nFractions);
        for(unsigned int iShift = 0; iShift < nFractions; iShift++) {
            temp_shift.shift_ = daily_shifts[iShift];
            random_scenario.rigid_shifts_.push_back(temp_shift);
        }

    } else {
        // otherwise generate a voxel shift for every beam

        unsigned int nBeams = the_geometry_->get_nBeams();

        random_scenario.rigid_shifts_.reserve(nBeams*nFractions);

        // Loop over all beams
        for(unsigned int iBeam=0; iBeam<nBeams; iBeam++) {

            //
            // set bixelNos for beam
            //

            // clear bixel numbers
            temp_shift.bixelNos_.resize(0);

            // set bixels that belong to the beam
            for(unsigned int iDB=0; iDB<the_geometry_->get_nBixels();
                    iDB++) {
                if(the_geometry_->get_beamNo(iDB) == iBeam) {
                    temp_shift.bixelNos_.push_back(iDB);
                }
            }

            // Loop over all fractions
            for(unsigned int iFraction=0; iFraction<nFractions; iFraction++) {

                unsigned int iShift = iBeam+iFraction*nBeams;

                // calculate effective voxel shift
                temp_shift.shift_ = transform_setup_error_to_voxel_shift(
                        daily_shifts[iFraction],iBeam);

                random_scenario.rigid_shifts_.push_back(temp_shift);
            }
        }
    }

    // Either use linear interpolation or rounding to find subscript shifts
    if(use_linear_interpolation_) {
        // Use linear interpolation
        random_scenario.linearly_interpolate_shifts(*the_geometry_);
    } else {
        // Just round the shifts to get the subscript shifts
        random_scenario.round_shifts(*the_geometry_);
    }


    // Merge shifts if appropriate
    if(merge_shifts_) {
        random_scenario.merge_shifts();
    }
}


/**
 * write results
 */
void SetupUncertaintyModel::write_dose(BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       string file_prefix, 
				       Dvh dvh) const
{
    write_scenario_dose(bixel_grid,dij,file_prefix,dvh);
}


/**
 * store a dose evaluation scenario
 */
bool SetupUncertaintyModel::set_dose_evaluation_scenario(
        map<string,string> scenario_map)
{
    float x,y,z,dummy,prob;
    string sx,sy,sz;
    unsigned int beamNo;
    vector<float> rangeShifts(0);
    RandomScenario scenario;

    // ----------------------- probability -----------------------------------

    std::istringstream iss_p(scenario_map["prob"]);

    if(!(iss_p >> dummy).fail()) {
	prob = dummy;
    }
    else {
	return false;
    }

    // ----------------------- setup -----------------------------------

    std::istringstream iss_s(scenario_map["setup"]);

    // read the string
    if((iss_s >> sx >> x >> sy >> y >> sz >> z).fail()) {
	return false;
    }
    if(!(sx=="x" && sy=="y" && sz=="z")) {
	return false;
    }

    // read shifts successfully 
    Shift_in_Patient_Coord shift = Shift_in_Patient_Coord(x,y,z);

    vector<Shift_in_Patient_Coord> shifts(1,shift);
    
    // set values in scenario
    set_shifts_in_scenario(scenario,shifts);

    // store scenario
    dose_eval_scenarios_.push_back(scenario);
    dose_eval_pdf_.push_back(prob);
    nDose_eval_scenarios_++;

    // print info to screen	
    cout << "set dose evaluation scenario " << dose_eval_scenarios_.size()
        << endl;
    cout << "Probability: " << prob << endl;
    cout << "Patient shift: " << shift << endl;
    cout << "effective shift / voxel subscript shifts per beam:" << endl;

    for(unsigned int i=0; i<scenario.rigid_shifts_.size(); i++) {
	cout << "beam " << i << ": " << scenario.rigid_shifts_[i].shift_
            << " / " << scenario.rigid_shifts_[i].subscript_shift_ << endl;
    }

    return(true);
}

