/**
 * @file MetaObjective.hpp
 * MetaObjective Class header
 */

#ifndef METAOBJECTIVE_HPP
#define METAOBJECTIVE_HPP

#include "Objective.hpp"
#include "Geometry.hpp"
#include "DoseDeliveryModel.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class MetaObjective is an abstract base class for all 
 * meta-objectives.  Meta-objectives are simply containers that collect 
 * several objectives togther.
 *
 * The Meta-objective contains a vector of primary objectives to be 
 * minimized.
 */
class MetaObjective
{

    public:

        // Constructor
        MetaObjective(unsigned int objNo,
                const Geometry *geometry,
                const DoseDeliveryModel &uncertainty,
                const vector<Objective*> &objectives
                );

        // Virtual destructor
        virtual ~MetaObjective();


        // Print description of objective
        friend std::ostream& operator<< (
                std::ostream& o, const MetaObjective& meta_objective);

        /// Prints a description of the objective.
        virtual void printOn(std::ostream& o) const;

        /// Print meta objective type
        virtual void print_meta_objective_type(std::ostream& o) const = 0;

        /// get number of meta objective
        unsigned int get_objNo() const {return objNo_;};

        /// whether the meta objective supports gradient evaluation
        virtual bool supports_gradient() const {return true;};

        /// Clase to hold estimate of meta-objective
        class estimate {
            public:
                estimate() {};

                double mean_objective_;
                double variance_;
                int nSamples_;
                vector<double> multi_objective_;
                vector<double> multi_variance_;
                vector<double> estimated_ssvo_;
        };


        /// calculate underlying objective for dose vector
        double calculate_dose_objective(
                const DoseVector & the_dose,
                vector<double> & multi_obj ) const;

        /**
         *  calculate or estimate objective
         *  appends estimated ssvo to supplied vector if available.
         */
        virtual double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                int nScenario_samples = 1,
                bool use_voxel_sampling = false,
                bool use_scenario_sampling = true) = 0;

        /// calculate or estimate objective and gradient 
        virtual double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                BixelVectorDirection & gradient,
				float gradient_multiplier = 1.0,
                int nScenario_samples = 1,
                bool use_voxel_sampling = false,
                bool use_scenario_sampling = true) = 0;

        /// not implemented
        virtual double calculate_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                BixelVectorDirection & gradient,
                const vector<float> & v,
                vector<float> & Hv,
                int nScenario_samples = 1,
                bool use_voxel_sampling = false,
                bool use_scenario_sampling = true) = 0;

        /// estimate the true objective by taking many samples
        virtual MetaObjective::estimate estimate_true_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                int max_samples,
                int min_samples,
                float max_uncertainty,
                bool use_voxel_sampling = true) = 0;

        /// calculate or estimate objective and gradient for one
        /// instance
        double calculate_scenario_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                BixelVectorDirection & gradient, 
                float gradient_multiplier,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_smapling = false);

        /// estimate objective and for one instance using voxel sampling
        double calculate_scenario_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                bool use_voxel_sampling = false);

        /// not implemented yet
        double calculate_scenario_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                BixelVectorDirection & gradient,
                float gradient_multiplier,
                const vector<float> & v,
                vector<float> & Hv,
                const DoseDeliveryModel &uncertainty_model,
                const RandomScenario &random_scenario,
                bool use_voxel_sampling = false
                );


        /// Find out how many objectives are in the multi-objective
        unsigned int get_nObjectives() const;

        /// Find out how many voxels were in objective if appropriate,
        /// otherwise 0
        vector<unsigned int> get_nVoxels() const;

        // Append nVoxels to vector
        void get_nVoxels(vector<unsigned int> &v) const;

        /// Find out voxel sampling rate if appropriate, otherwise 1
        vector<float> get_sampling_fraction() const;

        /// Append sampling fraction to a vector
        void get_sampling_fraction(vector<float> & v) const;

        /// Set voxel sampling rate for each objective
        void set_sampling_fraction(vector<float> sampling_fraction);

        /// Set voxel sampling rate based on iterators
        vector<float>::const_iterator set_sampling_fraction(
                vector<float>::const_iterator iter);

        /// Find out if objective uses sampling
        void supports_voxel_sampling(vector<bool> &v) const;

    protected:

        /// number of the meta objective
        unsigned int objNo_; 

        /// The geometry information
        const Geometry *the_geometry_;

        /// The uncertainty model
        const DoseDeliveryModel *the_UncertaintyModel_;

        /// the objectives that form the meta-objective
        vector<Objective*> the_objectives_;

        /// number of objectives included in this meta objective
        unsigned int nObjectives_;

    private:
        // Default constructor not allowed
        MetaObjective();
};


/**
 * Print MetaObjective to stream
 */
inline
std::ostream& operator<< (
        std::ostream& o, const MetaObjective& meta_objective)
{
  meta_objective.printOn(o);
  return o;
}


/**
 * Find out how many objectives are in the multi-objective
 */
inline
unsigned int MetaObjective::get_nObjectives() const
{
    return the_objectives_.size();
}


#endif
