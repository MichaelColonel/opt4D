/**
 * @file FastMeanObjective.hpp
 * FastMeanObjective Class header
 *
 * $ Id: $
 */

#ifndef TUMOREUDOBJECTIVE_HPP
#define TUMOREUDOBJECTIVE_HPP

#include "NonSeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class TumorEUDObjective implements Niemierko's EUD based on exponential cell kill.
 */
class TumorEUDObjective : public NonSeparableObjective
{

    public:

        // Constructor
        TumorEUDObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement);

        // Virtual destructor
        virtual ~TumorEUDObjective();


        // Initialize with the Dij
        virtual void initialize(DoseInfluenceMatrix& Dij);

        size_t get_voiNo() const;

        //
	// Functions reimplemented from NonSeparableObjective
        //
        virtual double calculate_objective(
                const vector<float> & dose) const;
        virtual double calculate_objective_and_d(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose) const;
        virtual double calculate_objective_and_d2(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose,
	        vector<float> & d2_obj_by_dose) const;


        /// Does the objective support RandomScenario?
        virtual bool supports_RandomScenario() const {return false;};

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;

    protected:

    private:
        // Default constructor not allowed
        TumorEUDObjective();

        // voi
  //        Voi *the_voi_;

        // radiosensitivity parameter, controls target homogeneity
        float alpha_;

        /// small number
        float epsilon_;
 };


/*
 * Inline functions
 */



inline
size_t TumorEUDObjective::get_voiNo() const
{
  return the_voi_->get_voiNo();
}


#endif
