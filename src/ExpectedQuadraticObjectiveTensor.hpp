/**
 * @file ExpectedQuadraticObjectiveTensor.hpp
 * ExpectedQuadraticObjectiveTensor class header
 *
 */

#ifndef EXPECTEDQUADRATICOBJECTIVETENSOR_HPP
#define EXPECTEDQUADRATICOBJECTIVETENSOR_HPP

class ExpectedQuadraticObjectiveTensor;

#include <vector>
using std::vector;
#include <string>
using std::string;

#include "DoseVector.hpp"
#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Objective.hpp"
#include "DoseDeliveryModel.hpp"



/**
 * This class is used to calculate a tensor that stores the contribution of a beamlet pair 
 * to the quadratic objective of a structure. This is primarily used in the context 
 * of incorporating uncertainties into the optimization when 
 * the expectation value of the quadratic objective is calculated.
 *
 * Note that this is a good way of evaluating the quadratic objective if the number of bixels
 * is much smaller than the number of voxels in the structure. For IMPT plans the number 
 * of bixels is about 10 times larger compared to IMRT plans, which makes the calculation 
 * of the objective 100 times slower. Therefore, this method can only be used if the 
 * number of bixels is relatively small.  
 *
 * The calculation of the tensor elements only implemented for 
 * some dose delivery models.
 *
 * The objective cannot be evaluated by a sampling method like in other objectives. 
 * Sampling could be applied in calculating the tensor elements but this
 * is not implemented.
 */
class ExpectedQuadraticObjectiveTensor : public Objective
{

public:
    /// constructor
    ExpectedQuadraticObjectiveTensor(Voi *the_voi, 
				     unsigned int objNo,
				     DoseInfluenceMatrix &Dij, 
				     DoseDeliveryModel &Uncertainty,
				     float desired_dose,
				     float weight,
				     string qjk_file_root,
				     bool read_qjk);

    // Virtual destructor
    virtual ~ExpectedQuadraticObjectiveTensor();

    // access objective contributions
    double get_LinearTerm(unsigned int BixelNo) const;
    double get_QuadraticTerm(unsigned int BixelNo_j, unsigned int BixelNo_k) const;

    // gradient optimization
    virtual double calculate_objective(
            const BixelVector & beam_weights,
            DoseInfluenceMatrix & Dij,
            bool use_voxel_sampling,
            double &estimated_ssvo);
    virtual double calculate_objective_and_gradient(
            const BixelVector & beam_weights,
            DoseInfluenceMatrix & Dij,
            BixelVectorDirection & gradient,
            float gradient_multiplier,
            bool use_voxel_sampling,
            double &estimated_ssvo);

    void printOn(std::ostream& o) const;

    // void initialize(DoseInfluenceMatrix& Dij);

    /// calculate quadratic objective based on dose vector. Does not take expectation.
    virtual double calculate_dose_objective(const DoseVector & the_dose);

    /// this function is not implemented
    virtual double calculate_objective_and_gradient_and_Hv(
	const BixelVector & beam_weights,
	DoseInfluenceMatrix & Dij,
	BixelVectorDirection & gradient,
        float gradient_multiplier,
	const vector<float> & v,
	vector<float> & Hv,
            bool use_voxel_sampling,
            double &estimated_ssvo);

    
    /// Does the objective support RandomScenario? (no)
    virtual bool supports_RandomScenario() const {return false;};

    
private:
    /// Make sure default constructor is never used
    ExpectedQuadraticObjectiveTensor();

    /// calculate bixel contributions to both linear and quadratic terms of the objective
    void calculate_contributions();

//    void add_to_LinearTerms(unsigned int BixelNo, float value);
//    void add_to_QuadraticTerms(unsigned int BixelNo_j, unsigned int BixelNo_k, float value);

    void read_contributions(string qjk_file_root);
    void write_contributions(string qjk_file_root);

    /// pointer to the Volume of interest
    Voi *the_voi_;

    /// The dose influence matrix
    DoseInfluenceMatrix *the_DoseInfluenceMatrix_;

    /// The uncertainty model
    DoseDeliveryModel *the_UncertaintyModel_;

    /// tensor for quadratic terms
    vector< vector<double> > QuadraticTerms_;

    /// vector for linear terms    
    vector<double> LinearTerms_;

    /// prescribed dose for targets, should be zero for OARs (does not make sence otherwise)
    float desired_dose_;

};

/*
inline
void ExpectedQuadraticObjectiveTensor::add_to_QuadraticTerms(unsigned int BixelNo_j, unsigned int BixelNo_k, float value)
{
    QuadraticTerms_[BixelNo_j][BixelNo_k] += (double)value;
}

inline
void ExpectedQuadraticObjectiveTensor::add_to_LinearTerms(unsigned int BixelNo, float value)
{
    LinearTerms_[BixelNo] += (double)value;
}
*/

inline
double ExpectedQuadraticObjectiveTensor::get_LinearTerm(unsigned int BixelNo) const
{
    return LinearTerms_[BixelNo];
}

inline
double ExpectedQuadraticObjectiveTensor::get_QuadraticTerm(unsigned int BixelNo_j, unsigned int BixelNo_k) const
{
    return QuadraticTerms_[BixelNo_j][BixelNo_k];
}


#endif
