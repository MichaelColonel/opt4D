/**
 * @file VvParser.hpp
 * Header for VvParser class.
 *  $Id: VvParser.hpp,v 1.1 2008/07/17 17:16:17 bmartin Exp $
 */

#ifndef VVPARSER_HPP
#define VVPARSER_HPP

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <iostream>
class VvParser;

//#include "Utility.hpp"

/**
 * The VvParser class reads voi files
 *
 * Reads and writes volumes of interest stored in vv format.
 * <p>
 * Here's the vv file format:<br>
 * Header:<br>
 * Magic string: "vv-opt4D\n" (includes \n to check for improper file 
 * transmission.)
 * Endian-ness marker "ab" or "ba", equal to 25,185 written out as a short.
 * Number of vois in ascii: "00001VOIs\n" for example (always 5 characters)
 * newline terminated list of names of each structure, max 80 characters per 
 * name.
 * 
 * Everything else is not human readable
 *
 * for each VOI:
 * int number of voxels
 * int32, uint8 pairs where the uint8 is used to say how many of the following 
 * voxels are included
 * There are almost always fewer pairs than voxels, but the sum of the number 
 * of pairs plus the sum of the uint8s must equal the number of voxels
 * <p>
 * These voi indexes are in column, slice, row order.  For
 * example, a 3x4x2 dose cube in CERR would be numbered in this order:
 * <pre>
 *  slice 0: (toward feet)
 *       front       r
 * l  [ 0  1  2  3]  i
 * e  [ 8  9 10 11]  h
 * f  [16 17 18 19]  h
 * t     back        t
 * </pre><pre>
 *  slice 1: (toward head)
 *       front       r
 * l  [ 4  5  6  7]  i
 * e  [12 13 14 15]  h
 * f  [20 21 22 23]  h
 * t     back        t
 * </pre>
 * Note that left corresponds to patient right and vice versa.  Returns the 
 * number of voxels in the file.
 */
class VvParser
{
    public:

        // Constructor (Read the file)
        VvParser(
                const string & file_name
                );

        // Construct from a vector of vectors of indices
        VvParser(const vector<std::pair<string,vector<unsigned int> > > & vois);

        // Write a vv file
        void write_file(
                const string & file_name
                ) const;

        // write out as voi file
        void write_voi_file(const string & file_name,
			    const vector<unsigned int> & voilist,
			    const unsigned int nVoxels) const;

        // Get a the indices in a VOI
        const std::pair<string,vector<unsigned int> > & operator[](
                const size_t voiNo) const {
            return vois_[voiNo];
        };

        // Get the number of vois present
        unsigned int size() const {
            return vois_.size();
        };

        bool operator ==(const VvParser & rhs) const;

    private:
        /// Default constructor not allowed
        VvParser();

        struct header {
            char file_type_name[8];     // "vv-opt4D"
            short endian_test_pattern;  // 25185 = "ab" or \x6261 on little
                                        // endian systems
            char newline_character;     // "\n"
            char nVois_text[6];         // "#VOIs:"
        };

        static const header reference_header_;
        static const int reference_header_size_ = 17;

        vector<std::pair<string,vector<unsigned int> > > vois_;
};


#endif
