/**
 * @file ColumnGenerationDaoOptimizer.cpp
 * ColumnGenerationDaoOptimizer Class implementation.
 *
 */

#include "ColumnGenerationDaoOptimizer.hpp"
#include "GradientOptimizer.hpp"


/**
 * Constructor
 */
ColumnGenerationDaoOptimizer::ColumnGenerationDaoOptimizer(Plan* thisplan, string log_file_name, Optimization::options options) 
  : Optimization(thisplan,log_file_name,options),
	aperture_weights_()
{
  cout << "ColumnGenerationDaoOptimizer constructor called" << endl;

  // make sure we have a DaoVector
  if ( the_plan_->get_parameter_vector()->getVectorType() != DAO_VECTOR_TYPE ) {
	  throw std::logic_error("Can only use column_generation_dao with a DaoVector");
  }

  // we can cast to the derived class as this optimizer will only work with a Dao Vector
  dao_vector_ = static_cast<DaoVector *>(the_plan_->get_pointer_to_parameter_vector());

  // set aperture vector
  set_aperture_weights();

  // create optimizer for the aperture weight optimization
  string log_file_master = options.out_file_prefix_ + "history_master.xml";
  cout << "master output " << log_file_master << endl;
  master_problem_optimizer_.reset(new GradientOptimizer(the_plan_, dao_vector_->get_aperture_dij(), 
														aperture_weights_.get(), log_file_master, options));
}

/**
 * Constructor
 */
ColumnGenerationDaoOptimizer::~ColumnGenerationDaoOptimizer() 
{
  cout << endl << "calling destructor of ColumnGenerationDaoOptimizer" << endl << endl;
}


/**
 * initialize
 */
void ColumnGenerationDaoOptimizer::initialize()
{
}


/**
 * Direct aperture optimization through column generation approach
 */
void ColumnGenerationDaoOptimizer::optimize() 
{
  cout << "starting to generate apertures ..." << endl;

  // Restart the timer
  timer_.restart();

  log_.new_optimization(0);

  // create bixel gradient
  BixelVectorDirection bixel_gradient = BixelVectorDirection(dao_vector_->get_nBixels());

  // True if we are finished
  bool stopped = false;
  unsigned int iterations = 0;  

  while(!stopped)
	{
	  // increment interation counter
	  iterations++;

	  // print message
	  cout << "creating aperture No: " << iterations << endl; 

	  // Record step start time
	  float step_start_time = timer_.get_time();

	  // calculate the bixel gradient
	  double objective = the_plan_->calculate_objective_and_gradient(*(the_plan_->get_dose_influence_matrix()), 
																	 *(dao_vector_->get_bixel_vector()),
																	 *(dynamic_cast<ParameterVectorDirection*>(&bixel_gradient)), 
																	 multi_objective_, 
																	 last_ssvo_, 
																	 false, false,
																	 options_.nScenario_samples_per_step_ );

	  cout << "objective: " << objective << endl;


	  // add most promising aperture 
	  dao_vector_->add_best_aperture(bixel_gradient);

	  // optimize aperture weights
	  if(dao_vector_->get_nApertures() > 0)
		{
		  /// get aperture weight vector
		  set_aperture_weights();
		  //aperture_weights_->set_unit_intensities();

		  // create optimizer for the aperture weight optimization
		  master_problem_optimizer_->reset(aperture_weights_.get());

		  // optimize aperture weights
		  master_problem_optimizer_->optimize();
		}

	  // synchronize dao vector
	  dao_vector_->synchronize(aperture_weights_.get());

	  // count nonzero aperture weights
	  float mean_weight = aperture_weights_->get_mean_parameter_value();
	  unsigned int nNonzeros = aperture_weights_->get_nNonzero_parameters(0.01*mean_weight);
	  log_.add_step_attribute("used_apertures", nNonzeros);

	  // recalculate objective
	  objective = the_plan_->calculate_objective( *(the_plan_->get_dose_influence_matrix()), 
												  *(dao_vector_->get_bixel_vector()),
												  multi_objective_, 
												  last_ssvo_, 
												  false, false,
												  options_.nScenario_samples_per_step_ );
	  
	  // store
	  log_.add_step_attribute("obj", objective);
	  log_.add_step_attribute("multi_obj", multi_objective_);

      // Finish off log for step
      log_.finish_step(step_start_time);

	  // check stopping criteria
	  if(iterations >= options_.max_apertures_) {
	  	stopped = true;
	  }
	}

  // manually delete master_problem_optimizer_ to write the log file - > this is a hack
  master_problem_optimizer_.reset();
}


/**
 * get aperture weights from the doa vector
 */
void ColumnGenerationDaoOptimizer::set_aperture_weights() 
{
  if(options_.use_variable_transformation_) {
	aperture_weights_ = dao_vector_->get_aperture_SquareRootBixelVector();
  }
  else {
	aperture_weights_ = dao_vector_->get_aperture_BixelVector();
  }
}
