/**
 * @file gEudObjective.hpp
 * gEudObjective Class header
 *
 */

#ifndef GEUDOBJECTIVE_HPP
#define GEUDOBJECTIVE_HPP

#include "NonSeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class for generalized uniform dose objective function
 */
class GEudObjective : public NonSeparableObjective
{

    public:

        // Constructor
        GEudObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement,
                bool  is_maximized);

        // Virtual destructor
        virtual ~GEudObjective();


        //
	// Functions reimplemented from NonSeparableObjective
        //
        virtual double calculate_objective(
                const vector<float> & dose) const;
        virtual double calculate_objective_and_d(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose) const;
        virtual double calculate_objective_and_d2(
                const vector<float> & dose,
	        vector<float> & d_obj_by_dose,
	        vector<float> & d2_obj_by_dose) const;

        //
        // Functions reimplemented from Objective
        //

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;


    private:
        // Default constructor not allowed
        GEudObjective();

        /// weight of objective
        float weight_;

        /// True if the gEUD is maximized (for targets)
        bool is_maximized_;

        /// exponent in gEUD formular
        float EUD_p_;

        /// small number
        float epsilon_;
};


#endif
