/**
 * @file KonradSquareRootBixelVector.cpp
 * KonradSquareRootBixelVector class implementation
 *
 */

#include "KonradSquareRootBixelVector.hpp"
#include "KonradBixelVector.hpp"
#include <typeinfo>

/**
 * Constructor: Read bixel vector from .bwf and (optionally) .stf files.
 */
KonradSquareRootBixelVector::KonradSquareRootBixelVector( Geometry *geometry,
							  const unsigned int nInstances,
							  const unsigned int nBeams_per_instance,
							  const float min_weight,
							  const string bwf_file_root,
							  const string stf_file_root )
  : SquareRootBixelVector(min_weight)
{
  // first construct the corresponding bixel vector
  bixel_vector_.reset(new KonradBixelVector(geometry,nInstances,nBeams_per_instance,bwf_file_root,stf_file_root));

  // synchronize the parameter vector with the bixel vector
  initialize();
  synchronize_bixel_vector();
}


/**
 * Constructor: construct bixel vector based on Dij matrix
 */
KonradSquareRootBixelVector::KonradSquareRootBixelVector( const DoseInfluenceMatrix & Dij,
							  Geometry *geometry,
							  const unsigned int nInstances,
							  const unsigned int nBeams_per_instance,
							  const float min_weight )
  : SquareRootBixelVector(min_weight)
{
  // first construct the corresponding bixel vector
  bixel_vector_.reset(new KonradBixelVector(Dij,geometry,nInstances,nBeams_per_instance));
  //cout << "type: " << typeid(*bixel_vector_).name() << endl;

  // synchronize the parameter vector with the bixel vector
  initialize();

  // set unit intensities
  set_unit_intensities();
  synchronize_bixel_vector();
  //cout << "type: " << typeid(*bixel_vector_).name() << endl;
}

/**
 * Copy Constructor
 */
KonradSquareRootBixelVector::KonradSquareRootBixelVector( const KonradSquareRootBixelVector &rhs )
  : SquareRootBixelVector(rhs)
{
}


/**
 * Get a pointer to a copy of the object.
 *
 * Careful: the calling function is responsible for deleting the returned object from memory.
 */
std::auto_ptr<ParameterVector> KonradSquareRootBixelVector::get_copy() const
{
  //  cout << "KonradSquareRootBixelVector::get_copy()" << endl;
  //  cout << "type: " << typeid(*bixel_vector_).name() << endl;

  std::auto_ptr<ParameterVector> temp( new KonradSquareRootBixelVector( *this ) );

  //  cout << "type old: " << typeid(*bixel_vector_).name() << endl;
  //cout << "type copy: " << typeid(*temp).name() << temp->get_value(50) << endl;
  //cout << "type this: " << typeid(*this).name() << value_[50] << endl;

  return temp;
}


//
// Read and write files
//

void KonradSquareRootBixelVector::load_bwf_files(const string & bwf_file_root)
{
  bixel_vector_->load_bwf_files(bwf_file_root);
  initialize();
}

void KonradSquareRootBixelVector::load_bwf_file(const string & bwf_file_name, unsigned int beamNo)
{
  bixel_vector_->load_bwf_file(bwf_file_name, beamNo);
}

void KonradSquareRootBixelVector::write_bwf_files(const string & bwf_file_root) const
{
  //cout << "type: " << typeid(bixel_vector_.get()).name() << endl;
  //cout << "type: " << typeid(*bixel_vector_).name() << endl;
  (*bixel_vector_).write_bwf_files(bwf_file_root);
}

void KonradSquareRootBixelVector::write_bwf_file(const string & bwf_file_name, const unsigned int beamNo) const
{
  (*bixel_vector_).write_bwf_file(bwf_file_name, beamNo);
}

void KonradSquareRootBixelVector::load_bixels_in_aperture(const string & stf_file_root)
{
  bixel_vector_->load_bixels_in_aperture(stf_file_root);
  initialize();
}


