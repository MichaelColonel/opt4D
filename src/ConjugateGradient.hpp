
/**
 * @file ConjugateGradient.hpp
 * ConjugateGradient Class Header
 *
 * $Id: ConjugateGradient.hpp,v 1.1 2007/10/10 00:35:16 bmartin Exp $
 */

#ifndef CONJUGATEGRADIENT_HPP
#define CONJUGATEGRADIENT_HPP

class ConjugateGradient;

#include <iostream>
#include <stdexcept>
#include "Optimizer.hpp"
#include "BixelVectorDirection.hpp"



/**
 * Class ConjugateGradient is an optimizer object to implement a conjugate
 * gradient descent algorithm.  Must be used with optimal step sizes for
 * correct results.
 * <p>
 * The overall algorithm works like this:
 * <ol>
 *   <li> Initialization:
 *   <ol>
 *     <li> The initial direction is the negative of the gradient
 *     <li> Store the gradient and direction for future use
 *   </ol>
 *   <li> Later steps:
 *   <ol>
 *     <li> Calculate beta = (gradient (dot) (gradient - last gradient)) /
 *     (last gradient (dot) last gradient)
 *     <li> Calculate the direction = last direction * beta - gradient
 *     <li> Store the gradient and direction for future use
 *   </ol>
 * </ol>
 *
 * Only needs the gradient at each step to find the next direction.
 */
class ConjugateGradient : public Optimizer
{

    public:
	class options;

	ConjugateGradient(
		unsigned int nParams
		);


	virtual ~ConjugateGradient() {};

        /// Calculate the direction based on just the gradient
        void calculate_direction(const ParameterVectorDirection & gradient,
				 const ParameterVector & param);

        /// Print out options (no options for CG class)
	virtual void print_options(std::ostream& o) const {};

        //
        // Conjugate gradient specific data
        //

        /// The previous gradient
        ParameterVectorDirection last_gradient_;

        /// The previous direction
        ParameterVectorDirection last_direction_;

        bool is_initialized_;
};

#endif

