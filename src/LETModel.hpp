/**
 * The model evaluates an objective or constraint for LET time dose
 *
 */

class LETModel;


#include "DoseDeliveryModel.hpp"


class LETModel : public DoseDeliveryModel
{

  public:
    /// constructor
    LETModel(const Geometry *the_geometry,
	     const DoseDeliveryModel::options *options,
	     const string dij_file_root,
	     const string stf_file_root);

    // Virtual destructor
    virtual ~LETModel();

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("LETModel"); }

    /// generate a random sample scenario
    virtual void generate_random_scenario(RandomScenario &random_scenario) const;

    /// generate the nominal scenario
    virtual void generate_nominal_scenario(
            RandomScenario &random_scenario) const;

    /// calculate LET times dose
    virtual float calculate_voxel_dose(unsigned int voxelNo,
				       const BixelVector &bixel_grid,
				       DoseInfluenceMatrix &dij,
				       const RandomScenario &random_scenario) const;

    /// calculate gradient contribution of the voxel for a scenario
    virtual void calculate_voxel_dose_gradient(
            unsigned int voxelNo,
 	    const BixelVector &bixel_grid,
            DoseInfluenceMatrix &dij,
            const RandomScenario &random_scenario,
            BixelVectorDirection &gradient,
            float gradient_multiplier) const;

    /// write results
    virtual void write_dose(BixelVector &bixel_grid,
			    DoseInfluenceMatrix &dij,
			    string file_prefix,
			    Dvh dvh) const;

 protected:

    /// The dose influence matrix containing LET times dose
    std::auto_ptr<DoseInfluenceMatrix> LETxDij_;

    /// The geometry for the LET input data
    std::auto_ptr<Geometry> LET_geometry_;

    /// The bixel vector for the LET input data
    std::auto_ptr<BixelVector> LET_bixels_;

    /// LETxD scale factor
    float LETxD_scalefactor_;

    /// physical dose scale factor
    float physD_scalefactor_;
};

