/**
 * @file: NonSeparableObjective.cpp
 * NonSeparableObjective Class implementation.
 *
 * $Id: NonSeparableObjective.cpp,v 1.4 2008/03/24 17:05:00 bmartin Exp $
 */

#include "NonSeparableObjective.hpp"

/**
 * Constructor: Initialize variables.
 */
NonSeparableObjective::NonSeparableObjective(
        Voi*  the_voi,
        unsigned int objNo,
        float weight,
        float sampling_fraction,
        bool sample_with_replacement
        )
: Objective(objNo, weight),
    the_voi_(the_voi),
    sampling_fraction_(sampling_fraction),
    sample_with_replacement_(sample_with_replacement),
    temp_voxelNos_(),
    temp_dose_(),
    temp_d_obj_by_dose_(),
    temp_d2_obj_by_dose_()
{
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
NonSeparableObjective::~NonSeparableObjective()
{
}


/**
 * Initialize the objective.
 * Does nothing for NonSeparableObjective except set is_initialized_ to true.
 */
void NonSeparableObjective::initialize(
    DoseInfluenceMatrix& Dij)
{
  // sampling_fraction_ = the_voi_->get_sampling_fraction();
  is_initialized_ = true;
}


/**
 * Calculate objective based on the supplied dose distribution
 *
 * @param the_dose The dose distribution to use
 */
double NonSeparableObjective::calculate_dose_objective(
    const DoseVector & the_dose)
{
    assert(is_initialized_);

    
    size_t nVoxels = the_voi_->get_nVoxels();

    // Copy dose into vector
    temp_dose_.resize(nVoxels);
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
        temp_dose_[iVoxel] = the_dose.get_voxel_dose(
                the_voi_->get_voxel(iVoxel));
    }

    // Calculate objective in derived class
    return calculate_objective(temp_dose_);
}


/**
 * Calculate dose and store the results in temp_dose_
 *
 * @param voxelNos A vector of voxel numbers to use
 */
void NonSeparableObjective::calculate_temp_dose(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        const vector<unsigned int>& voxelNos)
{
    // Resize dose vector
    temp_dose_.resize(voxelNos.size());

    // Calculate each voxel dose and store
    vector<unsigned int>::const_iterator voxelNo_iter = voxelNos.begin();
    vector<float>::iterator dose_iter = temp_dose_.begin();
    while(voxelNo_iter != voxelNos.end()) {
        *dose_iter = Dij.calculate_voxel_dose(*voxelNo_iter, beam_weights);

        ++dose_iter;
        ++voxelNo_iter;
    }
}


/**
 * Calculate dose for the given scenario and store the results in temp_dose_
 *
 * @param voxelNos A vector of voxel numbers to use
 */
void NonSeparableObjective::calculate_scenario_temp_dose(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        const DoseDeliveryModel & dose_model,
        const RandomScenario & random_scenario,
        const vector<unsigned int>& voxelNos)
{
    // Resize dose vector
    temp_dose_.resize(voxelNos.size());

    // Calculate each voxel dose and store
    vector<unsigned int>::const_iterator voxelNo_iter = voxelNos.begin();
    vector<float>::iterator dose_iter = temp_dose_.begin();
    while(voxelNo_iter != voxelNos.end()) {
        // Calculate dose to voxel
        *dose_iter = dose_model.calculate_voxel_dose(
                *voxelNo_iter, 
                beam_weights,
                dij,
                random_scenario); 
        ++dose_iter;
        ++voxelNo_iter;
    }
}


/**
 * Calculate or Estimate objective from BixelVector and DoseInfluenceMatrix
 */
double NonSeparableObjective::calculate_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    // Estimated ssvo is not supported for nonseparable objectives
    estimated_ssvo = 0.0;

    // Find voxel list to use
    const vector<unsigned int>* voxel_list;
    if(use_voxel_sampling) {
        // Sample voxels
        the_voi_->sample_voxels(
                temp_voxelNos_,
                sampling_fraction_,
                sample_with_replacement_);

        // Set voxel list pointer
        voxel_list = &temp_voxelNos_;
    } else {
        // Don't use voxel sampling

        // Set voxel list pointer to list in Voi
        voxel_list = &the_voi_->get_voxelNos();
    }

    // Calculate dose to sampled voxels
    calculate_temp_dose(beam_weights, Dij, *voxel_list);

    // Calculate objective
    return calculate_objective(temp_dose_);
}


/**
 * Calculate or Estimate the objective and the gradient
 */
double NonSeparableObjective::calculate_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    // Estimated ssvo is not supported for nonseparable objectives
    estimated_ssvo = 0.0;

    // Find voxel list to use
    const vector<unsigned int>* voxel_list;
    if(use_voxel_sampling) {
        // Sample voxels
        the_voi_->sample_voxels(
                temp_voxelNos_,
                sampling_fraction_,
                sample_with_replacement_);

        // Set voxel list pointer
        voxel_list = &temp_voxelNos_;
    } else {
        // Don't use voxel sampling

        // Set voxel list pointer to list in Voi
        voxel_list = &the_voi_->get_voxelNos();
    }

    // Calculate dose to voxels
    calculate_temp_dose(beam_weights, Dij, *voxel_list);

    // Calculate objective and the gradient with respect to the dose in
    // each voxel
    double objective = calculate_objective_and_d(
            temp_dose_, temp_d_obj_by_dose_);

    // Calculate contribution to gradient
    vector<unsigned int>::const_iterator v_iter = voxel_list->begin();
    vector<float>::const_iterator d_iter = temp_d_obj_by_dose_.begin();
    while(v_iter != voxel_list->end()) {
        if(*d_iter) {
            // This voxel constributes to the gradient
            Dij.calculate_voxel_dose_gradient(
                    *v_iter,
                    gradient,
                    *d_iter * gradient_multiplier);
        }
        ++v_iter;
        ++d_iter;
    }

    return objective;
}




/**
 * Calculate or estimate the objective for one instance of an uncertain 
 * treatment.
 */
double NonSeparableObjective::calculate_scenario_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        const DoseDeliveryModel & dose_model,
        const RandomScenario & random_scenario,
	bool use_voxel_sampling,
        double &estimated_ssvo)
{
    // Estimated ssvo is not supported for nonseparable objectives
    estimated_ssvo = 0.0;


    // Find voxel list to use
    const vector<unsigned int>* voxel_list;
    if(use_voxel_sampling) {
        // Sample voxels
        the_voi_->sample_voxels(
                temp_voxelNos_,
                sampling_fraction_,
                sample_with_replacement_);

        // Set voxel list pointer
        voxel_list = &temp_voxelNos_;
    } else {
        // Don't use voxel sampling

        // Set voxel list pointer to list in Voi
        voxel_list = &the_voi_->get_voxelNos();
    }

    // Calculate dose to voxels
    calculate_scenario_temp_dose(beam_weights,
            dij, dose_model, random_scenario, *voxel_list);

    // Calculate objective
    return calculate_objective(temp_dose_);
}


/**
 * Calculate or estimate the objective for one instance of an uncertain 
 * treatment.
 */
double NonSeparableObjective::calculate_scenario_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & dij,
        BixelVectorDirection & gradient, 
        float gradient_multiplier,
        const DoseDeliveryModel & dose_model,
        const RandomScenario & random_scenario,
	bool use_voxel_sampling,
        double &estimated_ssvo)
{
    // Estimated ssvo is not supported for nonseparable objectives
    estimated_ssvo = 0.0;


    // Find voxel list to use
    const vector<unsigned int>* voxel_list;
    if(use_voxel_sampling) {
        // Sample voxels
        the_voi_->sample_voxels(
                temp_voxelNos_,
                sampling_fraction_,
                sample_with_replacement_);

        // Set voxel list pointer
        voxel_list = &temp_voxelNos_;
    } else {
        // Don't use voxel sampling

        // Set voxel list pointer to list in Voi
        voxel_list = &(the_voi_->get_voxelNos());
    }

    // Calculate dose to voxels
    calculate_scenario_temp_dose( beam_weights,
            dij, dose_model, random_scenario, *voxel_list);

    // Calculate objective
    double objective = calculate_objective_and_d(temp_dose_,
            temp_d_obj_by_dose_);

    // Calculate contribution to gradient
    vector<unsigned int>::const_iterator v_iter = voxel_list->begin();
    vector<float>::const_iterator d_iter = temp_d_obj_by_dose_.begin();
    while(v_iter != voxel_list->end()) {
        if(*d_iter) {
            // This voxel constributes to the gradient
            dose_model.calculate_voxel_dose_gradient(
                    *v_iter,
		    beam_weights,
                    dij,
                    random_scenario,
                    gradient,
                    *d_iter * gradient_multiplier);
        }
        ++v_iter;
        ++d_iter;
    }

    return objective;
}


/// Find out how many voxels were in objective
unsigned int NonSeparableObjective::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}

/// Find out voxel sampling rate
float NonSeparableObjective::get_sampling_fraction() const
{
  return sampling_fraction_;
}

/// Set voxel sampling rate
void NonSeparableObjective::set_sampling_fraction(float sampling_fraction)
{
  sampling_fraction_= sampling_fraction;
}

