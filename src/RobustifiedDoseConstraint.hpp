/**
 * @file RobustifiedDoseConstraint.hpp
 * RobustifiedDoseConstraint Class header
 *
 */

#ifndef ROBUSTIFIEDDOSECONSTRAINT_HPP
#define ROBUSTIFIEDDOSECONSTRAINT_HPP

#include "Constraint.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class RobustifiedDoseConstraint implements a hard maximum or minimum dose constraint 
 * for all voxels in an ROI and all scenarios.

 */
class RobustifiedDoseConstraint : public Constraint
{

public:
  
  // Constructor
  RobustifiedDoseConstraint(Voi*  the_voi,
			    unsigned int consNo,
			    bool is_max_constraint,
			    bool is_min_constraint,
			    float max_dose,
			    float min_dose,
			    const DoseDeliveryModel &uncertainty);
  
  // Virtual destructor
  ~RobustifiedDoseConstraint();
  
  
  /// Find out how many voxels are in this constraint
  unsigned int get_nVoxels() const;
  
  /// Find out how many scenarios the uncertainty model has
  unsigned int get_nScenarios() const;

  /// get corresponding VOI
  size_t get_voiNo() const;
  
  /// Print a description of the constraint
  void printOn(std::ostream& o) const;

  /// set upper bound of a constraint
  void set_upper_bound(float value) {max_dose_ = value;};

  /// set upper bound of a constraint
  float get_upper_bound() const {return max_dose_;};

  /// set lower bound of a constraint
  void set_lower_bound(float value) {min_dose_ = value;};

  /// set lower bound of a constraint
  float get_lower_bound() const {return min_dose_;};

  /// initialize the Constraint
  void initialize(DoseInfluenceMatrix& Dij);

  /// if voxel is in active set
  bool is_active(unsigned int iScenario, unsigned int iVoxel) const;

  
  //
  // specific functions for augmented lagrangian approach not implemented
  //
  

  //
  // specific functions for projection method
  //
  
  bool supports_projection_solver() const {return(true);};
  bool supports_projection_optimizer() const {return(true);};

  /// sequentially project onto (beyond) the constraints
  unsigned int project_onto(BixelVector & beam_weights,
			    DoseInfluenceMatrix & Dij);

  /// puts all voxels into the active set
  void reset_active_set();

  /// ckeck if active set is empty
  bool is_active_set_empty() const;

  //
  // specific functions for external solver interface not implemented
  //
  


private:

  // Default constructor not allowed
  RobustifiedDoseConstraint();

  /// the uncertainty model
  const DoseDeliveryModel *the_UncertaintyModel_;

  /// the corresponding VOI
  Voi *the_voi_;

  /// is max dose constraint
  bool is_max_constraint_;

  /// is min dose constraint
  bool is_min_constraint_;

  /// maximum tolerance dose
  float max_dose_;

  /// minimum tolerance dose
  float min_dose_;

  /// defines the active set of constraints for robustified version
  vector< vector<bool> > in_active_set_;

  /// norm of Dij rows for every scenario
  vector< vector<float> > dose_contribution_norm_;

};

/*
 * Inline functions
 */


inline
size_t RobustifiedDoseConstraint::get_voiNo() const
{
  return the_voi_->get_voiNo();
}

inline
unsigned int RobustifiedDoseConstraint::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}

inline
unsigned int RobustifiedDoseConstraint::get_nScenarios() const
{
  return the_UncertaintyModel_->get_nDose_eval_scenarios();
}

inline
bool RobustifiedDoseConstraint::is_active(unsigned int iScenario, unsigned int iVoxel) const
{
  return in_active_set_[iScenario][iVoxel];
}

inline
void RobustifiedDoseConstraint::reset_active_set()
{
  for(unsigned int i=0;i<get_nScenarios();i++) {
    fill(in_active_set_[i].begin(),in_active_set_[i].end(),true);
  }
}

inline
bool RobustifiedDoseConstraint::is_active_set_empty() const
{
  for(unsigned int i=0;i<get_nScenarios();i++) {
    for(unsigned int j=0; j<in_active_set_.size(); j++) {
      if(in_active_set_[i][j]) {
	return false;
      }
    }
  }
  return true;
}



#endif
