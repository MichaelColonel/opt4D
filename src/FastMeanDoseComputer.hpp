/**
 * @file FastMeanDoseComputer.hpp
 * FastMeanDoseComputer Class header
 *
 */

#ifndef FASTMEANDOSECOMPUTER_HPP
#define FASTMEANDOSECOMPUTER_HPP

#include "DoseInfluenceMatrix.hpp"
#include "Voi.hpp"

/**
 * Class FastMeanDoseComputer implements methods to calculate the mean dose to a Voi fast
 * by precalculating bixel contributions to the mean dose
 */
class FastMeanDoseComputer
{

public:

  /// Constructor
  FastMeanDoseComputer();

  /// Destructor
  ~FastMeanDoseComputer();

  /// initialization: precalculates mean dose contributions to a Voi
  void initialize(Voi* the_voi, DoseInfluenceMatrix& Dij);

  /// Calculate the mean dose from a vector of beam weights
  float calculate_mean_dose(const BixelVector & beam_weights) const;

  /// precalculate contributions to the mean dose
  vector<float> mean_dose_contribution_;

  /// the norm of the vector containing the mean dose contributions
  float norm_;

  /// if it has been initialized  
  bool is_initialized_;
};




#endif
