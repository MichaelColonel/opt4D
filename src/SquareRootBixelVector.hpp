/**
 * @file SquareRootBixelVector.hpp
 * SquareRootBixelVector Class Definition.
 */

#ifndef SQUAREROOTBIXELVECTOR_HPP
#define SQUAREROOTBIXELVECTOR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>
#include <stdexcept>
#include <exception>
#include <limits>
using std::numeric_limits;

// Predefine class before including others
class SquareRootBixelVector;

#include "ParameterVector.hpp"
#include "BixelVector.hpp"
#include "BixelVectorDirection.hpp"
#include "ParameterVectorDirection.hpp"


/**
 * Class SquareRootBixelVector handles handles a ParameterVector in which the parameters are
 * the square roots of bixel intensities
 */
class SquareRootBixelVector : public ParameterVector {

public:

  /// constructor
  SquareRootBixelVector(const float min_weight, const unsigned int &nJ=0, const bool &set_aperture=true );

  /// Copy constructor
  SquareRootBixelVector( const SquareRootBixelVector &rhs);

  /// Get a pointer to a copy of the object.
  virtual std::auto_ptr<ParameterVector> get_copy() const;

  /// Translate the ParameterVector into a BixelVector
  BixelVector* translateToBixelVector() const;

  /// translate gradient in terms of bixels to parameter gradient
  ParameterVectorDirection translateBixelGradient( BixelVectorDirection & bvd ) const;

  /// Translate the Bixel Vector to a Parameter Vector
  void translateBixelVector( BixelVector & bv );

  //
  // reading/writing parameter files
  //

  // Replace inherritted virtual functions with 'bwf' equivalents.
  inline void load_parameter_files( string parameters_file_root ) {
    this->load_bwf_files( parameters_file_root );
  };

  inline void load_parameter_file( string parameters_file_name, unsigned int beamNo ) {
    this->load_bwf_file( parameters_file_name, beamNo );
  };

  inline void write_parameter_files( string parameters_file_root ) const {
    synchronize_bixel_vector();
    write_bwf_files( parameters_file_root );
  };

  inline void write_parameter_file( string parameters_file_name, const unsigned int beamNo ) const {
    synchronize_bixel_vector();
    write_bwf_file( parameters_file_name, beamNo );
  };

  // read/write bwf files (implemented in derived class)
  virtual void load_bwf_files( const string & bwf_file_root );
  virtual void load_bwf_file( const string & bwf_file_name, unsigned int beamNo );
  virtual void write_bwf_files( const string & bwf_file_root ) const;
  virtual void write_bwf_file( const string & bwf_file_name, const unsigned int beamNo ) const;
  virtual void load_bixels_in_aperture( const string & stf_file_root );

  /// get number of bixels
  unsigned int get_nBixels() const {return this->get_nParameters();};
  unsigned int get_beamNo( const int &ip ) const;

  /// Set the value of all bixels to one.
  void set_unit_intensities();
  void set_zero_intensities();
  void scale_intensities( float scale_factor );

  /// add parameter
  void push_back_parameter( const float &value, const float &lowBound, const float &highBound,
							const bool &isActive, const float &step_size );

  /**
   * Get the current type of parameter vector
   *
   * @return the parameter vector type enum indicating that this is a bixel vector
   */
  virtual inline ParameterVectorType getVectorType() const {
    return SQRRT_BIXEL_VECTOR_TYPE;
  }

protected:

  /// synchronize the internal bixelvector with the current parameters
  void synchronize_bixel_vector() const;

  /// initialize the parameters with the internal bixelvector
  void initialize();

  /// the corresponding bixel vector
  mutable std::auto_ptr<BixelVector> bixel_vector_;

  /// minimum bixel weight
  float min_weight_;

};


/**
 * Set the value of all bixels to one.
 */
inline void SquareRootBixelVector::set_unit_intensities()
{
  for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
    set_value(pi, 1.0f);
  }
  synchronize_bixel_vector();
}

/**
 * Set the value of all bixels to zero.
 */
inline void SquareRootBixelVector::set_zero_intensities()
{
  for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
    set_value(pi, 0.0f);
  }
  synchronize_bixel_vector();
}

/**
 * Scale the value of all bixels.
 */
inline void SquareRootBixelVector::scale_intensities( float scale_factor )
{
  assert(scale_factor >= 0);
  for ( unsigned int pi = 0; pi < this->get_nParameters(); pi++ ) {
    value_[pi] *= scale_factor;
  }
  synchronize_bixel_vector();
}




#endif
