/**
 * @file LQUncertaintyModel.hpp
 * LQUncertaintyModel Class header
 *
 */

#ifndef LQUNCERTAINTYMODEL_HPP
#define LQUNCERTAINTYMODEL_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class to specify uncertainty in the LQ model parameters
 */
class LQUncertaintyModel
{

public:

  /// Constructor
  LQUncertaintyModel(float alpha,
					 float betaalpha,
					 double maximum_cell_density,
					 string density_file_name,
					 unsigned int nVoxels);


  /// returns alpha value, returns nominal value if not overloaded 
  virtual float get_voxel_alpha(unsigned int iScenario,unsigned int voxelNo);

  /// returns initial cell density, returns nominal value if not overloaded
  virtual double get_voxel_cell_density(unsigned int iScenario,unsigned int voxelNo);

  /// returns number of scenarios, returns 1 if not overloaded
  unsigned int get_nScenarios() {return nScenarios_;};

  /// returns alpha/beta ratio, returns nominal value if not overloaded
  virtual float get_voxel_betaalpha(unsigned int iScenario,unsigned int voxelNo);

  /// get probability of scenario
  virtual float get_scenario_probability(unsigned int iScenario) {return 1;};

protected:

  /// Constructor
  LQUncertaintyModel();

  /// nominal value for alpha
  float nominal_alpha_;

  /// nominal value for the alpha/beta ratio
  float nominal_betaalpha_;

  /// nominal tumor cell distribution
  DoseVector nominal_log_cell_density_;

  /// maximum cell density, carrying capacity
  double maximum_cell_density_;

  /// number of scenarios
  unsigned int nScenarios_;

};

inline float LQUncertaintyModel::get_voxel_alpha(unsigned int iScenario,unsigned int voxelNo)
{
  return nominal_alpha_;
}

inline float LQUncertaintyModel::get_voxel_betaalpha(unsigned int iScenario,unsigned int voxelNo)
{
  return nominal_betaalpha_;
}

inline double LQUncertaintyModel::get_voxel_cell_density(unsigned int iScenario,unsigned int voxelNo)
{
  return( maximum_cell_density_ * exp((double)nominal_log_cell_density_.get_voxel_dose(voxelNo)) );
}



/**
 * Class to implement nominal LQ model, no uncertainty
 */
class LQModelNominal : public LQUncertaintyModel
{

public:

  /// Constructor
  LQModelNominal(float alpha,
				 float betaalpha,
				 double maximum_cell_density,
				 string density_file_name,
				 unsigned int nVoxels);

private:

  LQModelNominal();

};


/**
 * Class to implement LQ model with alpha uncertainty
 */
class LQModelAlphaUncertainty : public LQUncertaintyModel
{

public:

  /// Constructor
  LQModelAlphaUncertainty(float alpha_min,
						  float alpha_max,
						  unsigned int nAlpha_values,
						  float betaalpha,
						  double maximum_cell_density,
						  string density_file_name,
						  unsigned int nVoxels);

  /// returns alpha value for this voxel and scenario
  float get_voxel_alpha(unsigned int iScenario,unsigned int voxelNo);

  /// return probability of a scenario
  float get_scenario_probability(unsigned int iScenario);

private:

  LQModelAlphaUncertainty();

  /// minimum alpha value
  float alpha_min_;

  /// maximum alpha value
  float alpha_max_;

};



#endif



