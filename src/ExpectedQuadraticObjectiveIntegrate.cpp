/**
 * @file ExpectedQuadraticObjectiveIntegrate.cpp
 * ExpectedQuadraticObjectiveIntegrate class header
 *
 */

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "DoseVector.hpp"
#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "ExpectedQuadraticObjectiveIntegrate.hpp"



/**
 * constructor 
 *
 */
ExpectedQuadraticObjectiveIntegrate::ExpectedQuadraticObjectiveIntegrate(
        Voi *the_voi,
        unsigned int objNo,
        DoseDeliveryModel &Uncertainty,
        float desired_dose,
        float weight,
        float sampling_fraction
        )
    :Objective(objNo, weight),
    the_voi_(the_voi),
    the_UncertaintyModel_(&Uncertainty),
    desired_dose_(desired_dose),
    sampling_fraction_(sampling_fraction)
{
    cout << "Constructing ExpectedQuadraticObjectiveIntegrate for Voi "
        << the_voi_->get_name() << endl;

    // make sure the uncertainty model can calculate the variance
    if(!the_UncertaintyModel_->supports_calculate_voxel_variance()) {
	cout << the_UncertaintyModel_->get_uncertainty_model() 
	     << " does not support calculation of the dose variance" << endl;
	throw(std::runtime_error("Cannot use ExpectedQuadraticObjectiveIntegrate"));
    }
    // make sure quadratic objective is supported
    if(!the_UncertaintyModel_->supports_calculate_quadratic_objective_integrate()) {
	cout << the_UncertaintyModel_->get_uncertainty_model() 
	     << " does not support the quadratic objective" << endl;
	throw(std::runtime_error("Cannot use ExpectedQuadraticObjectiveIntegrate"));
    }
}

// destructor
ExpectedQuadraticObjectiveIntegrate::~ExpectedQuadraticObjectiveIntegrate()
{
}

/**
 * Initialize the sampling
 */
void ExpectedQuadraticObjectiveIntegrate::initialize(
    DoseInfluenceMatrix& Dij)
{
    is_initialized_ = true;
}



/**
 * calculate gradient and objective 
 *
 */
double ExpectedQuadraticObjectiveIntegrate::calculate_objective_and_gradient(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    assert(is_initialized_);
    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;
    double total_squared_voxel_objective = 0;
    int temp_sampling_threshold; 
    if(sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = use_voxel_sampling ? 
        ((float) nVoxels) * ((float) temp_sampling_threshold)
        / ((float) RAND_MAX): nVoxels;
    double adjusted_weight = weight_ / expected_nVoxels;

    // loop over all voxels in this VOI and look up dose deviation
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
        if(use_voxel_sampling && std::rand() > temp_sampling_threshold) {
            continue;
        }
        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

	double voxel_objective = 
            the_UncertaintyModel_->calculate_quadratic_objective(
                    voxelNo,
                    desired_dose_,
                    beam_weights,
                    Dij,
                    gradient,
                    gradient_multiplier,
                    adjusted_weight);
        objective += voxel_objective;
        total_squared_voxel_objective += voxel_objective * voxel_objective;
    }

    // Update SSVO if needed
    estimated_ssvo = use_voxel_sampling ? 
        (total_squared_voxel_objective / sampling_fraction_) 
        : total_squared_voxel_objective;

    return objective;
}

/**
 * calculate objective 
 *
 */
double ExpectedQuadraticObjectiveIntegrate::calculate_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        bool use_voxel_sampling,
        double &estimated_ssvo)
{
    double objective = 0;
    double total_squared_voxel_objective = 0;
    int temp_sampling_threshold; 
    if(sampling_fraction_ < 1) {
        temp_sampling_threshold = (int)(sampling_fraction_ * RAND_MAX);
    }
    else {
        temp_sampling_threshold = RAND_MAX;
    }

    size_t nVoxels = the_voi_->get_nVoxels();
    double expected_nVoxels = use_voxel_sampling ? 
        ((float) nVoxels) * ((float) temp_sampling_threshold)
        / ((float) RAND_MAX): nVoxels;
    double adjusted_weight = weight_ / expected_nVoxels;

    // loop over all voxels in this VOI and look up dose deviation
    for(size_t iVoxel=0; iVoxel<nVoxels; iVoxel++) {
        if(use_voxel_sampling && std::rand() > temp_sampling_threshold) {
            continue;
        }

        unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

	float exp_dose = the_UncertaintyModel_->calculate_expected_voxel_dose(voxelNo,
									      beam_weights,
									      Dij);
	float var = the_UncertaintyModel_->calculate_voxel_variance(voxelNo,
								    beam_weights,
								    Dij);

	double voxel_objective = adjusted_weight * ((exp_dose-desired_dose_)*(exp_dose-desired_dose_) + var);

        objective += voxel_objective;
        total_squared_voxel_objective += voxel_objective * voxel_objective;
    }

    // Update SSVO if needed
    estimated_ssvo = use_voxel_sampling ? 
        (total_squared_voxel_objective / sampling_fraction_) 
        : total_squared_voxel_objective;

    return objective;
}



/**
 * calculate objective based on dose vector. Does not take expectation
 */
double ExpectedQuadraticObjectiveIntegrate::calculate_dose_objective(
    const DoseVector & the_dose)
{
    double objective = 0;
    size_t nVoxels = the_voi_->get_nVoxels();
    double adjusted_weight = weight_ / nVoxels;

    // loop over all voxels in this VOI
    for(unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
	
	unsigned int voxelNo = the_voi_->get_voxel(iVoxel);

	objective += adjusted_weight * (the_dose.get_voxel_dose(voxelNo)-desired_dose_)
	    * (the_dose.get_voxel_dose(voxelNo)-desired_dose_);
    }

    return objective;
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void ExpectedQuadraticObjectiveIntegrate::printOn(std::ostream& o) const
{
  o << "OBJ " << get_objNo() << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  o << "\tprescribed dose = ";
  o << desired_dose_;
  o << "\tweight " << weight_;
  o << "\t(exp. quad. obj. int.)";
}


/**
 * not implemented
 */
double ExpectedQuadraticObjectiveIntegrate::calculate_objective_and_gradient_and_Hv(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier,
    const vector<float> & v,
    vector<float> & Hv,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    cout << "calculate_objective_and_gradient_and_Hv not implemented for ExpectedQuadraticObjectiveIntegrate" << endl;
	throw(std::runtime_error("cannot continue"));
}


