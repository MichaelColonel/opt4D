/**
 * @file: MaxMinOrMinMaxObjectiv.cpp
 * MaxMinOrMinMaxObjectiv Class implementation.
 *
 */


#include "MaxMinOrMinMaxObjective.hpp"

/**
 * Constructor
 */
MaxMinOrMinMaxObjective::MaxMinOrMinMaxObjective(Voi*  the_voi,
						 unsigned int objNo,
						 bool minimize_max,
						 float weight)
    : Objective(objNo,weight)
    , the_voi_(the_voi)
    , minimize_max_(minimize_max)
{
}

/**
 * Destructor
 */
MaxMinOrMinMaxObjective::~MaxMinOrMinMaxObjective()
{
}

/**
 * initialize
 */
void MaxMinOrMinMaxObjective::initialize(DoseInfluenceMatrix& Dij)
{
    is_initialized_ = true;
}
  
/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void MaxMinOrMinMaxObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    if(minimize_max_) {
	o << "\tminimizing max dose" << endl;
    }
    else {
	o << "\tmaximizing min dose" << endl;
    }
}

/**
 * returns total number of constraints for commercial solver interface
 */
unsigned int MaxMinOrMinMaxObjective::get_nGeneric_constraints() const
{
    return(get_nVoxels());
}

/**
 * returns total number of auxiliary variables
 */
unsigned int MaxMinOrMinMaxObjective::get_nAuxiliary_variables() const
{
    return(1);
}

/**
 * calculate objective
 */
double MaxMinOrMinMaxObjective::calculate_objective(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    bool use_voxel_sampling,
    double &estimated_ssvo)
{
    double max_dose = 0;
    double min_dose = OPT4D_INFINITY;
    unsigned int voxelNo;
    float dose;

    for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
	voxelNo = the_voi_->get_voxel(iVoxel);
	dose = Dij.calculate_voxel_dose(voxelNo, beam_weights);
	if(dose>max_dose) {
	    max_dose = dose;
	}
	if(dose<min_dose) {
	    min_dose = dose;
	}
    }
    if(minimize_max_) {
	return(weight_*max_dose);
    }
    else {
	return(weight_*min_dose);
    }
}

/**
 * calculate objective from dose vector
 */
double MaxMinOrMinMaxObjective::calculate_dose_objective(const DoseVector & the_dose)
{
    double max_dose = 0;
    double min_dose = OPT4D_INFINITY;
    unsigned int voxelNo;

    for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
	voxelNo = the_voi_->get_voxel(iVoxel);
	if(the_dose[voxelNo]>max_dose) {
	    max_dose = the_dose[voxelNo];
	}
	if(the_dose[voxelNo]<min_dose) {
	    min_dose = the_dose[voxelNo];
	}
    }
    if(minimize_max_) {
	return(weight_*max_dose);
    }
    else {
	return(weight_*min_dose);
    }
}


/**
 * prepare constraints for commercial solver interface
 */
void MaxMinOrMinMaxObjective::add_to_optimization_data_set(GenericOptimizationData & data, DoseInfluenceMatrix & Dij) const
{
  cout << "Creating generic optimization data for OBJ " << get_objNo() << endl;

  // index to the single auxiliary variable to be introduced
  unsigned int iAux = data.nVarInt_ + data.nAuxiliaries_until_now_;

  // update number of auxiliary variables
  data.nAuxiliaries_until_now_ += 1;

  // bounds for auxiliary variable
  data.upper_var_bound_[iAux] = OPT4D_INFINITY;
  data.lower_var_bound_[iAux] = -OPT4D_INFINITY;
  data.var_bound_type_[iAux] = FREE;

  // objective function coefficient
  data.obj_coeffs_[iAux] += weight_; // maximizing the min is handled via negative weight

  // add constraints
  for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {

    // Find the voxelNo of the voxel we are looking at
    unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
 
    // get number of nonzero dij elements
    unsigned int nEntries = Dij.get_nEntries_per_voxel(voxelNo);

    // initialize next constraint
    data.constraints_[data.nConstraints_until_now_].initialize(nEntries+1);

    // add to number of nonzero coefficients
    data.nNonZeros_ += (nEntries+1);

    // the coefficients are just the Dij elements
    unsigned int iEntry = 0;
    for(DoseInfluenceMatrix::iterator iter = Dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
      // 	data.constraints_.back().index_[iEntry] = iter.get_bixelNo();
      //	data.constraints_.back().value_[iEntry] = iter.get_influence();
      data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iter.get_bixelNo();
      data.constraints_[data.nConstraints_until_now_].value_[iEntry] = iter.get_influence();
      iEntry++;
    }

    // coefficient for auxiliary variable
    //    data.constraints_.back().index_[iEntry] = iAux; 
    //    data.constraints_.back().value_[iEntry] = -1.0;
    data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iAux; 
    data.constraints_[data.nConstraints_until_now_].value_[iEntry] = -1.0;

    // bounds
    if(minimize_max_) { // minimizing max dose
      //	data.constraints_.back().upper_bound_ = 0;
      //	data.constraints_.back().lower_bound_ = -OPT4D_INFINITY;
      //	data.constraints_.back().type_ = UPPERBOUND;
	data.constraints_[data.nConstraints_until_now_].upper_bound_ = 0;
	data.constraints_[data.nConstraints_until_now_].lower_bound_ = -OPT4D_INFINITY;
	data.constraints_[data.nConstraints_until_now_].type_ = UPPERBOUND;
    }
    else { //maximizing min dose
      //	data.constraints_.back().lower_bound_ = 0;
      //	data.constraints_.back().upper_bound_ = OPT4D_INFINITY;
      //	data.constraints_.back().type_ = LOWERBOUND;
	data.constraints_[data.nConstraints_until_now_].lower_bound_ = 0;
	data.constraints_[data.nConstraints_until_now_].upper_bound_ = OPT4D_INFINITY;
	data.constraints_[data.nConstraints_until_now_].type_ = LOWERBOUND;
    }

    // increment constrait counter
    data.nConstraints_until_now_ += 1;
    //    cout << data.nConstraints_until_now_ << " ";
  }
}
