/**
 * @file KonradDoseInfluenceMatrix.cpp
 * KonradDoseInfluenceMatrix class implementation
 *
 * @version 0.9.4
 * <pre>
 * ver 0.9.4    BM      Jul 15, 2004    several changes.
 * ver 0.9.2    AT      Apr 05, 2004    beams from Dij are now matched to the
 *                                      steering file info 
 * ver 0.9.0     TB      Mar 02, 2004   moved optimization functions from
 *                                      Dij_matrix to optimization;
 *                                      no abstraction Dij->dose_calc
 * ver 0.8.10    AT      Feb 27, 2004   minor fixes, changes, cleanup
 * ver 0.8.7     AT      Feb 05, 2004   added write_Dij_matrix (KonRad format),
 *                                      replace_Dij 
 * ver 0.8       AT      Dec 05, 2003   implemented optimization with weights,
 *                                      readout of KonRad Dij-matrix
 * ver 0.7       TB      Oct 25, 2003   implemented aperture matrix
 * ver 0.6       TB      Sep 28, 2003   zero columns are disregarded
 * ver 0.5       TB      Sep 09, 2003   creation
 * AT: Alexei Trofimov
 * TB: Thomas Bortfeld
 * BM: Ben Martin
 * Massachusetts General Hospital, Department of Radiation Oncology
 * </pre>
 */

#include <cmath>
#include <stdexcept>

#ifndef _WIN32
#include <unistd.h>  // sleep()
#else 
#include <winsock.h>
#define sleep(secs) (Sleep(secs * 1000))
#endif

#include "KonradDoseInfluenceMatrix.hpp"
// #include "PencilBeam.hpp"

#include "DijFileParser.hpp"

using std::cout;
using std::cerr;
using std::endl;


/**
 * Constructor that reads multiple Dij-matrices from a file and stores them as 
 * auxiliaries.
 * @todo add functionality to transpose Dij directly
 * @todo Change this constructor to use the DijFileParser class
 */
KonradDoseInfluenceMatrix::KonradDoseInfluenceMatrix(
        const string dij_file_root,
        Geometry *geometry,
        const bool unify,
        const size_t nBeams,
        const size_t nInstances,
        const bool transpose)
: DoseInfluenceMatrix(false),
    dij_file_root_(dij_file_root),
    nInstances_(nInstances),
    nBeams_(nBeams),
    nBeams_in_instance_(1, nBeams),
    starting_beamNo_(1, 0),
    nBixels_in_beam_(nBeams_, 0),
    starting_bixelNo_(nBeams_, 0),
    geometry_(geometry),
    dose_scale_coeficient_(nBeams_,0)
{
    assert(nInstances_>0);

    /* 
     * currently there is only one version of this constructor:
     *
     * for a given bixel, the all voxels that are included in one of the 
     * instances are included in the Dij (with zero influence value for the 
     * instances which didn't contain the element initially).
     *
     * There is supposed to be a secord version of this constructor which first 
     * reads the "refernce" Dij and than includes only this set of elements.  
     * Voxel-Bixel-Combinations which are included in one of the additional 
     * instances but not in the reference Dij will be ignored.
     */

    cout << endl << "Dij Constructor that reads Dijs for " << nInstances_ 
	<< " instances as auxiliary Dij matrices" << endl << endl;

    string dij_file_name;
    char cbeam[10];
    char cinstance[10];

    float fDummy;
    unsigned int iDummy;

    size_t total_nDij = 0;
    size_t total_nJ = 0;

    // create vector of file pointers
    std::ifstream dij_file;
    vector<std::ifstream *> dij_files;
    for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {
	dij_files.push_back(new std::ifstream());
    }

    // Temporary variables to hold geometry information
    vector<float> gantry_angle(nBeams_,0);
    vector<float> table_angle(nBeams_,0);
    vector<float> collimator_angle(nBeams_,0);
    vector<float> beam_bixel_dx(nBeams_,0);
    vector<float> beam_bixel_dy(nBeams_,0);
    vector<float> bixel_spot_x(0);
    vector<float> bixel_spot_y(0);
    vector<float> bixel_energy(0);
    vector<float> bixel_dx(0);
    vector<float> bixel_dy(0);
    vector<unsigned int> bixel_beamNo(0);
    vector<Bixel_Position> bixel_position(0);

    // Vectors to hold number of entries
    vector<unsigned int> nEntries_per_bixel;
    vector<unsigned int> nEntries_per_voxel;

    vector<bool> temp_index;
    vector<float> temp_influence;

    vector<vector<unsigned int> > voxels_in_bixel;

    cout << "pre-read files to get number of Dij elements" << endl;

    // Pre-read the files to find number of elements, etc
    float epsilon = 0.001;
    for(unsigned int iBeam=0; iBeam<nBeams_; iBeam++) {

	sprintf(cbeam,"%d",iBeam+1);

	for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {

	    sprintf(cinstance,"%d",iInstance+1);

	    if(iInstance == 0) {
		dij_file_name = dij_file_root + "_" + string(cbeam) + ".dij";
	    }
	    else {
		dij_file_name = dij_file_root + "_" + string(cinstance) 
		    + "_" + string(cbeam) + ".dij";
	    }

	    (*(dij_files[iInstance])).open(dij_file_name.c_str(),
		    std::ios::binary);

	    // check if the file exists
	    if ( !(*(dij_files[iInstance])).is_open() ) {
		std::cerr << "Can't open file: " << dij_file_name << std::endl;
		throw("Unable to open dij file");
	    }

	    // read the header
	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(iInstance == 0) {
		gantry_angle[iBeam] = fDummy;
	    } else {
		if(!comp_float_abs(fDummy,gantry_angle[iBeam],epsilon)) {
		    cout<<"error reading Dij: gantry"<<endl; exit(1);
		}
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(iInstance == 0) {
		table_angle[iBeam] = fDummy;
	    } else {
		if(!comp_float_abs(fDummy,table_angle[iBeam],epsilon)) {
		    cout<<"error reading Dij: table"<<endl; exit(1);
		}
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(iInstance == 0) {
		collimator_angle[iBeam] = fDummy;
	    } else {
		if(!comp_float_abs(fDummy,collimator_angle[iBeam],epsilon)) {
		    cout<<"error reading Dij: colli"<<endl;
		    exit(1);
		}
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(iInstance == 0) {
		beam_bixel_dx[iBeam] = fDummy;
	    } else {
		if(!comp_float_abs(fDummy,beam_bixel_dx[iBeam],epsilon)) {
		    cout<<"error reading Dij: bixel dx"<<endl;
		    exit(1);
		}
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(iInstance == 0) {
		beam_bixel_dy[iBeam] = fDummy;
	    } else {
		if(!comp_float_abs(fDummy,beam_bixel_dy[iBeam],epsilon)) {
		    cout<<"error reading Dij: bixel dy"<<endl; exit(1);
		}
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(!comp_float_abs(fDummy,geometry_->get_voxel_dx(),epsilon)) {
		cout<<"error reading Dij: voxel dx"<<endl;
	       	exit(1);
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(!comp_float_abs(fDummy,geometry_->get_voxel_dy(),epsilon)) {
		cout<<"error reading Dij: voxel dy"<<endl;
	       	exit(1);
	    }

	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(!comp_float_abs(fDummy,geometry_->get_voxel_dz(),epsilon)) {
		cout<<"error reading Dij: voxel dz"<<endl;
	       	exit(1);
	    }

	    iDummy = read_binary_int(*dij_files[iInstance]);
	    if(iDummy!=geometry_->get_voxel_nx()) {
		cout<<"error reading Dij: voxel nX"<<endl;
	       	exit(1);
	    }

	    iDummy = read_binary_int(*dij_files[iInstance]);
	    if(iDummy!=geometry_->get_voxel_ny()) {
		cout<<"error reading Dij: voxel nY"<<endl;
	       	exit(1);
	    }

	    iDummy = read_binary_int(*dij_files[iInstance]);
	    if(iDummy!=geometry_->get_voxel_nz()) {
		cout<<"error reading Dij: voxel nZ"<<endl;
	       	exit(1);
	    }

	    // read number of pencil beams
	    iDummy = read_binary_int(*dij_files[iInstance]);
	    if(iInstance == 0) {
		nBixels_in_beam_[iBeam] = iDummy;
		total_nJ += nBixels_in_beam_[iBeam]; 
		if(iBeam == 0) {
		    starting_bixelNo_[iBeam] = 0;
	       	} else {
		    starting_bixelNo_[iBeam] = starting_bixelNo_[iBeam-1]
			+ nBixels_in_beam_[iBeam-1];
		}
	    } else {
		if(iDummy!=nBixels_in_beam_[iBeam]) {
		    cout<<"error reading Dij: "
			<<"The number of bixels in instance "<<iInstance
			<<" is "<<iDummy<<" whereas it is supposed to be "
			<<nBixels_in_beam_[iBeam]<<endl; exit(1);
		}
	    }

	    // absolute dose scale factor
	    fDummy = read_binary_float(*dij_files[iInstance]);
	    if(iInstance == 0) {
		dose_scale_coeficient_[iBeam] = fDummy;
	    }
	    else {
		if(!comp_float_abs(fDummy,dose_scale_coeficient_[iBeam],epsilon)) {
		    cout<<"error reading Dij: absolute dose scale factor"<<endl;
		    exit(1);
		}
	    }

	} // end iInstance

	// set number of voxels/bixels
	this->set_nBixels(total_nJ);
	this->set_nVoxels(geometry_->get_voxel_nx()
		* geometry_->get_voxel_ny()
		* geometry_->get_voxel_nz());

	// Expand arrays to hold total number of beamlets
	bixel_spot_x.reserve(total_nJ);
	bixel_spot_y.reserve(total_nJ);
	bixel_energy.reserve(total_nJ);
	bixel_dx.reserve(total_nJ);
	bixel_dy.reserve(total_nJ);
	bixel_beamNo.reserve(total_nJ);
	bixel_position.reserve(total_nJ);
	nEntries_per_bixel.reserve(total_nJ);
	voxels_in_bixel.resize(total_nJ); 

	temp_index.resize(get_nVoxels(),false);

	// read the beam information first to calculate the total # of Dij 
	// entries
	for (size_t iBixel=0; iBixel<nBixels_in_beam_[iBeam]; iBixel++) {

	    unsigned int bixelNo = iBixel + starting_bixelNo_[iBeam];
	    unsigned int nCounts = 0;
	    float fx, fy, fE;

	    bixel_dx.push_back(beam_bixel_dx[iBeam]);
	    bixel_dy.push_back(beam_bixel_dy[iBeam]);
	    bixel_beamNo.push_back(iBeam); 

	    fill(temp_index.begin(),temp_index.end(),false);

	    for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {

		float epsilon = 0.001;

		// read the bixel header
		fDummy = read_binary_float(*dij_files[iInstance]);
		fx = fDummy;
		if(iInstance == 0) {
		    bixel_energy.push_back(fDummy);
		} else {
		    if(!comp_float_abs(fDummy,bixel_energy[bixel_energy.size()-1],epsilon)) {
			cout<<"error reading Dij: energy"<<endl;
		       	exit(1);
		    }
		}

		fDummy = read_binary_float(*dij_files[iInstance]);
		fy = fDummy;
		if(iInstance == 0) {
		    bixel_spot_x.push_back(fDummy);
		} else {
		    if(!comp_float_abs(fDummy,bixel_spot_x[bixel_spot_x.size()-1],epsilon)) {
			cout<<"error reading Dij: pos x"<<endl;
			exit(1);
		    }
		}

		fDummy = read_binary_float(*dij_files[iInstance]);
		fE = fDummy;
		if(iInstance == 0) {
		    bixel_spot_y.push_back(fDummy);
		} else {
		    if(!comp_float_abs(fDummy,bixel_spot_y[bixel_spot_y.size()-1],epsilon)) {
			cout<<"error reading Dij: pos y"<<endl;
			exit(1);
		    }
		}

		if(iInstance == 0) {
		    bixel_position.push_back(Bixel_Position(fx,fy,fE));
		}

		unsigned int Nvox = read_binary_int(*dij_files[iInstance]);

		for(unsigned int i=0; i<Nvox; i++) {
		    iDummy = read_binary_int(*dij_files[iInstance]);
		    (*dij_files[iInstance]).ignore(sizeof(short));
		    if(!temp_index[iDummy]) {
			temp_index[iDummy] = true;
			nCounts++;
		    }
		}
	    }

	    // store the voxels we found
	    total_nDij += nCounts; 
	    nEntries_per_bixel.push_back(nCounts);
	    voxels_in_bixel[bixelNo].reserve(nCounts);
	    voxels_in_bixel[bixelNo].resize(0);
	    for(unsigned int i=0; i<temp_index.size(); i++) {
		if(temp_index[i]) {
		    voxels_in_bixel[bixelNo].push_back(i);
		}
	    }
	}

	// close all files
	for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {
	    (*dij_files[iInstance]).close();
	    (*dij_files[iInstance]).clear();
	}
    }

    temp_influence.reserve(get_nVoxels());

    // reserve memory for the Dij and set the voxel indices
    this->reserve_Dij(total_nDij);
    this->set_nEntries_per_bixel(vector<unsigned int>(get_nBixels(),0));
    this->add_blank_space(nEntries_per_bixel);

    for (size_t iBixel=0; iBixel<get_nBixels(); iBixel++) {
	assert(nEntries_per_bixel[iBixel] == voxels_in_bixel[iBixel].size());
	temp_influence.resize(nEntries_per_bixel[iBixel]);
	fill(temp_influence.begin(),temp_influence.end(),0);
	this->add_Dij_elements_to_bixel(iBixel, voxels_in_bixel[iBixel],
		temp_influence);
    }

    // create auxiliaries
    for(unsigned int iInstance=0; iInstance<nInstances_-1; iInstance++) {
	create_auxiliary_Dij(); 
    } 

    // read the values
    for(unsigned int iBeam=0; iBeam<nBeams_; iBeam++) {

	sprintf(cbeam,"%d",iBeam+1);

	for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {

	    sprintf(cinstance,"%d",iInstance+1);

	    if(iInstance == 0) {
		dij_file_name = dij_file_root + "_" + string(cbeam) + ".dij";
	    }
	    else {
		dij_file_name = dij_file_root + "_" + string(cinstance) 
		    + "_" + string(cbeam) + ".dij";
	    }

	    dij_file.open(dij_file_name.c_str(), std::ios::binary);

	    // check if the file exists
	    if ( !dij_file.is_open() ) {
		std::cerr << "Can't open file: " << dij_file_name << std::endl;
		throw("Unable to open dij file");
	    }

	    // Skip the header since we've already read it and start reading 
	    // the Dij data
	    dij_file.ignore(9*sizeof(float)+4*sizeof(int));

	    for (unsigned iBixel=0; iBixel<nBixels_in_beam_[iBeam]; ++iBixel) {

		unsigned int bixelNo = iBixel + starting_bixelNo_[iBeam];

		// skip the bixel header
		unsigned int Nvox;
		dij_file.ignore(3*sizeof(float));
		Nvox = read_binary_int(dij_file);

		// clear temp vector
		temp_influence.resize(get_nVoxels());
		fill(temp_influence.begin(),temp_influence.end(),0);

		// Read each Dij entry
		for (unsigned int i=0; i<Nvox; i++) {
		    unsigned int voxel = 0;
		    short temp = 0;
		    float influence = 0;
		    voxel = read_binary_int(dij_file);
		    temp = read_binary_short(dij_file);
		    influence = ((float)temp) * dose_scale_coeficient_[iBeam];
		    temp_influence[voxel] = influence;
		}

		// iterate over the Dij elements and set the value
		for(DoseInfluenceMatrix::non_const_iterator iter =
			this->non_const_begin_bixel(bixelNo); 
			iter.get_bixelNo()==bixelNo; ++iter) {
		    iter.set_influence(temp_influence[iter.get_voxelNo()],
			    iInstance);
		}
	    }
	    // close file
	    dij_file.close();
	}

	cout <<  " Beam: " << iBeam 
	    << " Number of pencil beams:  " << nBixels_in_beam_[iBeam] << endl;
    }

    cout << "nBixels: " << get_nBixels() << endl;

    // double-check whether Dij matrix is sorted
    if(confirm_sorted()) {
	is_sorted_ = true;      
    }
    else {
	is_sorted_ = false;
    }

    // Pass Geometry info to geometry class
    geometry_->set_nBeams(nBeams_);
    geometry_->set_nBixels(get_nBixels());
    geometry_->set_gantry_angle(gantry_angle);
    geometry_->set_table_angle(table_angle);
    geometry_->set_collimator_angle(collimator_angle);
    geometry_->set_bixel_grid_size(beam_bixel_dx, beam_bixel_dy);
    geometry_->set_bixel_position(bixel_position);
    geometry_->set_bixel_beamNo(bixel_beamNo);
    geometry_->set_beam_starting_bixelNo(starting_bixelNo_);
    geometry_->set_nBixels_in_beam(nBixels_in_beam_);

    geometry_->initialize_bixel_grid();
    geometry_->set_isocenters();
    geometry_->set_initialized(true); 

}


/**
 * Constructor that reads Dij-matrix from a file
 */
KonradDoseInfluenceMatrix::KonradDoseInfluenceMatrix(
        const string dij_file_root,
        Geometry *geometry,
        const size_t nBeams_per_instance,
        const size_t nInstances,
        const bool transpose)
: DoseInfluenceMatrix(transpose) ,
    dij_file_root_(dij_file_root) ,
    nInstances_(nInstances) ,
    nBeams_(nBeams_per_instance * nInstances) ,
    nBeams_in_instance_(nInstances, nBeams_per_instance) ,
    starting_beamNo_(nInstances, 0) ,
    nBixels_in_beam_(nBeams_, 0) ,
    starting_bixelNo_(nBeams_, 0) ,
    geometry_(geometry) ,
    dose_scale_coeficient_(nBeams_,0)
{

    cout << endl << "Dij Constructor that reads Dijs for " << nInstances_ 
        << " instances as multiple beams" << endl << endl;

    string dij_file_name;
    char cbeam[10];
    char cinstance[10];

    size_t total_nDij = 0;
    size_t total_nJ = 0;

    // Temporary variables to hold geometry information
    DijFileParser::header temp_header;
    DijFileParser::bixel temp_bixel;
    float voxel_dx(0), voxel_dy(0), voxel_dz(0);
    unsigned int voxel_nX(0), voxel_nY(0), voxel_nZ(0);
    vector<float> gantry_angle(nBeams_,0);
    vector<float> table_angle(nBeams_,0);
    vector<float> collimator_angle(nBeams_,0);
    vector<float> beam_bixel_dx(nBeams_,0);
    vector<float> beam_bixel_dy(nBeams_,0);
    vector<unsigned int> bixel_beamNo(0);
    vector<Bixel_Position> bixel_position(0);

    // Fill starting_beamNo_ vector
    for(size_t iInstance = 1; iInstance < nInstances_; iInstance++) {
        starting_beamNo_[iInstance] = starting_beamNo_[iInstance-1]
            + nBeams_in_instance_[iInstance-1];
    }

    // Vectors to hold number of entries
    vector<unsigned int> nEntries_per_bixel;
    vector<unsigned int> nEntries_per_voxel;

    // Pre-read the files to find number of elements, etc
    for(size_t iInstance = 0; iInstance < nInstances_; iInstance ++) {
        unsigned int last_beamNo_in_instance = starting_beamNo_[iInstance]
            + nBeams_in_instance_[iInstance];
        for(size_t iBeam = starting_beamNo_[iInstance];
                iBeam < last_beamNo_in_instance; iBeam++) {
            // convert beam# to string
            unsigned int temp_int = iInstance+1;
            sprintf(cinstance,"%d",temp_int);
            temp_int = iBeam+1-starting_beamNo_[iInstance];
            sprintf(cbeam,"%d",temp_int);
            if(iInstance == 0) {
                dij_file_name = dij_file_root + "_" + string(cbeam) + ".dij";
            } else {
                dij_file_name = dij_file_root + "_" + string(cinstance) 
                    + "_" + string(cbeam) + ".dij";
            }

            // Open file (throws exception if openning fails)
            DijFileParser dij_file(dij_file_name);

            
            // Store information from header
            temp_header = dij_file.get_header();
            gantry_angle[iBeam] = temp_header.a_g_;
            table_angle[iBeam] = temp_header.a_t_;
            collimator_angle[iBeam] = temp_header.a_c_;
            beam_bixel_dx[iBeam] = temp_header.dx_b_;
            beam_bixel_dy[iBeam] = temp_header.dy_b_;
            nBixels_in_beam_[iBeam] = temp_header.npb_;
            dose_scale_coeficient_[iBeam] = temp_header.scalefactor_;
            voxel_dx = temp_header.dx_;
            voxel_dy = temp_header.dy_;
            voxel_dz = temp_header.dz_;
            voxel_nX = temp_header.nx_;
            voxel_nY = temp_header.ny_;
            voxel_nZ = temp_header.nz_;

            // Setup starting_bixelNo_
            total_nJ += nBixels_in_beam_[iBeam];
            if(iBeam == 0) {
                starting_bixelNo_[iBeam] = 0;
            }
            else {
                starting_bixelNo_[iBeam] = starting_bixelNo_[iBeam-1]
                    + nBixels_in_beam_[iBeam-1];
            }

            // check voxel grid size against dif file information */
            if ( temp_header.nx_ != geometry_->get_voxel_nx() ||
		 temp_header.ny_ != geometry_->get_voxel_ny() ||
		 temp_header.nz_ != geometry_->get_voxel_nz() )
            {
                std::cerr << "Voxel grid dimension in .dij files \n"
                    << "does not match the .dif file"
                    << std::endl;
                std::cerr << "size x: " << temp_header.nx_ << " should = "
                    << geometry_->get_voxel_nx() << std::endl;
                std::cerr << "size y: " << temp_header.ny_ << " should = "
                    << geometry_->get_voxel_ny() << std::endl;
                std::cerr << "size z: " << temp_header.nz_ << " should = "
                    << geometry_->get_voxel_nz() << std::endl;
                throw("wrong voxel grid");
            }

            // check voxel grid resolution against dif file information */
	    float epsilon = 0.001;
	    if (comp_float_abs(temp_header.dx_, geometry_->get_voxel_dx(), epsilon) &&
		comp_float_abs(temp_header.dy_, geometry_->get_voxel_dy(), epsilon) &&
		comp_float_abs(temp_header.dz_, geometry_->get_voxel_dz(), epsilon) )
	    {
		// everything ok
	    }
	    else {
                std::cerr << "Voxel grid resolution in .dij files \n"
                    << "does not match the .dif file"
                    << std::endl;
		std::cerr << "resolution x: " << temp_header.dx_ << " should = "
                    << geometry_->get_voxel_dx() << std::endl;
                std::cerr << "resolution y: " << temp_header.dy_ << " should = "
                    << geometry_->get_voxel_dy() << std::endl;
                std::cerr << "resolution z: " << temp_header.dz_ << " should = "
                    << geometry_->get_voxel_dz() << std::endl;
                throw("wrong voxel grid");
            }

            // Allocate room to hold nEntries per bixel or voxel
            if(transpose) {
                if(nEntries_per_voxel.size() == 0) {
                    nEntries_per_voxel.resize(voxel_nX*voxel_nY*voxel_nZ);
                    fill(nEntries_per_voxel.begin(), nEntries_per_voxel.end(),
                            0);
                }
            } else {
                nEntries_per_bixel.resize(total_nJ);
            }

            // read the beam information first to calculate the total # of Dij 
            // entries
            for (size_t iBixel=0; iBixel<nBixels_in_beam_[iBeam]; iBixel++) {

                // Read beam
                dij_file.read_next_bixel(temp_bixel);

                // Get the number of voxels
                unsigned int Nvox = temp_bixel.nVox_;
                total_nDij += Nvox;

                if(transpose) {
                    // Count the number of entries of each voxelNo
                    for (int i=0; i<temp_bixel.nVox_; i++) {
                        nEntries_per_voxel[temp_bixel.voxelNo_[i]]++;
                    }
                }
                else {
                    // Store the number of entries for the bixelNo
                    nEntries_per_bixel[iBixel + starting_bixelNo_[iBeam]]
                        = Nvox;

                    // Ignore the rest
                }
            }
        }
    }

    // Expand arrays to hold total number of beamlets
    bixel_beamNo.reserve(total_nJ);
    bixel_position.reserve(total_nJ);

    // bixel_nNonzero_voxels_.reserve(total_nJ);

    // Allocate space in parent class
    this->set_nBixels(total_nJ);
    this->set_nVoxels(voxel_nX * voxel_nY * voxel_nZ);
    this->reserve_Dij(total_nDij);
    if(transpose) {
        this->set_nEntries_per_voxel(nEntries_per_voxel);
    }
    else {
        this->set_nEntries_per_bixel(nEntries_per_bixel);
    }

    // Now really read the files
    for(size_t iInstance = 0; iInstance < nInstances_; iInstance ++) {
        unsigned int last_beamNo_in_instance = starting_beamNo_[iInstance]
            + nBeams_in_instance_[iInstance];
        for(size_t iBeam = starting_beamNo_[iInstance];
                iBeam < last_beamNo_in_instance;
                iBeam++) {

            // convert beam# to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
            if(iInstance == 0) {
                dij_file_name = dij_file_root + "_" + string(cbeam) + ".dij";
            }
            else {
                dij_file_name = dij_file_root + "_" + string(cinstance) 
                    + "_" + string(cbeam) + ".dij";
            }

            // Open file (throws exception if openning fails)
            DijFileParser dij_file(dij_file_name);

            // Skip the header since we've already read it and start reading 
            // the Dij data

            unsigned int nDij = 0;
            for (unsigned iBixel=0; iBixel<nBixels_in_beam_[iBeam]; ++iBixel) {

                // read the bixel data
                dij_file.read_next_bixel(temp_bixel);

                nDij += temp_bixel.nVox_;

                // Store bixel properties
		bixel_position.push_back(Bixel_Position(temp_bixel.spot_x_,
							temp_bixel.spot_y_,
							temp_bixel.energy_));

                bixel_beamNo.push_back(iBeam); 

                // Read each Dij entry
                for (size_t i=0; i<temp_bixel.nVox_; i++) {
                    float influence = ((float) temp_bixel.value_[i])
                        * dose_scale_coeficient_[iBeam];
                    add_Dij_element(iBixel + starting_bixelNo_[iBeam],
                            temp_bixel.voxelNo_[i], influence);
                }
            }

            cout << "Instance " << iInstance + 1
                <<  " Beam " << iBeam + 1 - starting_beamNo_[iInstance]
                << " Number of pencil beams:  " << nBixels_in_beam_[iBeam]
                << " Number of Dij-entries:  " << nDij << endl;
        }
    }

    cout << "nBixels: " << get_nBixels() << endl;

    // double-check whether Dij matrix is sorted
    if(confirm_sorted()) {
        is_sorted_ = true;      
    }
    else {
        is_sorted_ = false;
    }

    // Pass Geometry info to geometry class
    geometry_->set_nBeams(nBeams_);
    geometry_->set_nBixels(get_nBixels());
    geometry_->set_gantry_angle(gantry_angle);
    geometry_->set_table_angle(table_angle);
    geometry_->set_collimator_angle(collimator_angle);
    geometry_->set_bixel_grid_size(beam_bixel_dx, beam_bixel_dy);
    geometry_->set_bixel_position(bixel_position);
    geometry_->set_bixel_beamNo(bixel_beamNo);
    geometry_->set_beam_starting_bixelNo(starting_bixelNo_);
    geometry_->set_nBixels_in_beam(nBixels_in_beam_);

    geometry_->initialize_bixel_grid();
    geometry_->set_isocenters();
    geometry_->set_initialized(true); 
}



/**
 * Constructor that reads Dij-matrix from a file, but ignores bixels that are 
 * not included in the bixel vector.
 *
 */
KonradDoseInfluenceMatrix::KonradDoseInfluenceMatrix(
        const string dij_file_root,
        Geometry *geometry,
        const size_t nBeams_per_instance,
        const size_t nInstances,
        const BixelVector & bixel_vector,
        const bool transpose)
: DoseInfluenceMatrix(transpose) ,
    dij_file_root_(dij_file_root) ,
    nInstances_(nInstances) ,
    nBeams_(nBeams_per_instance * nInstances) ,
    nBeams_in_instance_(nInstances, nBeams_per_instance) ,
    starting_beamNo_(nInstances_,0) ,
    nBixels_in_beam_(geometry->get_nBixels_in_beam()) ,
    starting_bixelNo_(geometry->get_beam_starting_bixelNo()) ,
    geometry_(geometry) ,
    dose_scale_coeficient_(nBeams_,0)
{

    cout << endl << "Dij Constructor that reads Dijs for " << nInstances_ 
	 << " instances as multiple beams." << endl;
    cout << "The BixelVector has been initialized first, " << endl
	 << "bixels not contained in the steering file are ignored" << endl
	 << endl;

    // check if bixel grid is valid
    if(!geometry_->bixel_grid_is_valid()) {
	cout << "This constructor only works for a valid bixel grid" << endl;
	cout << "Cannot continue" << endl;
	throw(std::runtime_error("invalid bixel grid"));
    }

    string dij_file_name;
    char cbeam[10];
    char cinstance[10];

    size_t total_nDij = 0;

    // Fill starting_beamNo_ vector
    for(size_t iInstance = 1; iInstance < nInstances_; iInstance++) {
        starting_beamNo_[iInstance] = starting_beamNo_[iInstance-1]
            + nBeams_in_instance_[iInstance-1];
    }

    // Temporary variables to hold geometry information
    DijFileParser::header temp_header;
    DijFileParser::bixel temp_bixel;
    float voxel_dx(0), voxel_dy(0), voxel_dz(0);
    unsigned int voxel_nX(0), voxel_nY(0), voxel_nZ(0);
 
    Bixel_Position bixel_pos;
    Bixel_Subscripts bixel_sub;
    BixelIndex bixel_index;
    unsigned int bixelNo;

    // Vectors to hold number of entries
    vector<unsigned int> nEntries_per_bixel;
    vector<unsigned int> nEntries_per_voxel;

    // bixel grid to tag the bixels which are already read
    vector<bool> bixel_grid;

    // Pre-read the files to find number of elements, etc
    for(size_t iInstance = 0; iInstance < nInstances_; iInstance ++) {
        unsigned int last_beamNo_in_instance = starting_beamNo_[iInstance]
            + nBeams_in_instance_[iInstance];
        for(size_t iBeam = starting_beamNo_[iInstance];
                iBeam < last_beamNo_in_instance; iBeam++) {
            // convert beam# to string
            unsigned int temp_int = iInstance+1;
            sprintf(cinstance,"%d",temp_int);
            temp_int = iBeam+1-starting_beamNo_[iInstance];
            sprintf(cbeam,"%d",temp_int);
            if(iInstance == 0) {
                dij_file_name = dij_file_root + "_" + string(cbeam) + ".dij";
            }
            else {
                dij_file_name = dij_file_root + "_" + string(cinstance) 
                    + "_" + string(cbeam) + ".dij";
            }

	    // Open file (throws exception if opening fails)
            DijFileParser dij_file(dij_file_name);

            std::cout << "Reading dij file: " << dij_file_name << endl;

	    // Store information from header
	    temp_header = dij_file.get_header();

	    // Get reference header from geometry class
	    DijFileParser::header reference_header(*geometry, iBeam);

            // Store the scale factor
            dose_scale_coeficient_[iBeam] = temp_header.scalefactor_;

	    // Set scale factor equal since Geometry does not contain it
	    reference_header.scalefactor_ = temp_header.scalefactor_;

	    // Check if some pencil beams will be ignored
            if(temp_header.npb_ > reference_header.npb_) {
                cout << "Not reading all beamlets from beam " << iBeam+1
                    << endl;
                std::cout << temp_header.npb_ << " found,"
                    << " expected " << reference_header.npb_ << endl;

		// Set the number equal for comparison's sake
		reference_header.npb_ = temp_header.npb_;
            }

	    // Check if the two headers match
	    if(!(reference_header == temp_header)) {
		std::cerr << "Dij file header does not match information"
		    << " in the Geometry class.\n";
		std::cerr << "In dij: " << temp_header << "\n"
		    << "In Geometry: " << reference_header << "\n";
		throw(std::runtime_error("Inconsistent files"));
	    }

	    // Allocate room to hold nEntries per bixel or voxel
            if(transpose) {
                if(nEntries_per_voxel.size() == 0) {
                    nEntries_per_voxel.resize(temp_header.nx_ 
			    * temp_header.ny_ * temp_header.nz_);
                    fill(nEntries_per_voxel.begin(), nEntries_per_voxel.end(),
                            0);
                }
            } 
	    else {
                nEntries_per_bixel.resize(bixel_vector.get_nBixels());
            }

	    // initialize the bixel grid
	    bixel_grid.resize(geometry_->get_beam_bixel_nPoints(iBeam));
	    fill(bixel_grid.begin(),bixel_grid.end(),false);

            // Read the beam information first to calculate the total # of Dij entries
	    while(!dij_file.at_end()) {

		float epsilon = 0.001;

		// read the bixel from the file
                dij_file.read_next_bixel(temp_bixel);

		// create bixel position
		bixel_pos = Bixel_Position(temp_bixel.spot_x_,
					   temp_bixel.spot_y_,
					   temp_bixel.energy_);

		// convert to subscripts in the bixel grid
		bixel_sub = geometry_->convert_to_Bixel_Subscripts(bixel_pos,iBeam);

		// check if this bixel is contained in the bixel vector
		if(geometry_->is_valid_bixel(bixel_sub,iBeam)) {

		    // get grid index
		    bixel_index = geometry_->convert_to_BixelIndex(bixel_sub,iBeam);

		    // get bixelNo
		    bixelNo = geometry_->get_bixelNo(bixel_index,iBeam);

		    // check if it has not been read already
		    if(!bixel_grid[geometry_->convert_to_BixelIndex(bixel_sub,iBeam).index_]) {

			// tag the bixel grid point
			bixel_grid[bixel_index.index_] = true;

			// add to number of Dij entries
			total_nDij += temp_bixel.nVox_;

			if(transpose) {
			    // Count the number of entries of each voxelNo
			    unsigned int voxelNo = 0;
			    for (size_t i=0; i<temp_bixel.voxelNo_.size(); i++) {
				nEntries_per_voxel[temp_bixel.voxelNo_[i]]++;
			    }
			}
			else {
			    // Store the number of entries for the bixelNo
			    nEntries_per_bixel[bixelNo] = temp_bixel.nVox_;
			}
		    }
		    else {
		      cout << "Bixel " << bixel_pos << " with subscripts " << bixel_sub;
		      cout << " is found for the second time." << endl;
		      cout << "Either the bixel is duplicated (not allowed) " << endl;
		      cout << "or there is a programming error. " << endl;
		      throw DijFileParser::exception("Corrupt files");
		    }
		}
	    }
	    // The dij_file goes out of scope here and automatically closes

	    // check if all bixels were found
	    for(unsigned int iBixel = starting_bixelNo_[iBeam]; 
		iBixel<starting_bixelNo_[iBeam]+nBixels_in_beam_[iBeam]; 
		iBixel++) {
		if( !bixel_grid[geometry_->convert_to_BixelIndex(iBixel).index_] ) {
		    cout << "Not all bixels in the bixel vector were found in the Dij file." << endl;
		    cout << "Could not find bixel " << iBixel << " with energy " 
			 << geometry_->get_bixel_energy(iBixel) << " and position (x="
			 << geometry_->get_bixel_position_x(iBixel) << ", y="
			 << geometry_->get_bixel_position_y(iBixel) << ")" << endl;
		    throw DijFileParser::exception("Bixel not found in Dij");
		}
	    }
        }
    }

    // Allocate space in parent class
    this->set_nBixels(bixel_vector.get_nBixels());
    this->set_nVoxels(geometry->get_nVoxels());

    this->reserve_Dij(total_nDij);
    if(transpose) {
        this->set_nEntries_per_voxel(nEntries_per_voxel);
    }
    else {
        this->set_nEntries_per_bixel(nEntries_per_bixel);
    }

    // Now really read the files
    for(size_t iInstance = 0; iInstance < nInstances_; iInstance ++) {
        unsigned int last_beamNo_in_instance = starting_beamNo_[iInstance]
            + nBeams_in_instance_[iInstance];
        for(size_t iBeam = starting_beamNo_[iInstance];
                iBeam < last_beamNo_in_instance; iBeam++) {

            // convert beam# to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
            if(iInstance == 0) {
                dij_file_name = dij_file_root + "_" + string(cbeam) + ".dij";
            }
            else {
                dij_file_name = dij_file_root + "_" + string(cinstance) 
                    + "_" + string(cbeam) + ".dij";
            }

	    // Open file (throws exception if openning fails)
            DijFileParser dij_file(dij_file_name);

	    // initialize the bixel grid
	    bixel_grid.resize(geometry_->get_beam_bixel_nPoints(iBeam));
	    fill(bixel_grid.begin(),bixel_grid.end(),false);

            // Read the beam information first to calculate the total # of Dij entries
            unsigned int nDij = 0;
	    while(!dij_file.at_end()) {

		float epsilon = 0.001;

		// read the bixel from the file
                dij_file.read_next_bixel(temp_bixel);

		// create bixel position
		bixel_pos = Bixel_Position(temp_bixel.spot_x_,
					   temp_bixel.spot_y_,
					   temp_bixel.energy_);

		// convert to subscripts in the bixel grid
		bixel_sub = geometry_->convert_to_Bixel_Subscripts(bixel_pos,iBeam);

		// check if this bixel is contained in the bixel vector
		if(geometry_->is_valid_bixel(bixel_sub,iBeam)) {

		    // get grid index
		    bixel_index = geometry_->convert_to_BixelIndex(bixel_sub,iBeam);

		    // get bixelNo
		    bixelNo = geometry_->get_bixelNo(bixel_index,iBeam);

		    // check if it has not been read already
		    if(!bixel_grid[geometry_->convert_to_BixelIndex(bixel_sub,iBeam).index_]) {

			// tag the bixel grid point
			bixel_grid[bixel_index.index_] = true;

			nDij += temp_bixel.nVox_;

			// Read each Dij entry
			for (int i=0; i<temp_bixel.nVox_; i++) {
			    float influence = ((float)temp_bixel.value_[i]) 
				* dose_scale_coeficient_[iBeam];
			    add_Dij_element(bixelNo, temp_bixel.voxelNo_[i], influence);
			}
		    }
		    else {
		      cout << "Bixel " << bixel_pos << " with subscripts " << bixel_sub;
		      cout << " is found for the second time." << endl;
		      cout << "Either the bixel is duplicated (not allowed) " << endl;
		      cout << "or there is a programming error. " << endl;
		      throw DijFileParser::exception("Corrupt files");
		    }
		}
	    }

            cout << "Beam " << iBeam + 1 
                << " Number of pencil beams:  " << nBixels_in_beam_[iBeam]
                << " Number of Dij-entries:  " << nDij << endl;

            // The dij_file goes out of scope here and automatically closes
        }
    }

    // double-check whether Dij matrix is sorted
    if(confirm_sorted()) {
        is_sorted_ = true;
    }
    else {
        is_sorted_ = false;
    }
}



/**
 * Write Dij-matrices to files
 *
 * @todo Change this constructor to use the DijFileParser class
 * @todo Make this work for big-endian systems.
 */
void KonradDoseInfluenceMatrix::write_dij_files(string dij_file_root)
{

    unsigned int Nj, Nvox, temp_int;
    float temp_float;
    short sDummy;
    string dij_file_name;
    char temp_cstr[256];
    FILE *fOutput;

    // check for correct size of variables
    if (   (sizeof(float) != 4)
            || (sizeof(int)   != 4)
            || (sizeof(short) != 2) ) {
        cout << "Can not write data: wrong byte size";
        exit (-1);
    }

    // Check if Dij is transposed.  If so, we'll need to transpose it back
    ensure_not_transposed();

    // Initialize pointer to first bixel
    DoseInfluenceMatrix::iterator dij_iter(this->begin());
    for(size_t iBeam = 0; iBeam < nBeams_; iBeam++) {

        // Derive file name
        sprintf(temp_cstr,"_%d.dij",(int)iBeam+1); // convert beam# to string
        dij_file_name = dij_file_root + string(temp_cstr);

        fOutput = fopen(dij_file_name.c_str(),"wb");
        if (fOutput == NULL) {
            cout << "Can't open file: " << dij_file_name << endl;
            exit(-1);
        }

        cout << "Saving Dij data to file: " << dij_file_name << endl;

        // write the header

        // gantry, table, collimator angles
        temp_float = geometry_->get_gantry_angle()[iBeam];
        fwrite(&temp_float,sizeof(float),1,fOutput);
        temp_float = geometry_->get_table_angle()[iBeam];
        fwrite(&temp_float,sizeof(float),1,fOutput);
        temp_float = geometry_->get_collimator_angle()[iBeam];
        fwrite(&temp_float,sizeof(float),1,fOutput);

        // bixel spacing
        temp_float = geometry_->get_bixel_grid_dx()[iBeam];
        fwrite(&temp_float,sizeof(float),1,fOutput);
        temp_float = geometry_->get_bixel_grid_dy()[iBeam];
        fwrite(&temp_float,sizeof(float),1,fOutput);

        // voxel spacing
        temp_float = geometry_->get_voxel_dx();
        fwrite(&temp_float,sizeof(float),1,fOutput);
        temp_float = geometry_->get_voxel_dy();
        fwrite(&temp_float,sizeof(float),1,fOutput);
        temp_float = geometry_->get_voxel_dz();
        fwrite(&temp_float,sizeof(float),1,fOutput);

        // voxel cube dimensions
        temp_int = geometry_->get_voxel_nx();
        fwrite(&temp_int,sizeof(int),1,fOutput);
        temp_int = geometry_->get_voxel_ny();
        fwrite(&temp_int,sizeof(int),1,fOutput);
        temp_int = geometry_->get_voxel_nz();
        fwrite(&temp_int,sizeof(int),1,fOutput);


        // number of pencil beams
        Nj = nBixels_in_beam_[iBeam];
        fwrite(&Nj,sizeof(int),1,fOutput);

        // dose scale factor
        fwrite(&dose_scale_coeficient_[iBeam],sizeof(float),1,fOutput);

        // write the information about each bixel in beam
        for(size_t iBeamlet = starting_bixelNo_[iBeam];
                iBeamlet - starting_bixelNo_[iBeam] < nBixels_in_beam_[iBeam];
                iBeamlet ++) {

            // write beam header
            //       energy
            temp_float = geometry_->get_bixel_energy(iBeamlet);
            fwrite(&temp_float,sizeof(float),1,fOutput);
            //       bixel position
            Bixel_Position temp_pos = geometry_->get_bixel_position(iBeamlet);
            temp_float = temp_pos.x_;
            fwrite(&temp_float,sizeof(float),1,fOutput);
            temp_float = temp_pos.y_;
            fwrite(&temp_float,sizeof(float),1,fOutput);

            //       number of dose points
            // DoseInfluenceMatrix::iterator temp_iter = dij_iter; Nvox = 0;
            // while(temp_iter.not_at_end() && temp_iter.get_bixelNo() == iBeamlet) 
            // // {
            // Nvox++;
            // temp_iter++;
            // }
            Nvox = get_nEntries_per_bixel()[iBeamlet];
            fwrite(&Nvox,sizeof(int),1,fOutput);

            // write the Dij entries
            while(dij_iter.not_at_end() && dij_iter.get_bixelNo() == iBeamlet) {
                // voxel address
                unsigned int voxelNo = dij_iter.get_voxelNo();
                fwrite(&voxelNo,sizeof(int),1,fOutput);
                // scaled dose value
                sDummy = (short)((dij_iter.get_influence()
                            / dose_scale_coeficient_[iBeam]) + 0.5);
                fwrite(&sDummy,sizeof(short),1,fOutput);
                dij_iter++;
            }
        }

        fclose(fOutput);
    }
}

/*
void KonradDoseInfluenceMatrix::calculate_instance_dose(
    size_t instanceNo,
    DoseVector & dose_vector,
    BixelVector & bixel_vector) const
{
    if(starting_beamNo_.size()==1) {
	// staitic beam
	dose_forward(dose_vector,bixel_vector,instanceNo);
    }
    else {
	unsigned int j, i;

	size_t starting_beamNo, last_beamNo;
	size_t starting_bixelNo, end_bixelNo;
	
	starting_beamNo = starting_beamNo_[instanceNo];
	last_beamNo = starting_beamNo + nBeams_in_instance_[instanceNo] - 1;
	starting_bixelNo = starting_bixelNo_[starting_beamNo];
	end_bixelNo = starting_bixelNo_[last_beamNo] + nBixels_in_beam_[last_beamNo];
	
	DoseInfluenceMatrix::iterator iter(*this);
	while(iter.not_at_end()) {
	    i = iter.get_voxelNo();
	    j = iter.get_bixelNo();
	    if(j >= starting_bixelNo && j < end_bixelNo) {
		dose_vector.add_dose(i, bixel_vector.get_intensity(j) 
				     * iter.get_influence());
	    }
	    
	    ++iter;
	}
    }
}
*/


/**
 * Replace the Dij-matrix entries with the ones obtained with deformation
 * matrices.
 */
/*
void KonradDoseInfluenceMatrix::replace_Dij(VoxelGrid *voxel_ptr, BixelGrid *bixel_ptr,
                           int instanceNo, int beamNo)
{
  int Nj, i0, i1, Nvox, ivox;
  vector<int> iBeamStart;
  int iOffset;
  float fScale;
  string OutFile = "outfile.dat";
  string InFile = "infile.dat";
  string SystemCommand;
  FILE *fInput=NULL;

  // number of beam elements
  Nj = bixel_ptr->get_Nj();
  iBeamStart.resize(Nj);
  iBeamStart[0]=0;
  for (i0=1; i0<Nj; i0++)
  {
    iBeamStart[i0]=iBeamStart[i0-1]+nonzero_entries_[i0-1];
  }

  fScale = 1./fAbsoluteDoseCoeficient_;

  // remove the input file
  SystemCommand = "/bin/rm -f " + InFile;
  system(SystemCommand.c_str());

  for (i0=0; i0<Nj; i0++)
    //for (i0=0; i0<1; i0++)
  {

    Nvox = nonzero_entries_[i0];
    cout << "Beam " << beamNo << " element " << i0
         << " (of " << Nj << ") with " <<  Nvox << " voxels" << endl;
    iOffset = iBeamStart[i0];

    // initialize the dose matrix
    voxel_ptr->reset_dose();

    // write the Dij entries for this beam to the dose cube
    for (i1=0; i1<Nvox; i1++) {
      voxel_ptr->set_voxel_dose(Dij_voxel_[iOffset+i1],Dij_dose_[iOffset+i1]);
    }

    voxel_ptr->write_dose2CTcube(sizeof(short), fScale, OutFile);

    // this is where Eike's voxel displacement should be done
    SystemCommand = "./transform_dij.pl " + OutFile + " " + InFile + " &";
    system(SystemCommand.c_str());

    // wait until the file is there
    i1=0;
    int iDelay = 1; // seconds of delay
    while (fInput == NULL) {
      sleep(iDelay);
      i1 += iDelay;
      //      cout << "Waiting for the file ..." << endl;
      fInput = fopen(InFile.c_str(),"rb");
    }
    fclose(fInput);
    fInput = NULL;
    cout << "Transformation took " << i1 << " seconds" << endl;

    // read the deformed data back to the dose cube
    voxel_ptr->read_CTcube2dose(sizeof(short), fScale, InFile);

    // write the dose cube entries into the Dij
    for (i1=0; i1<Nvox; i1++)
    {
      ivox = Dij_voxel_[iOffset+i1];
      Dij_dose_[iOffset+i1] = voxel_ptr->get_voxel_dose(ivox);
    }

    // remove the file
    SystemCommand = "/bin/rm -f " + InFile;
    system(SystemCommand.c_str());
  }

}
*/

/*
void KonradDoseInfluenceMatrix::scale_instances(
    const vector<float> scaling_factors)
{
  assert(scaling_factors.size() >= nInstances_);
  unsigned int j;
  float x;

  // Assure that all instances have the same number of bixels
  vector<size_t> nBixels_in_instance(nInstances_,0);
  for(size_t iInstance = 0; iInstance < nInstances_; iInstance ++) {
    for(size_t iBeam = starting_beamNo_[iInstance];
       	iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance];
	iBeam++) {
      nBixels_in_instance[iInstance] += nBixels_in_beam_[iBeam];
    }

    assert(nBixels_in_instance[iInstance] == nBixels_in_instance[0]);
  }
  
  DoseInfluenceMatrix::non_const_iterator iter(*this);
  while(iter.not_at_end()) {
    j = iter.get_bixelNo();
    x = iter.get_influence();
    iter.set_influence(x * scaling_factors[j / nBixels_in_instance[0]]);
    ++iter;
  }
}
*/
///**
// * Currently broken function
// * @todo make this work with the new DoseInfluenceMatrix storage system
// */
/*
void KonradDoseInfluenceMatrix::combine_instances(
    const vector<float> scaling_factors)
{
  assert(false); // Make sure this is not run.

  assert(scaling_factors.size() >= nInstances_);
  unsigned int j;
  float x;

  // Assure that all instances have the same number of bixels
  vector<size_t> nBixels_in_instance(nInstances_,0);
  for(size_t iInstance = 0; iInstance < nInstances_; iInstance ++) {
    for(size_t iBeam = starting_beamNo_[iInstance];
       	iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance];
        iBeam++) {
      nBixels_in_instance[iInstance] += nBixels_in_beam_[iBeam];
    }

    assert(nBixels_in_instance[iInstance] == nBixels_in_instance[0]);
  }
  
  DoseInfluenceMatrix::non_const_iterator iter(*this);
  while(iter.not_at_end()) {
    j = iter.get_bixelNo();
    x = iter.get_influence();
    // iter.set_bixelNo(j % nBixels_in_instance[0]);
    iter.set_influence(x * scaling_factors[j / nBixels_in_instance[0]]);
    ++iter;
  }

  // this->sort_Dij();
}
*/
