/**
 * @file SortableTripple.hpp
 * SortableTripple Class header
 *
 * $Id: SortableTriple.hpp,v 1.1 2007/12/04 21:35:51 bmartin Exp $
 */

#ifndef SORTABLETRIPLE_HPP
#define SORTABLETRIPLE_HPP

/**
 * Class to hold three objects.  Allows sorting according to
 * first object
 */
template<class T_v1, class T_v2, class T_v3>
class SortableTriple
{
public:
    /// Default constructor
    SortableTriple()
        : value1_(),
        value2_(),
        value3_()
    {
    };

    SortableTriple(T_v1 value1, T_v2 value2, T_v3 value3)
        : value1_(value1),
        value2_(value2),
        value3_(value3)
	{
	};

    friend bool operator< (
	const SortableTriple& lhs,
       	const SortableTriple& rhs) {
      return lhs.value1_ < rhs.value1_;
    };

    // attributes
    T_v1 value1_;
    T_v2 value2_;
    T_v3 value3_;
};


#endif
