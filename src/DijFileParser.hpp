/**
 * @file DijFileParser.hpp
 * DijFileParser class header
 */

#ifndef DIJFILEPARSER_HPP
#define DIJFILEPARSER_HPP

class DijFileParser;

#include <iostream>
#include <fstream>
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>


#include "Geometry.hpp"

static const float EPSILON = 0.001;



/**
 * Class DijFileParser reads a Konrad formatted .dij file.
 *
 *
 * <h2>The Dij file format:</h2>
 * It'a a binary file with no "separators" between the entries.  All data is 
 * stored in little-endian format.
 * <ul>
 * <li> File header:
 * <ul>
 * <li> float a_g  - Gantry angle
 * <li> float a_t  - Table angle
 * <li> float a_c  - Collimator angle
 * <li> float  dx_b  - bixel dimensions (in mm)
 * <li> float  dy_b  - bixel dimensions (in mm)
 * <li> float dx  - voxel dimensions (in mm)
 * <li> float dy  - voxel dimensions (in mm)
 * <li> float dz  - voxel dimensions (in mm)
 * <li> int Nx - dose cube dimensions (number of voxels)
 * <li> int Ny - dose cube dimensions (number of voxels)
 * <li> int Nz - dose cube dimensions (number of voxels)
 * <li> int Npb - number of pencil beams (bixels) used for this field
 * <li> float  Dose-scalefactor - since the doses are stored as short
 *      integers, this is the conversion factor to the absolute dose.  For the 
 *      best resolution, this will be:
 *                      max(Dij_entry)/max(short) = max(Dij_entry)/(2^15-1)
 * </ul>
 * <li> <b>File body</b>. For each bixel, there is an entry with the following 
 * parts:
 * <ul>
 * <li> Beam header:
 * <ul>
 * <li> float energy
 * <li> float spot_x
 * <li> float spot_y - position of the bixel with respect to the
 *      central axis of the field (in mm)
 * <li> int Nvox - number of voxels with non-zero dose from this beamlet
 * </ul>
 * <li> For each Nvox:
 * <ul>
 * <li> int VoxelNumber - voxel ID in the cube (between 0 and Nx*Ny*Nz-1)
 * <li> short Value - multiply this by Dose-scalefactor to get the dose
 *      deposited by this beamlet to the voxel VoxelNumber, assuming the 
 *      beamlet weight of 1
 * </ul>
 * </ul>
 * </ul>
 * Of course, the index VoxelNumber is related to the voxel position in the 
 * cube.  KonRad uses a right-handed Cartesian coordinate system for the dose 
 * cube:
 * <p>
 * For gantry at 0, in beam's eye view, x is pointing to the right, y == up,
 * and z == towards the gantry.  So , the origin of the cube is in the distal 
 * lower left corner.
 * <p>
 * For a voxel at (x,y,z) w/ respect to the cube origin,  the VoxelNumber =
 *                                      x/dx + ( y/dy + z/dz*Ny )*Nx 
 * 
 */
class DijFileParser
{

    public:
        //
        // Sub-classes
        //

        // Class DijFileParser::exception is thrown when there is an errer 
        // reading a dij file.
        class exception;

        /**
         * Class DijFileParser::header contains all of the information from the 
         * header inside a dij file.
         */
        class header
        {
            public:
                // Default constructor
                header();

		// Constructor from geometry class
                header(const Geometry & geom, unsigned int beamNo);

		// Operator overrides
		bool operator==(const DijFileParser::header& rhs) const;
		friend std::ostream& operator<< (std::ostream& o,
			const DijFileParser::header& rhs);

                float a_g_;  ///< Gantry angle
                float a_t_;  ///< Table angle
                float a_c_;  ///< Collimator angle
                float dx_b_; ///< bixel dimensions (in mm)
                float dy_b_; ///< bixel dimensions (in mm)
                float dx_;   ///< voxel dimensions (in mm)
                float dy_;   ///< voxel dimensions (in mm)
                float dz_;   ///< voxel dimensions (in mm)
                int   nx_;   ///< dose cube dimensions (number of voxels)
                int   ny_;   ///< dose cube dimensions (number of voxels)
                int   nz_;   ///< dose cube dimensions (number of voxels)
                int   npb_;  ///< number of pencil beams (bixels) used for
                             ///< this field
                float scalefactor_; ///<  the conversion factor to the
                                    ///<  absolute dose
        };

        /**
         * Class DijFileParser::bixel contains all of the information about a
         * bixel inside a dij file.
         */
        class bixel
        {
            public:
                // Beamlet header
                float energy_;  ///< Bixel energy in MeV
                float spot_x_;  ///< position of the bixel in the field (in mm)
                float spot_y_;  ///< position of the bixel in the field (in mm)
                int   nVox_;  ///< number of voxels with dose from this beamlet

                // Beamlet data
                vector<int> voxelNo_; ///< voxel ID (between 0 and Nx*Ny*Nz-1)
                vector<short> value_; ///< multiply this by scalefactor to get
                                      ///< the influence
        };


        //
        // Constructors
        //

        // Open a file for reading and read the header
        DijFileParser(std::string file_name);

        // Open a file for writing and write the header
        DijFileParser(std::string file_name, DijFileParser::header head);

        //
        // Destructor
        //

        ~DijFileParser();

        //
        // Functions for reading dij files
        //

        // Get the header for the file
        DijFileParser::header get_header() const;

        // Read the next bixel
        void read_next_bixel(DijFileParser::bixel & bixel);

        // Check if the parser is at the end of the bixels
        bool at_end() const;

        //
        // Functions for writing dij files
        //

        // Write the next bixel
        void write_next_bixel(const DijFileParser::bixel & bixel);

    private:
        /// Make sure default constructor is never used
        DijFileParser();

        // Read a binary little-endian integer from a stream
        unsigned int read_binary_int(std::istream & input);

        // Read a binary little-endian integer from a stream
        unsigned short read_binary_short(std::istream & input);

        // Read a binary little-endian integer from a stream
        float read_binary_float(std::istream & input);

        // Write a binary little-endian integer to a stream
        void write_binary_int(std::ostream & output, int num);

        // Write a binary little-endian integer to a stream
        void write_binary_short(std::ostream & output, short num);

        // Write a binary little-endian integer to a stream
        void write_binary_float(std::ostream & output, float num);

	// Test if the compiled endianness is correct
	void test_endianness() const;


        //
	// Variables
        //

        std::string file_name_;

        //
        // Variables for reading
        //

        /// True if the file is opened for reading
        bool reading_;

        /// Stream to read from
        std::auto_ptr<std::ifstream> input_file_;

        /// Number of bixels read so far
        int nBixels_read_;

        //
        // Variables for writing
        //

        /// True if the file is opened for writing
        bool writing_;

        /// Stream to write to
        std::auto_ptr<std::ofstream> output_file_;

        /// Number of bixels written so far
        int nBixels_written_;

        // Header variables
        DijFileParser::header header_;

};

/*
 * Classes
 */

/**
 * Class DijFileParser::exception is thrown when there is an errer reading 
 * a dij file.
 */
class DijFileParser::exception: public std::exception
{
    public:
        exception(string reason)
        : reason_("dij file failed: " + reason) {};

        ~exception() throw() {};

        virtual const char* what() const throw()
        { return reason_.c_str(); };

    private:
        const string reason_;
};


/*
 * Inline Functions
 */


/**
 * Read a binary little-endian integer from a stream
 */
inline
unsigned int DijFileParser::read_binary_int(std::istream & input)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        // Need to rearrange bytes
        char SS[4], ST[4];
        input.read(SS, 4);
        ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
        return *( (unsigned int *) ST);
    } else {
        // No need to rearrange bytes
        unsigned int temp;
        input.read((char*) &temp, sizeof(int));
        return temp;
    }
}


/**
 * Read a binary little-endian short integer from a stream
 */
inline
unsigned short DijFileParser::read_binary_short(std::istream & input)
{

#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        // Need to rearrange bytes
        char SS[2], ST[2];
        input.read(SS, 2);
        ST[0] = SS[1];  ST[1] = SS[0];
        return *( (unsigned short *) ST);
    } else {
        // No need to rearrange bytes
        unsigned short temp;
        input.read((char*) &temp, sizeof(short));
        return temp;
    }
}


/**
 * Read a binary little-endian float from a stream
 */
inline
float DijFileParser::read_binary_float(std::istream & input)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        char SS[4], ST[4];
        input.read(SS, 4);
        ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
        return *( (float *) ST);
    } else {
        float temp;

        input.read((char*) &temp, sizeof(float));

        return temp;
    }
}


/**
 * Write a binary little-endian integer to a stream
 */
inline
void DijFileParser::write_binary_int(
        std::ostream & output,
        int num)
{

#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        // Need to rearrange bytes
        char *SS = (char*) &num;
        char ST[4];
        ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
        output.write(ST, 4);
    } else {
        // No need to rearrange bytes
        output.write((char*) &num, 4);
    }
}


/**
 * Write a binary little-endian short integer to a stream
 */
inline
void DijFileParser::write_binary_short(
        std::ostream & output,
        short num)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        char *SS = (char*) &num;
        char ST[2];
        ST[0] = SS[1];  ST[1] = SS[0];
        output.write(ST, 2);
    } else {
        output.write((char*) &num, 2);
    }
}


/**
 * Write a binary little-endian float to a stream
 */
inline
void DijFileParser::write_binary_float(
        std::ostream & output,
        float num)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
        char *SS = (char*) &num;
        char ST[4];
        ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
        output.write(ST, 4);
    } else {
        output.write((char*) &num, 4);
    }
}


/**
 *  Get the header for the file
 */
inline
DijFileParser::header DijFileParser::get_header() const
{
    return header_;
}


/**
 * Check if the parser is at the end of the bixels
 */
inline
bool DijFileParser::at_end() const
{
    return nBixels_read_ == header_.npb_;
}



#endif
