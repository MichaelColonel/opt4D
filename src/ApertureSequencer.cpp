/**
 * @file ApertureSequencer.cpp
 * ApertureSequencer Class implementation.
 *
 * </pre>
 */

#include <cmath>
#include <memory>

#include "ApertureSequencer.hpp"
#include "BixelVector.hpp"
#include "DirectParameterVector.hpp"
#include "ParameterVectorDirection.hpp"
#include "DeliveryMachineModel.hpp"


/**
 * Constructor: initialize variables.
 *
 * @param thisplan The plan to be optimized.
 */
ApertureSequencer::ApertureSequencer( Plan* thisplan )
        : the_plan_(thisplan),  options_() { };


/**
 * Destructor.
 */
ApertureSequencer::~ApertureSequencer() { };


/**
 * Sequence the plan apertures by calling the appropriate sequencer function.
 */
void ApertureSequencer::use_sequencer() {
    if ( the_plan_->get_parameter_vector()->getVectorType() != BIXEL_VECTOR_TYPE ) {
        throw ApertureSequencer::exception("Already converted or unknown parameter vector type");
    }

    switch ( options_.seqType_ ) {
        case VMAT_DUMMY:
                use_vmat_dummy_sequencer();
            break;
        default:
            throw std::logic_error("Invalid enum value.");
    }
};


/**
 * Sequence the plan apertures with simple square fields disregarding the actual bixel values.
 */
void ApertureSequencer::use_vmat_dummy_sequencer() {
    // Create a working copy of the bixel vector
    BixelVector * bixVect = static_cast<BixelVector*>( the_plan_->get_parameter_vector().release() );

    DeliveryMachineModel dmm(options_.machine_file_);

    unsigned int numDijBeams = the_plan_->get_Geometry()->get_gantry_angle().size();
    unsigned int cpOverSingleArc = numDijBeams * options_.cpPerBeam_ / options_.nFields_;
    vector<unsigned int> cpPerArc( options_.nFields_, cpOverSingleArc );

    // Create a parameter vector with the appropriate number of control points.
    // Have leaf, jaw and gantry parameters for every control point.
    DirectParameterVector * directVect = new DirectParameterVector(
            *bixVect, dmm, the_plan_->get_Geometry(), cpPerArc,
            true, true, true, true, false, false, true, true, false, false, false, false, false, false );
    // Two boolean values setting parameters per field and parameters per control point for each of:
    //          leaves, jaws, energy, gantry angle, collimator angle, couch angle, dose rate.

    // Construct a vector of the desired changes to the parameters so that all the changes can be
    // processed at once. This avoids problems with delivery constraints that may be incountered
    // if the changes were attempted incrementally.
    ParameterVectorDirection * changeVector = new ParameterVectorDirection( *directVect );

    // For each arc in plan distribute control points evenly around a full arc
    for ( unsigned int arcInd = 0; arcInd<options_.nFields_; arcInd++) {
        for ( unsigned int cpInd = 0; cpInd < cpOverSingleArc; cpInd++ ) {
            // Set gantry angle.
            int pNum = directVect->get_PInd_Gantry(cpInd);
            float gaAngle = fmod( (360.0f * cpInd) / cpOverSingleArc, 360.0f ) + dmm.getGantry()->getMinValue();
            gaAngle = directVect->get_RoundToValidValue( directVect->get_PInd_Gantry(cpInd), gaAngle );
            changeVector->set_value( pNum, gaAngle );

            // Set control point weight to 20.0 MU
            changeVector->set_value( directVect->get_PInd_Weight(cpInd), 20.0f );
        }
    }

    // Apply changes to direct parameter vector
    cout << "Applying parameter value changes : " << flush;
    try {
        directVect->set_value( *changeVector );
    } catch (DirectParameterVector_exception e) {
        cout << "Adjusting to ensure deliverability : " << flush;
        directVect->makePossible();
    }
    cout << "Done" << endl;

    // Leave leaf and jaw positions as the initial values
    // commented code below illustrates how to position the jaws and leaf pairs 15-24 for the first control point
    //    changeVector = directVect;
    //    unsigned int cpInd = 0;
    //    changeVector->set_value( directVect->get_PInd_XJaw(cpInd,0), 10.0f );
    //    changeVector->set_value( directVect->get_PInd_XJaw(cpInd,1), -10.0f );
    //    changeVector->set_value( directVect->get_PInd_YJaw(cpInd,0), 10.0f );
    //    changeVector->set_value( directVect->get_PInd_YJaw(cpInd,1), -10.0f );
    //
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,15), 5.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,16), 10.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,17), 15.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,18), 20.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,19), 25.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,20), 30.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,21), 35.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,22), 40.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,23), 45.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,24), 50.0f );
    //
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,55), -50.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,56), -45.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,57), -40.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,58), -35.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,59), -30.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,60), -25.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,61), -20.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,62), -15.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,63), -10.0f );
    //    changeVector->set_value( directVect->get_PInd_XLeaf(cpInd,64), -5.0f );
    //    directVect->set_value( *changeVector );

    // Swap plan parameter vector with new direct parameter vector
    the_plan_->parameter_vector_.reset( new DirectParameterVector( *directVect ) );
    if ( the_plan_->get_parameter_vector()->is_impossible() ) {
        cout << "Warning parameter set is not possible to deliver !!" << endl;
    } else {
        cout << "Parameter set can be delivered successfully." << endl;
    }

    delete changeVector;
	delete bixVect;
	delete directVect;
};


/**
 * Print options to stream
 */
std::ostream& operator<< (std::ostream& o, const ApertureSequencer::options& opt) {
    o << "Aperture Sequencer : machine description file = " << opt.machine_file_ << "\n";
    o << "Aperture Sequencer : optimize leaf positions = " << opt.optMlc_ << "\n";
    o << "Aperture Sequencer : optimize jaw positions = " << opt.optJaw_ << "\n";
    o << "Aperture Sequencer : Type = " << opt.seqType_ << "\n";
    o << "Aperture Sequencer : nFields_ = " << opt.nFields_ << "\n";
    o << "Aperture Sequencer : cpPerBeam_ = " << opt.cpPerBeam_ << "\n";
    return o;
};
