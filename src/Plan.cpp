/* $Id: Plan.cpp,v 1.112 2008/05/30 14:37:46 unkelbac Exp $ */

// #include <stdio.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <exception>
#include <ctime>

#include "Plan.hpp"

// Objectives
#include "ExpectedValueMetaObjective.hpp"
#include "MeanSquaredErrorObjective.hpp"
#include "SquaredMeanObjective.hpp"
#include "DvhCoverageObjective.hpp"
#include "FastMeanObjective.hpp"
#include "GEudObjective.hpp"
#include "TumorEUDObjective.hpp"
#include "VoxelMaxDoseObjective.hpp"
#include "MaxMinOrMinMaxObjective.hpp"
#include "LinearMeanObjective.hpp"
#include "AbsoluteErrorObjective.hpp"
#include "DoseConstraint.hpp"
#include "DvhObjective.hpp"
#include "VoxelDoseConstraint.hpp"
#include "RobustifiedDoseConstraint.hpp"
#include "MeanConstraint.hpp"
#include "MeanDoseObjective.hpp"
#include "BixelPositivityConstraint.hpp"
#include "CellSurvivalObjective.hpp"
#include "ExpectedQuadraticObjectiveTensor.hpp"
#include "ExpectedQuadraticObjectiveIntegrate.hpp"
#include "SfudHomogeneityObjective.hpp"
#include "ConstrainedObjective.hpp"
#include "ConstrainedMetaObjective.hpp"

#include "KonradBixelVector.hpp"
#include "KonradSquareRootBixelVector.hpp"
#include "KonradDoseInfluenceMatrix.hpp"
#include "DirectParameterVector.hpp"
#include "DaoVector.hpp"

// Uncertainty Models
#include "RangeSetupUncertaintyModel.hpp"
#include "SetupUncertaintyModel.hpp"
#include "LETModel.hpp"

// Other helpful classes
#include "VoiParser.hpp"
#include "VvParser.hpp"

txtFile_exception plan_pln_ex;


/**
 * @file Plan.cpp
 * Code for Plan class
 *
 * $Id: Plan.cpp,v 1.112 2008/05/30 14:37:46 unkelbac Exp $
 *
 * <pre>
 * ver 0.9.2     AT,TB   Apr 05, 2004    implemented "dij averaging" over
 *                                       instances
 * ver 0.9.0     TB      Mar 03, 2004    move function adapted, other minor
 *                                       changes
 * ver 0.8.10    AT      Feb.26, 2004    parameters passed from the command line
 * ver 0.5       TB      Jan 03, 2004    creation
 *
 * AT: Alexei Trofimov
 * TB: Thomas Bortfeld
 * Massachusetts General Hospital, Department of Radiation Oncology
 * </pre>
 */



/**
 * Constructor: create a plan based on plan description file.
 * <p>
 * The plan description file contains information about the plan and the
 * prescription.
 * <p>
 * For example, if KonRad_file is "default" and there are two beams, the
 * constructor will read the files:
 * <ul>
 *   <li>default.ctatts
 *   <li>default.dif
 *   <li>default.voi
 *   <li>default_1.bwf default_2.bwf
 *   <li>default_1.dij default_2.dij
 *   <li>default_1.stf default_2.stf
 * </ul>
 * If there are two instances, the following files will also be read:
 * <ul>
 *   <li>default.dif
 *   <li>default_2_1.bwf default_2_2.bwf
 *   <li>default_2_1.dij default_2_2.dij
 *   <li>default_2_1.stf default_2_2.stf
 * </ul>
 *
 * @param plan_file_name Plan file to read.
 * @param options Options to use with the plan
 * @todo update pln_file_format.txt
 */
Plan::Plan( std::string plan_file_name,  Plan::options options )
        : plan_file_root_(""),  plan_description_(""),  nInstances_(0),  nBeams_(0),  nNonair_voxels_(0),  vois_(),
        parameter_vector_(),  backup_parameter_vector_(),  dij_(NULL),  instance_scale_factor_(),
        objectives_(),  meta_objectives_(),  geometry_(),  options_(options),  dose_delivery_model_(NULL) {
    std::cout << "In Plan constructor." << endl;

    /*
     * Prepare attributes for pln file parsing
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     */

    /*
     * Normal attributes
     */
    vector<string> required_attributes;
    vector<string> optional_attributes;
    map<string,string> default_values;

    // Required attributes
    required_attributes.push_back("title");
    required_attributes.push_back("nBeams");
    required_attributes.push_back("pln_file_version");

    // Optional attributes
    optional_attributes.push_back("description");
    optional_attributes.push_back("nInstances");
    default_values["nInstances"] = "1";
    optional_attributes.push_back("DVH_bin_size");
    std::ostringstream s1;
    s1 << options_.dvh_bin_size_;
    default_values["DVH_bin_size"] = s1.str();
    optional_attributes.push_back("parameter_type");
    default_values["parameter_type"] = "bixel";
    optional_attributes.push_back("plan_file_root");
    optional_attributes.push_back("bwf_file_root");
    optional_attributes.push_back("stf_file_root");
    optional_attributes.push_back("dij_file_root");
    optional_attributes.push_back("dif_file_name");
    optional_attributes.push_back("ct_file_name");
    optional_attributes.push_back("ctatts_file_name");
    optional_attributes.push_back("voi_file_name");
    optional_attributes.push_back("vv_file_name");

    // Uncertainty attributes
    optional_attributes.push_back("Dose_Delivery_Model");
    default_values["Dose_Delivery_Model"] = "standard";

    optional_attributes.push_back("Uncertainty_Range_nRanges");
    default_values["Uncertainty_Range_nRanges"] = "0";
    optional_attributes.push_back("Uncertainty_Range_max");
    default_values["Uncertainty_Range_max"] = "0";
    optional_attributes.push_back("Uncertainty_Range_width");
    default_values["Uncertainty_Range_width"] = "0";
    optional_attributes.push_back("Uncertainty_Range_sigma_file_root");
    optional_attributes.push_back("Uncertainty_Range_correlation");
    default_values["Uncertainty_Range_correlation"] = "within_beam";
    optional_attributes.push_back("Uncertainty_Range_aux_dij_file_root");
    default_values["Uncertainty_Range_aux_dij_file_root"] = "auxiliaries";
    optional_attributes.push_back("Uncertainty_Range_read_dijs");
    optional_attributes.push_back("Uncertainty_Range_write_dijs");
    optional_attributes.push_back("Uncertainty_Range_dWEL");

    optional_attributes.push_back("Uncertainty_Setup_sigmaX");
    default_values["Uncertainty_Setup_sigmaX"] = "0";
    optional_attributes.push_back("Uncertainty_Setup_sigmaY");
    default_values["Uncertainty_Setup_sigmaY"] = "0";
    optional_attributes.push_back("Uncertainty_Setup_sigmaZ");
    default_values["Uncertainty_Setup_sigmaZ"] = "0";
    optional_attributes.push_back("Uncertainty_RandomSetup_sigmaX");
    default_values["Uncertainty_RandomSetup_sigmaX"] = "0";
    optional_attributes.push_back("Uncertainty_RandomSetup_sigmaY");
    default_values["Uncertainty_RandomSetup_sigmaY"] = "0";
    optional_attributes.push_back("Uncertainty_RandomSetup_sigmaZ");
    default_values["Uncertainty_RandomSetup_sigmaZ"] = "0";
    optional_attributes.push_back("Uncertainty_Gaussian_cutoff");
    default_values["Uncertainty_Gaussian_cutoff"] = "2";
    optional_attributes.push_back("Uncertainty_Setup_randomized_rounding");
    optional_attributes.push_back("Uncertainty_Setup_linear_interpolation");
    optional_attributes.push_back("Uncertainty_Setup_merge_shifts");
    optional_attributes.push_back("Uncertainty_Setup_correction");
    optional_attributes.push_back("Uncertainty_Setup_surface_normal");
    optional_attributes.push_back("Uncertainty_RandomSetup_nFractions");
    default_values["Uncertainty_RandomSetup_nFractions"] = "1";

    optional_attributes.push_back("Uncertainty_virtual_dij");
    default_values["Uncertainty_virtual_dij"] = "virtual";
    optional_attributes.push_back("Uncertainty_virtual_stf");
    default_values["Uncertainty_virtual_stf"] = "virtual";

    optional_attributes.push_back("Uncertainty_Respiratory_nModes");
    default_values["Uncertainty_Respiratory_nModes"] = "0";
    optional_attributes.push_back("Uncertainty_Respiratory_file_name");

	// BED model
	optional_attributes.push_back("BED_nFractions");
    default_values["BED_nFractions"] = "30";
	optional_attributes.push_back("BED_standard_fraction_dose");
    default_values["BED_standard_fraction_dose"] = "2";
	optional_attributes.push_back("LET_scalefactor");
    default_values["LET_scalefactor"] = "1.0";
	optional_attributes.push_back("physD_scalefactor");
    default_values["physD_scalefactor"] = "1.0";

    /*
     * Multi-attributes
     *
     * These are the attributes that can contain sub-attributes
     */
    vector< string > multi_attributes;
    map< string, vector< string > > required_sub_attributes;
    map< string, vector< string > > optional_sub_attributes;
    map< string, map< string, string > > default_sub_values;

    // Voi attributes
    multi_attributes.push_back("VOI");
    required_sub_attributes["VOI"].push_back("name");
    optional_sub_attributes["VOI"].push_back("description");
    optional_sub_attributes["VOI"].push_back("union");
    optional_sub_attributes["VOI"].push_back("remove");
    optional_sub_attributes["VOI"].push_back("color");
    optional_sub_attributes["VOI"].push_back("EUD_p");
    default_sub_values["VOI"]["EUD_p"] = "1";
    optional_sub_attributes["VOI"].push_back("alphabeta");
    default_sub_values["VOI"]["alphabeta"] = "2";
    optional_sub_attributes["VOI"].push_back("alpha");
    default_sub_values["VOI"]["alpha"] = "0.35";
    optional_sub_attributes["VOI"].push_back("beta");
    default_sub_values["VOI"]["beta"] = "0";
    optional_sub_attributes["VOI"].push_back("betaalpha");
    default_sub_values["VOI"]["betaalpha"] = "0";
    optional_sub_attributes["VOI"].push_back("is_target");
    optional_sub_attributes["VOI"].push_back("is_organ_at_risk");

    // Constraint attributes
    multi_attributes.push_back("CONS");
    required_sub_attributes["CONS"].push_back("type");
    optional_sub_attributes["CONS"].push_back("VOI");
    optional_sub_attributes["CONS"].push_back("min_dose");
    optional_sub_attributes["CONS"].push_back("max_dose");
    optional_sub_attributes["CONS"].push_back("weight");
    optional_sub_attributes["CONS"].push_back("robustify");
    optional_sub_attributes["CONS"].push_back("min_dose_file_name");
    optional_sub_attributes["CONS"].push_back("max_dose_file_name");
	optional_sub_attributes["CONS"].push_back("OBJ");
    optional_sub_attributes["CONS"].push_back("bound");

    // Objective attributes
    multi_attributes.push_back("OBJ");
    required_sub_attributes["OBJ"].push_back("type");
    required_sub_attributes["OBJ"].push_back("VOI");
    optional_sub_attributes["OBJ"].push_back("sampling_fraction");
    default_sub_values["OBJ"]["sampling_fraction"] = "1";
    optional_sub_attributes["OBJ"].push_back("sample_with_replacement");
    optional_sub_attributes["OBJ"].push_back("dose");
    optional_sub_attributes["OBJ"].push_back("min_dose");
    optional_sub_attributes["OBJ"].push_back("max_dose");
    optional_sub_attributes["OBJ"].push_back("max_mean_dose");
    optional_sub_attributes["OBJ"].push_back("min_mean_dose");
    optional_sub_attributes["OBJ"].push_back("min_EUD");
    optional_sub_attributes["OBJ"].push_back("max_EUD");
    optional_sub_attributes["OBJ"].push_back("min_volume");
    optional_sub_attributes["OBJ"].push_back("max_volume");
    optional_sub_attributes["OBJ"].push_back("weight");
    optional_sub_attributes["OBJ"].push_back("weight_over");
    optional_sub_attributes["OBJ"].push_back("weight_under");
    optional_sub_attributes["OBJ"].push_back("nSamples");
    optional_sub_attributes["OBJ"].push_back("qjk_file_root");
    optional_sub_attributes["OBJ"].push_back("read_qjk");
    optional_sub_attributes["OBJ"].push_back("maximize");
    optional_sub_attributes["OBJ"].push_back("dose_file_name");
    optional_sub_attributes["OBJ"].push_back("nFractions");
    optional_sub_attributes["OBJ"].push_back("lq_uncertainty_type");
	optional_sub_attributes["OBJ"].push_back("tolerance");

    // Meta-Objective attributes
    multi_attributes.push_back("METAOBJ");
    required_sub_attributes["METAOBJ"].push_back("type");
    required_sub_attributes["METAOBJ"].push_back("objectives");
    optional_sub_attributes["METAOBJ"].push_back("nSamples");
    default_sub_values["METAOBJ"]["nSamples"] = "1";
    optional_sub_attributes["METAOBJ"].push_back("tail_percentage");
    default_sub_values["METAOBJ"]["tail_percentage"] = "100";
    // optional_sub_attributes["METAOBJ"].push_back("nMax_samples_true_obj");
    // optional_sub_attributes["METAOBJ"].push_back("nMin_samples_true_obj");
    // optional_sub_attributes["METAOBJ"].push_back("accuracy_true_obj");
    // default_sub_values["METAOBJ"]["accuracy_true_obj"] = "0.01";

    // Dose evaluation
    multi_attributes.push_back("DoseEval");
    // required_sub_attributes["DoseEval"].push_back("type");
    optional_sub_attributes["DoseEval"].push_back("prob");
    default_sub_values["DoseEval"]["prob"] = "1.0";
    optional_sub_attributes["DoseEval"].push_back("setup");
    optional_sub_attributes["DoseEval"].push_back("range");


    /*
     * Read the pln file
     */

    // Read the pln file
    txtFileParser_.reset( new TxtFileParser(plan_file_name, options.param_values_, required_attributes,
                optional_attributes, default_values, multi_attributes,
                required_sub_attributes, optional_sub_attributes, default_sub_values) );

    /*
     * Get general plan attributes
     */
    plan_title_ = txtFileParser_->get_attribute("title");
    plan_description_ = txtFileParser_->get_attribute("description");
    nInstances_ = txtFileParser_->get_num_attribute<unsigned int>("nInstances");
    nBeams_ = txtFileParser_->get_num_attribute<unsigned int>("nBeams");
    options_.dvh_bin_size_ = txtFileParser_->get_num_attribute<float>("DVH_bin_size");

    /*
     * --------------- Get file name attributes -----------------
     */
    plan_file_root_ = txtFileParser_->get_attribute("plan_file_root");
    string bwf_file_root = txtFileParser_->get_attribute("bwf_file_root");
    string ct_file_name = txtFileParser_->get_attribute("ct_file_name");
    string ctatts_file_name = txtFileParser_->get_attribute("ctatts_file_name");
    string dif_file_name = txtFileParser_->get_attribute("dif_file_name");
    string dij_file_root = txtFileParser_->get_attribute("dij_file_root");
    string stf_file_root = txtFileParser_->get_attribute("stf_file_root");
    string voi_file_name = txtFileParser_->get_attribute("voi_file_name");
    string vv_file_name = txtFileParser_->get_attribute("vv_file_name");

    // Use default file names if they weren't specified
    if (dij_file_root == "")
        dij_file_root = plan_file_root_;
    if (bwf_file_root == "")
        bwf_file_root = plan_file_root_;
    if (stf_file_root == "")
        stf_file_root = plan_file_root_;
    if (dif_file_name == "")
        dif_file_name = plan_file_root_ + ".dif";
    if (ct_file_name == "" && file_exists(plan_file_root_ + ".ct"))
        ct_file_name = plan_file_root_ + ".ct";
    if (ctatts_file_name == "" && file_exists(plan_file_root_ + ".ctatts"))
        ctatts_file_name = plan_file_root_ + ".ctatts";
    if (vv_file_name == "" && file_exists(plan_file_root_ + ".vv"))
        vv_file_name = plan_file_root_ + ".vv";    
    if (voi_file_name == "" && file_exists(plan_file_root_ + ".voi"))
        voi_file_name = plan_file_root_ + ".voi";


    /*
     * ------------------ create VOIs -------------------------
     */

    // Create VOI information by reading the .voi or .vv file

    unsigned int nVoxels_voi_file;
    
    if(file_exists(vv_file_name)) {
        // vv file exists
        // Create VOI information by reading the .vv file
      nVoxels_voi_file = read_vv_file(vv_file_name);
    } else if(file_exists(voi_file_name)) {
        // Create VOI information by reading the .voi file
      nVoxels_voi_file = read_voi_file(voi_file_name);
    } else {
        // neither voi nor vv file exists (throws exception)
        cout << "Neither .vv or .voi file exist" << endl;
    }    
            
    // Create VOI information by reading the .voi file
    //    unsigned int nVoxels_voi_file = read_voi_file(voi_file_name);

    // get VOIs, prescriptions and objectives
    set_voi_parameters();


    /*
     * ------------------ Geometry class -------------------------
     *
     * (most of the values are set later when reading the Dij)
     */

    // create the geometry class by reading .dif file
    geometry_ = Geometry(dif_file_name);

    // create air voxel identifier in geometry class (requires the vois)
    geometry_.load_air_voxels(vois_);

    // set the planning ct attributes
    geometry_.load_ct_attributes(ctatts_file_name);

    // Check that the dif file has the same number of voxels as the voi file 
    // not applicable if vv file was read
    if(!file_exists(vv_file_name)) {
      if (geometry_.get_nVoxels() != nVoxels_voi_file) {
        std::cerr << "The .voi file and .dif files have different numbers of"
		  << " voxels (" << nVoxels_voi_file << ", " << geometry_.get_nVoxels() << ")\n"
		  << "Unable to continue." << endl;
        throw(plan_pln_ex);
      }
    }


    /* -----------------------------------------------------------
     * Initialize dose influence matrix, bixel vectors,
     * and Uncertainty model
     */

    time_t start_time = std::time(NULL);

    create_dij_and_parameters(dij_file_root, bwf_file_root, stf_file_root);

    create_dose_delivery_model( dij_file_root, bwf_file_root, stf_file_root );

    cout << "Time elapsed: " << (std::time(NULL) - start_time) << "s\n";


    /*
     * ----------------------- create Objectives ----------------------
     */

    // build objectives according to plan file
    create_objectives();

    // Initialize objectives
    for (unsigned int iObj = 0; iObj < objectives_.size(); iObj++) {
        objectives_[iObj]->initialize(*dij_);
    }

    // Apply uniform samling rate if needed
    if (options_.use_uniform_sampling_) {
        for (unsigned int i = 0; i < objectives_.size(); i++) {
            objectives_[i]->set_sampling_fraction( options_.uniform_sampling_fraction_ );
        }
    }

    // Apply uniform number of samples per VOI if needed
    if (options_.use_uniform_samples_per_obj_) {
        for (unsigned int i = 0; i < objectives_.size(); i++) {
            unsigned int obj_nVoxels = objectives_[i]->get_nVoxels();
            if (obj_nVoxels > 0) {
                float sampling_fraction = options_.samples_per_obj_ / obj_nVoxels;
                if (sampling_fraction > 1) {
                    sampling_fraction = 1;
                }
                objectives_[i]->set_sampling_fraction(sampling_fraction);
            }
        }
    }

    // create meta objectives
    create_meta_objectives();

			
			/*
			 * ----------------------- create Constraints ----------------------
			 */
			
			// build constraints according to plan file
			create_constraints();
			
			// Initialize constraints
			for (unsigned int iCons = 0; iCons < constraints_.size(); iCons++) {
				constraints_[iCons]->initialize(*dij_);
			}
			
			
			
			
    // Write out information about the VOIs and objectives
    cout << endl;
    print_voi_info(std::cout);
    cout << endl;
    print_constraint_info(std::cout);
    cout << endl;
    print_objective_info(std::cout);

    cout << "Created plan based on pln file " << plan_file_name << ", " << nBeams_ << " beams" << endl;

}

/**
 * Destructor: clean up.
 */
Plan::~Plan() {
    for (unsigned int iVoi = 0; iVoi < vois_.size(); iVoi++) {
        delete vois_[iVoi];
        vois_[iVoi] = NULL;
    }
    for (unsigned int iObj = 0; iObj < objectives_.size(); iObj++) {
        delete objectives_[iObj];
        objectives_[iObj] = NULL;
    }
    for (unsigned int iObj = 0; iObj < meta_objectives_.size(); iObj++) {
        delete meta_objectives_[iObj];
        meta_objectives_[iObj] = NULL;
    }
    for (unsigned int iCons = 0; iCons < constraints_.size(); iCons++) {
        delete constraints_[iCons];
        constraints_[iCons] = NULL;
    }
}


/**
 *
 */
void Plan::load_parameter_files(std::string parameters_file_root) {
// TODO (dualta#1#): Need to generalise this
//    read_bwf_files(parameters_file_root);
//    parameter_vector_->makePossible();

// I fixed this (Jan 05/04/10)
//  cout << "parametervector type " << typeid(*parameter_vector_).name() << endl;
  parameter_vector_->load_parameter_files(parameters_file_root);
  parameter_vector_->makePossible();
}


/**
 * Calculate nominal dose and return it in a new DoseVector object.
 *
 * First resets the dose to zero, then calculates the nominal dose based on the bixel intensities.
 */
std::auto_ptr<DoseVector> Plan::calculate_dose() const {
    std::auto_ptr<DoseVector> temp(new DoseVector(get_nVoxels()));
    calculate_dose(*temp);
    return temp;
}


/**
 * Calculate nominal dose and return it in the supplied DoseVector.
 *
 * First resets the dose to zero, then calculates the nominal dose based on the bixel intensities.
 *
 * @param dose The DoseVector to hold the resulting dose
 */
void Plan::calculate_dose(DoseVector & dose) const {
    calculate_dose(dose, *parameter_vector_);
}


/**
 * Calculate nominal dose based on the supplied beam weights and return it in the supplied DoseVector.
 *
 * First resets the dose to zero, then calculates the nominal dose based on the bixel intensities.
 *
 * @param dose The DoseVector to hold the resulting dose
 * @param parameter_vector The beam weights to use for the dose calculation
 */
void Plan::calculate_dose( DoseVector & dose,  const ParameterVector & parameter_vector ) const {
    // Reset dose for every voxel to zero
    dose.reset_dose();

    BixelVector * bv = parameter_vector.translateToBixelVector();

    // Calculate dose
    dose_delivery_model_->calculate_nominal_dose(dose, *bv, *dij_);

    delete bv;
}


/**
 * Calculate Dose from just one beam.
 * First resets the dose to zero, then calculates the dose based on the bixel intensities.
 * Does *not* project the beam weights.
 *
 * @param dose The DoseVector to hold the resulting dose
 * @param beamNo The number of the beam to compute
 */
void Plan::calculate_beam_dose( DoseVector & dose,  const unsigned int beamNo ) const {
    // First reset dose for every voxel to zero
    dose.reset_dose();

    std::auto_ptr<BixelVector> temp ( parameter_vector_->translateToBixelVector() );

    // set intensities of other beams to zero
    extract_beam(beamNo, *temp);

    // Calculate dose
    dose_delivery_model_->calculate_nominal_dose(dose, *temp, *dij_);
}


/**
 * Calculate Dose from just one instance.
 * First resets the dose to zero, then calculates the dose based on the bixel intensities.
 * Does *not* project the beam weights.
 *
 * @param dose The DoseVector to hold the resulting dose
 * @param instanceNo The number of the instance to compute
 */
void Plan::calculate_instance_dose( DoseVector & dose,  const unsigned int instanceNo ) const {
    // First reset dose for every voxel to zero
    dose.reset_dose();

    std::auto_ptr<BixelVector> temp ( parameter_vector_->translateToBixelVector() );

    // Calculate dose
    dose_delivery_model_->calculate_instance_dose( dose, *temp, *dij_, instanceNo );
}

/**
 * Calculate Dose from just one auxiliary.
 * First resets the dose to zero, then calculates the dose based on the bixel intensities.
 * Does *not* project the beam weights.
 *
 * @param dose The DoseVector to hold the resulting dose
 * @param auxiliaryNo The number of the auxiliary to compute
 */
void Plan::calculate_auxiliary_dose( DoseVector & dose, const unsigned int auxiliaryNo ) const {
    // First reset dose for every voxel to zero
    dose.reset_dose();
	BixelVector * bix = parameter_vector_->translateToBixelVector();

    // Calculate dose
	dij_->dose_forward(dose, *bix, auxiliaryNo);
	delete bix;
}


/**
 * Calculate Dose from a random scenario.
 * First resets the dose to zero, then calculates the dose based on the bixel
 * intensities using the dose delivery model and a randomly sampled scenario.
 *
 * @param dose The DoseVector to hold the resulting dose
 */
void Plan::calculate_random_scenario_dose( DoseVector & dose ) const {
    RandomScenario temp_scenario;

    // Generate random scenario
    dose_delivery_model_->generate_random_scenario(temp_scenario);

    // Calculate dose
    calculate_random_scenario_dose(dose, temp_scenario);

}


/**
 * Calculate Dose from a random scenario.
 * First resets the dose to zero, then calculates the dose based on the bixel
 * intensities using the dose delivery model and a randomly sampled scenario.
 *
 * @param dose The DoseVector to hold the resulting dose
 * @param scenario The RandomScenario to calculate the dose for
 */
void Plan::calculate_random_scenario_dose( DoseVector & dose, const RandomScenario & scenario ) const {
    // First reset dose for every voxel to zero
    dose.reset_dose();
    std::auto_ptr<BixelVector> temp ( parameter_vector_->translateToBixelVector() );

    // Now Calculate the dose
    dose_delivery_model_->calculate_dose( dose, *temp, *dij_, scenario );
}


/// Scale the intensities of all beamlets
void Plan::scale_intensities(const double scale_factor) {
	// scale intensity for all beams in all instances
	parameter_vector_->scale_intensities(scale_factor);
}


/// Set the intensities of all beamlets to zero
void Plan::set_zero_intensities() {
    // start with zero intensity for all beams in all instances
    parameter_vector_->set_zero_intensities();
}

/// Set the intensities of all beamlets to one
void Plan::set_unit_intensities() {
    // start with unit intensity for all beams in all instances
    parameter_vector_->set_unit_intensities();
}

/// add noise to parameter vector
void Plan::add_noise_to_intensities(float scale_factor) {
    parameter_vector_->add_noise(scale_factor);
}


/**
 * Get the max intensity in any beam (in Gy)
 */
float Plan::get_max_intensity() const {
    std::cout << "get_max_intensity() in Plan.cpp : Ambigious for ParameterVector class exiting ...";
    assert(false);
    return 0;
    //return parameter_vector_->get_max_intensity();
}


/**
 * Reset the intensities of all beamlets to the last saved values
 */
void Plan::restore_intensities() {
    parameter_vector_ = backup_parameter_vector_->get_copy();
}

/**
 * Backup the intensities of all beamlets.  Overwrites the last backup.
 */
void Plan::backup_intensities() {
    backup_parameter_vector_ = parameter_vector_->get_copy();
}

/**
 * Add step to beam.  Adds d * step_size to the current beam weights
 *
 * @param pvd Direction to add to the current beam weights
 * @param step_size Scaling to be applied when adding d.
 */
void Plan::add_intensity_step( const ParameterVectorDirection& pvd,  const double step_size ) {
    try {
        if (step_size == 1) {
            parameter_vector_->add_value( pvd );
        } else {
            parameter_vector_->add_scaled_parameter_vector_direction( pvd, step_size );
        }
    } catch (DirectParameterVector_exception e) {
        cout << "Warning : Gradient step violated parameter value constraints : ";
        parameter_vector_->makePossible();
        cout << "Mapped to deliverable set" << endl;
    }

    // JU 08-19-10: projection onto bound constraints is handles in optimization class
}

/**
 * Add step to beam.  Adds d * step_size to the current beam weights
 */
void Plan::project_on_parameter_bounds()
{
  parameter_vector_->makePossible();
}


/**
 * Find the objective that would result from adding a step to beam
 */
double Plan::try_intensity_step( const ParameterVectorDirection& pvd,  const double step_size ) {
    assert(pvd.get_nParameters() == parameter_vector_->get_nParameters());

    // Store the current intensities
    backup_intensities();

    // Add the intensity step
    add_intensity_step(pvd, step_size);

    // Calculate the objective
    vector<double> temp_mo;
    vector<double> temp_ssvo;
    const vector<unsigned int> nScenario_samples( get_nMeta_objectives(), 1 );
    double temp = calculate_objective( *dij_, *parameter_vector_, temp_mo,temp_ssvo, false, false, nScenario_samples );

    // Bring back the old intensities
    restore_intensities();

    return temp;
}


/// Returns the total number of voxels in the dose cube.
unsigned int Plan::get_nVoxels() const {
    return( geometry_.get_nVoxels());
}


/// Returns the total number of non-air voxels in the dose cube.
unsigned int Plan::get_nNonair_voxels() const {
    return nNonair_voxels_;
}

/// Returns the number of volumes of interest.
unsigned int Plan::get_nVois() const {
    return vois_.size();
}

/// True if VOI number voiNo is a target.
bool Plan::is_target( unsigned int voiNo ) const {
    return vois_[voiNo]->is_target();
}


/**
 * Reads volumes of interest stored in voi format.  Because of the file format, this is accomplished in two passes,
 * one to find the number of voxel in each structure and one to store the index of each voxel in the appropriate volume.
 * Creates new Voi objects if needed.
 * <p>
 * Modifies:
 *   - vois_
 *   - nNonair_voxels_
 *   .
 * <p>
 * Here's the voi file format:<br>
 * Voi files are simply a list of 8 bit integers.  Each integer tells the volume of interest that the corresponding
 * voxel in the dose cube belongs to.  For example, structure 1 is represented by a 0 in the voi file.  Volume of
 * interest 128 (127 in file) corresponds to air and is ignored during optimization.
 * <p>
 * These voi numbers are written in column, slice, row order.
 * For example, a 3x4x2 dose cube in CERR would be written in this order:
 * <pre>
 *  slice 1: (toward feet)
 *       front       r
 * l  [ 1  2  3  4]  i
 * e  [ 9 10 11 12]  h
 * f  [17 18 19 20]  h
 * t     back        t
 * </pre><pre>
 *  slice 2: (toward head)
 *       front       r
 * l  [ 5  6  7  8]  i
 * e  [13 14 15 16]  h
 * f  [21 22 23 24]  h
 * t     back        t
 * </pre>
 * Note that left corresponds to patient right and vice versa.  Returns the number of voxels in the file.
 *
 * @param  voi_file_name the precise name of the file
 */
unsigned int Plan::read_voi_file( std::string voi_file_name ) {
    // Add VOI for air if needed
    if (vois_.size() == 0) {
        vois_.push_back(new Voi(0));
    }

    // Set properties for air
    vois_[0]->set_name("air");
    vois_[0]->set_air(true);

    // Read voi file
    VoiParser voi_file(voi_file_name,false);

    // Add other vois from voi file
    for ( unsigned int iVoi = 1; iVoi < voi_file.get_nVois(); iVoi++ ) {
        // Add new Voi objects if needed
        while (vois_.size() <= iVoi) {
            vois_.push_back(new Voi(vois_.size()));
        }

        // Add voxels to voi
        vois_[iVoi]->add_voxels(voi_file[iVoi]);
    }

    // Read number of voxels in voi file
    unsigned int nVoxels = voi_file.get_nVoxels();

    // Read the number of nonair voxels
    nNonair_voxels_ = voi_file.get_nNonair_voxels();

    // Print total numer of volumes of interest found
    cout << "Found " << vois_.size() << " Voi (including non-classified 0-VOI)" << endl;
    cout << "Found " << nNonair_voxels_ << " non-air voxels out of " << nVoxels << " voxels" << endl;

    // Make sure each voi has the correct voiNo
    for (unsigned int i = 0; i < vois_.size(); i++) {
        assert(vois_[i]->get_voiNo() == i);
    }

    return nVoxels;
}


unsigned int Plan::read_vv_file( std::string vv_file_name ) {

  
      // Read vv file
    VvParser vv_file(vv_file_name);

    // Add VOI for air if needed
    if(vois_.size() == 0) {
        vois_.push_back(new Voi(0));
    }

    // Set properties for air
    vois_[0]->set_name("air");
    vois_[0]->set_air(true);

    // Add new Voi objects if needed
    while(vois_.size() <= vv_file.size()) {
        vois_.push_back(new Voi(vois_.size()));
    }

    // Add other vois from vv file
    for(unsigned int iVoi = 1; iVoi < vv_file.size()+1; iVoi++) {

        // Add voxels to voi
        vois_[iVoi]->add_voxels(vv_file[iVoi-1].second);

        // Check if name matches
        if(vois_[iVoi]->get_name().size() == 0) {
            // No name, so use name from vv file
            vois_[iVoi]->set_name(vv_file[iVoi-1].first);
	    cout << "VOI: " << iVoi << " = " << vois_[iVoi]->get_name() << "(" << vois_[iVoi]->get_nVoxels() << ")" << endl;
        } else if(vois_[iVoi]->get_name() != vv_file[iVoi-1].first) {
            // Miss matched names
            cout << vv_file_name << "Warning: VOI name missmatch for VOI "
                << iVoi << " (" << vois_[iVoi]->get_name() << " != "
                << vv_file[iVoi-1].first << "\n";
        }
    }

    // Count non-air voxels
    nNonair_voxels_ = 0;
    for(unsigned int iVoi = 0; iVoi < vv_file.size(); iVoi++) {
        nNonair_voxels_ += vv_file[iVoi].second.size();
    }    

    // Print total numer of volumes of interest found
    cout << "Found " << vois_.size() <<
        " Voi (including non-classified 0-VOI)" << endl;

    // Make sure each voi has the correct voiNo
    for(size_t i = 0; i < vois_.size(); i++) {
        assert(vois_[i]->get_voiNo() == i);
    }
 
 
    return nNonair_voxels_;
}


/**
 * Read beam weight files.  Only reads files for first instance if the beams are static.
 *
 * @param bwf_file_root
 */
/*
void Plan::read_bwf_files( const std::string bwf_file_root ) {
    read_bwf_and_stf_files(bwf_file_root, "");
}
*/
/**
 * Read beam weight and steering files.  Only reads files for first instance if the beams are static.
 *
 * @param bwf_file_root
 * @param stf_file_root
 */
/*
void Plan::read_bwf_and_stf_files( const std::string bwf_file_root,  const std::string stf_file_root ) {
	// TODO (dualta#1#): Generalise for non-bixelVectors
    // Create bixel grids for the first instance
    // Read steering and beam weight files
    cout << "Plan::read_bwf_and_stf_files() NBeams = " << nBeams_ << endl;
    if (options_.static_beam_) {
        parameter_vector_.reset( new KonradBixelVector( &geometry_, 1, nBeams_, bwf_file_root, stf_file_root ) );
    } else {
        parameter_vector_.reset( new KonradBixelVector(&geometry_, nInstances_, nBeams_, bwf_file_root, stf_file_root));
    }
    parameter_vector_->makePossible();
    backup_parameter_vector_ = parameter_vector_->get_copy();

    cout << "Total bixels: " << parameter_vector_->get_nParameters() << endl;
}
*/

/**
 * Print VOI info to a stream
 */
void Plan::print_voi_info(std::ostream& o) const {
    unsigned int iVoi;

    // Don't print anything unless there are volumes of interest defined
    if ( vois_.size() == 0 )
        return;

    // Print the table header
    vois_[0]->print_voi_info_header(o);
    o << std::endl;
    for (iVoi=0; iVoi<vois_.size(); iVoi++) {
        // Print voi number and name
        vois_[iVoi]->print_voi_info(o);
        o << "\n";
    }
}


/**
 * Calculate statistics for the various Vois
 *
 * @param dose The dose vector to bas stats on
 */
void Plan::calculate_voi_stats(const DoseVector & dose) {
    for (unsigned int iVoi = 0; iVoi < get_nVois(); iVoi++) {
        vois_[iVoi]->calculate_stats(dose);
    }
}


/**
 * Print VOI stats to a stream based on the nominal dose
 */
void Plan::print_voi_stats(std::ostream& o) {
    DoseVector temp(get_nVoxels());
    calculate_dose(temp);

    print_voi_stats(temp, o);
}


/**
 * Print VOI stats to a stream for the given dose
 */
void Plan::print_voi_stats(const DoseVector & dose, std::ostream& o) {
    unsigned int iVoi;

    // Don't print anything unless there are volumes of interest defined
    if ( vois_.size() == 0 )
        return;

    // Calculate statistics for the VOIs
    calculate_voi_stats(dose);

    // Print the table header
    vois_[0]->print_voi_stats_header(o);
    o << endl;
    for (iVoi=0; iVoi<vois_.size(); iVoi++) {
        // Print voi number and name
        vois_[iVoi]->print_voi_stats(o);
    }
}


/**
 * Print objective info to stream
 *
 * @param o The stream to write the objective info to
 */
void Plan::print_objective_info(std::ostream& o) const {
    o << "Plan Objectives:" << endl;
    for (unsigned int iObj=0; iObj<objectives_.size(); iObj++) {
        // Print objective info
        o << *objectives_[iObj];
        o << "\n";
    }
    o << "Plan MetaObjectives:" << endl;
    for (unsigned int iObj=0; iObj<meta_objectives_.size(); iObj++) {
        // Print objective info
        o << *meta_objectives_[iObj];
        o << "\n";
    }
}

/**
 * Print constraint info to stream
 *
 * @param o The stream to write the constraint info to
 */
void Plan::print_constraint_info(std::ostream& o) const {
    o << "Plan Constraints:" << endl;
    for (unsigned int iCons=0; iCons<constraints_.size(); iCons++) {
        // Print info
        o << *constraints_[iCons];
        // o << "\n";
    }
}


/**
 * Write bwf files
 */
void Plan::write_parameter_files(std::string bwf_file_root) const {
    parameter_vector_->write_parameter_files(bwf_file_root);
}


/**
 * Check if a file exists
 *
 * @param file_name The name of the file to check
 */
bool Plan::file_exists(std::string file_name) {
    std::ifstream inp;
    inp.open(file_name.c_str(), std::ifstream::in);
    inp.close();
    if (inp.fail()) {
        return false;
    } else {
        return true;
    }
}


/**
 * Calculate or Estimate the objective using supplied beam weights.
 *
 * @param pv          The beam weights to use
 * @param multi_objective       Vector to hold returned each separate objective
 * @param estimated_ssvo        Vector to hold returned estimated sum of squared voxel objectives
 * @param use_voxel_sampling    True if voxel sampling should be used
 * @param instance_samples      Number of instance samples to use when estimating objective for each meta_objective
 */
double Plan::calculate_objective( DoseInfluenceMatrix & dij, const ParameterVector & pv, 
								  vector<double> & multi_objective, vector<double> & estimated_ssvo, 
								  bool use_voxel_sampling, bool use_scenario_sampling,
								  const vector<unsigned int>& instance_samples ) const {
    double objective = 0;

    multi_objective.resize(0);
    estimated_ssvo.resize(0);

    BixelVector * bv = pv.translateToBixelVector();

    unsigned int nObjectives = objectives_.size();
    for (unsigned int i = 0; i < nObjectives; i++) {
        double temp_ssvo = 0;
	double temp_obj = objectives_[i]->calculate_objective( *bv, dij, use_voxel_sampling, temp_ssvo );

        objective += temp_obj;
        multi_objective.push_back(temp_obj);
        estimated_ssvo.push_back(temp_ssvo);
    }

    // Calculate objectives for meta_objectives
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
      double temp_obj = meta_objectives_[i]->calculate_objective( *bv, dij, multi_objective,
								  estimated_ssvo, instance_samples[i],
								  use_voxel_sampling, use_scenario_sampling );
      objective += temp_obj;
    }

    // add constraint contributions
    objective += calculate_aug_lagrangian(dij,*bv);

    delete bv;

    return objective;
}


/**
 * Calculate merit function for augmented lagrangian method
 */
double Plan::calculate_merit_function( DoseInfluenceMatrix & dij, const ParameterVector & pv,double & lagrangian,  
									   double & objective, vector<double> & multi_objective,
									   double & constraint, vector<double> & multi_constraint) const {
  double merit = 0;
  objective = 0;
  constraint = 0;
  lagrangian = 0;
  multi_objective.resize(0);
  multi_constraint.resize(0);

  BixelVector * bv = pv.translateToBixelVector();

  unsigned int nObjectives = objectives_.size();
  for (unsigned int i = 0; i < nObjectives; i++) 
	{
	  double temp_ssvo = 0;
	  double temp_obj = objectives_[i]->calculate_objective( *bv, dij, false, temp_ssvo );

      merit += temp_obj;
	  objective += temp_obj;
	  lagrangian += temp_obj;
      multi_objective.push_back(temp_obj);
    }

  // Calculate objectives for meta_objectives
  for (unsigned int i = 0; i < meta_objectives_.size(); i++) 
	{
	  vector<double> estimated_ssvo(0);
	  double temp_obj = meta_objectives_[i]->calculate_objective( *bv, dij, multi_objective,
																  estimated_ssvo, 1,
																  false, false );
      objective += temp_obj;
	  lagrangian += temp_obj;
	  merit += temp_obj;
  }

  // add constraint contributions
  for (unsigned int i = 0; i < constraints_.size(); i++) 
	{
	  double temp_cons = 0;
	  double temp_merit = 0;
      double temp_lag = constraints_[i]->calculate_aug_lagrangian(*bv, dij, temp_cons, temp_merit);
	  
      merit += temp_merit;
	  constraint += temp_cons;
	  lagrangian += temp_lag;
      multi_constraint.push_back(temp_cons);
    }

    delete bv;

    return merit;
}


/**
 * Estimate the true value objective using supplied beam weights.
 * Calculates the objective for regular objectives and estimates it for meta-objectives.
 *
 * @param pv The beam weights to use
 * @param multi_objective       Vector to hold returned each separate objective
 * @param estimated_ssvo        Vector to hold returned estimated sum of squared voxel objectives
 * @param estimated_variance
 * @param use_voxel_sampling_for_objectives True if voxel sampling should be used when calculating objective
            for normal objectives. (default false)
 * @param use_voxel_sampling_for_metaobjectives True if voxel sampling should be used when calculating objective
            for normal objectives. (default false)
 * @param max_samples Maximum number of instance samples to use when estimating objective (default 1).
 *          Use -1 to use all possible instances.
 * @param min_samples Minimum number or instances to use for estimate
 * @param max_uncertainty Maximum uncertainty allowed as a fraction of the mean objective.
 *          Used to decide how many instance samples to calculate.
 *
 * @todo Do something with the stddev.
 */
double Plan::estimate_true_objective( DoseInfluenceMatrix & dij, const ParameterVector & pv,  vector<double> & multi_objective,
	vector<double> & estimated_ssvo,  vector<double> & estimated_variance,
	bool use_voxel_sampling_for_objectives, bool use_voxel_sampling_for_metaobjectives,
	int max_samples,  int min_samples,  float max_uncertainty ) const {

    double objective = 0;
    double temp;

	BixelVector * bv = pv.translateToBixelVector();

    multi_objective.resize(0);
    estimated_ssvo.resize(0);
    estimated_variance.resize(0);

    for (unsigned int i = 0; i < objectives_.size(); i++) {
        double temp_ssvo = 0;
		temp = objectives_[i]->calculate_objective( *bv, dij, use_voxel_sampling_for_objectives,
													temp_ssvo );
        objective += temp;
        multi_objective.push_back(temp);
        estimated_ssvo.push_back(temp_ssvo);
        estimated_variance.push_back(0);
    }

    // Calculate objectives for meta_objectives
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
        MetaObjective::estimate temp;
		temp = meta_objectives_[i]->estimate_true_objective(
				*bv, dij, max_samples, min_samples, max_uncertainty, use_voxel_sampling_for_metaobjectives );

        objective += temp.mean_objective_;

        for (unsigned int j = 0; j < temp.multi_objective_.size(); j++) {
            multi_objective.push_back( temp.multi_objective_[j] );
            estimated_ssvo.push_back( temp.estimated_ssvo_[j] );
            estimated_variance.push_back( temp.multi_variance_[j] );
        }
    }

	delete bv;

    return objective;
}


/**
 * ckeck if all objectives support its calculation via a dose vector
 */
bool Plan::supports_calculation_via_dose_vector() const
{
  if(meta_objectives_.size() < 0) {
	cout << "calculation_via_dose_vector not supported by meta-objectives.\n" << endl;
	return(false);
  }

  for (unsigned int i = 0; i < get_nPlain_objectives(); i++) {
	if(!objectives_[i]->supports_calculation_via_dose_vector()) {
	  cout << "calculation_via_dose_vector not supported by OBJ " << i+1 << endl;
	  return(false);
	}
  }

  for (unsigned int i = 0; i < get_nConstraints(); i++) {
	if(!constraints_[i]->supports_calculation_via_dose_vector()) {
	  cout << "calculation_via_dose_vector not supported by CONS " << i+1 << endl;
	  return(false);
	}
  }

  return(true);
}


/**
 * calculate objective value based on the dose vector.  
 *
 * @param the_dose              dose distribution to use
 * @param multi_objective       Vector to hold returned each separate objective
 */
double Plan::calculate_dose_objective( const DoseVector & the_dose, vector<double> & multi_objective ) const {
    double objective = 0;
    double temp;
    multi_objective.resize(0);

	if(!supports_calculation_via_dose_vector()) {
	  cout << "Objective function via dose vector not supported\n" << endl;
	  throw( std::runtime_error( "Method not implemented."));
	}

    // Calculate dose objectives for normal objectives
    for (unsigned int i = 0; i < objectives_.size(); i++) {
        temp = objectives_[i]->calculate_dose_objective(the_dose);

        objective += temp;
        multi_objective.push_back(temp);
    }

	// note: Meta-objectives do not support this method

    return objective;
}


/**
 * calculate constraint contribution to augmented lagrangian based on the dose vector.  
 *
 * @param the_dose              dose distribution to use
 * @param multi_objective       Vector to hold returned each separate objective
 */
double Plan::calculate_dose_aug_lagrangian( const DoseVector & the_dose ) const {
    double objective = 0;
    double temp;

    for (unsigned int i = 0; i < constraints_.size(); i++) {
        temp = constraints_[i]->calculate_dose_aug_lagrangian(the_dose);

        objective += temp;
    }

	// note: Meta-objectives do not support this method

    return objective;
}


/**
 * calculate objective value based on the dose vector
 * and the gradient with respect to the dose values.  
 * not supported by all objectives.
 * 
 */
double Plan::calculate_dose_objective_and_gradient( const DoseVector & the_dose,  DoseVector & dose_gradient,  vector<double> & multi_objective ) const
{
    double objective = 0;
    double temp;
    multi_objective.resize(0);

    // Calculate dose objectives for normal objectives
    for (unsigned int i = 0; i < objectives_.size(); i++) {
	  temp = objectives_[i]->calculate_dose_objective_and_gradient(the_dose, dose_gradient);

	  objective += temp;
	  multi_objective.push_back(temp);
    }

	// note: Meta-objectives do not support this method

    return objective;
}

/**
 * calculate objective value based on the dose vector
 * and the gradient with respect to the dose values.  
 * not supported by all objectives.
 * 
 */
double Plan::calculate_dose_aug_lagrangian_and_gradient( const DoseVector & the_dose,  DoseVector & dose_gradient ) const
{
    double objective = 0;
    double temp;

    // Calculate dose objectives for normal objectives
    for (unsigned int i = 0; i < constraints_.size(); i++) {
	  temp = constraints_[i]->calculate_dose_aug_lagrangian_and_gradient(the_dose, dose_gradient);
	  objective += temp;
    }

    return objective;
}

/**
 * calculate objective based on the dose vector
 * This method does not require the dij to be ordered according to voxels
 * Does not work for all objectives
 */
double Plan::calculate_objective_via_dose( DoseInfluenceMatrix & dij,
										   const BixelVector & beamweights,
										   vector<double> & multi_objective) const 
{
  if(!supports_calculation_via_dose_vector()) {
	cout << "Objective function via dose vector not supported\n" << endl;
	throw( std::runtime_error( "Method not implemented."));
  }

  multi_objective.resize(0);

  // calculate dose
  DoseVector dose = DoseVector(dij.get_nVoxels());
  dij.dose_forward(dose,beamweights);

  // calculate objective and dose gradient for objective
  double objective = calculate_dose_objective(dose,multi_objective);

  // add constraint contribution
  objective += calculate_dose_aug_lagrangian(dose);

  return objective;
}


/**
 * calculate objective and gradient based on the dose vector
 * This method does not require the dij to be ordered according to voxels
 * Does not work for all objectives
 */
double Plan::calculate_objective_and_gradient_via_dose( DoseInfluenceMatrix & dij,
														const BixelVector & beamweights,
														BixelVectorDirection & gradient,
														vector<double> & multi_objective) const 
{
  if(!supports_calculation_via_dose_vector()) {
	cout << "Objective function via dose vector not supported\n" << endl;
	throw( std::runtime_error( "Method not implemented."));
  }

  multi_objective.resize(0);
  assert(gradient.get_nParameters() == beamweights.get_nParameters());

  // calculate dose
  DoseVector dose = DoseVector(dij.get_nVoxels());
  DoseVector dose_gradient = DoseVector(dij.get_nVoxels());
  dij.dose_forward(dose,beamweights);

  // calculate objective and dose gradient for objective
  double objective = calculate_dose_objective_and_gradient(dose,dose_gradient,multi_objective);

  // add constraint contribution
  objective += calculate_dose_aug_lagrangian_and_gradient(dose,dose_gradient);

  // now multiply the dij with dose gradient to get the beam weight gradient
  for(DoseInfluenceMatrix::iterator iter = dij.begin(); iter.not_at_end(); iter++) {
	gradient[iter.get_bixelNo()] += dose_gradient[iter.get_voxelNo()] * iter.get_influence();
  }

  return objective;
}




/**
 * Calculate or estimate objective and the gradient of the objective.
 */
double Plan::calculate_objective_and_gradient( DoseInfluenceMatrix & dij, 
											   const ParameterVector & pv,  
											   ParameterVectorDirection & gradient,  
											   vector<double> & multi_objective, 
											   vector<double> & estimated_ssvo, 
											   bool use_voxel_sampling,
											   bool use_scenario_sampling,
											   const vector<unsigned int> & instance_samples) const 
{
    assert(gradient.get_nParameters() == pv.get_nParameters());

    multi_objective.resize(0);
    estimated_ssvo.resize(0);

	// translate parameter vector to bixel vector
	BixelVector * bv = pv.translateToBixelVector();

    BixelVectorDirection bvd_gradient = BixelVectorDirection(bv->get_nBixels());

    float gradient_multiplier = 1.0f;
    double objective = 0;
    double temp_obj;


	// gradient calculation via the dose vector (to be used if the dij is not transposed)
	if(!dij.is_transposed())
	  {
		cout << "calculating gradient via dose vector" << endl;
		objective = calculate_objective_and_gradient_via_dose( dij,
															   *bv,
															   bvd_gradient,
															   multi_objective);

	  }
	// gradient calculation via beam weights directly
	else 
	  {
		// Calculate objectives 
		for (unsigned int i = 0; i < objectives_.size(); i++) {

		  double temp_ssvo = 0;
		  temp_obj = objectives_[i]->calculate_objective_and_gradient(*bv, dij, bvd_gradient, gradient_multiplier, use_voxel_sampling, temp_ssvo );

		  objective += temp_obj;
		  multi_objective.push_back(temp_obj);
		  estimated_ssvo.push_back(temp_ssvo);
		}

		// Calculate objectives for meta_objectives
		for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
		  temp_obj = meta_objectives_[i]->calculate_objective_and_gradient(*bv, dij, multi_objective, estimated_ssvo, 
																		   bvd_gradient, gradient_multiplier, instance_samples[i],
																		   use_voxel_sampling, use_scenario_sampling);
		  
		  objective += temp_obj;
		}

		// add constraint contributions
		objective += calculate_aug_lagrangian_and_gradient(dij, *bv, bvd_gradient);
	  }

	// Change from gradient per bixel to gradient per parameter
	gradient = pv.translateBixelGradient(bvd_gradient);

	delete bv;

    return objective;
}


/**
 * Calculate or estimate objective and the gradient of the objective.
 *
 * @param pv The beam weights to use for the objective calculation
 * @param multi_objective       Vector to hold returned each separate objective
 * @param gradient The object that the gradient will be returned in.
 */
double Plan::calculate_objective_and_gradient( DoseInfluenceMatrix & dij, 
											   const ParameterVector & pv, 
											   ParameterVectorDirection & gradient, 
											   vector<double> & multi_objective) const 
{
  vector<double> estimated_ssvo(0);
  vector<unsigned int> instance_samples(get_nMeta_objectives(),1);

  return calculate_objective_and_gradient( dij, pv, gradient, 
										   multi_objective, estimated_ssvo,
										   false, false, instance_samples );
}



/**
 * Calculate objective, gradient, and Hessian times a vector of the objective.
 *
 * @param multi_objective       Vector to hold returned each separate objective
 * @param gradient The object that the gradient will be returned in.
 * @param v The vector to multiply the Hessian by
 * @param Hv the objectect used to return the Hessian times v
 * @param estimated_ssvo Vector to hold returned estimated sum of squared voxel objectives.
 * @param use_voxel_sampling True if voxel sampling should be used
 * @param instance_samples Number of instance samples to use when estimating objective for each meta_objective
 */
double Plan::calculate_objective_and_gradient_and_Hv( vector<double> & multi_objective,
        vector<double> & estimated_ssvo,  ParameterVectorDirection & gradient,  const std::vector<float> & v,
        std::vector<float> & Hv,  bool use_voxel_sampling,  bool use_scenario_sampling,
        const vector<unsigned int>& instance_samples ) const {
    return calculate_objective_and_gradient_and_Hv( *parameter_vector_, multi_objective, estimated_ssvo,
	    gradient, v, Hv, use_voxel_sampling, use_scenario_sampling, instance_samples );
}


/**
 * Calculate objective, gradient, and Hessian times a vector of the objective.
 *
 * @param pv The beam weights to use when calculating the objective
 * @param multi_objective       Vector to hold returned each separate objective
 * @param gradient The object that the gradient will be returned in.
 * @param v The vector to multiply the Hessian by
 * @param Hv the object used to return the Hessian times v
 * @param estimated_ssvo Vector to hold returned estimated sum of squared voxel objectives.
 * @param use_voxel_sampling True if voxel sampling should be used
 * @param instance_samples Number of instance samples to use when estimating objective for each meta_objective
 */
double Plan::calculate_objective_and_gradient_and_Hv( const ParameterVector & pv,
        vector<double> & multi_objective,  vector<double> & estimated_ssvo,  ParameterVectorDirection & gradient,
        const std::vector<float> & v,  std::vector<float> & Hv,  bool use_voxel_sampling,
        bool use_scenario_sampling, const vector<unsigned int>& instance_samples ) const {

    assert(gradient.get_nParameters() == pv.get_nParameters());
    float gradient_multiplier = 1;

    multi_objective.resize(0);
    estimated_ssvo.resize(0);

	BixelVector * bv = pv.translateToBixelVector();
    BixelVectorDirection bvd_gradient = BixelVectorDirection(bv->get_nBixels());

	for (unsigned int i = 0; i < Hv.size(); i++) {
        Hv[i] = 0;
    }

    double objective = 0;
    double temp_obj;

    for (unsigned int i = 0; i < objectives_.size(); i++) {
        double temp_ssvo = 0;
		temp_obj = objectives_[i]->calculate_objective_and_gradient_and_Hv(
				*bv, *dij_, bvd_gradient, gradient_multiplier, v, Hv, use_voxel_sampling, temp_ssvo );
        objective += temp_obj;
        multi_objective.push_back(temp_obj);
        estimated_ssvo.push_back(temp_ssvo);
    }

    // Calculate objectives for meta_objectives
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
		temp_obj = meta_objectives_[i]->calculate_objective_and_gradient_and_Hv(
				*bv, *dij_, multi_objective, estimated_ssvo, bvd_gradient, v, Hv, instance_samples[i],
				use_voxel_sampling, use_scenario_sampling );

        objective += temp_obj;
    }

	// Change from gradient per bixel to gradient per parameter
	gradient = pv.translateBixelGradient(bvd_gradient);
	Hv = pv.translateBixelHessian(Hv);

	delete bv;

	return objective;
}

/**
 * ckeck if augmented lagrangian approach is supported by all constraints
 */
bool Plan::supports_augmented_lagrangian() const
{
    for (unsigned int i = 0; i < get_nConstraints(); i++) {
	if(!constraints_[i]->supports_augmented_lagrangian()) {
	    cout << "Augmented lagrangian approach is not supported by CONS " << i+1 << endl;
	    return(false);
	}
    }
    return(true);
}

/**
 * calculate constraint contributions to augmented lagrangian
 */
 double Plan::calculate_aug_lagrangian_and_gradient(DoseInfluenceMatrix & dij, const BixelVector & beam_weights, BixelVectorDirection & gradient) const
{

    assert(beam_weights.get_nParameters() == gradient.get_nParameters());

    float gradient_multiplier = 1.0f;

    double objective = 0;
    for (unsigned int i = 0; i < constraints_.size(); i++) {
      objective += constraints_[i]->calculate_aug_lagrangian_and_gradient(beam_weights, dij, gradient, gradient_multiplier);
    }

    return objective;
}

/**
 * calculate constraint contributions to augmented lagrangian
 */
double Plan::calculate_aug_lagrangian(DoseInfluenceMatrix & dij, const BixelVector & beam_weights) const
{
    double objective = 0;
    for (unsigned int i = 0; i < constraints_.size(); i++) {
      objective += constraints_[i]->calculate_aug_lagrangian(beam_weights, dij);
    }
    return objective;
}

/**
 * update lagrange multipliers for hard constraints
 */
void Plan::update_lagrange_multipliers(DoseInfluenceMatrix & dij, const ParameterVector & pv) {

  BixelVector * bv = pv.translateToBixelVector();

  cout << "updating lagrange multipliers" << endl;

  for (unsigned int i = 0; i < get_nConstraints(); i++) {
    constraints_[i]->update_lagrange_multipliers(*bv, dij);
  }

  delete bv;
}

/**
 * update penalties for hard constraints
 */
void Plan::update_penalty(DoseInfluenceMatrix & dij, const ParameterVector & pv, float tol, float multiplier) {

  BixelVector * bv = pv.translateToBixelVector();

  cout << "updating penalties" << endl;

  for (unsigned int i = 0; i < get_nConstraints(); i++) {
    constraints_[i]->update_penalty(*bv, dij, tol, multiplier);
  }

  delete bv;
}

/**
 * ckeck if projection solver is supported
 */
bool Plan::supports_projection_solver() const
{
    if(objectives_.size() > 0) {
	cout << "WARNING: all objectives are ignored in the linear projection solver" << endl;
    }

    for (unsigned int i = 0; i < get_nConstraints(); i++) {
	if(!constraints_[i]->supports_projection_solver()) {
	    cout << "Projection Solver is not supported by CONS " << i+1 << endl;
	    return(false);
	}
    }
    return(true);
}

/**
 * ckeck if projection solver is supported
 */
bool Plan::supports_projection_optimizer(unsigned consNo) const
{
  return(constraints_[consNo]->supports_projection_optimizer());
}


/**
 * project onto constraints
 */
unsigned int Plan::project_onto_constraints() {
  return(project_onto_constraints(*parameter_vector_));
}

/**
 * project onto constraints
 */
unsigned int Plan::project_onto_constraints(ParameterVector & pv)
{
  BixelVector * bv = pv.translateToBixelVector();

  unsigned int nProjections = 0;

  for (unsigned int i = 0; i < get_nConstraints(); i++) {
    nProjections += constraints_[i]->project_onto(*bv, *dij_);
  }

  // save new bixel intensities
  parameter_vector_->translateBixelVector(*bv);
  delete bv;

  return nProjections;
}

/**
 * get the upper bound of a constraint
 */
float Plan::get_upper_constraint_bound(unsigned int consNo) const
{
  return(constraints_[consNo]->get_upper_bound());
}

/**
 * get the lower bound of a constraint
 */
float Plan::get_lower_constraint_bound(unsigned int consNo) const
{
  return(constraints_[consNo]->get_lower_bound());
}

/**
 * set the upper bound of a constraint
 */
void Plan::set_upper_constraint_bound(unsigned int consNo, float value)
{
  constraints_[consNo]->set_upper_bound(value);
}

/**
 * set the lower bound of a constraint
 */
void Plan::set_lower_constraint_bound(unsigned int consNo, float value)
{
  constraints_[consNo]->set_lower_bound(value);
}


/**
 * check if active set in all constraints is empty
 */
bool Plan::is_active_set_empty() const
{
  for (unsigned int i = 0; i < get_nConstraints(); i++) {
    if(!constraints_[i]->is_active_set_empty()) {
      return false;
    }
  }
  return true;
}

/**
 * put all constraints back into the active set
 */
void Plan::reset_active_set()
{
  for (unsigned int i = 0; i < get_nConstraints(); i++) {
    constraints_[i]->reset_active_set();
  }
}


/**
 * check if all objectives and constraints support the external solver interface
 */
bool Plan::supports_external_solver() const
{
  // meta-objectives don't support external solver
  if(get_nMeta_objectives() > 0) {
      cout << "External solver interface is not supported by Meta-Objectives" << endl;
      return(false);
  }

  // deal with constraints
  for (unsigned int i = 0; i < get_nConstraints(); i++) {
      if(!constraints_[i]->supports_external_solver()) {
	  cout << "External solver interface is not supported by CONS " << i+1 << endl;
	  return(false);
      }
  }
  // deal with objectives
  for (unsigned int i = 0; i < objectives_.size(); i++) {
      if(!objectives_[i]->supports_external_solver()) {
	  cout << "External solver interface is not supported by OBJ " << i+1 << endl;
	  return(false);
      }
  }
  return(true);
}

/**
 * get number of generic constraints for external solver
 */
unsigned int Plan::get_nGeneric_constraints() const
{
  unsigned int nCons = 0;

  // deal with constraints
  for (unsigned int i = 0; i < get_nConstraints(); i++) {
      nCons += constraints_[i]->get_nGeneric_constraints();
  }
  // deal with objectives
  for (unsigned int i = 0; i < objectives_.size(); i++) {
    nCons += objectives_[i]->get_nGeneric_constraints();
  }
  return nCons;
}

/**
 * get number of auxiliary variables for external solver
 */
unsigned int Plan::get_nAuxiliary_variables() const
{
  unsigned int nVar = 0;

  for (unsigned int i = 0; i < objectives_.size(); i++) {
    nVar += objectives_[i]->get_nAuxiliary_variables();
  }
  return nVar;
}

/**
 * make generic data set for external solver
 */
void Plan::get_generic_optimization_data(GenericOptimizationData & data)
{
  // deal with constraints
  for (unsigned int i = 0; i < get_nConstraints(); i++) {
    constraints_[i]->add_to_optimization_data_set(data, *dij_);
  }

  // deal with objectives
  for (unsigned int i = 0; i < objectives_.size(); i++) {
    objectives_[i]->add_to_optimization_data_set(data, *dij_);
  }

  assert(data.nConstraints_until_now_==data.nConstraints_);
  assert(data.nAuxiliaries_until_now_==data.nVarAux_);
}


/**
 *  construct Vois
 */
void Plan::set_voi_parameters() {
    cout << "create Vois" << endl;

    /*
     * Read voi parameters
     */

    unsigned int nVois_in_pln_file = txtFileParser_->get_multi_attribute_nThings( "VOI" );

    // Make sure there are at least as many VOIs as needed
    while (vois_.size() <= nVois_in_pln_file) {
        vois_.push_back(new Voi( vois_.size() ));
    }

    for (unsigned int iVoi = 0; iVoi < nVois_in_pln_file; iVoi++) {

        if (txtFileParser_->get_multi_attribute("VOI","name",iVoi)!="") {
          vois_[iVoi+1]->set_name(txtFileParser_->get_multi_attribute("VOI","name",iVoi));
        }
        if (txtFileParser_->is_set_multi_attribute("VOI", "description",iVoi)) {
            vois_[iVoi+1]->set_description( txtFileParser_->get_multi_attribute( "VOI", "description",iVoi ) );
        }

        if (txtFileParser_->is_set_multi_attribute("VOI", "EUD_p",iVoi)) {
            vois_[iVoi+1]->set_EUD_p( txtFileParser_->get_num_multi_attribute<float>( "VOI", "EUD_p", iVoi ) );
        }

        if (txtFileParser_->is_set_multi_attribute("VOI", "alphabeta",iVoi)) {
            vois_[iVoi+1]->set_alphabeta( txtFileParser_->get_num_multi_attribute<float>( "VOI", "alphabeta", iVoi ) );
        }

        if (txtFileParser_->is_set_multi_attribute("VOI", "alpha",iVoi)) {
            vois_[iVoi+1]->set_alpha( txtFileParser_->get_num_multi_attribute<float>( "VOI", "alpha", iVoi ) );
        }

        if (txtFileParser_->is_set_multi_attribute("VOI", "beta",iVoi)) {
            vois_[iVoi+1]->set_beta( txtFileParser_->get_num_multi_attribute<float>( "VOI", "beta", iVoi ) );
        }

        if (txtFileParser_->is_set_multi_attribute("VOI", "betaalpha",iVoi)) {
		  vois_[iVoi+1]->set_betaalpha( txtFileParser_->get_num_multi_attribute<float>( "VOI", "betaalpha", iVoi ) );
		}

        if (txtFileParser_->is_set_multi_attribute("VOI", "is_target",iVoi))
            vois_[iVoi+1]->set_target(true);

        if (txtFileParser_->is_set_multi_attribute("VOI", "is_organ_at_risk",iVoi))
            vois_[iVoi+1]->set_organ_at_risk(true);

        // Attempt to read color
        if (txtFileParser_->is_set_multi_attribute( "VOI", "color",iVoi)) {
            // The color was supplied, so parse it
            std::istringstream iss(txtFileParser_->get_multi_attribute( "VOI", "color", iVoi ));
            float red, green, blue;
            if ((iss >> red).fail() || (iss >> green).fail() || (iss >> blue).fail() ) {
                txtFileParser_->report_error("Failed while reading color for VOI");
            }
            vois_[iVoi+1]->set_color(red, green, blue);
        }

        // Read and process unions
        if (txtFileParser_->is_set_multi_attribute( "VOI", "union", iVoi)) {
            std::istringstream iss(txtFileParser_->get_multi_attribute( "VOI", "union", iVoi ));
            unsigned int voiNo_to_join;
            while (!(iss >> voiNo_to_join).fail()) {
                if (voiNo_to_join > iVoi+1) {
                    // Can only join with lower numbered Vois
                    txtFileParser_->report_error("It is only possible to union " + string(" with lower numbered VOIs"));
                } else {
                    vois_[iVoi+1]->union_Voi(*vois_.at(voiNo_to_join));
                }
            }
        }

        // Read and process voi removals
        if (txtFileParser_->is_set_multi_attribute( "VOI", "remove", iVoi)) {
            std::istringstream iss(txtFileParser_->get_multi_attribute( "VOI", "remove", iVoi ));
            unsigned int voiNo_to_remove;
            while (!(iss >> voiNo_to_remove).fail()) {
                if (voiNo_to_remove > iVoi+1) {
                    // Can only remove lower numbered Vois
                    txtFileParser_->report_error("It is only possible to remove " + string(" lower numbered VOIs"));
                } else {
                    vois_[iVoi+1]->remove_Voi(*vois_.at(voiNo_to_remove));
                }
            }
        }
    }
}


/**
 * Create all objectives
 */
void Plan::create_constraints() {
    cout << "create constraints" << endl;

    unsigned int nCons_in_pln_file = txtFileParser_->get_multi_attribute_nThings( "CONS" );

    for (unsigned int iCons = 0; iCons < nCons_in_pln_file; iCons++) {

        /*
         * Read attributes that are common to most constraints
         */

        // Read weight if it is set, otherwise use 1
	    float weight = 1.0;
		if (txtFileParser_->is_set_multi_attribute("CONS", "weight", iCons)) {
		  weight = txtFileParser_->get_num_multi_attribute<float>( "CONS", "weight", iCons );
		}
	  
        /*
         * Now figure out what type of constraint we have
         */
        string constraint_type = to_lower(txtFileParser_->get_multi_attribute( "CONS", "type", iCons ));

        if (constraint_type == "doseconstraint") {

            bool is_min_constraint = false;
            bool is_max_constraint = false;
            float min_dose = 0;
            float max_dose = 0;
			unsigned int voiNo;
			
			// First get VOI number
			if (txtFileParser_->is_set_multi_attribute( "CONS", "VOI", iCons )) {
			  voiNo = txtFileParser_->get_num_multi_attribute<unsigned int>( "CONS", "VOI", iCons );
			  if (voiNo >= vois_.size()) {
				txtFileParser_->report_error("CONS VOI too high");
			  }
			}
			else {
			  txtFileParser_->report_error("CONS VOI not specified");
			}
			
            if (txtFileParser_->is_set_multi_attribute( "CONS", "min_dose", iCons )) {
			  // Min dose constraint
			  is_min_constraint = true;
			  min_dose = txtFileParser_->get_num_multi_attribute<float>( "CONS", "min_dose", iCons );
			  
			  // Set Voi parameters
			  if (min_dose > 0) {
				vois_[voiNo]->set_target(true);
				vois_[voiNo]->set_desired_dose(min_dose);
			  }
            }
			
            if (txtFileParser_->is_set_multi_attribute( "CONS", "max_dose", iCons )) {
			  // Max dose constraint
			  is_max_constraint = true;
			  max_dose = txtFileParser_->get_num_multi_attribute<float>( "CONS", "max_dose", iCons );
			  
			  // Set VOI prameters
			  vois_[voiNo]->set_organ_at_risk(true);
            }
			
			if (txtFileParser_->is_set_multi_attribute( "CONS", "robustify", iCons )) {
			  constraints_.push_back( new RobustifiedDoseConstraint( vois_[voiNo], iCons+1,
																	 is_max_constraint, is_min_constraint,
																	 max_dose, min_dose,
																	 *dose_delivery_model_) );
			}
			else {
			  constraints_.push_back( new UniformDoseConstraint( vois_[voiNo], iCons+1,
																 is_max_constraint, is_min_constraint,
																 max_dose, min_dose,
																 weight) );
			}
			
        } else if (constraint_type == "voxeldoseconstraint") {

            bool is_min_constraint = false;
            bool is_max_constraint = false;
            string min_dose_file_name = "";
            string max_dose_file_name = "";
			unsigned int voiNo;
			
			// First get VOI number
			if (txtFileParser_->is_set_multi_attribute( "CONS", "VOI", iCons )) {
			  voiNo = txtFileParser_->get_num_multi_attribute<unsigned int>( "CONS", "VOI", iCons );
			  if (voiNo >= vois_.size()) {
				txtFileParser_->report_error("CONS VOI too high");
			  }
			}
			else {
			  txtFileParser_->report_error("CONS VOI not specified");
			}
			
            if (txtFileParser_->is_set_multi_attribute( "CONS", "min_dose_file_name", iCons )) {
			  // Min dose constraint
			  is_min_constraint = true;
			  min_dose_file_name = txtFileParser_->get_multi_attribute("CONS", "min_dose_file_name", iCons);
			}

            if (txtFileParser_->is_set_multi_attribute( "CONS", "max_dose_file_name", iCons )) {
			  // Max dose constraint
			  is_max_constraint = true;
			  max_dose_file_name = txtFileParser_->get_multi_attribute("CONS", "max_dose_file_name", iCons);
			}

			if (!is_min_constraint && !is_max_constraint) {
				txtFileParser_->report_error("CONS type voxeldoseconstraint: no file specified");
			}


			constraints_.push_back( new VaryingDoseConstraint( vois_[voiNo], iCons+1,
															   is_max_constraint, is_min_constraint, geometry_.get_nVoxels(),
															   max_dose_file_name, min_dose_file_name,
															   weight) );
			
        } else if (constraint_type == "meanconstraint") {

		  bool is_min_constraint = false;
		  bool is_max_constraint = false;
		  float min_dose = 0;
		  float max_dose = 0;
		  unsigned int voiNo;

	    // First get VOI number
	    if (txtFileParser_->is_set_multi_attribute( "CONS", "VOI", iCons )) {
	      voiNo = txtFileParser_->get_num_multi_attribute<unsigned int>( "CONS", "VOI", iCons );
	      if (voiNo >= vois_.size()) {
		txtFileParser_->report_error("CONS VOI too high");
	      }
	    }
	    else {
	      txtFileParser_->report_error("CONS VOI not specified");
	    }

            if (txtFileParser_->is_set_multi_attribute( "CONS", "min_dose", iCons )) {
                // Min dose constraint
                is_min_constraint = true;
                min_dose = txtFileParser_->get_num_multi_attribute<float>( "CONS", "min_dose", iCons );

                // Set Voi parameters
                if (min_dose > 0) {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(min_dose);
                }
            }

            if (txtFileParser_->is_set_multi_attribute( "CONS", "max_dose", iCons )) {
                // Max dose constraint
                is_max_constraint = true;
                max_dose = txtFileParser_->get_num_multi_attribute<float>( "CONS", "max_dose", iCons );

                // Set VOI prameters
                vois_[voiNo]->set_organ_at_risk(true);
            }

	    constraints_.push_back( new MeanConstraint( vois_[voiNo], iCons+1,
							is_max_constraint, is_min_constraint,
							max_dose, min_dose,
							weight) );

        } else if (constraint_type == "bixelpositivity") {

	    constraints_.push_back( new BixelPositivityConstraint(iCons+1,weight) );

        } else if (constraint_type == "constrainedobjective") {
			
		  Objective* tmp_obj;
            if (txtFileParser_->is_set_multi_attribute( "CONS", "OBJ", iCons )) {
                unsigned int objNo = txtFileParser_->get_num_multi_attribute<unsigned int>( "CONS", "OBJ", iCons );
				bool found = false;
				vector<Objective*>::iterator iter = objectives_.begin();
				while (iter!=objectives_.end()) {
					if ((*iter)->get_objNo() == objNo) {
					  found = true;
					  tmp_obj = *iter;
					  // Remove objective from the objective list
					  objectives_.erase(iter);
					  break;
					} else {
					  iter++;
					}
				}
				if(!found) {
				  txtFileParser_->report_error("constrained objective not found");
				}
			}
			else {
				txtFileParser_->report_error("CONS OBJ attribute missing");
			}
				
			float bound = 0;
            if (txtFileParser_->is_set_multi_attribute( "CONS", "bound", iCons )) {
                bound = txtFileParser_->get_num_multi_attribute<float>( "CONS", "bound", iCons );
			}
			
			// create constraint			
			constraints_.push_back( new ConstrainedObjective(iCons+1,tmp_obj,bound,weight) );
			
        } else if (constraint_type == "constrainedmetaobjective") {
			
		  MetaObjective* tmp_obj;
            if (txtFileParser_->is_set_multi_attribute( "CONS", "OBJ", iCons )) {
                unsigned int objNo = txtFileParser_->get_num_multi_attribute<unsigned int>( "CONS", "OBJ", iCons );
				bool found = false;
				vector<MetaObjective*>::iterator iter = meta_objectives_.begin();
				while (iter!=meta_objectives_.end()) {
					if ((*iter)->get_objNo() == objNo) {
					  found = true;
					  tmp_obj = *iter;
					  // Remove objective from the objective list
					  meta_objectives_.erase(iter);
					  break;
					} else {
					  iter++;
					}
				}
				if(!found) {
				  txtFileParser_->report_error("constrained meta objective not found");
				}
			}
			else {
				txtFileParser_->report_error("CONS OBJ attribute missing");
			}
				
			float bound = 0;
            if (txtFileParser_->is_set_multi_attribute( "CONS", "bound", iCons )) {
                bound = txtFileParser_->get_num_multi_attribute<float>( "CONS", "bound", iCons );
			}
			
			// create constraint			
			constraints_.push_back( new ConstrainedMetaObjective(iCons+1,tmp_obj,bound,weight) );
			
		} else {
		  txtFileParser_->report_error("Unknown constraint type: " + constraint_type);
        }
    }
}


/**
 * Create all objectives
 */
void Plan::create_objectives() {
    cout << "create objectives" << endl;

    unsigned int nObjs_in_pln_file = txtFileParser_->get_multi_attribute_nThings( "OBJ" );

    for (unsigned int iObj = 0; iObj < nObjs_in_pln_file; iObj++) {

        /*
         * Read attributes that are common to most objectives
         */

        // First get VOI number (required attribute, so we know it's valid)
        unsigned int voiNo = txtFileParser_->get_num_multi_attribute<unsigned int>( "OBJ", "VOI", iObj );
        if (voiNo >= vois_.size()) {
            txtFileParser_->report_error("OBJ VOI too high");
        }

        // Read weight if it is set, otherwise use 1
        float weight = 1.0;
        if (txtFileParser_->is_set_multi_attribute("OBJ", "weight", iObj)) {
            weight = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "weight", iObj );
        }


        // Read weight under if it is set, otherwise use 1
        float weight_under = 1.0;
        if (txtFileParser_->is_set_multi_attribute("OBJ", "weight_under", iObj)) {
            weight_under = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "weight_under", iObj );
        }


        // Read weight over if it is set, otherwise use 1
        float weight_over = 1.0;
        if (txtFileParser_->is_set_multi_attribute( "OBJ", "weight_over", iObj )) {
            weight_over = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "weight_over", iObj );
        }


        // Read sampling fraction if it is set, otherwise use 1
        float sampling_fraction = 1.0;
        if (txtFileParser_->is_set_multi_attribute( "OBJ", "sampling_fraction", iObj )) {
            sampling_fraction = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "sampling_fraction", iObj );
        }


        // Read resample voxels if it is set, otherwise use 1
        bool sample_with_replacement =
            txtFileParser_->is_set_multi_attribute( "OBJ", "sample_with_replacement", iObj );


        /*
         * Now figure out what type of objective we have
         */
        string objective_type = to_lower(txtFileParser_->get_multi_attribute( "OBJ", "type", iObj ));

        if (objective_type == "meansquarederror") {
            bool is_min_constraint = false;
            bool is_max_constraint = false;
            float min_dose = 0;
            float max_dose = 0;

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "min_dose", iObj )) {
                // Min dose constraint
                is_min_constraint = true;
                min_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "min_dose", iObj );

                // Set Voi parameters
                if (min_dose > 0) {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(min_dose);
                }
                // Set legacy parameters
                vois_[voiNo]->get_prescription_ptr()->set_min_dose(
                    min_dose);
                vois_[voiNo]->get_prescription_ptr()->set_weight_under(
                    weight_under);
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "max_dose", iObj )) {
                // Max dose constraint
                is_max_constraint = true;
                max_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "max_dose", iObj );

                // Set VOI prameters
                vois_[voiNo]->set_organ_at_risk(true);

                // Set legacy parameters
                vois_[voiNo]->get_prescription_ptr()->set_max_dose( max_dose );
                vois_[voiNo]->get_prescription_ptr()->set_weight_over( weight_over );
            }

            objectives_.push_back( new MeanSquaredErrorObjective( vois_[voiNo], iObj+1, weight, sampling_fraction,
                    sample_with_replacement, is_max_constraint, max_dose, weight_over, is_min_constraint, min_dose,
                    weight_under) );

        } else if (objective_type == "absoluteerror") {
            bool is_min_constraint = false;
            bool is_max_constraint = false;
            float min_dose = 0;
            float max_dose = 0;

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "min_dose", iObj )) {
                // Min dose constraint
                is_min_constraint = true;
                min_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "min_dose", iObj );

                // Set Voi parameters
                if (min_dose > 0) {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(min_dose);
                }
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "max_dose", iObj )) {
                // Max dose constraint
                is_max_constraint = true;
                max_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "max_dose", iObj );

                // Set VOI prameters
                vois_[voiNo]->set_organ_at_risk(true);

            }

            objectives_.push_back( new AbsoluteErrorObjective( vois_[voiNo], iObj+1, weight,
							       is_max_constraint, max_dose, weight_over,
							       is_min_constraint, min_dose, weight_under) );

        } else if (objective_type == "fastsquaredmeanerror") {
            bool is_min_constraint = false;
            bool is_max_constraint = false;
            float min_mean_dose = 0;
            float max_mean_dose = 0;

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "max_mean_dose", iObj)) {
                // Max mean dose constraint
                is_max_constraint = true;
                max_mean_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "max_mean_dose", iObj);

                // Set VOI parameters
                vois_[voiNo]->set_organ_at_risk(true);


                // Set legacy parameters
                vois_[voiNo]->get_prescription_ptr()->set_max_dose( max_mean_dose);
                vois_[voiNo]->get_prescription_ptr()->set_weight_over( weight_over);
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "min_mean_dose", iObj)) {
                // Min meandose constraint
                is_min_constraint = true;
                min_mean_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "min_mean_dose", iObj);

                // Set VOI parameters
                if (min_mean_dose > 0) {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(min_mean_dose);
                }

                // Set legacy parameters
                vois_[voiNo]->get_prescription_ptr()->set_min_dose( min_mean_dose );
                vois_[voiNo]->get_prescription_ptr()->set_weight_under( weight_under );
            }

            // Create objectives
            if (is_min_constraint && min_mean_dose > 0) {
                objectives_.push_back(new FastMeanObjective(vois_[voiNo], iObj+1, min_mean_dose, false, weight_under));
            }
            if (is_max_constraint) {
                objectives_.push_back(new FastMeanObjective(vois_[voiNo], iObj+1, max_mean_dose, true, weight_over));
            }
        } else if (objective_type == "squaredmeanerror") {
            bool is_min_constraint = false;
            bool is_max_constraint = false;
            float min_mean_dose = 0;
            float max_mean_dose = 0;

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "max_mean_dose", iObj)) {
                // Max mean dose constraint
                is_max_constraint = true;
                max_mean_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "max_mean_dose", iObj);

                // Set VOI parameters
                vois_[voiNo]->set_organ_at_risk(true);


                // Set legacy parameters
                vois_[voiNo]->get_prescription_ptr()->set_max_dose(
                    max_mean_dose);
                vois_[voiNo]->get_prescription_ptr()->set_weight_over(
                    weight_over);
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "min_mean_dose", iObj)) {
                // Min meandose constraint
                is_min_constraint = true;
                min_mean_dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "min_mean_dose", iObj);

                // Set VOI parameters
                if (min_mean_dose > 0) {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(min_mean_dose);
                }

                // Set legacy parameters
                vois_[voiNo]->get_prescription_ptr()->set_min_dose( min_mean_dose);
                vois_[voiNo]->get_prescription_ptr()->set_weight_under( weight_under);
            }

            // Create objectives
            objectives_.push_back(new SquaredMeanObjective( vois_[voiNo], iObj+1, weight, sampling_fraction,
                                      sample_with_replacement, is_max_constraint, max_mean_dose, weight_over,
                                      is_min_constraint, min_mean_dose, weight_under));
	}
        else if(objective_type == "geud") {
	    bool is_maximized = false;

		if(txtFileParser_->is_set_multi_attribute("OBJ", "maximize", iObj)) {
		  is_maximized = true;
		  vois_[voiNo]->set_target(true);
		  // handle maximization through negative weight of objective
		  weight = weight*(-1);
		  assert(weight<0);
	    }

	    // Create objectives
            objectives_.push_back(new GEudObjective(
                        vois_[voiNo],
                        iObj+1,
                        weight,
                        sampling_fraction,
                        sample_with_replacement,
                        is_maximized));
 	}
        else if(objective_type == "tumoreud") {

	    // Create objectives
            objectives_.push_back(new TumorEUDObjective(
                        vois_[voiNo],
                        iObj+1,
                        -1*weight,
                        sampling_fraction,
                        sample_with_replacement));
 	}
        else if(objective_type == "tcp") {

	    // get clonogen density file name
	    string dose_file_name = "clonogen_density.dat";
            if(txtFileParser_->is_set_multi_attribute("OBJ", "dose_file_name", iObj)) {
	      dose_file_name = txtFileParser_->get_multi_attribute("OBJ", "dose_file_name", iObj);
	    }

	    // lq model uncertainty
	    string lq_uncertainty_type = "none";
		if(txtFileParser_->is_set_multi_attribute("OBJ", "lq_uncertainty_type", iObj)) {
		  lq_uncertainty_type = txtFileParser_->get_multi_attribute("OBJ", "lq_uncertainty_type", iObj);
	    }

		unsigned int nFractions = 1;
		if(txtFileParser_->is_set_multi_attribute("OBJ", "nFractions", iObj)) {
		  nFractions = txtFileParser_->get_num_multi_attribute<unsigned int>( "OBJ", "nFractions", iObj );
		}
		
	    // Create objectives
		objectives_.push_back(new PoissonTcpObjective(vois_[voiNo],
													  iObj+1,
													  dose_file_name,
													  10000,
													  nFractions,
													  lq_uncertainty_type,
													  (-1)*weight,
													  geometry_.get_nVoxels()));

	}
        else if(objective_type == "logsurvival") {

	    // get clonogen density file name
	    string dose_file_name = "clonogen_density.dat";
            if(txtFileParser_->is_set_multi_attribute("OBJ", "dose_file_name", iObj)) {
	      dose_file_name = txtFileParser_->get_multi_attribute("OBJ", "dose_file_name", iObj);
	    }

	    // lq model uncertainty
	    string lq_uncertainty_type = "none";
		if(txtFileParser_->is_set_multi_attribute("OBJ", "lq_uncertainty_type", iObj)) {
		  lq_uncertainty_type = txtFileParser_->get_multi_attribute("OBJ", "lq_uncertainty_type", iObj);
	    }

		unsigned int nFractions = 1;
		if(txtFileParser_->is_set_multi_attribute("OBJ", "nFractions", iObj)) {
		  nFractions = txtFileParser_->get_num_multi_attribute<unsigned int>( "OBJ", "nFractions", iObj );
		}
		
	    // Create objectives
		objectives_.push_back(new LogSurvivalObjective(vois_[voiNo],
													   iObj+1,
													   dose_file_name,
													   10000,
													   nFractions,
													   lq_uncertainty_type,
													   weight,
													   geometry_.get_nVoxels()));

	}
        else if(objective_type == "voxeldose") {
            bool is_min_constraint = false;
            bool is_max_constraint = false;

            if(txtFileParser_->is_set_multi_attribute(
                        "OBJ", "min_dose", iObj)) {

                // we want to exceed the reference dose
                is_min_constraint = true;
	    }

            if(txtFileParser_->is_set_multi_attribute(
                        "OBJ", "max_dose", iObj)) {
                // we want to stay below the reference dose
                is_max_constraint = true;
	    }

	    // get dose file name
	    string dose_file_name = "reference_dose.dat";
            if(txtFileParser_->is_set_multi_attribute(
                        "OBJ", "dose_file_name", iObj)) {

	      dose_file_name = txtFileParser_->get_multi_attribute("OBJ", "dose_file_name", iObj);
	    }


            objectives_.push_back(new VoxelMaxDoseObjective(
                        vois_[voiNo], iObj+1,
			dose_file_name, geometry_.get_nVoxels(), weight,
                        sampling_fraction, sample_with_replacement,
                        is_max_constraint, weight_over,
                        is_min_constraint, weight_under));
 	}
        else if(objective_type == "maxmin") {
            objectives_.push_back(new MaxMinOrMinMaxObjective(
				      vois_[voiNo], iObj+1, false, -1*weight));
	}
        else if(objective_type == "minmax") {
            objectives_.push_back(new MaxMinOrMinMaxObjective(
				      vois_[voiNo], iObj+1, true, weight));
	}
        else if(objective_type == "maxmean") {
	    objectives_.push_back(new LinearMeanObjective(
				      vois_[voiNo], iObj+1, false, -1*weight));
	}
        else if(objective_type == "minmean") {
	    objectives_.push_back(new LinearMeanObjective(
				      vois_[voiNo], iObj+1, true, weight));
	}
        else if(objective_type == "minimizemean") {
	    objectives_.push_back(new MeanDoseObjective(
							vois_[voiNo], iObj+1, weight, 1.0, false, true));
	}
        else if(objective_type == "maximizemean") {
	    objectives_.push_back(new MeanDoseObjective(
							vois_[voiNo], iObj+1, -1*weight, 1.0, false, false));
	}
        else if (objective_type == "dvhcoverage") {
            // Indicator variable that doesn't affect the optimization
            bool is_min_constraint = false;
            bool is_max_constraint = false;
            float dose = 0;
            float min_volume = 0;
            float max_volume = 100;

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "dose", iObj)) {
                // Read dose
                dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "dose", iObj);
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "min_volume", iObj)) {
                // Read min_volume
                min_volume = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "min_volume", iObj);
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "max_volume", iObj)) {
                // Read max_volume
                max_volume = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "max_volume", iObj);
            }

            // Create objectives
            objectives_.push_back(new DvhCoverageObjective( vois_[voiNo], iObj+1, weight, sampling_fraction,
                                      sample_with_replacement, dose, min_volume / 100.0f, max_volume / 100.0f ));
        } else if (objective_type == "expectedquadratictensor") {

            float dose = 0;
            bool read_qjk = false;
            string qjk_file_root = get_name();

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "read_qjk", iObj)) {
                read_qjk = true;
            }

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "qjk_file_root", iObj)) {
                qjk_file_root = txtFileParser_->get_multi_attribute( "OBJ", "qjk_file_root", iObj);
            }
            if (txtFileParser_->is_set_multi_attribute( "OBJ", "dose", iObj)) {
                // prescribed dose
                dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "dose", iObj);

                // Set Voi parameters
                vois_[voiNo]->get_prescription_ptr()->set_max_dose(dose);
                vois_[voiNo]->get_prescription_ptr()->set_weight_over(weight);

                if (dose > 0)
                {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(dose);
                    vois_[voiNo]->get_prescription_ptr()->set_min_dose(dose);
                    vois_[voiNo]->get_prescription_ptr()->set_weight_under(weight);
                } else {
                    vois_[voiNo]->set_organ_at_risk(true);
                }
            }

            // create objective
            objectives_.push_back(new ExpectedQuadraticObjectiveTensor( vois_[voiNo], iObj+1, *dij_,
                                        *dose_delivery_model_, dose, weight, qjk_file_root, read_qjk));

        } else if (objective_type == "expectedquadraticintegrate") {

            float dose = 0;

            if (txtFileParser_->is_set_multi_attribute( "OBJ", "dose", iObj))
            {
                // prescribed dose
                dose = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "dose", iObj);

                // Set Voi parameters
                vois_[voiNo]->get_prescription_ptr()->set_max_dose(dose);
                vois_[voiNo]->get_prescription_ptr()->set_weight_over(weight);

                if (dose > 0) {
                    vois_[voiNo]->set_target(true);
                    vois_[voiNo]->set_desired_dose(dose);
                    vois_[voiNo]->get_prescription_ptr()->set_min_dose(dose);
                    vois_[voiNo]->get_prescription_ptr()->set_weight_under(weight);
                } else {
                    vois_[voiNo]->set_organ_at_risk(true);
                }
            }

            // create objective
            objectives_.push_back(new ExpectedQuadraticObjectiveIntegrate( vois_[voiNo], iObj+1, *dose_delivery_model_,
                                      dose, weight, sampling_fraction));

        } else if (objective_type == "dvh") {

		  string min_dvh_points = "";
		  bool is_min_obj = false;
		  if(txtFileParser_->is_set_multi_attribute("OBJ", "min_dose", iObj)) {
			min_dvh_points = txtFileParser_->get_multi_attribute("OBJ", "min_dose", iObj);
			is_min_obj = true;
		  }

		  string max_dvh_points = "";
		  bool is_max_obj = false;
		  if(txtFileParser_->is_set_multi_attribute("OBJ", "max_dose", iObj)) {
			max_dvh_points = txtFileParser_->get_multi_attribute("OBJ", "max_dose", iObj);
			is_max_obj = true;
		  }

		  // intepret input
		  DvhObjective::DvhInputParser min_input(min_dvh_points);
		  DvhObjective::DvhInputParser max_input(max_dvh_points);

		  // create objective
		  objectives_.push_back( new DvhObjective( vois_[voiNo], iObj+1, is_max_obj, is_min_obj,
												   max_input.get_dvh_points(), min_input.get_dvh_points(),
												   weight, sampling_fraction, sample_with_replacement));

        } else if (objective_type == "sfud") {

		  float tol = 0;
		  if (txtFileParser_->is_set_multi_attribute( "OBJ", "tolerance", iObj)) {
			// prescribed dose
			tol = txtFileParser_->get_num_multi_attribute<float>( "OBJ", "tolerance", iObj);
		  }

		  // create objective
		  objectives_.push_back( new SfudHomogeneityObjective( vois_[voiNo], iObj+1, get_geometry(), tol, weight));

        } else {
            txtFileParser_->report_error("Unknown objective type: " + objective_type);
        }
    }
    /*
        else if(attribute == "max_DVH") {
            vois_[voiNo]->set_organ_at_risk(true);
            prescription_dose = read_float(inFile);
            prescription_volume = read_float(inFile);
            vois_[voiNo]->get_prescription_ptr()->add_max_DVH_point(
                    prescription_dose, prescription_volume);
        }
        else if(attribute == "min_DVH") {
            prescription_dose = read_float(inFile);
            prescription_volume = read_float(inFile);
            vois_[voiNo]->get_prescription_ptr()->add_min_DVH_point(
                    prescription_dose, prescription_volume);
            vois_[voiNo]->set_target(true);
            vois_[voiNo]->set_desired_dose(prescription_dose);
        }
        else if(attribute == "max_EUD") {
            prescription_dose = read_float(inFile);
            vois_[voiNo]->get_prescription_ptr()->set_max_EUD(
                    prescription_dose);
            vois_[voiNo]->set_organ_at_risk(true);
        }
        else if(attribute == "min_EUD") {
            vois_[voiNo]->set_target(true);
            prescription_dose = read_float(inFile);
            vois_[voiNo]->get_prescription_ptr()->set_min_EUD(
                    prescription_dose);
            vois_[voiNo]->set_desired_dose(prescription_dose);
        }
    */
}


/**
 * create meta objectives
 */
void Plan::create_meta_objectives() {
    unsigned int nMetaObjs_in_pln_file = txtFileParser_->get_multi_attribute_nThings( "METAOBJ");

    for (unsigned int iObj = 0; iObj < nMetaObjs_in_pln_file; iObj++) {

        // figure out what type of objective we have
        string objective_type = to_lower(txtFileParser_->get_multi_attribute( "METAOBJ", "type", iObj));

        // Read number of samples
        unsigned int nSamples = 1;
        if (txtFileParser_->is_set_multi_attribute("METAOBJ", "nSamples", iObj)) {
            nSamples = txtFileParser_->get_num_multi_attribute<unsigned int>( "METAOBJ", "nSamples", iObj);
        }

        // Read tail percentage for cvar objective
        float tail_percentage = 100.0;
        if (txtFileParser_->is_set_multi_attribute("METAOBJ", "tail_percentage", iObj)) {
            tail_percentage = txtFileParser_->get_num_multi_attribute<float>( "METAOBJ", "tail_percentage", iObj);
        }

        // Read objective numbers to be eaten
        vector<unsigned int> objNos_to_eat(0);
        if (txtFileParser_->is_set_multi_attribute( "METAOBJ", "objectives",iObj)) {
            std::istringstream iss(txtFileParser_->get_multi_attribute( "METAOBJ", "objectives",iObj));
            int temp;
            while (!(iss >> temp).fail()) {
                objNos_to_eat.push_back(temp);
            }
        }

        // Eat the objectives
        vector<Objective*> eaten_objectives(0);
        eaten_objectives.reserve(objNos_to_eat.size());
        for (unsigned int i = 0; i < objNos_to_eat.size(); i++) {
            bool ate_obj = false;
            vector<Objective*>::iterator iter = objectives_.begin();
            while (iter!=objectives_.end()) {
                if ((*iter)->get_objNo() == objNos_to_eat[i]) {
                    // add pointer to objective to the vector of eaten objectives
                    eaten_objectives.push_back(*iter);

                    // Remove objective from list
                    objectives_.erase(iter);

                    ate_obj = true;
                    break;
                } else {
                    iter++;
                }
            }
            if (!ate_obj) {
                char cdummy[10];
                sprintf(cdummy,"%du",objNos_to_eat[i]);
                txtFileParser_->report_error("Objective " + string(cdummy) + " does not exist.\n"
                                          + "Cannot create " + objective_type + " meta-objective");
            }
        }

        // Create MetaObjective of the right type
        if (objective_type == "expectedvalue") {

            // Creating ExpectedValueMetaObjective
            // create the meta objective
		  meta_objectives_.push_back(new ExpectedValueMetaObjective(iObj+1,  &geometry_, 
																	*dose_delivery_model_,
																	eaten_objectives));

        } else if (objective_type == "cvar") {

	  cout << "Warning, CVAR meta objective temporarily disabled.\n" << endl;
          throw( std::runtime_error( "CVAR meta objective doesn't work."));

            // create CVAR meta objective
            // meta_objectives_.push_back(new CondValueAtRiskMetaObjective( &geometry_, *dose_delivery_model_,
            //                                eaten_objectives, tail_percentage / 100.0));
        } else {
            // Unknown meta objective type
            txtFileParser_->report_error("Unknown meta-objective: " + objective_type);
        }
    }
}


/**
 *  Get the sampling fraction for each objective
 *  Gets the number for each objective contained in a multi-objective as well.
 */
vector<float> Plan::get_objective_sampling_fractions() const {
    unsigned int nTotal_objectives = get_total_n_objectives();

    vector<float> sampling_fraction(0);
    sampling_fraction.reserve(nTotal_objectives);

    // Read sampling fraction for normal objectives
    for (unsigned int i = 0; i < objectives_.size(); i++) {
        sampling_fraction[i] = objectives_[i]->get_sampling_fraction();
    }

    // Get Sampling fraction for multi-objectives
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
        meta_objectives_[i]->get_sampling_fraction(sampling_fraction);
    }
    return sampling_fraction;
}


/**
 *  Set the sampling fraction for each objective
 */
void Plan::set_objective_sampling_fractions(const vector<float> & sampling_fractions) {
    assert(sampling_fractions.size() == get_total_n_objectives());
    unsigned int nObjectives = objectives_.size();
    vector<float>::const_iterator iter = sampling_fractions.begin();
    for (unsigned int i = 0; i < nObjectives; i++) {
        objectives_[i]->set_sampling_fraction(*iter);
        iter++;
    }
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
        iter = meta_objectives_[i]->set_sampling_fraction(iter);
    }
    assert(iter == sampling_fractions.end());
}


/**
 *  Get the number of voxels for each objective
 */
vector<unsigned int> Plan::get_objective_nVoxels() const {
    unsigned int n_total_objectives = get_total_n_objectives();
	vector<unsigned int> temp(n_total_objectives);
    temp.resize(0);

    // Get values for normal objectives
    for (unsigned int i = 0; i < objectives_.size(); i++) {
        temp.push_back(objectives_[i]->get_nVoxels());
    }

    // Get values for meta objectives
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
        meta_objectives_[i]->get_nVoxels(temp);
    }
    return temp;
}


/**
 *  Get list of which objectives support sampling
 */
vector<bool> Plan::get_objective_supports_sampling() const {
    unsigned int n_total_objectives = get_total_n_objectives();
    vector<bool> temp(n_total_objectives);
    temp.resize(0);

    // Get values for normal objectives
    for (unsigned int i = 0; i < objectives_.size(); i++) {
        temp.push_back(objectives_[i]->supports_voxel_sampling());
    }

    // Get values for meta objectives
    for (unsigned int i = 0; i < meta_objectives_.size(); i++) {
        meta_objectives_[i]->supports_voxel_sampling(temp);
    }
    return temp;
}


/**
 * Sets the intensity values for the bixel vector to those of the passed in bixel vector.
 * Does not change any additional traits related to parameter_vector_ being a KonradBixelVector.
 *
 * @param rhs The ParameterVector to copy
 */
void Plan::set_parameter_vector(const ParameterVector & rhs) {
    assert( rhs.get_nParameters() == parameter_vector_->get_nParameters() );
    parameter_vector_ = rhs.get_copy();
}

void Plan::set_parameter_vector(const unsigned int index, const float value) {
    assert( index < parameter_vector_->get_nParameters() );
    parameter_vector_->set_value(index,value);
}


/**
 * Swaps the intensity values for the bixel vector with those of the passed in bixel vector.
 * Does not change any additional traits related to parameter_vector_ being a KonradBixelVector.
 *
 * @param rhs The ParameterVector to swap
 */
void Plan::swap_parameter_vector( ParameterVector & rhs ) {
    assert(rhs.get_nParameters() == parameter_vector_->get_nParameters());
    parameter_vector_->swap(rhs);
}


/**
 * Writes the nominal dvh and dose files with the given prefix.
 *
 * @param file_prefix The prefix for the files.  For example, if the prefix is "dummy_", the dose file
 *           will be written as "dummy_dose.dat", "dummy_DVH.dat", etc.
 * @param CT_dose_flag write dose on planning CT grid
 */
void Plan::write_dose(string file_prefix, bool CT_dose_flag) {
    cout << "write nominal dose ..." << endl;

    // calculate nominal dose
    DoseVector dose(get_nVoxels());
    calculate_dose(dose);

    // Write dose file
    string dose_file = file_prefix + DOSE_FILE_SUFFIX;
    dose.write_dose(dose_file);

    // write dose on planning CT grid
    if (CT_dose_flag) {
        string dose_file_CT = file_prefix + "CT_" + DOSE_FILE_SUFFIX;
        geometry_.write_dose_on_planning_CT(dose, dose_file_CT);
    }

    // Calculate DVH
    Dvh temp_dvh = get_empty_Dvh();
    temp_dvh.calculate(dose);

    // Write DVH file
    string dvh_file_name = file_prefix + DVH_FILE_SUFFIX;
    temp_dvh.write_DVH(dvh_file_name);
}


/**
 * Calculate and write the nominal dvh file.
 *
 * @param file_name The name for the dvh file.
 */
void Plan::write_DVH(string dvh_file_name) {
    // calculate nominal dose
    DoseVector dose(get_nVoxels());
    calculate_dose(dose);

    // Calculate DVH
    Dvh temp_dvh = get_empty_Dvh();
    temp_dvh.calculate(dose);

    // Write DVH file
    temp_dvh.write_DVH(dvh_file_name);
}


/**
 * Calculate and append the dvh to a file.
 *
 * @param file_name The name for the dvh file.
 */
void Plan::append_DVH(string dvh_file_name) {
    // calculate nominal dose
    DoseVector dose(get_nVoxels());
    calculate_dose(dose);

    // Calculate DVH
    Dvh temp_dvh = get_empty_Dvh();
    temp_dvh.calculate(dose);

    // Write DVH file
    temp_dvh.append_DVH(dvh_file_name);
}


/**
 * Writes the dose files with the contribution from each beam.
 *
 * @param file_prefix The prefix for the files.  For example, if the prefix is "dummy_", the files will be written
 *          as "dummy_beam_1_dose.dat", etc.  If there are more than 9 beams, the numbers will be written as
 *          01, 02, and so on.
 */
void Plan::write_beam_dose(string file_prefix) {
    string dose_file_suffix = "dose.dat";
    string outfile;
    char cbeam[10];

    DoseVector dose(get_nVoxels());

    cout << "write dose distributions of individual beams ..." << endl;

    // Write dose distributions of individual beams for nominal case
    unsigned int nBeams = get_nBeams();
    for (unsigned int iBeam=0; iBeam<nBeams; iBeam++) {
        // Calculate dose
        calculate_beam_dose(dose, iBeam);

        // Write dose
        if (nBeams < 9) {
            sprintf(cbeam,"%d",iBeam+1);
        } else if (nBeams < 99) {
            sprintf(cbeam,"%02d",iBeam+1);
        } else if (nBeams < 999) {
            sprintf(cbeam,"%03d",iBeam+1);
        } else {
            sprintf(cbeam,"%04d",iBeam+1);
        }
        outfile = file_prefix + "beam_" + string(cbeam) + '_' + dose_file_suffix;
        dose.write_dose(outfile);
    }
}


/**
 * Writes the dose and DVH files with the contribution from each instance.
 *
 * @param file_prefix   The prefix for the files.  For example, if the prefix is "dummy_", the files will be written
 *                      as "dummy_inst_1_dose.dat", "dummy_inst_1_DVH.dat", etc.  If there are more than 9 beams, the
 *                      numbers will be written as 01, 02, and so on.
 */
void Plan::write_instance_dose(string file_prefix) {
    string dose_file_suffix = "dose.dat";
    string DVH_file_suffix = "DVH.dat";
    string dose_file, dvh_file;
    char c_str[10];

    DoseVector dose(get_nVoxels());
    Dvh dvh = get_empty_Dvh();

    // write dose distribution and DVH for instances
    unsigned int nInst = get_nInstances();
    for (unsigned int iInstance = 0; iInstance < nInst; iInstance++) {
        // calculate dose from this instance alone
        calculate_instance_dose(dose, iInstance);

        // Calculate DVH for this instance
        dvh.calculate(dose);

        // Determine file names
        if (nInst < 9) {
            sprintf(c_str,"%d",iInstance+1);
        } else if (nInst < 99) {
            sprintf(c_str,"%02d",iInstance+1);
        } else if (nInst < 999) {
            sprintf(c_str,"%03d",iInstance+1);
        } else {
            sprintf(c_str,"%04d",iInstance+1);
        }

        dose_file = file_prefix + "inst_" + string(c_str) + '_' + dose_file_suffix;
        dvh_file = file_prefix + "inst_" + string(c_str) + '_' + DVH_file_suffix;

        dose.write_dose(dose_file);
        dvh.write_DVH(dvh_file);
    }
}


/**
 * Writes the dose and DVH for scenarios specified by a dose delivery model
 *
 * @param file_prefix The prefix for the files.
 */
void Plan::write_scenario_dose(string file_prefix) {
    Dvh dvh = get_empty_Dvh();
    std::auto_ptr<BixelVector> temp( parameter_vector_->translateToBixelVector() );
    dose_delivery_model_->write_dose( *temp, *dij_, file_prefix, dvh );
}


/**
 * Writes the dose and DVH files with the contribution from each auxilary.
 *
 * @param file_prefix   The prefix for the files.  For example, if the prefix is "dummy_", the files will be written
 *                      as "dummy_aux_1_dose.dat", "dummy_aux_1_DVH.dat", etc.  If there are more than 9 beams, the
 *                      numbers will be written as 01, 02, and so on.
 */
void Plan::write_auxiliary_dose(string file_prefix) {
    string dose_file_suffix = "dose.dat";
    string DVH_file_suffix = "DVH.dat";
    string dose_file, dvh_file;
    char c_str[10];

    DoseVector dose(get_nVoxels());
    Dvh dvh = get_empty_Dvh();

    // write dose distribution and DVH for auxiliaries
    unsigned int nAux = get_nAuxiliaries()+1;
    for (unsigned int iAux=0; iAux < nAux; iAux++) {
        // calculate dose from this auxiliary alone
        calculate_auxiliary_dose(dose, iAux);

        // Calculate DVH for this auxiliary
        dvh.calculate(dose);

        // Determine file names
        if (nAux < 10) {
            sprintf(c_str,"%d",iAux);
        } else if (nAux < 99) {
            sprintf(c_str,"%02d",iAux);
        } else if (nAux < 999) {
            sprintf(c_str,"%03d",iAux);
        } else {
            sprintf(c_str,"%04d",iAux);
        }

        dose_file = file_prefix + "aux_" + string(c_str) + '_' + dose_file_suffix;
        dvh_file = file_prefix + "aux_" + string(c_str) + '_' + DVH_file_suffix;

        // Write files
        dose.write_dose(dose_file);
        dvh.write_DVH(dvh_file);
    }
}


/**
 * Writes the variance files if appropriate.
 *
 * @param file_prefix The prefix for the file.
 *              For example, if the prefix is "dummy_", the dose file will be written as "dummy_variance.dat".
 */
void Plan::write_variance(string file_prefix) {
    const string variance_file_suffix = "variance.dat";
    const string vvh_file_suffix = "variance_DVH.dat";

    // write variance if it is supported
    if (dose_delivery_model_->supports_calculate_variance()) {
        // calculate variance
        DoseVector variance(get_nVoxels());
        std::auto_ptr<BixelVector> temp( parameter_vector_->translateToBixelVector() );
        dose_delivery_model_->calculate_variance(variance, *temp, *dij_);

        // write variance
        string variance_file = file_prefix + variance_file_suffix;
        variance.write_dose(variance_file);

        // Calculate Variance Volume Histogram
        Dvh vvh = get_empty_Dvh();
        vvh.calculate(variance);

        // Write Variance Volume Histogram
        string vvh_file = file_prefix + vvh_file_suffix;
        vvh.write_DVH(vvh_file);
    }
}


/**
 * calculate and write the average DVH, the standard deviation of the dose and samples of objective value
 */
void Plan::write_objective_samples(string file_prefix) {
    const string scenario_file_suffix = "scenarios.dat";
    const string std_dev_file_suffix = "std_dev.dat";
    const string obj_samples_file_suffix = "obj_samples.dat";
    const string multi_obj_samples_file_suffix = "multi_obj_samples.dat";
    const string expected_dose_file_suffix = "expected_dose.dat";
    const string exp_dvh_file_suffix = "expected_DVH.dat";
    const string vvh_file_suffix = "std_dev_DVH.dat";
    string file_name;

    double objective;
    vector<double> multi_objective;
    DoseVector dose(get_nVoxels());
    DoseVector exp_dose(get_nVoxels());
    DoseVector exp_dose2(get_nVoxels());
    DoseVector std_dev(get_nVoxels());
    Dvh dvh = get_empty_Dvh();
    RandomScenario scenario;

    // number of samples
    unsigned int nSamples = options_.nObjective_samples_;

    // expected objective
    double exp_obj = 0;
    vector<double> exp_multi_objective;

    // expected dvh
    Dvh exp_dvh = get_empty_Dvh();

    // vector to store sample objective values
    vector<double> objective_samples;
    objective_samples.reserve(nSamples);

    // vector to store sample multi-objective values
    vector<vector<double> > multi_objective_samples;
    multi_objective_samples.reserve(nSamples);

    cout << "calculating " << nSamples << " objective samples and statistics ..." << endl;

    // open file for scenario samples
    file_name = file_prefix + scenario_file_suffix;
    std::ofstream ofile_scen(file_name.c_str(), std::ios::out | std::ios::trunc);

    // generate the samples
    for (unsigned int iSample=0;iSample<nSamples; iSample++) {
        // generate a random scenario
        dose_delivery_model_->generate_random_scenario(scenario);

        // calculate dose
        std::auto_ptr<BixelVector> temp( parameter_vector_->translateToBixelVector() );
        dose_delivery_model_->calculate_dose(dose, *temp, *dij_, scenario);

        // Calculate DVH
        dvh.calculate(dose);

        // average DVH
        exp_dvh += dvh;

        // get objective
        objective = calculate_dose_objective( dose, multi_objective );

        // store objective
        objective_samples.push_back(objective);
        multi_objective_samples.push_back(multi_objective);

        // average objective
        exp_obj += objective;

        // Average mult-objective
        for (unsigned int iObj = 0;iObj < multi_objective.size(); iObj++) {
            if (iObj >= exp_multi_objective.size()) {
                exp_multi_objective.push_back(multi_objective[iObj]);
            } else {
                exp_multi_objective[iObj] += multi_objective[iObj];
            }
        }

        // average dose
        for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
            exp_dose[iVoxel] += dose[iVoxel];
            exp_dose2[iVoxel] += dose[iVoxel]*dose[iVoxel];
        }

        // Write scenario samples
        ofile_scen << scenario << "\n";

        // print progress on screen
        cout << "." << std::flush;
        if (((iSample+1) % 10)==0) {
            cout << iSample+1 << std::flush;
        }
    }

    // close file for scenarios
    ofile_scen.close();

    // normalize average
    exp_dvh *= (1.0/nSamples);
    exp_obj *= (1.0/nSamples);

    cout << endl << "final estimated expected objective: " << exp_obj << endl;

    // calculate standard deviation of dose
    for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
        exp_dose[iVoxel] *= (1.0/nSamples);
        exp_dose2[iVoxel] *= (1.0/nSamples);
        std_dev[iVoxel] = exp_dose2[iVoxel] - (exp_dose[iVoxel]*exp_dose[iVoxel]);
        if (std_dev[iVoxel]>0) {
            std_dev[iVoxel] = sqrt(std_dev[iVoxel]);
        } else {
            std_dev[iVoxel] = 0;
        }
    }

    // write expected dose
    file_name = file_prefix + expected_dose_file_suffix;
    exp_dose.write_dose(file_name);

    // write standard deviation
    file_name = file_prefix + std_dev_file_suffix;
    std_dev.write_dose(file_name);

    // Write expected DVH
    file_name = file_prefix + exp_dvh_file_suffix;
    exp_dvh.write_DVH(file_name);

    // Calculate Variance Volume Histogram
    file_name = file_prefix + vvh_file_suffix;
    dvh.calculate(std_dev);
    dvh.write_DVH(file_name);

    // Write objective samples
    {
        file_name = file_prefix + obj_samples_file_suffix;
        std::ofstream ofile(file_name.c_str(), std::ios::out | std::ios::trunc);
        for (unsigned int iSample=0;iSample<nSamples; iSample++) {
            ofile << objective_samples[iSample] << "\n";
        }
        ofile.close();
    }

    // Write multi-objective samples
    {
        file_name = file_prefix + multi_obj_samples_file_suffix;
        std::ofstream ofile(file_name.c_str(), std::ios::out | std::ios::trunc);
        for (unsigned int iSample=0;iSample<nSamples; iSample++) {
            for (unsigned int iObj=0;iObj<multi_objective_samples[iSample].size(); iObj++) {
                if (iObj > 0) {
                    ofile << "\t";
                }

                ofile << multi_objective_samples[iSample][iObj];
            }
            ofile << "\n";
        }
        ofile.close();
    }

}


/**
 * calculate and write the expected dose, expected DVH, and DVH of the expected dose
 *
 * @param file_prefix The prefix for the files
 * @param nScenarios The number of scenarios to sample (-1 for all)
 */
void Plan::write_expected_dose(string file_prefix, int nScenarios) {
    const string expected_dose_file_suffix = "expected_dose.dat";
    const string exp_dvh_file_suffix = "expected_DVH.dat";
    const string exp_dose_dvh_file_suffix = "expected_dose_DVH.dat";
    const string std_dev_file_suffix = "std_dev.dat";
    string file_name;

    DoseVector dose(get_nVoxels());
    DoseVector exp_dose(get_nVoxels());
    DoseVector exp_dose2(get_nVoxels());
    Dvh dvh = get_empty_Dvh();

    RandomScenario scenario;

    // number of samples
    unsigned int nSamples;
    if(dose_delivery_model_->has_discrete_scenarios()){
      nSamples = dose_delivery_model_->get_nDose_eval_scenarios();
      cout << "calculating expected dose with all " << nSamples << " scenarios\n";
    }
    else {
      nSamples = nScenarios;
      cout << "calculating expected dose with " << nSamples << " scenario samples\n";
    }

    // expected dvh
    Dvh exp_dvh = get_empty_Dvh();

    // generate the samples
    for (unsigned int iSample=0; iSample<nSamples; iSample++) {

        // generate random scenario and probabilty
        float prob;
        if (dose_delivery_model_->has_discrete_scenarios()) {
            // Select numbered scenario
            scenario = dose_delivery_model_->get_dose_eval_scenario(iSample);

            // get probability of the scenario
            prob = dose_delivery_model_->get_dose_eval_pdf(iSample);
        } else {
            // generate random scenario
            dose_delivery_model_->generate_random_scenario(scenario);

            prob = 1.0 / nSamples;
        }

        // calculate dose
        std::auto_ptr<BixelVector> temp( parameter_vector_->translateToBixelVector() );
        dose_delivery_model_->calculate_dose( dose, *temp, *dij_, scenario );

        // Calculate DVH
        dvh.calculate(dose);

        // Scale DVH
        dvh *= prob;

        // add DVH to expected dvh
        exp_dvh += dvh;

        // Add dose to expected dose and expected dose squared
        for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
            float temp = dose[iVoxel];
            exp_dose[iVoxel] += temp * prob;
            exp_dose2[iVoxel] += temp * temp * prob;
        }
    }

    // Calculate dvh of expected dose
    Dvh exp_dose_dvh = get_empty_Dvh();
    exp_dose_dvh.calculate(exp_dose);

    // calculate standard deviation of dose
    DoseVector std_dev(get_nVoxels());
    for (unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++) {
        std_dev[iVoxel] = exp_dose2[iVoxel] - (exp_dose[iVoxel]*exp_dose[iVoxel]);
        if (std_dev[iVoxel]>0) {
            std_dev[iVoxel] = sqrt(std_dev[iVoxel]);
        } else {
            std_dev[iVoxel] = 0;
        }
    }

    // write expected dose
    file_name = file_prefix + expected_dose_file_suffix;
    exp_dose.write_dose(file_name);

    // write standard deviation
    file_name = file_prefix + std_dev_file_suffix;
    std_dev.write_dose(file_name);

    // Write expected DVH
    file_name = file_prefix + exp_dvh_file_suffix;
    exp_dvh.write_DVH(file_name);

    // Write DVH of expected dose
    file_name = file_prefix + exp_dose_dvh_file_suffix;
    exp_dose_dvh.write_DVH(file_name);
}

/**
 * write lagrange parameters to a dose cube
 */
void Plan::write_lagrange_multipliers(string file_prefix)
{
  cout << "write lagrange multipliers" << endl;

    string file_suffix = "lagrange.dat";
    string file;

    DoseVector lagrange(get_nVoxels());

    // write lagrange multipliers to dose cubes
    for (unsigned int i=0; i < get_nConstraints(); i++) {
	vector<pair<unsigned int,float> > data = constraints_[i]->get_lagrange_multipliers();
	for(unsigned int j=0; j<data.size(); j++) {
	    lagrange.set_voxel_dose(data[j].first,data[j].second);
	}
    }

    // Write files
    file = file_prefix + file_suffix;
    lagrange.write_dose(file);
}


/**
 * Get a DVH object with no dose.
 */
Dvh Plan::get_empty_Dvh() const {
    vector< const Voi *> voi_ptrs;
    for (unsigned i = 0; i < get_nVois(); i++) {
        voi_ptrs.push_back(vois_[i]);
    }
    return Dvh(voi_ptrs, options_.dvh_bin_size_);
}



/**
 * create the DoseInfluenceMatrix and the ParameterVector
 *
 * not used yet
 */
void Plan::create_dij_and_parameters( string dij_file_root,  string bwf_file_root,  string stf_file_root )
{
  string parameter_type = txtFileParser_->get_attribute("parameter_type");

  if (parameter_type == "bixel") {

    if(options_.read_auxiliary_dij_) {
      // read multiple instances as auxiliaries
      dij_.reset(new KonradDoseInfluenceMatrix(dij_file_root, &geometry_, true, nBeams_, nInstances_, false));

      // Construct the beams based on the information from the Dij; note: ,nInstances_ = 1
      parameter_vector_.reset(new KonradBixelVector(*dij_,&geometry_,1,nBeams_));
    }
    else if (options_.read_beamlets_first_) {
      // read beam steering file
      parameter_vector_.reset(new KonradBixelVector(&geometry_,nInstances_,nBeams_,bwf_file_root, stf_file_root));

      // Load dij files
      BixelVector * bix = parameter_vector_->translateToBixelVector();
      dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
						*bix, options_.transpose_when_reading_Dij_));
      delete bix;
    }
    else {
      // read Dij
      dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
						options_.transpose_when_reading_Dij_));

      // Construct the beams based on the information from the Dij
      parameter_vector_.reset(new KonradBixelVector(*dij_,&geometry_,nInstances_,nBeams_));
    }
  } else if (parameter_type == "squarerootbixel") {

    if (options_.read_beamlets_first_) {
      // read beam steering file
      parameter_vector_.reset(new KonradSquareRootBixelVector(&geometry_,nInstances_,nBeams_, options_.min_beam_weight_,
															  bwf_file_root, stf_file_root));

      // Load dij files
      BixelVector * bix = parameter_vector_->translateToBixelVector();
      dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
						*bix, options_.transpose_when_reading_Dij_));
      delete bix;
    }
    else {
      // read Dij
      dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
						options_.transpose_when_reading_Dij_));

      // Construct the beams based on the information from the Dij
      parameter_vector_.reset(new KonradSquareRootBixelVector(*dij_,&geometry_,nInstances_,nBeams_, options_.min_beam_weight_));
    }
  } else if (parameter_type == "dao") {

	// read Dij
	dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
											  options_.transpose_when_reading_Dij_));

	// Construct the beams based on the information from the Dij
	parameter_vector_.reset(new DaoVector(&geometry_, dij_.get()));

  } else {
    cout << "unknown type of parameter vector" << endl;
    throw(plan_pln_ex);
  }

  // copy bixel vector
  backup_parameter_vector_ = parameter_vector_->get_copy();
}



/**
 * create the dose delivery model. This includes:
 * - the DoseInfluenceMatrix
 * - the uncertainty model
 * - the bixel vector
 */
void Plan::create_dose_delivery_model( string dij_file_root,  string bwf_file_root,  string stf_file_root ) {
    // get the Uncertainty parameters
    DoseDeliveryModel::options options(txtFileParser_.get());

    // get uncertainty type
    string uncertainty_type = options.dose_delivery_model_;

    /*
      if (uncertainty_type != "multi_instance_gating" && uncertainty_type != "pdf_variation") {
        if (nInstances_!=1) {
            cout << "Error: Attempt to create a dose delivery model of type "
                << uncertainty_type << " with multiple instances." << endl;
            throw(plan_pln_ex);
        }
    }

    // first check for models that need special Dij constructors

    // Pdf variation
    if (uncertainty_type == "pdf_variation") {
        // TODO (dualta#1#): Generalise this for other ParameterVector types
        // read multiple instances as auxiliaries
        dij_.reset(new KonradDoseInfluenceMatrix(dij_file_root, &geometry_, true, nBeams_, nInstances_, false));

        // Construct the beams based on the information from the Dij; note: ,nInstances_ = 1
        parameter_vector_.reset(new KonradBixelVector(*dij_,&geometry_,1,nBeams_));
        backup_parameter_vector_ = parameter_vector_->get_copy();

        // create the uncertainty model
        create_pdf_variation_model();

    } else {

        if (options_.read_beamlets_first_) {
            // read beam steering file
            parameter_vector_.reset(new KonradBixelVector(&geometry_,nInstances_,nBeams_,bwf_file_root, stf_file_root));

			BixelVector * bix = parameter_vector_->translateToBixelVector();
            // Load dij files
            dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
							*bix, options_.transpose_when_reading_Dij_));
			delete bix;
        } else {
            // read Dij
            dij_.reset(new KonradDoseInfluenceMatrix( dij_file_root, &geometry_, nBeams_, nInstances_,
                           options_.transpose_when_reading_Dij_));

            // Construct the beams based on the information from the Dij
            parameter_vector_.reset(new KonradBixelVector(*dij_,&geometry_,nInstances_,nBeams_));
        }

        // copy bixel vector
        backup_parameter_vector_ = parameter_vector_->get_copy();
*/

    // no uncertainty
    if (uncertainty_type == "standard") {
      // create the uncertainty model
      dose_delivery_model_.reset(new StandardDoseModel(&geometry_));

      // multi instance gating
    } else if (uncertainty_type == "multi_instance_gating") {
      // create uncertainty model
      dose_delivery_model_.reset(new MultiInstanceGatingModel( &geometry_, nInstances_, nBeams_));

      // reset number of beams in plan class
      nBeams_ = nBeams_*nInstances_;

      // range uncertainty (Make sure you use the lower case word here)
    } else if (uncertainty_type == "range") {
      // create the range uncertainty model
      dose_delivery_model_.reset(new GeneralAuxiliaryRangeModel(*dij_, &geometry_, &options));

      // range uncertainty (Make sure you use the lower case word here)
    } else if (uncertainty_type == "discrete_range") {
      // create the range uncertainty model
      dose_delivery_model_.reset(new DiscreteAuxiliaryRangeModel(*dij_, &geometry_, &options));

      // setup
    } else if (uncertainty_type == "setup") {
      // read surface normals
      options.set_surface_normals(txtFileParser_.get(),&geometry_);
      // create the setup uncertainty model
      dose_delivery_model_.reset(new SetupUncertaintyModel(&geometry_, &options));

      // setup + range
    } else if (uncertainty_type == "range+setup") {
      // read surface normals
      options.set_surface_normals(txtFileParser_.get(),&geometry_);
      // create the range+setup uncertainty model
      dose_delivery_model_.reset(new RangeSetupUncertaintyModel(*dij_, &geometry_, &options));

      // systematic setup error modelled by virtual bixels
    } else if (uncertainty_type == "virtual_bixel_systematic_setup") {
      dose_delivery_model_.reset(new ContinuousVirtualBixelSetupModel(&geometry_, &options));

      // systematic range error modelled by virtual bixels
    } else if (uncertainty_type == "virtual_bixel_systematic_range") {
      dose_delivery_model_.reset(new ContinuousVirtualBixelRangeModel(&geometry_, &options));

      // systematic range+setup error modelled by virtual bixels
    } else if (uncertainty_type == "virtual_bixel_systematic_range+setup") {
      dose_delivery_model_.reset(new ContinuousVirtualBixelRangeSetupModel(&geometry_, &options));

      // random and systematic setup uncertainty
    } else if (uncertainty_type == "virtual_bixel_random_and_systematic_setup") {
      dose_delivery_model_.reset(new VirtualBixelRandomAndSystematicSetupModel(&geometry_, &options));

      // random and systematic setup uncertainty
    } else if (uncertainty_type == "virtual_bixel_discrete_range+setup") {
      dose_delivery_model_.reset(new DiscreteVirtualBixelRangeSetupModel(&geometry_, &options));

      // biologically equivalent dose
    } else if(uncertainty_type == "bed") {
      // create the BED model
      dose_delivery_model_.reset(new BiologicallyEquivalentDoseModel(&geometry_, &options, vois_,
								     nInstances_, nBeams_, options.nFractions_, 
								     options.standard_fraction_dose_));
      // reset number of beams in plan class
      nBeams_ = nBeams_*nInstances_;

      // LET times dose model
    } else if(uncertainty_type == "let") {
      // file root for LET times dose
      string lij_file_root = dij_file_root + "_let";
      // need temporary bixel vector
      //BixelVector * bix = parameter_vector_->translateToBixelVector();
      // create the LET model
      dose_delivery_model_.reset(new LETModel(&geometry_, &options, lij_file_root, stf_file_root));
      //delete bix;

    } else if(uncertainty_type == "pdf_variation") {
      // create the uncertainty model
      assert(options_.read_auxiliary_dij_);
      create_pdf_variation_model();

    } else {
      // invalid uncertainty model
      cout << "Error: invalid dose delivery model: " << uncertainty_type << endl;
      throw(plan_pln_ex);
    }

    // create dose evaluation scenarios
    set_dose_evaluation_scenarios(txtFileParser_.get());
}


/**
 * set dose evaluation scenarios
 */
void Plan::set_dose_evaluation_scenarios(const TxtFileParser* txtFileParser) const {
    // number of dose evaluation scenarios in plan file
    unsigned int nScenarios_in_pln_file =
        txtFileParser->get_multi_attribute_nThings("DoseEval");

    for (unsigned int iScen = 0; iScen < nScenarios_in_pln_file; iScen++) {

        // store plan file information in a map
        map<string,string> temp;

	// temp["type"] = txtFileParser_->get_multi_attribute("DoseEval", "type", iScen);
        temp["prob"] = txtFileParser_->get_multi_attribute("DoseEval", "prob", iScen);
        temp["range"] = txtFileParser_->get_multi_attribute("DoseEval", "range", iScen);
        temp["setup"] = txtFileParser_->get_multi_attribute("DoseEval", "setup", iScen);

        // call dose delivery model to interpret and store the scenario
        if (dose_delivery_model_->set_dose_evaluation_scenario(temp)) {
            // everything is fine
        } else {
            cout << "WARNING: error reading the dose evaluation scenario " << iScen << " in ";
            cout << dose_delivery_model_->get_uncertainty_model() << ": " << endl;
            // cout << "type: " << temp["type"] << endl;
            cout << "prob: " << temp["prob"] << endl;
            cout << "range: " << temp["range"] << endl;
            cout << "setup: " << temp["setup"] << endl;
            cout << "Will be ignored!" << endl;
        }
    }

    // normalize the probability of scenarios to one
    dose_delivery_model_->normalize_dose_eval_pdf();
}


/**
 * create dose delivery model for a variable Pdf
 */
void Plan::create_pdf_variation_model() {
    unsigned int pdf_variation_nModes = 0;
    string pdf_variation_file_name = "";

    if (get_nInstances() != get_nAuxiliaries()+1) {
        cout << "number of instances is " << get_nInstances()
            << " and number of auxiliaries + 1 is " << get_nAuxiliaries()+1
            << ". Cannot create PdfVariationModel" << endl;

        throw(plan_pln_ex);
    }

    // get plan file parameters
    pdf_variation_nModes = txtFileParser_->get_num_attribute<unsigned int>( "Uncertainty_Respiratory_nModes" );

    pdf_variation_file_name = txtFileParser_->get_attribute( "Uncertainty_Respiratory_file_name" );
    if (pdf_variation_file_name == "") {
        pdf_variation_file_name = get_name();
    }

    // create model
    dose_delivery_model_.reset(new PdfVariationModel( &geometry_, pdf_variation_file_name, get_nInstances(),
                                   pdf_variation_nModes));
}

