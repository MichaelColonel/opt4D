#include "BiologicallyEquivalentDoseModel.hpp"
#include <fstream>




// ---------------------------------------------------------------------
// Dose delivery model for Martijn's tumor dose variance approach
// ---------------------------------------------------------------------

/**
 * constructor 
 */
BiologicallyEquivalentDoseModel::BiologicallyEquivalentDoseModel(
    const Geometry *the_geometry, 
    const DoseDeliveryModel::options *options,
    vector< Voi* > the_vois, 
    unsigned int nInstances,
    unsigned int nBeams_per_instance,
	unsigned int nFractions,
	float standard_fraction_dose)
    : DoseDeliveryModel(the_geometry) 
    , the_vois_(the_vois)
    , nInstances_(nInstances)
    , bixels_instanceNo_(the_geometry->get_nBixels(),0)
    , voxels_voiNo_(the_geometry->get_nVoxels(),0)
	, nFractions_(nFractions)
    , standard_fraction_dose_(standard_fraction_dose)
{
    cout << "Constructing biologically equivalent dose model" << endl;
    cout << "number of instances: " << nInstances_ << endl;
    cout << "number of fractions per instance: " << nFractions_ << endl;

    // set index to instance for every bixel
    for(unsigned int iBixel=0;iBixel<the_geometry_->get_nBixels();iBixel++) {
	unsigned int iInstance = 0;
	bool found_it = false;
	while(!found_it) {
	    if(the_geometry_->get_beamNo(iBixel) < (iInstance+1)*nBeams_per_instance) {
		bixels_instanceNo_[iBixel] = iInstance;
		found_it = true;
	    }
	    else {
		iInstance++;
		if(iInstance >= nInstances_) {
		    cout << "Error while initializing bixels_instanceNo_" << endl;
		    exit(-1);
		}
	    }
	}
    }

    // set index to Voi for every voxel
    set_voi_indices_for_every_voxel();
}


/**
 * destructor 
 */
BiologicallyEquivalentDoseModel::~BiologicallyEquivalentDoseModel()
{
}

/**
 * Set index to Voi for every voxel: 
 * This does not allow for overlapping structures
 * Last Voi in the list has highes priority
 */
void BiologicallyEquivalentDoseModel::set_voi_indices_for_every_voxel()
{
  for(unsigned int iVoi=0;iVoi<the_vois_.size();iVoi++)
    {
      for(unsigned int i=0;i<the_vois_[iVoi]->get_nVoxels();i++)
	{
	  voxels_voiNo_[the_vois_[iVoi]->get_voxel(i)] = the_vois_[iVoi]->get_voiNo();
	}
    }
}


/**
 * calculates physical dose for an instance
 */
void BiologicallyEquivalentDoseModel::calculate_instance_dose(
    DoseVector &dose_vector,
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &dij,
    unsigned int iInstance) const
{
    // clear dose vector
    dose_vector.reset_dose();
	
    if(iInstance >= nInstances_) {
	cout << "cannot calculate dose for instance "
	     << iInstance+1 << "." << endl; 
	cout << "Number of instances is: "
	     << nInstances_ << endl;
    }
    else {
	// loop over the whole dose influence matrix and skip bixels 
	// which don't belong to the instance
	DoseInfluenceMatrix::iterator iter = dij.begin();
	while(iter.not_at_end()) {
	    if(bixels_instanceNo_[iter.get_bixelNo()] == iInstance) {
		dose_vector.add_dose(iter.get_voxelNo(), 
				     bixel_grid.get_intensity(iter.get_bixelNo()) 
				     * iter.get_influence());
	    }
	    
	    ++iter;
	}
    }
}

/**
 * calculates total biologically equivalent dose for a voxel
 */
float BiologicallyEquivalentDoseModel::calculate_voxel_dose(
    unsigned int voxelNo,	
    const BixelVector &bixel_grid,               
    DoseInfluenceMatrix &dij,
    const RandomScenario &random_scenario) const 
{
  bool oldversion = false;

  if(oldversion) {
    float totalbiodose = 0;
    
    float alphabeta = the_vois_[voxels_voiNo_[voxelNo]]->get_alphabeta();
    //  cout << voxels_voiNo_[voxelNo] << " " << alphabeta << " ";
    
    // loop over the instances
    for(unsigned int iInstance=0;iInstance<nInstances_;iInstance++)
      {
	// skip this instance if we want a specific (different) instance
	if(random_scenario.use_single_instance_ && 
	   random_scenario.instanceNo_ != iInstance)
	  {
	    continue;
	  }
	
	// calculate physical dose for this instance
	float d = calculate_physical_voxel_dose(voxelNo,iInstance,bixel_grid,dij);
	
	// convert to biological dose
	//float bio_buf = d * (1+d/alphabeta);
	float bio_buf = nFractions_*d * (1+d/alphabeta);
	
	// add up
	totalbiodose += bio_buf;
      }
    
    //return(totalbiodose / float(nInstances_));
    return(totalbiodose);
  }  
  else {
    float totalbiodose = 0;
    
    float alphabeta = the_vois_[voxels_voiNo_[voxelNo]]->get_alphabeta();
    
    // get physical dose for all instances
    vector<float> d;
    d = calculate_physical_voxel_doses(voxelNo,bixel_grid,dij);

    // loop over the instances
    for(unsigned int iInstance=0;iInstance<nInstances_;iInstance++)
      {
	// convert to biological dose
	float bio_buf = nFractions_*d[iInstance] * (1+d[iInstance]/alphabeta);
	
	// add up
	totalbiodose += bio_buf;
      }
    
    return(totalbiodose);
  }
}



/**
 * calculates gradient of total biologically equivalent dose for a voxel
 */
void BiologicallyEquivalentDoseModel::calculate_voxel_dose_gradient(
    unsigned int voxelNo,	
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &dij,
    const RandomScenario &random_scenario,
    BixelVectorDirection &gradient,
    float gradient_multiplier) const
{
  bool oldversion = false;

  if(oldversion) {
    float alphabeta = the_vois_[voxels_voiNo_[voxelNo]]->get_alphabeta();
    
    // loop over the instances
    for(unsigned int iInstance=0;iInstance<nInstances_;iInstance++)
      {
	// skip this instance if we want a specific (different) instance
	if(random_scenario.use_single_instance_ && 
	   random_scenario.instanceNo_ != iInstance)
	  {
	    continue;
	  }
	
	float d = calculate_physical_voxel_dose(voxelNo,iInstance,bixel_grid,dij);
	
	//float scale_factor = ( 1.0 + (2*d/alphabeta) );
	float scale_factor = nFractions_*( 1.0 + (2*d/alphabeta) );
	
	// calculate gradient contributions of this instance
	for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo); iter.get_voxelNo() == voxelNo; iter++) 
	  {
	    if(bixels_instanceNo_[iter.get_bixelNo()] == iInstance) {
	      gradient[iter.get_bixelNo()] += gradient_multiplier * scale_factor * iter.get_influence();
	    }
	  }
      }
  }  
  else {
    float alphabeta = the_vois_[voxels_voiNo_[voxelNo]]->get_alphabeta();
    
    // get physical dose for all instances
    vector<float> d;
    d = calculate_physical_voxel_doses(voxelNo,bixel_grid,dij);

    vector<float> scale_factor;
    scale_factor.resize(nInstances_,0);

    for(unsigned int iInstance=0;iInstance<nInstances_;iInstance++)
      {
	scale_factor[iInstance] = nFractions_*( 1.0 + (2*d[iInstance]/alphabeta) );
      }

    // calculate gradient contributions 
    for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo); iter.get_voxelNo() == voxelNo; iter++) 
      {
	gradient[iter.get_bixelNo()] += gradient_multiplier * 
	  scale_factor[bixels_instanceNo_[iter.get_bixelNo()]] * iter.get_influence();
      }
  }  
}

/**
 * calculates physical dose to a voxel for one instance
 */
float BiologicallyEquivalentDoseModel::calculate_physical_voxel_dose(unsigned int voxelNo,	
    unsigned int instanceNo,
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &dij) const
{
  float dose_buf = 0;

  for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo); iter.get_voxelNo() == voxelNo; iter++) 
    {
      if(bixels_instanceNo_[iter.get_bixelNo()] == instanceNo) {
	dose_buf += bixel_grid[iter.get_bixelNo()] * iter.get_influence();
      }
    }

  return(dose_buf);
}

/**
 * calculates physical dose to a voxel for all instances
 */
vector<float> BiologicallyEquivalentDoseModel::calculate_physical_voxel_doses(unsigned int voxelNo,	
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &dij) const
{
  vector<float> dose_buf;
  dose_buf.resize(nInstances_,0);

  for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo); iter.get_voxelNo() == voxelNo; iter++) 
    {
      dose_buf.at(bixels_instanceNo_[iter.get_bixelNo()]) = 
	dose_buf.at(bixels_instanceNo_[iter.get_bixelNo()]) +
	bixel_grid[iter.get_bixelNo()] * iter.get_influence();
    }

  return(dose_buf);
}

/**
 * does nothing so far 
 */
void BiologicallyEquivalentDoseModel::generate_random_scenario(RandomScenario &random_scenario) const
{
}

/**
 * does nothing so far 
 */
void BiologicallyEquivalentDoseModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
}

/**
 * write results
 */
void BiologicallyEquivalentDoseModel::write_dose(BixelVector &bixel_grid,
						 DoseInfluenceMatrix &dij,
						 string file_prefix, 
						 Dvh dvh) const
{
    string dose_file_suffix = "dose.dat";
    string DVH_file_suffix = "DVH.dat";
    string dose_file, dvh_file;
    char cScen[10];

    DoseVector dose(the_geometry_->get_nVoxels());
    DoseVector totaldose(the_geometry_->get_nVoxels());

    // loop over the instances
    for(unsigned int iInstance=0;iInstance<nInstances_;iInstance++)
    {
 
	// calculate dose
        calculate_instance_dose(dose,bixel_grid,dij,iInstance);
	totaldose += dose;

        // Calculate DVH 
	dvh.calculate(dose);

        // Write dose
        if(nInstances_ < 9) {
            sprintf(cScen,"%d",iInstance+1);
        } else if(nInstances_ < 99) {
            sprintf(cScen,"%02d",iInstance+1);
        } else if(nInstances_ < 999) {
            sprintf(cScen,"%03d",iInstance+1);
        } else {
            sprintf(cScen,"%04d",iInstance+1);
        }
        dose_file = file_prefix + "instance_" + string(cScen)
            + '_' + dose_file_suffix;
        dvh_file = file_prefix + "instance_" + string(cScen)
            + '_' + DVH_file_suffix;
        
        dose.write_dose(dose_file);
        dvh.write_DVH(dvh_file);
    }

    // output total physical dose
    //totaldose.scale_dose(1.0/(float)nInstances_);
    dvh.calculate(totaldose);

    dose_file = file_prefix + "physical_" + dose_file_suffix;
    dvh_file = file_prefix + "physical_" + DVH_file_suffix;

    totaldose.write_dose(dose_file);
    dvh.write_DVH(dvh_file);

    // output difference of physical and biological dose
    dose.reset_dose();
    calculate_nominal_dose(dose,bixel_grid,dij);
    totaldose.scale_dose(-1.0);
    dose += totaldose;

    dose_file = file_prefix + "biol_minus_phys_" + dose_file_suffix;
    dvh_file = file_prefix + "biol_minus_phys_" + DVH_file_suffix;

    dose.write_dose(dose_file);
    dvh.write_DVH(dvh_file);

}


/**
 * calculates physical dose for all instances for selected voxels
 */
void BiologicallyEquivalentDoseModel::calculate_instance_doses(vector<DoseVector> &instance_doses,
							       const BixelVector &bixel_grid,                  
							       DoseInfluenceMatrix &dij,
							       vector<unsigned int> voxelNos) const
{
    // clear dose vector
    for(unsigned int iInstance=0;iInstance<nInstances_;iInstance++)
      {
	instance_doses[iInstance].reset_dose();
      }

    for(unsigned int iVoxel=0;iVoxel<voxelNos.size();iVoxel++) 
      {
	unsigned int voxelNo = voxelNos[iVoxel];

	for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo); iter.get_voxelNo() == voxelNo; iter++) 
	  {
	    instance_doses[bixels_instanceNo_[iter.get_bixelNo()]].add_dose(voxelNo,
									    bixel_grid.get_intensity(iter.get_bixelNo()) 
									    * iter.get_influence());
	    ++iter;
	  }
      }
}

