/**
 * @file: ConstrainedObjective.cpp
 * ConstrainedObjective Class implementation.
 */

#include "ConstrainedObjective.hpp"

/**
 * constructor
 */
ConstrainedObjective::ConstrainedObjective(unsigned int consNo,
										   Objective* obj,
										   float bound,
										   float initial_penalty)
: Constraint(consNo)
, the_obj_(obj)
, bound_(bound)
, initial_penalty_(initial_penalty)
, penalty_(initial_penalty)
, lagrange_(0)
{
}

/**
 * destructor
 */
ConstrainedObjective::~ConstrainedObjective()
{
	delete the_obj_;
}

/**
 * Prints a description of the constraint on the given stream
 *
 * @param o The output stream to write to
 */
void ConstrainedObjective::printOn(std::ostream& o) const
{
	o << "CONS " << consNo_ << ": constrained objective with bound " << bound_ << endl; 
	the_obj_->printOn(o);
}

/*
 * augmented lagrangian is supported if the objective supports gradient
 */
bool ConstrainedObjective::supports_augmented_lagrangian() const
{
	return the_obj_->supports_gradient();
}


/*
 * calculate the augmented lagrangian function 
 */
double ConstrainedObjective::calculate_aug_lagrangian(const BixelVector & beam_weights,
													  DoseInfluenceMatrix & Dij,
													  double & constraint,
													  double & merit)
{
	if(verbose_) {
		cout << "calc augmented lagragian for constraint " << get_consNo() << endl;
	}
	
	double lagrangian = 0;
	double ssvo = 0;	
	constraint = 0;
	merit = 0;
	
	lagrangian = -0.5*lagrange_*lagrange_/penalty_;
	
	double obj = the_obj_->calculate_objective(beam_weights,Dij,false,ssvo);
	double cons = obj - bound_;
	
	if(-1*lagrange_/penalty_ < cons) {
		lagrangian = lagrange_*cons + 0.5*penalty_*cons*cons;
	}
	
	if(cons > 0) {
		merit = 0.5*initial_penalty_*cons;
		constraint = cons;
	}
	
	
	return lagrangian; 
}


/**
 * Calculate contribution of constraint to augmented Lagrangian and its gradient
 */
double ConstrainedObjective::calculate_aug_lagrangian_and_gradient(const BixelVector & beam_weights,
																   DoseInfluenceMatrix & Dij,
																   BixelVectorDirection & gradient,
																   float gradient_multiplier)
{
	if(verbose_) {
		cout << "calc gradient of augmented lagragian for constraint " << get_consNo() << endl;
	}
	
	assert(beam_weights.get_nBixels() == gradient.get_nBixels());
	
	double lagrangian = 0;
	double ssvo = 0;	

	lagrangian = -0.5*lagrange_*lagrange_/penalty_;
	
	double obj = the_obj_->calculate_objective(beam_weights,Dij,false,ssvo);
	double cons = obj - bound_;
	
	if(-1*lagrange_/penalty_ < cons) {
		lagrangian = lagrange_*cons + 0.5*penalty_*cons*cons;
		double factor = lagrange_ + penalty_ * cons;
		the_obj_->calculate_objective_and_gradient(beam_weights,
												   Dij,
												   gradient,
												   gradient_multiplier*factor,
												   false,
												   ssvo);
		
	}
	
	return lagrangian; 
}

/**
 * update the penalty factor
 */
void ConstrainedObjective::update_penalty(const BixelVector & beam_weights,
									DoseInfluenceMatrix & Dij, 
									float tol, float multiplier)
{
	double ssvo = 0;	
	double obj = the_obj_->calculate_objective(beam_weights,Dij,false,ssvo);
	double cons = obj - bound_;

	if(cons > tol*bound_) { // constraint violated by more than the tolerance
		penalty_ *= multiplier;
	}
}


/**
 * update the lagrange multipliers
 */
void ConstrainedObjective::update_lagrange_multipliers(const BixelVector & beam_weights,
													   DoseInfluenceMatrix & Dij)
{
	if(verbose_) {
		cout << "updating lagrange multipliers for constraint " << get_consNo() << endl;
	}  
	
	double ssvo = 0;	
	double obj = the_obj_->calculate_objective(beam_weights,Dij,false,ssvo);
	double cons = obj - bound_;
		
	if(-1*lagrange_/penalty_ < cons) {
		lagrange_ = lagrange_ + penalty_*cons;
	}
}
	
