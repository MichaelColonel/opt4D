/**
 * @file: ExpectedValueMetaObjective.cpp
 * ExpectedValueMetaObjective Class implementation.
 *
 * $Id: ExpectedValueMetaObjective.cpp,v 1.21 2008/03/24 17:05:00 bmartin Exp $
 */

#include "ExpectedValueMetaObjective.hpp"

/**
 * Constructor: eat primary objectives
 */
ExpectedValueMetaObjective::ExpectedValueMetaObjective(
	unsigned int objNo,
    const Geometry *geometry,
    const DoseDeliveryModel &uncertainty,
    const vector<Objective*> &objectives)
  :MetaObjective(objNo, geometry, uncertainty, objectives)
{
    cout << "create expected value meta-objective" << endl;
}

/**
 * Destructor: default.
 */
ExpectedValueMetaObjective::~ExpectedValueMetaObjective()
{
}


/**
 * calculates or estimate objective
 *
 * @param estimated_ssvo If not NULL, appends the estimated ssvo for each 
 * objective to this vector.
 */
double ExpectedValueMetaObjective::calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                int nScenario_samples,
                bool use_voxel_sampling,
                bool use_scenario_sampling)
{
    double objective = 0;

    // Prepare multi_objective and estimated ssvo
    unsigned int mo_offset = multi_objective.size();
    unsigned int ssvo_offset = estimated_ssvo.size();

    for(unsigned int i = 0; i < the_objectives_.size(); i++) {
        multi_objective.push_back(0.0);
        estimated_ssvo.push_back(0.0);
    }

    // Temp vectors for multi objective and estimated ssvo
    vector<double> temp_mo(0);
    vector<double> temp_ssvo(0);

    unsigned int nScenarios;
    RandomScenario random_scenario;
    float prob;


    // get number of scenarios
    if(the_UncertaintyModel_->has_discrete_scenarios()){
      nScenarios = the_UncertaintyModel_->get_nDose_eval_scenarios();
    }
    else {
      nScenarios = nScenario_samples;
      //      nScenarios = get_nScenario_samples();
    }

    // loop over the scenarios
    for(unsigned int iSample=0; iSample<nScenarios; iSample++) {

      // Check if the uncertainty model has a finite number of discrete scenarios
      if(the_UncertaintyModel_->has_discrete_scenarios()){

	// check if scenario sampling is used
	if(use_scenario_sampling && std::rand() > RAND_MAX*get_scenario_sampling_threshold(iSample)) {
	  continue;
	}

	// get probability of the scenario
	if(use_scenario_sampling) {
	  prob = get_scenario_weight(iSample);
	}
	else {
	  prob = the_UncertaintyModel_->get_dose_eval_pdf(iSample);
	}

	// get scenario
	random_scenario = the_UncertaintyModel_->get_dose_eval_scenario(iSample);
      }
      else {
	// generate random scenario
	prob = 1.0 / float(nScenarios);
	the_UncertaintyModel_->generate_random_scenario(random_scenario);
      }

      double temp_obj;

      // estimate contribution to objective
      temp_mo.resize(0); temp_ssvo.resize(0);
      temp_obj = calculate_scenario_objective(
		beam_weights,
                Dij,
                *the_UncertaintyModel_,
                random_scenario,
                temp_mo,
                temp_ssvo,
                use_voxel_sampling);

      // Add contribution to mo estimate
      for(unsigned int i = 0; i < temp_mo.size(); i++) {
	multi_objective[mo_offset + i] += temp_mo[i] * prob;
	estimated_ssvo[ssvo_offset + i] += temp_ssvo[i] * prob;
      }

      // sum up
      objective += (temp_obj*prob);
    }

    return objective;
}


/**
 * estimates the true objective by generating samples until the standard 
 * deviation of the estimate drops below the threshold
 */
MetaObjective::estimate ExpectedValueMetaObjective::estimate_true_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        int max_samples,
        int min_samples,
        float max_uncertainty,
        bool use_voxel_sampling)
{
    cout << "estimating true contribution of expected value meta objective ..."
        << endl;

    double temp_objective,stddev,objective2;
    double objective = 0;
    double variance = 0;
    double sum_objective = 0;
    double sum_objective2 = 0;
    vector<double> sum_mo(nObjectives_,0);
    vector<double> sum_mo2(nObjectives_,0);
    vector<double> sum_ssvo(nObjectives_,0);


    // Temp vectors for multi objective and estimated ssvo
    vector<double> temp_mo(0);
    vector<double> temp_ssvo(0);

    // create class to hold random scenario
    RandomScenario random_scenario;

    // generate samples until max number of steps is reached or accuracy 
    // is good enough
    unsigned int sampleNo = 0;    
    while(sampleNo < max_samples) {

	sampleNo++;

	// generate random scenario
	the_UncertaintyModel_->generate_random_scenario(random_scenario);

	// estimate contribution to objective
        temp_mo.resize(0); temp_ssvo.resize(0);
        temp_objective = calculate_scenario_objective(
                beam_weights,
                Dij,
                *the_UncertaintyModel_,
                random_scenario,
                temp_mo,
                temp_ssvo,
                use_voxel_sampling);

	// add contributions
	sum_objective += temp_objective;
	sum_objective2 += temp_objective*temp_objective;
	objective = sum_objective/(double)sampleNo;
	objective2 = sum_objective2/(double)sampleNo;

        // Add contribution to mo and estimates
        for(unsigned int i = 0; i < nObjectives_; i++) {
            sum_mo[i] += temp_mo[i];
            sum_mo2[i] += temp_mo[i]*temp_mo[i];
            sum_ssvo[i] += temp_ssvo[i];
        }

	if(sampleNo >= min_samples) {

	    // estimate standard deviation
	    variance = objective2 - objective*objective;
	    if(variance<0) {variance=0;}
	    stddev = sqrt(variance/sampleNo);
	    
	    // check if below threshold
	    if(stddev < max_uncertainty * objective) {
		break;
	    }
	}
    }

    cout << "reached accuracy of " << 100.0*stddev/objective
	 << "% after " << sampleNo << " samples." << endl;

    // Prepare return value
    MetaObjective::estimate result;
    result.mean_objective_ = objective;
    result.variance_ = variance;
    result.nSamples_ = sampleNo;

    // Prepare multi-objective and ssvo
    result.multi_objective_.resize(nObjectives_);
    result.multi_variance_.resize(nObjectives_);
    result.estimated_ssvo_.resize(nObjectives_);
    for(unsigned int i = 0; i < nObjectives_; i++) {
        double temp_obj = sum_mo[i]/sampleNo;
        result.multi_objective_[i] = temp_obj;

        // estimate variance
        double temp = (sum_mo2[i]/sampleNo - temp_obj * temp_obj);
        if(temp<0) {
            temp=0;
        }
        result.multi_variance_[i] = temp;

        result.estimated_ssvo_[i] = sum_ssvo[i]/sampleNo;
    }

    return result;
}


/**
 * Calculate or estimate objective and gradient.
 * Uses all possible scenarios if nScenario_samples = -1
 */
double ExpectedValueMetaObjective::calculate_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        vector<double> &multi_objective,
        vector<double> &estimated_ssvo,
        BixelVectorDirection & gradient,
		float gradient_multiplier,
        int nScenario_samples,
        bool use_voxel_sampling,
        bool use_scenario_sampling)
{
    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;

    // Prepare multi_objective and estimated ssvo
    unsigned int mo_offset = multi_objective.size();
    unsigned int ssvo_offset = estimated_ssvo.size();

    for(unsigned int i = 0; i < the_objectives_.size(); i++) {
        multi_objective.push_back(0.0);
        estimated_ssvo.push_back(0.0);
    }

    // Temp vectors for multi objective and estimated ssvo
    vector<double> temp_mo(0);
    vector<double> temp_ssvo(0);

    unsigned int nScenarios;
    RandomScenario random_scenario;
    float prob;

    // get number of scenarios
    if(the_UncertaintyModel_->has_discrete_scenarios()){
      nScenarios = the_UncertaintyModel_->get_nDose_eval_scenarios();
    }
    else {
      nScenarios = nScenario_samples;
      //      nScenarios = get_nScenario_samples();
    }

    // loop over the scenarios
    for(unsigned int iSample=0; iSample<nScenarios; iSample++) {

      // Check if the uncertainty model has a finite number of discrete scenarios
      if(the_UncertaintyModel_->has_discrete_scenarios()){

	// check if scenario sampling is used
	if(use_scenario_sampling && std::rand() > RAND_MAX*get_scenario_sampling_threshold(iSample)) {
	  continue;
	}

	// get probability of the scenario
	if(use_scenario_sampling) {
	  prob = get_scenario_weight(iSample);
	}
	else {
	  prob = the_UncertaintyModel_->get_dose_eval_pdf(iSample);
	}

	// get scenario
	random_scenario = the_UncertaintyModel_->get_dose_eval_scenario(iSample);
      }
      else {
	// generate random scenario
	prob = 1.0 / float(nScenarios);
	the_UncertaintyModel_->generate_random_scenario(random_scenario);
      }
    
      // estimate contribution to objective
      double temp_obj = 0;
      temp_mo.resize(0); temp_ssvo.resize(0);
      temp_obj = calculate_scenario_objective_and_gradient(
		beam_weights,
		Dij,
                temp_mo,
                temp_ssvo,
                gradient, 
                gradient_multiplier*prob,
                *the_UncertaintyModel_,
                random_scenario,
                use_voxel_sampling);

      // Add contribution to mo and ssvo estimate
      for(unsigned int i = 0; i < temp_mo.size(); i++) {
	multi_objective[mo_offset + i] += temp_mo[i] * prob;
	estimated_ssvo[ssvo_offset + i] += temp_ssvo[i] * prob;
      }

      // add contributions
      objective += temp_obj * prob;
    }
    return objective;
}


/**
 * Calculate or estimate objective, gradient, and Hessian times a vector
 * Uses all possible scenarios if nScenario_samples = -1
 */
double ExpectedValueMetaObjective::calculate_objective_and_gradient_and_Hv(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        vector<double> &multi_objective,
        vector<double> &estimated_ssvo,
        BixelVectorDirection & gradient,
        const vector<float> & v,
        vector<float> & Hv,
        int nScenario_samples,
        bool use_voxel_sampling,
        bool use_scenario_sampling)
{
    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;

    // Prepare multi_objective and estimated ssvo
    unsigned int mo_offset = multi_objective.size();
    unsigned int ssvo_offset = estimated_ssvo.size();

    for(unsigned int i = 0; i < the_objectives_.size(); i++) {
        multi_objective.push_back(0.0);
        estimated_ssvo.push_back(0.0);
    }

    // Temp vectors for multi objective and estimated ssvo
    vector<double> temp_mo(0);
    vector<double> temp_ssvo(0);

    unsigned int nScenarios;
    RandomScenario random_scenario;
    float prob;

    // get number of scenarios
    if(the_UncertaintyModel_->has_discrete_scenarios()){
      nScenarios = the_UncertaintyModel_->get_nDose_eval_scenarios();
    }
    else {
      nScenarios = nScenario_samples;
      //      nScenarios = get_nScenario_samples();
    }

    // loop over the scenarios
    for(unsigned int iSample=0; iSample<nScenarios; iSample++) {

      // Check if the uncertainty model has a finite number of discrete scenarios
      if(the_UncertaintyModel_->has_discrete_scenarios()){

	// check if scenario sampling is used
	if(use_scenario_sampling && std::rand() > RAND_MAX*get_scenario_sampling_threshold(iSample)) {
	  continue;
	}

	// get probability of the scenario
	if(use_scenario_sampling) {
	  prob = get_scenario_weight(iSample);
	}
	else {
	  prob = the_UncertaintyModel_->get_dose_eval_pdf(iSample);
	}

	// get scenario
	random_scenario = the_UncertaintyModel_->get_dose_eval_scenario(iSample);
      }
      else {
	// generate random scenario
	prob = 1.0 / float(nScenarios);
	the_UncertaintyModel_->generate_random_scenario(random_scenario);
      }

      // estimate contribution to objective
      double temp_obj = 0;
      temp_mo.resize(0); temp_ssvo.resize(0);
      temp_obj = calculate_scenario_objective_and_gradient_and_Hv(
                beam_weights,
                Dij,
                temp_mo,
                temp_ssvo,
                gradient, 
                prob,
                v,
                Hv,
                *the_UncertaintyModel_,
                random_scenario,
                use_voxel_sampling);

      // Add contribution to mo and ssvo estimate
      for(unsigned int i = 0; i < temp_mo.size(); i++) {
	multi_objective[mo_offset + i] += temp_mo[i] * prob;
	estimated_ssvo[ssvo_offset + i] += temp_ssvo[i] * prob;
      }

      // add contributions
      objective += temp_obj * prob;
    }
    return objective;
}


/**
 * returns probability for a scenario to be included during scenario sampling
 */
float ExpectedValueMetaObjective::get_scenario_sampling_threshold(unsigned int iSample) const
{
  // not yet implemented, use sampling rate of 1
  return(1.0);
}


/**
 * returns weight of a scenario for scenario sampling
 */
float ExpectedValueMetaObjective::get_scenario_weight(unsigned int iSample) const
{
  // not yet implemented, return probability of scenario
  return(the_UncertaintyModel_->get_dose_eval_pdf(iSample));
}
