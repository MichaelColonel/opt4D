/**
 * @file Geometry.cpp
 * Code for Geometry class
 *
 * $Id: Geometry.cpp,v 1.24 2008/05/30 14:26:05 unkelbac Exp $
 */

#include "Geometry.hpp"
#include <fstream>
#include <cstring>
#include <algorithm>

/**
 * Default constructor.
 */
Geometry::Geometry(unsigned int nBeams, unsigned int nBixels)
  : is_initialized_(false)
  , bixel_grid_is_valid_(false)
  , ctatts_()
  , nX_(0), nY_(0), nZ_(0)
  , voxel_dx_(0), voxel_dy_(0), voxel_dz_(0)
  , iso_x_(0), iso_y_(0), iso_z_(0)
  , is_air_()
  , nBeams_(nBeams)
  , beam_starting_bixelNo_(nBeams, 0)
  , beam_gantry_angle_(nBeams,0)
  , beam_table_angle_(nBeams,0)
  , beam_collimator_angle_(nBeams,0)
  , beam_sad_(nBeams,0)
  , isocenter_(nBeams)
  , nBixels_(nBixels)
  , bixel_beamNo_(nBixels, 0)
  , bixel_index_(nBixels)
  , bixel_subscripts_(nBixels)
  , bixel_is_physical_(nBixels, true)
  , bixel_position_(nBixels)
  , beam_bixel_nX_(nBeams, 0)
  , beam_bixel_nY_(nBeams, 0)
  , beam_bixel_nE_(nBeams, 0)
  , beam_bixel_dx_(nBeams,0), beam_bixel_dy_(nBeams,0)
  , beam_bixel_minX_(nBeams, 0), beam_bixel_maxX_(nBeams, 0)
  , beam_bixel_minY_(nBeams, 0), beam_bixel_maxY_(nBeams, 0)
  , beam_bixel_dwel_(nBeams, 0)
  , beam_grid_bixelNo_(nBeams)
  , beam_grid_is_bixel_(nBeams)
  , beam_energy_table_(nBeams)
{
}

/**
 * copy constructor which only copies the voxel grid information
 */
Geometry::Geometry(const Geometry & rhs, unsigned int nBeams, unsigned int nBixels)
  : is_initialized_(false)
  , bixel_grid_is_valid_(false)
  , ctatts_()
  , nX_(rhs.nX_), nY_(rhs.nY_), nZ_(rhs.nZ_)
  , voxel_dx_(rhs.voxel_dx_), voxel_dy_(rhs.voxel_dy_), voxel_dz_(rhs.voxel_dz_)
  , iso_x_(rhs.iso_x_), iso_y_(rhs.iso_y_), iso_z_(rhs.iso_z_)
  , is_air_(rhs.is_air_)
  , nBeams_(nBeams)
  , beam_starting_bixelNo_(nBeams, 0)
  , beam_gantry_angle_(nBeams,0)
  , beam_table_angle_(nBeams,0)
  , beam_collimator_angle_(nBeams,0)
  , beam_sad_(nBeams,0)
  , isocenter_(nBeams)
  , nBixels_(nBixels)
  , bixel_beamNo_(nBixels, 0)
  , bixel_index_(nBixels)
  , bixel_subscripts_(nBixels)
  , bixel_is_physical_(nBixels, true)
  , bixel_position_(nBixels)
  , beam_bixel_nX_(nBeams, 0)
  , beam_bixel_nY_(nBeams, 0)
  , beam_bixel_nE_(nBeams, 0)
  , beam_bixel_dx_(nBeams,0), beam_bixel_dy_(nBeams,0)
  , beam_bixel_minX_(nBeams, 0), beam_bixel_maxX_(nBeams, 0)
  , beam_bixel_minY_(nBeams, 0), beam_bixel_maxY_(nBeams, 0)
  , beam_bixel_dwel_(nBeams, 0)
  , beam_grid_bixelNo_(nBeams)
  , beam_grid_is_bixel_(nBeams)
  , beam_energy_table_(nBeams)
{
}

/**
 * constructor reading dif file
 */
Geometry::Geometry(string dif_file_name, unsigned int nBeams, unsigned int nBixels)
  : is_initialized_(false)
  , bixel_grid_is_valid_(false)
  , ctatts_()
  , nX_(0), nY_(0), nZ_(0)
  , voxel_dx_(0), voxel_dy_(0), voxel_dz_(0)
  , iso_x_(0), iso_y_(0), iso_z_(0)
  , is_air_()
  , nBeams_(nBeams)
  , beam_starting_bixelNo_(nBeams, 0)
  , beam_gantry_angle_(nBeams,0)
  , beam_table_angle_(nBeams,0)
  , beam_collimator_angle_(nBeams,0)
  , beam_sad_(nBeams,0)
  , isocenter_(nBeams)
  , nBixels_(nBixels)
  , bixel_beamNo_(nBixels, 0)
  , bixel_index_(nBixels)
  , bixel_subscripts_(nBixels)
  , bixel_is_physical_(nBixels, true)
  , bixel_position_(nBixels)
  , beam_bixel_nX_(nBeams, 0)
  , beam_bixel_nY_(nBeams, 0)
  , beam_bixel_nE_(nBeams, 0)
  , beam_bixel_dx_(nBeams,0), beam_bixel_dy_(nBeams,0)
  , beam_bixel_minX_(nBeams, 0), beam_bixel_maxX_(nBeams, 0)
  , beam_bixel_minY_(nBeams, 0), beam_bixel_maxY_(nBeams, 0)
  , beam_bixel_dwel_(nBeams, 0)
  , beam_grid_bixelNo_(nBeams)
  , beam_grid_is_bixel_(nBeams)
  , beam_energy_table_(nBeams)
{

    unsigned int temp_int;
    unsigned int itempX, itempY, itempZ;
    float temp_float;
    float ftempX, ftempY, ftempZ;
    char aLine[100];
    char temp_par[100];
    FILE *fInput;

    cout << "Constructor for geometry class called ..." << endl;

    // Open .dif file
    fInput = fopen(dif_file_name.c_str(),"r");
    if (fInput == NULL)
    {
	cout << "Can't open file: " << dif_file_name << endl;
	exit(-1);
    }

    cout << "reading dimension information file: " << dif_file_name << endl;

    // resolution
    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %f",temp_par,&temp_float);
    if (strstr(temp_par, "Delta-X") == NULL) {
	cout << "error reading .dif file " << dif_file_name << endl;
	cout << temp_par << " " << temp_float << " " << endl;
	exit(-1);
    }
    else {
    	ftempX =temp_float;
    }

    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %f",temp_par,&temp_float);
    if (strstr(temp_par, "Delta-Y") == NULL) {
	cout << "error reading .dif file " << dif_file_name << endl;
	cout << temp_par << " " << temp_float << " " << endl;
	exit(-1);
    }
    else {
    	ftempY =temp_float;
    }

    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %f",temp_par,&temp_float);
    if (strstr(temp_par, "Delta-Z") == NULL) {
	cout << "error reading .dif file " << dif_file_name << endl;
	cout << temp_par << " " << temp_float << " " << endl;
	exit(-1);
    }
    else {
    	ftempZ =temp_float;
    }

    // set voxel resolution
    set_voxel_size(ftempX,ftempY,ftempZ);

    // skip CT cube dimension
    fgets(aLine,sizeof(aLine),fInput);
    fgets(aLine,sizeof(aLine),fInput);
    fgets(aLine,sizeof(aLine),fInput);

    // dose cube dimension
    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %d",temp_par,&temp_int);
    if (strstr(temp_par, "Dimension-Dose-X") == NULL) {
	cout << "error reading .dif file " << dif_file_name << endl;
	cout << temp_par << " " << temp_int << " " << endl;
	exit(-1);
    }
    else {
    	itempX =temp_int;
    }

    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %d",temp_par,&temp_int);
    if (strstr(temp_par, "Dimension-Dose-Y") == NULL) {
	cout << "error reading .dif file " << dif_file_name << endl;
	cout << temp_par << " " << temp_int << " " << endl;
	exit(-1);
    }
    else {
    	itempY =temp_int;
    }

    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %d",temp_par,&temp_int);
    if (strstr(temp_par, "Dimension-Dose-Z") == NULL) {
	cout << "error reading .dif file " << dif_file_name << endl;
	cout << temp_par << " " << temp_int << " " << endl;
	exit(-1);
    }
    else {
    	itempZ =temp_int;
    }

    // set dose cube size
    set_nVoxels(itempX,itempY,itempZ);

    // skip CT cube dimension
    fgets(aLine,sizeof(aLine),fInput);
    fgets(aLine,sizeof(aLine),fInput);
    fgets(aLine,sizeof(aLine),fInput);

    // dose cube isocenter voxel
    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %f",temp_par,&temp_float);
    if (strstr(temp_par, "ISO-Index-Dose-X") != NULL) {
	ftempX = temp_float; }
    else {
	cout << "error reading .dif file " << dif_file_name << endl;
	exit(-1); }

    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %f",temp_par,&temp_float);
    if (strstr(temp_par, "ISO-Index-Dose-Y") != NULL) {
	ftempY = temp_float; }
    else {
	cout << "error reading .dif file " << dif_file_name << endl;
	exit(-1); }

    fgets(aLine,sizeof(aLine),fInput);
    sscanf(aLine,"%s %f",temp_par,&temp_float);
    if (strstr(temp_par, "ISO-Index-Dose-Z") != NULL) {
	ftempZ = temp_float; }
    else {
	cout << "error reading .dif file " << dif_file_name << endl;
	exit(-1); }

    // set isocenter in voxel coordinates
    set_isocenter_in_voxel_coordinates(ftempX,ftempY,ftempZ);

    // close file
    fclose(fInput);
}

/**
 * Resize the geometry to hold the given number of beams and zero out any
 * information already stored in the beams.
 */
void Geometry::set_nBeams(unsigned int nBeams)
{
  nBeams_ = nBeams;

  beam_starting_bixelNo_.resize(nBeams);
  fill(beam_starting_bixelNo_.begin(),beam_starting_bixelNo_.end(), 0);

  beam_gantry_angle_.resize(nBeams);
  fill(beam_gantry_angle_.begin(),beam_gantry_angle_.end(), 0);

  beam_table_angle_.resize(nBeams);
  fill(beam_table_angle_.begin(),beam_table_angle_.end(), 0);

  beam_collimator_angle_.resize(nBeams);
  fill(beam_collimator_angle_.begin(),beam_collimator_angle_.end(), 0);

  beam_bixel_dx_.resize(nBeams_);
  fill(beam_bixel_dx_.begin(),beam_bixel_dx_.end(), 0);

  beam_bixel_dy_.resize(nBeams_);
  fill(beam_bixel_dy_.begin(),beam_bixel_dy_.end(), 0);

  beam_sad_.resize(nBeams_);
  fill(beam_sad_.begin(),beam_sad_.end(), 0);

  beam_bixel_nX_.resize(nBeams_);
  fill(beam_bixel_nX_.begin(),beam_bixel_nX_.end(), 0);

  beam_bixel_nY_.resize(nBeams_);
  fill(beam_bixel_nY_.begin(),beam_bixel_nY_.end(), 0);

  beam_bixel_minX_.resize(nBeams_);
  fill(beam_bixel_minX_.begin(),beam_bixel_minX_.end(), 0);

  beam_bixel_maxX_.resize(nBeams_);
  fill(beam_bixel_maxX_.begin(),beam_bixel_maxX_.end(), 0);

  beam_bixel_minY_.resize(nBeams_);
  fill(beam_bixel_minY_.begin(),beam_bixel_minY_.end(), 0);

  beam_bixel_maxY_.resize(nBeams_);
  fill(beam_bixel_maxY_.begin(),beam_bixel_maxY_.end(), 0);

  beam_bixel_nE_.resize(nBeams_);
  fill(beam_bixel_nE_.begin(),beam_bixel_nE_.end(), 0);

  beam_bixel_dwel_.resize(nBeams_);
  fill(beam_bixel_dwel_.begin(),beam_bixel_dwel_.end(), 0);

  beam_energy_table_.resize(nBeams_);
  fill(beam_energy_table_.begin(), beam_energy_table_.end(), vector<float>());

  beam_bixel_nPoints_.resize(nBeams_);
  beam_grid_bixelNo_.resize(nBeams_);
  beam_grid_is_bixel_.resize(nBeams_);

}

void Geometry::set_nBixels(unsigned int nBixels)
{
  nBixels_ = nBixels;

  bixel_beamNo_.resize(nBixels_);
  fill(bixel_beamNo_.begin(), bixel_beamNo_.end(), 0);

  bixel_index_.resize(nBixels_);
  fill(bixel_index_.begin(), bixel_index_.end(), BixelIndex());

  bixel_subscripts_.resize(nBixels_);
  fill(bixel_subscripts_.begin(), bixel_subscripts_.end(), Bixel_Subscripts());

  bixel_position_.resize(nBixels_);
  fill(bixel_position_.begin(), bixel_position_.end(), Bixel_Position());

  bixel_is_physical_.resize(nBixels_);
  fill(bixel_is_physical_.begin(), bixel_is_physical_.end(), true);
}

/**
 * set location of the isocenters of all beams
 *
 */
void Geometry::set_isocenters()
{
    // calculate isocenter in patient coordinate system
    Point_in_Patient_Coord isocenter = Point_in_Patient_Coord(iso_x_*voxel_dx_,
							      iso_y_*voxel_dy_,
							      iso_z_*voxel_dz_);
    // resize isocenter vector
    isocenter_.resize(nBeams_);

    // set all isocenters
    for (unsigned int i=0;i<nBeams_;i++)
    {
	isocenter_.at(i) = isocenter;
    }
}

/**
 * reads all vois to figure out which voxel in the dose cube is air
 *
 */
void Geometry::load_air_voxels(vector<Voi*> vois)
{
    assert(get_nVoxels() > 0);

    // init vector
    is_air_.resize(get_nVoxels(),true);

    // loop over vois
    for (vector<Voi*>::iterator iter = vois.begin(); iter!=vois.end(); ++iter)
    {
	for (unsigned int iVoxel=0;iVoxel<(*iter)->get_nVoxels();iVoxel++)
	{
	    is_air_.at( (*iter)->get_voxel(iVoxel) ) = false;
	}
    }

}


/**
 * Transform Room coordinate into gantry coordinate
 *
 */
Point_in_Gantry_Coord Geometry::transform_to_Gantry_Coord(
	const Point_in_Room_Coord rhs, const int BeamNo) const
{
  Point_in_Gantry_Coord temp;

  // gantry rotation (around y axis) into gantry system
  temp.x_ = rhs.x_ * cos(get_gantry_angle()[BeamNo]*PI/180.)
      - rhs.z_ * sin(get_gantry_angle()[BeamNo]*PI/180.);

  temp.y_ = rhs.y_;

  temp.depth_ = + rhs.x_ * sin(get_gantry_angle()[BeamNo]*PI/180.)
      + rhs.z_ * cos(get_gantry_angle()[BeamNo]*PI/180.);

  return temp;
}

Point_in_Room_Coord Geometry::transform_to_Room_Coord(
	const Point_in_Gantry_Coord rhs, const int BeamNo) const
{
  Point_in_Room_Coord temp;

  // gantry rotation (around y axis) into room system
  temp.x_ = rhs.x_ * cos(get_gantry_angle()[BeamNo]*PI/180.)
      + rhs.depth_ * sin(get_gantry_angle()[BeamNo]*PI/180.);

  temp.y_ = rhs.y_;

  temp.z_ = - rhs.x_ * sin(get_gantry_angle()[BeamNo]*PI/180.)
      + rhs.depth_ * cos(get_gantry_angle()[BeamNo]*PI/180.);

  return temp;
}

/**
 * Transform table coordinate into room coordinate
 *
 */
Point_in_Room_Coord Geometry::transform_to_Room_Coord(
	const Point_in_Table_Coord rhs, const int BeamNo) const
{
  Point_in_Room_Coord temp;

  // table rotation (around z axis) into room system
  temp.x_ = rhs.x_ * cos(get_table_angle()[BeamNo]*PI/180.)
      - rhs.y_ * sin(get_table_angle()[BeamNo]*PI/180.);

  temp.y_ = rhs.x_ * sin(get_table_angle()[BeamNo]*PI/180.)
      + rhs.y_ * cos(get_table_angle()[BeamNo]*PI/180.);

  temp.z_ = rhs.z_;

  return temp;
}

Point_in_Table_Coord Geometry::transform_to_Table_Coord(
	const Point_in_Room_Coord rhs, const int BeamNo) const
{
  Point_in_Table_Coord temp;

  // table rotation (around z axis) into table system
  temp.x_ = + rhs.x_ * cos(get_table_angle()[BeamNo]*PI/180.)
      + rhs.y_ * sin(get_table_angle()[BeamNo]*PI/180.);

  temp.y_ = - rhs.x_ * sin(get_table_angle()[BeamNo]*PI/180.)
      + rhs.y_ * cos(get_table_angle()[BeamNo]*PI/180.);

  temp.z_ = rhs.z_;

  return temp;
}

/**
 * transform gantry coordinates into collimator coordinates(not implemented)
 *
 * @todo implement collimator rotation
 */
Point_in_Collimator_Coord Geometry::transform_to_Collimator_Coord(
    const Point_in_Gantry_Coord rhs, const int BeamNo) const
{
    // implement collimator rotation here
    if(get_collimator_angle()[BeamNo] != 0) {
	cout << "Collimator rotation has not been implemented and Collimator angle is "
	     << get_collimator_angle()[BeamNo] << " for beam " << BeamNo << endl
	     << "Cannot continue" << endl;
    }

    Point_in_Collimator_Coord temp;
    temp.x_ = rhs.x_;
    temp.y_ = rhs.y_;
    temp.depth_ = rhs.depth_;

    return temp;
}

/**
 * transform collimator coordinates into gantry coordinates(not implemented)
 *
 * @todo implement collimator rotation
 */
Point_in_Gantry_Coord Geometry::transform_to_Gantry_Coord(
    const Point_in_Collimator_Coord rhs, const int BeamNo) const
{
    // implement collimator rotation here
    if(get_collimator_angle()[BeamNo] != 0) {
	cout << "Collimator rotation has not been implemented and Collimator angle is "
	     << get_collimator_angle()[BeamNo] << " for beam " << BeamNo << endl
	     << "Cannot continue" << endl;
    }

    Point_in_Gantry_Coord temp;
    temp.x_ = rhs.x_;
    temp.y_ = rhs.y_;
    temp.depth_ = rhs.depth_;

    return temp;
}


/**
 * Transform patient coordinate into table coordinate
 * note: this function currently only implements the location of the isocenter
 * with respect to the origin of the patient coordinate system.
 * The non-isocentric table rotation is not implemented.
 * This would be the place to do it though.
 */
Point_in_Table_Coord Geometry::transform_to_Table_Coord(
	const Point_in_Patient_Coord rhs, const int BeamNo) const
{
  Point_in_Table_Coord temp;
  Point_in_Patient_Coord isocenter = get_isocenter(BeamNo);

  // isocenter shift
  temp.x_ = rhs.x_ - isocenter.x_;
  temp.y_ = rhs.y_ - isocenter.y_;
  temp.z_ = rhs.z_ - isocenter.z_;

  return temp;
}

Point_in_Patient_Coord Geometry::transform_to_Patient_Coord(
	const Point_in_Table_Coord rhs, const int BeamNo) const
{
  Point_in_Patient_Coord temp(rhs.x_, rhs.y_, rhs.z_);

  // isocenter shift
  temp += get_isocenter(BeamNo);

  return temp;
}

/**
 * Transform patient coordinate to gantry coordinate
 *
 */
Point_in_Gantry_Coord Geometry::transform_to_Gantry_Coord(
	const Point_in_Patient_Coord rhs, const int BeamNo) const
{
    return ( transform_to_Gantry_Coord(
		 transform_to_Room_Coord(
		     transform_to_Table_Coord(rhs,BeamNo), BeamNo ), BeamNo) );
}

Point_in_Patient_Coord Geometry::transform_to_Patient_Coord(
	const Point_in_Gantry_Coord rhs, const int BeamNo) const
{
    return ( transform_to_Patient_Coord(
		 transform_to_Table_Coord(
		     transform_to_Room_Coord(rhs,BeamNo), BeamNo ), BeamNo) );
}

/**
 * Transform shift in table coordinates to shift in patient
 * (note: the non-isocentric table rotation is not implemented)
 */
Shift_in_Patient_Coord Geometry::transform_to_Shift_in_Patient_Coord(
	const Point_in_Table_Coord rhs, const int BeamNo) const
{
  Shift_in_Patient_Coord temp(rhs.x_, rhs.y_, rhs.z_);

  return temp;
}

/**
 * Transform shift in gantry coordinates to shift in patient
 */
Shift_in_Patient_Coord Geometry::transform_to_Shift_in_Patient_Coord(
    const Point_in_Gantry_Coord rhs, const int BeamNo) const
{
    return ( transform_to_Shift_in_Patient_Coord(
		 transform_to_Table_Coord(
		     transform_to_Room_Coord(rhs,BeamNo), BeamNo ), BeamNo) );
}

/**
 * Transform shift in patient coordinates to shift in collimator coordinates
 * (note: the non-isocentric table rotation is not implemented)
 */
Shift_in_Collimator_Coord Geometry::transform_to_Shift_in_Collimator_Coord(
    const Shift_in_Patient_Coord rhs, const int BeamNo) const
{
    Point_in_Table_Coord temp_table(rhs.x_, rhs.y_, rhs.z_);

    Point_in_Collimator_Coord temp_colli =
	transform_to_Collimator_Coord(
	    transform_to_Gantry_Coord(
		transform_to_Room_Coord(temp_table,BeamNo),BeamNo),BeamNo);

    return Shift_in_Collimator_Coord(temp_colli.x_,temp_colli.y_,temp_colli.depth_);
}


/**
 * Calculates dose to a point in the patient by tri-linear interpolation of the voxel grid
 * The function assumes that dose_vector contains a physically meaningful dose distribution. It does not check if the dose value in a voxel is zero, e.g. due to voxel sampling. It only checks if the neighboring voxels are within the dose cube and not air. If that's the case, these voxels are excluded.
 *
 * @todo make this function more clearly arranged
 */
float Geometry::get_dose_by_trilinear_interpolation(
	const DoseVector &dose_vector, const Point_in_Patient_Coord point) const
{
    float dose = 0;
    vector<Voxel_Subscripts> temp_sub(8,Voxel_Subscripts());
    vector<VoxelIndex> temp_index(8,VoxelIndex(0));
    vector<float> temp_weight(8,1.0);

    // get subscripts and position of nearest voxel
    Voxel_Subscripts nearest_voxel_sub = convert_to_Voxel_Subscripts(point);
    Point_in_Patient_Coord nearest_voxel_coord = convert_to_Point_in_Patient_Coord(nearest_voxel_sub);

    // check if nearest voxel is in the dose grid
    if (!is_inside(nearest_voxel_sub))
    {
//	cout << "Warning: called get_dose_by_trilinear_interpolation for a point which is not within the dose cube" << endl;
	return(0);
    }

    // get index
    VoxelIndex nearest_voxel_index = convert_to_VoxelIndex(nearest_voxel_sub);

    // check if voxel is in air
    if ( is_air(nearest_voxel_index.index_) )
    {
//	cout << "Warning! Nearest voxel for tri-linear dose interpolation is in air." << endl;
	return(0);
    }
    else // use trilinear interpolation
    {
	// get subscripts and distance off all 8 neighboring voxels
	if (point.x_ > nearest_voxel_coord.x_)
	{
	    temp_sub[0].iX_ = nearest_voxel_sub.iX_;
	    temp_sub[2].iX_ = nearest_voxel_sub.iX_;
	    temp_sub[4].iX_ = nearest_voxel_sub.iX_;
	    temp_sub[6].iX_ = nearest_voxel_sub.iX_;
	    temp_weight[0] *= 1.0 - (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	    temp_weight[2] *= 1.0 - (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	    temp_weight[4] *= 1.0 - (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	    temp_weight[6] *= 1.0 - (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();

	    temp_sub[1].iX_ = nearest_voxel_sub.iX_ + 1;
	    temp_sub[3].iX_ = nearest_voxel_sub.iX_ + 1;
	    temp_sub[5].iX_ = nearest_voxel_sub.iX_ + 1;
	    temp_sub[7].iX_ = nearest_voxel_sub.iX_ + 1;
	    temp_weight[1] *= (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	    temp_weight[3] *= (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	    temp_weight[5] *= (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	    temp_weight[7] *= (point.x_ - nearest_voxel_coord.x_)/get_voxel_dx();
	}
	else
	{
	    temp_sub[0].iX_ = nearest_voxel_sub.iX_ - 1;
	    temp_sub[2].iX_ = nearest_voxel_sub.iX_ - 1;
	    temp_sub[4].iX_ = nearest_voxel_sub.iX_ - 1;
	    temp_sub[6].iX_ = nearest_voxel_sub.iX_ - 1;
	    temp_weight[0] *= (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	    temp_weight[2] *= (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	    temp_weight[4] *= (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	    temp_weight[6] *= (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();

	    temp_sub[1].iX_ = nearest_voxel_sub.iX_;
	    temp_sub[3].iX_ = nearest_voxel_sub.iX_;
	    temp_sub[5].iX_ = nearest_voxel_sub.iX_;
	    temp_sub[7].iX_ = nearest_voxel_sub.iX_;
	    temp_weight[1] *= 1.0 - (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	    temp_weight[3] *= 1.0 - (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	    temp_weight[5] *= 1.0 - (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	    temp_weight[7] *= 1.0 - (nearest_voxel_coord.x_ - point.x_)/get_voxel_dx();
	}

	if (point.y_ > nearest_voxel_coord.y_)
	{
	    temp_sub[0].iY_ = nearest_voxel_sub.iY_;
	    temp_sub[1].iY_ = nearest_voxel_sub.iY_;
	    temp_sub[4].iY_ = nearest_voxel_sub.iY_;
	    temp_sub[5].iY_ = nearest_voxel_sub.iY_;
	    temp_weight[0] *= 1.0 - (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	    temp_weight[1] *= 1.0 - (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	    temp_weight[4] *= 1.0 - (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	    temp_weight[5] *= 1.0 - (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();

	    temp_sub[2].iY_ = nearest_voxel_sub.iY_ + 1;
	    temp_sub[3].iY_ = nearest_voxel_sub.iY_ + 1;
	    temp_sub[6].iY_ = nearest_voxel_sub.iY_ + 1;
	    temp_sub[7].iY_ = nearest_voxel_sub.iY_ + 1;
	    temp_weight[2] *= (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	    temp_weight[3] *= (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	    temp_weight[6] *= (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	    temp_weight[7] *= (point.y_ - nearest_voxel_coord.y_)/get_voxel_dz();
	}
	else
	{
	    temp_sub[0].iY_ = nearest_voxel_sub.iY_ - 1;
	    temp_sub[1].iY_ = nearest_voxel_sub.iY_ - 1;
	    temp_sub[4].iY_ = nearest_voxel_sub.iY_ - 1;
	    temp_sub[5].iY_ = nearest_voxel_sub.iY_ - 1;
	    temp_weight[0] *= (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	    temp_weight[1] *= (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	    temp_weight[4] *= (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	    temp_weight[5] *= (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();

	    temp_sub[2].iY_ = nearest_voxel_sub.iY_;
	    temp_sub[3].iY_ = nearest_voxel_sub.iY_;
	    temp_sub[6].iY_ = nearest_voxel_sub.iY_;
	    temp_sub[7].iY_ = nearest_voxel_sub.iY_;
	    temp_weight[2] *= 1.0 - (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	    temp_weight[3] *= 1.0 - (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	    temp_weight[6] *= 1.0 - (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	    temp_weight[7] *= 1.0 - (nearest_voxel_coord.y_ - point.y_)/get_voxel_dy();
	}

	if (point.z_ > nearest_voxel_coord.z_)
	{
	    temp_sub[0].iZ_ = nearest_voxel_sub.iZ_;
	    temp_sub[1].iZ_ = nearest_voxel_sub.iZ_;
	    temp_sub[2].iZ_ = nearest_voxel_sub.iZ_;
	    temp_sub[3].iZ_ = nearest_voxel_sub.iZ_;
	    temp_weight[0] *= 1.0 - (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	    temp_weight[1] *= 1.0 - (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	    temp_weight[2] *= 1.0 - (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	    temp_weight[3] *= 1.0 - (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();

	    temp_sub[4].iZ_ = nearest_voxel_sub.iZ_ + 1;
	    temp_sub[5].iZ_ = nearest_voxel_sub.iZ_ + 1;
	    temp_sub[6].iZ_ = nearest_voxel_sub.iZ_ + 1;
	    temp_sub[7].iZ_ = nearest_voxel_sub.iZ_ + 1;
	    temp_weight[4] *= (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	    temp_weight[5] *= (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	    temp_weight[6] *= (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	    temp_weight[7] *= (point.z_ - nearest_voxel_coord.z_)/get_voxel_dz();
	}
	else
	{
	    temp_sub[0].iZ_ = nearest_voxel_sub.iZ_ - 1;
	    temp_sub[1].iZ_ = nearest_voxel_sub.iZ_ - 1;
	    temp_sub[2].iZ_ = nearest_voxel_sub.iZ_ - 1;
	    temp_sub[3].iZ_ = nearest_voxel_sub.iZ_ - 1;
	    temp_weight[0] *= (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	    temp_weight[1] *= (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	    temp_weight[2] *= (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	    temp_weight[3] *= (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();

	    temp_sub[4].iZ_ = nearest_voxel_sub.iZ_;
	    temp_sub[5].iZ_ = nearest_voxel_sub.iZ_;
	    temp_sub[6].iZ_ = nearest_voxel_sub.iZ_;
	    temp_sub[7].iZ_ = nearest_voxel_sub.iZ_;
	    temp_weight[4] *= 1.0 - (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	    temp_weight[5] *= 1.0 - (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	    temp_weight[6] *= 1.0 - (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	    temp_weight[7] *= 1.0 - (nearest_voxel_coord.z_ - point.z_)/get_voxel_dz();
	}

    }

    // calculate index
    for (unsigned int i=0;i<temp_sub.size();i++)
    {
	if ( is_inside(temp_sub[i]) ) {
	    temp_index[i] = convert_to_VoxelIndex(temp_sub[i]);
	}
	else {
	    temp_index[i] = 0;
	}
    }

    // check whether all voxels are within the dose cube and not air
    unsigned int count = 8;
    float weight;

    for (unsigned int i=0;i<temp_sub.size();i++)
    {
	if ( !is_inside(temp_sub[i]) || is_air(temp_index[i].index_) )
	{
	    count = count - 1;

	    // increase weight of other voxels
	    weight = temp_weight[i];
	    for (unsigned int j=0;j<temp_sub.size();j++)
	    {
		temp_weight[j] *= (1.0 / (1.0-weight));
		// note that (1.0-weight)==0 should not occur since the nearest voxel is NOT air
	    }

	    // delete the contribution of this voxel
	    temp_weight[i] = 0;
	}
    }

    // average the dose
    for (unsigned int i=0;i<temp_sub.size();i++)
    {
	if (temp_weight[i] != 0)
	{
	    dose += dose_vector.get_voxel_dose(temp_index[i].index_) * temp_weight[i];
	}
//	cout << "dose: " << dose_vector.get_voxel_dose(temp_index[i].index_);
//	cout << " weight: " << temp_weight[i] << " index: " << temp_index[i].index_ << endl;
    }

//    cout << "dose: " << dose << endl << endl;

    return dose;
}


/**
 * initialize the beamlet grid
 */
void Geometry::initialize_bixel_grid()
{
    // init energy-index lookup table
    initialize_energy_table();

    // set bixel grid size
    initialize_bixel_grid_size();

    // set index and subscripts for all bixels
    set_bixel_subscripts_and_index();

    // set bixel number for each grid point
    if(!set_bixelNo_in_bixel_grid()) {
        bixel_grid_is_valid_ = false;
    }
    else {
        bixel_grid_is_valid_ = true;
    }
}

/**
 * initialize the energy-subscript lookup table
 */
void Geometry::initialize_energy_table()
{
    float epsilon = 0.001;

    // loop over the beamlets and get all energies
    for(unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++) {

	// get beamNo
	unsigned int beamNo = get_beamNo(iBixel);

	// loop over the existing energies
	bool found_it = false;
	for(vector<float>::iterator iter = beam_energy_table_[beamNo].begin();
	    iter != beam_energy_table_[beamNo].end(); iter++) {
	    //cout << iBixel << " " << get_bixel_energy(iBixel) << " " << *iter << endl;
	    if(comp_float_abs(get_bixel_energy(iBixel),*iter,epsilon)) {
		found_it = true;
		break;
	    }
	}

	// push back new energy if it does not exist
	if(!found_it) {
	    beam_energy_table_[beamNo].push_back(get_bixel_energy(iBixel));
	}
    }

    // sort the entries
    for(unsigned int iBeam=0; iBeam<get_nBeams(); iBeam++) {
	sort(beam_energy_table_[iBeam].begin(),beam_energy_table_[iBeam].end());
    }
}

/**
 * initialize bixel grid size
 */
void Geometry::initialize_bixel_grid_size()
{
    unsigned int beamNo;

    // loop over the beamlets and get all energies
    for(unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++) {

        beamNo = get_beamNo(iBixel);

        // find minimum x
        if( beam_bixel_minX_[beamNo] > get_bixel_position_x(iBixel) ) {
            beam_bixel_minX_[beamNo] = get_bixel_position_x(iBixel);
        }
        // find maximum x
        if( beam_bixel_maxX_[beamNo] < get_bixel_position_x(iBixel) ) {
            beam_bixel_maxX_[beamNo] = get_bixel_position_x(iBixel);
        }
        // find minimum y
        if( beam_bixel_minY_[beamNo] > get_bixel_position_y(iBixel) ) {
            beam_bixel_minY_[beamNo] = get_bixel_position_y(iBixel);
        }
        // find maximum y
        if( beam_bixel_maxY_[beamNo] < get_bixel_position_y(iBixel) ) {
            beam_bixel_maxY_[beamNo] = get_bixel_position_y(iBixel);
        }
    }

    // set grid size
    for(unsigned int iBeam=0; iBeam<get_nBeams(); iBeam++) {

        float max_x = std::max( fabs(beam_bixel_minX_[iBeam]), fabs(beam_bixel_maxX_[iBeam]) );
        float max_y = std::max( fabs(beam_bixel_minY_[iBeam]), fabs(beam_bixel_maxY_[iBeam]) );

        beam_bixel_nX_[iBeam] = (int) (max_x / beam_bixel_dx_[iBeam] + 0.5);
        beam_bixel_nY_[iBeam] = (int) (max_y / beam_bixel_dy_[iBeam] + 0.5);

        // energies
        beam_bixel_nE_[iBeam] = beam_energy_table_[iBeam].size();

        // total number of points
        beam_bixel_nPoints_[iBeam] = (1+2*beam_bixel_nX_[iBeam]) *
            (1+2*beam_bixel_nY_[iBeam]) * beam_bixel_nE_[iBeam];
	}
}

/**
 * set bixel subscripts
 */
void Geometry::set_bixel_subscripts_and_index()
{
    unsigned int beamNo;
    for(unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++) {
        beamNo = get_beamNo(iBixel);
        bixel_subscripts_[iBixel] = convert_to_Bixel_Subscripts(bixel_position_[iBixel],beamNo);

        // make sure the subscript is valid
        assert_bixel_inside(bixel_subscripts_[iBixel],beamNo);

        bixel_index_[iBixel] = convert_to_BixelIndex(bixel_subscripts_[iBixel],beamNo);
    }
}

/**
 * set bixelNo in bixel grid
 */
bool Geometry::set_bixelNo_in_bixel_grid()
{
    // resize vectors
    for(unsigned int iBeam=0; iBeam<get_nBeams(); iBeam++) {

        // number of bixel grid points for this beam
        unsigned int nGridPoints = (1+2*beam_bixel_nX_[iBeam]) *
            (1+2*beam_bixel_nY_[iBeam]) * beam_bixel_nE_[iBeam];

        beam_grid_is_bixel_[iBeam].resize(nGridPoints);
        beam_grid_bixelNo_[iBeam].resize(nGridPoints);
        fill(beam_grid_is_bixel_[iBeam].begin(), beam_grid_is_bixel_[iBeam].end(), false);
        fill(beam_grid_bixelNo_[iBeam].begin(), beam_grid_bixelNo_[iBeam].end(), get_nBixels());
    }

    // set bixel numbers
    for(unsigned int iBixel=0; iBixel<get_nBixels(); iBixel++) {
        if(beam_grid_is_bixel_[get_beamNo(iBixel)][bixel_index_[iBixel].index_] == true) {

            // there is a duplicated beamlet which may course problems
            std::cout << std::endl;
            std::cout << "Warning: Bixel " << iBixel << " with energy " << get_bixel_energy(iBixel)
                  << " and position x=" << get_bixel_position_x(iBixel) << " and y="
                  << get_bixel_position_y(iBixel) << " is duplicated." << std::endl
                  << "The bixel grid is invalid." << std::endl << std::endl;

            // set all values to zero and quit
            for(unsigned int iBeam=0; iBeam<get_nBeams(); iBeam++) {

                // number of bixel grid points for this beam
                unsigned int nGridPoints = (1+2*beam_bixel_nX_[iBeam]) *
                                        (1+2*beam_bixel_nY_[iBeam]) * beam_bixel_nE_[iBeam];
                fill(beam_grid_is_bixel_[iBeam].begin(), beam_grid_is_bixel_[iBeam].end(), false);
                fill(beam_grid_bixelNo_[iBeam].begin(), beam_grid_bixelNo_[iBeam].end(), get_nBixels());
            }

            return(false);
        } else {
            beam_grid_bixelNo_[get_beamNo(iBixel)][bixel_index_[iBixel].index_] = iBixel;
            beam_grid_is_bixel_[get_beamNo(iBixel)][bixel_index_[iBixel].index_] = true;
        }
    }
    return(true);
}


/**
 * convert bixel position to bixel subscripts
 */
Bixel_Subscripts Geometry::convert_to_Bixel_Subscripts(Bixel_Position rhs, unsigned int beamNo) const
{
    float epsilon = 0.001;
    Bixel_Subscripts temp;

    // lateral position
    temp.iX_ = (int) floor(rhs.x_ / beam_bixel_dx_[beamNo] + beam_bixel_nX_[beamNo] + 0.5);
    temp.iY_ = (int) floor(rhs.y_ / beam_bixel_dy_[beamNo] + beam_bixel_nY_[beamNo] + 0.5);

    // initialize energy with value outside the grid,
    // leads to invalid subscripts if energy is not found
    temp.iE_ = -1;

    // find energy
    for(unsigned int iEnergy=0; iEnergy<beam_energy_table_[beamNo].size(); iEnergy++) {
        if(comp_float_abs(rhs.E_,beam_energy_table_[beamNo][iEnergy],epsilon)) {
            temp.iE_ = iEnergy;
            break;
        }
    }

    return temp;
}

/**
 * print out a dose cube on the planning CT
 */
void Geometry::write_dose_on_planning_CT(const DoseVector &dose_vector, string file_name) const
{
    if(ctatts_.is_initialized()) {
	// interpolate dose cube
	DoseVector temp = ctatts_.interpolate_dose_cube(dose_vector);
	temp.write_dose(file_name);
    }
    else {
	std::cout << "Warning: cannot write dose on planning CT" << std::endl;
	std::cout << "Planning CT attributes are unknown" << std::endl;
    }
}

/**
 * initialize ct attributes class
 */
void Geometry::load_ct_attributes(string ctatts_file_name)
{
    ctatts_ = CTscanAttributes(this,ctatts_file_name);
}






/* ------------------- CT scan information ------------------------ */

/**
 * Constructor
 */
CTscanAttributes::CTscanAttributes()
    : the_geometry_(NULL)
    , is_initialized_(false)
    , slice_distance_(0)
    , pixel_size_(0)
    , nSlices_(0)
    , nDimension_(0)
    , isoX_(0)
    , isoY_(0)
    , isoZ_(0)
{
}

/**
 * Constructor which reads ctatts file
 */
CTscanAttributes::CTscanAttributes(Geometry *the_geometry, string ctatts_file_name)
    : the_geometry_(the_geometry)
    , is_initialized_(false)
{
    string attribute;
    unsigned int uiDummy;
    float fDummy;

    std::ifstream infile;

    infile.open(ctatts_file_name.c_str(), std::ios::in);
    // check if the file exists
    if ( !infile.is_open() ) {
        std::cout << "Can't open file: " << ctatts_file_name << std::endl;
        std::cout << "Warning: Attributes of planning CT unknown" << std::endl;
    }
    else {
	// read ctatts file
	std::cout << "Reading ctatts file: " << ctatts_file_name << endl;

	infile >> attribute;
	while(attribute != "slice_dimension" && infile.good()) {
            infile >> attribute;
	}
	if(attribute != "slice_dimension") {
	    cout << "Error: expected slice_dimension" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> uiDummy;
	nDimension_ = uiDummy;

	infile >> attribute;
	if(attribute != "slice_number") {
	    cout << "Error: expected slice_number" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> uiDummy;
	nSlices_ = uiDummy;

	infile >> attribute;
	if(attribute != "pixel_size") {
	    cout << "Error: expected pixel_size" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> fDummy;
	pixel_size_ = fDummy;

	infile >> attribute;
	if(attribute != "slice_distance") {
	    cout << "Error: expected slice_distance" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> fDummy;
	slice_distance_ = fDummy;

	infile >> attribute;
	if(attribute != "Pos-Isocenter-X") {
	    cout << "Error: expected Pos-Isocenter-X" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> fDummy;
	isoX_ = fDummy;

	infile >> attribute;
	if(attribute != "Pos-Isocenter-Y") {
	    cout << "Error: expected Pos-Isocenter-Y" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> fDummy;
	isoY_ = fDummy;

	infile >> attribute;
	if(attribute != "Pos-Isocenter-Z") {
	    cout << "Error: expected Pos-Isocenter-Z" << endl;
	    throw(std::runtime_error("Error reading .ctatts file"));
	}
	infile >> fDummy;
	isoZ_ = fDummy;

	// close file
	infile.close();

	is_initialized_ = true;
    }
}


/**
 * returns an interpolated dose cube which has the size of the original CT
 */
DoseVector CTscanAttributes::interpolate_dose_cube(const DoseVector &dose_vector) const
{
    cout << "interpolate dose cube to planning CT resolution ... " << endl;

    Point_in_Patient_Coord temp;

    // get number of CT voxels
    unsigned int nVox = nSlices_*nDimension_*nDimension_;

    // create empty dose vector
    DoseVector dose_ct = DoseVector(nVox);

    // loop over all voxels
    for(unsigned int i=1; i<nVox; i++) {

	// convert CT voxel number of point in patient coords
	temp = convert_to_Point_in_Patient_Coord(
	    convert_to_Point_in_CT_Coord(CTVoxelIndex(i)));

	dose_ct.set_voxel_dose(i,the_geometry_->get_dose_by_trilinear_interpolation(dose_vector,temp));
    }

    return dose_ct;
}

/**
 * Convert from point in CT coordinates to point in patient coordinates
 */
Point_in_Patient_Coord CTscanAttributes::convert_to_Point_in_Patient_Coord(const Point_in_CT_Coord rhs) const
{
    // get isocenter in patient coordinates
    Point_in_Patient_Coord iso_dose = the_geometry_->get_isocenter(0);

    Point_in_Patient_Coord temp;

    // x is indeed x=LR
    temp.x_ = rhs.x_ - (isoX_ - iso_dose.x_);

    // y in CT is z=AP in opt4D, and axis is mirrord
    temp.y_ = rhs.z_ - (isoZ_ - iso_dose.y_);

    // z in CT is y=SI in opt4D
    temp.z_ = - rhs.y_ + (isoY_ + iso_dose.z_);

    return temp;
}

/**
 * print patient shift
 */
std::ostream& operator<< (std::ostream& o, const Shift_in_Patient_Coord& rhs)
{
    o << "(" << rhs.x_ << ", " << rhs.y_ << ", " << rhs.z_ << ")";
}

/**
 * print voxel shift subscript
 */
std::ostream& operator<< (std::ostream& o, const Voxel_Shift_Subscripts& rhs)
{
    o << "(" << rhs.iX_ << ", " << rhs.iY_ << ", " << rhs.iZ_ << ")";
}

/**
 * print bixel shift
 */
std::ostream& operator<< (std::ostream& o, const Bixel_Shift_Subscripts& rhs)
{
    o << "(" << rhs.iX_ << ", " << rhs.iY_ << ", " << rhs.iE_ << ")";
}

/**
 * print bixel subscript
 */
std::ostream& operator<< (std::ostream& o, const Bixel_Subscripts& rhs)
{
    o << "(" << rhs.iX_ << ", " << rhs.iY_ << ", " << rhs.iE_ << ")";
}

/**
 * print bixel position
 */
std::ostream& operator<< (std::ostream& o, const Bixel_Position& rhs)
{
    o << "(" << rhs.x_ << ", " << rhs.y_ << ", " << rhs.E_ << ")";
}



/**
 * Calculate Radiological Path Length. This is purely a dummy
 * implementation.
 */
/*
inline
float Geometry::calculate_RPL(
    const unsigned int beamNo,
    const unsigned int j, const float z)const
{
  const float r_phant=15.;
  if (z<beam_sad_[beamNo]-r_phant) return 0.;
  else if (z>beam_sad_[beamNo]+r_phant) return 2*r_phant;
  else return z-beam_sad_[beamNo]+r_phant;
}
*/
/**
 * Rotate a point from the table coordinate system to the gantry
 * coordinate system.  Changes the input variables.
 *
 * @param beamNo Beam number
 * @param *xx x-coordiante
 * @param *yy y-coordiante
 * @param *zz z-coordiante
 */
/*
void Geometry::rot_table_gantry(unsigned int beamNo, float* xx, float* yy, float* zz) const
{
  float xf, yf, zf;

  // rotation from table system into fixed system using table angle around
  // z-axis
  xf = (*xx)*std::cos(beam_table_angle_[beamNo]*PI/180.)
    + (*yy)*std::sin(beam_table_angle_[beamNo]*PI/180.);
  yf = -(*xx)*std::sin(beam_table_angle_[beamNo]*PI/180.)
    + (*yy)*std::cos(beam_table_angle_[beamNo]*PI/180.);
  zf = (*zz);

  // gantry rotation (around y axis) into gantry system
  (*xx) = xf*std::cos(beam_gantry_angle_[beamNo]*PI/180.)
    + zf*std::sin(beam_gantry_angle_[beamNo]*PI/180.);
  (*yy) = yf;
  (*zz) = -xf*std::sin(beam_gantry_angle_[beamNo]*PI/180.)
    + zf*std::cos(beam_gantry_angle_[beamNo]*PI/180.);

  //  scalefact = SAD/(SAD-zg);

  // gantry system into cone system (beam divergence)
  //  xcone = xg * scalefact;
  //  ycone = yg * scalefact;
  //  (*xx) = xcone;
  //  (*yy) = ycone;
  // this is the correct formula:
  //  (*zz) = zg * sqrt(1. + (xcone*xcone+ycone*ycone)/SAD/SAD);
  // 1st order Taylor series: sqrt(1+eps) approx 1 + eps/2:
  //  (*zz) = zg * (1. + (xcone*xcone+ycone*ycone)/SAD/SAD/2.);

}
*/

/**
 * Translate from a parallel to a conical coordinate system
 */
/*
void Geometry::trans_par_cone(const unsigned int beamNo, float* xx, float* yy, float* zz) const
{
  float scalefact;

  scalefact = beam_sad_[beamNo]/(beam_sad_[beamNo]-(*zz));

  // into cone system (beam divergence)
  (*xx) = (*xx) * scalefact;
  (*yy) = (*yy) * scalefact;

  // z is the distance from the source
  // this is the correct formula:
  //  (*zz) = (SAD-(*zz)) * sqrt(1. + ((*xx)*(*xx)+(*yy)*(*yy))/SAD/SAD);
  // 1st order Taylor series: sqrt(1+eps) approx 1 + eps/2
  (*zz) = (beam_sad_[beamNo]-(*zz)) * (1. + ((*xx)*(*xx)+(*yy)*(*yy))/beam_sad_[beamNo]/beam_sad_[beamNo]/2.);

}
*/



