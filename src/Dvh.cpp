
/**
 * @file Dvh.cpp
 *
 * Function definitions for Dvh class.
 * 
 *  $Id: Dvh.cpp,v 1.3 2007/10/24 16:29:35 unkelbac Exp $
 */

#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <math.h>

#include "Dvh.hpp"

/**
 * Constructor
 *
 * @param the_vois The volumes of interest that the DVH will be based on.  The 
 * calling function is responsible for making sure that Dvh::calculate is not 
 * called after the volumes of interest are deleted.  Does not delete the 
 * Vois!!!!
 *
 * @param DVH_bin_size The size of the bins to use in Gy. Must be > 1.
 */
Dvh::Dvh(const vector<const Voi* > & the_vois, float DVH_bin_size)
    : the_vois_(the_vois)
    , nVois_(the_vois.size())
    , bin_size_(DVH_bin_size)
    , DVH_(nVois_)
    , max_nBins_(0)
{
}

/**
 * add one DVH to another, extend number of bins if necessary
 */
Dvh& Dvh::operator+= (const Dvh &rhs)
{
    float epsilon = 0.000001;

    // check number of vois and bin size
    if(nVois_ != rhs.nVois_) {
	cout << "cannot add DVHs with different numbers of Vois" << endl;
	throw(std::logic_error("cannot add DVHs"));
    }
    if(fabs(bin_size_ - rhs.bin_size_) > epsilon) {
	cout << "cannot add DVHs with different bin size" << endl;
	throw(std::logic_error("cannot add DVHs"));
    }
    
    // loop over Vois
    for(unsigned int iVoi = 0; iVoi < nVois_; iVoi++) {

	// check if number of bins has to be extended
	if(rhs.DVH_[iVoi].size() > DVH_[iVoi].size()) {
	    DVH_[iVoi].resize(rhs.DVH_[iVoi].size(),0);
	}

	// check if max_nBins_ has to be updated
	if(DVH_[iVoi].size() > max_nBins_) {
	    max_nBins_ = DVH_[iVoi].size();
	}

	// add values
	for(unsigned int i=0; i<rhs.DVH_[iVoi].size(); i++) {
	    DVH_[iVoi][i] += rhs.DVH_[iVoi][i];
	}
    }

    return(*this);
}

/**
 * scale DVH values by a number
 */
Dvh& Dvh::operator*= (const float &rhs)
{
    // loop over Vois
    for(unsigned int iVoi = 0; iVoi < nVois_; iVoi++) {

	// scale values
	for(unsigned int i=0; i<DVH_[iVoi].size(); i++) {
	    DVH_[iVoi][i] *= rhs;
	}
    }

    return(*this);
}


/**
 * Calculate dose-volume histogram based on voxel doses.
 */
void Dvh::calculate(const DoseVector & dose_vector)
{
    max_nBins_ = 0;
    float scale_dose = 1./bin_size_;

    for(unsigned int iVoi = 0; iVoi < nVois_; iVoi++) {

        /*
         * Find the maximum dose in the VOI
         */
        float max_dose = 0;
        unsigned int nVoxels = the_vois_[iVoi]->get_nVoxels();
        for(unsigned int iVoxel = 0; iVoxel < nVoxels; iVoxel++) {
            unsigned int voxelNo = the_vois_[iVoi]->get_voxel(iVoxel);
            if(dose_vector[voxelNo] > max_dose) {
                max_dose = dose_vector[voxelNo];
            }
        }

        /*
         * Reserve enough space to hold DVH
         */
        unsigned int voi_nBins = int(max_dose * scale_dose) + 1;
        if (voi_nBins > DVH_[iVoi].capacity()) {
            DVH_[iVoi].reserve(int(voi_nBins*1.1));
            // cout << "Voi " << iVoi << " DVH point capacity increased to "
            //      << DVH_[iVoi].capacity() << endl;
        }


        /*
         * resize DVH and reset to zero
         */
        DVH_[iVoi].resize(voi_nBins);
        fill(DVH_[iVoi].begin(), DVH_[iVoi].end(), float(0.));

        /*
         * Count the number of voxels in each dose bin
         */
        for(unsigned int iVoxel = 0; iVoxel < nVoxels; iVoxel++) {
            unsigned int voxelNo = the_vois_[iVoi]->get_voxel(iVoxel);
            float dose = dose_vector.get_voxel_dose(voxelNo);
            unsigned int iDose = int(dose * scale_dose );
            DVH_[iVoi][iDose] += 1;
        }

        /*
         * Integrate DVH starting at tail end
         */
        if(voi_nBins >=2) {
            for(unsigned int iDose = voi_nBins-1; iDose > 0; iDose--) {
                DVH_[iVoi][iDose-1] += DVH_[iVoi][iDose];
            }
        }

        /*
         * Normalize DVH
         */
        float normalization_factor = 100.f / DVH_[iVoi][0];
        for(unsigned int iDose = 0; iDose < voi_nBins; iDose++) {
            DVH_[iVoi][iDose] *= normalization_factor;
        }

        if(DVH_[iVoi].size() > max_nBins_) {
            max_nBins_ = DVH_[iVoi].size();
        }
    }
}


/**
 *  Print DVH on a stream.
 *
 * @param o A stream to  write the DVH onto.
 */
void Dvh::print_on(std::ostream & o) const
{
    o << std::left;

    for(unsigned int iBin = 0; iBin < max_nBins_; iBin++) {
        // Write the dose
        o << std::setw(8) << get_dose(iBin);

        // Write the percentage for each VOI (skipping air)
        for(unsigned int iVoi=0; iVoi<nVois_; iVoi++) {
            // don't print air DVH
            if(! the_vois_[iVoi]->is_air()) {
                o << " " << std::setw(9) << get_percentage(iVoi, iBin);
            }
        }
        o << "\n";
    }
    return;
}


/**
 * Write DVH to file.
 *
 * @param DVHfile the name of the file to write to
 */
void Dvh::write_DVH(std::string DVHfile) const
{
    std::ofstream ofile(DVHfile.c_str(), std::ios::out | std::ios::trunc);
    print_on(ofile);
    ofile.close();
    return;
}


/**
 * Append DVH to file.
 *
 * @param DVHfile the name of the file to write to
 */
void Dvh::append_DVH(std::string DVHfile) const
{
    std::ofstream ofile(DVHfile.c_str(), std::ios::app | std::ios::trunc);
    print_on(ofile);
    ofile.close();
    return;
}

