/**
 * @file Optimization.hpp
 * Optimization Class Header
 *
 * $Id: Optimization.hpp,v 1.55 2008/06/03 16:23:34 bmartin Exp $
 */

#ifndef OPTIMIZATION_HPP
#define OPTIMIZATION_HPP

class Optimization;
      
#include <vector>
using std::vector;
#include <queue>
using std::priority_queue;
#include <set>
using std::set;
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

#include "Plan.hpp"
#include "Voi.hpp"
#include "CpuTimer.hpp"
#include "Optimizer.hpp"
#include "ConjugateGradient.hpp"
#include "DeltaBarDelta.hpp"
#include "SteepestDescent.hpp"
#include "Lbfgs.hpp"
#include "ApertureSequencer.hpp"


enum OptimizationTypes {
    STEEPEST_DESCENT,  DIAGONAL_NEWTON,  LBFGS,  LEGACY,  CONJUGATE_GRADIENT,  DELTA_BAR_DELTA,  MOMENTUM,
    STOCHASTIC_META_DESCENT, LINEAR_PROJECTION_SOLVER, MOSEK, OPT_PP, DAO
};

// Print OptimizationType
inline std::ostream& operator<< (std::ostream& o,  const OptimizationTypes type)
{
    switch ( type) {
        case STEEPEST_DESCENT:
            return o << "STEEPEST_DESCENT";
        case DIAGONAL_NEWTON:
            return o << "DIAGONAL_NEWTON";
        case LBFGS:
            return o << "LBFGS";
        case LEGACY:
            return o << "LEGACY";
        case CONJUGATE_GRADIENT:
            return o << "CONJUGATE_GRADIENT";
        case DELTA_BAR_DELTA:
            return o << "DELTA_BAR_DELTA";
        case MOMENTUM:
            return o << "MOMENTUM";
        case STOCHASTIC_META_DESCENT:
            return o << "STOCHASTIC_META_DESCENT";
        case LINEAR_PROJECTION_SOLVER:
            return o << "LINEAR_PROJECTION_SOLVER";
        case MOSEK:
            return o << "MOSEK";
        case OPT_PP:
            return o << "OPT_PP";
        case DAO:
            return o << "DAO";
        default:
            throw std::runtime_error("Invalid switch type.");
    }
};


/// type of OptimizationEvents.  Make sure to update the print operator when
/// adding event types.  Make sure stop stays the last type in the declaration
enum OptimizationEventType {
    start_OptimizationEventType=0,  ACTIVATE_STOP_FRACT = start_OptimizationEventType,  UPDATE_SAMPLING,
    CALCULATE_TRUE_OBJECTIVE,  ESTIMATE_TRUE_OBJECTIVE,  CALCULATE_DIRECTIONAL_DERIVATIVE,
    DECREASE_STEP_SIZE,  INCREASE_AS_SAMPLING_FRACTION,  INCREASE_SCENARIO_SAMPLES,  SET_ZERO_PARAMETERS,
    SET_UNIT_PARAMETERS,  WRITE_PARAMETER_FILES,  WRITE_DVH,  WRITE_DOSE,  WRITE_CT_DOSE,  WRITE_BEAM_DOSE,
    WRITE_SCENARIO_DOSE,  WRITE_EXPECTED_DOSE,  WRITE_AUX_DOSE,  WRITE_INST_DOSE,  WRITE_VARIANCE,
    WRITE_OBJ_SAMPLES,  WRITE_DOSE_SAMPLES,  UPDATE_LAGRANGE_MULTIPLIERS,  UPDATE_PENALTY,  
	SWITCH_OPTIMIZER,  SEQUENCE_APERTURES,
    STOP,  end_OptimizationEventType
};
typedef enum OptimizationEventType OptimizationEventType;

template <class Enum> Enum & enum_increment( Enum & value, Enum begin, Enum end )
{
    return value = (value == end) ? begin : Enum(value + 1);
}

inline OptimizationEventType& operator++( OptimizationEventType & rhs )
{
    return enum_increment<OptimizationEventType>(rhs, start_OptimizationEventType, end_OptimizationEventType);
}


/// Print OptimizationEventType
inline std::ostream& operator<< ( std::ostream& o,  const OptimizationEventType event_type )
{
    switch ( event_type) {
    case ACTIVATE_STOP_FRACT:
        return o << "ACTIVATE_STOP_FRACT";
    case UPDATE_SAMPLING:
        return o << "UPDATE_SAMPLING";
    case CALCULATE_TRUE_OBJECTIVE:
        return o << "CALCULATE_TRUE_OBJECTIVE";
    case ESTIMATE_TRUE_OBJECTIVE:
        return o << "ESTIMATE_TRUE_OBJECTIVE";
    case CALCULATE_DIRECTIONAL_DERIVATIVE:
        return o << "CALCULATE_DIRECTIONAL_DERIVATIVE";
    case DECREASE_STEP_SIZE:
        return o << "DECREASE_STEP_SIZE";
    case INCREASE_AS_SAMPLING_FRACTION:
        return o << "INCREASE_AS_SAMPLING_FRACTION";
    case INCREASE_SCENARIO_SAMPLES:
        return o << "INCREASE_SCENARIO_SAMPLES";
    case SET_ZERO_PARAMETERS:
        return o << "SET_ZERO_PARAMETERS";
    case SET_UNIT_PARAMETERS:
        return o << "SET_UNIT_PARAMETERS";
    case WRITE_PARAMETER_FILES:
        return o << "WRITE_PARAMETER_FILES";
    case WRITE_DVH:
        return o << "WRITE_DVH";
    case WRITE_DOSE:
        return o << "WRITE_DOSE";
    case WRITE_CT_DOSE:
        return o << "WRITE_CT_DOSE";
    case WRITE_BEAM_DOSE:
        return o << "WRITE_BEAM_DOSE";
    case WRITE_SCENARIO_DOSE:
        return o << "WRITE_SCENARIO_DOSE";
    case WRITE_EXPECTED_DOSE:
        return o << "WRITE_EXPECTED_DOSE";
    case WRITE_AUX_DOSE:
        return o << "WRITE_AUX_DOSE";
    case WRITE_INST_DOSE:
        return o << "WRITE_INST_DOSE";
    case WRITE_VARIANCE:
        return o << "WRITE_VARIANCE";
    case WRITE_OBJ_SAMPLES:
        return o << "WRITE_OBJ_SAMPLES";
    case WRITE_DOSE_SAMPLES:
        return o << "WRITE_DOSE_SAMPLES";
    case UPDATE_LAGRANGE_MULTIPLIERS:
        return o << "UPDATE_LAGRANGE_MULTIPLIERS";
    case UPDATE_PENALTY:
	  return o << "UPDATE_PENALTY";
    case SWITCH_OPTIMIZER:
        return o << "SWITCH_OPTIMIZER";
    case SEQUENCE_APERTURES:
        return o << "SEQUENCE_APERTURES";
    case STOP:
        return o << "STOP";
    default:
        throw std::logic_error("Invalid enum value.");
    }
};


/// Read OptimizationEventType from a stream
std::istream& operator>> ( std::istream& i, OptimizationEventType & event_type );

/**
 * Class Optimization does the actual optimization of a plan.
 * <p>
 * Several optimization algorithms are available, but all are gradient based.
 * <p>
 * The overall algorithm works like this:
 * <ol>
 *   <li> Initialization:
 *   <ol>
 *     <li> load Plan into memory
 *     <li> set initial beam weights to zero
 *     <li> Compute initial value of objective
 *     <li> Determine scaling factor
 *     <li> Reset beam weights to their initial value
 *   </ol>
 *   <li> Use Plan class to Calculate Objective and first and second partial
 *        derivative of objective with respect to the beam weights.
 *   <li> Quit if objective is satisfactory.
 *   <li> Update the beam weights based on a scaled gradient method.
 *   <li> Go back to step 2.
 * </ol>
 * <p>
 * Any optimization algorithm that only needs the objective and gradient at each step can be used.
 */
class Optimization
{

public:

    /**
     * Class for events used in optimization.
     */
    class event
    {
    public:

        /// Constructor
        event( OptimizationEventType type = STOP,  double time_or_step = 0,  double recurrence = 0 )
            : type_(type), trigger_(time_or_step), recurrence_(recurrence) {};

        /// Comparison operator (operates backwards to make priority queue work)
        bool operator< (const Optimization::event & rhs) const {
            // Note that this is backwards
            return trigger_ > rhs.trigger_;
        };

        // Print description of event
        friend std::ostream& operator<< ( std::ostream& o,  const Optimization::event& rhs );

        // Read event from stream
        friend std::istream& operator>> ( std::istream& i,  Optimization::event& rhs );

        /// Type of event
        OptimizationEventType type_;

        /// Trigger for event
        double trigger_;

        /// Recurrence time/steps after being triggered (0 to not recurr)
        double recurrence_;

    private:

    };

    /**
     * Optimization options
     *
     * Make sure you update the print_on function when adding or changing
     * options.
     */
    class options
    {
    public:
        options()
            : optimization_type_(1,STEEPEST_DESCENT),  out_file_prefix_(),  step_events_(),
              time_events_(),  final_events_(),  step_size_multiplier_(1),  use_hessian_(true),
              use_alternate_linesearch_(false),  verbosity_(1),  linesearch_steps_(10),
              use_optimal_steps_(false),  use_strong_wolfe_(false), 
			  wolfe_condition_c1_(0.0001),  wolfe_condition_c2_(0.9), 
			  use_voxel_sampling_(false),  use_scenario_sampling_(false),  nScenario_samples_per_step_(),
              calculate_true_objective_(false),  use_diminishing_step_sizes_(false),
              momentum_gradient_smoothing_exponent_(0.7),  smd_smoothing_coefficient_(0.95),
              smd_meta_step_size_(0.01),  lps_maxmin_(false),  lps_minmax_(false),
              lps_objNo_(1),  lps_epsilon_(0.1),  delta_bar_delta_options_(),  min_steps_(0),
              max_steps_(100),  max_time_(3600),  stopping_fraction_(0),  daoOpt_(false),
              sequencer_options_(),  use_adaptive_sampling_(false),  as_update_period_(1),
              as_sampling_fraction_(0.1),  as_min_sampling_fraction_(0),  as_smoothing_factor_(0.5),
              calculate_constrained_gradient_(false),  calculate_feasible_gradient_(false),
              project_on_bound_constraints_(true),  steepest_descent_options_(),
              estimate_sample_voxels_(false), estimate_max_samples_(100),
              estimate_min_samples_(10),  estimate_max_uncertainty_(0.01),
              expected_dose_nScenarios_(100),  write_dose_samples_nDoses_(10),
	          constraint_tolerance_(0.01), constraint_penalty_multiplier_(2.0), max_apertures_(100), use_variable_transformation_(false) { };

        /// Print options
        friend std::ostream& operator<< ( std::ostream& o, const Optimization::options& opt );

        /// Which optimization algorithm to use
        vector<OptimizationTypes> optimization_type_;

        /// Prefix to use when writing files
        string out_file_prefix_;

        /// Extra events based on step count
        vector<Optimization::event> step_events_;

        /// Extra events based on time elapsed
        vector<Optimization::event> time_events_;

        /// Extra events once the optimization is complete
        vector<OptimizationEventType> final_events_;

        /// Number to multiply the default step size by
        double step_size_multiplier_;

        /// Determine if the optimization should use the Hessian during the optimization.
        bool use_hessian_;

        /// Optionally use a different linesearch algorithm (uses directional / derivatives)
        bool use_alternate_linesearch_;

        /// Verbosity (how much debugging info is shown, default 1)
        int verbosity_;

        /// How many steps to use in linesearch (default 3)
        unsigned int linesearch_steps_;

        /// Whether or not to use a linesearch to find the optimal step size
        bool use_optimal_steps_;

 		/// Whether or not to use a step size that satisfies the strong wolfe conditions
		bool use_strong_wolfe_;
		  
		/// sufficient decrease parameter for the strong wolfe condition
		float wolfe_condition_c1_;
		  
		/// curvature parameter for the strong wolfe condition
		float wolfe_condition_c2_;

        /// Whether or not to use voxel sampling during the optimization
		bool use_voxel_sampling_;

        /// Whether or not to evaluate meta-objectives exactly (not possible for most uncertainty models)
        bool use_scenario_sampling_;

        /// Number of scenario samples to use per optimization step
        vector<unsigned int> nScenario_samples_per_step_;

        /// Whether or not to calculate the true objective if using voxel sampling during the optimization
        bool calculate_true_objective_;

        /// Diminishing step sizes
        bool use_diminishing_step_sizes_;

        /// Gradient smoothing exponent for momentum
        double momentum_gradient_smoothing_exponent_;

        /// Parameters for SMD
        double smd_smoothing_coefficient_;
        double smd_meta_step_size_;

        /// Parameters for linear projection solver
        bool lps_maxmin_;
        bool lps_minmax_;
        unsigned int lps_objNo_;
        float lps_epsilon_;

        /// Parameters for Delta-bar-delta
        DeltaBarDelta::options delta_bar_delta_options_;

        /// The minimum number of optimization iterations to perform.
        unsigned int min_steps_;
        /// The maximum number of optimization iterations to perform.
        unsigned int max_steps_;
        /// The maximum amount of time to spend on optimization.
        double max_time_;
        /// Stop if the objective functions improves by less than this fraction. (.002 corresponds to a 0.2% improvement)
        double stopping_fraction_;

        //
        // Direct Aperture Options
        //

	    // max number of apertures in column generation
	    unsigned int max_apertures_;

        /// Optimize aperture parameters directly rather than bixel weights
        bool daoOpt_;
        /// Options for ApertureSequencer class
        ApertureSequencer::options sequencer_options_;

        //
        // Adaptive Sampling Options
        //

        /// True if the optimization should adapt the sampling fraction
        bool use_adaptive_sampling_;

        /// The number of steps between updating the sampling fraction
        float as_update_period_;

        /// Fraction of voxels to use as goal during sampling rate adaptation
        float as_sampling_fraction_;

        /// Minimum fraction of voxels for any objective during sampling rate adaptation.
        /// Set to zero to use the initial sampling fractions as the minimum.
        float as_min_sampling_fraction_;

        /// mean squared voxel objective smoothing factor.
        /// Must be greater than zero and less than or equal to 1.  Higher values are smoother.
        double as_smoothing_factor_;


        /// True if you should use the constrained gradient for SMD or CG optimization. (default false)
        bool calculate_constrained_gradient_;

        /// True if you should use the feasible gradient for SMD or CG optimization. (default false)
        bool calculate_feasible_gradient_;

        /// True if we project onto parameter bound constraints after each step (default true)
        bool project_on_bound_constraints_;

        /// handle positivity constraint via variable transformation
	    bool use_variable_transformation_;

        /// Options for SteepestDescent class
        SteepestDescent::options steepest_descent_options_;


        /// Should we use voxel sampling while estimating the true objective?
        bool estimate_sample_voxels_;

        /// Maximum number of samples to use when estimating true objective
        unsigned int estimate_max_samples_;

        /// Minimum number of samples to use when estimating true objective
        unsigned int estimate_min_samples_;

        /// Maximum alowed uncertainty to use when estimating true objective
        float estimate_max_uncertainty_;

        /// Number of scenarios to use while calculating the expected dose
        unsigned int expected_dose_nScenarios_;

        /// Number of samples of dose to write for action WRITE_DOSE_SAMPLES
        unsigned int write_dose_samples_nDoses_;

        /// Options for limited memory BFGS quasi-newton
        Lbfgs::options lbfgs_options_;

  	    /// tolerance of constraint satisfaction for augmented lagrangian method
	    float constraint_tolerance_;
	    
	    /// factor by which penalties for constraints are increased for augmented lagrangian method
	    float constraint_penalty_multiplier_;

    };

    /**
     * Optimization logging class
     */
    class log
    {
    public:
        // Constructor
        log( string file_name,  int initial_stepNo=0 );

        // Destructor
        ~log();

        // Store properties of optimization
        template<typename T> void add_optimization_contents( string tag,  T value );

        // store properties of step
        template<typename T> void add_step_attribute( string key,  T value );
        template<typename T> void add_step_contents( string tag,  T value );

        // Finish the step and supply the time
        void finish_step( double time );

        // Start a new optimization element
        void new_optimization( int stepNo = 0 );

    private:
        log(); ///< Default constructor forbidden

        void ensure_optimization_open();

        /// The xml file that is written to
        std::ofstream out_file_;

        /// The step number for the current step
        int stepNo_;

        /// The temporary stream used to hold the current step attributes
        std::ostringstream current_step_attributes_;

        /// Temporary stream to hold current step contents
        std::ostringstream current_step_contents_;

        /// True if <OPT4D_HIST> has been written to the file but not </OPT4D_HIST>
        bool opt4d_hist_open_;

        /// True if <header> has been written to the file but not </header>
        bool header_open_;

        /// True if <optimization> has been written to the file but not </optimization>
        bool optimization_open_;

    };

  Optimization(Plan* thisplan, string log_file_name, Optimization::options options);
  //    ~Optimization();

    void set_options( Optimization::options options);
    Optimization::options get_options();

    virtual void initialize() = 0;

    void reset_history();
    virtual void reset() {};
    virtual void reset(ParameterVector * parameters) {};

    virtual void optimize() = 0;

  //    void calc_norm_vector();
  //    void calculate_total_dose();

    void write_history_file(string file_prefix);

    class exception;

protected:


    // Event handlers

    /// Pull current events off a priority queue
    int harvest_events( priority_queue<Optimization::event> & p_queue, double trigger,
                        set<OptimizationEventType> & event_set ) const;


    /// Call the aperture sequencer.
    void call_aperture_sequencer();

    // Preform sampling adaptation
    void init_adaptive_sampling();
    void adjust_sampling_rate();
    void reset_adaptive_sampling();

    /* * * * * * * * * * *
     *    Data Members   *
     * * * * * * * * * * */

    /// The plan to be optimized
    Plan* the_plan_;

    /// The current value of the objective function
    double objective_;

    /// The current value of the objective for each constraint
    vector<double> multi_objective_;

    /// Options for the optimization
    Optimization::options options_;

    /// History for the optimization
    mutable Optimization::log log_;

    /// Stop watch for timing
    mutable CpuTimer timer_;

    /// Minimum sampling fraction for each objective
    vector<float>  min_sampling_fractions_;

    /// Minimum sampling fraction for each objective
    vector<float>  initial_sampling_fractions_;

    /// For use with adaptive sampling
    vector<double>  last_ssvo_;

    /// For use with adaptive sampling
    vector<double>  smoothed_ssvo_;

    /// File counters for written files
    int bwf_file_counter_, dose_file_counter_, dvh_file_counter_;

    /// Sequencer to convert bixel vector to direct parameter vector
    ApertureSequencer daoSeq_;
};


/**
 * Exception thrown when something goes wrong with the optimization.  Simply send to cout to read.
 */
class Optimization::exception
{
public:
    exception(std::string comment) : comment_(comment) {};

    // Print description of exception
    friend std::ostream& operator<< (std::ostream& o, const Optimization::exception& rhs);

private:
    /// Private default constructor to ensure that it is never called
    exception();

    /// The comment for the objective
    std::string comment_;
};


/**
 * Print the reason that the exception was called
 */
inline std::ostream& operator<< (std::ostream& o, const Optimization::exception& rhs)
{
    o << rhs.comment_;
    return o;
}

/// Print vector of OptimizationEventType to stream
std::ostream& operator<< ( std::ostream& o,  const vector<OptimizationEventType> & rhs );

/// Print set of OptimizationEventType to stream
std::ostream& operator<< ( std::ostream& o,  const set<OptimizationEventType> & rhs );

/// Print vector of Optimization::event to stream
std::ostream& operator<< ( std::ostream& o,  const vector<Optimization::event> & rhs );


/* Inline functions */


/**
 * Set optimization options
 * @param options The options to use
 */
inline void Optimization::set_options( Optimization::options options )
{
    options_ = options;
}


/**
 * Get optimization options
 * @returns A copy of the current optimization options
 */
inline Optimization::options Optimization::get_options()
{
    return options_;
}


/**
 * Reset the history of the optimization
 */
inline void Optimization::reset_history()
{
    log_.new_optimization();
    reset_adaptive_sampling();
}




/**
 * Print vector of objects to a stream as space delimited list
 */
template<typename T> inline std::ostream& operator<< (std::ostream& o, const vector<T>& rhs)
{
    if (rhs.size() > 0) {
        o << rhs[0];
    }
    for (unsigned i = 1; i < rhs.size(); i++) {
        o << " " << rhs[i];
    }
    return o;
};


/**
 * Add an attribute to the current step
 */
template<typename T> inline void Optimization::log::add_step_attribute(string key, T value)
{
    current_step_attributes_ << " " << key << "=\"" << value << "\"";
}


/**
 * Add an element to the current step
 */
template<typename T> inline void Optimization::log::add_step_contents(string tag, T value)
{
    current_step_contents_ << "<" << tag << ">" << value << "</" << tag << ">\n";
}


/**
 * Add an element to the current step
 */
template<typename T> inline void Optimization::log::add_optimization_contents(string tag, T value)
{
    // Make sure we are in an optimization step
    ensure_optimization_open();

    // Write out tag
    out_file_ << "<" << tag << ">" << value << "</" << tag << ">\n";
}


#endif
