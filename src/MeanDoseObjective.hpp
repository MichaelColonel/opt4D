/**
 * @file MeanDoseObjective.hpp
 * MeanDoseObjective Class header
 *
 * $Id: MeanDoseObjective.hpp,v 1.26 2008/03/14 15:00:26 bmartin Exp $
 */

#ifndef MEANDOSEOBJECTIVE_HPP
#define MEANDOSEOBJECTIVE_HPP

#include "SeparableObjective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class MeanDoseObjective is used to apply linear mean objectives.
 */
class MeanDoseObjective : public SeparableObjective
{

    public:

        // Constructor
        MeanDoseObjective(
                Voi*  the_voi,
                unsigned int objNo,
                float weight,
                float sampling_fraction,
                bool sample_with_replacement,
		bool is_minimized);

        // Virtual destructor
        virtual ~MeanDoseObjective();

        //
        // Functions reimplemented from SeparableObjective
        //
        virtual double calculate_voxel_objective(
                float dose, unsigned int voxelNo, double nVoxels) const;
        virtual double calculate_dvoxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const;
        virtual double calculate_d2voxel_objective_by_dose(
                float dose, unsigned int voxelNo, double nVoxels) const;

        //
        // Functions reimplemented from Objective
        //

        /// Prints a description of the objective
        virtual void printOn(std::ostream& o) const;

        /// Metaobjective support
        bool supports_meta_objective() const {return true;};


    private:
        // Default constructor not allowed
        MeanDoseObjective();

        /// True if mean dose is minimized, false if maximized
        bool is_minimized_;

};

/*
 * Inline functions
 */


/**
 * Calculate the contribution to the objective from one voxel with the given 
 * dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double MeanDoseObjective::calculate_voxel_objective(
        float dose, unsigned int voxelNo, double nVoxels) const
{
  return dose * weight_ / nVoxels;
}


/**
 * Calculate the partial derivative of the voxel contribution to the objective 
 * from one voxel with the given dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double MeanDoseObjective::calculate_dvoxel_objective_by_dose(
        float dose, unsigned int voxelNo, double nVoxels) const
{
  return weight_ / nVoxels;
}


/**
 * Calculate the second partial derivative of the voxel contribution to the 
 * objective from one voxel with the given dose.
 *
 * @param dose The dose of the voxel.
 */
inline
double MeanDoseObjective::calculate_d2voxel_objective_by_dose(
        float dose, unsigned int voxelNo, double nVoxels) const
{
    return 0;
}

#endif
