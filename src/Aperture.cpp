/*
 *  Aperture.cpp
 */


#include "Aperture.hpp"

/**
 * Constructor
 */
Aperture::Aperture(const Geometry* geometry, unsigned int beamNo)
: geometry_(geometry)
, beamNo_(beamNo)
, nLeafs_(0)
, nBixelsinRow_(0)
, nActive_leafs_(0)
, right_bixel_()
, left_bixel_()
, max_right_bixel_()
, max_left_bixel_()
, has_bixels_()
, is_open_()
, bixel_vector_(geometry->get_nBixels())
, aperture_opening_()
, verbose_(true)
{
  /*
	we assume leaf travel direction is x
	thus y corresponds to leaf pair index
  */
	
  if(verbose_) {
	cout << "constructing aperture for beam " << beamNo << endl;
  } 

	// initialize the number of leaf pairs to the y size of the bixel grid
	nLeafs_ = 1+2*geometry_->get_bixel_grid_nY(beamNo_);
	nBixelsinRow_ = 1+2*geometry_->get_bixel_grid_nX(beamNo_);
	
	cout << "nLeafs: " << nLeafs_ << " nBixelsinRow: " << nBixelsinRow_ << endl;

	// initialize vectors
	right_bixel_.resize(nLeafs_,0);
	left_bixel_.resize(nLeafs_,0);
	max_right_bixel_.resize(nLeafs_,0);
	max_left_bixel_.resize(nLeafs_,nBixelsinRow_-1);
	has_bixels_.resize(nLeafs_,false);
	is_open_.resize(nLeafs_,false);

	for (unsigned int iLeaf=0; iLeaf<nLeafs_; iLeaf++) {
	  aperture_opening_.push_back(vector<bool>(nBixelsinRow_,false));
	}
	
	/*
	 loop over all bixels and determine the active leaf pairs and the extreme positions
	 a leaf pair is set not active if there is no valid bixel in this row
	 */
	for (unsigned int iBixel=0; iBixel<geometry_->get_nBixels(); iBixel++) {
		
		if (geometry_->get_beamNo(iBixel) == beamNo_) {
			
			// get bixel subscripts
			Bixel_Subscripts bixelsub = geometry_->get_bixel_subscript(iBixel);
			
			// the leaf pair index corresponds to the y index of the bixel
			unsigned int iLeaf = bixelsub.iY_;
			unsigned int iBix = bixelsub.iX_;
			
			// set leaf pair active
			has_bixels_[iLeaf] = true;
			//aperture_opening_[iLeaf][iBix] = true;

			// update the leaf position bounds
			if (iBix > max_right_bixel_[iLeaf]) {
				max_right_bixel_[iLeaf] = iBix;
			}
			if (iBix < max_left_bixel_[iLeaf]) {
				max_left_bixel_[iLeaf] = iBix;
			}
		}
	}
	
	
	// count active leaf pairs
	for (unsigned int iLeaf=0; iLeaf<nLeafs_; iLeaf++) {
	  if (has_bixels_[iLeaf]) {
		nActive_leafs_++;
	  }
	}
}


/**
 * clear aperture
 */
void Aperture::reset()
{
  bixel_vector_.clear_values();
  
  for (unsigned int iLeaf=0; iLeaf<nLeafs_; iLeaf++) {
	right_bixel_[iLeaf] = 0;
	left_bixel_[iLeaf] = 0;
	is_open_[iLeaf] = false;

	for(unsigned int iBixel=0; iBixel<nBixelsinRow_; iBixel++) {
	  aperture_opening_[iLeaf][iBixel] = false;
	}
  }
}



/**
 * create effective bixel vector (does not contain the aperture weight)
 */
void Aperture::set_bixel_vector()
{
  cout << "set bixel vector in aperture" << endl;

	// reset bixel weights
	bixel_vector_.clear_values();
	
	for (unsigned int iLeaf=0; iLeaf<nLeafs_; iLeaf++) {
		if (is_open_[iLeaf]) {
			
			for (unsigned int iX=left_bixel_[iLeaf]; iX<=right_bixel_[iLeaf]; iX++) {
			  unsigned int bixelNo = geometry_->get_bixelNo(Bixel_Subscripts(iX,iLeaf,0),beamNo_);
			  //cout << "iX " << iX << " iLeaf " << iLeaf << " bixelNo " << bixelNo << endl;
			  bixel_vector_.set_intensity(bixelNo,1.0);
			  aperture_opening_[iLeaf][iX] = true;
			}
		}
	}
}


/// add the aperture to a bixel vector
void Aperture::add_aperture_to_bixel_vector(BixelVector & bixelvector, float weight) const
{
  bixelvector.add_scaled_parameter_vector(bixel_vector_,weight);
}


/**
 * calculate aperture weight gradient from the bixel gradient
 */
float Aperture::calculate_aperture_weight_gradient(const BixelVectorDirection & bixel_gradient) const
{
  float tmp = 0;
  for(unsigned int i=0; i<geometry_->get_nBixels(); i++) {
	tmp += bixel_vector_.get_value(i)*bixel_gradient.get_value(i);
  }
  return tmp;
}

/**
 * returns a bixel vector representing this aperture with unit intensity
 */
BixelVector Aperture::get_unit_intensity_bixel_vector() const
{
  return bixel_vector_;
}		  


/**
 * solve pricing problem for the ideal MLC for one leaf pair
 */
float Aperture::solve_pricing_problem(const BixelVectorDirection & bixelgradient)
{

  cout << "solving pricing problem for beam " << beamNo_ << endl;

  // reset aperture
  this->reset();

  float aperture_price = 0;
  
  for (unsigned int iLeaf=0; iLeaf<nLeafs_; iLeaf++) {
	if (has_bixels_[iLeaf]) {
	  
	  //cout << "leafNo: " << iLeaf;

	  float cum_price = 0;
	  float max_price = 0;
	  float opt_price = 0;
	  int opt_r = max_left_bixel_[iLeaf];
	  int opt_l = max_left_bixel_[iLeaf] - 1;
	  
	  //cout << " max right: " << max_right_bixel_[iLeaf] << " max left: " << max_left_bixel_[iLeaf] << endl;
	  
	  int l = max_left_bixel_[iLeaf] - 1; // index to last bixel covered by the left leaf
	  int r = max_left_bixel_[iLeaf];     // index to the first fixel covered by the right leaf
	  
	  while(r <= max_right_bixel_[iLeaf])
		{
		  // get price of this bixel
		  unsigned int bixelNo = geometry_->get_bixelNo(Bixel_Subscripts(r,iLeaf,0),beamNo_);
		  cum_price += bixelgradient.get_value(bixelNo);
		  
		  //cout << bixelgradient.get_value(bixelNo) << " ";

		  if(cum_price >= max_price)
			{
			  max_price = cum_price;
			  l = r;
			}
		  
		  if(cum_price - max_price < opt_price)
			{
			  opt_price = cum_price - max_price;
			  opt_l = l;
			  opt_r = r+1;
			}
		  
		  // increment right leaf
		  r++;
		}

	  //cout << endl << "leafNo: " << iLeaf;
	  //cout << " opt right: " << opt_r << " opt left: " << opt_l << endl;
	  
	  // add leaf pair to overall aperture price
	  aperture_price += opt_price;

	  if(opt_price < 0) { 
		// save optimal leaf positions
		is_open_[iLeaf] = true;
		right_bixel_[iLeaf] = opt_r-1;
		left_bixel_[iLeaf] = opt_l+1;
	  }
	  else {
		// leafs are closed
		is_open_[iLeaf] = false;
		right_bixel_[iLeaf] = 0;
		left_bixel_[iLeaf] = 0;
	  }
	}
  }
  return aperture_price;
}




