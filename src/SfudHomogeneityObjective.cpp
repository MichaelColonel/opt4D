/**
 * @file: SfudHomogeneityObjective.cpp
 * SfudHomogeneityObjective Class implementation.
 *
 */

#include "SfudHomogeneityObjective.hpp"

/**
 * Constructor
 */
SfudHomogeneityObjective::SfudHomogeneityObjective(Voi*  the_voi,
												   unsigned int objNo,
												   const Geometry* geometry,
												   float tolerance,
												   float weight)
: Objective(objNo,weight),
  mean_dose_computer_(),
  the_voi_(the_voi),
  the_geometry_(geometry),
  tolerance_(tolerance),
  beam_bixels_()
{
}

/**
 * Initialize the objective: calculate mean dose contributions of beamlets
 */
void SfudHomogeneityObjective::initialize(DoseInfluenceMatrix& Dij)
{
    // calculate mean dose contributions
	mean_dose_computer_.initialize(the_voi_,Dij);

	// get bixel numbers that belong to a given beam
	for(unsigned int iBeam=0; iBeam<the_geometry_->get_nBeams(); iBeam++) {
	  beam_bixels_.push_back(vector<unsigned int>(0));
	}

	for(unsigned int iBixel=0; iBixel<Dij.get_nBixels(); iBixel++) {
	  unsigned int beamNo = the_geometry_->get_beamNo(iBixel);
	  beam_bixels_[beamNo].push_back(iBixel);
	}

	is_initialized_ = true;
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void SfudHomogeneityObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\t SFUD homogeneity (tolerance " << tolerance_ << ")" << endl;
}


/**
 * calculate objective from dose vector is not possible
 */
double SfudHomogeneityObjective::calculate_dose_objective(const DoseVector & the_dose)
{
  cout << "Warning: SfudHomogeneityObjective::calculate_dose_objective not applicable" << endl;
  return 0;
}


/**
 * calculate objective
 */
double SfudHomogeneityObjective::calculate_objective(const BixelVector & beam_weights,
													 DoseInfluenceMatrix & Dij,
													 bool use_voxel_sampling,
													 double &estimated_ssvo)
{
	// make copy of the weights
	BixelVector beamweights; 
	
	unsigned int nVoxels = the_voi_->get_nVoxels();
	double dVoxels = (double) nVoxels;
	double objective = 0;
	
	// loop over the beam directions
	for(unsigned int iBeam=0; iBeam<the_geometry_->get_nBeams(); iBeam++)
	{
		// extract beam, set beam weights to zero if not part of the current beam
		beamweights = beam_weights;
		beamweights.extract_beam(the_geometry_,iBeam);
		
		// calculate mean dose 
		float meandose = mean_dose_computer_.calculate_mean_dose(beamweights);
		
		float lower_quad = 0;
		float upper_quad = 0;
		
		// calculate objective and gradient factor
		for (unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
			unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
			float dose = Dij.calculate_voxel_dose(voxelNo, beamweights);
			
			if (dose < meandose - tolerance_) {
				
				float tmp = meandose - tolerance_ - dose;
				lower_quad += tmp*tmp;
			}
			
			if (dose > meandose + tolerance_) {
				
				float tmp = meandose + tolerance_ - dose;
				upper_quad += tmp*tmp;
			}
		}

		// add objective contributions over the beam directions
		objective += weight_ * (upper_quad + lower_quad) / dVoxels;
	}
	return objective;
}


/**
 * calculate objective and gradient
 */
double SfudHomogeneityObjective::calculate_objective_and_gradient(const BixelVector & beam_weights,
																  DoseInfluenceMatrix & Dij,
																  BixelVectorDirection & gradient,
																  float gradient_multiplier,
																  bool use_voxel_sampling,
																  double &estimated_ssvo)
{
	// make copy of the weights
	BixelVector beamweights; 
	
	unsigned int nVoxels = the_voi_->get_nVoxels();
	double dVoxels = (double) nVoxels;
	vector<float> dosevector(nVoxels,0);
	double objective = 0;
	
	// loop over the beam directions
	for(unsigned int iBeam=0; iBeam<the_geometry_->get_nBeams(); iBeam++)
	{
		// extract beam, set beam weights to zero if not part of the current beam
		beamweights = beam_weights;
		beamweights.extract_beam(the_geometry_,iBeam);

		// calculate mean dose 
		float meandose = mean_dose_computer_.calculate_mean_dose(beamweights);
		
		//cout << "mean dose " << meandose << endl;

		float lower_abs = 0;
		float lower_quad = 0;
		float upper_abs = 0;
		float upper_quad = 0;
		
		// calculate objective and gradient factor
		for (unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
			unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
			float dose = Dij.calculate_voxel_dose(voxelNo, beamweights);
			dosevector[iVoxel] = dose;
			
			if (dose < meandose - tolerance_) {
				
				float tmp = meandose - tolerance_ - dose;
				lower_abs += tmp;
				lower_quad += tmp*tmp;
			}
			
			if (dose > meandose + tolerance_) {
				
			    float tmp = dose - (meandose + tolerance_);
				upper_abs += tmp;
				upper_quad += tmp*tmp;
			}
		}
		
		//cout << iBeam << " " << lower_abs << " " << upper_abs << endl;

		// loop over the voxels to add up gradient contribution
		for (unsigned int iVoxel=0; iVoxel<nVoxels; iVoxel++) {
			unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
			float dose = dosevector[iVoxel];
			
			// gradient contribution from the mean dose
			float factor = 2*(lower_abs - upper_abs)/dVoxels;
			
			// gradient contribution from the individual voxel
			if (dose < meandose - tolerance_) {
				factor += (-2)*(meandose-tolerance_-dose);
			}
			if (dose > meandose + tolerance_) {
			    factor += 2*(dose-(meandose+tolerance_));
			}
			
			// add gradient contribution
			//Dij.calculate_voxel_dose_gradient(voxelNo,gradient,(weight_/dVoxels)*factor*gradient_multiplier);
			Dij.calculate_voxel_dose_gradient(voxelNo,gradient,
											  factor*gradient_multiplier*(weight_/dVoxels),
											  0, beam_bixels_[iBeam]);
		}
		
		// add objective contributions over the beam directions
		objective += weight_ * (upper_quad + lower_quad) / dVoxels;
	}
	return objective;
}

