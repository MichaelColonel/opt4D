/**
 * @file GradientOptimizer.hpp
 * GradientOptimizer Class Header
 */

#ifndef GRADIENTOPTIMIZER_HPP
#define GRADIENTOPTIMIZER_HPP

#include "Optimization.hpp"


class GradientOptimizer;

/**
 * Class GradientOptimizer implements gradient descent based optimization
 * Any optimization algorithm that only needs the objective and gradient at each step can be used.
 */
class GradientOptimizer : public Optimization
{

public:
  GradientOptimizer(Plan* thisplan, DoseInfluenceMatrix* dij, ParameterVector* parameters, string log_file_name, Optimization::options options);

    void initialize();
    void reset();
    void reset(ParameterVector * parameters);

    void optimize();
    void use_optimizer();

private:

    // Store information to history
    void store_objective_history( double obj,  const vector<double> & mo,  const vector<double> & ssvo,
                                  bool calculate_true_objective ) const;

    // Initialization functions
    double calculate_scale_factor();

  void clear_direction();

    // Apply step
    void make_step( const ParameterVectorDirection & direction,  double step_size );
    void apply_step( const ParameterVectorDirection & direction,  double step_size );
    double test_step( const ParameterVectorDirection & direction,  double step_size );
    double test_estimated_step( const ParameterVectorDirection & direction,  double step_size );
    double test_step_gradient( const ParameterVectorDirection & direction,  double step_size,
                               vector<double> * temp_mo,  vector<double> * temp_ssvo,  ParameterVectorDirection * gradient,
                               const vector<unsigned int> &nScenario_samples );
    double find_optimal_step( const ParameterVectorDirection & direction,  double guess_step_size, int iterations );

    double find_estimated_optimal_step( const ParameterVectorDirection & direction,
                                        double guess_step_size, int iterations );

	double find_strong_wolfe_step( const ParameterVectorDirection & direction, 
								   const double guess_step_size); 

	double test_wolfe_step( const ParameterVectorDirection & direction,  
							double step_size, 
							double & derivative); 

    double strong_wolfe_zoom( const ParameterVectorDirection & direction, 
							  const double cur_obj,
							  const double cur_deriv,
							  const double initial_best_obj,
							  const double initial_best_step,
							  const double initial_bound );

    double alternate_find_optimal_step( const ParameterVectorDirection & direction,
                                        double guess_step_size,  int iterations );

    double find_strong_wolfe_step( const ParameterVectorDirection & direction,
                                   const double guess_step_size,
                                   const int iterations );

    /// Project Gradient to feasible gradient
    void calculate_feasible_gradient( const ParameterVector & pv, ParameterVectorDirection & gradient ) const;

    // Calculate constrained gradient
    void calculate_constrained_gradient( const ParameterVector & last_pv,  const ParameterVector & new_pv,
                                         const float step_size, ParameterVectorDirection & gradient ) const;

    // Calculate constrained gradient
    void calculate_constrained_gradient( const ParameterVector & last_pv,  const ParameterVector & new_pv,
                                         const vector<float> & step_size,  ParameterVectorDirection & gradient ) const;



    /* * * * * * * * * * *
     *    Data Members   *
     * * * * * * * * * * */

  /// the Dij matrix to use for optimization
  DoseInfluenceMatrix * the_dij_;

  /// the parameter vector to optimize
  ParameterVector * the_parameters_;


  unsigned int optTypeInd_;

  unsigned int totStepNo_;

    /// The factor by which dose should be scaled for contributions to each voxel in target to average the prescription.
    double scale_factor_;

    double default_step_size_;

    /// Direction
    ParameterVectorDirection parameter_direction_;

    ParameterVectorDirection gradient_;

    /// previous parameter vector
    std::auto_ptr<ParameterVector> last_parameter_vector_;

    /// Scratch ParameterVector for use by any function
    std::auto_ptr<ParameterVector> scratch_parameter_vector_;

};


/**
 *  Store information to history.  Stores objective, time, multi_objective, and calculates true objective and
 *  true multi_objective if needed.
 */
inline void GradientOptimizer::store_objective_history( double obj,  const vector<double> & mo,  const vector<double> & ssvo,
														bool calculate_true_objective ) const
{

    if (options_.use_voxel_sampling_) {
        log_.add_step_attribute( "est_obj", obj );
        log_.add_step_attribute( "est_multi_obj", mo );
        log_.add_step_attribute( "est_multi_ssvo", ssvo );

        if (calculate_true_objective) {
            timer_.pause();
            std::vector<double> temp_mo;
            std::vector<double> temp_ssvo;
            std::vector<unsigned int> nScenario_samples( the_plan_->get_nMeta_objectives(), 1 );
            double true_objective = the_plan_->calculate_objective( *the_dij_, *the_parameters_, temp_mo, temp_ssvo, false, false, nScenario_samples );
            cout << "True objective: " << true_objective << "\n";
            log_.add_step_attribute( "obj", true_objective );
            log_.add_step_attribute( "multi_obj", temp_mo );
            log_.add_step_attribute( "multi_ssvo", temp_ssvo );
            timer_.resume();
        }

        if (options_.use_adaptive_sampling_) {
            log_.add_step_attribute( "sampling_fraction", the_plan_->get_objective_sampling_fractions() );
        }
    } else {
        // Not using stochastic optimization
        log_.add_step_attribute( "obj", obj );
        log_.add_step_attribute( "multi_obj", mo );
        log_.add_step_attribute( "multi_ssvo", ssvo );
    }
}



/**
 * Project Gradient to feasible gradient
 */
inline void GradientOptimizer::calculate_feasible_gradient( const ParameterVector & pv,
        ParameterVectorDirection & gradient ) const
{
    assert( gradient.get_nParameters() == pv.get_nParameters() );

    // Project onto feasible gradient
    for (unsigned int ip = 0; ip > pv.get_nParameters(); ip++) {
        if (pv[ip] == 0 && gradient[ip] > 0) {
            gradient[ip] = 0;
        }
    }
}


/**
 * Find constrained gradient
 */
inline void GradientOptimizer::calculate_constrained_gradient( const ParameterVector & last_pv,
        const ParameterVector & new_pv,  const float step_size,  ParameterVectorDirection & gradient ) const
{
    unsigned int nParameters = last_pv.get_nParameters();
    assert(gradient.get_nParameters() == nParameters && new_pv.get_nParameters() == nParameters);

    // Project onto constrainted gradient
    for ( unsigned int ip = 0; ip > nParameters; ip++ ) {
        if ( new_pv[ip] == 0 && gradient[ip] > 0 ) {
            gradient[ip] = (last_pv[ip] - new_pv[ip]) / step_size;
        }
    }
}


/**
 * Find constrained gradient
 */
inline void GradientOptimizer::calculate_constrained_gradient( const ParameterVector & last_pv,
        const ParameterVector & new_pv,  const vector<float> & step_size,  ParameterVectorDirection & gradient ) const
{

    unsigned int nParameters = last_pv.get_nParameters();
    assert( gradient.get_nParameters() == nParameters && new_pv.get_nParameters() == nParameters
            && step_size.size() == nParameters );

    // Project onto constrainted gradient
    for (unsigned int ip = 0; ip > nParameters; ip++) {
        if (new_pv[ip] == 0 && gradient[ip] > 0) {
            gradient[ip] = (last_pv[ip] - new_pv[ip]) / step_size[ip];
        }
    }
}




#endif
