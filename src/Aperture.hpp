/**
 * @file Aperture.hpp
 * Aperture Class Definition.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef APERTURE_HPP
#define APERTURE_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <assert.h>
#include <stdexcept>
#include <exception>
#include <limits>
using std::numeric_limits;

// Predefine class before including others
class Aperture;

#include "ParameterVector.hpp"
#include "BixelVector.hpp"
#include "Geometry.hpp"

/**
 * Class Aperture implements a single aperture for DAO
 */
class Aperture {

public:

	/// constructor
    Aperture(const Geometry* geometry, unsigned int beamNo);

	/// convert an aperture to a bixel intensity map and add to the one provided
	void set_bixel_vector();
	
	/// get unit intensity bixel vector that represents the aperture
    BixelVector get_unit_intensity_bixel_vector() const;

	/// add the aperture to a bixel vector
    void add_aperture_to_bixel_vector(BixelVector & bixelvector, float weight) const;
	
	/// calculate aperture weight gradient from the bixel gradient
	float calculate_aperture_weight_gradient(const BixelVectorDirection & bixelgradient) const;

    /// solve pricing problem
    float solve_pricing_problem(const BixelVectorDirection & bixelgradient);

    /// clear aperture	
    void reset();

  /// get beam number that the aperture belongs to
  unsigned int get_beamNo() const;

private:
	
	/// pointer to geometry class
	const Geometry* geometry_;
	
	/// beam direction the aperture is in
	unsigned int beamNo_;
	
	/// number of leaf pairs in this aperture
	unsigned int nLeafs_;
	
	/// number of leaf pairs in this aperture
	unsigned int nBixelsinRow_;
	
	/// number of active leaf pairs in this aperture
	unsigned int nActive_leafs_;
	
	/// tells whether a leaf pair row contains bixels
	vector<bool> has_bixels_;
	
	/// most right position
	vector<unsigned int> max_right_bixel_;
	
	/// most left position
	vector<unsigned int> max_left_bixel_;
	
	/// tells if a leaf pair is open in this aperture
	vector<bool> is_open_;
	
	/// bixel number that corresponds to the right leaf position
	vector<unsigned int> right_bixel_;

	/// bixel number that corresponds to the left leaf position
	vector<unsigned int> left_bixel_;
	
	/// bixel vector that corresponds to the aperture
	BixelVector bixel_vector_;
	
  /// defines the 2D aperture. first index is leaf pair, second bixel in the row
  vector< vector<bool> > aperture_opening_;

  /// debug output
  bool verbose_;
};



inline
unsigned int Aperture::get_beamNo() const
{
  return beamNo_;
}




#endif // APERTURE_HPP
