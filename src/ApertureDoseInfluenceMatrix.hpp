/**
 * @file ApertureDoseInfluenceMatrix.hpp
 * ApertureDoseInfluenceMatrix Class Definition.
 */

#ifndef APERTUREDOSEINFLUENCEMATRIX_HPP
#define APERTUREDOSEINFLUENCEMATRIX_HPP



// Predefine class before including others
class ApertureDoseInfluenceMatrix;


#include "DoseInfluenceMatrix.hpp"



/**
 * Class ApertureDoseInfluenceMatrix implements a DoseInfluenceMatrix that holds the dose contribution of apertures instead of bixels
 */
class ApertureDoseInfluenceMatrix : public DoseInfluenceMatrix {

  public:

  /// constructor: creates empty matrix
  ApertureDoseInfluenceMatrix();

  /// initialize: reserves memory
  void initialize(unsigned int nVoxels, unsigned int nEntries);

  /// add an aperture to the matrix
  void add_aperture(unsigned int apNo, vector<unsigned int> voxelNos, vector<float> influence);
  
};


#endif



