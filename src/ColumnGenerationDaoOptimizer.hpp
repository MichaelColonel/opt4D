/**
 * @file ColumnGenerationDaoOptimizer.hpp
 * ColumnGenerationDaoOptimizer Class Header
 */

#ifndef COLUMNGENERATIONDAOOPTIMIZER_HPP
#define COLUMNGENERATIONDAOOPTIMIZER_HPP

class ColumnGenerationDaoOptimizer;


#include "Plan.hpp"
#include "Optimization.hpp"
#include "DaoVector.hpp"





/**
 * Direct aperture optimization using the column generation approach
 */
class ColumnGenerationDaoOptimizer : public Optimization
{

public:

  /// destructor
  ColumnGenerationDaoOptimizer(Plan* thisplan, string log_file_name, Optimization::options options);

  /// constructor
  ~ColumnGenerationDaoOptimizer();

  /// optimize plan
  void optimize();

  /// initialize
  void initialize();

private:

  /// get aperture weights from the dao vector
  void set_aperture_weights();

  /// Optimizer for the master problem (i.e. aperture weight optimization)
  std::auto_ptr<Optimization> master_problem_optimizer_;

  /// pointer to the plan's DaoVector
  DaoVector * dao_vector_; 

  // aperture weights
  std::auto_ptr<ParameterVector> aperture_weights_;

};


#endif



