/**
 * @file ConstrainedObjective.hpp
 * ConstrainedObjective Class header
 *
 */

#ifndef CONSTRAINEDOBJECTIVE_HPP
#define CONSTRAINEDOBJECTIVE_HPP

#include <iostream>
#include <cmath>
#include <map>

#include "BixelVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "Objective.hpp"
#include "Constraint.hpp"

/**
 * Class ConstrainedObjective implements a class to set a hard constrained on the maximum value of an objective
 *
 */
class ConstrainedObjective : public Constraint 
{

public:
	
/// constructor
	ConstrainedObjective(unsigned int consNo,
						 Objective* obj,
						 float bound,
						 float initial_penalty);

  /// destructor, resposible for deleting the associated objective
  ~ConstrainedObjective();
  
  
  /// Print a description of the constraint
  void printOn(std::ostream& o) const;



	//
	// specific functions for augmented lagrangian approach
	//
	
	/// augmented lagrangian is supported if the objective supports gradient
	bool supports_augmented_lagrangian() const;
	
	/// calculate the augmented lagrangian function
	double calculate_aug_lagrangian(const BixelVector & beam_weights,
									DoseInfluenceMatrix & Dij,
									double & constraint,
									double & merit);
	
	/// calculate the augmented lagrangian function and its gradient
	double calculate_aug_lagrangian_and_gradient(const BixelVector & beam_weights,
												 DoseInfluenceMatrix & Dij,
												 BixelVectorDirection & gradient,
												 float gradient_multiplier);
	
	/// update lagrange parameter
	void update_lagrange_multipliers(const BixelVector & beam_weights,
									 DoseInfluenceMatrix & Dij);
	
	/// update penalty parameter
	void update_penalty(const BixelVector & beam_weights,
						DoseInfluenceMatrix & Dij, 
						float tol, 
						float multiplier);
	

private:

  // Default constructor not allowed
  ConstrainedObjective();
	
	/// associated objective
	Objective* the_obj_;
	
	/// bound for the objective value
	float bound_;
	
	/// penalty factor
	float penalty_;
	
	/// initial penalty factor
	float initial_penalty_;
	
	/// lagrange multiplier
	float lagrange_;
	
};


 

#endif
