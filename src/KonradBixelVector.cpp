/* $Id: KonradBixelVector.cpp,v 1.19 2007/09/26 13:22:03 unkelbac Exp $ */

#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <exception>

#include "KonradBixelVector.hpp"


/**
 * Class bwf_exception is thrown when there is an errer reading a bwf file.
 */
class bwf_exception: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Failed while reading bwf file.";
    }
} bwf_ex;


/**
 * Class stf_exception is thrown when there is an errer reading a stf file.
 */
class stf_exception: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Failed while reading stf file.";
    }
} stf_ex;


/**
 * @file KonradBixelVector.cpp
 * KonradBixelVector functions
 */

KonradBixelVector::~KonradBixelVector()
{
}

/**
 * Constructor to read beam info from KonRad steering and beam weight files.
 *
 * @param geometry The Geometry object to store the geometry info in.  It will modify the contents of this object.
 * @param nInstances The number of instances of the geometry to read
 * @param nBeams_per_instance The number of beams per instance
 * @param bwf_file_root The root of the beam weight file names.  If <code>bwf_file_root</code> is "default", the files
 * default_1.bwf, default_2.bwf, default_3.bwf, and so on will be read.
 * @param stf_file_root (optional) The root of the beam steering file names. If <code>stf_file_root</code> is "default",
 * the files default_1.stf, default_2.stf, default_3.stf, and so on will be read.
 */
KonradBixelVector::KonradBixelVector( Geometry *geometry,  const unsigned int nInstances,
        const unsigned int nBeams_per_instance, const string bwf_file_root,  const string stf_file_root )
        : BixelVector(),  nInstances_(nInstances),  nBeams_(nBeams_per_instance * nInstances),
        nBeams_in_instance_(nInstances, nBeams_per_instance),  starting_beamNo_(nInstances, 0),
        nBixels_in_beam_(nBeams_, 0), starting_bixelNo_(nBeams_, 0), geometry_(geometry)
{
    int iValue;
    string bwf_file_name;
    string stf_file_name;
    char cbeam[10];
    char cinstance[10];
    std::ifstream infile;
    string dummy_str;

    // Allocate vectors to hold information about the beams
    vector<float> gantry_angle(nBeams_,0);
    vector<float> table_angle(nBeams_,0);
    vector<float> collimator_angle(nBeams_,0);
    vector<float> beam_grid_dx(nBeams_,0);
    vector<float> beam_grid_dy(nBeams_,0);
    vector<float> beam_SAD(nBeams_,0);

    // Fill starting_beamNo_ vector
    for(unsigned int iInstance = 1; iInstance < nInstances_; iInstance++) {
        starting_beamNo_[iInstance] = starting_beamNo_[iInstance-1] + nBeams_in_instance_[iInstance-1];
    }

    // Pre-read the files to find out how many bixels are in each beam
    int total_nJ = 0;
    for(unsigned int iInstance = 0; iInstance < nInstances_; iInstance ++) {
        for(unsigned int iBeam = starting_beamNo_[iInstance];
                iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance];  iBeam++) {

            // convert beam# to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
            if(iInstance == 0) {
                bwf_file_name = bwf_file_root + "_" + string(cbeam) + ".bwf";
            } else {
                bwf_file_name = bwf_file_root + "_" + string(cinstance) + "_" + string(cbeam) + ".bwf";
            }

            // Open Beam-weight file
            infile.clear();
            infile.open( bwf_file_name.c_str() );
            if (!infile.is_open()) {
                cout << "Can't open file: " << bwf_file_name << endl;
                throw(bwf_ex);
            }

            char line_buffer[128];

            // Header
            // read beam number
            infile.getline(line_buffer, 128, '\n');
            sscanf(line_buffer, "%*s %*s %d", &iValue);

            if(iValue != (int) (iBeam - starting_beamNo_[iInstance])) {
                std::cerr << "Expected beamNo " << iBeam - starting_beamNo_[iInstance]
                            << " found " << iValue << " in file: " << bwf_file_name << endl;
                throw(bwf_ex);
            }

            // read number of beamlets:
            infile.getline(line_buffer, 128, '\n');

            int temp_int = 0;
            sscanf(line_buffer, "%*s %*s %*s %d", &temp_int);
            nBixels_in_beam_[iBeam] = temp_int;

            infile.close();

            total_nJ += nBixels_in_beam_[iBeam];
        }
    }

    // Calculate starting bixel number for each beam
    for(unsigned int iBeam = 0; iBeam < nBeams_; iBeam++) {
        if(iBeam == 0) {
            starting_bixelNo_[iBeam] = 0;
        } else {
            starting_bixelNo_[iBeam] = nBixels_in_beam_[iBeam-1] + starting_bixelNo_[iBeam-1];
        }
    }

    // Reserve enough space for all beamlets
    this->reserve_nParams(total_nJ);

    // Allocate vectors to hold information about the bixels
    vector<float> bixel_xpos(total_nJ);
    vector<float> bixel_ypos(total_nJ);
    vector<Bixel_Position> bixel_position(total_nJ);
    vector<unsigned int> bixel_beamNo(total_nJ);

    // Read bixel info from bwf and stf files
    for(unsigned int iInstance = 0; iInstance < nInstances_; iInstance ++) {
        for(unsigned int iBeam = starting_beamNo_[iInstance];
                iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance];  iBeam++) {

            // convert beam# to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
            if(iInstance == 0) {
                bwf_file_name = bwf_file_root + "_" + string(cbeam) + ".bwf";
            } else {
                bwf_file_name = bwf_file_root + "_" + string(cinstance) + "_" + string(cbeam) + ".bwf";
            }

            // Open Beam-weight file
            infile.clear();
            infile.open(bwf_file_name.c_str());
            if (!infile.is_open()) {
                cout << "Can't open file: " << bwf_file_name << endl;
                throw(bwf_ex);
            }

            char line_buffer[128];

            // Header
            // read beam number
            infile.getline(line_buffer, 128, '\n');
            sscanf(line_buffer, "%*s %*s %d", &iValue);

            // read number of beamlets:
            infile.getline(line_buffer, 128, '\n');
            int temp_int = 0;
            sscanf(line_buffer, "%*s %*s %*s %d", &temp_int);
            nBixels_in_beam_[iBeam] = temp_int;

            // Ignore the next line
            infile.ignore(256, '\n');

            // Now read the beamlet data
            for(unsigned int i = 0; i < nBixels_in_beam_[iBeam] && infile.getline(line_buffer, 128, '\n'); i++) {
                int db, np, used, cFree, index;
                float weight, ssd, x, y;

                sscanf(line_buffer, "%d %f %f %d %d %d %d %f %f", &db, &weight, &ssd, &np, &used, &cFree, &index,&x,&y);

                bool temp_aperture = false;

                this->push_back_parameter(weight, temp_aperture);

                bixel_xpos[i + starting_bixelNo_[iBeam]] = x;
                bixel_ypos[i + starting_bixelNo_[iBeam]] = y;
                bixel_beamNo[i + starting_bixelNo_[iBeam]] = iBeam;
            }

            // Close bwf file
            infile.close();

            if(stf_file_root.size() > 0) {
                // Open beam steering file
                sprintf(cinstance,"%d",(int)iInstance+1);
                sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
                if(iInstance == 0) {
                    stf_file_name = stf_file_root + "_" + string(cbeam) + ".stf";
                } else {
                    stf_file_name = stf_file_root + "_" + string(cinstance) + "_" + string(cbeam) + ".stf";
                }

                // Open Beam-steering file
                infile.clear();
                infile.open(stf_file_name.c_str());
                if (!infile.is_open()) {
                    cout << "Can't open file: " << stf_file_name << endl;
                    throw(stf_ex);
                }

                cout << "Reading beam steering file: " << stf_file_name << endl;

                // Read stf file header
                while(infile.peek() == '#') {
                    infile.ignore(1);
                    infile >> std::ws;

                    string attribute;
                    infile >> attribute;
                    if(attribute == "Header" || attribute == "Machinename:") {
                        infile.ignore(256, '\n');
                    } else if(attribute == "Gantry-Angle:") {
                        infile >> gantry_angle[iBeam];
                    } else if(attribute == "Table-Angle:") {
                        infile >> table_angle[iBeam];
                    } else if(attribute == "Colli-Angle:") {
                        infile >> collimator_angle[iBeam];
                    } else if(attribute == "Delta") {
                        char line_buffer[128];
                        infile.getline(line_buffer, 128, '\n');
                        sscanf(line_buffer, "%*s %f %*s %*s %f", &beam_grid_dx[iBeam], &beam_grid_dy[iBeam]);
                    }
                    infile >> std::ws;
                }

                // Read stf file body
                char line_buffer[128];
                for( unsigned int i = 0; i < nBixels_in_beam_[iBeam] && infile.good(); i++ ) {
                    infile.getline(line_buffer, 128, '\n');
                    float temp_E(0), temp_x(0), temp_y(0);
                    int n;
                    sscanf(line_buffer, "%d %f %*s %*s %*s %f %f", &n, &temp_E, &temp_x, &temp_y);
                    bixel_position[i + starting_bixelNo_[iBeam]] =
                    Bixel_Position( bixel_xpos[i + starting_bixelNo_[iBeam]], bixel_ypos[i + starting_bixelNo_[iBeam]],
                        temp_E );
                }
                infile.close();
            } else {
                cout << "Warning: Not reading stf file. Beamlet energy will be wrong." << endl;
                for (unsigned int i=0; i<nBixels_in_beam_[iBeam]; i++) {
                    bixel_position[i + starting_bixelNo_[iBeam]] = Bixel_Position(
                        bixel_xpos[i + starting_bixelNo_[iBeam]], bixel_ypos[i + starting_bixelNo_[iBeam]], 0 );
                }
            }

            beam_SAD[iBeam] = 10.E4;  // mm

            cout << "Read beam " << iBeam+1 << " with " << nBixels_in_beam_[iBeam] << " beam elements" << endl;
        }
    }

    // Store information about beams
    geometry_->set_nBeams(nBeams_);
    geometry_->set_gantry_angle(gantry_angle);
    geometry_->set_table_angle(table_angle);
    geometry_->set_collimator_angle(collimator_angle);
    geometry_->set_bixel_grid_size(beam_grid_dx, beam_grid_dy);
    geometry_->set_beam_SAD(beam_SAD);
    geometry_->set_beam_starting_bixelNo(starting_bixelNo_);
    geometry_->set_nBixels_in_beam(nBixels_in_beam_);

    // Store information about the bixels
    geometry_->set_nBixels(total_nJ);
    geometry_->set_bixel_position(bixel_position);
    geometry_->set_bixel_beamNo(bixel_beamNo);

    geometry_->initialize_bixel_grid();
    geometry_->set_isocenters();
    geometry_->set_initialized(true);

}


/**
 * Constructor to read beam info from KonradDoseInfluenceMatrix class.
 *
 * @param Dij The class to pull attributes from.
 */
KonradBixelVector::KonradBixelVector( const DoseInfluenceMatrix & Dij,  Geometry *geometry,
        const unsigned int nInstances, const unsigned int nBeams_per_instance)
        : BixelVector(Dij.get_nBixels()),  nInstances_(nInstances),  nBeams_(nBeams_per_instance * nInstances) ,
        nBeams_in_instance_(nInstances, nBeams_per_instance),  starting_beamNo_(nInstances_,0),
        nBixels_in_beam_(geometry->get_nBixels_in_beam()),  starting_bixelNo_(geometry->get_beam_starting_bixelNo()),
        geometry_(geometry) {

    // Fill starting_beamNo_ vector
    for( unsigned int iInstance = 1; iInstance < nInstances_; iInstance++ ) {
        starting_beamNo_[iInstance] = starting_beamNo_[iInstance-1] + nBeams_in_instance_[iInstance-1];
    }
}


/**
 * Copy Constructor
 *
 * @param kbv   A KonradBixelVector from which to copy the object data
 */
KonradBixelVector::KonradBixelVector( const KonradBixelVector &kbv )
        : BixelVector(kbv), nBeams_in_instance_(kbv.nBeams_in_instance_), starting_beamNo_(kbv.starting_beamNo_),
        nBixels_in_beam_(kbv.nBixels_in_beam_), starting_bixelNo_(kbv.starting_bixelNo_), geometry_(kbv.geometry_),
        nBeams_(kbv.nBeams_), nInstances_(kbv.nInstances_)
{}


/**
 * read a steering file containing bixels inside the aperture
 */
void KonradBixelVector::load_bixels_in_aperture( const string & stf_file_root )
{
    cout << "set bixels in aperture ..." << endl;

    float epsilon = 0.001;

    string stf_file_name;
    char cbeam[10];
    char cinstance[10];
    std::ifstream infile;

    // Fill starting_beamNo_ vector
    for(unsigned int iInstance = 1; iInstance < nInstances_; iInstance++) {
        starting_beamNo_[iInstance] = starting_beamNo_[iInstance-1] + nBeams_in_instance_[iInstance-1];
    }

    for( unsigned int iInstance = 0; iInstance < nInstances_; iInstance++ ) {
        for( unsigned int iBeam = starting_beamNo_[iInstance];
                iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance]; iBeam++ ) {

            // convert beamNo to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));

            if(iInstance == 0) {
                stf_file_name = stf_file_root + "_" + string(cbeam) + ".stf";
            } else {
                stf_file_name = stf_file_root + "_" + string(cinstance) + "_" + string(cbeam) + ".stf";
            }

            // Open steering file
            infile.clear();
            infile.open(stf_file_name.c_str());
            if (!infile.is_open()) {
                cout << "Can't open file: " << stf_file_name << endl;
                throw(bwf_ex);
            }

            cout << "Reading beam steering file: " << stf_file_name << endl;

            // Read stf file header
            while(infile.peek() == '#') {
                infile.ignore(1);
                infile >> std::ws;

                string attribute;
                float fDummy;
                infile >> attribute;
                if(attribute == "Header" || attribute == "Machinename:") {
                    infile.ignore(256, '\n');
                } else if(attribute == "Gantry-Angle:") {
                    infile >> fDummy;
                    if(!comp_float_abs(fDummy,geometry_->get_gantry_angle()[iBeam],epsilon)) {
                    cout << "gantry angle " << fDummy << " in " << stf_file_name
                         << ". Expected " << geometry_->get_gantry_angle()[iBeam] << endl;
                    throw(bwf_ex);
                    }
                } else if(attribute == "Table-Angle:") {
                    infile >> fDummy;
                    if(!comp_float_abs(fDummy,geometry_->get_table_angle()[iBeam],epsilon)) {
                    cout << "table angle " << fDummy << " in " << stf_file_name
                         << ". Expected " << geometry_->get_table_angle()[iBeam] << endl;
                    throw(bwf_ex);
                    }
                } else if(attribute == "Colli-Angle:") {
                    infile >> fDummy;
                    if(!comp_float_abs(fDummy,geometry_->get_collimator_angle()[iBeam],epsilon)) {
                    cout << "colli angle " << fDummy << " in " << stf_file_name
                         << ". Expected " << geometry_->get_collimator_angle()[iBeam] << endl;
                    throw(bwf_ex);
                    }
                } else if(attribute == "Delta") {
                    char line_buffer[128];
                    float dx, dy;
                    infile.getline(line_buffer, 128, '\n');
                    sscanf(line_buffer, "%*s %f %*s %*s %f", &dx, &dy);
                    if( !comp_float_abs(dx,geometry_->get_bixel_grid_dx()[iBeam],epsilon) ||
                            !comp_float_abs(dy,geometry_->get_bixel_grid_dy()[iBeam],epsilon) ) {
                        cout << "beamlet resolution (dx=" << dx << ", dy=" << dy << ") in " << stf_file_name
                             << ". Expected (dx=" << geometry_->get_bixel_grid_dx()[iBeam]
                             << ", dy=" << geometry_->get_bixel_grid_dy()[iBeam] << ")" << endl;
                    }

                }
                infile >> std::ws;
            }

            // Read stf file body
            char line_buffer[128];
            int n;
            int count = 0;
            while(infile.good()) {
                // get next line
                infile.getline(line_buffer, 128, '\n');

                bool found_it = false;
                float E(0), x(0), y(0);
                sscanf(line_buffer, "%d %f %*s %*s %*s %f %f", &n, &E, &x, &y);

                // find this beamlet in the Dij based bixel vector
                for(unsigned int iBixel = starting_bixelNo_[iBeam];
                    iBixel < starting_bixelNo_[iBeam] + nBixels_in_beam_[iBeam];
                    iBixel++) {

                    if(comp_float_abs(x, geometry_->get_bixel_position_x(iBixel), epsilon) &&
                            comp_float_abs(y, geometry_->get_bixel_position_y(iBixel), epsilon) &&
                            comp_float_abs(E, geometry_->get_bixel_energy(iBixel), epsilon) ) {

                        // tag this beamlet as being within the aperture
                        set_isActive(iBixel, true);
                        found_it = true;
                        count++;
                        break;
                    }
                }

                if(!found_it) {
                    cout << "WARNING: Beamlet " << n << " with energy " << E
                     << " and position (x=" << x << ", y=" << y
                     << ") was not found in Dij matrix" << endl;
                }
            }

            infile.close();

            cout << "bixels in aperture: " << count << endl;
            cout << "bixels in " << stf_file_name << ": " << n+1 << endl;
            cout << "bixels in Dij matrix: " << nBixels_in_beam_[iBeam] << endl;
        }
    }

    // copy information to geometry class
    for( unsigned int ip = 0; ip < get_nParameters(); ip++ ) {
        if( get_isActive(ip) ) {
            geometry_->set_physical_bixel( ip, true );
        } else {
            geometry_->set_physical_bixel( ip, false );
        }
    }
}

/**
 * Load intensities from a file
 */
void KonradBixelVector::load_bwf_files( const string & bwf_file_root)
{
    char cbeam[10];
    char cinstance[10];
    string bwf_file_name;

    for( unsigned int iInstance = 0; iInstance < nInstances_; iInstance ++ ) {
        for( unsigned int iBeam = starting_beamNo_[iInstance];
                iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance]; iBeam++ ) {
            // convert beam# to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
            if(iInstance == 0) {
                bwf_file_name = bwf_file_root + "_" + string(cbeam) + ".bwf";
            } else {
                bwf_file_name = bwf_file_root + "_" + string(cinstance) + "_" + string(cbeam) + ".bwf";
            }

            load_bwf_file(bwf_file_name, iBeam);
        }
    }
}


/**
 * Load intensities from a file
 */
void KonradBixelVector::load_bwf_file( const string & bwf_file_name,  unsigned int beamNo )
{
    unsigned int temp_int;
    char aLine[100];
    string DataFile;
    FILE *fInput;

    // Open Beam-weight file
    fInput = fopen(bwf_file_name.c_str(),"r");
    if (fInput == NULL) {
        cout << "Can't open file: " << bwf_file_name << endl;
        exit(-1);
    }
    cout << "Reading beam data from file: " << bwf_file_name << endl;

    // Header
    // read beam number
    fscanf(fInput,"%*s %*s %d",&temp_int);

    // read number of beamlets:
    fscanf(fInput,"%*s %*s %*s %d\n",&temp_int);
    if(temp_int != nBixels_in_beam_[beamNo]) {
        std::cerr << "Wrong number of beamlets in file " << bwf_file_name << endl;
        exit(-1);
    }

    // Throw away the next line
    fgets(aLine,100,fInput);

    // cout << "Finished reading header. There are " << Nj_ << " beams" << endl;

    // Now read the beamlet data
    for ( unsigned int ip=starting_bixelNo_[beamNo]; ip<starting_bixelNo_[beamNo] + nBixels_in_beam_[beamNo]; ip++ ) {
        float value(0), xpos(0), ypos(0);
        fscanf( fInput, "%*s %f %*s %*s %*s %*s %*s %f %f", &value, &xpos, &ypos );
        set_value( ip, value );
        set_isActive( ip, true );
        set_lowBound( ip, 0.0f );
        set_highBound( ip, std::numeric_limits<float>::infinity() );
        set_step_size(ip, 0.0f);

        // Optionally here you can check that the bixels are in the right place,
        // but this is currently ommitted
    }

    fclose(fInput);
}


/**
 * Write intensities to bwf files
 */
void KonradBixelVector::write_bwf_files(const string & bwf_file_root) const
{
    cout << "Writing files " << bwf_file_root << endl;

    char cbeam[10];
    char cinstance[10];
    string bwf_file_name;

    for( unsigned int iInstance = 0; iInstance < nInstances_; iInstance ++ ) {
        for( unsigned int iBeam = starting_beamNo_[iInstance];
                iBeam < starting_beamNo_[iInstance] + nBeams_in_instance_[iInstance]; iBeam++ ) {
            // convert beam# to string
            sprintf(cinstance,"%d",(int)iInstance+1);
            sprintf(cbeam,"%d",(int)(iBeam+1-starting_beamNo_[iInstance]));
            if(iInstance == 0) {
                bwf_file_name = bwf_file_root + "_" + string(cbeam) + ".bwf";
            } else {
                bwf_file_name = bwf_file_root + "_" + string(cinstance) + "_" + string(cbeam) + ".bwf";
            }

            write_bwf_file(bwf_file_name, iBeam);
        }
    }

    /*
    cout << "Writing files " << bwf_file_root << endl;
    char cbeam[10];
    string bwf_file_name;
    for(unsigned int iBeam = 0; iBeam < nBeams_; ++iBeam) {
        // convert beam# to string
        sprintf(cbeam,"%d",(int)iBeam+1);
        bwf_file_name = bwf_file_root + "_" + string(cbeam) + ".bwf";
        write_bwf_file(bwf_file_name, iBeam);
    }
    */
}


/**
 * Write beam weights as a .bwf formatted file
 */
void KonradBixelVector::write_bwf_file( const string & bwf_file_name,  const unsigned int beamNo ) const
{
    /* NEW
    cout << " beamNo = "<< beamNo << "\t starting_bixelNo_[beamNo] = "<< starting_bixelNo_[beamNo] << endl;
    if ((starting_bixelNo_[beamNo]+nBixels_in_beam_[beamNo]) > geometry_->get_nBixels())
    {
      cout << "not writing out "<< bwf_file_name <<"  , since \n " << starting_bixelNo_[beamNo] << " + "
            << nBixels_in_beam_[beamNo] <<" > "<< geometry_->get_nBixels() <<"\n";
      return;
    }
    */
    std::ofstream ofile(bwf_file_name.c_str());

    // Header
    ofile << "#Beam Number: " << beamNo << "\n";
    ofile << "#Number of DBs: " << nBixels_in_beam_[beamNo] << "\n";
    ofile << "#DB Weight SSD NP Used cFree index ray-x y \n";
    ofile << setiosflags(std::ios::fixed | std::ios:: showpoint);

    // Data
    for(unsigned int j=0; j<nBixels_in_beam_[beamNo]; j++) {
        unsigned int bixelNo = starting_bixelNo_[beamNo] + j;
        Bixel_Position temp_pos = geometry_->get_bixel_position(bixelNo);
        ofile << j << " " << get_value(bixelNo) << " 0.000000 0 0 0 0 " <<  temp_pos.x_ << " " <<  temp_pos.y_ << "\n";
    }

    ofile.close();
}

