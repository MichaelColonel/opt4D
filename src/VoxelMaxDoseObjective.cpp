/**
 * @file: MeanSquaredErrorObjective.cpp
 * MeanSquaredErrorObjective Class implementation.
 *
 * $Id: MeanSquaredErrorObjective.cpp,v 1.31 2008/03/13 22:06:17 bmartin Exp $
 */

#include "VoxelMaxDoseObjective.hpp"
#include "DoseParser.hpp"


/**
 * Constructor: Initialize variables, read max/min voxel dose from file
 *
 * @param the_voi The Volume of intrestest that the objective acts on
 * @param objNo The number of this objective
 * @param dose_file_name The dose vector file containing the voxel max/min doses
 * @param nVoxels The total number of voxels in the dose vector
 * @param weight The overall weight for this objective
 * @param is_max_constraint True if this is a maximum dose constraint
 * @param weight_over Penalty for overdosing
 * @param is_min_constraint True if this is a minimum dose constraint
 * @param weight_under Penalty for underdosing
 */
VoxelMaxDoseObjective::VoxelMaxDoseObjective(
      Voi*  the_voi,
      unsigned int objNo,
      string dose_file_name,
      unsigned int nVoxels,
      float weight,
      float sampling_fraction,
      bool sample_with_replacement,
      bool  is_max_constraint,
      float weight_over,
      bool  is_min_constraint,
      float weight_under)
: SeparableObjective(the_voi, objNo, weight,
		     sampling_fraction, 
		     sample_with_replacement),
    reference_dose_(),
    is_max_constraint_(is_max_constraint),
    weight_over_(weight_over),
    is_min_constraint_(is_min_constraint),
    weight_under_(weight_under)
{
  // read dose from file
  DoseParser dose_parser(dose_file_name,nVoxels);
  reference_dose_ = dose_parser.get_dose();

}

/**
 * Destructor: default.
 */
VoxelMaxDoseObjective::~VoxelMaxDoseObjective()
{
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void VoxelMaxDoseObjective::printOn(std::ostream& o) const
{
  o << "OBJ " << objNo_ << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  if(is_max_constraint_) {
    o << "\tvoxel dependent max dose objective";
    if(weight_over_ != 1.0) {
        o << "\tweight " << weight_over_;
    }
  }
  if(is_min_constraint_) {
    o << "\tvoxel dependent min dose objective";
    if(weight_under_ != 1.0) {
        o << "\tweight " << weight_under_;
    }
  }
  if(weight_ != 1.0) {
      o << "\tweight " << weight_;
  }

}


