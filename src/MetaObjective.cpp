/**
 * @file: MetaObjective.cpp
 * MetaObjective Class implementation.
 *
 * $Id: MetaObjective.cpp,v 1.7 2008/03/24 17:05:00 bmartin Exp $
 */

#include "MetaObjective.hpp"

/**
 * Constructor. Takes responsibility for deleting the objectives.
 */
MetaObjective::MetaObjective(unsigned int objNo,
							 const Geometry *geometry,
							 const DoseDeliveryModel &uncertainty,
							 const vector<Objective*> &objectives)
  : objNo_(objNo),
	the_geometry_(geometry),
    the_UncertaintyModel_(&uncertainty),
    the_objectives_(objectives),
    nObjectives_(objectives.size())
{
    // Check if all of the objectives support meta-objectives
    for(vector<Objective*>::const_iterator iter = the_objectives_.begin();
            iter != the_objectives_.end();
            iter++) {
        if(!(*iter)->supports_meta_objective()) {
            throw(std::runtime_error("Included unsupported objective in meta objective"));
        }
    }

    cout << "Created meta-objective\n";
}

/**
 * Destructor: default.  Not responsible for deleting the objectives
 */
MetaObjective::~MetaObjective()
{
    vector<Objective*>::iterator iter;
    for(iter = the_objectives_.begin(); iter != the_objectives_.end(); ++iter) {
        delete *iter;
        *iter = 0;
    }
    the_objectives_.resize(0);
}


/**
 * Estimate or calculate the objective and the gradient for one instance 
 * of an uncertain treatment
 */
double MetaObjective::calculate_scenario_objective_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        vector<double> &multi_objective,
        vector<double> &estimated_ssvo,
        BixelVectorDirection & gradient, 
        float gradient_multiplier,
        const DoseDeliveryModel &uncertainty_model,
        const RandomScenario &random_scenario,
        bool use_voxel_sampling)
{
    double objective = 0;

    // loop over all objectives in this meta-objective
    for(unsigned int iObj=0;iObj<the_objectives_.size();iObj++) {
        double temp_ssvo = 0;
        double temp_obj = 
            the_objectives_[iObj]->calculate_scenario_objective_and_gradient(
                    beam_weights,
                    Dij,
                    gradient,
                    gradient_multiplier,
                    uncertainty_model,
                    random_scenario,
                    use_voxel_sampling,
                    temp_ssvo);
        objective += temp_obj;
        multi_objective.push_back(temp_obj);
        estimated_ssvo.push_back(temp_ssvo);
    }

    return objective;
}

/**
 * Estimate or calculate the objective for one instance of an uncertain 
 * treatment
 */
double MetaObjective::calculate_scenario_objective(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        const DoseDeliveryModel &uncertainty_model,
        const RandomScenario &random_scenario,
        vector<double> &multi_objective,
        vector<double> &estimated_ssvo,
        bool use_voxel_sampling)
{
    double objective = 0;

    // loop over all objectives in this meta-objective
    for(unsigned int iObj=0;iObj<the_objectives_.size();iObj++) {
        double temp_ssvo = 0;
        double temp_obj =
            the_objectives_[iObj]->calculate_scenario_objective(
                    beam_weights,
                    Dij,
                    uncertainty_model,
                    random_scenario,
                    use_voxel_sampling,
                    temp_ssvo);
        objective += temp_obj;
        multi_objective.push_back(temp_obj);
        estimated_ssvo.push_back(temp_ssvo);
    }

    return objective;
}


/**
 * Blah
 */
double MetaObjective::calculate_scenario_objective_and_gradient_and_Hv(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        vector<double> &multi_objective,
        vector<double> &estimated_ssvo,
        BixelVectorDirection & gradient,
        float gradient_multiplier,
        const vector<float> & v,
        vector<float> & Hv,
        const DoseDeliveryModel &uncertainty_model,
        const RandomScenario &random_scenario,
        bool use_voxel_sampling
        )
{
    cout << "called function which is not implemented for MetaObjective"
        << endl;
    exit(-1);
}



/**
 * Prints a description of the objective on the given stream
 * Provides a reasonable default if not overridden.
 *
 * @param o The output stream to write to
 */
void MetaObjective::printOn(std::ostream& o) const
{
    this->print_meta_objective_type(o);

    o << ": ";
    if(the_objectives_.size() == 0) {
	o << "does not contain any objectives" << endl;
    }
    else {
	o << endl;
	for(unsigned int iObj=0;iObj<the_objectives_.size();iObj++) {
	    the_objectives_[iObj]->printOn(o);
	    o << endl;
	}
    }
}


/**
 * Calculate the dose objective for the given dose.  Ignores any special 
 * behavior of the meta-objective.
 *
 * @param the_dose The dose distribution to evaluate
 * @param multi_obj A vector to hold the dose-objective for each contained 
 * objective.  The results are appended to the vector.
 */
double MetaObjective::calculate_dose_objective(
    const DoseVector & the_dose,
    vector<double> & multi_obj) const
{
    double objective = 0;

    // loop over all objectives in this meta-objective
    for(unsigned int iObj=0;iObj<the_objectives_.size();iObj++) {
        double temp_obj =
            the_objectives_[iObj]->calculate_dose_objective(the_dose);
        objective += temp_obj;
        multi_obj.push_back(temp_obj);
    }

    return objective;
}


/**
 * Find out how many voxels are in each objective if appropriate,
 * otherwise 0
 */
vector<unsigned int> MetaObjective::get_nVoxels() const
{
    vector<unsigned int> temp(the_objectives_.size());
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        temp[i] = the_objectives_[i]->get_nVoxels();
    }
    return temp;
}


/**
 * Find out how many voxels are in each objective if appropriate,
 * otherwise 0.  Appends results to supplied vector.
 */
void MetaObjective::get_nVoxels(vector<unsigned int> &v) const
{
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        v.push_back( the_objectives_[i]->get_nVoxels());
    }
}


/**
 * Find out voxel sampling rate if appropriate, otherwise 1
 */
vector<float> MetaObjective::get_sampling_fraction() const
{
    vector<float> temp(the_objectives_.size());
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        temp[i] = the_objectives_[i]->get_sampling_fraction();
    }
    return temp;
}


/**
 * Append voxel sampling rate for each objective to a vector
 */
void MetaObjective::get_sampling_fraction(vector<float> &v) const
{
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        v.push_back(the_objectives_[i]->get_sampling_fraction());
    }
}


/**
 * Set voxel sampling rate if appropriate, otherwise do nothing
 */
void MetaObjective::set_sampling_fraction(vector<float> sampling_fraction)
{
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        the_objectives_[i]->set_sampling_fraction(sampling_fraction[i]);
    }
}

/**
 * Set voxel sampling rate for each objective.  Returns an iterator 
 * after the last one used by this function
 */
vector<float>::const_iterator MetaObjective::set_sampling_fraction(
        vector<float>::const_iterator iter)
{
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        the_objectives_[i]->set_sampling_fraction(*iter);
        iter++;
    }
    return iter;
}


/**
 * Find out if each objective uses sampling and append to vector
 */
void MetaObjective::supports_voxel_sampling(vector<bool> &v) const
{
    for(size_t i = 0; i < the_objectives_.size(); i++) {
        v.push_back(the_objectives_[i]->supports_voxel_sampling());
    }
}

