#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <assert.h>

#include "ParameterVectorDirection.hpp"

using namespace std;


/**
 * @file ParameterVectorDirection.cpp
 * ParameterVectorDirection functions
 ***********************************/


/**
 * Constructor to initialize direction with given number of parameters.
 *
 * @param nParams     The number of parameters
 */
ParameterVectorDirection::ParameterVectorDirection( int nParams=0 )
	: nParams_(nParams), pvdType_(GENERAL_PVD_TYPE) {

	value_.resize( nParams_ );
	for (int ip = 0; ip < nParams_; ip++ ) value_[ip] = 0;
}


/**
 * Constructor to initialize direction with values from a ParameterVector
 *
 * @param nParams     The number of parameters
 */
ParameterVectorDirection::ParameterVectorDirection(const ParameterVector & rhs)
  : nParams_(rhs.get_nParameters())
  , pvdType_(GENERAL_PVD_TYPE)
{
  value_.resize( nParams_ );
  for (int ip = 0; ip < nParams_; ip++ ) value_[ip] = rhs.get_value(ip);
}


/**
 * Copy Constructor
 *
 * @param rhs the grid to copy
 */
ParameterVectorDirection::ParameterVectorDirection( const ParameterVectorDirection &rhs ) {
	nParams_ = rhs.nParams_;
	value_.resize( nParams_ );
	pvdType_ = rhs.pvdType_;
	for (int ip = 0; ip < nParams_; ip++ )
		value_[ip] = rhs.value_[ip];
}


/**
 * Destructor
 */
ParameterVectorDirection::~ParameterVectorDirection() {
	// Default works fine
}


/**
 * Assignment operator.
 *
 * @param rhs The ParameterVectorDirection object to copy
 */
void ParameterVectorDirection::operator=( const ParameterVectorDirection &rhs ) {
	nParams_ = rhs.nParams_;
	value_.resize( nParams_ );
	pvdType_ = rhs.pvdType_;
	for ( int ip = 0; ip < nParams_; ip++ )
		value_[ip] = rhs.value_[ip];
}



/**
 * Addition operator
 * Add the elements of one ParameterVectorDirection to another.
 */
ParameterVectorDirection ParameterVectorDirection::operator+(
		const ParameterVectorDirection& rhs ) const {

	assert(value_.size() == rhs.value_.size());
	assert( pvdType_ == rhs.pvdType_ || pvdType_ == GENERAL_PVD_TYPE || rhs.pvdType_ == GENERAL_PVD_TYPE );

	ParameterVectorDirection temp( *this );
	for ( size_t ip = 0; ip < value_.size(); ip++ )
		temp.value_[ip] += rhs.value_[ip];

	return ( temp );
}


/**
 * Subtraction operator.
 * Subtract the elements of one ParameterVectorDirection from another.
 */
ParameterVectorDirection ParameterVectorDirection::operator-(
		const ParameterVectorDirection& rhs ) const {

	assert(value_.size() == rhs.value_.size());
	assert( pvdType_ == rhs.pvdType_ || pvdType_ == GENERAL_PVD_TYPE || rhs.pvdType_ == GENERAL_PVD_TYPE );

	ParameterVectorDirection temp( *this );
	for ( size_t ip = 0; ip < value_.size(); ip++ )
		temp.value_[ip] -= rhs.value_[ip];

	return ( temp );
}


/**
 * Multiplication operator
 * Multiply the elements of a ParameterVectorDirection by a factor.
 */
ParameterVectorDirection ParameterVectorDirection::operator*( const float factor ) const {

	ParameterVectorDirection temp( 0 );
	temp.nParams_ = nParams_;
	temp.value_.resize( value_.size() );
	for ( size_t ip = 0; ip < value_.size(); ip++ )
		temp.value_[ip] = value_[ip] * factor;

	return ( temp );
}


/**
 * Addition to operator.
 * Add the respective elements of two ParameterVectorDirection vectors together.
 * Assumes that the added ParameterVectorDirection has the same number of parameters.
 *
 * @param rhs The ParameterVectorDirection object to add to this object
 */
void ParameterVectorDirection::operator+=( const ParameterVectorDirection &rhs ) {
	assert(rhs.nParams_ == nParams_);
	assert( pvdType_ == rhs.pvdType_ || pvdType_ == GENERAL_PVD_TYPE || rhs.pvdType_ == GENERAL_PVD_TYPE );

	for ( int ip = 0; ip < nParams_; ip++ )
		value_[ip] += rhs.value_[ip];
}


/**
 * Product operator.
 * Multiply the respective elements of two vectors together.
 * Assumes that the added ParameterVectorDirection has the same number of parameters.
 *
 * @param rhs The ParameterVectorDirection object to add to this object
 */
void ParameterVectorDirection::operator*=( const vector<float>& rhs ) {
	assert(rhs.size() == nParams_);

	for ( int ip = 0; ip < nParams_; ip++ )
		value_[ip] *= rhs[ip];
}


/**
 * Global Multiplication operator.
 * Multiply vector elements d by the requested scaling factor.
 *
 * @param factor    Float scaling factor
 * @param pvd       Vector of type ParameterVectorDirection.
 *
 * @return vector of type ParameterVectorDirection as the product of the input vector with the scaling factor.
 */
ParameterVectorDirection operator*( const float factor, const ParameterVectorDirection &pvd ) {
	return ( pvd * factor );
}


/**
 * Reset all Parameter intenities to zero.
 */
void ParameterVectorDirection::clear_values() {
	for (size_t ip = 0; ip < value_.size(); ip++ )
		value_[ip] = 0.;

	return;
}


/**
 * Scale all Parameter values by a constant.
 *
 * @param scale_factor all values will be multiplied by this
 */
void ParameterVectorDirection::scale_values( float scale_factor ) {
	for (size_t ip = 0; ip < value_.size(); ip++ )
		value_[ip] *= scale_factor;
}


/**
 * Calculate the dot product of two Parameter grid directions.
 *
 * @param rhs The other ParameterVectorDirection to dot with this one.
 */
float ParameterVectorDirection::dot_product( const ParameterVectorDirection & rhs ) const {
	assert(value_.size() == rhs.value_.size());
	assert( pvdType_ == rhs.pvdType_ || pvdType_ == GENERAL_PVD_TYPE || rhs.pvdType_ == GENERAL_PVD_TYPE );

	float product = 0;

	for ( size_t ip = 0; ip < value_.size(); ip++ )
		product += value_[ip] * rhs.value_[ip];

	return product;
}


/*float ParameterVectorDirection::dot_product( const ParameterVector & rhs ) const {
	assert(value_.size() == rhs.value_.size());

	float product = 0;

	for ( size_t ip = 0; ip < value_.size(); ip++ )
		product += value_[ip] * rhs.value_[ip];

	return product;
	}*/
