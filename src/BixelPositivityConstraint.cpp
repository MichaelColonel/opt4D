/**
 * @file: BixelPositivityConstraint.cpp
 * BixelPositivityConstraint Class implementation.
 */

#include "BixelPositivityConstraint.hpp"



/**
 * Constructor
 */
BixelPositivityConstraint::BixelPositivityConstraint(unsigned int consNo, float penalty)
  : Constraint(consNo),
    lagrange_(),
    initial_penalty_(penalty),
    penalty_(),
    in_active_set_(true)
{
}

/**
 * Destructor
 */
BixelPositivityConstraint::~BixelPositivityConstraint()
{
}

/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void BixelPositivityConstraint::printOn(std::ostream& o) const
{
  o << "CONS " << consNo_ << ": "; 
  o << "Bixel Positivity" << endl;
}

/**
 * initialize
 */
void BixelPositivityConstraint::initialize(DoseInfluenceMatrix& Dij)
{
  lagrange_.resize(Dij.get_nBixels(),0);
  penalty_.resize(Dij.get_nBixels(),initial_penalty_);
}



/**
 * update the lagrange multipliers
 */
void BixelPositivityConstraint::update_lagrange_multipliers(const BixelVector & beam_weights,
							    DoseInfluenceMatrix & Dij)
{
  if(verbose_) {
    cout << "updating lagrange multipliers for constraint " << get_consNo() << endl;
  }  

  // loop over all bixels
  for(unsigned int iBixel=0; iBixel<beam_weights.get_nBixels(); iBixel++) {
 
      float cons = -1 * beam_weights[iBixel];
      float lag_buf = lagrange_[iBixel];
	  float penalty = penalty_[iBixel];
      float temp_lagrange = 0;

      if(-1*lag_buf/penalty < cons) {
		temp_lagrange = lag_buf + penalty*cons;
      }

      lagrange_[iBixel] = temp_lagrange;
    }
}

/**
 * update the penalty factor
 */
void BixelPositivityConstraint::update_penalty(const BixelVector & beam_weights,
											   DoseInfluenceMatrix & Dij, 
											   float tol, 
											   float multiplier)
{
  for(unsigned int iBixel=0; iBixel<beam_weights.get_nBixels(); iBixel++) 
	{
	  if(beam_weights[iBixel]<-1*tol) {
		penalty_[iBixel] *= multiplier;
	  }
	}
}

/**
 * Calculate contribution of constraint to augmented Lagrangian
 */
double BixelPositivityConstraint::calculate_aug_lagrangian(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij)
{
  if(verbose_) {
    cout << "calc augmented lagragian for constraint " << get_consNo() << endl;
  }

    double objective = 0;
    double bixel_lagrange = 0;

    // loop over all bixels
    for(unsigned int iBixel=0; iBixel<beam_weights.get_nBixels(); iBixel++) {

      float cons = -1 * beam_weights[iBixel];
      float lag_buf = lagrange_[iBixel];
	  float penalty = penalty_[iBixel];

      if(-1*lag_buf/penalty < cons) {
		bixel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;
      }
      else {
		bixel_lagrange = -0.5*lag_buf*lag_buf/penalty;
      }

      objective += bixel_lagrange;
    }
    return objective;
}

/**
 * Calculate contribution of constraint to augmented Lagrangian and its gradient
 */
double BixelPositivityConstraint::calculate_aug_lagrangian_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier)
{
  if(verbose_) {
    cout << "calc gradient of augmented lagragian for constraint " << get_consNo() << endl;
  }

    assert(beam_weights.get_nBixels() == gradient.get_nBixels());

    double objective = 0;
    double bixel_lagrange = 0;

    // loop over all bixels
    for(unsigned int iBixel=0; iBixel<beam_weights.get_nBixels(); iBixel++) {

      float cons = -1 * beam_weights[iBixel];
      float lag_buf = lagrange_[iBixel];
	  float penalty = penalty_[iBixel];

      if(-1*lag_buf/penalty < cons) {
		// augmented lagrangian contribution
		bixel_lagrange = lag_buf*cons + 0.5*penalty*cons*cons;
		// gradient contribution
		gradient[iBixel] += -1 * (lag_buf + penalty * cons) * gradient_multiplier;
      }
      else {
		bixel_lagrange = -0.5*lag_buf*lag_buf/penalty;
      }
	  
      objective += bixel_lagrange;
    }
    return objective;
}

