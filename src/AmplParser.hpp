/**
 * @file AmplParser.hpp
 * AmplParser class header
 */

#ifndef AMPLPARSER_HPP
#define AMPLPARSER_HPP

class AmplParser;

#include <utility>
#include <iostream>
#include <fstream>
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <set>
using std::set;
using std::pair;
#include <assert.h>


#include "Plan.hpp"

/**
 * Class AmplParser extracts information from the Plan to write out 
 * an ampl style .dat file. Only the Voxel, Bixel, Voi, and Dij information
 * is written, no support for objectives.
 *
 * It allows to sample the voxels in a voi or to skip some of the vois completely.
 */
class AmplParser
{

    public:

    AmplParser(Plan* the_plan, string voiNos, bool use_voxel_sampling,
            string voi_sampling_fractions);

    void write_dat_file(string file_prefix);

    private:

    void get_nVois();
    void get_VoisVoxels();

    void get_nBixels();
    void get_allBixels();
    void get_nBeams();
    void get_allBeams();
    void get_nBixelsInBeam();
    void get_BeamsBixels();

    /// the plan the ampl file corresponds to
    Plan* the_plan_;

    /// the vois to be included
    set<unsigned int> vois_to_include_;

    /// the voxels to be included
    vector<bool> voxels_to_include_;

    /// whether to sample voxels in vois
    bool use_voxel_sampling_;

    /// whether to sample voxels in vois
    std::map<unsigned int, float> voxel_sampling_fractions_;

    // file names
    string dat_file_name_;

    // file pointers
    std::auto_ptr<std::ofstream> dat_file_;

    // Voxels and Vois

    /// total number of voxels
    pair<string,string> nVoxels_;
    /// total number of vois
    pair<string,string> nVois_;
    /// all voxels
    pair<string,string> nVoxelsInVoi_;
    /// voxels of individual vois
    pair<string,string> allVoxels_;
    /// number of voxels in each Voi
    vector<pair<string,string> > VoisVoxels_;

    // beamlets and beams

    /// total number of beamlets
    pair<string,string> nBixels_;
    /// total number of beams
    pair<string,string> nBeams_;
    /// all beamlets
    pair<string,string> allBixels_;
    /// all beams
    pair<string,string> allBeams_;
    /// number of bixels in each beam
    pair<string,string> nBixelsInBeam_;
    /// bixels of individual beams
    vector<pair<string,string> > BeamsBixels_;

};

#endif

