/**
 * @file: RobustifiedDoseConstraint.cpp
 * RobustifiedDoseConstraint Class implementation.
 *
 */

#include "RobustifiedDoseConstraint.hpp"

/**
 * Constructor: Initialize variables.  
 */
RobustifiedDoseConstraint::RobustifiedDoseConstraint(Voi*  the_voi,
						     unsigned int consNo,
						     bool is_max_constraint,
						     bool is_min_constraint,
						     float max_dose,
						     float min_dose,
						     const DoseDeliveryModel &uncertainty)
: Constraint(consNo),
  the_UncertaintyModel_(&uncertainty),
  the_voi_(the_voi),
  is_max_constraint_(is_max_constraint),
  is_min_constraint_(is_min_constraint),
  max_dose_(max_dose),
  min_dose_(min_dose),
  in_active_set_(),
  dose_contribution_norm_() 
{
  // make sure the uncertainty model works with discrete scenarios
  if(!the_UncertaintyModel_->has_discrete_scenarios()) {
    throw(std::runtime_error("Robustified constraint requires discrete scenarios"));
  }

  for(unsigned int i=0;i<get_nScenarios();i++) {

    in_active_set_.push_back(vector<bool>(get_nVoxels(),true));
    dose_contribution_norm_.push_back(vector<float>(get_nVoxels(),0));
  }
}


/**
 * Destructor: default.  
 */
RobustifiedDoseConstraint::~RobustifiedDoseConstraint()
{
}


/**
 * Initialize the constraint: calculate norm of dose contribution vectors
 */
void RobustifiedDoseConstraint::initialize(DoseInfluenceMatrix& Dij)
{
  BixelVectorDirection bvd = BixelVectorDirection(Dij.get_nBixels());
  BixelVector bv = BixelVector(Dij.get_nBixels());
  RandomScenario random_scenario;

  for(unsigned int iScenario=0; iScenario<get_nScenarios(); iScenario++) 
    {
      random_scenario = the_UncertaintyModel_->get_dose_eval_scenario(iScenario);
      
      for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
	{
	  bvd.clear_values();
	  unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	  // get the Dij row into bvd
	  the_UncertaintyModel_->calculate_voxel_dose_gradient(voxelNo,bv,Dij,random_scenario,bvd,1);
	  
	  // calculate norm
	  double norm = 0;
	  for(unsigned int i=0; i<Dij.get_nBixels(); i++) {
	    norm += bvd.get_intensity(i) * bvd.get_intensity(i);
	  }
	  
	  // set value
	  dose_contribution_norm_[iScenario][iVoxel] = norm;
	}
    }
  is_initialized_ = true;
}



/**
 * Prints a description of the constraint on the given stream
 *
 * @param o The output stream to write to
 */
void RobustifiedDoseConstraint::printOn(std::ostream& o) const
{
  if(is_max_constraint_) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\trobustified maximum dose hard constraint: max dose < " << max_dose_ << endl;
  }
  if(is_min_constraint_ && min_dose_ > 0) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\trobustified minimum dose hard constraint: min dose > " << min_dose_ << endl;
  }
}


/**
 * sequentially project onto voxel constraints 
 */
unsigned int RobustifiedDoseConstraint::project_onto(BixelVector & beam_weights,
						     DoseInfluenceMatrix & Dij)
{
  BixelVectorDirection bvd = BixelVectorDirection(Dij.get_nBixels());
  RandomScenario random_scenario;

  unsigned int nProjections = 0;

  // cout << "projecting onto constraint " << get_consNo() << ": ";

  for(unsigned int iScenario=0; iScenario<get_nScenarios(); iScenario++) 
    {
      random_scenario = the_UncertaintyModel_->get_dose_eval_scenario(iScenario);
      
      for(unsigned int iVoxel=0; iVoxel<get_nVoxels(); iVoxel++)
	{
	  if(is_active(iScenario,iVoxel)) 
	    {
	      // Find the voxelNo of the voxel we are looking at
	      unsigned int voxelNo = the_voi_->get_voxel(iVoxel);
	  
	      // Calculate dose to voxel
	      float dose = the_UncertaintyModel_->calculate_voxel_dose(voxelNo, beam_weights, Dij, random_scenario);

	      // need to distinguish four cases
	      float factor;
	      bool project = false;
	      if(dose>max_dose_){
		project = true;
		if(dose-max_dose_<0.5*(max_dose_-min_dose_)) {
		  factor = -2*(dose-max_dose_); 
		}
		else {
		  factor = -1*(dose-max_dose_) - 0.5*(max_dose_-min_dose_); 
		}
	      } 
	      if (dose<min_dose_){
		project = true;
		if(min_dose_-dose<0.5*(max_dose_-min_dose_)) {
		  factor = 2*(min_dose_-dose);
		}
		else {
		  factor = (min_dose_-dose) + 0.5*(max_dose_-min_dose_);
		}
	      }
	      
	      if(project) {
		// divide by the norm of the dij column
		factor = factor / dose_contribution_norm_[iScenario][iVoxel];

		// do the actual weight change
		bvd.clear_values();
		the_UncertaintyModel_->calculate_voxel_dose_gradient(voxelNo,beam_weights,Dij,random_scenario,bvd,1);
		beam_weights.add_scaled_parameter_vector_direction(bvd,factor);
		
		// increment projection counter
		nProjections++;
	      }
	      else {
		// constraint satisfied, no projection needed
		// remove voxel from active set
		in_active_set_[iScenario][iVoxel] = false;
	      }
	    }
	}
    }
  //  cout << nProjections << " projections" << endl;
  return nProjections;
}






