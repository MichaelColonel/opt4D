/**
 * @file: LQUncertaintyModel.cpp
 * LQUncertaintyModel Class implementation.
 *
 */

#include "LQUncertaintyModel.hpp"
#include "DoseParser.hpp"


/**
 * Constructor: Initialize variables, read clonogen density from file
 */
LQUncertaintyModel::LQUncertaintyModel(float alpha,
									   float betaalpha,
									   double maximum_cell_density,
									   string file_name,
									   unsigned int nVoxels)
  : nominal_alpha_(alpha),
	nominal_betaalpha_(betaalpha),
	nominal_log_cell_density_(),
    maximum_cell_density_(maximum_cell_density),
	nScenarios_(1)
{
  // read log cell tumor density from file
  DoseParser dose_parser(file_name,nVoxels);
  nominal_log_cell_density_ = dose_parser.get_dose();

  // find maximum
  float cell_max = std::numeric_limits<float>::min();
  for(unsigned int i=0; i<nominal_log_cell_density_.get_nVoxels(); i++){
	if(nominal_log_cell_density_.get_voxel_dose(i) > cell_max) {
	  cell_max = nominal_log_cell_density_.get_voxel_dose(i);
	}
  }

  // nomalize maximum to one
  float offset = 1-cell_max;
  for(unsigned int i=0; i<nominal_log_cell_density_.get_nVoxels(); i++){
	nominal_log_cell_density_.add_dose(i,offset);
  }
}

/**
 * Constructor: Initialize variables, read clonogen density from file
 */
LQModelNominal::LQModelNominal(float alpha,
							   float betaalpha,
							   double maximum_cell_density,
							   string file_name,
							   unsigned int nVoxels)
  : LQUncertaintyModel(alpha,betaalpha,maximum_cell_density,file_name,nVoxels)
{
}

/**
 * Constructor: Initialize variables, read clonogen density from file
 */
LQModelAlphaUncertainty::LQModelAlphaUncertainty(float alpha_min,
												 float alpha_max,
												 unsigned int nAlpha_values,
												 float betaalpha,
												 double maximum_cell_density,
												 string file_name,
												 unsigned int nVoxels)
  : LQUncertaintyModel(alpha_min,betaalpha,maximum_cell_density,file_name,nVoxels),
	alpha_min_(alpha_min),
	alpha_max_(alpha_max)
{
  // set number of scenarios
  nScenarios_ = nAlpha_values;

  if(nScenarios_ <= 1) {
	string temp = "Number of scenarios is not larger than one: ";
	std::cout << temp << nScenarios_ << std::endl;
	throw(std::runtime_error(temp));
  }

}


/**
 * returns alpha value for this voxel and scenario
 */
float LQModelAlphaUncertainty::get_voxel_alpha(unsigned int iScenario,unsigned int voxelNo)
{
  assert(iScenario < nScenarios_);
  return ( alpha_min_ + (alpha_max_-alpha_min_)*((float)iScenario/((float)nScenarios_-1)) );
}


/**
 * return probability of a scenario
 */
float LQModelAlphaUncertainty::get_scenario_probability(unsigned int iScenario)
{
  assert(iScenario < nScenarios_);
  return ( 1.0 / (float)nScenarios_ );
}

