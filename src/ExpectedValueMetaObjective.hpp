/**
 * @file ExpectedValueMetaObjective.hpp
 * ExpectedValueMetaObjective Class header
 */

#ifndef EXPECTEDVALUEMETAOBJECTIVE_HPP
#define EXPECTEDVALUEMETAOBJECTIVE_HPP

#include "MetaObjective.hpp"
#include "Geometry.hpp"
#include "DoseDeliveryModel.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class ExpectedValueMetaObjective optimizes the expectation value of a 
 * sum of primary objectives with respect to an uncertainty model.
 * 
 * The Meta-objective contains a vector of primary objectives to be minimized.
 * 
 * @todo implement exact calculation of objective
 * @todo adaptive voxel-sampling rate isn't implemented
 */
class ExpectedValueMetaObjective : public MetaObjective
{

    public:

        // Constructor
        ExpectedValueMetaObjective(unsigned int objNo,
								   const Geometry *geometry,
								   const DoseDeliveryModel &uncertainty,
								   const vector<Objective*> &objectives);

        // Print type
        virtual void print_meta_objective_type(std::ostream& o) const
        { o << "Expected Value";};

        // Virtual destructor
        virtual ~ExpectedValueMetaObjective();

	/// calculate or estimate objective
        virtual double calculate_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                int nScenario_samples = 1,
                bool use_voxel_sampling = false,
                bool use_scenario_sampling = true);


        /// calculate or estimate objective and gradient      
        virtual double calculate_objective_and_gradient(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                BixelVectorDirection & gradient,
				float gradient_multiplier = 1.0,
                int nScenario_samples = 1,
                bool use_voxel_sampling = false,
                bool use_scenario_sampling = true);


        /// calculate or estimate objective, gradient, and Hessian times        
        /// a vector.
        virtual double calculate_objective_and_gradient_and_Hv(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                vector<double> &multi_objective,
                vector<double> &estimated_ssvo,
                BixelVectorDirection & gradient,
                const vector<float> & v,
                vector<float> & Hv,
                int nScenario_samples = 1,
                bool use_voxel_sampling = false,
                bool use_scenario_sampling = true);

        /// estimate the true objective by taking many samples
        virtual MetaObjective::estimate estimate_true_objective(
                const BixelVector & beam_weights,
                DoseInfluenceMatrix & Dij,
                int max_samples,
                int min_samples,
                float max_uncertainty,
                bool use_voxel_sampling = true);

    protected:
        /// Prints a description of the objective (don't override)
        /// virtual void printOn(std::ostream& o) const;

    private:
        // Default constructor not allowed
        ExpectedValueMetaObjective();

        // returns probability for a scenario to be included during scenario sampling
        float get_scenario_sampling_threshold(unsigned int iSample) const;

        // returns probability for a scenario to be included during scenario sampling
        float get_scenario_weight(unsigned int iSample) const;
};


#endif
