
/**
 * @file ParameterVectorDirection.hpp
 * ParameterVectorDirection Class Definition.
 */

#ifndef PARAMETERVECTORDIRECTION_HPP
#define PARAMETERVECTORDIRECTION_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

// Predefine class before including others
class ParameterVectorDirection;

#include "ParameterVector.hpp"

enum ParameterVectorDirectionType { GENERAL_PVD_TYPE, BIXEL_PVD_TYPE, DIRECT_PVD_TYPE };

/// Print ParameterType
inline std::ostream& operator<< ( std::ostream& o,  const enum ParameterVectorDirectionType pType ) {
    switch ( pType ) {
        case GENERAL_PVD_TYPE:
            return o << "General Parameter Vector Direction";
        case BIXEL_PVD_TYPE:
            return o << "Bixel Vector Direction";
        case DIRECT_PVD_TYPE:
            return o << "Direct Parameter Vector Direction";
        default:
            throw std::logic_error("Invalid enum value.");
    }
};

/**
 * Class ParameterVectorDirection describes a displacement through parameter vector space.
 * It is similar to ParameterVector class but does not describe parameter activty or valid boundaries.
 */
class ParameterVectorDirection {

    public:

    // Constructors
    ParameterVectorDirection( void ) : nParams_(0), pvdType_(GENERAL_PVD_TYPE) {};
    ParameterVectorDirection( const int nParams );
    ParameterVectorDirection(const ParameterVector &rhs);

    // Copy Constructor
    ParameterVectorDirection( const ParameterVectorDirection &rhs );

    // Destructor
    ~ParameterVectorDirection();

    // Overloaded operators
    float& operator[]( const size_t ip );
    float operator[]( const size_t ip ) const;
    void operator=( const ParameterVectorDirection &rhs );
    void operator=( const ParameterVector &rhs );
    ParameterVectorDirection operator+( const ParameterVectorDirection &rhs ) const;
    ParameterVectorDirection operator-( const ParameterVectorDirection& rhs ) const;
    ParameterVectorDirection operator*( const float factor ) const;
    void operator+=( const ParameterVectorDirection &rhs );
    void operator*=( const float factor );
    void operator*=( const vector<float>& rhs );

    friend ParameterVectorDirection operator*( const float factor, const ParameterVectorDirection& pvd );

    float dot_product( const ParameterVectorDirection & rhs ) const;
  //    float dot_product( const ParameterVector & rhs ) const;


    /**
     * Functions about parameter value
     */

    // Functions to change all bixels
    void clear_values();
    void scale_values( const float scale_factor );

    /**
     * Get the value at a point.
     *
     * @param ip the parameter index.
     *
     * @return the parameter value.
     */
    inline float get_value( const int ip ) const {
        return value_[ip];
    };


    /**
     * Add to the parameter value at a point.
     * Add the desired increment value to the parameter value at a point.
     *
     * @param ip         The parameter index.
     * @param value_inc  The desired increment to the parameter value.
     */
    inline void add_value( const int ip, const float value_inc ) {
        value_[ip] += value_inc;
    };


    /**
     * Set the parameter value at a point.
     *
     * @param ip         The parameter index.
     * @param value     The desired parameter value.
     */
    inline void set_value( const int ip, const float value ) {
        value_[ip] = value;
    };

    /**
     * change the number of parameters and set values to zero
     *
     * @param nParams         new number of parameters
     */
    inline void resize( const unsigned int nParams ) {
	  value_.resize(nParams,0);
	  clear_values();
	  nParams_ = nParams;
    };


    /**
     * Get the number of parameters in the set
     *
     * @return the number of parameters in the set.
     */
    inline size_t get_nParameters() const {
        return nParams_;
    };


    /**
     * Get the current type of parameter
     *
     * @return the parameter vector direction type as an enum
     */
    inline ParameterVectorDirectionType getVectorType() const {
        return pvdType_;
    }


    /**
     * Set the current type of parameter
     *
     * @return the number of parameters in the set.
     */
    inline void setVectorType( ParameterVectorDirectionType pvdt ) {
        pvdType_ = pvdt;
    }

    /// Print Parameter Vector Direction Type
    friend std::ostream& operator<< ( std::ostream& o,  const enum ParameterVectorDirectionType pType );

protected:

    vector<float> value_;   // parameter values
    int nParams_;           // number of parameters
    ParameterVectorDirectionType pvdType_;  // The type or class of object
};


/* Inline functions
 * --------------------
 */

/**
 * Over-ride bracket to access parameter element at index.
 */
inline float & ParameterVectorDirection::operator[]( const size_t ip ) {
    return value_[ip];
}


/**
 * Over-ride bracket to access parameter element at index.
 */
inline float ParameterVectorDirection::operator[]( const size_t ip ) const {
  return value_[ip];
}


/**
 * Scale vector by a factor.
 */
inline void ParameterVectorDirection::operator*=( const float factor ) {
    scale_values( factor );
}


#endif // PARAMETERVECTORDIRECTION_HPP
