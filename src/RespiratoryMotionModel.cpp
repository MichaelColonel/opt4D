/**
 * @file: PredictableMotionModel.cpp
 * PredictableMotionModel Class implementation.
 *
 */

#include "RespiratoryMotionModel.hpp"
#include <fstream>




// ---------------------------------------------------------------------
// Dose delivery model for an uncertain motion Pdf
// ---------------------------------------------------------------------

/**
 * constructor 
 *
 */
PdfVariationModel::PdfVariationModel(const Geometry *the_geometry, 
				     string pdf_file_root,
				     unsigned int nInstances, 
				     unsigned int nModes)
    : DoseDeliveryModel(the_geometry)
      , nInstances_(nInstances)
      , nModes_(nModes)
      , auxiliary_dij_lut_(nInstances,0)
      , nominal_pdf_(nInstances,0)
      , modes_(nModes)
      , sigma_(nModes)
{
    cout << "Constructing PdfVariationModel:" << endl;

    for(unsigned int iInstance=0; iInstance<nInstances; iInstance++) {
	
	// auxiliary indices
	auxiliary_dij_lut_[iInstance] = iInstance;
    }

    // modes
    for(unsigned int iMode=0; iMode<nModes_; iMode++) {
	modes_[iMode].resize(nInstances);
    }

    // read the PDF and Modes from file
    read_pdf(pdf_file_root);

    // print info
    cout << "number of instances: " << get_nInstances() << endl;
    cout << "number of Pdf perturbation modes: " << get_nModes() << endl;
    cout << "sigmas: ";
	
    for(unsigned int iMode=0; iMode<nModes_; iMode++) {
	cout << get_sigma(iMode) << "\t";
    }

    cout << endl << "inst \tnominal Pdf \tModes" << endl;

    for(unsigned int iInstance=0; iInstance<nInstances; iInstance++) {
	cout << iInstance+1 << "\t";
	cout << get_nominal_pdf(iInstance) << "\t";
	
	for(unsigned int iMode=0; iMode<nModes_; iMode++) {
	    cout << get_mode(iMode,iInstance) << "\t";
	}
	cout << endl; 
    }
}

/**
 * constructor 
 *
 */
PdfVariationModel::PdfVariationModel(const Geometry *the_geometry, 
				     unsigned int nInstances, 
				     vector<float> Pdf,
				     vector<unsigned int> auxiliaryNos,
				     unsigned int nModes,
				     vector<vector<float> > modes,
				     vector<float> sigma)
    : DoseDeliveryModel(the_geometry)
      , nInstances_(nInstances)
      , nModes_(nModes)
      , auxiliary_dij_lut_(nInstances,0)
      , nominal_pdf_(nInstances,0)
      , modes_(nModes)
      , sigma_(nModes)
{
    cout << "Constructing PdfVariationModel:" << endl;

    assert(modes.size() == nModes);
    assert(sigma.size() == nModes);

    for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {
	
	// auxiliary indices
	auxiliary_dij_lut_[iInstance] = auxiliaryNos[iInstance];
	
	// nominal Pdf
	nominal_pdf_[iInstance] = Pdf[iInstance];
    }

    // modes
    for(unsigned int iMode=0; iMode<nModes_; iMode++) {
	
	modes_[iMode].resize(nInstances_);
	modes_[iMode] = modes[iMode];
	sigma_[iMode] = sigma[iMode];
    }

    // print info
    cout << "number of instances: " << get_nInstances();
    cout << "number of Pdf perturbation modes: " << get_nModes();
    cout << "instance \tnominal Pdf \tModes" << endl;

    for(unsigned int iInstance=0; iInstance<nInstances_; iInstance++) {
	cout << iInstance+1 << "\t";
	cout << get_nominal_pdf(iInstance) << "\t";
	
	for(unsigned int iMode=0; iMode<nModes_; iMode++) {
	    cout << get_mode(iMode,iInstance) << "\t";
	}
	cout << endl; 
    }
}

// destructor
PdfVariationModel::~PdfVariationModel()
{
}


// generate a random sample scenario 
void PdfVariationModel::generate_random_scenario(RandomScenario &random_scenario) const
{
    // get Pdf
    generate_Pdf(random_scenario);
}

// generate the nominal scenario 
void PdfVariationModel::generate_nominal_scenario(RandomScenario &random_scenario) const
{
    // set Pdf values
    random_scenario.Pdf_.resize(get_nInstances());
    random_scenario.auxiliary_dij_lut_.resize(get_nInstances());

    for(unsigned int i=0; i<get_nInstances(); i++) {
	// auxiliaries
	random_scenario.auxiliary_dij_lut_[i] = get_AuxiliaryDijNo(i);
	
	// nominal Pdf
	random_scenario.Pdf_[i] = get_nominal_pdf(i);
    }
}

/** 
 * generate a random Pdf.
 * The Pdf has to be positive for every instance. 
 * This is currently implemented implemented by 
 * discarding random Pdfs which do not fullfill that. 
 */
void PdfVariationModel::generate_Pdf(RandomScenario &random_scenario) const
{
    random_scenario.Pdf_.resize(get_nInstances());
    random_scenario.auxiliary_dij_lut_.resize(get_nInstances());

    vector<float> temp_ampl(nModes_,0);
    bool is_valid = false;

    while(!is_valid) {
	// reset Pdf
	fill(random_scenario.Pdf_.begin(),random_scenario.Pdf_.end(),0);
	
	// generate random amplitude
	for(unsigned int iMode=0; iMode<nModes_; iMode++) {
	    temp_ampl[iMode] = get_normal_distributed_random_number(sigma_[iMode]);
	}
	
	// set Pdf values
	for(unsigned int i=0; i<get_nInstances(); i++) {
	    // auxiliaries
	    random_scenario.auxiliary_dij_lut_[i] = get_AuxiliaryDijNo(i);
	    
	    // nominal Pdf
	    random_scenario.Pdf_[i] += get_nominal_pdf(i);
	    
	    // add modes 
	    for(unsigned int iMode=0; iMode<nModes_; iMode++) {
		random_scenario.Pdf_[i] += temp_ampl[iMode] * get_mode(iMode, i);
	    }
	}
	
	// check whether Pdf is positive definite
	for(unsigned int i=0; i<get_nInstances(); i++) {
	    if(random_scenario.Pdf_[i]<0) {
		break;
	    }
	    is_valid = true;
	}
    }
}

// calculate voxel dose for a scenario
float PdfVariationModel::calculate_voxel_dose(unsigned int voxelNo,	
					      const BixelVector &bixel_grid,                  
					      DoseInfluenceMatrix &dij,
					      const RandomScenario &random_scenario) const
{
    float dose_buf = 0;
    float dij_buf = 0;
    
    assert(random_scenario.Pdf_.size() == dij.get_nAuxiliaries()+1);

    for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
	dij_buf=0;
	for(unsigned int iInstance=0; iInstance<random_scenario.Pdf_.size(); iInstance++) {
	    dij_buf += (random_scenario.Pdf_[iInstance] 
			* iter.get_influence(random_scenario.auxiliary_dij_lut_[iInstance]) );	    
	}
	dose_buf += bixel_grid[iter.get_bixelNo()] * dij_buf;
    }
    
    return dose_buf;
}


// calculate gradient contributione of the voxel for a scenario
void PdfVariationModel::calculate_voxel_dose_gradient(
        unsigned int voxelNo,	
	const BixelVector &bixel_grid,                  
        DoseInfluenceMatrix &dij,
        const RandomScenario &random_scenario,
        BixelVectorDirection &gradient,
        float gradient_multiplier) const
{
    assert(random_scenario.Pdf_.size() == dij.get_nAuxiliaries()+1);

    float dij_buf = 0;

    for(DoseInfluenceMatrix::iterator iter = dij.begin_voxel(voxelNo);
	iter.get_voxelNo() == voxelNo; iter++) {
	dij_buf = 0;
	for(unsigned int iInstance=0; iInstance<random_scenario.Pdf_.size(); iInstance++) {
	    dij_buf += (random_scenario.Pdf_[iInstance] 
			* iter.get_influence(random_scenario.auxiliary_dij_lut_[iInstance]) );	    
	}
	gradient[iter.get_bixelNo()] += gradient_multiplier * dij_buf;
    }
}


/**
 * read Pdf and its variation modes
 *
 */
void PdfVariationModel::read_pdf(string pdf_file_root)
{
    string attribute;
    unsigned int uiDummy;
    float fDummy;

    string file_name = pdf_file_root + ".pdf";
    std::ifstream infile;

    infile.open(file_name.c_str(), std::ios::in);
    // check if the file exists
    if ( !infile.is_open() ) {
        std::cerr << "Can't open file: " << file_name << std::endl;
        throw("Unable to open .pdf file");
    }

    // read pdf file
    std::cout << "Reading PDF file: " << file_name << endl;
 
    // Header
    infile >> attribute;
    if(attribute != "number_of_instances") {
	cout << "Error: expected number_of_instances" << endl;
        throw("Error reading PDF file");
    }
    infile >> uiDummy;

    // check
    if(uiDummy!=get_nInstances()) {
	cout << "Error: wrong number of instances" << endl;
	throw("Error reading PDF file");
    }

    infile >> attribute;
    if(attribute != "number_of_modes") {
	cout << "Error: expected number_of_modes" << endl;
        throw("Error reading PDF file");
    }
    infile >> uiDummy;

    // check
    if(uiDummy!=get_nModes()) {
	cout << "Error: wrong number of modes" << endl;
	throw("Error reading PDF file");
    }

    // now read sigmas
    infile >> attribute;
    if(attribute != "sigmas:") {
	cout << "Error: expected sigmas:" << endl;
        throw("Error reading PDF file");
    }

    for(unsigned int iMode=0; iMode<get_nModes(); iMode++) {
	infile >> uiDummy;
	if(uiDummy!=iMode+1) {
	    cout << "Error: wrong mode when reading sigmas" << endl;
	    throw("Error reading PDF file");
	}
	infile >> fDummy;
	sigma_.at(iMode) = fDummy;
    }

    // now read pdf and modes
    infile >> attribute;
    if(attribute != "pdf:") {
	cout << "Error: expected pdf:" << endl;
        throw("Error reading PDF file");
    }

    for(unsigned int iInstance=0; iInstance<get_nInstances(); iInstance++) {
	infile >> uiDummy;
	if(uiDummy != iInstance+1) {
	    cout << "Error: wrong instance number" << endl;
	    throw("Error reading PDF file");
	}
	infile >> fDummy;
	nominal_pdf_.at(iInstance) = fDummy;
	for(unsigned int iMode=0; iMode<get_nModes(); iMode++) {
	    infile >> fDummy;
	    modes_.at(iMode).at(iInstance) = fDummy;
	}
    }

    infile.close();
}


/* ---------------------------------------------------------------- */
// multi instance gating model
/* ---------------------------------------------------------------- */

/**
 * constructor
 */
MultiInstanceGatingModel::MultiInstanceGatingModel(
    const Geometry *the_geometry, 
    unsigned int nInstances,
    unsigned int nBeams_per_instance)
    : StandardDoseModel(the_geometry) 
    , nInstances_(nInstances)
    , bixels_instanceNo_(the_geometry->get_nBixels(),0)
{
    cout << "Constructing dose delivery model for multiple instance gating" << endl;
    cout << "number of gating instances: " << nInstances_ << endl;

    for(unsigned int iBixel=0;iBixel<the_geometry_->get_nBixels();iBixel++) {
	unsigned int iInstance = 0;
	bool found_it = false;
	while(!found_it) {
	    if(the_geometry_->get_beamNo(iBixel) < (iInstance+1)*nBeams_per_instance) {
		bixels_instanceNo_[iBixel] = iInstance;
		found_it = true;
	    }
	    else {
		iInstance++;
		if(iInstance >= nInstances_) {
		    cout << "Error while initializing bixels_instanceNo_" << endl;
		    exit(-1);
		}
	    }
	}
    }
}

// destructor
MultiInstanceGatingModel::~MultiInstanceGatingModel()
{
}

/**
 * calculate instance dose
 */
void MultiInstanceGatingModel::calculate_instance_dose(
    DoseVector &dose_vector,
    const BixelVector &bixel_grid,                  
    DoseInfluenceMatrix &dij,
    unsigned int iInstance) const
{
    // clear dose vector
    dose_vector.reset_dose();
	
    if(iInstance >= nInstances_) {
	cout << "cannot calculate dose for instance "
	     << iInstance+1 << "." << endl; 
	cout << "Number of instances is: "
	     << nInstances_ << endl;
    }
    else {
	// loop over the whole dose influence matrix and skip bixels 
	// which don't belong to the instance
	DoseInfluenceMatrix::iterator iter = dij.begin();
	while(iter.not_at_end()) {
	    if(bixels_instanceNo_[iter.get_bixelNo()] == iInstance) {
		dose_vector.add_dose(iter.get_voxelNo(), 
				     bixel_grid.get_intensity(iter.get_bixelNo()) 
				     * iter.get_influence());
	    }
	    
	    ++iter;
	}
    }
}

