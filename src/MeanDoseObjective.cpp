/**
 * @file: MeanDoseObjective.cpp
 * MeanDoseObjective Class implementation.
 *
 * $Id: MeanDoseObjective.cpp,v 1.31 2008/03/13 22:06:17 bmartin Exp $
 */

#include "MeanDoseObjective.hpp"

/**
 * Constructor: Initialize variables.  
 *
 */
MeanDoseObjective::MeanDoseObjective(
      Voi*  the_voi,
      unsigned int objNo,
      float weight,
      float sampling_fraction,
      bool sample_with_replacement,
      bool  is_minimized)
: SeparableObjective(the_voi, objNo, weight,
                     sampling_fraction, sample_with_replacement),
    is_minimized_(is_minimized){
}

/**
 * Destructor: default.
 */
MeanDoseObjective::~MeanDoseObjective()
{
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void MeanDoseObjective::printOn(std::ostream& o) const
{
  o << "OBJ " << objNo_ << ": "; 
  o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
  if(is_minimized_) {
    o << "minimizing mean dose";
  }
  else {
    o << "maximizing mean dose";
  }
}


