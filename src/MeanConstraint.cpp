/**
 * @file: MeanConstraint.cpp
 * MeanConstraint Class implementation.
 *
 */

#include "MeanConstraint.hpp"

/**
 * Constructor
 */
MeanConstraint::MeanConstraint(Voi*  the_voi,
			       unsigned int consNo,
			       bool is_max_constraint,
			       bool is_min_constraint,
			       float max_dose,
			       float min_dose,
			       float initial_penalty)
: Constraint(consNo),
  mean_dose_computer_(),
  the_voi_(the_voi),
  is_max_constraint_(is_max_constraint),
  is_min_constraint_(is_min_constraint),
  max_dose_(max_dose),
  min_dose_(min_dose),
  penalty_(initial_penalty),
  initial_penalty_(initial_penalty),
  lagrange_(0),
  in_active_set_(true)
{
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
MeanConstraint::~MeanConstraint()
{
}


/**
 * Initialize the objective: calculate mean dose contributions of beamlets
 */
void MeanConstraint::initialize(DoseInfluenceMatrix& Dij)
{
  mean_dose_computer_.initialize(the_voi_,Dij);
  is_initialized_ = true;
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void MeanConstraint::printOn(std::ostream& o) const
{
  if(is_max_constraint_) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmax mean dose < " << max_dose_ << endl;
  }
  if(is_min_constraint_ && min_dose_ > 0) {
    o << "CONS " << consNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";
    o << "\tmin mean dose > " << min_dose_ << endl;
  }
}


/**
 * get lagrange multipliers as pairs <voxelNo,value>
 */
vector<pair<unsigned int,float> > MeanConstraint::get_lagrange_multipliers()
{
  cout << "get lagrange multipliers for constraint " << get_consNo() << endl;

  vector<pair<unsigned int,float> > data;

  for(unsigned int i=0; i<get_nVoxels(); i++) {
    unsigned int voxelNo = the_voi_->get_voxel(i);
    data.push_back(pair<unsigned int,float>(voxelNo,lagrange_));
  }
  return data;
}


/**
 * update the lagrange multipliers
 */
void MeanConstraint::update_lagrange_multipliers(const BixelVector & beam_weights,
						    DoseInfluenceMatrix & Dij)
{
  if(verbose_) {
    cout << "updating lagrange multipliers for constraint " << get_consNo() << endl;
  }

  // calculate mean dose
  float mean_dose = mean_dose_computer_.calculate_mean_dose(beam_weights);

  double temp_lagrange = 0;

  // upper bound active
  if(is_max_constraint_) {
    double cons = mean_dose-max_dose_;
    if(lagrange_ + penalty_*cons > 0) {
      temp_lagrange = lagrange_ + penalty_*cons;
    }
  }

  // lower bound active
  if(is_min_constraint_) {
    double cons = mean_dose-min_dose_;
    if(lagrange_ + penalty_*cons < 0) {
      temp_lagrange = lagrange_ + penalty_*cons;
    }
  }

  // set new value
  lagrange_ = temp_lagrange;

  if(verbose_) {
    cout << "new lagrange multiplier: " << lagrange_ << endl;
  }
}


/**
 * update the lagrange multipliers
 */
 void MeanConstraint::update_penalty(const BixelVector & beam_weights,
									 DoseInfluenceMatrix & Dij, 
									 float tol, 
									 float multiplier)
{
  // calculate mean dose
  double mean_dose = mean_dose_computer_.calculate_mean_dose(beam_weights);
  
  // upper bound active
  if(is_max_constraint_) {
    double cons = mean_dose-max_dose_;
	if(cons > tol*max_dose_) {
	  penalty_ *= multiplier;
	}
  }

  // lower bound active
  if(is_min_constraint_) {
    double cons = mean_dose-min_dose_;
	if(cons < -1*tol*min_dose_) {
	  penalty_ *= multiplier;
	}
  }
}



/**
 * Calculate contribution of constraint to augmented Lagrangian
 */
double MeanConstraint::calculate_aug_lagrangian(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
		double & constraint,
		double & merit)
{
  if(verbose_) {
    cout << "calc augmented lagragian for constraint " << get_consNo() << endl;
  }

  // calculate mean dose
  double mean_dose = mean_dose_computer_.calculate_mean_dose(beam_weights);
  
  // unconstrained minimum of penalty function
  double objective = 0.5*lagrange_*lagrange_/penalty_;

  // upper bound active
  if(is_max_constraint_) {
    double cons = mean_dose-max_dose_;
    if(-1*lagrange_/penalty_ < cons) {
      objective = lagrange_*cons + 0.5*penalty_*cons*cons;
    }
	if(cons > 0) {
	  constraint = cons;
	}
  }

  // lower bound active
  if(is_min_constraint_) {
    double cons = mean_dose-min_dose_;
    if(-1*lagrange_/penalty_ > cons) {
      objective = lagrange_*cons + 0.5*penalty_*cons*cons;
    }
	if(cons < 0) {
	  constraint = -1*cons;
	}
  }
  
  merit = 0.5*initial_penalty_*constraint*constraint;
  return objective;
}


/**
 * Calculate contribution of constraint to augmented Lagrangian and its gradient
 */
double MeanConstraint::calculate_aug_lagrangian_and_gradient(
        const BixelVector & beam_weights,
        DoseInfluenceMatrix & Dij,
        BixelVectorDirection & gradient,
        float gradient_multiplier)
{
  if(verbose_) {
    cout << "calc gradient of augmented lagragian for constraint " << get_consNo() << endl;
  }

  assert(beam_weights.get_nBixels() == gradient.get_nBixels());

  // calculate mean dose
  double mean_dose = mean_dose_computer_.calculate_mean_dose(beam_weights);

  // unconstrained minimum of penalty function
  double objective = -0.5*lagrange_*lagrange_/penalty_;

  // upper bound active
  if(is_max_constraint_) {
    double cons = mean_dose-max_dose_;
    if(-1*lagrange_/penalty_ < cons) {
      objective = lagrange_*cons + 0.5*penalty_*cons*cons;

      double factor = lagrange_ + penalty_ * cons;
      for(unsigned int iBixel = 0; iBixel < beam_weights.get_nBixels(); iBixel++) {
	gradient[iBixel] += factor*gradient_multiplier * mean_dose_computer_.mean_dose_contribution_[iBixel];
      }
    }
  }

  // lower bound active
  if(is_min_constraint_) {
    double cons = mean_dose-min_dose_;
    if(-1*lagrange_/penalty_ > cons) {
      objective = lagrange_*cons + 0.5*penalty_*cons*cons;

      double factor = lagrange_ + penalty_ * cons;
      for(unsigned int iBixel = 0; iBixel < beam_weights.get_nBixels(); iBixel++) {
	gradient[iBixel] += factor*gradient_multiplier * mean_dose_computer_.mean_dose_contribution_[iBixel];
      }
    }
  }
  
  return objective;
}



/**
 * project onto / beyond constraint
 */
unsigned int MeanConstraint::project_onto(BixelVector & beam_weights,
					  DoseInfluenceMatrix & Dij)
{
  // note: mean constraint is always check, independent of whether it's in the active set

  // cout << "projecting onto constraint " << get_consNo() << endl;

  // Calculate dose to voxel
  float dose = mean_dose_computer_.calculate_mean_dose(beam_weights);

  // need to distinguish four cases
  float factor;
  bool project = false;
  if(dose>max_dose_){
    project = true;
    if(dose-max_dose_<0.5*(max_dose_-min_dose_)) {
      factor = -2*(dose-max_dose_); 
    }
    else {
      factor = -1*(dose-max_dose_) - 0.5*(max_dose_-min_dose_); 
    }
  } 
  if (dose<min_dose_){
    project = true;
    if(min_dose_-dose<0.5*(max_dose_-min_dose_)) {
      factor = 2*(min_dose_-dose);
    }
    else {
      factor = (min_dose_-dose) + 0.5*(max_dose_-min_dose_);
    }
  }
  
  if(project) {
    // divide by the norm of the dij column
    factor = factor / mean_dose_computer_.norm_;

    // do the actual weight change
    for(unsigned int iBixel=0; iBixel<beam_weights.get_nBixels(); iBixel++) {
      beam_weights[iBixel] += factor * mean_dose_computer_.mean_dose_contribution_[iBixel];
    }
    in_active_set_ = true;
    return 1;
  }
  else {
    // constraint satisfied, no projection needed
    // remove voxel from active set
    in_active_set_ = false;
    return 0;
  }
}

/**
 * prepare constraints for commercial solver interface
 */
void MeanConstraint::add_to_optimization_data_set(GenericOptimizationData & data, DoseInfluenceMatrix & Dij) const
{
    cout << "Creating generic constraint set for CONS " << get_consNo() << endl;
    
    // number of coefficients is number of bixels
    unsigned int nEntries = Dij.get_nBixels();
    
    // initialize next constraint
    data.constraints_[data.nConstraints_until_now_].initialize(nEntries);

    // add coefficients
    for(unsigned int iEntry = 0; iEntry<nEntries; iEntry++) {
 	data.constraints_[data.nConstraints_until_now_].index_[iEntry] = iEntry;
	data.constraints_[data.nConstraints_until_now_].value_[iEntry] = 
	  mean_dose_computer_.mean_dose_contribution_[iEntry];
    }
    
    // add to number of nonzero coefficients
    data.nNonZeros_ += nEntries;

    // bounds
    if(is_max_constraint_) {
	data.constraints_[data.nConstraints_until_now_].upper_bound_ = max_dose_;
    }
    else {
	data.constraints_[data.nConstraints_until_now_].upper_bound_ = OPT4D_INFINITY;
    }
    
    if(is_min_constraint_) {
	data.constraints_[data.nConstraints_until_now_].lower_bound_ = min_dose_;
    }
    else {
	data.constraints_[data.nConstraints_until_now_].lower_bound_ = -OPT4D_INFINITY;
    }
    
    // constraint type
    if(is_max_constraint_ && is_min_constraint_) {
	if(abs(max_dose_-min_dose_) < OPT4D_EPSILON) {
	    data.constraints_[data.nConstraints_until_now_].type_ = EQUALITY;
	}
	else {
	    data.constraints_[data.nConstraints_until_now_].type_ = TWOSIDED;
	}
    }
    else if(is_max_constraint_ && !is_min_constraint_) {
	data.constraints_[data.nConstraints_until_now_].type_ = UPPERBOUND;
    }
    else if(!is_max_constraint_ && is_min_constraint_) {
	data.constraints_[data.nConstraints_until_now_].type_ = LOWERBOUND;
    }
    else {
	cout << "WARNING: CONS " << get_consNo() << " has neither max nor min dose." << endl; 
	data.constraints_[data.nConstraints_until_now_].type_ = FREE; // bounds are +/- infinity
    }

    // increment constrait counter
    data.nConstraints_until_now_ += 1;
}


