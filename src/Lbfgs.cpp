
/**
 * @file Lbfgs.cpp
 * Lbfgs Class implementation.
 * 
 */


#include "Lbfgs.hpp"


/**
 * Constructor
 */
Lbfgs::HistoryPoint::HistoryPoint()
  : diff_param_(),
    diff_grad_(),
    product_(0)
{
}

/**
 * Constructor
 */
Lbfgs::HistoryPoint::HistoryPoint(unsigned int nParams)
  : diff_param_(nParams),
    diff_grad_(nParams),
    product_(0)
{
}

/**
 * calculate 1/(diff_grad_ * diff_param_)
 */
void Lbfgs::HistoryPoint::calculate_product()
{
  product_ = 1.0 / (diff_grad_.dot_product(diff_param_));
}

/**
 * Constructor
 */
Lbfgs::options:: options()
  : nHistory_(10)
{
}

/**
 * Constructor: initialize options
 */
Lbfgs::Lbfgs(Lbfgs::options options, unsigned int nParams)
  :   Optimizer(nParams),
      options_(options),
      history_(),
      is_initialized_(false)
{
  cout << "L-BFGS constructor called" << endl;
}

/**
 * Destructor
 */
Lbfgs::~Lbfgs()
{
  for(unsigned int i=0; i<history_.size(); i++) {
    delete history_[i];
    history_[i] = NULL;
  }
}


/**
 * Calculate the direction based on current gradient and parameter vector
 * and the stored history
 *
 * This implements the two-loop recursion algorithm described in
 * Nocedal & Wright, Numerical Optimization, 2006, p178
 *
 */
void Lbfgs::calculate_direction(const ParameterVectorDirection & gradient,
				const ParameterVector & param)
{
  // check if this is the first step and there is no history yet
  if(!is_initialized_) {
    // save gradient
    last_gradient_ = gradient;
    last_param_ = ParameterVectorDirection(param);

    // just use the gradient as the direction
    the_direction_ = gradient;
    the_direction_ *= -1;

    is_initialized_ = true;
  }
  else {

    // update history
    history_.push_front(new Lbfgs::HistoryPoint(param.get_nParameters()));
    history_.front()->diff_param_ = ParameterVectorDirection(param) - last_param_;
    history_.front()->diff_grad_ = gradient - last_gradient_;
    history_.front()->calculate_product();
    
    // delete the oldest history point if history is at full length
    if(history_.size() > options_.nHistory_) {
      history_.pop_back();
    }

    // save gradient
    last_gradient_ = gradient;
    last_param_ = ParameterVectorDirection(param);

    // do L-BFGS update
    ParameterVectorDirection q = gradient;
    vector<float> a;
    a.resize(history_.size(),0);
    
    for(unsigned int i=0; i<history_.size(); i++) {
      a[i] = history_[i]->product_ * q.dot_product(history_[i]->diff_param_);
      q += ( history_[i]->diff_grad_ * (-1*a[i]) );
      //cout << a[i] << " ";
    }
    //cout << endl;
    
    // scale q with initial diagonal hessian
    float scale_factor = history_[0]->diff_param_.dot_product(history_[0]->diff_grad_) / 
                         history_[0]->diff_grad_.dot_product(history_[0]->diff_grad_);

    q *= scale_factor;

    float b = 0;
    for(int i=history_.size()-1; i>=0; i=i-1) {
      b = history_[i]->product_ * history_[i]->diff_grad_.dot_product(q);
      q += ( history_[i]->diff_param_ * (a[i] - b) ) ;
      //cout << b << " " ;
    }
    //cout << endl;

    // Set the direction
    the_direction_ = q;
    the_direction_ *= -1;
  }
}

/**
 * deletes the history
 */
void Lbfgs::reset_optimizer()
{
  history_.resize(0);
  last_param_.clear_values();
  last_gradient_.clear_values();
  is_initialized_ = false;
}

/**
 * corrects the stored last gradient
 */
void Lbfgs::reset_optimizer(const ParameterVectorDirection & last_gradient)
{
  last_gradient_ = last_gradient;
}

