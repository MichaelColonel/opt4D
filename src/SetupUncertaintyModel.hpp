#ifndef SETUPUNCERTAINTYMODEL_HPP
#define SETUPUNCERTAINTYMODEL_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

// Predefine class before including others
class SetupUncertaintyModel;


#include "Geometry.hpp"
#include "DoseDeliveryModel.hpp"
#include "GaussianSetupError.hpp"



// ---------------------------------------------------------------------
// Setup uncertainty
// ---------------------------------------------------------------------


/**
 * This class implements setup errors. Dose distributions in the shifted patient
 * are approximated using the static dose cloud assumption. The approximation can be improved 
 * by correcting for a shift of the static dose cloud in beam direction. 
 */
class SetupUncertaintyModel : public ShiftBasedErrorModel
{

  public:
    /// constructor
    SetupUncertaintyModel(const Geometry *the_geometry,
			  const DoseDeliveryModel::options *options);

    /// destructor
    virtual ~SetupUncertaintyModel();

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("SetupUncertaintyModel");}

    /// generate a random sample scenario 
    void generate_random_scenario(RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    bool set_dose_evaluation_scenario(map<string,string> scenario_map);

    /// write results
    void write_dose(BixelVector &bixel_grid,
		    DoseInfluenceMatrix &dij,
		    string file_prefix, 
		    Dvh dvh) const;

  private:
    /// Make sure default constructor is never used
    SetupUncertaintyModel();

    /// Put shifts in to RandomScenario
    void set_shifts_in_scenario(
            RandomScenario &random_scenario,
            vector<Shift_in_Patient_Coord> daily_shifts) const;

    /// transform setup shift to effective voxel shift
    Shift_in_Patient_Coord transform_setup_error_to_voxel_shift(
	Shift_in_Patient_Coord setup_shift, unsigned int iBeam) const;

    /// correct for shift of the static dose cloud in beam direction 
    bool use_shift_correction_;

    /// use randomized rounding for every voxel
    bool use_randomized_rounding_;

    /// use linear interpolation for shifts
    bool use_linear_interpolation_;

    /// switch on merging of matching shifts
    bool merge_shifts_;

    /// Setup error model:
    GaussianSetupError setup_model_;

    /// normal vector on the patient surface for each beam direction 
    vector<Shift_in_Patient_Coord> surface_normal_;

    friend class RangeSetupUncertaintyModel;
};


#endif
