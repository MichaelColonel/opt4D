#ifndef RANGEUNCERTAINTYMODEL_HPP
#define RANGEUNCERTAINTYMODEL_HPP

// Predefine class before including others
class RangeUncertaintyModel;


#include <vector>
using std::vector;
#include <string>
using std::string;

#include "DoseVector.hpp"
#include "DoseInfluenceMatrix.hpp"
#include "DoseDeliveryModel.hpp"



/**
 * This class handles range uncertainties. It includes the model to describe range uncertainties and calculates auxiliary Dij matrices for different range shifts
 */
class AuxiliaryRangeModel : public AuxiliaryBasedSystematicErrorModel
{          

  public:
    /// constructor: 
    AuxiliaryRangeModel(DoseInfluenceMatrix &Dij, 
			const Geometry *Geom,
			const DoseDeliveryModel::options *options); 

    /// generate nominal scenario 
    virtual void generate_nominal_scenario(RandomScenario &random_scenario) const;

    /// store a dose evaluation scenario
    virtual bool set_dose_evaluation_scenario(map<string,string> scenario_map);

  protected:
    // access functions
    float get_max_range_dev() const;
    float get_pdf_width(unsigned int iBixel) const;
    float get_RangeShift(unsigned int iInstance, unsigned int iBixel);
    unsigned int get_auxiliary_for_range_shift(unsigned int bixelNo, float range_shift);

    /// The dose influence matrix
    DoseInfluenceMatrix *the_DoseInfluenceMatrix_;

  private:
    /// default constructor 
    AuxiliaryRangeModel();

    // initialize 
    void init_RangeShifts();
    void init_Dijs();

    void calculate_auxiliary_Dijs();

    /// determines how many new Dij elements have to be added
    void add_blank_space_to_Dij();

    /// adds new Dij elements 
    void add_elements_to_Dij();

    /// calculates auxiliary Dij elements for all range shifts
    void calculate_auxiliary_Dij_elements();

    void read_range_uncertainty_model(string dij_file_root);
    void write_range_uncertainty_model(string dij_file_root);

    /// maximum range deviation in multiples of the distribution width 
    float max_range_dev_;

    /// probability distribution width for each beamlet
    vector<float> pdf_width_;

    /// set of range shifts for each beamlet
//    vector< vector<float> > range_shifts_;
    vector< map<unsigned int,float> > range_shifts_;
};


inline
float AuxiliaryRangeModel::get_pdf_width(unsigned int iBixel) const
{
    return pdf_width_.at(iBixel);
}

inline
float AuxiliaryRangeModel::get_RangeShift(unsigned int iInstance, unsigned int iBixel)
{
//    return range_shifts_.at(iInstance).at(iBixel);
    return(range_shifts_[iBixel][iInstance]);
}

inline
float AuxiliaryRangeModel::get_max_range_dev() const
{
    return(max_range_dev_);
}


/**
 * auxiliary based range uncertainty model that allows for different beamlet correlation models
 */
class GeneralAuxiliaryRangeModel : public AuxiliaryRangeModel
{          

  public:
    /// constructor: 
    GeneralAuxiliaryRangeModel(DoseInfluenceMatrix &Dij, 
			       const Geometry *Geom,
			       const DoseDeliveryModel::options *options); 

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("GeneralAuxiliaryRangeModel");}

    /// returns true if bixels j and k are correlated
    virtual bool bixels_correlated(unsigned int BixelNo_j,unsigned int BixelNo_k) const;

    /// returns the bixel correlation model
    virtual std::string get_correlation_model() const;

    /// generate a random sample scenario 
    virtual void generate_random_scenario(RandomScenario &random_scenario) const;

    /// calculate the expected dose in a voxel
    virtual float calculate_expected_voxel_dose(unsigned int voxelNo,	
						const BixelVector &bixel_grid,                  
						DoseInfluenceMatrix &dij) const;
    
    /// calculate the variance of the dose in a voxel
    virtual float calculate_voxel_variance(unsigned int voxelNo,
					   const BixelVector &bixel_grid, 
					   DoseInfluenceMatrix &dij) const;

    /// Returns true if the model supports calculate_voxel_variance
    virtual bool supports_calculate_voxel_variance() const {return true;};
    
    /// True if the objective can be calculated exactly by an uncertainty model
    virtual bool supports_calculate_quadratic_objective_integrate() const {return true;};

    /// True if the objective can be calculated exactly by an uncertainty model
    virtual bool supports_calculate_quadratic_objective_tensor() const {return true;};

    /// calculate gradient contribution of the voxel variance
    virtual float calculate_quadratic_objective(
            unsigned int voxelNo,
            float prescribed_dose,	
            const BixelVector &bixel_grid,                  
            DoseInfluenceMatrix &dij,
            BixelVectorDirection &gradient,
            float gradient_multiplier,
            double adjusted_weight) const;
    
    /// calculate bixel pair contributions to quadratic objective
    virtual void calculate_bixel_pair_contributions_to_quadratic_objective(
	unsigned int voxelNo,
	vector< vector<double> > &QuadraticTerms,
	vector<double> &LinearTerms,
	vector<float> &Temp_DijRow,
	vector<float> &Temp_LinearTerms) const;

    /// write results
    virtual void write_dose(BixelVector &bixel_grid,
			    DoseInfluenceMatrix &dij,
			    string file_prefix, 
			    Dvh dvh) const;

  protected:

  private:
    /// default constructor 
    GeneralAuxiliaryRangeModel();

    /// init auxiliary Pdf
    void init_Pdf();
    void init_GaussianPdf();

    /// calculate variance for correlation model "within_beam"
    float calculate_voxel_variance_within_beam(unsigned int voxelNo, 
					       const BixelVector &bixel_grid, 
					       DoseInfluenceMatrix &dij) const;

    /// calculate voxel contribution to quadratic objective
    float calculate_quadratic_objective_within_beam(
            unsigned int voxelNo,
            float prescribed_dose,	
            const BixelVector &bixel_grid,                  
            DoseInfluenceMatrix &dij,
            BixelVectorDirection &gradient,
            float gradient_multiplier,
            double adjusted_weight) const;

    /// auxiliaries for a random scenario
    void generate_random_auxiliaries(RandomScenario &random_scenario) const;

    /// correlation model for beamlets
    std::string correlation_model_;

    friend class RangeSetupUncertaintyModel;
};


inline
std::string GeneralAuxiliaryRangeModel::get_correlation_model() const
{
    return correlation_model_;
}


/**
 * Range uncertainty model based on auiliaries which uses a discrete set of scenarios 
 */
class DiscreteAuxiliaryRangeModel : public AuxiliaryRangeModel
{          

  public:
    /// constructor: 
    DiscreteAuxiliaryRangeModel(DoseInfluenceMatrix &Dij, 
				const Geometry *Geom,
				const DoseDeliveryModel::options *options); 

    /// returns type of uncertainty model
    virtual std::string get_uncertainty_model() const {
	return("DiscreteAuxiliaryRangeModel");}

    /// returns true
    // bool supports_calculate_exact_objective() const {return true;};
    bool has_discrete_scenarios() const {return true;};

    /// generate a random sample scenario 
    virtual void generate_random_scenario(RandomScenario &random_scenario) const;

    /// write results
    virtual void write_dose(BixelVector &bixel_grid,
			    DoseInfluenceMatrix &dij,
			    string file_prefix, 
			    Dvh dvh) const;

  protected:

  private:
    /// default constructor 
    DiscreteAuxiliaryRangeModel();
};



#endif

