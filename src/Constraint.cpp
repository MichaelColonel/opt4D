/**
 * @file: Constraint.cpp
 * Constraint Class implementation.
 */

#include "Constraint.hpp"



/**
 * Constructor: Initialize variables.  By default, there are no constraints.
 */
Constraint::Constraint(unsigned int consNo)
  : is_initialized_(false)
  , consNo_(consNo)
  , verbose_(false)
{
}

/**
 * Destructor: default.  Not responsible for deleting the Voi
 */
Constraint::~Constraint()
{
}


//
// functions that should not be called or should be implemented in derived class
//

float Constraint::get_lower_bound() const
{
    cout << "Called Constraint::get_lower_bound()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

float Constraint::get_upper_bound() const
{
    cout << "Called Constraint::get_upper_bound()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

void Constraint::set_lower_bound(float value)
{
    cout << "Called Constraint::set_lower_bound()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

void Constraint::set_upper_bound(float value)
{
    cout << "Called Constraint::set_upper_bound()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

unsigned int Constraint::get_nGeneric_constraints() const 
{
    cout << "WARNING: called Constraint::get_nGeneric_constraints()" << endl;
    return(0);
}

void Constraint::add_to_optimization_data_set(GenericOptimizationData & data, 
					      DoseInfluenceMatrix & Dij) const
{
    cout << "Called Constraint::add_to_optimization_data_set()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

unsigned int Constraint::project_onto(BixelVector & beam_weights,
				      DoseInfluenceMatrix & Dij)
{
    cout << "Called Constraint::project_onto()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

bool Constraint::is_active_set_empty() const
{
    cout << "Called Constraint::is_active_set_empty()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

void Constraint::reset_active_set()
{
    cout << "Called Constraint::reset_active_set()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

double Constraint::calculate_dose_aug_lagrangian(const DoseVector & the_dose)
{
    cout << "Called Constraint::calculate_dose_aug_lagrangian" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

double Constraint::calculate_dose_aug_lagrangian_and_gradient(const DoseVector & the_dose, DoseVector & dose_gradient)
{
    cout << "Called Constraint::calculate_dose_aug_lagrangian_and_gradient" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

double Constraint::calculate_aug_lagrangian(const BixelVector & beam_weights,
											DoseInfluenceMatrix & Dij,
											double & constraint,
											double & merit)
{
    cout << "Called Constraint::calculate_aug_lagrangian()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

double Constraint::calculate_aug_lagrangian(const BixelVector & beam_weights,
											DoseInfluenceMatrix & Dij)
{
  double constraint = 0;
  double merit = 0;
  return this->calculate_aug_lagrangian(beam_weights,Dij,constraint,merit);
}

double Constraint::calculate_aug_lagrangian_and_gradient(
    const BixelVector & beam_weights,
    DoseInfluenceMatrix & Dij,
    BixelVectorDirection & gradient,
    float gradient_multiplier)
{
    cout << "Called Constraint::calculate_aug_lagrangian_and_gradient()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

void Constraint::update_lagrange_multipliers(const BixelVector & beam_weights,
					     DoseInfluenceMatrix & Dij)
{
    cout << "Called Constraint::update_lagrange_multipliers()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

void Constraint::update_penalty(const BixelVector & beam_weights,
								DoseInfluenceMatrix & Dij, 
								float tol, float multiplier)
{
    cout << "Called Constraint::update_penalty()" << endl;
    throw(std::runtime_error("Function not implemented in derived class"));
}

vector<pair<unsigned int,float> > Constraint::get_lagrange_multipliers()
{
    cout << "WARNING: Called Constraint::get_lagrange_multipliers()" << endl;
}
