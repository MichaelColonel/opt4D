/**
 * @file MaxMinOrMinMaxObjective.hpp
 * MaxMinOrMinMaxObjective Class header
 *
 */

#ifndef MAXMINORMINMAXOBJECTIVE_HPP
#define MAXMINORMINMAXOBJECTIVE_HPP

#include "Objective.hpp"
#include "Voi.hpp"
#include <cmath>

/**
 * Class MaxMinOrMinMaxObjective implements an objective to maximize the minimum dose 
 * or minimizing the maximum dose to a VOI. 
 *
 * Supports: external solver interface
 */
class MaxMinOrMinMaxObjective : public Objective
{

public:
  
  // Constructor
  MaxMinOrMinMaxObjective(Voi*  the_voi,
			  unsigned int objNo,
			  bool minimize_max,
			  float weight);
  
  // destructor
  ~MaxMinOrMinMaxObjective();
  
  /// calculate objective
  double calculate_objective(const BixelVector & beam_weights,
			     DoseInfluenceMatrix & Dij,
			     bool use_voxel_sampling,
			     double &estimated_ssvo);
  
  /// calculate objective from dose vector
  double calculate_dose_objective(const DoseVector & the_dose);

  /// Find out how many voxels are in this objective
  unsigned int get_nVoxels() const;
  
  /// Print a description of the objective
  void printOn(std::ostream& o) const;

  /// initialize the objective
  void initialize(DoseInfluenceMatrix& Dij);

  //
  // specific functions for commercial solver interface
  //

  bool supports_external_solver() const {return true;};
  
  /// returns total number of constraints
  unsigned int get_nGeneric_constraints() const;

  /// returns total number of auxiliary variables
  unsigned int get_nAuxiliary_variables() const;

  /// extract data for commercial solver
  void add_to_optimization_data_set(GenericOptimizationData & data, 
				    DoseInfluenceMatrix & Dij) const;



private:

  // Default constructor not allowed
  MaxMinOrMinMaxObjective();

  /// the corresponding VOI
  Voi *the_voi_;

  /// true if we minimize the max, false if we maximize the min
  bool minimize_max_;

};

/*
 * Inline functions
 */


inline
unsigned int MaxMinOrMinMaxObjective::get_nVoxels() const
{
  return the_voi_->get_nVoxels();
}



#endif
