/**
 * @file: DvhCoverageObjective.cpp
 * DvhCoverageObjective Class implementation.
 *
 * $Id: DvhCoverageObjective.cpp,v 1.1 2008/03/13 22:06:17 bmartin Exp $
 */

#include "DvhCoverageObjective.hpp"

/**
 * Constructor: Initialize variables.
 *
 * @param the_voi The Volume of intrestest that the objective acts on
 * @param objNo The number of this objective
 * @param weight The overall weight for this objective
 * @param dose Dose to compare
 * @param min_volume Minimum fractional volume to recieve at least the dose 
 * (between 0 and 1).
 * @param max_volume Maximum fractional volume to recieve at least the dose 
 * (between 0 and 1).
 */
DvhCoverageObjective::DvhCoverageObjective(
        Voi*  the_voi,
        unsigned int objNo,
        float weight,
        float sampling_fraction,
        bool sample_with_replacement,
        float dose,
        float min_volume,
        float max_volume
        )
: NonSeparableObjective(the_voi, objNo, weight,
        sampling_fraction, sample_with_replacement),
    dose_(dose),
    min_volume_(min_volume),
    max_volume_(max_volume)
{
}

/**
 * Destructor: default.
 */
DvhCoverageObjective::~DvhCoverageObjective()
{
}


/**
 * Prints a description of the objective on the given stream
 *
 * @param o The output stream to write to
 */
void DvhCoverageObjective::printOn(std::ostream& o) const
{
    o << "OBJ " << objNo_ << ": "; 
    o << "VOI " << the_voi_->get_voiNo() << "(" << the_voi_->get_name() << ")";

    o << "\tVolume at least " << dose_ << "Gy";
    if(min_volume_ > 0) {
        o << "\t> " << min_volume_*100 << "%";
    }
    if(max_volume_ < 1) {
        o << "\t< " << max_volume_*100 << "%";
    }

    if(weight_ != 1.0) {
	o << "\tweight " << weight_;
    }
}


