
/**
 * @file KonradBixelVector.hpp
 * KonradBixelVector Class Definition.
 * <pre>
 * Ver
 * </pre>
 */

#ifndef KONRADBIXELVECTOR_HPP
#define KONRADBIXELVECTOR_HPP

#include <vector>
using std::vector;
#include <string>
using std::string;

// Predefine class before including others
class KonradBixelVector;

#include "BixelVector.hpp"
#include "KonradDoseInfluenceMatrix.hpp"
#include "Geometry.hpp"

/**
 * Class KonradBixelVector handles individual beams and their bixel intensities.
 * The constructor creates set of bixel positions for those bixels that connect with target voxels.
 */
class KonradBixelVector : public BixelVector {
  public:
    // Constructors

    /// Read bixel vector from .bwf and (optionally) .stf files.
    KonradBixelVector( Geometry *geometry,  const unsigned int nInstances,  const unsigned int nBeams_per_instance,
            const string bwf_file_root,  const string stf_file_root = "" );

    /// construct bixel vector based on Dij matrix
    KonradBixelVector( const DoseInfluenceMatrix & Dij,  Geometry *geometry,  const unsigned int nInstances,
            const unsigned int nBeams_per_instance );

    /// Copy Constructor
    KonradBixelVector( const KonradBixelVector &kbv );

    /// Destructor
    ~KonradBixelVector();

    // Read and write files
    void load_bwf_files(const string & bwf_file_root);
    void load_bwf_file(const string & bwf_file_name, unsigned int beamNo);
    void write_bwf_files(const string & bwf_file_root) const;
    void write_bwf_file(const string & bwf_file_name, const unsigned int beamNo) const;

    /// read a steering file containing bixels inside the aperture
    void load_bixels_in_aperture(const string & stf_file_root);

    // Compare two floats using an absolute tolerance
    bool comp_float_abs(float a, float b, float epsilon) const;


    /**
     * Get a pointer to a copy of the object.
     * N.B. Careful the calling function is responsible for deleting the returned object from memory.
     */
    std::auto_ptr<ParameterVector> get_copy() const {
        std::auto_ptr<ParameterVector> temp( new KonradBixelVector( *this ) );
        return temp;
    }
    std::auto_ptr<BixelVector> get_bixel_copy() const {
        std::auto_ptr<BixelVector> temp( new KonradBixelVector( *this ) );
        return temp;
    }

private:
  KonradBixelVector();

  /// Geometry information about beams
  Geometry *geometry_;

  unsigned int nInstances_;
  unsigned int nBeams_;
  vector<unsigned int> nBeams_in_instance_;
  vector<unsigned int> starting_beamNo_;
  vector<unsigned int> nBixels_in_beam_;
  vector<unsigned int> starting_bixelNo_;

  // Properties of each beam
  // vector<float> gantry_angle_, table_angle_, collimator_angle_;

  // vector<float> SAD_;           // source to axis distance

  friend class KonradDoseInfluenceMatrix;
};


/**
 * Compare two floats using an absolute tolerance
 */
inline bool KonradBixelVector::comp_float_abs(float a, float b, float epsilon) const
{
    if(fabs(a-b) < epsilon) {
        return true;
    }
    return false;
}


#endif // KONRADBIXELVECTOR_HPP
