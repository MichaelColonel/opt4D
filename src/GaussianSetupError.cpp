/**
 * @file: GaussianSetupError.cpp
 * GaussianSetupError Class implementation.
 *
 */

#include "GaussianSetupError.hpp"

const float EPSILON = 0.000001;

/**
 * Constructor
 */
GaussianSetupError::GaussianSetupError(
        const Geometry *the_geometry,
        GaussianSetupError::options options)
: the_geometry_(the_geometry),
    options_(options)
{
}


/**
 * Destructor
 */
GaussianSetupError::~GaussianSetupError()
{
}


/**
 *  Generate random shifts
 */
void GaussianSetupError::generate_random_shifts(
        vector<Shift_in_Patient_Coord>& shifts) const
{
    shifts.resize(options_.nFractions_);

    // generate systematic setup error
    Shift_in_Patient_Coord systematic_shift
        = get_truncated_gaussian_setup_error(
            options_.systematic_sigmaX_, options_.systematic_sigmaY_,
            options_.systematic_sigmaZ_,
            options_.cutoff_);

    // Generate fractiony shifts (systematic+random)
    for(int iFraction = 0; iFraction < options_.nFractions_; iFraction++) {
        shifts[iFraction] = (systematic_shift +
                get_truncated_gaussian_setup_error(
                    options_.random_sigmaX_, options_.random_sigmaY_,
                    options_.random_sigmaZ_,
                    options_.cutoff_));
    }
}


/**
 *  Generate nominal shift.
 *
 *  It is zero.
 */
void GaussianSetupError::generate_nominal_shifts(
        vector<Shift_in_Patient_Coord>& shifts) const
{
    shifts.resize(1);
    shifts[0] = Shift_in_Patient_Coord(0,0,0);
}


/**
 * Get a normal random variable with standard deviation 1
 */
float GaussianSetupError::get_normal_distributed_random_number() const
{
    int iX,iY;
    float fX,fY;
    float fRandomNumber;

    iX = rand();
    iY = rand();

    fX = ((double)iX)/RAND_MAX;
    fY = ((double)iY)/RAND_MAX;

    fRandomNumber = sqrt(-2*log(fX))*cos(2*M_PI*fY);

    return(fRandomNumber);
}


/**
 * Get a normal random variable with standard deviation one and absolute value
 * less than the cutoff.
 */
float GaussianSetupError::get_truncated_normal_distributed_random_number(
        float cutoff) const
{
    float fRandomNumber;
    int maxTry = 100;
    int tryNo = 0;

    while(tryNo<maxTry) {

        fRandomNumber = get_normal_distributed_random_number();

	if(fabs(fRandomNumber) <= cutoff) {
	    return(fRandomNumber);
	}

	tryNo++;
    }

    cout << "tried " << maxTry
        << " times to generate a Gaussian distributed random number" << endl;
    cout << " with sigma 1 and cutoff " << cutoff << endl;
    throw(std::runtime_error("cannot generate Gaussian random number"));
}


/**
 * gives a 3D Gaussian setup error in a 2sigma elipse
*/
Shift_in_Patient_Coord GaussianSetupError::get_truncated_gaussian_setup_error(
    float sigmaX, float sigmaY, float sigmaZ, float cutoff) const
{
    float epsilon = 0.001;
    float tmpx,tmpy,tmpz;
    unsigned int tryNo = 0;
    unsigned int maxTry = 100;

    Shift_in_Patient_Coord temp;

    while(tryNo<maxTry) {

	// generate random error

        if(the_geometry_->comp_float_abs(sigmaX,0.0,epsilon)) {
            temp.x_ = 0;
            tmpx = 0;
        } else {
            temp.x_ = sigmaX * get_normal_distributed_random_number();
	    tmpx = temp.x_/(sigmaX*cutoff);
	}

	if(the_geometry_->comp_float_abs(sigmaY,0.0,epsilon)) {
            temp.y_ = 0;
	    tmpy = 0;
	} else {
	    temp.y_ = sigmaY * get_normal_distributed_random_number();
            tmpy = temp.y_/(sigmaY*cutoff);
	}

        if(the_geometry_->comp_float_abs(sigmaZ,0.0,epsilon)) {
            temp.z_ = 0;
            tmpz = 0;
        } else {
            temp.z_ = sigmaZ * get_normal_distributed_random_number();
            tmpz = temp.z_/(sigmaZ*cutoff);
        }

	// check if this shift is within 2 sigma elipse
	if(tmpx*tmpx + tmpy*tmpy + tmpz*tmpz < 1) {
	    return Shift_in_Patient_Coord(temp);
	}

	// increment counter
	tryNo++;
    }

    // check if max number of trys the get a valid shift is exceeded
    cout << "Number of attempts to get a valid setup shift exceeded "
        << maxTry << endl;
    throw(std::runtime_error("cannot get valid setup shift"));
}


/// Write class to stream
std::ostream & operator<< (
        std::ostream& o,
        const GaussianSetupError& rhs)
{
    o << rhs.options_;
}

//
// GaussianSetupError::options functions
//

/**
 * Constructor from TxtFileParser
 */
GaussianSetupError::options::options(const TxtFileParser* pln_parser)
: nFractions_(1),
    systematic_sigmaX_(0),
    systematic_sigmaY_(0),
    systematic_sigmaZ_(0),
    random_sigmaX_(0),
    random_sigmaY_(0),
    random_sigmaZ_(0),
    cutoff_(2)
{
    // number of fractions
    nFractions_ = pln_parser->get_num_attribute<unsigned int>(
	"Uncertainty_RandomSetup_nFractions");

    // systematic error
    systematic_sigmaX_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_Setup_sigmaX");
    systematic_sigmaY_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_Setup_sigmaY");
    systematic_sigmaZ_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_Setup_sigmaZ");

    // random error
    random_sigmaX_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_RandomSetup_sigmaX");
    random_sigmaY_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_RandomSetup_sigmaY");
    random_sigmaZ_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_RandomSetup_sigmaZ");

    // Gaussian cutoff
    cutoff_ = pln_parser->get_num_attribute<float>(
            "Uncertainty_Gaussian_cutoff");
}


/// Write class to stream
std::ostream & operator<< (
        std::ostream& o,
        const GaussianSetupError::options& rhs)
{
    o << "nFractions: " << rhs.nFractions_ << "\n";
    o << "systematic sigmaX (LR): " << rhs.systematic_sigmaX_ << "\n";
    o << "systematic sigmaY (SI): " << rhs.systematic_sigmaY_ << "\n";
    o << "systematic sigmaZ (AP): " << rhs.systematic_sigmaZ_ << "\n";
    o << "random sigmaX (LR): " << rhs.random_sigmaX_ << "\n";
    o << "random sigmaY (SI): " << rhs.random_sigmaY_ << "\n";
    o << "random sigmaZ (AP): " << rhs.random_sigmaZ_ << "\n";
    o << "Gaussian cutoff: " << rhs.cutoff_ << "\n";
}

