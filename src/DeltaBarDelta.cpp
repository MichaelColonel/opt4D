
/**
 * @file DeltaBarDelta.cpp
 * DeltaBarDelta Class implementation.
 * 
 * $Id: DeltaBarDelta.cpp,v 1.1 2007/10/09 22:30:22 bmartin Exp $
 */


#include "DeltaBarDelta.hpp"


/**
 * Constructor: initialize options
 *
 */
DeltaBarDelta::DeltaBarDelta(
	DeltaBarDelta::options options, ///< The options
	unsigned int nParams ///< The number of bixels
	)
:   Optimizer(nParams),
    options_(options),
    smoothed_gradient_(nParams),
    learning_rate_(nParams, 1),
    is_initialized_(false)
{
}


/*
 * DeltaBarDelta functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/// Calculate the direction based on just the gradient
void DeltaBarDelta::calculate_direction(
	const ParameterVectorDirection & gradient,
	const ParameterVector & param)
{
    // Verify that the opions are valid (otherwise throws exception)
    options_.check_options();

    // Preform any first step actions if this is the first time this function 
    // was called
    if(!is_initialized_) {
        // initialize smoothed gradient to the current gradient
        smoothed_gradient_ = gradient;

        // The learning rate is already initialized to 1

        is_initialized_ = true;
    }
    else {
        // We're already initialized, so find the learning rate and smoothed 
        // gradient

        // Update learning rate (based on previous smoothed gradient)
        for(size_t j = 0; j < learning_rate_.size(); j++) {
            float temp = smoothed_gradient_[j] * gradient[j];
            if(temp < 0)
                // Opposite sign, so decrease learning rate
                learning_rate_[j] *= options_.phi_;
            else if(temp > 0)
                // Same sign, so increase learning rate
                learning_rate_[j] += options_.r_;
        }

        // calculate smoothed gradient
        // eqivalent to sg = theta * sg + (1-theta) * g so long as theta != 1
        smoothed_gradient_ *= options_.theta_ / (1 - options_.theta_);
        smoothed_gradient_ += gradient;
        smoothed_gradient_ *= (1 - options_.theta_);
    }

    // Now we're ready to find the direction
    the_direction_ = gradient;
    the_direction_ *= -options_.step_size_;
    the_direction_ *= learning_rate_;
}


/// Print out options
void DeltaBarDelta::print_options(std::ostream& o) const
{
    o << options_;
}


/*
 * DeltaBarDelta::options functions
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 */

/**
 * Print options to stream
 */
std::ostream& operator<< (std::ostream& o, const DeltaBarDelta::options& opt)
{
    o << "step_size_ = " << opt.step_size_ << "\n";
    o << "r_ = " << opt.r_ << "\n";
    o << "phi_ = " << opt.phi_ << "\n";
    o << "theta_ = " << opt.theta_ << "\n";
    
    return o;
}

/**
 * Check the validity of the options (throw exception if not valid)
 */
void DeltaBarDelta::options::check_options() const
{
    if(step_size_ <= 0) {
        throw std::runtime_error("Invalid step size");
    }

    if(r_ <= 0) {
        throw std::runtime_error("Invalid r_");
    }

    if(phi_ < 0 | phi_ >= 1) {
        throw std::runtime_error("Invalid phi_");
    }

    if(theta_ < 0 | theta_ >= 1) {
        throw std::runtime_error("Invalid theta_");
    }
}

