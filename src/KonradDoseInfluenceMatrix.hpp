/**
 * @file KonradDoseInfluenceMatrix.hpp
 * KonradDoseInfluenceMatrix class header
 */

#ifndef KONRADDOSEINFLUENCEMATRIX_HPP
#define KONRADDOSEINFLUENCEMATRIX_HPP

class KonradDoseInfluenceMatrix;

#include <iostream>
#include <vector>
using std::vector;
#include <string>
using std::string;

#include "DoseInfluenceMatrix.hpp"

#include "BixelVector.hpp"
#include "KonradBixelVector.hpp"

class KonradDoseInfluenceMatrix;

#include "Geometry.hpp"

/**
 * Class KonradDoseInfluenceMatrix holds pre-calculated dose matrix and 
 * calculates dose from a given set of bixel intensities.
 */
class KonradDoseInfluenceMatrix : public DoseInfluenceMatrix
{

public:
    // Constructors

    // Include all bixels
    KonradDoseInfluenceMatrix(
            string dij_file_root,
            Geometry *geometry,
            size_t nBeams,
            size_t nInstances,
            bool transposed = false);

    // Include all bixels
    KonradDoseInfluenceMatrix(
            string dij_file_root,
            Geometry *geometry,
	    bool unify,
            size_t nBeams,
            size_t nInstances,
            bool transposed = false);

    // Only include the bixels that are in the bixel vector
    KonradDoseInfluenceMatrix(
        const string dij_file_root,
        Geometry *geometry,
        const size_t nBeams_per_instance,
        const size_t nInstances,
        const BixelVector & bixel_vector,
        const bool transpose);

  /// Write out KonRad formatted files for Dij matrix
  virtual void write_dij_files ( string dij_file_root );
  
//  void scale_instances(const vector<float> scaling_factors);
//  void combine_instances(const vector<float> scaling_factors);

private:
  // Make sure default constructor is never used
  KonradDoseInfluenceMatrix();

  /// Dij file name
  string dij_file_root_;
  
  size_t nInstances_;
  size_t nBeams_;

  // Information about each instance
  vector<size_t> nBeams_in_instance_;
  vector<size_t> starting_beamNo_;

  // Information about each beam
  vector<unsigned int> nBixels_in_beam_;
  vector<unsigned int> starting_bixelNo_;

  /// Geometry Information
  Geometry *geometry_;

  // vector<float> gantry_angle_, table_angle_, collimator_angle_;
  // vector<float> bixel_dx_, bixel_dy_;
  vector<float> dose_scale_coeficient_;
  
  // Information that is shared by each beam
  // float voxel_dx_, voxel_dy_, voxel_dz_;
  // unsigned int nX_, nY_, nZ_;

  // Information about each beamlet
  // vector<unsigned int> bixel_nNonzero_voxels_;

  // Read a binary little-endian integer from a stream
  unsigned int read_binary_int(std::istream & input);

  // Read a binary little-endian integer from a stream
  unsigned short read_binary_short(std::istream & input);

  // Read a binary little-endian integer from a stream
  float read_binary_float(std::istream & input);

  // Compare two floats using an absolute tolerance
  bool comp_float_abs(float a, float b, float epsilon) const; 

  friend class KonradBixelVector;
};

/*
 * Inline Functions
 */


/**
 * Read a binary little-endian integer from a stream
 */
inline
unsigned int KonradDoseInfluenceMatrix::read_binary_int(std::istream & input)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
	// Need to rearrange bytes
	char SS[4], ST[4];
	input.read(SS, 4);
	ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
	return *( (unsigned int *) ST);
    } else {
	// No need to rearrange bytes
	unsigned int temp;
	input.read((char*) &temp, sizeof(int));
	return temp;
    }
}


/**
 * Read a binary little-endian short integer from a stream
 */
inline
unsigned short KonradDoseInfluenceMatrix::read_binary_short(std::istream & input)
{

#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
	// Need to rearrange bytes
	char SS[2], ST[2];
	input.read(SS, 2);
	ST[0] = SS[1];  ST[1] = SS[0];
	return *( (unsigned short *) ST);
    } else {
	// No need to rearrange bytes
	unsigned short temp;
	input.read((char*) &temp, sizeof(short));
	return temp;
    }
}


/**
 * Read a binary little-endian float integer from a stream
 */
inline
float KonradDoseInfluenceMatrix::read_binary_float(std::istream & input)
{
#if defined(__BIG_ENDIAN__)
    bool big_endian = true;
#else
    bool big_endian = false;
#endif

    if(big_endian) {
	char SS[4], ST[4];
	input.read(SS, 4);
	ST[0] = SS[3];  ST[1] = SS[2];  ST[2] = SS[1];  ST[3] = SS[0];
	return *( (float *) ST);
    } else {
	float temp;

	input.read((char*) &temp, sizeof(float));

	return temp;
    }
}

/** 
 * Compare two floats using an absolute tolerance
 */
inline
bool KonradDoseInfluenceMatrix::comp_float_abs(float a, float b, float epsilon) const
{
    if(fabs(a-b) < epsilon) {
	return true;
    }
    return false;
}
 



#endif
