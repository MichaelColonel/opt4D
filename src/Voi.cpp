
/**
 * @file Voi.cpp
 * Voi Class implementation.
 * $Id: Voi.cpp,v 1.15 2008/03/13 22:06:17 bmartin Exp $
 */

/* Old comments on file:
 *
 * @version 0.9.4
 * <pre>
 * ver 0.9.4     BM      Jul 15, 2004    Changed to hold just one Voi and other
 *                                       changes
 * ver 0.9.3     AT      Apr 09, 2004    added print_VOI_info
 * ver 0.8.10    AT      Feb 27, 2004    minor fixes, changes, cleanup
 * ver 0.5       TB, AT  Feb 08, 2004    creation
 * AT: Alexei Trofimov
 * TB: Thomas Bortfeld
 * BM: Ben Martin
 * Massachusetts General Hospital, Department of Radiation Oncology
 * </pre>
 */

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <cmath>
#include <assert.h>

#include "Voi.hpp"

using namespace std;


/**
 * Constructor: default.
 *
 * @param voiNo the volume number for this volume of interest
 */
Voi::Voi(const unsigned int voiNo)
  : structure_name_("")
  , voiNo_(voiNo)
  , is_air_(false)
  , is_target_(false)
  , is_organ_at_risk_(false)
  , EUD_p_(0)
  , red_(0), green_(0), blue_(0)
  , voxel_indices_()
  , nVoxels_(0)
  , prescription_(new Prescription())
  , max_dose_(0)
  , min_dose_(0)
  , total_dose_(0)
  , EUD_(0)
  , alphabeta_(1000)
  , alpha_(0)
  , beta_(0)
  , betaalpha_(0)
  , standard_deviation_(0)
  , desired_dose_(0)
{
}


/**
 * Destructor.
 */
Voi::~Voi()
{
}


/**
 * Reserve space for Voi to be able to hold a given number of voxels.
 *
 * @param nVoxels the expected number of voxels
 */
void Voi::reserve_voxels( unsigned int nVoxels )
{
  voxel_indices_.reserve(nVoxels);
}

/**
 *  Add a vector of voxels to VOI.
 *  Ensures that the added voxels are sorted and the resultant voxel list does 
 *  not contain duplicate entries.
 *
 *  @param voxelNos The voxelNos to add to the list.  Does not need to be 
 *  sorted, but may not contain duplicate items.
 */
void Voi::add_voxels(const vector<unsigned int> & voxelNos) 
{
  // Quit if there aren't any voxels to add
  if(voxelNos.size() == 0) {
    return;
  }

  // Make sure that voxels to be added are sorted
  vector<unsigned int> sorted_voxelNos = voxelNos;
  sort(sorted_voxelNos.begin(), sorted_voxelNos.end());
  
  if(nVoxels_ == 0 || sorted_voxelNos.front() > voxel_indices_.back()) {

    // Add all elements to the end
    voxel_indices_.reserve(sorted_voxelNos.size() + voxel_indices_.size());
    for(vector<unsigned int>::const_iterator iter= sorted_voxelNos.begin();
       	iter != sorted_voxelNos.end(); iter++) {
      voxel_indices_.push_back(*iter);
    }
  } else {
    // The lists need to be merged
    vector<unsigned int> temp;
    temp.reserve(sorted_voxelNos.size() + voxel_indices_.size());
    vector<unsigned int>::const_iterator iter1= sorted_voxelNos.begin();
    vector<unsigned int>::const_iterator iter2= voxel_indices_.begin();

    // Merge vectors into temp vector until one is empty
    while(iter1 != sorted_voxelNos.end() && iter2 != voxel_indices_.end()) {
      if(*iter1 < *iter2) {
        temp.push_back(*iter1);
        iter1++;
      } else if(*iter1 > *iter2) {
        temp.push_back(*iter2);
        iter2++;
      } else {
        // The two items are equal, so only move one, but move both iterators
        temp.push_back(*iter1);
        iter1++;
        iter2++;
      }
    }

    // copy any remaining elements on non-empty list
    while(iter1 != sorted_voxelNos.end()) {
	temp.push_back(*iter1);
	iter1++;
    }
    while(iter2 != voxel_indices_.end()) {
	temp.push_back(*iter2);
	iter2++;
    }
    
    // Move merged list into voxel_indices_
    voxel_indices_.swap(temp);
  }

  // Make nVoxels_ correct
  nVoxels_ = voxel_indices_.size();
}


/**
 * Union with the voxels of the other Voi
 *
 * @param rhs The other Voi to read voxelNos from.
 */
void Voi::union_Voi(const Voi & rhs)
{
  add_voxels(rhs.voxel_indices_);
}


/**
 * Calculates statistics for the Volume of Interest.  The statistics are the 
 * min, max, mean, and total dose plus the standard deviation and the EUD.
 *
 * @param dose_vector The dose that the statistics are calculated based on
 */
void Voi::calculate_stats(const DoseVector & dose_vector)
{
    unsigned int i;
    float dose;

    unsigned int nVoxels = voxel_indices_.size();

    total_dose_ = 0.f;
    if(nVoxels_ > 0) {
        max_dose_ = dose_vector.get_voxel_dose(voxel_indices_[0]);
        min_dose_ = dose_vector.get_voxel_dose(voxel_indices_[0]);
        total_dose_ = dose_vector.get_voxel_dose(voxel_indices_[0]);

        for (i = 1; i < voxel_indices_.size(); i++) {
            dose = dose_vector.get_voxel_dose(voxel_indices_[i]);
            total_dose_ += dose;
            if(dose > max_dose_)
                max_dose_ = dose;
            if(dose < min_dose_)
                min_dose_ = dose;
        }
        mean_dose_ = total_dose_ / nVoxels_;

        // Calculate standard deviation
        standard_deviation_ = 0.;
        for (i = 0; i < voxel_indices_.size(); i++) {
            dose = dose_vector.get_voxel_dose(voxel_indices_[i]);
            standard_deviation_ += (dose-mean_dose_)*(dose-mean_dose_);
        }
        standard_deviation_ = sqrt(standard_deviation_ / nVoxels_);

        EUD_ = calculate_EUD(dose_vector);
    }
    else {
        max_dose_ = min_dose_ = 0.f;
        standard_deviation_ = 0.;
        EUD_ = 0;
    }
}


/**
 * Calculate EUD based on voxel grid.  More accurate than EUD based on DVH,
 * but much slower.
 *
 * @param dose_vector  the voxel grid to read the dose from
 */
float Voi::calculate_EUD(const DoseVector & dose_vector)
{
  unsigned int i;
  double dEUD, dEUD_p, dose;

  // Check to make sure that the VOI has a valid EUD
  // (EUD_p_ is set correctly, it is not air, and there is at least one voxel)
  if (EUD_p_ == 0. || nVoxels_ == 0 || is_air_) {
    EUD_ = 0.;
    return EUD_;
  }

  // The voi is valid, so procede.

  // loop to determine EUD
  dEUD = 0.;
  dEUD_p = EUD_p_;

  // calculate EUD
  for (i = 0; i < nVoxels_; i++) {
    dose = dose_vector.get_voxel_dose(voxel_indices_[i]);
    dEUD += pow(dose,dEUD_p);
  }

  EUD_ = pow(dEUD / nVoxels_, 1.0 / dEUD_p);
  return EUD_;
}


/**
 * Print VOI info header to stream
 */
void Voi::print_voi_info_header(std::ostream& o) const
{
  o << "# Name\t\tType\tNVoxels\tEUD_p\t";
  // removed:
  // << "PxType\tMaxDose\tWtOver\tMinDose\tWtUnder" << endl; 
}


/**
 * Print VOI info to stream
 */
void Voi::print_voi_info(std::ostream& o) const
{
  // Print voi number and name
  o << voiNo_ << " " << structure_name_ << "\t"; 
  if (structure_name_.size()<6)
    o << "\t"; 

  // Print voi type
  if ( is_target_ )
    o << "Target\t";
  else if ( is_organ_at_risk_ )
    o << "OAR\t";
  else if ( is_air_ )
    o << "Air\t";
  else o << "\t";

  // Print number of voxels in voi
  o << nVoxels_ << "\t";

  // Print EUD_p_ if valid
  if(EUD_p_ != 0.f)
    o << EUD_p_;

  /* Removed functionallity 
   *
   * // Print DVH or EUD constraints if present
   * if (prescription_->is_DVH_type() ) {    
   *   // if (is_organ_at_risk_)
   *     o << "DVH:\t" << prescription_->get_max_DVH_dose(0) 
   *          << "\t" << prescription_->get_weight_over() << "\t";
   *   if (is_target_ ) {
   *     o << prescription_->get_min_DVH_dose(0) << "\t"
   *          << prescription_->get_weight_under() << "\t"; 
   *   }
   *  }
   *  else if (prescription_->is_min_type() || prescription_->is_max_type()) {
   *    o << "MinMax:\t" << prescription_->get_max_dose() 
   *         << "\t" << prescription_->get_weight_over() << "\t";
   *    if (is_target_ ) {
   *      o << prescription_->get_min_dose() << "\t"
   *           << prescription_->get_weight_under() << "\t"; 
   *    }
   *  }
   *  else if (prescription_->is_EUD_type() ) {
   *    o << "EUD:\t" << prescription_->get_max_EUD_dose() 
   *         << "\t" << prescription_->get_weight_over() << "\t";
   *    if (is_target_ ) {
   *      o << prescription_->get_min_EUD_dose() << "\t"
   *           << prescription_->get_weight_under() << "\t"; 
   *    }
   *  }
   *  o << endl;
   */
}


/**
 * Print VOI stats header to cout
 */
void Voi::print_voi_stats_header(std::ostream& o) const
{
  o << "Volume of interest statistics:";
}

/**
 * Print VOI stats to cout
 */
void Voi::print_voi_stats(std::ostream& o) const
{
  // Print voi number and name
  cout << left << setw(3) << voiNo_ << setw(12) << structure_name_;

  // Print number of voxels in voi
  cout << " (" << nVoxels_ << " voxels)";

  if(nVoxels_ > 0) {
    // find_max_dose(dose_vector);
    cout << " Min " << min_dose_;
    cout << ", Max " << max_dose_;
    cout << ", Mean " << total_dose_ / nVoxels_;
    cout << ", StDev " << standard_deviation_;

    if(EUD_p_ != 0) {
      cout << ", EUD " << EUD_;
    }
  }

  cout << endl;
}


/**
 * Sample voxels into the supplied vector
 *
 * When sampling the voxels in VOI without replacement, each voxel will be 
 * sampled or not independently.  The total number of voxels sampled is not 
 * guaranteed, but each voxel is inclued at most once.  This generates one 
 * random number per voxel in the VOI, so it may be slow for large VOIs.
 *
 * @param voxelNos The vector to hold the returned voxel number samples.  Any 
 * existing content is cleared.
 * @param sampling_fraction The fraction of voxels to include.  Between 0 and 1 
 * inclusive.
 * @param sample_with_replacement Set to true to sample each voxel 
 * independently, possibly allowing multiple samples of the same voxel.
 */
void Voi::sample_voxels(
        vector<unsigned int> & voxelNos,
        float sampling_fraction,
        bool sample_with_replacement) const
{
    // Clear old samples
    voxelNos.resize(0);

    unsigned int nVoxels = voxel_indices_.size();

    if(sample_with_replacement) {
        // Sample each voxel independently

        // Precalculate sampling theshold
        int threshold = static_cast<int>(
                sampling_fraction * static_cast<float>(RAND_MAX));

        // Sample each voxel
        for(unsigned int i = 0; i < nVoxels; i++) {
            if(std::rand() <= threshold) {
                voxelNos.push_back(voxel_indices_[i]);
            }
        }
    } else {
        // Sample separately

        // Figure out how many samples to draw
        unsigned int nSamples = static_cast<int>(sampling_fraction * 
                static_cast<float>(nVoxels) + 0.5);

        // Clear old samples
        voxelNos.resize(0);

        float temp_multiplier = static_cast<float>(nVoxels) / 
            (static_cast<float>(RAND_MAX)+1.0f);

        // Draw new samples
        for(unsigned int i = 0; i < nSamples; i++) {
            // Figure out which sample to draw
            unsigned int sampled_voxel = static_cast<int>(
                    static_cast<float>(std::rand()) * temp_multiplier);
            voxelNos.push_back(voxel_indices_[sampled_voxel]);
        }
    }
}

/**
 * Resample voxels in VOI without replacement.
 * Each voxel will be sampled or not independently.  The total number of voxels 
 * sampled is not guaranteed, but each voxel is inclued at most once.  This 
 * generates one random number per voxel in the VOI, so it may be slow for 
 * large VOIs.
 */
// void Voi::resample()
// {
    // sample_voxels(
            // samples_,
            // sampling_fraction_,
            // false);
// }


/**
 * Resample voxels in VOI with replacement.
 * Voxels will be selected independently with no guarantee that the voxels 
 * aren't included multiple times.  This method only generates one random 
 * number per sample, so it should be much faster than the other method when 
 * the sampling fraction is small.
 */
// void Voi::resample_with_replacement()
// {
    // sample_voxels(
            // samples_,
            // sampling_fraction_,
            // true);
// }

/**
 * remove voxels from the voi that belong to another voi
 */
void Voi::remove_Voi(const Voi & rhs)
{
  // copy voxel indices and make sure voxels are sorted
  vector<unsigned int> tmpvoi = voxel_indices_;
  sort(tmpvoi.begin(), tmpvoi.end());

  // sort voxels to remove
  vector<unsigned int> voxelNos = rhs.voxel_indices_;
  sort(voxelNos.begin(), voxelNos.end());

  // use stl function to remove voxels
  vector<unsigned int>::iterator iter;
  iter=set_difference (tmpvoi.begin(), 
		       tmpvoi.end(),
		       voxelNos.begin(),
		       voxelNos.end(),
		       voxel_indices_.begin() );

  // resize
  voxel_indices_.resize(iter - voxel_indices_.begin()); 

  // set number of voxels
  nVoxels_ = voxel_indices_.size();

}
